﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using System.Diagnostics;

namespace BMA.MiddlewareApp.Test
{
    public class MainViewModel
    {

        public MainViewModel()
        {
            this.Tabs = new ObservableCollection<TabViewModel>();
            this.AddItem(null);
        }

        /// <summary>
        /// Gets the collection of tabs.
        /// </summary>
        public ObservableCollection<TabViewModel> Tabs
        {
            get;
            private set;
        }

        /// <summary>
        /// Adds new tab item to the Tabs collection.
        /// </summary>
        public void AddItem(TabViewModel sender)
        {
            TabViewModel newTabItem = new TabViewModel(this);
            newTabItem.Header = "New Tab";
            newTabItem.IsSelected = true;
            if (sender != null)
            {
                int insertIndex = this.Tabs.IndexOf(sender) + 1;
                this.Tabs.Insert(insertIndex, newTabItem);
            }
            else
            {
                this.Tabs.Add(newTabItem);
            }
        }

        /// <summary>
        /// Removes an item from the Tabs collection.
        /// </summary>
        /// <param name="tabItem">The tab item.</param>
        public void RemoveItem(TabViewModel tabItem)
        {
            this.Tabs.Remove(tabItem);
        }
    }
}
