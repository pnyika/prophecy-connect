﻿namespace BMA.MiddlewareApp
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Navigation;
    using BMA.MiddlewareApp.LoginUI;
    using BMA.MiddlewareApp.Web;
    using System.Linq;
    using System.Collections.Generic;
    using System;
    using Telerik.Windows.Controls;
    using System.Xml.Linq;
    using System.ServiceModel.DomainServices.Client;
    using Telerik.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using BMA.MiddlewareApp.Controls;
    using System.Collections.ObjectModel;
using Telerik.Windows.Controls.GridView;
   
    /// <summary>
    /// <see cref="UserControl"/> class providing the main UI for the application.
    /// </summary>
    public partial class MainFrame : UserControl
    {
        /// <summary>
        /// Creates a new <see cref="MainPage"/> instance.
        /// </summary>
        /// 

        EditorContext context = new EditorContext();
        UserInformation activeUser = new UserInformation();
        MenuStructure thisMenuStruct = new MenuStructure();
        public List<BMA.MiddlewareApp.Web.MenuItem> menuList { get; set; }
        public List<BMA.MiddlewareApp.Web.MenuItem> SubMenu { get; set; }
        public static int DisplayID;
        public static int userID;
       // public static int newHeight = outlookBar.Height;
        string title;
        ObservableCollection<TabItemModel> tabItemsModel = new ObservableCollection<TabItemModel>();

        public MainFrame()
        {
           
            InitializeComponent();

           // btnButtons.SaveClick += new EventHandler(btnSave_Click);
            ctlLogin.LoginComplete += (se, ev) => { LoginClick(); };
            if (App.Current.IsRunningOutOfBrowser)
            {
                App.Current.MainWindow.WindowState = WindowState.Maximized;
            }
            LayoutRoot.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
           
           // this.DataContext = new Test.MainViewModel();
            

        }

        void ctlLogin_LoginClick(object sender, EventArgs e)
        {
                LoginClick();
        }
        private void LoginClick()
        {
            if (WebContext.Current.Authentication.User.IsInRole("Admin"))
                this.Content = new Admin.AdminMain();
            else
            {
              string username =  WebContext.Current.Authentication.User.Identity.Name;

              LoadOperation<UserInformation> loadOperation = context.Load(context.GetUserInformationWithMenuStructureQuery().Where(u => u.UserName == username), CallbackUser, null);
                this.tvHierarchyView.AddHandler(Telerik.Windows.Controls.RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.radTreeView_MouseLeftButtonDown), true);
                ctlLogin.Visibility = Visibility.Collapsed;
            }
        }
    //private void CreateTabItems()
    //{
    //    // Create items:
       
    //        tabItemsModel.Add(new TabItemModel()
    //        {
    //            Title ="Start Page",
    //            Content = new Controls.TestPanels()
    //        });
        
    //    // Attach the items:
    //    tabControl.ItemsSource = tabItemsModel;
    //}


    public void OnCloseClicked(object sender, RoutedEventArgs e)
    {
        var tabItem = sender as RadTabItem;
        // Remove the item from the collection the control is bound to
        tabItemsModel.Remove(tabItem.DataContext as TabItemModel);
    }


        void stack1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            //RightClickContentMenu contextMenu = new RightClickContentMenu();
            //contextMenu.Show(e.GetPosition(LayoutRoot));
        }
        private void radTreeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Telerik.Windows.Controls.RadTreeView treeView = sender as Telerik.Windows.Controls.RadTreeView;

            
            Category selectedItem = treeView.SelectedItem as Category;
            if (selectedItem == null)
                return;
            int menuID = Convert.ToInt32(selectedItem.ID);

            //GetTable(menuID);
            // this.Content = new Page2();
            if (selectedItem.DefID != null)
            {

                DisplayID = selectedItem.DefID.Value;
                LoadOperation<FormType> ldop = context.Load(context.GetFormTypeByMenuIDQuery(menuID), CallbackType, null);

                title = selectedItem.Name;
                //  LoadOperation<DisplayDefinition> ldop = context.Load(context.GetDisplayDefinitionsQuery(selectedItem.DefID.Value), CallbackDefDisplay, null);

            }
            else
            {
                if (selectedItem.Name == "Logout")
                {
                    Content = new Admin.AdminMain();
                }
            }
            
        }

        private void CallbackUser(LoadOperation<UserInformation> loadedUser)
        {
            List<UserInformation> orig = loadedUser.Entities.ToList();


           if (orig != null)
           {
               var activeUser = orig.FirstOrDefault();
               userID = activeUser.ID;
               LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => m.MenuStructure_ID == activeUser.MenuStructure_ID || m.MenuStructure_ID == 8), Callback, null);
           }
        }


        private void Callback(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadMenus)
        {
            if (loadMenus != null)
            {
                List<BMA.MiddlewareApp.Web.MenuItem> orig = loadMenus.Entities.ToList().OrderBy(x => x.SequenceNumber).ToList();

                this.tvHierarchyView.ItemsSource = orig;
                LoadData(orig);
            }
        }

        /// <summary>
        /// After the Frame navigates, ensure the <see cref="HyperlinkButton"/> representing the current page is selected
        /// </summary>
        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            //foreach (UIElement child in LinksStackPanel.Children)
            //{
            //    HyperlinkButton hb = child as HyperlinkButton;
            //    if (hb != null && hb.NavigateUri != null)
            //    {
            //        if (hb.NavigateUri.ToString().Equals(e.Uri.ToString()))
            //        {
            //            VisualStateManager.GoToState(hb, "ActiveLink", true);
            //        }
            //        else
            //        {
            //            VisualStateManager.GoToState(hb, "InactiveLink", true);
            //        }
            //    }
            //}
        }
        private void DoubleFrame_Navigated(object sender, NavigationEventArgs e)
        {
            
           // Frame frame =this.parent as Frame
           //this.DoubleFrame = Frame as pare
        }
        /// <summary>
        /// If an error occurs during navigation, show an error window
        /// </summary>
        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            //ContentFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }
        private void DoubleFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            //DoubleFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }

        private void TrippleFrame_Navigated(object sender, NavigationEventArgs e)
        {

        }
        private void TrippleFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
          //  DoubleFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }
  

        private void tvHierarchyView_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            
            
        }



        private void LoadData(List<BMA.MiddlewareApp.Web.MenuItem> elements)
        {
            List<Category> categories = new List<Category>();
            categories = this.GetCategories(elements);
            this.tvHierarchyView.ItemsSource = categories;
        }




        private List<Category> GetCategories(List<BMA.MiddlewareApp.Web.MenuItem> element)
        {
            int id = 0;
            return (from category in element.Where(p => p.ParentMenu_ID == 0)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        DefID = category.DisplayDefinition_ID,

                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private List<Category> GetChild(List<BMA.MiddlewareApp.Web.MenuItem> element, int menuLevel)
        {
           
            return (from category in element.Where(p => p.ParentMenu_ID == menuLevel)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        DefID = category.DisplayDefinition_ID,

                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private void tvHierarchyView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            
           // this.Content = new Page2();
            Category selectedItem = tvHierarchyView.SelectedItem as Category;
           
            try
            {

                string selected =selectedItem.ID.ToString();
               // MessageBox.Show(selected);
                 int menuID = Convert.ToInt32(selected);

                 //GetTable(menuID);
               // this.Content = new Page2();

                 if (selectedItem.DefID != null)
                 {
                     if (selectedItem.Name == "Logout")
                     {
                         MessageBox.Show("Logged out !");
                     }
                     else
                     {
                         LoadOperation<FormType> ldop = context.Load(context.GetFormTypeByMenuIDQuery(menuID), CallbackType, null);
                         DisplayID = selectedItem.DefID.Value;
                         //  LoadOperation<DisplayDefinition> ldop = context.Load(context.GetDisplayDefinitionsQuery(selectedItem.DefID.Value), CallbackDefDisplay, null);
                     }
                 }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //private void CallbackDefDisplay(LoadOperation<DisplayDefinition> lo)
        //{
        //    if (lo.Entities.Count<FormType>() == 0)
        //        return;
        //}


        private void CallbackType(LoadOperation<FormType> lo)
        {
            if (lo.Entities.Count<FormType>() == 0)
                return;
            FormType orig = lo.Entities.FirstOrDefault();

            if (orig.Description == "Single")
            {


                try
                {
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + DisplayID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                    string docPaneName = title.Replace(" ", "");

                    Controls.OneGrid.DisplayID = DisplayID;


                    RadTabItem tab = new RadTabItem();
                    tab.Content = new Controls.OneGrid();
                   // tab.Padding = 
                    tab.DropDownContent = title ;
                    tab.Header = title;
                    tab.Name = title.Replace(" ", "") + DisplayID;
                    tab.IsSelected = true;
                    tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                    tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                    this.MainTab.Items.Add(tab);
                 
                }

                    //RadDocumentPane docPane = FindControl<RadDocumentPane>((UIElement)radPaneGroup, typeof(RadDocumentPane), docPaneName);

                    //if (docPane != null)
                    //    radDocking1.ActivePane = docPane;
                    //else
                    //{
                    //    tg.Name = docPaneName;
                    //    tg.Title = title;
                    //    tg.CanUserClose = true;
                    //    //tg.Content = new Controls.OneGrid();
                    //    tg.Content = new Controls.TestPanels();
                    //    radPaneGroup.Items.Add(tg);
                    //}

                    
                  




                }
                catch
                {

                }
            }
            if (orig.Description == "Double")
            {

                try
                {
                    Controls.TwoGrids.DisplayID = DisplayID;

                    string docPaneName = title.Replace(" ", "");
                     RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + DisplayID);

                     if (pane != null)
                         MainTab.SelectedItem = pane;
                     else
                     {
                         //  string docPaneName = title.Replace(" ", "");

                         Controls.TwoGrids.DisplayID = DisplayID;


                         RadTabItem tab = new RadTabItem();
                         tab.Content = new Controls.TwoGrids();
                         // tab.Padding = 
                         tab.DropDownContent = title;
                         tab.Header = title;
                         tab.Name = title.Replace(" ", "") + DisplayID;
                         tab.IsSelected = true;
                         tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                         tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                         this.MainTab.Items.Add(tab);
                     }
                }
                catch
                {
                }


            }

            if (orig.Description == "Tripple")
            {


               




                Controls.ThreeGrids.DisplayID = DisplayID;

                string docPaneName = title.Replace(" ", "");





                RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + DisplayID);

                if (pane != null)
                    MainTab.SelectedItem = pane;
                else
                {
                    //  string docPaneName = title.Replace(" ", "");

                    Controls.ThreeGrids.DisplayID = DisplayID;


                    RadTabItem tab = new RadTabItem();
                    tab.Content = new Controls.ThreeGrids();
                    // tab.Padding = 
                    tab.DropDownContent = title;
                    tab.Header = title;
                    tab.Name = title.Replace(" ", "") + DisplayID;
                    tab.IsSelected = true;
                    tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                    tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                    this.MainTab.Items.Add(tab);
                  
                }

            }
        }

        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        ContextMenu cMenu;
        RadTabItem selectedTab;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            selectedTab = sender as RadTabItem;
            
             cMenu = new ContextMenu();
            System.Windows.Controls.MenuItem menuItem;

            menuItem = new System.Windows.Controls.MenuItem();
            menuItem.Header = "Close";
            menuItem.Click += new RoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new System.Windows.Controls.MenuItem();
            menuItem.Header = "Close All But This";
            menuItem.Click += new RoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.IsOpen = true;
            cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X;
            cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;

            
        }

        void menuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.MenuItem menu = sender as System.Windows.Controls.MenuItem;

            switch (menu.Header.ToString())
            {
                case "Close":
                    CloseTab();
                    break;

                case "Close All But This":
                    CloseAllTab();
                    break;
                default:
                    break;
            }
            cMenu.IsOpen = false;
        }


        private void CloseTab()
        {
            try
            {
                this.MainTab.Items.Remove(selectedTab);
            }
            catch
            {
            }
        }


        private void CloseAllTab()
        {
            try
            {
                foreach (RadTabItem item in MainTab.Items)
                {
                    if(item != selectedTab)
                     this.MainTab.Items.Remove(item);
                }

            }
            catch
            {
            }
        }
        


        public T FindControl<T>(UIElement parent, Type targetType, string ControlName) where T : FrameworkElement
        {

            if (parent == null) return null;

            if (parent.GetType() == targetType && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }
            T result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                if (FindControl<T>(child, targetType, ControlName) != null)
                {
                    result = FindControl<T>(child, targetType, ControlName);
                    break;
                }
            }
            return result;
        }
    }

  
}

