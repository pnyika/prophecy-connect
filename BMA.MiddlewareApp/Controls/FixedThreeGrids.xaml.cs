﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Markup;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.ComponentModel;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Persistence;
using Telerik.Windows.Persistence.Services;
using System.IO;
using System.Text;
using Telerik.Windows.Persistence.Storage;
using System.Windows.Navigation;
using Telerik.Windows;
using System.Xml.Linq;
using SilverlightMessageBox;
using BMA.MiddlewareApp.AppCode;
using System.Reflection;

namespace BMA.MiddlewareApp.Controls
{
    public partial class FixedThreeGrids : UserControl
    {
        #region local variable
        
        //EOInterfaceContext context = new EOInterfaceContext();
        //private List<GridViewRowInfo> modifiedRows = new List<GridViewRowInfo>();
        IEnumerable _lookup;
        ObservableCollection<DataTableService.DataTableInfo> _tables;
        private string tableName;
        private int tableID;
        private string tableName2;
        private int tableID2;
        private string tableName3;
        private int tableID3;
        private string databaseName;
        private string connString;
        ObservableCollection<DataObject> newDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList = new List<GroupViewFilter>();

        ObservableCollection<DataObject> newDataObjectList2 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList2 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList2 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList2 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList2 = new List<GroupViewFilter>();


        ObservableCollection<DataObject> newDataObjectList3 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList3 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList3 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList3 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList3 = new List<GroupViewFilter>();


        RadGridViewSettings settings = null;
        RadGridViewSettings primarySettings = null;
        RadGridViewSettings settings2 = null;
        RadGridViewSettings primarySettings2 = null;
        RadGridViewSettings settings3 = null;
        RadGridViewSettings primarySettings3 = null;

        private Stream stream;
        private PersistenceManager manager = new PersistenceManager();
        private IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
        private bool useIsolatedStorage;
        private int userID;
        private int displayDefID;
        private string XMLString;
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList = new List<ViewFieldSummary>();
        List<Table> tableList = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary;
        List<ViewRelationship> relationshipList2 = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList2 = new List<ViewFieldSummary>();
        List<Table> tableList2 = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary2;

        List<ViewRelationship> relationshipList3 = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList3 = new List<ViewFieldSummary>();
        List<Table> tableList3 = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary3;

        private int viewID;
        private int viewID2;
        private int viewID3;
        private bool singleTable = true;
        private bool isReadOnly = true;
        private bool singleTable2 = true;
        private bool readOnly2 = true;
        private bool singleTable3 = true;
        private bool readOnly3 = true;
        public static int DisplayID;

        private int fieldIndex = 0;
        private int indexMax = 0;
        private string dropDownName;
        private List<TempField> tempFieldList = new List<TempField>();
        List<DataObject> propertiesList = new List<DataObject>();
        List<string> columnNames = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList = new List<TempExtendedProperty>();



        private int pageNumber = 1;
        private int pageTotal = 1;
        public static int pageSize;
        string sqlText = "";
        string newSqlText = "";
        private bool canGenerateColumns = true;


        int grid1ID = 0;
        int grid2ID = 0;
        int grid3ID = 0;

        private int pageNumber2 = 1;
        private int pageTotal2 = 1;
        public static int pageSize2;
        string sqlText2 = "";
        string newSqlText2 = "";
        private bool canGenerateColumns2 = true;
        private int fieldIndex2 = 0;
        private int indexMax2 = 0;
        private string dropDownName2;
        private List<TempField> tempFieldList2 = new List<TempField>();
        List<DataObject> propertiesList2 = new List<DataObject>();
        List<string> columnNames2 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList2 = new List<TempExtendedProperty>();


        private int pageNumber3 = 1;
        private int pageTotal3 = 1;
        public static int pageSize3;
        string sqlText3 = "";
        string newSqlText3 = "";
        private bool canGenerateColumns3 = true;
        private int fieldIndex3 = 0;
        private int indexMax3 = 0;
        private string dropDownName3;
        private List<TempField> tempFieldList3 = new List<TempField>();
        List<DataObject> propertiesList3 = new List<DataObject>();
        List<string> columnNames3 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList3 = new List<TempExtendedProperty>();

        #endregion
        public FixedThreeGrids()
        {
             InitializeComponent();
            TemplateManager.LoadTemplate("DataGrid/Content.zip");
            LoadLayoutFromString();
            
            userID = MainPage.userID;
            displayDefID = DisplayID;
            getTableData();
            settings = new RadGridViewSettings(this.radGridView1);
            primarySettings = new RadGridViewSettings(this.radGridView1);
            primarySettings.SaveState();
            settings2 = new RadGridViewSettings(this.radGridView2);
            primarySettings2 = new RadGridViewSettings(this.radGridView2);
            primarySettings2.SaveState();

            settings3 = new RadGridViewSettings(this.radGridView3);
            primarySettings3 = new RadGridViewSettings(this.radGridView3);
            primarySettings3.SaveState();

            this.radGridView1.Deleted += new EventHandler<GridViewDeletedEventArgs>(gridView_Deleted);

            loadPageNumber();
            // The events below notify when saving and loading have been completed
            //btnButtons.SaveClick += new EventHandler(btnSave_Click);
            //btnButtons.AddClick += new EventHandler(btnAddNew_Click);
            //btnButtons.DeleteClick += new EventHandler(btnDelete_Click);
            //btnButtons.CancelClick += new EventHandler(btnCancel_Click);
            //btnButtons.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            //btnButtons.SaveFilterClick += new EventHandler(btnSaveConfig_Click);
            //btnButtons.ExportClick += new EventHandler(btnButtons_ExportClick);


            //btnInactive.ExportClick += new EventHandler(btnButtons_ExportClick);
            //btnInactive.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            //btnInactive.FilterClick += new EventHandler(btnSaveConfig_Click);
            btnInactive.Visibility = Visibility.Collapsed;

            //btnButtons2.SaveClick += new EventHandler(btnSave_Click2);
            //btnButtons2.AddClick += new EventHandler(btnAddNew_Click2);
            //btnButtons2.DeleteClick += new EventHandler(btnDelete_Click2);
            //btnButtons2.CancelClick += new EventHandler(btnCancel_Click2);
            //btnButtons2.SetDefaultClick += new EventHandler(btnSetDefault_Click2);
            //btnButtons2.SaveFilterClick += new EventHandler(btnSaveConfig_Click2);
            //btnButtons2.ExportClick += new EventHandler(btnButtons_ExportClick2);


            //btnInactive2.ExportClick += new EventHandler(btnButtons_ExportClick2);
            //btnInactive2.SetDefaultClick += new EventHandler(btnSetDefault_Click2);
            //btnInactive2.FilterClick += new EventHandler(btnSaveConfig_Click2);
            btnInactive2.Visibility = Visibility.Collapsed;


            //btnButtons3.SaveClick += new EventHandler(btnSave_Click3);
            //btnButtons3.AddClick += new EventHandler(btnAddNew_Click3);
            //btnButtons3.DeleteClick += new EventHandler(btnDelete_Click3);
            //btnButtons3.CancelClick += new EventHandler(btnCancel_Click3);
            //btnButtons3.SetDefaultClick += new EventHandler(btnSetDefault_Click3);
            //btnButtons3.SaveFilterClick += new EventHandler(btnSaveConfig_Click3);
            //btnButtons3.ExportClick += new EventHandler(btnButtons_ExportClick3);


         //   btnInactive3.ExportClick += new EventHandler(btnButtons_ExportClick3);
           // btnInactive3.SetDefaultClick += new EventHandler(btnSetDefault_Click3);
          //  btnInactive3.FilterClick += new EventHandler(btnSaveConfig_Click3);
            btnInactive3.Visibility = Visibility.Collapsed;


            //RightClickMenu.Save += new EventHandler(RightClickMenu_Save);
            //RightClickMenu.Cancel += new EventHandler(RightClickMenu_Cancel);
            //RightClickMenu.Delete += new EventHandler(RightClickMenu_Delete);
            //RightClickMenu.SaveFilter += new EventHandler(RightClickMenu_SaveFilter);
            //RightClickMenu.SetDefault += new EventHandler(RightClickMenu_SetDefault);
            //RightClickMenu.AddNew += new EventHandler(RightClickMenu_AddNew);
            //RightClickMenu.Export += new EventHandler(RightClickMenu_Export);

            //stackPanel1.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonDown);
            ////LayoutRoot.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
            //radGridView1.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);

            this.radGridView1.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded);
            //this.radGridView2.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded2);
           // this.radGridView3.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded3);
            ServiceProvider.RegisterPersistenceProvider<ICustomPropertyProvider>(typeof(RadDocking), new DockingCustomPropertyProvider());



            dataPager.Back_Click += new EventHandler(dataPager_Back_Click);
            dataPager.Last_Click += new EventHandler(dataPager_Last_Click);
            dataPager.Next_Click += new EventHandler(dataPager_Next_Click);
            dataPager.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged);
            dataPager.First_Click += new EventHandler(dataPager_First_Click);
            Paging();

            //dataPager2.Back_Click += new EventHandler(dataPager_Back_Click2);
            //dataPager2.Last_Click += new EventHandler(dataPager_Last_Click2);
            //dataPager2.Next_Click += new EventHandler(dataPager_Next_Click2);
            //dataPager2.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged2);
            //dataPager2.First_Click += new EventHandler(dataPager_First_Click2);
            //Paging2();

            //dataPager3.Back_Click += new EventHandler(dataPager_Back_Click3);
            //dataPager3.Last_Click += new EventHandler(dataPager_Last_Click3);
            //dataPager3.Next_Click += new EventHandler(dataPager_Next_Click3);
            //dataPager3.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged3);
            //dataPager3.First_Click += new EventHandler(dataPager_First_Click3);
            //Paging3();
    }

        private void LoadLayoutFromString()
        {

            XDocument categoriesXML = XDocument.Load("ThreeGd.xml");
            string xml = categoriesXML.ToString();
            using (Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                stream.Seek(0, SeekOrigin.Begin);
                this.Docking.LoadLayout(stream);
            }
        }



        private void loadPageNumber()
        {
            List<PageNumber> pageList = new List<PageNumber>();

            pageList.Add(new PageNumber { Size = 10, Display = "10" });
            pageList.Add(new PageNumber { Size = 25, Display = "25" });
            pageList.Add(new PageNumber { Size = 50, Display = "50" });
            pageList.Add(new PageNumber { Size = 75, Display = "75" });
            pageList.Add(new PageNumber { Size = 100, Display = "100" });
            pageList.Add(new PageNumber { Size = 150, Display = "150" });
            pageList.Add(new PageNumber { Size = 250, Display = "250" });
            pageList.Add(new PageNumber { Size = 500, Display = "500" });
            pageList.Add(new PageNumber { Size = 750, Display = "750" });
            pageList.Add(new PageNumber { Size = 1000, Display = "1000" });
            pageList.Add(new PageNumber { Size = 100000000, Display = "All" });
            ddlPageSize.DisplayMemberPath = "Display";
            ddlPageSize.SelectedValuePath = "Size";
            ddlPageSize.ItemsSource = pageList;
            ddlPageSize.SelectedValue = pageSize;

            ddlPageSize2.DisplayMemberPath = "Display";
            ddlPageSize2.SelectedValuePath = "Size";
            ddlPageSize2.ItemsSource = pageList;
            ddlPageSize2.SelectedValue = pageSize2;

            ddlPageSize3.DisplayMemberPath = "Display";
            ddlPageSize3.SelectedValuePath = "Size";
            ddlPageSize3.ItemsSource = pageList;
            ddlPageSize3.SelectedValue = pageSize3;

        }
        #region Grid One
        #region Paging
        void dataPager_First_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = 1;
            ReloadPage(newPageNumber);
        }

        private void Paging()
        {
            dataPager.txtNumber.Text = pageNumber.ToString();
            dataPager.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox = true;
        void dataPager_Number_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox)
                {
                    int newPageNumber = Convert.ToInt32(dataPager.txtNumber.Text);
                    ReloadPage(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox = true;
        }
        private void ReloadPage(int newPageNumber)
        {
            if ((newDataObjectList.Count > 0) || (editedDataObjectList.Count > 0) || (deletedDataObjectList.Count > 0))
            {
                CustomMessage customMessage = new CustomMessage("This page contains some changes, Do you want to save the changes?", CustomMessage.MessageType.Confirm);

                customMessage.OKButton.Click += (obj, args) =>
                {
                    ///SaveChanges();

                    if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                    {
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        isTextBox = false;
                    }
                    else
                    {
                        pageNumber = newPageNumber;
                        dataPager.txtNumber.Text = pageNumber.ToString();
                      //  GetData(newSqlText, pageNumber, pageSize, tableName);
                        //radGridView1.IsBusy = false;
                    }
                };

                customMessage.CancelButton.Click += (obj, args) =>
                {

                    newDataObjectList = new ObservableCollection<DataObject>();
                    editedDataObjectList = new ObservableCollection<DataObject>();
                    deletedDataObjectList = new ObservableCollection<DataObject>();
                    if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                    {
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        isTextBox = false;
                    }
                    else
                    {
                        pageNumber = newPageNumber;
                        dataPager.txtNumber.Text = pageNumber.ToString();
                     //   GetData(newSqlText, pageNumber, pageSize, tableName);
                        //radGridView1.IsBusy = false;
                    }
                };


                customMessage.Show();
            }
            else
            {

                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();
                 //   GetData(newSqlText, pageNumber, pageSize, tableName);
                    //radGridView1.IsBusy = false;
                }
            }




        }

        void dataPager_Next_Click(object sender, EventArgs e)
        {
            if (pageNumber < pageSize)
            {
                isTextBox = false;
                int newPageNumber = pageNumber + 1;
                ReloadPage(newPageNumber);
            }

        }

        void dataPager_Last_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageTotal;
            ReloadPage(newPageNumber);
        }

        void dataPager_Back_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageNumber - 1;
            ReloadPage(newPageNumber);

        }

        #endregion



      



        #region Call Ria Services Method


        private void getTableData()
        {

           // App app = (App)Application.Current;
            //LoadOperation<Model> loadOp = context.Load(context.GetModelsQuery(), CallbackModel, null);
        }

        //private void CallbackModel(LoadOperation<Model> results)
        //{


        //    if (results != null)
        //    {
        //        var modelList  = results.Entities.ToList();
        //        radGridView1.ItemsSource = modelList;


        //    }
        //}


    

    
        #endregion



        #region Private Methods

        private void Cancel()
        {
            editedDataObjectList = new ObservableCollection<DataObject>();
            newDataObjectList = new ObservableCollection<DataObject>();
            deletedDataObjectList = new ObservableCollection<DataObject>();
            cMenu.IsOpen = false;
        }

        private void ExcelExport()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView1.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        //private void SaveChanges()
        //{
        //    try
        //    {
        //        if (newDataObjectList.Count > 0)
        //        {
        //            InsertDataTable(newDataObjectList, tableName);
        //        }


        //        //Update
        //        if (editedDataObjectList.Count > 0)
        //        {
        //            Update(editedDataObjectList, tableName);
        //        }

        //        if (deletedDataObjectList.Count > 0)
        //        {
        //            DeleteDataTable(deletedDataObjectList, tableName);
        //        }
        //        //this.radGridView1.DeferRefresh();
        //    }
        //    catch
        //    {
        //    }
        //}



        private void DeleteRows()
        {
            try
            {

                if (this.radGridView1.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView1.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView1.Items.Remove(item as DataObject);
                    deletedDataObjectList.Add(item);
                }

                int count = deletedDataObjectList.Count;
            }
            catch
            {
            }
        }


        private void ClearFilters()
        {
            primarySettings.LoadOriginalState();
            this.radGridView1.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView1.Columns)
            {
                column.ClearFilters();
            }
            this.radGridView1.FilterDescriptors.ResumeNotifications();
        }

        private void SaveConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings = new RadGridViewSettings(this.radGridView1);

                    string settingsXML = settings.SaveState();
                    config.Setting.Settings = settingsXML;
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML; 

                    //context.SubmitChanges(so =>
                    //{
                    //    if (so.HasError)
                    //    {
                    //        Message.ErrorMessage(so.Error.Message + " Error adding setting...");
                    //    }
                    //    else
                    //    {

                    //        Message.InfoMessage("Successfully Saved");



                    //    }

                    //}, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }

        private void SaveConfigAs()
        {
            try
            {

                settings = new RadGridViewSettings(this.radGridView1);

                string settingsXML = settings.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 1);
                //config.Closed += new EventHandler(ChildWin_Closed);
                config.Show();
            }
            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private void DeleteConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                   // context.UserGridConfigs.Remove(config);
                    //context.SubmitChanges(so =>
                    //{
                    //    if (so.HasError)
                    //    {
                    //        Message.ErrorMessage(so.Error.Message + "....");
                    //    }
                    //    else
                    //    {

                    //        Message.InfoMessage("Successfully Deleted");
                    //        ClearFilters();
                    //        ddlConfig.SelectedValue = 0;


                    //    }

                    //}, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private bool isFirstLoad = true;
        string xmlCache = "";
        string primaryXml = "";
        private void GetDefaultSetting(Setting setting)
        {
            if (isFirstLoad)
            {

                primarySettings = new RadGridViewSettings(this.radGridView1);
                primarySettings.SaveState();
                primaryXml = primarySettings.SaveState();
                isFirstLoad = false;
            }


            ClearFilters();

            if (setting.ID > 0)
            {
                ddlConfig.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    ClearFilters();
                    //  primarySettings.LoadOriginalState();
                    primarySettings.ResetState();

                    settings = new RadGridViewSettings(this.radGridView1);
                    string xml = setting.Settings;
                    xmlCache = xml;
                    settings.LoadState(xml);

                    createSqlWithNewFilters();


                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }

        private string testName = "";


        private void GenerateColumns()
        {
            if (canGenerateColumns)
            {
                int i = 1;
                tempFieldList = new List<TempField>();
                this.radGridView1.AutoGenerateColumns = false;
                this.radGridView1.ShowInsertRow = false;


                foreach (ViewFieldSummary field in viewFieldSummary)
                {

                    if (columnNames.Contains(field.FieldName))
                    {
                        GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                        columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                        columnComboBox.Header = field.DisplayName;
                        columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        columnComboBox.SelectedValueMemberPath = "Value";
                        columnComboBox.DisplayMemberPath = "Display";
                        columnComboBox.IsFilteringDeferred = true;
                        columnComboBox.IsFilteringDeferred = true;
                        this.radGridView1.Columns.Add(columnComboBox);
                    }
                    else
                    {
                        if (field.UseValueField != null)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";
                            columnComboBox.IsFilteringDeferred = true;
                            this.radGridView1.Columns.Add(columnComboBox);

                            tempFieldList.Add(new TempField { ID = field.FieldID, Name = CharacterHandler.ReplaceSpecialCharacter(field.FieldName), Index = i });
                            i++;




                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;

                            if (field.IsCalculatedField.Value == true)
                            {


                                column.IsReadOnly = true;

                                column.Background = Resources["SkyBlue2"] as SolidColorBrush;// new SolidColorBrush(Colors.Blue);
                            }
                           
                            this.radGridView1.Columns.Add(column);



                        }
                    }
                }
                canGenerateColumns = false;

                tempFieldList = tempFieldList.OrderBy(t => t.Index).ToList();
                indexMax = tempFieldList.Count;
                LoadExtendedProperties();
               // LoadDropDowns();



            }
        }


        private void LoadExtendedProperties()
        {
            foreach (DataObject dataObject in propertiesList)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView1.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList;
                }
                catch
                {
                }
            }
        }

        private string CreateReadOnlyTable()
        {
            string sql = string.Empty;
            //table list
            //display field list
            //relationshiplist

            string tables = " From ";
            string fields = " Select ";
            string relationships = " Where ";
            string viewFilter = "";
            foreach (Table table in tableList)
            {
                tables += "[" + table.DBName + "].[dbo].[" + table.TableName + "] as " + table.TableName.ToString().Replace(" ", "") + " ,";
            }

            foreach (ViewFieldSummary field in fieldList)
            {
                if (field.TableID > 0)
                {
                    fields += field.TableField + " ,";//" as [" + field.DisplayName + "] ,";
                }
                else
                {
                    fields += " (" + field.Expression + " ) as [" + field.DisplayName + "] ,";
                }
            }

            foreach (ViewRelationship rship in relationshipList)
            {
                relationships += " " + rship.Summary + " and";

            }

            foreach (GroupViewFilter item in GroupViewFilterList)
            {
                viewFilter += " " + item.Summary + " and";
            }

            tables = tables.TrimEnd(',');
            fields = fields.TrimEnd(',');
            relationships = relationships.TrimEnd('d');
            relationships = relationships.TrimEnd('n');
            relationships = relationships.TrimEnd('a');
            viewFilter = viewFilter.TrimEnd('d');
            viewFilter = viewFilter.TrimEnd('n');
            viewFilter = viewFilter.TrimEnd('a');

            if (relationships == " Where ")
            {
                if (viewFilter != "")
                {
                    relationships += viewFilter;
                }
                else
                {
                    relationships = string.Empty;
                }

            }
            else
            {
                if (viewFilter != "")
                {
                    relationships += " and " + viewFilter;
                }
            }


            sql = fields + tables + relationships;

            return sql;

        }
        #endregion






        #region GridView Events
        private void radGridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e)
        {

        }

        private void radGridView_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList.Contains(dataObject))
                        {
                            if (newDataObjectList.Contains(dataObject))
                            {
                                this.newDataObjectList.Remove(dataObject);
                                this.newDataObjectList.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList.Remove(dataObject);
                            this.editedDataObjectList.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void radGridView_Filtered(object sender, GridViewFilteredEventArgs e)
        {

            createSqlWithNewFilters();
        }


        private void createSqlWithNewFilters()
        {
            string newSql = "";


            //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)


            if (radGridView1.FilterDescriptors.Count > 0)
            {


                newSql += OperatorHandler.BulidQuery(radGridView1);



            }

            if (newSql.Length > 0)
            {
                newSql = newSql.Remove(newSql.Length - 3, 3);

                if (sqlText.Contains("Where"))
                {
                    newSqlText = sqlText + " and " + newSql;
                }
                else
                {
                    newSqlText = sqlText + " Where " + newSql;
                }
            }
            else
            {
                newSqlText = sqlText;

            }

           // GetTotalRows(newSqlText);
            pageNumber = 1;
          //  GetData(newSqlText, pageNumber, pageSize, tableName);
        }

        void gridView_Deleted(object sender, GridViewDeletedEventArgs e)
        {
            DataObject dataObject = e.Items as DataObject;
            deletedDataObjectList.Add(dataObject);



        }


        private void gridView_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e)
        {
            //RadComboBox countries = e.DetailsElement.FindName("rcbCountries") as RadComboBox;
            //countries.ItemsSource = GetCountries();
            // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }
        #endregion


        #region GridMenu Events & Methods
        public MouseButtonEventHandler gridRightClick { get; set; }

        private void radGridView1_Loaded(object sender, GridViewRowItemEventArgs e)
        {
           // /e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }

        private void radGridView1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
           // stack1_MouseRightButtonUp(sender, e);
        }




        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        RadContextMenu cMenu;
        RadTabItem selectedTab;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var element = sender as UIElement;


            cMenu = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Changes";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Add";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Selected Rows";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
            exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "ExcelML";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Word";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Csv";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings As";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Set Current Filter Settings as Defualt";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.PlacementTarget = radGridView1;

            Point p = e.GetPosition(this.radGridView1);


            cMenu.Placement = PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu.HorizontalOffset = p.X;
            cMenu.VerticalOffset = p.Y;


            cMenu.IsOpen = true;
            // cMenu.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            //  RadContextMenu.SetContextMenu(this.radGridView1, cMenu);
            //cMenu.IsOpen = true;
            //  cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X + 350;
            //  cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }
        void RadGridView1_RowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {

                ((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);

            }
        }


        void menuItem_Click(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                       // SaveChanges();
                        break;
                    case "Add New":
                        radGridView1.BeginInsert();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        //SetDefault();
                        break;


                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }

            }
        }

        private void RadContextMenu_ItemClick(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        //SaveChanges();
                        break;
                    case "Add New":
                        radGridView1.BeginInsert();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        //SetDefault();
                        break;


                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }
            }
           // cMenu.IsOpen = false;
        }

        private void Export(string selectedItem)
        {
            try
            {
            string extension = "";
            ExportFormat format = ExportFormat.Html;



            switch (selectedItem)
            {
                case "Excel": extension = "xls";
                    format = ExportFormat.Html;
                    break;
                case "ExcelML": extension = "xml";
                    format = ExportFormat.ExcelML;
                    break;
                case "Word": extension = "doc";
                    format = ExportFormat.Html;
                    break;
                case "Csv": extension = "csv";
                    format = ExportFormat.Csv;
                    break;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = extension;
            dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
            dialog.FilterIndex = 1;
           // dialog.ShowDialog();
           
                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView1.Export(stream, exportOptions);
                    }
                }
                else
                {
                }
            }
            catch
            {
            }
        }


        private void RadContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion


        #endregion



        
    }
}
