﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls;
using SilverlightMessageBox;
using System.Collections;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls.GridView;
using System.IO;
using Telerik.Windows;
using AmCharts.Windows.QuickCharts;
using System.Reflection;
using System.Threading;

namespace BMA.MiddlewareApp.Controls
{
    public partial class NewForm : UserControl
    {
        public static int DisplayID;
        public static int formID;
        List<ObjectFunctionParameter> parameterList = new List<ObjectFunctionParameter>();
        List<ObjectDefintion> objectList = new List<ObjectDefintion>();
        List<PagerController> pagerControllerList = new List<PagerController>();
        List<Dependend> dependends = new List<Dependend>();
        PagerControl dataPager = new PagerControl();
        string connString;
        string databaseName;
        private int pageNumber = 1;
        private int controlID;
        private bool start = false;

        private int pageTotal = 1;
        public  int pageSize = 1000;
        string sqlText = "";
        System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        EditorContext context = new EditorContext();
        public NewForm()
        {
            InitializeComponent();
            GetForm();
            this.Unloaded += new RoutedEventHandler(NewForm_Unloaded);
            this.Loaded += new RoutedEventHandler(NewForm_Loaded);
            MainPage.StatusUpdated += new EventHandler(MainPage_StatusUpdated);
        }
         bool  isUpdating = false;
        void MainPage_StatusUpdated(object sender, EventArgs e)
        {
            if (!isUpdating)
            {
                isUpdating = true;
                Grid grid = sender as Grid;
                if (grid != null)
                    this.LayoutRoot.Height = grid.ActualHeight;
                this.LayoutRoot.Width = grid.ActualWidth;
                this.form.Height = grid.ActualHeight;
                this.form.Width = grid.ActualWidth;
            }

            isUpdating = false;
        }


        void NewForm_Loaded(object sender, RoutedEventArgs e)
        {
           //   if(start)
          // StartTimer();
        }

        void NewForm_Unloaded(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Close()
        {
            myDispatcherTimer.Stop();
        }


        public void StartTimer(object o, RoutedEventArgs sender)
        {
            Button btn = o as Button;
            string txtTag = btn.Tag.ToString();

         //   GetObjectFuction(Convert.ToInt32(txtTag));

            ObjectDefintion displayControl = objectList.Where(obj => obj.ID == Convert.ToInt32(txtTag)).FirstOrDefault();
           // LoadData();
            int sec = 15;
            if (displayControl != null)
            {
                if (displayControl.Interval != null)
                    sec = displayControl.Interval.Value;

            }
            
            myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, sec, 0); // 15seconds 

            // myDispatcherTimer.Tick += new EventHandler(Each_Tick);

            myDispatcherTimer.Tick += delegate(object sendr, EventArgs e)
            { Each_Tick(o, e); };
            myDispatcherTimer.Start();

        }

       private void LoadDropdowns()
        {

            foreach (var item in objectList.Where(o => o.ObjectType == "RadComboBox"))
            {
                GetDropdownFuction(item.ID);
            }
       }


       private void GetDropdownFuction(int ID)
       {

           LoadOperation<ObjectFunction> loadOp = context.Load(context.GetObjectFunctionsQuery().Where(o => o.ObjectDefinition_ID == ID), CallbackDropdownFunction, null);


       }


      /// string displayControlName = "";
       ///string storeProcName = "";
      /// string displayType = "";

       void CallbackDropdownFunction(LoadOperation<ObjectFunction> results)
       {
           displayType = "";

           if (results != null)
           {
               ObjectFunction obj = results.Entities.FirstOrDefault();
               if (obj != null)
               {
                   storeProcName = obj.Query;
                   databaseName = obj.Server.DatabaseName;
                   connString = obj.Server.ConnectionString;
                  /// if ((obj.TargetDisplayObject_ID != null))
                   //{
                       ObjectDefintion displayControl = objectList.Where(o => o.ID == obj.ObjectDefinition_ID).FirstOrDefault();

                       if (displayControl != null)
                       {
                           displayType = displayControl.ObjectType;
                           displayControlName = displayControl.ObjectName + displayControl.ID.ToString();
                           parentId = displayControl.ID;
                           if (displayControl.ObjectType == "RadComboBox")
                           {

                               currentComboxBox = FindControl<RadComboBox>((UIElement)form, typeof(RadComboBox), displayControlName);
                               currentComboxBox.SelectedValuePath = obj.ValueField;
                               currentComboxBox.DisplayMemberPath = obj.DisplayField;
                           }
                           
                         //  controlID = obj.TargetDisplayObject_ID.Value;


                           if (obj.FunctionType == "Stored Procedure")
                           {
                               LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackDropdownFunctionParameter, null);
                           }
                           else
                           {

                               LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackSqlDropdownFunctionParameter, null);


                           }
                       }
                       else
                       {
                           Message.ErrorMessage("Display control not found, Please set DataGrid or listview for the function!");
                       }

                   }
                 //  else
                 //  {
                     //  if (obj.FunctionType == "Stored Procedure")
                   //    {
                        //   LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackObjectFunctionParameter, null);
                   //    }
                       // Message.ErrorMessage("There no control set to display data!");
                  // }
              // }
           }
       }
        public void StartTimer()
        {
           
            LoadData();
           // LoadDropdowns();
            myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 30, 0); // 15seconds 

             myDispatcherTimer.Tick += new EventHandler(Each_Tick);

             myDispatcherTimer.Start();
          

        }

        private void LoadData()
        {
           // myDispatcherTimer.Stop();
            foreach (var item in objectList.Where(o=>o.ObjectType=="RadButton"))
            {
                 GetObjectFuction(item.ID);
            }
            //myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            //myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 30, 0); // 15seconds 
            //myDispatcherTimer.Tick += new EventHandler(Each_Tick);
            //myDispatcherTimer.Start();
        }
     
        // A variable to count with.
        int i = 0;

        // Fires every 15 seconds while the DispatcherTimer is active.
        public void Each_Tick(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string txtTag = btn.Tag.ToString();

            GetObjectFuction(Convert.ToInt32(txtTag));

            
            
          
        
        }



         private void GetForm()
        {

            LoadOperation<ObjectForm> loadOp = context.Load(context.GetObjectFormsQuery().Where(o => o.DisplayDefinition_ID == DisplayID), CallbackObjectFrom, null);


        }




        private void CallbackObjectFrom(LoadOperation<ObjectForm> results)
        {


            if (results != null)
            {

                ObjectForm  form = results.Entities.FirstOrDefault();
                if (form != null)
                {
                    LoadOperation<ObjectDefintion> loadOp = context.Load(context.GetObjectDefintionsByFormIDQuery(form.ID), CallbackObject, null);
                }
            }
        }


        private void GetObjects()
        {

            
        }



        private void CallbackObject(LoadOperation<ObjectDefintion> results)
        {


            if (results != null)
            {
                objectList = results.Entities.ToList();

                foreach (var control in results.Entities)
                {

                    DynamicallyDrawControl(control);
                }
                start = true;
              //  StartTimer();
               // LoadDropdowns();
            }
        }



        private void DynamicallyDrawControl(ObjectDefintion control)
        {
            try
            {
                string xamlstring = control.ObjectProperties.FirstOrDefault().XmlString;
                double w = control.SizeWidth.Value;
                double h = control.SizeHeight.Value;
                if (h < 5)
                    h = 23;


                switch (control.ObjectType)
                {
                    case "TextBlock":
                        // TextBlock newControl = new TextBlock();
                        System.Windows.Controls.Label newControl = (System.Windows.Controls.Label)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //if (w > 3)
                        //    newControl.Width = w;
                        //newControl.Height = h;
                        //newControl.Text = control.Text;

                        form.Children.Add(newControl);
                        Canvas.SetLeft(newControl, control.PositionX - 160);
                        Canvas.SetTop(newControl, control.PositionY - 60);
                        break;

                    case "Label":
                        System.Windows.Controls.Label newLabel = new System.Windows.Controls.Label();
                        if (w > 3)
                            newLabel.Width = w;
                        newLabel.Height = h;
                        newLabel.Content = control.Text;

                        form.Children.Add(newLabel);
                        Canvas.SetLeft(newLabel, control.PositionX - 160);
                        Canvas.SetTop(newLabel, control.PositionY - 60);
                        break;

                    case "TextBox":
                        //TextBox newTextBox = new TextBox();
                        System.Windows.Controls.TextBox newTextBox = (System.Windows.Controls.TextBox)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //if (w > 3)
                        //    newTextBox.Width = w;
                        //newTextBox.Height = h;
                        // newTextBox.Text = control.Text;
                        newTextBox.Name = control.ObjectName + control.ID.ToString();

                        form.Children.Add(newTextBox);
                        Canvas.SetLeft(newTextBox, control.PositionX - 160);
                        Canvas.SetTop(newTextBox, control.PositionY - 60);
                        break;

                    case "RadButton":
                        //  RadButton newRadButton = new RadButton();
                        RadButton newRadButton = (RadButton)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //if (w > 3)
                        //    newRadButton.Width = w;
                        //newRadButton.Height = h;
                        //newRadButton.Content = control.Text;
                        //   newRadButton.Visibility = Visibility.Collapsed;
                        newRadButton.Tag = control.ID.ToString();
                        newRadButton.Name = control.ObjectName + control.ID.ToString();
                        newRadButton.Click += new RoutedEventHandler(newRadButton_Click);
                        if (control.UseTimer.Value == 2)
                        {
                            newRadButton.Loaded += new RoutedEventHandler(StartTimer);
                            newRadButton.Visibility = Visibility.Collapsed;
                        }
                        form.Children.Add(newRadButton);
                        Canvas.SetLeft(newRadButton, control.PositionX - 160);
                        Canvas.SetTop(newRadButton, control.PositionY - 60);

                        break;

                    case "RadDatePicker":
                        //TextBox newTextBox = new TextBox();
                        RadDatePicker radDatePicker = (RadDatePicker)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //if (w > 3)
                        //    newTextBox.Width = w;
                        //newTextBox.Height = h;
                        // newTextBox.Text = control.Text;
                        radDatePicker.Name = control.ObjectName + control.ID.ToString();
                        radDatePicker.SelectedDate = DateTime.Now.Date;
                        form.Children.Add(radDatePicker);
                        Canvas.SetLeft(radDatePicker, control.PositionX - 160);
                        Canvas.SetTop(radDatePicker, control.PositionY - 60);
                        break;

                    case "RadComboBox":
                        //  RadButton newRadButton = new RadButton();
                        RadComboBox radComboBox = (RadComboBox)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //if (w > 3)
                        //    newRadButton.Width = w;
                        //newRadButton.Height = h;
                        //newRadButton.Content = control.Text;
                        //   newRadButton.Visibility = Visibility.Collapsed;
                        radComboBox.Tag = control.ID.ToString();
                        radComboBox.Name = control.ObjectName + control.ID.ToString();
                        radComboBox.SelectionChanged += new Telerik.Windows.Controls.SelectionChangedEventHandler(radComboBox_SelectionChanged);
                        radComboBox.GotFocus += new RoutedEventHandler(radComboBox_GotFocus);
                        //if (control.UseTimer.Value == 2)
                        //{
                        //    newRadButton.Loaded += new RoutedEventHandler(StartTimer);
                        //    newRadButton.Visibility = Visibility.Collapsed;
                        //}
                        form.Children.Add(radComboBox);
                        Canvas.SetLeft(radComboBox, control.PositionX - 160);
                        Canvas.SetTop(radComboBox, control.PositionY - 60);

                        break;
                    case "RadGridView":
                        StackPanel panel = new StackPanel();
                        panel.Orientation = Orientation.Vertical;
                        string panelName = "panel" + control.ObjectName + control.ID.ToString();
                        panel.Name = panelName;
                        RadGridView newGrid = (RadGridView)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //RadGridView newGrid = new RadGridView();
                        if (h > 250)
                        {
                            newGrid.MaxHeight = 500;
                        }

                        if (h < 250)
                        {
                            newGrid.MinHeight = 250;
                        }
                        // newGrid.Height = h;
                        newGrid.Name = control.ObjectName + control.ID.ToString();
                        newGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                        // newGrid.Width = form.ActualWidth - control.PositionX +150 ;
                        newGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        newGrid.ShowColumnFooters = true;
                        //newGrid.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded);
                        newGrid.MouseRightButtonDown += new MouseButtonEventHandler(newGrid_MouseRightButtonDown);
                        newGrid.MouseRightButtonUp += new MouseButtonEventHandler(newGrid_MouseRightButtonUp);

                        form.Children.Add(panel);
                        Canvas.SetLeft(panel, control.PositionX - 160);
                        Canvas.SetTop(panel, control.PositionY - 60);
                        StackPanel panelStack = FindControl<StackPanel>((UIElement)form, typeof(StackPanel), panelName);
                        panel.Children.Add(newGrid);
                        PagerControl pager = new PagerControl();
                        pager.txtNumber.Text = "1";
                        pager.lblTotal.Text = "1";
                        pager.btnBack.Tag = control.ID.ToString();
                        pager.btnFirst.Tag = control.ID.ToString();
                        pager.btnLast.Tag = control.ID.ToString();
                        pager.btnNext.Tag = control.ID.ToString();
                        pager.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        //pager.Width = form.ActualWidth -control.PositionX + 150;
                        pager.Name = "pager" + control.ObjectName + control.ID.ToString();
                        pager.Back_Click += new EventHandler(dataPager_Back_Click);
                        pager.Last_Click += new EventHandler(dataPager_Last_Click);
                        pager.Next_Click += new EventHandler(dataPager_Next_Click);
                        pager.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged);
                        pager.First_Click += new EventHandler(dataPager_First_Click);
                        panel.Children.Add(pager);
                        break;

                    case "SerialChart":



                        SerialChart seriesChart = (SerialChart)System.Windows.Markup.XamlReader.Load(xamlstring);
                        if (h > 250)
                        {
                            //   seriesChart.MaxHeight = 500;
                        }

                        if (h < 250)
                        {
                            seriesChart.MinHeight = 250;
                        }
                        //seriesChart.Height = 500;
                        seriesChart.Name = control.ObjectName + control.ID.ToString();
                        seriesChart.HorizontalAlignment = HorizontalAlignment.Stretch;
                        seriesChart.Tag = control.ID;
                        seriesChart.MaxWidth = form.ActualWidth - 40;

                        seriesChart.Cursor = Cursors.Hand;
                        form.Children.Add(seriesChart);
                        Canvas.SetLeft(seriesChart, control.PositionX - 160);
                        Canvas.SetTop(seriesChart, control.PositionY - 60);
                        break;

                    case "PieChart":

                        PieChart pieChart = (PieChart)System.Windows.Markup.XamlReader.Load(xamlstring);
                        if (h > 250)
                        {
                            //   seriesChart.MaxHeight = 500;
                        }

                        if (h < 250)
                        {
                            pieChart.MinHeight = 250;
                        }
                        //seriesChart.Height = 500;
                        pieChart.Name = control.ObjectName + control.ID.ToString();
                        pieChart.HorizontalAlignment = HorizontalAlignment.Stretch;
                        pieChart.Tag = control.ID;
                        pieChart.MaxWidth = form.ActualWidth - 40;

                        pieChart.Cursor = Cursors.Hand;
                        form.Children.Add(pieChart);
                        Canvas.SetLeft(pieChart, control.PositionX - 160);
                        Canvas.SetTop(pieChart, control.PositionY - 60);
                        break;



                }
            }
            catch
            {
            }

        }

        void radComboBox_GotFocus(object sender, RoutedEventArgs e)
        {

            RadComboBox comboBox = sender as RadComboBox;
            if (comboBox.ItemsSource == null)
            {
                string txtTag = comboBox.Tag.ToString();

                GetDropdownFuction(Convert.ToInt32(txtTag));
            }
            else if (comboBox.Items.Count < 1)
            {
                string txtTag = comboBox.Tag.ToString();

                GetDropdownFuction(Convert.ToInt32(txtTag));
            }
        }

        void radComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            RadComboBox comboBox = sender as RadComboBox;
            string txtTag = comboBox.Tag.ToString();
            var dependList = dependends.Where(d => d.ChildrenID == Convert.ToInt32(txtTag));
            foreach (var item in dependList)
            {
                GetDropdownFuction(item.ParentID);
            }

            //foreach (var item in objectList.Where(o => o.ObjectType == "RadComboBox"))
            //{
            //    GetDropdownFuction(item.ID);

            //}
        }

           
        

     

        private void newRadButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            string txtTag = btn.Tag.ToString();

            GetObjectFuction(Convert.ToInt32(txtTag));


        }

        private void GetObjectFuction(int ID)
        {

            LoadOperation<ObjectFunction> loadOp = context.Load(context.GetObjectFunctionsQuery().Where(o => o.ObjectDefinition_ID == ID), CallbackObjectFunction, null);


        }


        string displayControlName = "";
        string storeProcName = "";
        string displayType = "";
        int parentId = 0;
        void CallbackObjectFunction(LoadOperation<ObjectFunction> results)
        {
            displayType = "";

            if (results != null)
            {
                ObjectFunction obj = results.Entities.FirstOrDefault();
                if (obj != null)
                {
                    storeProcName = obj.Query;
                    databaseName = obj.Server.DatabaseName;
                    connString = obj.Server.ConnectionString;
                    if ((obj.TargetDisplayObject_ID !=  null))
                    {
                       ObjectDefintion displayControl = objectList.Where(o => o.ID == obj.TargetDisplayObject_ID.Value).FirstOrDefault();
                       
                        if (displayControl != null)
                        {
                            displayType = displayControl.ObjectType;
                            displayControlName = displayControl.ObjectName + obj.TargetDisplayObject_ID.Value.ToString();
                            if (displayControl.ObjectType == "SerialChart")
                            {
                                currentGraph = FindControl<SerialChart>((UIElement)form, typeof(SerialChart), displayControlName);
                                isGraph = true;
                                isPie = false;
                            }
                            else if (displayControl.ObjectType == "PieChart")
                            {
                                currentPie = FindControl<PieChart>((UIElement)form, typeof(PieChart), displayControlName);
                                isGraph = true;
                                isPie = true;
                            }
                            else
                            {
                                dataPager = FindControl<PagerControl>((UIElement)form, typeof(PagerControl), "pager" + displayControlName);
                                currentGrid = FindControl<RadGridView>((UIElement)form, typeof(RadGridView), displayControlName);
                                isGraph = false;
                                PagerController pagerContr = new PagerController();
                                pagerContr.ID = obj.TargetDisplayObject_ID.Value;
                                pagerContr.PageNumber = 1;
                                pagerContr.PageSize = pageSize;
                                pagerContr.TotalPages = 1;
                                pagerContr.SqlText = storeProcName;
                            
                                if (obj.FunctionType == "Stored Procedure")
                                    pagerContr.isStoreProc = true;
                                else
                                    pagerContr.isStoreProc = false;
                                pagerControllerList.Add(pagerContr);
                            }
                           
                            controlID = obj.TargetDisplayObject_ID.Value;
                           
                           
                            if (obj.FunctionType == "Stored Procedure")
                            {
                                LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackObjectFunctionParameter, null);
                            }
                            else
                            {

                                LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackSqlObjectFunctionParameter, null);
                              

                            }
                        }
                        else
                        {
                            Message.ErrorMessage("Display control not found, Please set DataGrid or listview for the function!");
                        }

                    }
                    else
                    {
                        if (obj.FunctionType == "Stored Procedure")
                        {
                            LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackObjectFunctionParameter, null);
                        }
                       // Message.ErrorMessage("There no control set to display data!");
                    }
                }
            }
        }

        void CallbackObjectFunctionParameter(LoadOperation<ObjectFunctionParameter> results)
        {


            if (results != null)
            {
                parameterList = new List<ObjectFunctionParameter>();
                parameterList = results.Entities.ToList();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
              
                foreach (var item in parameterList)
                {
                    
                     ObjectDefintion objectDefinition = objectList.Where(o => o.ID == item.ObjectDefinition_ID).FirstOrDefault();
                      string name   = objectDefinition.ObjectName;
                      if (objectDefinition.ObjectType == "TextBox")
                      {
                          TextBox textBox = FindControl<TextBox>((UIElement)form, typeof(TextBox), name + item.ObjectDefinition_ID);
                          parameters.Add(item.ParameterName + ";" + textBox.Text);

                      }
                      if (objectDefinition.ObjectType == "RadComboBox")
                      {
                          RadComboBox comboBox = FindControl<RadComboBox>((UIElement)form, typeof(RadComboBox), name + item.ObjectDefinition_ID);
                          parameters.Add(item.ParameterName + ";" + comboBox.SelectedValue);

                      }

                      if (objectDefinition.ObjectType == "RadDatePicker")
                      {
                          RadDatePicker radDatePicker = FindControl<RadDatePicker>((UIElement)form, typeof(RadDatePicker), name + item.ObjectDefinition_ID);
                          parameters.Add(item.ParameterName + ";" + radDatePicker.SelectedDate.Value.Date);

                      }

                   
                    
                }
              
                if (!isGraph)
                {
                    if (displayType != "")
                    {
                        PagerController controller = pagerControllerList.Where(p => p.ID == controlID).FirstOrDefault();
                        GetTotalProcRows(storeProcName, parameters);
                        controller.Parameters = new ObservableCollection<string>();
                        controller.Parameters = parameters;
                    }

                }
                ExecuteStoreProcedure(storeProcName, parameters, 1, pageSize);
            }
        }


        void CallbackSqlObjectFunctionParameter(LoadOperation<ObjectFunctionParameter> results)
        {


            if (results != null)
            {
                parameterList = new List<ObjectFunctionParameter>();
                parameterList = results.Entities.ToList();
                string[] Split = storeProcName.Split(new Char[] { ' ' });
                 

                foreach (var item in parameterList)
                {

                   

                    string name = objectList.Where(o => o.ID == item.ObjectDefinition_ID).FirstOrDefault().ObjectName;

                    
                    string parameterValue = "";


                    ObjectDefintion objDef = objectList.Where(o => o.ID == item.ObjectDefinition_ID).FirstOrDefault();

                    if (objDef.ObjectType == "TextBox")
                    {
                        TextBox textBox = FindControl<TextBox>((UIElement)form, typeof(TextBox), name + item.ObjectDefinition_ID);
                        parameterValue = "'" + textBox.Text + "'";
                    }
                    if (objDef.ObjectType == "RadComboBox")
                    {
                        RadComboBox comboBox = FindControl<RadComboBox>((UIElement)form, typeof(RadComboBox), name + item.ObjectDefinition_ID);

                        parameterValue = "'" + comboBox.SelectedValue + "'";
                    }

                    if (objDef.ObjectType == "RadDatePicker")
                    {
                        RadDatePicker radDatePicker = FindControl<RadDatePicker>((UIElement)form, typeof(RadDatePicker), name + item.ObjectDefinition_ID);

                        parameterValue = "'" + radDatePicker.SelectedDate.Value + "'";
                    }

                    foreach (var para in Split)
                    {
                        
                        if (para == "@"+item.ParameterName)
                        {

                            storeProcName = storeProcName.Replace(para, parameterValue);
                        }
                    }

                }





                if (!isGraph)
                {
                    if (displayType != "")
                    {
                        PagerController controller = pagerControllerList.Where(p => p.ID == controlID).FirstOrDefault();
                        controller.SqlText = storeProcName;
                        GetTotalRows(storeProcName);
                    }
                }

                GetData(storeProcName, 1, pageSize, "");
            }
        }

        void ExecuteStoreProcedure(string procName, ObservableCollection<string> parameters,int pageNumber, int pageSize)
        {
        if(!isGraph)
            if (displayType != "")
            {
                currentGrid.IsBusy = true;
            }
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted);
            ws.ExecuteStoreProcAsync(connString, databaseName, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }

        void ws_ExecuteStoreProcedureCompleted(object sender, BMA.MiddlewareApp.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);

            else
            {
                if (e.Result != null)
                {

                    if (displayType != "")
                    {

                        IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                        if (e.Result.Tables.Count > 0)
                        {
                            // RadGridView dataGrid = FindControl<RadGridView>((UIElement)form, typeof(RadGridView), displayControlName);
                            if (isGraph)
                            {
                                if (isPie)
                                {
                                    if (currentPie != null)
                                    {

                                        currentPie.DataSource = list;

                                    }
                                }

                                else
                                {
                                    if (currentGraph != null)
                                    {
                                        ObservableCollection<DataObject> dataList = new ObservableCollection<DataObject>();
                                        foreach (var item in list)
                                        {
                                            DataObject newItem = item as DataObject;
                                            dataList.Add(newItem);
                                        }
                                        foreach (var item in dataList)
                                        {
                                            Type t = item.GetType();
                                            PropertyInfo[] props = t.GetProperties();

                                            foreach (PropertyInfo prp in props)
                                            {

                                                try
                                                {

                                                    var name = prp.Name;
                                                    if (prp.PropertyType.ToString() == "System.DateTime")
                                                    {

                                                        var value = prp.GetValue(item, null);
                                                        DateTime newValue = Convert.ToDateTime(value);
                                                        value = newValue.Date.ToShortDateString();

                                                        prp.SetValue(item, value, null);

                                                    }
                                                }

                                                catch { }
                                            }
                                        }

                                        currentGraph.DataSource = dataList;

                                    }
                                }
                            }
                            else
                            {
                                if (currentGrid != null)
                                {
                                    currentGrid.Columns.Clear();
                                    currentGrid.ItemsSource = list;

                                }
                            }
                        }
                    }
                    else
                    {
                        if (displayType == "")
                        {
                            Message.InfoMessage("Successfully executed!");
                        }
                    }
                }
                else
                {
                    if (displayType == "")
                    {
                        Message.InfoMessage("Successfully executed!");
                    }
                }
            }
            if (!isGraph)
                if (displayType != "")
                {
                    currentGrid.IsBusy = false;
                }
        }


        private void GetData(string sql, int pagenumber, int pagesize, object userState)
        {
            if (!isGraph)
                if (displayType != "")
                {
                    currentGrid.IsBusy = true;
                }
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {

                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);


                if (e.Result.Tables.Count > 0)
                {
                    // RadGridView dataGrid = FindControl<RadGridView>((UIElement)form, typeof(RadGridView), displayControlName);
                    if (isGraph)
                    {
                        if (isPie)
                        {
                            if (currentPie != null)
                            {

                                currentPie.DataSource = list;

                            }
                        }

                        else
                        {
                            if (currentGraph != null)
                            {

                                currentGraph.DataSource = list;

                            }
                        }
                    }
                    else
                    {
                        if (currentGrid != null)
                        {
                            currentGrid.Columns.Clear();
                            currentGrid.ItemsSource = list;

                        }
                    }
                }
                else
                {
                    Message.InfoMessage("Successfully executed!");
                }

            }
            if (!isGraph)
                if (displayType != "")
                {
                    currentGrid.IsBusy = false;
                }
        }
        RadGridView currentGrid;
        RadComboBox currentComboxBox;
        SerialChart currentGraph;
        PieChart currentPie;
        bool isPie = false;
        bool isGraph = false;

        void GetTotalProcRows(string parameterSql, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnProcTotalCompleted);
            ws.TotalProcRowsAsync(connString, parameterSql, databaseName, parameters);

            ///.Progress.Start();
        }
       
        void ws_ReturnProcTotalCompleted(object sender, BMA.MiddlewareApp.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);

                if (total > pageSize)
                {
                    double totalDouble = (double)(total / (double)pageSize);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager.lblTotal.Text = tol.ToString();
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    pageTotal = tol;
                    PagerController controller = pagerControllerList.Where(p => p.ID == controlID).FirstOrDefault();
                    controller.TotalPages = pageTotal;
                }
                else
                {
                    dataPager.lblTotal.Text = "1";
                    dataPager.txtNumber.Text = pageNumber.ToString();

                }
            }
            //this.Progress.Stop();
        }

        void GetTotalRows(string parameterSql)
        {
            var ws = WCF.GetService();
            ws.TotalRowsCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs>(ws_ReturnTotalCompleted);
            ws.TotalRowsAsync(connString, parameterSql, databaseName);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted(object sender, BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);

                if (total > pageSize)
                {
                    double totalDouble = (double)(total / (double)pageSize);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager.lblTotal.Text = tol.ToString();
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    pageTotal = tol;
                    PagerController controller = pagerControllerList.Where(p => p.ID == controlID).FirstOrDefault();
                    controller.TotalPages = pageTotal;
                }
                else
                {
                    dataPager.lblTotal.Text = "1";
                    dataPager.txtNumber.Text = pageNumber.ToString();

                }
            }
            //this.Progress.Stop();
        }
        #region Paging


        private void setCurrentGrid(int ID)
        {
            string gridName = objectList.Where(o => o.ID == ID).FirstOrDefault().ObjectName;
            currentGrid = FindControl<RadGridView>((UIElement)form, typeof(RadGridView), gridName +ID.ToString());
            dataPager = FindControl<PagerControl>((UIElement)form, typeof(PagerControl), "pager"+gridName+ID.ToString());

        }
        void dataPager_First_Click(object sender, EventArgs e)
        {
            Control button = (Control)sender;
            int id = Convert.ToInt32(button.Tag.ToString());
            setCurrentGrid(id);
            isTextBox = false;
            int newPageNumber = 1;
            ReloadPage(newPageNumber, id);
           
        }

        private void Paging()
        {
            dataPager.txtNumber.Text = pageNumber.ToString();
            dataPager.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox = true;
        void dataPager_Number_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox)
                {
                    TextBox button = (TextBox)sender;
                    int id = Convert.ToInt32(button.Tag.ToString());
                    setCurrentGrid(id);
                    int newPageNumber = Convert.ToInt32(dataPager.txtNumber.Text);
                    ReloadPage(newPageNumber, id);
                }
            }
            catch
            {

            }
            isTextBox = true;
        }
        private void ReloadPage(int newPageNumber, int currentID)
        {
          
                    if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                    {
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        isTextBox = false;
                    }
                    else
                    {
                        pageNumber = newPageNumber;
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        PagerController controller = pagerControllerList.Where(p => p.ID == currentID).FirstOrDefault();
                        controller.PageNumber = newPageNumber;
                        if (controller.isStoreProc)
                            ExecuteStoreProcedure(controller.SqlText, controller.Parameters, pageNumber, pageSize);

                        else
                            GetData(controller.SqlText, pageNumber, pageSize, "");
                        //radGridView1.IsBusy = false;
                    }
                
            }
            
           




        

        void dataPager_Next_Click(object sender, EventArgs e)
        {
            Control button = (Control)sender;
            int id = Convert.ToInt32(button.Tag.ToString());
            setCurrentGrid(id);
            PagerController controller = pagerControllerList.Where(p => p.ID == id).FirstOrDefault();
            if (controller.PageNumber < controller.TotalPages)
            {
                isTextBox = false;
                int newPageNumber = controller.PageNumber + 1;
                ReloadPage(newPageNumber, id);
            }

        }

        void dataPager_Last_Click(object sender, EventArgs e)
        {
            Control button = (Control)sender;
            int id = Convert.ToInt32(button.Tag.ToString());
            setCurrentGrid(id);
            PagerController controller = pagerControllerList.Where(p => p.ID == id).FirstOrDefault();
            isTextBox = false;
            int newPageNumber = controller.TotalPages;
            ReloadPage(newPageNumber, id);
        }

        void dataPager_Back_Click(object sender, EventArgs e)
        {
            Control button = (Control)sender;
            int id = Convert.ToInt32(button.Tag.ToString());
            setCurrentGrid(id);
            PagerController controller = pagerControllerList.Where(p => p.ID == id).FirstOrDefault();
            isTextBox = false;
            int newPageNumber = controller.PageNumber - 1;
            ReloadPage(newPageNumber, id);

        }

        #endregion

        #region GridMenu Events & Methods

        RadGridView radGrid;

        public MouseButtonEventHandler gridRightClick { get; set; }

       
       




        void newGrid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        RadContextMenu cMenu;
        RadTabItem selectedTab;
        void newGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            
            var element = sender as UIElement;
            radGrid = (RadGridView)sender;

            cMenu = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
          

           

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
            exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "ExcelML";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Word";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Csv";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

       

            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.PlacementTarget = radGrid;

            Point p = e.GetPosition(radGrid);


            cMenu.Placement = PlacementMode.MousePoint;

            cMenu.HorizontalOffset = e.GetPosition(form).X + 120;
            cMenu.VerticalOffset = e.GetPosition(form).Y;
            // cMenu.IconColumnWidth = 0;
           // cMenu.HorizontalOffset = p.X + 200;
           //cMenu.VerticalOffset = p.Y + 100;


            cMenu.IsOpen = true;
            // cMenu.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            //  RadContextMenu.SetContextMenu(this.radGridView1, cMenu);
            //cMenu.IsOpen = true;
            //  cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X + 350;
            //  cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }
        void RadGridView1_RowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {

               // ((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
               // ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);

            }
        }


        void menuItem_Click(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                   
                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }

            }
        }

        private void RadContextMenu_ItemClick(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                  


                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }
            }
            //cMenu.IsOpen = false;
        }

        private void Export(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGrid.Export(stream, exportOptions);
                    }
                }
            }
            catch
            {
                cMenu.IsOpen = false;
            }
        }


        private void RadContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }




        private void Cancel()
        {
          
            cMenu.IsOpen = false;
        }
        #endregion

        void CallbackSqlDropdownFunctionParameter(LoadOperation<ObjectFunctionParameter> results)
        {


            if (results != null)
            {
                parameterList = new List<ObjectFunctionParameter>();
                parameterList = results.Entities.ToList();
                string[] Split = storeProcName.Split(new Char[] { ' ' });


                foreach (var item in parameterList)
                {
                    string parameterValue = "";
                    string name = objectList.Where(o => o.ID == item.ObjectDefinition_ID).FirstOrDefault().ObjectName;
                    ObjectDefintion objDef = objectList.Where(o => o.ID == item.ObjectDefinition_ID).FirstOrDefault();

                    if (objDef.ObjectType == "TextBox")
                    {
                        TextBox textBox = FindControl<TextBox>((UIElement)form, typeof(TextBox), name + item.ObjectDefinition_ID);
                         parameterValue = "'" + textBox.Text + "'";
                    }
                    if (objDef.ObjectType == "RadComboBox")
                    {
                        RadComboBox comboBox = FindControl<RadComboBox>((UIElement)form, typeof(RadComboBox), name + item.ObjectDefinition_ID);
                       
                        parameterValue = "'" + comboBox.SelectedValue + "'";
                    }
                    if (objDef.ObjectType == "RadDatePicker")
                    {
                        RadDatePicker datePicker = FindControl<RadDatePicker>((UIElement)form, typeof(RadDatePicker), name + item.ObjectDefinition_ID);

                        parameterValue = "'" + datePicker.SelectedDate.Value + "'";
                    }


                    foreach (var para in Split)
                    {

                        if (para == "@" + item.ParameterName)
                        {

                            storeProcName = storeProcName.Replace(para, parameterValue);
                        }
                    }
                    var list = dependends.Where(d => d.ParentID == parentId & d.ChildrenID == item.ObjectDefinition_ID).ToList();
                    if (list.Count < 1)
                    {
                        dependends.Add(new Dependend { ParentID = parentId, ChildrenID = item.ObjectDefinition_ID });
                    }
                }







                GetDropdownData(storeProcName, 1, pageSize, "");
            }
        }

        void CallbackDropdownFunctionParameter(LoadOperation<ObjectFunctionParameter> results)
        {


            if (results != null)
            {
                parameterList = new List<ObjectFunctionParameter>();
                parameterList = results.Entities.ToList();
             
                ObservableCollection<string> parameters = new ObservableCollection<string>();

                foreach (var item in parameterList)
                {

                    ObjectDefintion objectDefinition = objectList.Where(o => o.ID == item.ObjectDefinition_ID).FirstOrDefault();
                    string name = objectDefinition.ObjectName;
                    if (objectDefinition.ObjectType == "TextBox")
                    {
                        TextBox textBox = FindControl<TextBox>((UIElement)form, typeof(TextBox), name + item.ObjectDefinition_ID);
                        parameters.Add(item.ParameterName + ";" + textBox.Text);

                    }
                    if (objectDefinition.ObjectType == "RadComboBox")
                    {
                        RadComboBox comboBox = FindControl<RadComboBox>((UIElement)form, typeof(RadComboBox), name + item.ObjectDefinition_ID);
                        parameters.Add(item.ParameterName + ";" + comboBox.SelectedValue);

                    }                  

                    var list = dependends.Where(d => d.ParentID == parentId & d.ChildrenID == item.ObjectDefinition_ID).ToList();
                    if (list.Count < 1)
                    {
                        dependends.Add(new Dependend { ParentID = parentId, ChildrenID = item.ObjectDefinition_ID });
                    }

                }

              
                ExecuteDropdownStoreProcedure(storeProcName, parameters, 1, pageSize);
            }
        }

        private void GetDropdownData(string sql, int pagenumber, int pagesize, object userState)
        {
           
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDropdownDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDropdownDataCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {

                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);

               // currentComboxBox.DisplayMemberPath = "ModelName";
               // currentComboxBox.SelectedValuePath = "ID";
                currentComboxBox.ItemsSource = list;

            }
         
        }


        void ExecuteDropdownStoreProcedure(string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
           
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteDropdownStoreProcedureCompleted);
            ws.ExecuteStoreProcAsync(connString, databaseName, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }

        void ws_ExecuteDropdownStoreProcedureCompleted(object sender, BMA.MiddlewareApp.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                if (e.Result != null)
                {
                    IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                  
                        // RadGridView dataGrid = FindControl<RadGridView>((UIElement)form, typeof(RadGridView), displayControlName);
                        currentComboxBox.ItemsSource = list;
                    
                }
            }
        }


        public T FindControl<T>(UIElement parent, Type targetType, string ControlName) where T : FrameworkElement
        {

            if (parent == null) return null;

            if (parent.GetType() == targetType && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }
            T result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                if (FindControl<T>(child, targetType, ControlName) != null)
                {
                    result = FindControl<T>(child, targetType, ControlName);
                    break;
                }
            }
            return result;
        }




       
    }

    public class PagerController
    {
        public int ID { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public string SqlText { get; set; }
        public bool isStoreProc { get; set; }
        public ObservableCollection<string> Parameters {get;set;}  
    }

    public class Dependend
    {
        public int ParentID { get; set; }
        public int ChildrenID { get; set; }
    }
}
