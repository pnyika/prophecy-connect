﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.Controls
{
    public partial class PagerControl : UserControl
    {
        public event EventHandler First_Click;
        public event EventHandler Back_Click;
        public event EventHandler Next_Click;
        public event EventHandler Last_Click;
        public event EventHandler Number_TextChanged;

        public PagerControl()
        {
            InitializeComponent();
        }

        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            First_Click(sender, e);
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Back_Click(sender, e);
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            Next_Click(sender, e);
        }

        private void btnLast_Click(object sender, RoutedEventArgs e)
        {
            Last_Click(sender, e);
        }

        private void txtNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            Number_TextChanged(sender, e);
        }
    }
}
