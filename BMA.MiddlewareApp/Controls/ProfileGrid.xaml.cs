﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Markup;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.ComponentModel;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Persistence;
using Telerik.Windows.Persistence.Services;
using System.IO;
using System.Text;
using Telerik.Windows.Persistence.Storage;
using System.Windows.Navigation;
using Telerik.Windows;
using System.Xml.Linq;
using SilverlightMessageBox;
using BMA.MiddlewareApp.AppCode;
using System.Reflection;

namespace BMA.MiddlewareApp.Controls
{
    public partial class ProfileGrid : UserControl
    {
        #region local variable
        EditorContext context = new EditorContext();
        //private List<GridViewRowInfo> modifiedRows = new List<GridViewRowInfo>();
        IEnumerable _lookup;
        ObservableCollection<DataTableService.DataTableInfo> _tables;
        private string tableName;
        private int tableID;
        private string tableName2;
        private int tableID2;
        private string tableName3;
        private int tableID3;
        private string databaseName;
        private string connString;
        ObservableCollection<DataObject> newDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList = new List<GroupViewFilter>();
        ObservableCollection<DataObject> ModelList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> newDataObjectList2 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList2 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList2 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList2 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList2 = new List<GroupViewFilter>();


        ObservableCollection<DataObject> newDataObjectList3 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList3 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList3 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList3 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList3 = new List<GroupViewFilter>();
        List<GridRelationship> GridRelationshipList = new List<GridRelationship>();
        List<GridRelationship> GridRelationshipList3 = new List<GridRelationship>();

        RadGridViewSettings settings = null;
        RadGridViewSettings primarySettings = null;
        RadGridViewSettings settings2 = null;
        RadGridViewSettings primarySettings2 = null;
        RadGridViewSettings settings3 = null;
        RadGridViewSettings primarySettings3 = null;

        private Stream stream;
        private PersistenceManager manager = new PersistenceManager();
        private IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
        private bool useIsolatedStorage;
        private int userID;
        private int displayDefID;
        private string XMLString;
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList = new List<ViewFieldSummary>();
        List<Table> tableList = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary;
        List<ViewRelationship> relationshipList2 = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList2 = new List<ViewFieldSummary>();
        List<Table> tableList2 = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary2;

        List<ViewRelationship> relationshipList3 = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList3 = new List<ViewFieldSummary>();
        List<Table> tableList3 = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary3;

        private int viewID;
        private int viewID2;
        private int viewID3;
        private bool singleTable = true;
        private bool isReadOnly = true;
        private bool singleTable2 = true;
        private bool readOnly2 = true;
        private bool singleTable3 = true;
        private bool readOnly3 = true;
        public static int DisplayID;

        private int fieldIndex = 0;
        private int indexMax = 0;
        private string dropDownName;
        private List<TempField> tempFieldList = new List<TempField>();
        List<DataObject> propertiesList = new List<DataObject>();
        List<string> columnNames = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList = new List<TempExtendedProperty>();



        private int pageNumber = 1;
        private int pageTotal = 1;
        public static int pageSize;
        string sqlText = "";
        string newSqlText = "";
        private bool canGenerateColumns = true;


        int grid1ID = 0;
        int grid2ID = 0;
        int grid3ID = 0;

        private int pageNumber2 = 1;
        private int pageTotal2 = 1;
        public static int pageSize2;
        string sqlText2 = "";
        string newSqlText2 = "";
        private bool canGenerateColumns2 = true;
        private int fieldIndex2 = 0;
        private int indexMax2 = 0;
        private string dropDownName2;
        private List<TempField> tempFieldList2 = new List<TempField>();
        List<DataObject> propertiesList2 = new List<DataObject>();
        List<string> columnNames2 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList2 = new List<TempExtendedProperty>();


        private int pageNumber3 = 1;
        private int pageTotal3 = 1;
        public static int pageSize3;
        public static long ModelID;
        string sqlText3 = "";
        string newSqlText3 = "";
        private bool canGenerateColumns3 = true;
        private int fieldIndex3 = 0;
        private int indexMax3 = 0;
        private string dropDownName3;
        private List<TempField> tempFieldList3 = new List<TempField>();
        List<DataObject> propertiesList3 = new List<DataObject>();
        List<string> columnNames3 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList3 = new List<TempExtendedProperty>();

        #endregion
        public ProfileGrid()
        {
             InitializeComponent();
            TemplateManager.LoadTemplate("DataGrid/Content.zip");
            LoadLayoutFromString();
            
            userID = MainPage.userID;
            displayDefID = DisplayID;
           // getTableData();
            settings = new RadGridViewSettings(this.radGridView1);
            primarySettings = new RadGridViewSettings(this.radGridView1);
            primarySettings.SaveState();
            settings2 = new RadGridViewSettings(this.radGridView2);
            primarySettings2 = new RadGridViewSettings(this.radGridView2);
            primarySettings2.SaveState();

            settings3 = new RadGridViewSettings(this.radGridView3);
            primarySettings3 = new RadGridViewSettings(this.radGridView3);
            primarySettings3.SaveState();

            this.radGridView1.Deleted += new EventHandler<GridViewDeletedEventArgs>(gridView_Deleted);

            // The events below notify when saving and loading have been completed
            btnButtons.SaveClick += new EventHandler(btnSave_Click);
            btnButtons.AddClick += new EventHandler(btnAddNew_Click);
            btnButtons.DeleteClick += new EventHandler(btnDelete_Click);
            btnButtons.CancelClick += new EventHandler(btnCancel_Click);
            btnButtons.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            btnButtons.SaveFilterClick += new EventHandler(btnSaveConfig_Click);
            btnButtons.ExportClick += new EventHandler(btnButtons_ExportClick);


            btnInactive.ExportClick += new EventHandler(btnButtons_ExportClick);
            btnInactive.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            btnInactive.FilterClick += new EventHandler(btnSaveConfig_Click);
            btnInactive.Visibility = Visibility.Collapsed;

            btnButtons2.SaveClick += new EventHandler(btnSave_Click2);
            btnButtons2.AddClick += new EventHandler(btnAddNew_Click2);
            btnButtons2.DeleteClick += new EventHandler(btnDelete_Click2);
            btnButtons2.CancelClick += new EventHandler(btnCancel_Click2);
            btnButtons2.SetDefaultClick += new EventHandler(btnSetDefault_Click2);
            btnButtons2.SaveFilterClick += new EventHandler(btnSaveConfig_Click2);
            btnButtons2.ExportClick += new EventHandler(btnButtons_ExportClick2);


            btnInactive2.ExportClick += new EventHandler(btnButtons_ExportClick2);
            btnInactive2.SetDefaultClick += new EventHandler(btnSetDefault_Click2);
            btnInactive2.FilterClick += new EventHandler(btnSaveConfig_Click2);
            btnInactive2.Visibility = Visibility.Collapsed;


            btnButtons3.SaveClick += new EventHandler(btnSave_Click3);
            btnButtons3.AddClick += new EventHandler(btnAddNew_Click3);
            btnButtons3.DeleteClick += new EventHandler(btnDelete_Click3);
            btnButtons3.CancelClick += new EventHandler(btnCancel_Click3);
            btnButtons3.SetDefaultClick += new EventHandler(btnSetDefault_Click3);
            btnButtons3.SaveFilterClick += new EventHandler(btnSaveConfig_Click3);
            btnButtons3.ExportClick += new EventHandler(btnButtons_ExportClick3);


            btnInactive3.ExportClick += new EventHandler(btnButtons_ExportClick3);
            btnInactive3.SetDefaultClick += new EventHandler(btnSetDefault_Click3);
            btnInactive3.FilterClick += new EventHandler(btnSaveConfig_Click3);
            btnInactive3.Visibility = Visibility.Collapsed;


            RightClickMenu.Save += new EventHandler(RightClickMenu_Save);
            RightClickMenu.Cancel += new EventHandler(RightClickMenu_Cancel);
            RightClickMenu.Delete += new EventHandler(RightClickMenu_Delete);
            RightClickMenu.SaveFilter += new EventHandler(RightClickMenu_SaveFilter);
            RightClickMenu.SetDefault += new EventHandler(RightClickMenu_SetDefault);
            RightClickMenu.AddNew += new EventHandler(RightClickMenu_AddNew);
            RightClickMenu.Export += new EventHandler(RightClickMenu_Export);

            stackPanel1.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonDown);
            //LayoutRoot.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
            radGridView1.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);

            this.radGridView1.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded);
            this.radGridView2.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded2);
            this.radGridView3.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded3);
            ServiceProvider.RegisterPersistenceProvider<ICustomPropertyProvider>(typeof(RadDocking), new DockingCustomPropertyProvider());

            loadPageNumber();
            
            dataPager.Back_Click += new EventHandler(dataPager_Back_Click);
            dataPager.Last_Click += new EventHandler(dataPager_Last_Click);
            dataPager.Next_Click += new EventHandler(dataPager_Next_Click);
            dataPager.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged);
            dataPager.First_Click += new EventHandler(dataPager_First_Click);
            Paging();

            dataPager2.Back_Click += new EventHandler(dataPager_Back_Click2);
            dataPager2.Last_Click += new EventHandler(dataPager_Last_Click2);
            dataPager2.Next_Click += new EventHandler(dataPager_Next_Click2);
            dataPager2.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged2);
            dataPager2.First_Click += new EventHandler(dataPager_First_Click2);
            Paging2();

            dataPager3.Back_Click += new EventHandler(dataPager_Back_Click3);
            dataPager3.Last_Click += new EventHandler(dataPager_Last_Click3);
            dataPager3.Next_Click += new EventHandler(dataPager_Next_Click3);
            dataPager3.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged3);
            dataPager3.First_Click += new EventHandler(dataPager_First_Click3);
            Paging3();
            LoadConfigSetting();
            getTableData();
            MainPage.StatusUpdated += new EventHandler(MainPage_StatusUpdated);
            this.topSpliter.LayoutUpdated += new EventHandler(slipt_LayoutUpdated);
            MainPage.cnUpdate = true;

            radGridView1.SelectionChanged += radGridView1_SelectionChanged;
        }

        


        private void LoadLayoutFromString()
        {

            XDocument categoriesXML = XDocument.Load("ThreeGd.xml");
            string xml = categoriesXML.ToString();
            using (Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                stream.Seek(0, SeekOrigin.Begin);
                this.Docking.LoadLayout(stream);
            }
        }

        void slipt_LayoutUpdated(object sender, EventArgs e)
        {
            if (cnLayoutUpdate)
            {
               StartTimer();
            }
        }


        System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        bool cnLayoutUpdate = true;
        public void StartTimer()
        {

            cnLayoutUpdate = false;
            myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 4, 0); // 15seconds 

            myDispatcherTimer.Tick += new EventHandler(Each_Tick);

            myDispatcherTimer.Start();


        }

        public void Each_Tick(object sender, EventArgs e)
        {
            try
            {
                double height = newHeight;//this.myGrid.Height;
                double datagridHeight = (height / 2) - 30;
                double h = topPane.Height - 40;
                topSpliter.Height = datagridHeight + 30;
                this.radGridView1.Height = datagridHeight;
                //double ht = topSpliter.ActualHeight;
                //double testh =firstpane.Height;
                this.radGridView2.Height = datagridHeight;
                this.radGridView3.Height = datagridHeight;
                myDispatcherTimer.Stop();
                cnLayoutUpdate = true;
            }
            catch { }
        }

        bool isUpdating = false;
        double newHeight;
        void MainPage_StatusUpdated(object sender, EventArgs e)
        {
            try
            {
                if (!isUpdating)
                {
                    isUpdating = true;
                    Grid grid = sender as Grid;
                    if (grid != null)
                    {
                        this.myGrid.Height = grid.ActualHeight - 25;
                        Docking.Height = grid.ActualHeight - 30;
                        newHeight = grid.ActualHeight - 25;
                    }
                }

            }
            catch
            {
            }
            isUpdating = false;
        }

        #region Grid One
        #region Paging


        private void loadPageNumber()
        {
            List<PageNumber> pageList = new List<PageNumber>();

            pageList.Add(new PageNumber { Size = 10, Display = "10" });
            pageList.Add(new PageNumber { Size = 25, Display = "25" });
            pageList.Add(new PageNumber { Size = 50, Display = "50" });
            pageList.Add(new PageNumber { Size = 75, Display = "75" });
            pageList.Add(new PageNumber { Size = 100, Display = "100" });
            pageList.Add(new PageNumber { Size = 150, Display = "150" });
            pageList.Add(new PageNumber { Size = 250, Display = "250" });
            pageList.Add(new PageNumber { Size = 500, Display = "500" });
            pageList.Add(new PageNumber { Size = 750, Display = "750" });
            pageList.Add(new PageNumber { Size = 1000, Display = "1000" });
            pageList.Add(new PageNumber { Size = 100000000, Display = "All" });
            ddlPageSize.DisplayMemberPath = "Display";
            ddlPageSize.SelectedValuePath = "Size";
            ddlPageSize.ItemsSource = pageList;
            ddlPageSize.SelectedValue = pageSize;

            ddlPageSize2.DisplayMemberPath = "Display";
            ddlPageSize2.SelectedValuePath = "Size";
            ddlPageSize2.ItemsSource = pageList;
            ddlPageSize2.SelectedValue = pageSize2;

            ddlPageSize3.DisplayMemberPath = "Display";
            ddlPageSize3.SelectedValuePath = "Size";
            ddlPageSize3.ItemsSource = pageList;
            ddlPageSize3.SelectedValue = pageSize3;

        }

        void dataPager_First_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = 1;
            ReloadPage(newPageNumber);
        }

        private void Paging()
        {
            dataPager.txtNumber.Text = pageNumber.ToString();
            dataPager.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox = true;
        void dataPager_Number_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox)
                {
                    int newPageNumber = Convert.ToInt32(dataPager.txtNumber.Text);
                    ReloadPage(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox = true;
        }
        private void ReloadPage(int newPageNumber)
        {
            if ((newDataObjectList.Count > 0) || (editedDataObjectList.Count > 0) || (deletedDataObjectList.Count > 0))
            {
                CustomMessage customMessage = new CustomMessage("This page contains some changes, Do you want to save the changes?", CustomMessage.MessageType.Confirm);

                customMessage.OKButton.Click += (obj, args) =>
                {
                    SaveChanges();

                    if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                    {
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        isTextBox = false;
                    }
                    else
                    {
                        pageNumber = newPageNumber;
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        GetData(newSqlText, pageNumber, pageSize, tableName);
                        //radGridView1.IsBusy = false;
                    }
                };

                customMessage.CancelButton.Click += (obj, args) =>
                {

                    newDataObjectList = new ObservableCollection<DataObject>();
                    editedDataObjectList = new ObservableCollection<DataObject>();
                    deletedDataObjectList = new ObservableCollection<DataObject>();
                    if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                    {
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        isTextBox = false;
                    }
                    else
                    {
                        pageNumber = newPageNumber;
                        dataPager.txtNumber.Text = pageNumber.ToString();
                        GetData(newSqlText, pageNumber, pageSize, tableName);
                        //radGridView1.IsBusy = false;
                    }
                };


                customMessage.Show();
            }
            else
            {

                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    GetData(newSqlText, pageNumber, pageSize, tableName);
                    //radGridView1.IsBusy = false;
                }
            }




        }

        void dataPager_Next_Click(object sender, EventArgs e)
        {
            if (pageNumber < pageSize)
            {
                isTextBox = false;
                int newPageNumber = pageNumber + 1;
                ReloadPage(newPageNumber);
            }

        }

        void dataPager_Last_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageTotal;
            ReloadPage(newPageNumber);
        }

        void dataPager_Back_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageNumber - 1;
            ReloadPage(newPageNumber);

        }

        #endregion



        #region Events

        void LayoutRoot_LayoutUpdated(object sender, EventArgs e)
        {
            //  radGridView1.MaxHeight = this.stack1.RenderSize.Height;
        }


        private void btnCascade_Click(object sender, RoutedEventArgs e)
        {
            ClearFilters();
            //primarySettings.LoadOriginalState();
            //   primarySettings.ResetState();


            settings = new RadGridViewSettings(this.radGridView1);
            UserGridConfig setting = ddlConfig.SelectedItem as UserGridConfig;
            if (setting != null)
            {
                if (setting.Settings_ID > 0)
                {

                    LoadOperation<Setting> loadOperation = context.Load(context.GetSettingsQuery().Where(x => x.ID == setting.Settings_ID), CallbackSettings, null);

                }
                else
                {
                    PullData();
                }
            }
            else
            {
                PullData();
            }


        }

        void PullData()
        {
          sqlText=  CreateReadOnlyTable();
            createSqlWithNewFilters();
            GetTotalRows(newSqlText);
            pageNumber = 1;
            GetData(newSqlText, pageNumber, pageSize, tableName);
        }


        void CallbackSettings(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache = xml;
                settings.LoadState(xml);
            }

            PullData();
        }
        void RightClickMenu_Export(object sender, EventArgs e)
        {
            ExcelExport();
        }

        void RightClickMenu_AddNew(object sender, EventArgs e)
        {
            if (singleTable)
            {
                this.radGridView1.Items.AddNew();
            }
        }

        void RightClickMenu_SetDefault(object sender, EventArgs e)
        {
            SetDefault();
        }

        void RightClickMenu_SaveFilter(object sender, EventArgs e)
        {
            SaveConfig();
        }

        void RightClickMenu_Delete(object sender, EventArgs e)
        {
            if (singleTable)
            {
                DeleteRows();
            }
        }

        void RightClickMenu_Cancel(object sender, EventArgs e)
        {
            Cancel();
        }



        void RightClickMenu_Save(object sender, EventArgs e)
        {

            SaveChanges();
        }

        public static void color_TestClick()
        {
            MessageBox.Show("Red");
        }

        void stack1_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        void stack1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            RightClickContentMenu contextMenu = new RightClickContentMenu();
            //  contextMenu.Show(e.GetPosition(LayoutRoot));
        }


        void dataGridRightClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("right");
        }

        protected void btnButtons_ExportClick(object sender, EventArgs e)
        {

            ExcelExport();

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            var item = this.radGridView1.Items.AddNew();
            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);

                    }
                }

                catch { }
            }
            DataObject newDataObject = item as DataObject;

            object id = ModelID as object;
            List<DummyModel> dummyList = new List<DummyModel>();
            foreach (var m in ModelList)
            {
                DataObject dataObject = m as DataObject;
                long strID = Convert.ToInt64(dataObject.GetFieldValue("ID").ToString());
                
                dummyList.Add(new DummyModel { ModelID = strID, Date = dataObject.GetFieldValue("ReferenceDate"), Time = dataObject.GetFieldValue("ReferenceTime") });
            }

            var model = dummyList.Where(m => m.ModelID == ModelID).FirstOrDefault();
            if (model != null)
            {
                newDataObject.SetFieldValue("ReferenceDate", model.Date);
                newDataObject.SetFieldValue("ReferenceTime", model.Time);
            }
            else
            {
              
                newDataObject.SetFieldValue("ReferenceDate", DateTime.Now.Date.AddDays(5));
                newDataObject.SetFieldValue("ReferenceTime", DateTime.Now.TimeOfDay);
            }

            newDataObject.SetFieldValue("Model_ID", ModelID);
        }

        private void btnSave_Click(object sender, EventArgs e)
        { //perform

            SaveChanges();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            editedDataObjectList = new ObservableCollection<DataObject>();
            newDataObjectList = new ObservableCollection<DataObject>();
            deletedDataObjectList = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteRows();

        }



        private void btnSaveConfig_Click(object sender, EventArgs e)
        {

            SaveConfig();

        }


        //DeleteConfig


        void ChildWin_Closed(object sender, EventArgs e)
        {
            //  LoadConfigSetting();
        }

        private void btnSetDefault_Click(object sender, EventArgs e)
        {
            SetDefault();

        }


        bool isClearFilter = false;
        private void ddlConfig_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            //try
            //{
            //    UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
            //    if (config == null)
            //        return;
            //    else
            //    {
            //        if (config.ID > 0)
            //        {
            //            isClearFilter = false;
            //            GetDefaultSetting(config.Settings_ID);


            //        }
            //        else
            //        {
            //            isClearFilter = true;
            //            ClearFilters();

            //            settings = new RadGridViewSettings(this.radGridView1);

            //            settings.LoadState(primaryXml);
            //            newSqlText = sqlText;

            //            GetTotalRows(newSqlText);
            //            pageNumber = 1;
            //            GetData(newSqlText, pageNumber, pageSize, tableName);
            //            // isClearFilter = false;
            //        }
            //    }
            //}
            //catch
            //{
            //}

        }

        #endregion



        #region Call Ria Services Method

        private void LoadConfigSetting()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting, null);
        }
        private void getTableData()
        {

            App app = (App)Application.Current;
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByDisplayDefIDQuery(DisplayID), CallbackTable, null);
        }

        private void CallbackTable(LoadOperation<Table> loadOp)
        {
            Table table = loadOp.Entities.FirstOrDefault();

            if (table != null)
            {
                tableName = table.TableName;
                tableID = table.ID;
                databaseName = table.DBName;
                getServerConnections(tableID);


            }
        }


        private void getServerConnections(int tableID)
        {

            App app = (App)Application.Current;
            LoadOperation<Server> loadOp = context.Load(context.GetServersByTableIDQuery(tableID), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {
            Server server = loadOp.Entities.FirstOrDefault();

            if (server != null)
            {
                connString = server.ConnectionString;

                LoadOperation<View> loadOperation = context.Load(context.GetViewsByDisplayDefinitionIDQuery(displayDefID), CallbackView, null);

                GetModel();
            }
        }


        private void CallbackView(LoadOperation<View> loadOperation)
        {
            View view = loadOperation.Entities.FirstOrDefault();
            if (view != null)
            {
                viewID = view.ID;
                singleTable = view.SingleTable;
                isReadOnly = view.ReadOnly;

               GetTables();
                View view2 = loadOperation.Entities.Where(v => v.ID != view.ID).FirstOrDefault();
                if (view2 != null)
                {
                    viewID2 = view2.ID;
                    singleTable2 = view2.SingleTable;
                    readOnly2 = view2.ReadOnly;
                }
                View view3 = loadOperation.Entities.Where(v => v.ID != view.ID && v.ID != view2.ID).FirstOrDefault();
                if (view3 != null)
                {
                    viewID3 = view3.ID;
                    singleTable3 = view3.SingleTable;
                    readOnly3 = view3.ReadOnly;
                }


            }
        }



        private void GetTables()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackTables, null);


        }



        private void CallbackTables(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                tableList = loadOp.Entities.ToList();
                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(viewID), CallbackViewFields, null);

            }

        }



        private void CallbackViewFields(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                viewFieldSummary = new ObservableCollection<ViewFieldSummary>();
                foreach (ViewFieldSummary summary in loadOp.Entities)
                {
                    viewFieldSummary.Add(summary);

                }

                fieldList = loadOp.Entities.ToList();

                GetRelationshipsRefresh();

            }

        }


        private void GetGroupViewFilds()
        {
            int groupID = 0;
            if (Globals.CurrentUser.Group_ID != null)
                groupID = Globals.CurrentUser.Group_ID.Value;
            LoadOperation<GroupViewFilter> loadOp = context.Load(context.GetGroupViewFiltersByGroupViewIDQuery(groupID, viewID), CallbackGroupViewFilters, null);

        }



        private void CallbackGroupViewFilters(LoadOperation<GroupViewFilter> results)
        {

            if (results != null)
            {
                GroupViewFilterList = results.Entities.ToList();
            }


            string sql = CreateReadOnlyTable();
            sqlText = sql;
            newSqlText = sql;
            //btnSave.IsEnabled = false;
            //  GetTotalRows(newSqlText);
            // GetData(newSqlText, pageNumber, pageSize, tableName);

            GenerateColumns();
        }




        private void GetRelationshipsRefresh()
        {
            EditorContext cont = new EditorContext();
            LoadOperation<ViewRelationship> loadOp = cont.Load(cont.GetViewRelationshipByViewIDQuery(viewID), CallbackRelationships, null);




        }




        private void CallbackRelationships(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {


                relationshipList = new List<ViewRelationship>();
                relationshipList = loadOp.Entities.ToList();

                GetGroupViewFilds();
            }
        }

        private void GetFields()
        {
            LoadOperation<Field> loadOperation = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == tableID), CallbackFields, null);
        }


        private void CallbackFields(LoadOperation<Field> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                this.radGridView1.AutoGenerateColumns = false;
                foreach (Field field in loadOperation.Entities)
                {
                    GridViewDataColumn column = new GridViewDataColumn();
                    column.DataMemberBinding = new Binding(field.FieldName.Replace(" ", "_"));
                    column.Header = field.FriendlyName;
                    column.UniqueName = field.FieldName;
                    if (field.Type == "datetime")
                    {
                        column.DataFormatString = "{0:dd/MM/yyyy}";
                    }


                    this.radGridView1.Columns.Add(column);
                }

            }

        }


        private void LoadDropDowns()
        {
            if (tempFieldList.Count > 0)
            {


                TempField temp = tempFieldList.Where(t => t.Index == fieldIndex + 1).FirstOrDefault();
                dropDownName = temp.Name;
                LoadOperation<DropDownPairResult> loadOp = context.Load(context.GetDropDownPairResultQuery(temp.ID), CallbackDropDown, null);
                fieldIndex = temp.Index;
            }
        }

        private void CallbackDropDown(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                string sqlString = dropDown.SqlQueryString;
                if (sqlString.Contains("Model"))
                {
                    string modelIDs = "";
                    foreach (var item in MainPage.ModelList)
	                 {
                         modelIDs += item.Model_ID.ToString() + ",";
	                 }
                  modelIDs =  modelIDs.TrimEnd(',');
                    if(modelIDs !="")
                    sqlString += " Where ID in (" + modelIDs + ")";
                }

                GetColumnData(dropDown.ConnectionString, sqlString, 1, 500, "");

            }
        }


        //private void LoadConfigSetting()
        //{
        //    LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting, null);
        //}

        private void CallbackSetting(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 1)
                        settingObjectList.Add(sett);
                }
                settingObjectList.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 1, Settings_ID = 0 });

                ddlConfig.ItemsSource = settingObjectList;


                var query = (from s in settingObjectList
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                    GetDefaultSetting(query.Settings_ID);

              
            }


        }


        private void CallbackDefault(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    // primarySettings.ResetState();

                    //settings = new RadGridViewSettings(this.radGridView1);
                    //string xml = setting.Settings;
                    // settings.LoadState(xml);




                }
                catch
                {
                }
            }
        }
        #endregion



        #region Private Methods

        private void Cancel()
        {
            editedDataObjectList = new ObservableCollection<DataObject>();
            newDataObjectList = new ObservableCollection<DataObject>();
            deletedDataObjectList = new ObservableCollection<DataObject>();
            //cMenu.IsOpen = false;
        }

        private void ExcelExport()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView1.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        private void SaveChanges()
        {
            try
            {
                if (newDataObjectList.Count > 0)
                {
                    InsertDataTable(newDataObjectList, tableName);
                }


                //Update
                if (editedDataObjectList.Count > 0)
                {
                    Update(editedDataObjectList, tableName);
                }

                if (deletedDataObjectList.Count > 0)
                {
                    DeleteDataTable(deletedDataObjectList, tableName);
                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }



        private void DeleteRows()
        {
            try
            {

                if (this.radGridView1.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView1.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView1.Items.Remove(item as DataObject);
                    deletedDataObjectList.Add(item);
                }

                int count = deletedDataObjectList.Count;
            }
            catch
            {
            }
        }


        private void ClearFilters()
        {

            this.radGridView1.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView1.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView1.FilterDescriptors.ResumeNotifications();
            // primarySettings.LoadOriginalState();
        }

        private void SaveConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings = new RadGridViewSettings(this.radGridView1);

                    string settingsXML = settings.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            Message.ErrorMessage(so.Error.Message + " Error adding setting...");
                        }
                        else
                        {

                            Message.InfoMessage("Successfully Saved");



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }

        private void SaveConfigAs()
        {
            try
            {

                settings = new RadGridViewSettings(this.radGridView1);

                string settingsXML = settings.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 1);
                config.Closed += new EventHandler(ChildWin_Closed);
                config.Show();
            }
            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private void DeleteConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            Message.ErrorMessage(so.Error.Message + " ...");
                        }
                        else
                        {

                            Message.InfoMessage("Successfully Deleted");
                            ClearFilters();
                            ddlConfig.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private bool isFirstLoad = true;
        string xmlCache = "";
        string primaryXml = "";
        private void GetDefaultSetting(int settingID)
        {
            if (isFirstLoad)
                //{

                //    primarySettings = new RadGridViewSettings(this.radGridView1);
                //    primarySettings.SaveState();
                //    primaryXml = primarySettings.SaveState();
                //    isFirstLoad = false;
                //}




                if (settingID > 0)
                {
                    ddlConfig.SelectedValue = settingID;
                    // apply setting to the gridview

                    try
                    {

                        //ClearFilters();
                        ////  primarySettings.LoadOriginalState();
                        //primarySettings.ResetState();

                        //settings = new RadGridViewSettings(this.radGridView1);
                        //string xml = setting.Settings;
                        //xmlCache = xml;
                        //settings.LoadState(xml);

                        //createSqlWithNewFilters();




                    }
                    catch
                    {
                    }
                    //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
                }
        }

        private string testName = "";


        private void GenerateColumns()
        {
            if (canGenerateColumns)
            {
                int i = 1;
                tempFieldList = new List<TempField>();
                this.radGridView1.AutoGenerateColumns = false;
                this.radGridView1.ShowInsertRow = false;


                foreach (ViewFieldSummary field in viewFieldSummary)
                {

                    if (columnNames.Contains(field.FieldName))
                    {
                        GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                        columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                        columnComboBox.Header = field.DisplayName;
                        columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        columnComboBox.SelectedValueMemberPath = "Value";
                        columnComboBox.DisplayMemberPath = "Display";
                        columnComboBox.IsFilteringDeferred = true;
                        columnComboBox.IsFilteringDeferred = true;
                        this.radGridView1.Columns.Add(columnComboBox);
                    }
                    else
                    {
                        if (field.UseValueField != null)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";
                            columnComboBox.IsFilteringDeferred = true;
                            this.radGridView1.Columns.Add(columnComboBox);

                            tempFieldList.Add(new TempField { ID = field.FieldID, Name = CharacterHandler.ReplaceSpecialCharacter(field.FieldName), Index = i });
                            i++;




                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            if (field.IsCalculatedField.Value == true)
                            {


                                column.IsReadOnly = true;

                                column.Background = Resources["SkyBlue2"] as SolidColorBrush;// new SolidColorBrush(Colors.Blue);
                            }

                            this.radGridView1.Columns.Add(column);



                        }
                    }
                }
                canGenerateColumns = false;

                tempFieldList = tempFieldList.OrderBy(t => t.Index).ToList();
                indexMax = tempFieldList.Count;
                LoadExtendedProperties();
                LoadDropDowns();
                Attach();


            }

            LoadConfigSetting2();
            GetTables2();
        }


        private void LoadExtendedProperties()
        {
            foreach (DataObject dataObject in propertiesList)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView1.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList;
                }
                catch
                {
                }
            }
        }

        private string CreateReadOnlyTable()
        {
            string sql = string.Empty;
            //table list
            //display field list
            //relationshiplist

            string tables = " From ";
            string fields = " Select ";
            string relationships = " Where ";
            string viewFilter = "";
            string modelClause = " Model_ID =" + ModelID.ToString() + " ";
            foreach (Table table in tableList)
            {
                tables += "[" + table.DBName + "].[dbo].[" + table.TableName + "] as " + "["+table.TableName.ToString() + "] ,";
            }

            foreach (ViewFieldSummary field in fieldList)
            {
                if (field.TableID > 0)
                {
                    fields += field.TableField + " ,";//" as [" + field.DisplayName + "] ,";
                }
                else
                {
                    fields += " (" + field.Expression + " ) as [" + field.DisplayName + "] ,";
                }
            }

            foreach (ViewRelationship rship in relationshipList)
            {
                relationships += " " + rship.Summary + " and";

            }

            foreach (GroupViewFilter item in GroupViewFilterList)
            {
                viewFilter += " " + item.Summary + " and";
            }

            tables = tables.TrimEnd(',');
            fields = fields.TrimEnd(',');
            relationships = relationships.TrimEnd('d');
            relationships = relationships.TrimEnd('n');
            relationships = relationships.TrimEnd('a');
            viewFilter = viewFilter.TrimEnd('d');
            viewFilter = viewFilter.TrimEnd('n');
            viewFilter = viewFilter.TrimEnd('a');

            
            if (relationships == " Where ")
            {
                if (viewFilter != "")
                {
                    relationships += viewFilter + " and " + modelClause;
                }
                else
                {
                    relationships += modelClause;
                }

            }
            else
            {
                if (viewFilter != "")
                {
                    relationships += " and " + viewFilter + " and " + modelClause;
                }
                else 
                {
                    relationships += " and " + modelClause;
            }
            }


            sql = fields + tables + relationships;

            return sql;

        }
        #endregion



        #region Call WFC Services Methods


        private void GetModel()
        {
            string sqlString = "Select * from Model ";
            if (sqlString.Contains("Model"))
            {
                string modelIDs = "";
                foreach (var item in MainPage.ModelList)
                {
                    modelIDs += item.Model_ID.ToString() + ",";
                }
                modelIDs = modelIDs.TrimEnd(',');
                if (modelIDs != "")
                    sqlString += " Where ID in (" + modelIDs + ")";
            }


            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetModelCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sqlString, 1, 1000, "");
        }


        void ws_GetModelCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                //  _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                { }
                // _lookup = list;

                else
                {


                    ModelList = new ObservableCollection<DataObject>();

                    foreach (var item in list)
                    {
                        DataObject dataitem = item as DataObject;
                        ModelList.Add(dataitem);
                        
                    }
                }

            }

        }

        private void GetData(string sql, int pagenumber, int pagesize, object userState)
        {
            this.radGridView1.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView1.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    radGridView1.ItemsSource = list;
                    foreach (var item in desc)
                    {
                        radGridView1.FilterDescriptors.Add(item);
                    }
                    if (canGenerateColumns)
                    {
                        GetExendedPropertiesData(connString, ExtendedPropertySQL.ExtendedPropertiesSQl(databaseName, tableName), 1, 500, null);

                        if (!(singleTable))
                        {
                            btnInactive.Visibility = Visibility.Visible;
                            btnButtons.Visibility = Visibility.Collapsed;

                        }
                        else
                        {
                            btnInactive.Visibility = Visibility.Collapsed;
                            btnButtons.Visibility = Visibility.Visible;
                        }

                    }
                    else
                    {
                        //if (!isClearFilter)
                        //{
                        //  settings = new RadGridViewSettings(this.radGridView1);

                        // settings.LoadState(xmlCache);
                        //}
                    }


                }

            }
            this.radGridView1.IsBusy = false;
        }



        void Update(ObservableCollection<DataObject> objectCollection, string strTablename)
        {

            var ws = WCF.GetService();
            ws.UpdateCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.UpdateCompletedEventArgs>(ws_UpdateCompleted);
            ws.UpdateAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), strTablename);

            ///.Progress.Start();
        }

        void ws_UpdateCompleted(object sender, BMA.MiddlewareApp.DataTableService.UpdateCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                editedDataObjectList = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }

        }



        void InsertDataTable(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), strTablename);

            ///.Progress.Start();
        }

        void ws_InsertCompleted(object sender, BMA.MiddlewareApp.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }


        void GetTotalRows(string parameterSql)
        {
            var ws = WCF.GetService();
            ws.TotalRowsCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs>(ws_ReturnTotalCompleted);
            ws.TotalRowsAsync(connString, parameterSql, databaseName);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted(object sender, BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);

                if (total > pageSize)
                {
                    double totalDouble = (double)(total / (double)pageSize);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager.lblTotal.Text = tol.ToString();
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    pageTotal = tol;
                }
                else
                {
                    dataPager.lblTotal.Text = "1";
                    dataPager.txtNumber.Text = pageNumber.ToString();

                }
            }
            //this.Progress.Stop();
        }


        private void SetDefault()
        {
            UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 1);
            }
        }


        void ws_updateUserConfigDefaultCompleted(object sender, BMA.MiddlewareApp.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    Message.InfoMessage("New default configurating setting have been set");
                }
                else
                {
                    Message.ErrorMessage("Error!, Fail to set new default settings");
                }
            }
            else
            {
                Message.ErrorMessage("An Error has occurred while processing your request!");
            }
        }



        private void GetColumnData(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = list;

                    if (fieldIndex < indexMax)
                    {
                        LoadDropDowns();
                    }


                }

            }

        }





        private void GetExendedPropertiesData(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetExendedPropertiesDataCompleted);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetExendedPropertiesDataCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    columnNames = new List<string>();
                    foreach (DataObject dataObject in list)
                    {

                        propertiesList.Add(dataObject);
                        string column = dataObject.GetFieldValue("ColumnName").ToString();
                        columnNames.Add(column);
                    }




                }


            }
         //   GenerateColumns();

           // LoadConfigSetting();

        }

        void DeleteDataTable(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted(object sender, BMA.MiddlewareApp.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }

        #endregion



        #region GridView Events


        void radGridView1_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {

            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            ClearFilters2();
            settings2 = new RadGridViewSettings(this.radGridView2);
            UserGridConfig setting = ddlConfig2.SelectedItem as UserGridConfig;
            if (setting != null)
            {
                if (setting.Settings_ID > 0)
                {

                    LoadOperation<Setting> loadOperation = context.Load(context.GetSettingsQuery().Where(x => x.ID == setting.Settings_ID), CallbackSettings2, null);

                }
                else
                {
                    PullData2();
                }
            }
            else
            {
                PullData2();
            }
        }
        private void radGridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e)
        {
            DataObject newDataObject = e.NewObject as DataObject;
            newDataObject.SetFieldValue("ReferenceDate", DateTime.Now.Date);
            newDataObject.SetFieldValue("ReferenceTime", DateTime.Now.TimeOfDay);

        }

        private void radGridView_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        
                        
                        newDataObjectList.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList.Contains(dataObject))
                        {
                            if (newDataObjectList.Contains(dataObject))
                            {
                                this.newDataObjectList.Remove(dataObject);
                                this.newDataObjectList.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList.Remove(dataObject);
                            this.editedDataObjectList.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void radGridView_Filtered(object sender, GridViewFilteredEventArgs e)
        {

            PullData();
        }

        void gridView_Deleted(object sender, GridViewDeletedEventArgs e)
        {
            DataObject dataObject = e.Items as DataObject;
            deletedDataObjectList.Add(dataObject);



        }


        private void gridView_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e)
        {
            //RadComboBox countries = e.DetailsElement.FindName("rcbCountries") as RadComboBox;
            //countries.ItemsSource = GetCountries();
            // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }
        #endregion


        #region GridMenu Events & Methods
        public MouseButtonEventHandler gridRightClick { get; set; }

        private void radGridView1_Loaded(object sender, GridViewRowItemEventArgs e)
        {
            e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }

        private void radGridView1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            stack1_MouseRightButtonUp(sender, e);
        }




        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            // tab_MouseRightButtonUp(sender, e);
            tab_MouseRightButtonUp(sender, e);
            cMenu.StaysOpen = true;
        }
        RadContextMenu cMenu;
        RadTabItem selectedTab;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var element = sender as UIElement;


            cMenu = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Changes";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Add";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Selected Rows";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
            exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "ExcelML";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Word";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Csv";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings As";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Set Current Filter Settings as Defualt";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.PlacementTarget = radGridView1;

            Point p = e.GetPosition(this.radGridView1);


            cMenu.Placement = PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu.HorizontalOffset = p.X-10;
            cMenu.VerticalOffset = p.Y+50;


            cMenu.IsOpen = true;
            // cMenu.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            //  RadContextMenu.SetContextMenu(this.radGridView1, cMenu);
            //cMenu.IsOpen = true;
            //  cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X + 350;
            //  cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }
        void RadGridView1_RowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {

              //  ((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
              //  ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);

            }
        }


        void menuItem_Click(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges();
                        break;
                    case "Add New":
                        radGridView1.BeginInsert();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault();
                        break;


                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }

            }
        }

        private void RadContextMenu_ItemClick(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges();
                        break;
                    case "Add New":
                        radGridView1.BeginInsert();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault();
                        break;


                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }
            }
            // cMenu.IsOpen = false;
        }

        private void Export(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView1.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }


        private void RadContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion
        #region MyRegion

        private void Attach()
        {
            if (radGridView1 != null)
            {

                // create menu
                RadContextMenu contextMenu = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu, StyleManager.GetTheme(radGridView1));

                contextMenu.Opened += OnMenuOpened;
                contextMenu.ItemClick += OnMenuItemClick;

                RadContextMenu.SetContextMenu(radGridView1, contextMenu);
            }
        }
        void OnMenuOpened(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);

                // create menu items
                foreach (GridViewColumn column in radGridView1.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.Header;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Changes";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Add";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Edit";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Selected Rows";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }

        void OnMenuItemClick(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView1.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView1.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView1.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView1.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView1.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView1.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView1.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView1.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }
        #endregion

        #endregion



        #region GridTwo



        #region Paging
        void dataPager_First_Click2(object sender, EventArgs e)
        {
            isTextBox2 = false;
            int newPageNumber = 1;
            ReloadPage2(newPageNumber);
        }

        private void Paging2()
        {
            dataPager2.txtNumber.Text = pageNumber.ToString();
            dataPager2.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox2 = true;
        void dataPager_Number_TextChanged2(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox2)
                {
                    int newPageNumber = Convert.ToInt32(dataPager2.txtNumber.Text);
                    ReloadPage2(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox2 = true;
        }
        private void ReloadPage2(int newPageNumber)
        {
            if ((newDataObjectList2.Count > 0) || (editedDataObjectList2.Count > 0) || (deletedDataObjectList2.Count > 0))
            {
                CustomMessage customMessage = new CustomMessage("This page contains some changes, Do you want to save the changes?", CustomMessage.MessageType.Confirm);

                customMessage.OKButton.Click += (obj, args) =>
                {
                    SaveChanges2();

                    if ((newPageNumber > pageTotal2) || (newPageNumber < 1))
                    {
                        dataPager2.txtNumber.Text = pageNumber2.ToString();
                        isTextBox2 = false;
                    }
                    else
                    {
                        pageNumber2 = newPageNumber;
                        dataPager2.txtNumber.Text = pageNumber.ToString();
                        GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
                        //radGridView1.IsBusy = false;
                    }
                };

                customMessage.CancelButton.Click += (obj, args) =>
                {

                    newDataObjectList2 = new ObservableCollection<DataObject>();
                    editedDataObjectList2 = new ObservableCollection<DataObject>();
                    deletedDataObjectList2 = new ObservableCollection<DataObject>();
                    if ((newPageNumber > pageTotal2) || (newPageNumber < 1))
                    {
                        dataPager2.txtNumber.Text = pageNumber2.ToString();
                        isTextBox2 = false;
                    }
                    else
                    {
                        pageNumber2 = newPageNumber;
                        dataPager2.txtNumber.Text = pageNumber2.ToString();
                        GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
                        //radGridView1.IsBusy = false;

                    }
                };


                customMessage.Show();
            }
            else
            {

                if ((newPageNumber > pageTotal2) || (newPageNumber < 1))
                {
                    dataPager2.txtNumber.Text = pageNumber2.ToString();
                    isTextBox2 = false;
                }
                else
                {
                    pageNumber2 = newPageNumber;
                    dataPager2.txtNumber.Text = pageNumber2.ToString();
                    GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
                    //radGridView1.IsBusy = false;
                }
            }




        }

        void dataPager_Next_Click2(object sender, EventArgs e)
        {
            if (pageNumber2 < pageSize2)
            {
                isTextBox2 = false;
                int newPageNumber = pageNumber2 + 1;
                ReloadPage2(newPageNumber);
            }

        }

        void dataPager_Last_Click2(object sender, EventArgs e)
        {
            isTextBox2 = false;
            int newPageNumber = pageTotal2;
            ReloadPage2(newPageNumber);
        }

        void dataPager_Back_Click2(object sender, EventArgs e)
        {
            isTextBox2 = false;
            int newPageNumber = pageNumber2 - 1;
            ReloadPage2(newPageNumber);

        }

        #endregion



        #region Events
        bool isFirst2 = true;
        private void ddlPageSize_SelectionChanged2(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst2)
            {
                PageNumber page = ddlPageSize2.SelectedItem as PageNumber;
                pageSize2 = page.Size;
                isTextBox2 = false;
                int newPageNumber2 = 1;
                GetTotalRows2(newSqlText2);
                ReloadPage2(newPageNumber2);
            }
            isFirst2 = false;
        }

        private void btnCascade_Click2(object sender, RoutedEventArgs e)
        {

            //primarySettings.LoadOriginalState();
            //   primarySettings.ResetState();

            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            ClearFilters2();
            settings2 = new RadGridViewSettings(this.radGridView2);
            UserGridConfig setting = ddlConfig2.SelectedItem as UserGridConfig;
            if (setting != null)
            {
                if (setting.Settings_ID > 0)
                {

                    LoadOperation<Setting> loadOperation = context.Load(context.GetSettingsQuery().Where(x => x.ID == setting.Settings_ID), CallbackSettings2, null);

                }
                else
                {
                    PullData2();
                }
            }
            else
            {
                PullData2();
            }


        }

        void PullData2()
        {
            createSqlWithNewFilters2();
            GetTotalRows2(newSqlText2);
            pageNumber2 = 1;
            GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
        }


        void CallbackSettings2(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache2 = xml;
                settings.LoadState(xml);
            }

            PullData2();
        }





        bool isFirst = true;
        private void ddlPageSize_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst)
            {
                PageNumber page = ddlPageSize.SelectedItem as PageNumber;
                pageSize = page.Size;
                isTextBox = false;
                int newPageNumber = 1;
                GetTotalRows(newSqlText);
                ReloadPage(newPageNumber);
            }
            isFirst = false;
        }

        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {


            Filter();
        }

        private void Filter()
        {
            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;


            // Get tables for grid two

            GetTables2();

        }

        void LayoutRoot_LayoutUpdated2(object sender, EventArgs e)
        {
            //  radGridView1.MaxHeight = this.stack1.RenderSize.Height;
        }




        void RightClickMenu_Export2(object sender, EventArgs e)
        {
            ExcelExport2();
        }

        void RightClickMenu_AddNew2(object sender, EventArgs e)
        {
            if (singleTable2)
            {
                this.radGridView2.Items.AddNew();
            }
        }

        void RightClickMenu_SetDefault2(object sender, EventArgs e)
        {
            SetDefault2();
        }

        void RightClickMenu_SaveFilter2(object sender, EventArgs e)
        {
            SaveConfig2();
        }

        void RightClickMenu_Delete2(object sender, EventArgs e)
        {
            if (singleTable2)
            {
                DeleteRows2();
            }
        }

        void RightClickMenu_Cancel2(object sender, EventArgs e)
        {
            Cancel2();
        }



        void RightClickMenu_Save2(object sender, EventArgs e)
        {

            SaveChanges2();
        }

        public static void color_TestClick2()
        {
            MessageBox.Show("Red");
        }

        void stack1_MouseRightButtonDown2(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        void stack1_MouseRightButtonUp2(object sender, MouseButtonEventArgs e)
        {
            RightClickContentMenu contextMenu = new RightClickContentMenu();
            //  contextMenu.Show(e.GetPosition(LayoutRoot));
        }


        void dataGridRightClick2(object sender, MouseEventArgs e)
        {
            MessageBox.Show("right");
        }

        protected void btnButtons_ExportClick2(object sender, EventArgs e)
        {

            ExcelExport2();

        }

        private void btnAddNew_Click2(object sender, EventArgs e)
        {
            var item = this.radGridView2.Items.AddNew();

            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);

                    }
                }

                catch { }
            }


            DataObject newDataObject = item as DataObject;
            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            if (dataObject != null)
            {
                var value = dataObject.GetFieldValue("ID");

                newDataObject.SetFieldValue("Profile_ID", value);
                var value2 = dataObject.GetFieldValue("ReferenceDate").ToString();
                DateTime datetime = Convert.ToDateTime(value2);
                int rowcount = radGridView2.Items.Count;
                newDataObject.SetFieldValue("ReferenceDateTime", datetime.AddDays(rowcount));

            }
          
        }

        private void btnSave_Click2(object sender, EventArgs e)
        { //perform

            SaveChanges2();
        }


        private void btnCancel_Click2(object sender, EventArgs e)
        {
            editedDataObjectList2 = new ObservableCollection<DataObject>();
            newDataObjectList2 = new ObservableCollection<DataObject>();
            deletedDataObjectList2 = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click2(object sender, EventArgs e)
        {
            DeleteRows2();

        }



        private void btnSaveConfig_Click2(object sender, EventArgs e)
        {

            SaveConfig2();

        }


        //DeleteConfig


        void ChildWin_Closed2(object sender, EventArgs e)
        {
            LoadConfigSetting2();
        }

        private void btnSetDefault_Click2(object sender, EventArgs e)
        {
            SetDefault2();

        }


        bool isClearFilter2 = false;
        private void ddlConfig_SelectionChanged2(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            //try
            //{
            //    UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
            //    if (config == null)
            //        return;
            //    else
            //    {
            //        if (config.ID > 0)
            //        {
            //            isClearFilter2 = false;
            //            GetDefaultSetting2(config.Settings_ID);


            //        }
            //        else
            //        {
            //            isClearFilter2 = true;
            //            ClearFilters2();

            //            settings2 = new RadGridViewSettings(this.radGridView2);

            //            settings2.LoadState(primaryXml2);
            //            newSqlText2 = sqlText2;

            //            GetTotalRows2(newSqlText2);
            //            pageNumber2 = 1;
            //            GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
            //            // isClearFilter = false;
            //        }
            //    }
            //}
            //catch
            //{
            //}

        }

        #endregion



        #region Call Ria Services Method




        private void GetTables2()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID2), CallbackTables2, null);


        }



        private void CallbackTables2(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                tableList2 = loadOp.Entities.ToList();
                if (tableList2.Count > 0)
                {
                    tableName2 = tableList2.FirstOrDefault().TableName;
                }
                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(viewID2), CallbackViewFields2, null);

            }

        }



        private void CallbackViewFields2(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                viewFieldSummary2 = new ObservableCollection<ViewFieldSummary>();
                foreach (ViewFieldSummary summary in loadOp.Entities)
                {
                    viewFieldSummary2.Add(summary);

                }

                fieldList2 = loadOp.Entities.ToList();

                GetRelationshipsRefresh2();

            }

        }


        private void GetGroupViewFilds2()
        {
            int groupID = 0;
            if (Globals.CurrentUser.Group_ID != null)
                groupID = Globals.CurrentUser.Group_ID.Value;
            LoadOperation<GroupViewFilter> loadOp = context.Load(context.GetGroupViewFiltersByGroupViewIDQuery(groupID, viewID2), CallbackGroupViewFilters2, null);

        }



        private void CallbackGroupViewFilters2(LoadOperation<GroupViewFilter> results)
        {

            if (results != null)
            {
                GroupViewFilterList2 = results.Entities.ToList();
            }

            LoadOperation<GridRelationship> loadOperation = context.Load(context.GetGridRelationshipQuery().Where(g => g.DisplayDefinition_ID == DisplayID), CallbackGridRelationships, null);

        }

        private void CallbackGridRelationships(LoadOperation<GridRelationship> loadOperation)
        {
            string sql = CreateReadOnlyTable2(loadOperation.Entities.ToList());


            sqlText2 = sql;
            newSqlText2 = sql;
            GenerateColumns2();
            //btnSave.IsEnabled = false;
            // GetTotalRows2(newSqlText2);
            // GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);



        }

        private void GetRelationshipsRefresh2()
        {
            EditorContext cont = new EditorContext();
            LoadOperation<ViewRelationship> loadOp = cont.Load(cont.GetViewRelationshipByViewIDQuery(viewID2), CallbackRelationships2, null);




        }




        private void CallbackRelationships2(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {


                relationshipList2 = new List<ViewRelationship>();
                relationshipList2 = loadOp.Entities.ToList();

                GetGroupViewFilds2();
            }
        }

        private void GetFields2()
        {
            LoadOperation<Field> loadOperation = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == tableID2), CallbackFields2, null);
        }


        private void CallbackFields2(LoadOperation<Field> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                this.radGridView2.AutoGenerateColumns = false;
                foreach (Field field in loadOperation.Entities)
                {
                    GridViewDataColumn column = new GridViewDataColumn();
                    column.DataMemberBinding = new Binding(field.FieldName.Replace(" ", "_"));
                    column.Header = field.FriendlyName;
                    column.UniqueName = field.FieldName;
                    if (field.Type == "datetime")
                    {
                        column.DataFormatString = "{0:dd/MM/yyyy}";
                    }


                    this.radGridView2.Columns.Add(column);
                }

            }

        }


        private void LoadDropDowns2()
        {
            if (tempFieldList2.Count > 0)
            {


                TempField temp = tempFieldList2.Where(t => t.Index == fieldIndex2 + 1).FirstOrDefault();
                dropDownName2 = temp.Name;
                LoadOperation<DropDownPairResult> loadOp = context.Load(context.GetDropDownPairResultQuery(temp.ID), CallbackDropDown2, null);
                fieldIndex2 = temp.Index;
            }
        }

        private void CallbackDropDown2(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                GetColumnData2(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");

            }
        }


        private void LoadConfigSetting2()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting2, null);
        }

        private void CallbackSetting2(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList2 = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 2)
                        settingObjectList2.Add(sett);
                }
                settingObjectList2.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 2, Settings_ID = 0 });

                ddlConfig2.ItemsSource = settingObjectList2;


                var query = (from s in settingObjectList2
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                    GetDefaultSetting2(query.Settings_ID);

              

            }


        }


        private void CallbackDefault2(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig2.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    primarySettings2.ResetState();

                    settings2 = new RadGridViewSettings(this.radGridView2);
                    string xml = setting.Settings;
                    settings2.LoadState(xml);




                }
                catch
                {
                }
            }
        }
        #endregion



        #region Private Methods

        private void Cancel2()
        {
            editedDataObjectList2 = new ObservableCollection<DataObject>();
            newDataObjectList2 = new ObservableCollection<DataObject>();
            deletedDataObjectList2 = new ObservableCollection<DataObject>();
           // cMenu2.IsOpen = false;
        }

        private void ExcelExport2()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView2.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        private void SaveChanges2()
        {
            try
            {
                if (newDataObjectList2.Count > 0)
                {
                    InsertDataTable2(newDataObjectList2, tableName2);
                }


                //Update
                if (editedDataObjectList2.Count > 0)
                {
                    Update(editedDataObjectList2, tableName2);
                }

                if (deletedDataObjectList2.Count > 0)
                {
                    DeleteDataTable2(deletedDataObjectList2, tableName2);
                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }



        private void DeleteRows2()
        {
            try
            {

                if (this.radGridView2.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView2.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView2.Items.Remove(item as DataObject);
                    deletedDataObjectList2.Add(item);
                }

                int count = deletedDataObjectList2.Count;
            }
            catch
            {
            }
        }


        private void ClearFilters2()
        {

            this.radGridView2.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView2.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView2.FilterDescriptors.ResumeNotifications();
            // primarySettings2.LoadOriginalState();
        }

        private void SaveConfig2()
        {
            try
            {


                UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings2 = new RadGridViewSettings(this.radGridView1);

                    string settingsXML = settings2.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            Message.ErrorMessage(so.Error.Message + " Error adding setting...");
                        }
                        else
                        {

                            Message.InfoMessage("Successfully Saved");



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }

        private void SaveConfigAs2()
        {
            try
            {

                settings2 = new RadGridViewSettings(this.radGridView2);

                string settingsXML = settings2.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 2);
                config.Closed += new EventHandler(ChildWin_Closed2);
                config.Show();
            }
            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private void DeleteConfig2()
        {
            try
            {


                UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            Message.ErrorMessage(so.Error.Message + " ...");
                        }
                        else
                        {

                            Message.InfoMessage("Successfully Deleted");
                            ClearFilters2();
                            ddlConfig2.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private bool isFirstLoad2 = true;
        string xmlCache2 = "";
        string primaryXml2 = "";
        private void GetDefaultSetting2(int settingID)
        {
            //if (isFirstLoad2)
            //{

            //    primarySettings2 = new RadGridViewSettings(this.radGridView2);
            //    primarySettings2.SaveState();
            //    primaryXml2 = primarySettings2.SaveState(); ;
            //    isFirstLoad2 = false;
            //}




            if (settingID > 0)
            {
                ddlConfig2.SelectedValue = settingID;
                // apply setting to the gridview

                try
                {
                    //ClearFilters2();
                    ////  primarySettings.LoadOriginalState();
                    //primarySettings2.ResetState();

                    //settings2 = new RadGridViewSettings(this.radGridView2);
                    //string xml = setting.Settings;
                    //xmlCache2 = xml;
                    //settings2.LoadState(xml);


                    //createSqlWithNewFilters2();

                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }

        private string testName2 = "";


        private void GenerateColumns2()
        {
            if (canGenerateColumns2)
            {
                int i = 1;
                tempFieldList2 = new List<TempField>();
                this.radGridView2.AutoGenerateColumns = false;
                this.radGridView2.ShowInsertRow = false;


                foreach (ViewFieldSummary field in viewFieldSummary2)
                {

                    if (columnNames2.Contains(field.FieldName))
                    {
                        GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                        columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                        columnComboBox.Header = field.DisplayName;
                        columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        columnComboBox.SelectedValueMemberPath = "Value";
                        columnComboBox.DisplayMemberPath = "Display";
                        columnComboBox.IsFilteringDeferred = true;
                        columnComboBox.IsFilteringDeferred = true;
                        this.radGridView2.Columns.Add(columnComboBox);
                    }
                    else
                    {
                        if (field.UseValueField != null)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";
                            columnComboBox.IsFilteringDeferred = true;
                            this.radGridView2.Columns.Add(columnComboBox);

                            tempFieldList2.Add(new TempField { ID = field.FieldID, Name = CharacterHandler.ReplaceSpecialCharacter(field.FieldName), Index = i });
                            i++;




                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;

                            if (field.IsCalculatedField.Value == true)
                            {


                                column.IsReadOnly = true;

                                column.Background = Resources["SkyBlue2"] as SolidColorBrush;// new SolidColorBrush(Colors.Blue);
                            }

                            this.radGridView2.Columns.Add(column);



                        }
                    }
                }
                canGenerateColumns2 = false;

                tempFieldList2 = tempFieldList2.OrderBy(t => t.Index).ToList();
                indexMax2 = tempFieldList2.Count;
                LoadExtendedProperties2();
                LoadDropDowns2();

                Attach2();

            }

           LoadConfigSetting3();
          // GetTables3();
        }


        private void LoadExtendedProperties2()
        {
            foreach (DataObject dataObject in propertiesList2)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList2 = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList2.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView2.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList2;
                }
                catch
                {
                }
            }
        }

        private string CreateReadOnlyTable2(List<GridRelationship> gridRships)
        {
            //   DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            GridRelationshipList = gridRships;
            string sql = string.Empty;
            //table list
            //display field list
            //relationshiplist

            string tables = " From ";
            string fields = " Select ";
            string relationships = " Where ";
            string viewFilter = "";
            //   string gridrship = "";
            foreach (Table table in tableList2)
            {
                tables += "[" + table.DBName + "].[dbo].[" + table.TableName + "] as " + "[" + table.TableName.ToString() + "] ,";
            }

            foreach (ViewFieldSummary field in fieldList2)
            {
                if (field.TableID > 0)
                {
                    fields += field.TableField + " ,";//" as [" + field.DisplayName + "] ,";
                }
                else
                {
                    fields += " (" + field.Expression + " ) as [" + field.DisplayName + "] ,";
                }
            }

            foreach (ViewRelationship rship in relationshipList2)
            {
                relationships += " " + rship.Summary + " and";

            }


            //foreach (GridRelationship rship in gridRships)
            //{
            //    try
            //    {
            //        ViewFieldSummary field = fieldList.Where(f => f.FieldID == rship.ViewField1).FirstOrDefault();
            //        ViewFieldSummary field2 = fieldList2.Where(f => f.FieldID == rship.ViewField2).FirstOrDefault();
            //        string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
            //        gridrship += " " + field2.TableField + " = '" + parameterValue + "' and";
            //    }
            //    catch { }
            //}

            //if (gridrship != string.Empty)
            //{
            //    gridrship = gridrship.TrimEnd('d');
            //    gridrship = gridrship.TrimEnd('n');
            //    gridrship = gridrship.TrimEnd('a');
            //}

            foreach (GroupViewFilter item in GroupViewFilterList2)
            {
                viewFilter += " " + item.Summary + " and";
            }

            tables = tables.TrimEnd(',');
            fields = fields.TrimEnd(',');
            relationships = relationships.TrimEnd('d');
            relationships = relationships.TrimEnd('n');
            relationships = relationships.TrimEnd('a');
            viewFilter = viewFilter.TrimEnd('d');
            viewFilter = viewFilter.TrimEnd('n');
            viewFilter = viewFilter.TrimEnd('a');

            if (relationships == " Where ")
            {

                if (viewFilter != "")
                {
                    relationships += viewFilter;
                }
                else
                {

                    relationships = "";
                }

            }
            else
            {


                if (viewFilter != "")
                {
                    relationships += " and " + viewFilter;
                }
            }





            sql = fields + tables + relationships;

            return sql;

        }
        #endregion



        #region Call WFC Services Methods

        private void GetData2(string sql, int pagenumber, int pagesize, object userState)
        {
            this.radGridView2.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted2);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted2(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView2.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    radGridView2.ItemsSource = list;
                    foreach (var item in desc)
                    {
                        radGridView2.FilterDescriptors.Add(item);
                    }
                    if (canGenerateColumns2)
                    {
                        GetExendedPropertiesData2(connString, ExtendedPropertySQL.ExtendedPropertiesSQl(databaseName, tableName2), 1, 500, null);

                        if (!(singleTable2))
                        {
                            btnInactive2.Visibility = Visibility.Visible;
                            btnButtons2.Visibility = Visibility.Collapsed;

                        }
                        else
                        {
                            btnInactive2.Visibility = Visibility.Collapsed;
                            btnButtons2.Visibility = Visibility.Visible;
                        }

                    }

                    else
                    {
                        //if (!isClearFilter2)
                        //{
                        //    settings2 = new RadGridViewSettings(this.radGridView2);

                        //    settings2.LoadState(xmlCache2);
                        //}
                    }


                }

            }
            this.radGridView2.IsBusy = false;
        }



        void Update2(ObservableCollection<DataObject> objectCollection, string strTablename)
        {

            var ws = WCF.GetService();
            ws.UpdateCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.UpdateCompletedEventArgs>(ws_UpdateCompleted2);
            ws.UpdateAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName2);

            ///.Progress.Start();
        }

        void ws_UpdateCompleted2(object sender, BMA.MiddlewareApp.DataTableService.UpdateCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                editedDataObjectList2 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }

        }



        void InsertDataTable2(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted2);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName2);

            ///.Progress.Start();
        }

        void ws_InsertCompleted2(object sender, BMA.MiddlewareApp.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList2 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }


        void GetTotalRows2(string parameterSql)
        {
            var ws = WCF.GetService();
            ws.TotalRowsCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs>(ws_ReturnTotalCompleted2);
            ws.TotalRowsAsync(connString, parameterSql, databaseName);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted2(object sender, BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);

                if (total > pageSize2)
                {
                    double totalDouble = (double)(total / (double)pageSize2);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager2.lblTotal.Text = tol.ToString();
                    dataPager2.txtNumber.Text = pageNumber2.ToString();
                    pageTotal2 = tol;
                }
                else
                {
                    dataPager2.lblTotal.Text = "1";
                    dataPager2.txtNumber.Text = pageNumber2.ToString();

                }
            }
            //this.Progress.Stop();
        }


        private void SetDefault2()
        {
            UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted2);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 2);
            }
        }


        void ws_updateUserConfigDefaultCompleted2(object sender, BMA.MiddlewareApp.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    Message.InfoMessage("New default configurating setting have been set");
                }
                else
                {
                    Message.ErrorMessage("Error!, Fail to set new default settings");
                }
            }
            else
            {
                Message.ErrorMessage("An Error has occurred while processing your request!");
            }
        }



        private void GetColumnData2(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted2);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted2(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = list;

                    if (fieldIndex < indexMax)
                    {
                        LoadDropDowns2();
                    }


                }

            }

        }





        private void GetExendedPropertiesData2(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetExendedPropertiesDataCompleted2);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetExendedPropertiesDataCompleted2(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    columnNames2 = new List<string>();
                    foreach (DataObject dataObject in list)
                    {

                        propertiesList2.Add(dataObject);
                        string column = dataObject.GetFieldValue("ColumnName").ToString();
                        columnNames2.Add(column);
                    }




                }


            }
          //  GenerateColumns2();

           // LoadConfigSetting3();

        }

        void DeleteDataTable2(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted2);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName2, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted2(object sender, BMA.MiddlewareApp.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList2 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }

        #endregion



        #region GridView Events
        private void radGridView_AddingNewDataItem2(object sender, GridViewAddingNewEventArgs e)
        {

        }

        private void radGridView_RowEditEnded2(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList2.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList2.Contains(dataObject))
                        {
                            if (newDataObjectList2.Contains(dataObject))
                            {
                                this.newDataObjectList2.Remove(dataObject);
                                this.newDataObjectList2.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList2.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList2.Remove(dataObject);
                            this.editedDataObjectList2.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void radGridView_Filtered2(object sender, GridViewFilteredEventArgs e)
        {


            PullData2();
        }


        private void createSqlWithNewFilters2()
        {
            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;

            string newSql = "";
            string gridrship = "";

            //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)

            foreach (GridRelationship rship in GridRelationshipList)
            {
                try
                {
                    ViewFieldSummary field = fieldList.Where(f => f.FieldID == rship.ViewField1).FirstOrDefault();
                    ViewFieldSummary field2 = fieldList2.Where(f => f.FieldID == rship.ViewField2).FirstOrDefault();
                    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                    gridrship += " " + field2.TableField + " = '" + parameterValue + "' and";
                }
                catch { }
            }
            if (gridrship != string.Empty)
            {
                gridrship = gridrship.TrimEnd('d');
                gridrship = gridrship.TrimEnd('n');
                gridrship = gridrship.TrimEnd('a');
            }



            if (sqlText2.Contains("Where"))
            {
                newSqlText2 = sqlText2 + " and " + gridrship;
            }
            else
            {
                newSqlText2 = sqlText2 + " Where " + gridrship;
            }




            if (radGridView2.FilterDescriptors.Count > 0)
            {


                newSql += OperatorHandler.BulidQuery(radGridView2);



            }

            if (newSql.Length > 0)
            {
                newSql = newSql.Remove(newSql.Length - 3, 3);

                if (newSqlText2.Contains("Where"))
                {
                    newSqlText2 = newSqlText2 + " and " + newSql;
                }
                else
                {
                    newSqlText2 = newSqlText2 + " Where " + newSql;
                }
            }
            //  else
            // {
            // newSqlText2 = sqlText2;
            // }
            // GetTotalRows2(newSqlText2);
            pageNumber2 = 1;
            // GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
        }

        private void createSqlWithNewFilters()
        {
            string newSql = "";


            //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)


            if (radGridView1.FilterDescriptors.Count > 0)
            {


                newSql += OperatorHandler.BulidQuery(radGridView1);



            }

            if (newSql.Length > 0)
            {
                newSql = newSql.Remove(newSql.Length - 3, 3);

                if (sqlText.Contains("Where"))
                {
                    newSqlText = sqlText + " and " + newSql;
                }
                else
                {
                    newSqlText = sqlText + " Where " + newSql;
                }
            }
            else
            {
                newSqlText = sqlText;

            }

            //  GetTotalRows(newSqlText);
            //  pageNumber = 1;
            // GetData(newSqlText, pageNumber, pageSize, tableName);
        }

        void gridView_Deleted2(object sender, GridViewDeletedEventArgs e)
        {
            DataObject dataObject = e.Items as DataObject;
            deletedDataObjectList2.Add(dataObject);



        }


        private void gridView_LoadingRowDetails2(object sender, GridViewRowDetailsEventArgs e)
        {
            //RadComboBox countries = e.DetailsElement.FindName("rcbCountries") as RadComboBox;
            //countries.ItemsSource = GetCountries();
            // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }
        #endregion


        #region GridMenu Events & Methods
        public MouseButtonEventHandler gridRightClick2 { get; set; }








        void tab_MouseRightButtonDown2(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            // tab_MouseRightButtonUp(sender, e);
            tab_MouseRightButtonUp2(sender, e);
            cMenu2.StaysOpen = true;
        }
        RadContextMenu cMenu2;
        RadTabItem selectedTab2;
        void tab_MouseRightButtonUp2(object sender, MouseButtonEventArgs e)
        {
            var element = sender as UIElement;


            cMenu2 = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Changes";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Add";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Selected Rows";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
            exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "ExcelML";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Word";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Csv";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings As";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Set Current Filter Settings as Defualt";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);
            cMenu2.PlacementTarget = Docking;

            Point p = e.GetPosition(this.Docking);


            cMenu2.Placement = PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu2.HorizontalOffset = p.X-10;
            cMenu2.VerticalOffset = p.Y+50;


            cMenu2.IsOpen = true;



        }
        void RadGridView1_RowLoaded2(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {

              //  ((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp2);
               // ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown2);

            }
        }


        void menuItem_Click2(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges2();
                        break;
                    case "Add New":
                        radGridView2.BeginInsert();
                        break;
                    case "Edit":
                        radGridView2.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows2();
                        break;

                    case "Save Filter Settings":
                        SaveConfig2();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs2();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig2();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault2();
                        break;


                    case "Excel":
                        Export2(header);
                        break;

                    case "Word":
                        Export2(header);
                        break;

                    case "ExcelML":
                        Export2(header);
                        break;
                    case "Csv":
                        Export2(header);
                        break;
                    case "Cancel":
                        Cancel2();
                        break;
                    default:
                        break;
                }

            }
        }

        private void RadContextMenu_ItemClick2(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges2();
                        break;
                    case "Add New":
                        radGridView2.BeginInsert();
                        break;
                    case "Edit":
                        radGridView2.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows2();
                        break;

                    case "Save Filter Settings":
                        SaveConfig2();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs2();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig2();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault2();
                        break;


                    case "Excel":
                        Export2(header);
                        break;

                    case "Word":
                        Export2(header);
                        break;

                    case "ExcelML":
                        Export2(header);
                        break;
                    case "Csv":
                        Export2(header);
                        break;
                    case "Cancel":
                        Cancel2();
                        break;
                    default:
                        break;
                }
            }
            // cMenu.IsOpen = false;
        }

        private void Export2(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView2.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }


        private void RadContextMenu_Opened2(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion


        #region MyRegion

        private void Attach2()
        {
            if (radGridView2 != null)
            {

                // create menu
                RadContextMenu contextMenu2 = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu2, StyleManager.GetTheme(radGridView2));

                contextMenu2.Opened += OnMenuOpened2;
                contextMenu2.ItemClick += OnMenuItemClick2;

                RadContextMenu.SetContextMenu(radGridView2, contextMenu2);
            }
        }
        void OnMenuOpened2(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);

                // create menu items
                foreach (GridViewColumn column in radGridView2.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.Header;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Changes";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Add";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Edit";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Selected Rows";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }

        void OnMenuItemClick2(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView2.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView2.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView2.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView2.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView2.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView2.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView2.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView2.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView2.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView2.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView2.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }
        #endregion


        #endregion



        #region Grid Three



        #region Paging
        void dataPager_First_Click3(object sender, EventArgs e)
        {
            isTextBox3 = false;
            int newPageNumber = 1;
            ReloadPage3(newPageNumber);
        }

        private void Paging3()
        {
            dataPager3.txtNumber.Text = pageNumber.ToString();
            dataPager3.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox3 = true;
        void dataPager_Number_TextChanged3(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox3)
                {
                    int newPageNumber = Convert.ToInt32(dataPager3.txtNumber.Text);
                    ReloadPage3(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox3 = true;
        }
        private void ReloadPage3(int newPageNumber)
        {
            if ((newDataObjectList3.Count > 0) || (editedDataObjectList3.Count > 0) || (deletedDataObjectList3.Count > 0))
            {
                CustomMessage customMessage = new CustomMessage("This page contains some changes, Do you want to save the changes?", CustomMessage.MessageType.Confirm);

                customMessage.OKButton.Click += (obj, args) =>
                {
                    SaveChanges3();

                    if ((newPageNumber > pageTotal3) || (newPageNumber < 1))
                    {
                        dataPager3.txtNumber.Text = pageNumber3.ToString();
                        isTextBox3 = false;
                    }
                    else
                    {
                        pageNumber3 = newPageNumber;
                        dataPager3.txtNumber.Text = pageNumber.ToString();
                       // GetData3(newSqlText3, pageNumber3, pageSize3, tableName3);
                        GetData3(newSqlText3, parameters, pageNumber3, pageSize3);
                    }
                };

                customMessage.CancelButton.Click += (obj, args) =>
                {

                    newDataObjectList3 = new ObservableCollection<DataObject>();
                    editedDataObjectList3 = new ObservableCollection<DataObject>();
                    deletedDataObjectList3 = new ObservableCollection<DataObject>();
                    if ((newPageNumber > pageTotal3) || (newPageNumber < 1))
                    {
                        dataPager3.txtNumber.Text = pageNumber3.ToString();
                        isTextBox3 = false;
                    }
                    else
                    {
                        pageNumber3 = newPageNumber;
                        dataPager3.txtNumber.Text = pageNumber3.ToString();
                        GetData3(newSqlText3, parameters, pageNumber3, pageSize3);
                        //radGridView1.IsBusy = false;

                    }
                };


                customMessage.Show();
            }
            else
            {

                if ((newPageNumber > pageTotal3) || (newPageNumber < 1))
                {
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    isTextBox3 = false;
                }
                else
                {
                    pageNumber3 = newPageNumber;
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    GetData3(newSqlText3, parameters, pageNumber3, pageSize3);
                    //radGridView1.IsBusy = false;
                }
            }




        }

        void dataPager_Next_Click3(object sender, EventArgs e)
        {
            if (pageNumber3 < pageSize3)
            {
                isTextBox3 = false;
                int newPageNumber = pageNumber3 + 1;
                ReloadPage3(newPageNumber);
            }

        }

        void dataPager_Last_Click3(object sender, EventArgs e)
        {
            isTextBox3 = false;
            int newPageNumber = pageTotal3;
            ReloadPage3(newPageNumber);
        }

        void dataPager_Back_Click3(object sender, EventArgs e)
        {
            isTextBox3 = false;
            int newPageNumber = pageNumber3 - 1;
            ReloadPage3(newPageNumber);

        }

        #endregion



        #region Events
        bool isFirst3 = true;
        private void ddlPageSize_SelectionChanged3(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst3)
            {
                PageNumber page = ddlPageSize3.SelectedItem as PageNumber;
                pageSize3 = page.Size;
                isTextBox3 = false;
                int newPageNumber3 = 1;
                PullData3();
               
            }
            isFirst3 = false;
        }

        ObservableCollection<string> parameters = new ObservableCollection<string>();
        private void btnCascade_Click3(object sender, RoutedEventArgs e)
        {

            //primarySettings.LoadOriginalState();
            //   primarySettings.ResetState();

            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            ClearFilters3();
            settings3 = new RadGridViewSettings(this.radGridView3);
            UserGridConfig setting = ddlConfig3.SelectedItem as UserGridConfig;
            if (setting != null)
            {
                if (setting.Settings_ID > 0)
                {

                    LoadOperation<Setting> loadOperation = context.Load(context.GetSettingsQuery().Where(x => x.ID == setting.Settings_ID), CallbackSettings3, null);

                }
                else
                {
                    PullData3();
                }
            }
            else
            {
                PullData3();
            }


        }

        void PullData3()
        {

            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;

            parameters = new ObservableCollection<string>();
                 //foreach (GridRelationship rship in gridRships)
            //{
            //    try
            //    {
            //        ViewFieldSummary field = fieldList.Where(f => f.FieldID == rship.ViewField1).FirstOrDefault();
            //        ViewFieldSummary field2 = fieldList2.Where(f => f.FieldID == rship.ViewField2).FirstOrDefault();
                  string parameterValue = dataObject.GetFieldValue("ID").ToString();
            //        gridrship += " " + field2.TableField + " = '" + parameterValue + "' and";
            //    }
            //    catch { }
            //}
                  string parameter = "Profile_ID;" + parameterValue;
                  parameters.Add(parameter);
                
          //  createSqlWithNewFilters3();
            //GetTotalRows3(newSqlText3);
            pageNumber3 = 1;

            newSqlText3 = "ListProfile";

            GetTotalProcRows(newSqlText3, parameters);
            GetData3(newSqlText3, parameters, pageNumber3, pageSize3);
        }


        void CallbackSettings3(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache3 = xml;
                settings.LoadState(xml);
            }

            PullData3();
        }





        //bool isFirst3 = true;
        //private void ddlPageSize_SelectionChanged3(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        //{
        //    if (!isFirst3)
        //    {
        //        PageNumber page = ddlPageSize.SelectedItem as PageNumber;
        //        pageSize = page.Size;
        //        isTextBox = false;
        //        int newPageNumber = 1;
        //        GetTotalRows(newSqlText);
        //        ReloadPage(newPageNumber);
        //    }
        //    isFirst3 = false;
        //}

        private void btnFilter_Click3(object sender, RoutedEventArgs e)
        {


            Filter3();
        }

        private void Filter3()
        {
            DataObject dataObject = this.radGridView2.SelectedItem as DataObject;
            if (dataObject == null)
                return;


            // Get tables for grid two

            GetTables3();

        }

        void LayoutRoot_LayoutUpdated3(object sender, EventArgs e)
        {
            //  radGridView1.MaxHeight = this.stack1.RenderSize.Height;
        }




        void RightClickMenu_Export3(object sender, EventArgs e)
        {
            ExcelExport3();
        }

        void RightClickMenu_AddNew3(object sender, EventArgs e)
        {
            if (singleTable3)
            {
                this.radGridView3.Items.AddNew();
            }
        }

        void RightClickMenu_SetDefault3(object sender, EventArgs e)
        {
            SetDefault3();
        }

        void RightClickMenu_SaveFilter3(object sender, EventArgs e)
        {
            SaveConfig3();
        }

        void RightClickMenu_Delete3(object sender, EventArgs e)
        {
            if (singleTable3)
            {
                DeleteRows3();
            }
        }

        void RightClickMenu_Cancel3(object sender, EventArgs e)
        {
            Cancel3();
        }



        void RightClickMenu_Save3(object sender, EventArgs e)
        {

            SaveChanges3();
        }

        public static void color_TestClick3()
        {
            MessageBox.Show("Red");
        }

        void stack1_MouseRightButtonDown3(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        void stack1_MouseRightButtonUp3(object sender, MouseButtonEventArgs e)
        {
            RightClickContentMenu contextMenu = new RightClickContentMenu();
            //  contextMenu.Show(e.GetPosition(LayoutRoot));
        }


        void dataGridRightClick3(object sender, MouseEventArgs e)
        {
            MessageBox.Show("right");
        }

        protected void btnButtons_ExportClick3(object sender, EventArgs e)
        {

            ExcelExport3();

        }

        private void btnAddNew_Click3(object sender, EventArgs e)
        {
            var item = this.radGridView3.Items.AddNew();

            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);

                    }
                }

                catch { }
            }
        }

        private void btnSave_Click3(object sender, EventArgs e)
        { //perform

            SaveChanges3();
        }


        private void btnCancel_Click3(object sender, EventArgs e)
        {
            editedDataObjectList3 = new ObservableCollection<DataObject>();
            newDataObjectList3 = new ObservableCollection<DataObject>();
            deletedDataObjectList3 = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click3(object sender, EventArgs e)
        {
            DeleteRows3();

        }



        private void btnSaveConfig_Click3(object sender, EventArgs e)
        {

            SaveConfig3();

        }


        //DeleteConfig


        void ChildWin_Closed3(object sender, EventArgs e)
        {
            LoadConfigSetting3();
        }

        private void btnSetDefault_Click3(object sender, EventArgs e)
        {
            SetDefault3();

        }


        bool isClearFilter3 = false;
        private void ddlConfig_SelectionChanged3(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            //try
            //{
            //    UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
            //    if (config == null)
            //        return;
            //    else
            //    {
            //        if (config.ID > 0)
            //        {
            //            isClearFilter3 = false;
            //            GetDefaultSetting3(config.Settings_ID);


            //        }
            //        else
            //        {
            //            isClearFilter3 = true;
            //            ClearFilters3();

            //            settings3 = new RadGridViewSettings(this.radGridView3);

            //            settings3.LoadState(primaryXml3);
            //            newSqlText3 = sqlText3;

            //            GetTotalRows3(newSqlText3);
            //            pageNumber3 = 1;
            //            GetData3(newSqlText3, pageNumber3, pageSize3, tableName3);
            //            // isClearFilter = false;
            //        }
            //    }
            //}
            //catch
            //{
            //}

        }

        #endregion



        #region Call Ria Services Method




        private void GetTables3()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID3), CallbackTables3, null);


        }



        private void CallbackTables3(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                tableList3 = loadOp.Entities.ToList();
                if (tableList3.Count > 0)
                {
                    tableName3 = tableList3.FirstOrDefault().TableName;
                }
                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(viewID3), CallbackViewFields3, null);

            }

        }



        private void CallbackViewFields3(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                viewFieldSummary3 = new ObservableCollection<ViewFieldSummary>();
                foreach (ViewFieldSummary summary in loadOp.Entities)
                {
                    viewFieldSummary3.Add(summary);

                }

                fieldList3 = loadOp.Entities.ToList();

                GetRelationshipsRefresh3();

            }

        }


        private void GetGroupViewFilds3()
        {
            int groupID = 0;
            if (Globals.CurrentUser.Group_ID != null)
                groupID = Globals.CurrentUser.Group_ID.Value;
            LoadOperation<GroupViewFilter> loadOp = context.Load(context.GetGroupViewFiltersByGroupViewIDQuery(groupID, viewID2), CallbackGroupViewFilters3, null);

        }



        private void CallbackGroupViewFilters3(LoadOperation<GroupViewFilter> results)
        {

            if (results != null)
            {
                GroupViewFilterList3 = results.Entities.ToList();
            }

            LoadOperation<GridRelationship> loadOperation = context.Load(context.GetGridRelationshipQuery().Where(g => g.DisplayDefinition_ID == DisplayID), CallbackGridRelationships3, null);

        }

        private void CallbackGridRelationships3(LoadOperation<GridRelationship> loadOperation)
        {
            string sql = CreateReadOnlyTable3(loadOperation.Entities.ToList());


            sqlText3 = sql;
            newSqlText3 = sql;
            GenerateColumns3();
            //btnSave.IsEnabled = false;
            // GetTotalRows2(newSqlText2);
            // GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);



        }

        private void GetRelationshipsRefresh3()
        {
            EditorContext cont = new EditorContext();
            LoadOperation<ViewRelationship> loadOp = cont.Load(cont.GetViewRelationshipByViewIDQuery(viewID3), CallbackRelationships3, null);




        }




        private void CallbackRelationships3(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {


                relationshipList3 = new List<ViewRelationship>();
                relationshipList3 = loadOp.Entities.ToList();

                GetGroupViewFilds3();
            }
        }

        private void GetFields3()
        {
            LoadOperation<Field> loadOperation = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == tableID3), CallbackFields3, null);
        }


        private void CallbackFields3(LoadOperation<Field> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                //this.radGridView3.AutoGenerateColumns = false;
                foreach (Field field in loadOperation.Entities)
                {
                    GridViewDataColumn column = new GridViewDataColumn();
                    column.DataMemberBinding = new Binding(field.FieldName.Replace(" ", "_"));
                    column.Header = field.FriendlyName;
                    column.UniqueName = field.FieldName;
                    if (field.Type == "datetime")
                    {
                        column.DataFormatString = "{0:dd/MM/yyyy}";
                    }


                    this.radGridView3.Columns.Add(column);
                }

            }

        }


        private void LoadDropDowns3()
        {
            if (tempFieldList3.Count > 0)
            {


                TempField temp = tempFieldList3.Where(t => t.Index == fieldIndex3 + 1).FirstOrDefault();
                dropDownName3 = temp.Name;
                LoadOperation<DropDownPairResult> loadOp = context.Load(context.GetDropDownPairResultQuery(temp.ID), CallbackDropDown3, null);
                fieldIndex3 = temp.Index;
            }
        }

        private void CallbackDropDown3(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                GetColumnData3(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");

            }
        }


        private void LoadConfigSetting3()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting3, null);
        }

        private void CallbackSetting3(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList3 = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 3)
                        settingObjectList3.Add(sett);
                }
                settingObjectList3.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 3, Settings_ID = 0 });

                ddlConfig3.ItemsSource = settingObjectList3;


                var query = (from s in settingObjectList3
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                    GetDefaultSetting3(query.Settings_ID);

            //    GetTables3();

            }


        }


        private void CallbackDefault3(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig3.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    primarySettings3.ResetState();

                    settings3 = new RadGridViewSettings(this.radGridView3);
                    string xml = setting.Settings;
                    settings3.LoadState(xml);




                }
                catch
                {
                }
            }
        }
        #endregion



        #region Private Methods

        private void Cancel3()
        {
            editedDataObjectList3 = new ObservableCollection<DataObject>();
            newDataObjectList3 = new ObservableCollection<DataObject>();
            deletedDataObjectList3= new ObservableCollection<DataObject>();
           // cMenu3.IsOpen = false;
        }

        private void ExcelExport3()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView3.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        private void SaveChanges3()
        {
            try
            {
                if (newDataObjectList3.Count > 0)
                {
                    InsertDataTable3(newDataObjectList3, tableName3);
                }


                //Update
                if (editedDataObjectList3.Count > 0)
                {
                    Update(editedDataObjectList3, tableName3);
                }

                if (deletedDataObjectList3.Count > 0)
                {
                    DeleteDataTable3(deletedDataObjectList3, tableName3);
                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }



        private void DeleteRows3()
        {
            try
            {

                if (this.radGridView3.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView3.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView3.Items.Remove(item as DataObject);
                    deletedDataObjectList3.Add(item);
                }

                int count = deletedDataObjectList3.Count;
            }
            catch
            {
            }
        }


        private void ClearFilters3()
        {

            this.radGridView3.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView3.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView3.FilterDescriptors.ResumeNotifications();
            // primarySettings2.LoadOriginalState();
        }

        private void SaveConfig3()
        {
            try
            {


                UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings3 = new RadGridViewSettings(this.radGridView1);

                    string settingsXML = settings3.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            Message.ErrorMessage(so.Error.Message + " Error adding setting...");
                        }
                        else
                        {

                            Message.InfoMessage("Successfully Saved");



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }

        private void SaveConfigAs3()
        {
            try
            {

                settings3 = new RadGridViewSettings(this.radGridView3);

                string settingsXML = settings3.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 3);
                config.Closed += new EventHandler(ChildWin_Closed3);
                config.Show();
            }
            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private void DeleteConfig3()
        {
            try
            {


                UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            Message.ErrorMessage(so.Error.Message + " ...");
                        }
                        else
                        {

                            Message.InfoMessage("Successfully Deleted");
                            ClearFilters3();
                            ddlConfig3.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                Message.ErrorMessage(ex.Message);
            }
        }


        private bool isFirstLoad3 = true;
        string xmlCache3 = "";
        string primaryXml3 = "";
        private void GetDefaultSetting3(int settingID)
        {
            //if (isFirstLoad2)
            //{

            //    primarySettings2 = new RadGridViewSettings(this.radGridView2);
            //    primarySettings2.SaveState();
            //    primaryXml2 = primarySettings2.SaveState(); ;
            //    isFirstLoad2 = false;
            //}




            if (settingID > 0)
            {
                ddlConfig3.SelectedValue = settingID;
                // apply setting to the gridview

                try
                {
                    //ClearFilters2();
                    ////  primarySettings.LoadOriginalState();
                    //primarySettings2.ResetState();

                    //settings2 = new RadGridViewSettings(this.radGridView2);
                    //string xml = setting.Settings;
                    //xmlCache2 = xml;
                    //settings2.LoadState(xml);


                    //createSqlWithNewFilters2();

                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }

        private string testName3 = "";


        private void GenerateColumns3()
        {
            if (canGenerateColumns3)
            {
                int i = 1;
                tempFieldList3 = new List<TempField>();
               // this.radGridView3.AutoGenerateColumns = false;
                this.radGridView3.ShowInsertRow = false;


                foreach (ViewFieldSummary field in viewFieldSummary3)
                {

                    if (columnNames3.Contains(field.FieldName))
                    {
                        GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                        columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                        columnComboBox.Header = field.DisplayName;
                        columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        columnComboBox.SelectedValueMemberPath = "Value";
                        columnComboBox.DisplayMemberPath = "Display";
                        columnComboBox.IsFilteringDeferred = true;
                        columnComboBox.IsFilteringDeferred = true;
                        this.radGridView3.Columns.Add(columnComboBox);
                    }
                    else
                    {
                        if (field.UseValueField != null)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";
                            columnComboBox.IsFilteringDeferred = true;
                            this.radGridView3.Columns.Add(columnComboBox);

                            tempFieldList3.Add(new TempField { ID = field.FieldID, Name = CharacterHandler.ReplaceSpecialCharacter(field.FieldName), Index = i });
                            i++;




                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;

                            if (field.IsCalculatedField.Value == true)
                            {


                                column.IsReadOnly = true;

                                column.Background = Resources["SkyBlue2"] as SolidColorBrush;// new SolidColorBrush(Colors.Blue);
                            }

                            this.radGridView3.Columns.Add(column);



                        }
                    }
                }
                canGenerateColumns3 = false;

                tempFieldList3 = tempFieldList3.OrderBy(t => t.Index).ToList();
                indexMax3 = tempFieldList3.Count;
                LoadExtendedProperties3();
                LoadDropDowns3();

                Attach3();

            }
        }


        private void LoadExtendedProperties3()
        {
            foreach (DataObject dataObject in propertiesList3)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList3 = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList3.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView3.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList3;
                }
                catch
                {
                }
            }
        }

        private string CreateReadOnlyTable3(List<GridRelationship> gridRships)
        {
            //   DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            GridRelationshipList3 = gridRships;
            string sql = string.Empty;
            //table list
            //display field list
            //relationshiplist

            string tables = " From ";
            string fields = " Select ";
            string relationships = " Where ";
            string viewFilter = "";
            //   string gridrship = "";
            foreach (Table table in tableList3)
            {
                tables += "[" + table.DBName + "].[dbo].[" + table.TableName + "] as " + "[" + table.TableName.ToString() + "] ,";
            }

            foreach (ViewFieldSummary field in fieldList3)
            {
                if (field.TableID > 0)
                {
                    fields += field.TableField + " ,";//" as [" + field.DisplayName + "] ,";
                }
                else
                {
                    fields += " (" + field.Expression + " ) as [" + field.DisplayName + "] ,";
                }
            }

            foreach (ViewRelationship rship in relationshipList3)
            {
                relationships += " " + rship.Summary + " and";

            }


            //foreach (GridRelationship rship in gridRships)
            //{
            //    try
            //    {
            //        ViewFieldSummary field = fieldList.Where(f => f.FieldID == rship.ViewField1).FirstOrDefault();
            //        ViewFieldSummary field2 = fieldList2.Where(f => f.FieldID == rship.ViewField2).FirstOrDefault();
            //        string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
            //        gridrship += " " + field2.TableField + " = '" + parameterValue + "' and";
            //    }
            //    catch { }
            //}

            //if (gridrship != string.Empty)
            //{
            //    gridrship = gridrship.TrimEnd('d');
            //    gridrship = gridrship.TrimEnd('n');
            //    gridrship = gridrship.TrimEnd('a');
            //}

            foreach (GroupViewFilter item in GroupViewFilterList3)
            {
                viewFilter += " " + item.Summary + " and";
            }

            tables = tables.TrimEnd(',');
            fields = fields.TrimEnd(',');
            relationships = relationships.TrimEnd('d');
            relationships = relationships.TrimEnd('n');
            relationships = relationships.TrimEnd('a');
            viewFilter = viewFilter.TrimEnd('d');
            viewFilter = viewFilter.TrimEnd('n');
            viewFilter = viewFilter.TrimEnd('a');

            if (relationships == " Where ")
            {

                if (viewFilter != "")
                {
                    relationships += viewFilter;
                }
                else
                {

                    relationships = "";
                }

            }
            else
            {


                if (viewFilter != "")
                {
                    relationships += " and " + viewFilter;
                }
            }





            sql = fields + tables + relationships;

            return sql;

        }
        #endregion



        #region Call WFC Services Methods

        //private void GetData3(string sql, int pagenumber, int pagesize, object userState)
        //{
        //    this.radGridView3.IsBusy = true;
        //    var ws = WCF.GetService();
        //    ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted3);
        //    ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        //}


        void GetData3(string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            this.radGridView3.IsBusy = true;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteDropdownStoreProcedureCompleted);
            ws.ExecuteStoreProcAsync(connString, databaseName, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }


        void ws_ExecuteDropdownStoreProcedureCompleted(object sender, BMA.MiddlewareApp.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {

                    radGridView3.ItemsSource = list;
                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView3.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                  
                   // radGridView3.AutoGenerateColumns = false;
                    foreach (var item in desc)
                    {
                        radGridView3.FilterDescriptors.Add(item);
                    }
                    btnInactive3.Visibility = Visibility.Visible;
                    btnButtons3.Visibility = Visibility.Collapsed;


                }

            }
            this.radGridView3.IsBusy = false;
        }



        void ws_GetDataSetDataCompleted3(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView3.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    radGridView3.ItemsSource = list;
                    foreach (var item in desc)
                    {
                        radGridView3.FilterDescriptors.Add(item);
                    }
                    if (canGenerateColumns3)
                    {
                        GetExendedPropertiesData3(connString, ExtendedPropertySQL.ExtendedPropertiesSQl(databaseName, tableName3), 1, 500, null);

                        if (!(singleTable3))
                        {
                            btnInactive3.Visibility = Visibility.Visible;
                            btnButtons3.Visibility = Visibility.Collapsed;

                        }
                        else
                        {
                            btnInactive3.Visibility = Visibility.Collapsed;
                            btnButtons3.Visibility = Visibility.Visible;
                        }

                    }

                    else
                    {
                        //if (!isClearFilter2)
                        //{
                        //    settings2 = new RadGridViewSettings(this.radGridView2);

                        //    settings2.LoadState(xmlCache2);
                        //}
                    }


                }

            }
            this.radGridView3.IsBusy = false;
        }



        void Update3(ObservableCollection<DataObject> objectCollection, string strTablename)
        {

            var ws = WCF.GetService();
            ws.UpdateCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.UpdateCompletedEventArgs>(ws_UpdateCompleted3);
            ws.UpdateAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(editedDataObjectList3 as IEnumerable, _tables), tableName3);

            ///.Progress.Start();
        }

        void ws_UpdateCompleted3(object sender, BMA.MiddlewareApp.DataTableService.UpdateCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                editedDataObjectList3 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }

        }



        void InsertDataTable3(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted3);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName3);

            ///.Progress.Start();
        }

        void ws_InsertCompleted3(object sender, BMA.MiddlewareApp.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList3 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }
        void GetTotalProcRows(string parameterSql, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnProcTotalCompleted);
            ws.TotalProcRowsAsync(connString, parameterSql, databaseName, parameters);

            ///.Progress.Start();
        }

        void ws_ReturnProcTotalCompleted(object sender, BMA.MiddlewareApp.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);



                if (total > pageSize3)
                {
                    double totalDouble = (double)(total / (double)pageSize3);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager3.lblTotal.Text = tol.ToString();
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    pageTotal3 = tol;
                }
                else
                {
                    dataPager3.lblTotal.Text = "1";
                    dataPager3.txtNumber.Text = pageNumber3.ToString();

                }
                //this.Progress.Stop();
            }
        }

        void GetTotalRows3(string parameterSql)
        {
            var ws = WCF.GetService();
            ws.TotalRowsCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs>(ws_ReturnTotalCompleted3);
            ws.TotalRowsAsync(connString, parameterSql, databaseName);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted3(object sender, BMA.MiddlewareApp.DataTableService.TotalRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);

                if (total > pageSize2)
                {
                    double totalDouble = (double)(total / (double)pageSize3);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager3.lblTotal.Text = tol.ToString();
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    pageTotal3 = tol;
                }
                else
                {
                    dataPager3.lblTotal.Text = "1";
                    dataPager3.txtNumber.Text = pageNumber3.ToString();

                }
            }
            //this.Progress.Stop();
        }


        private void SetDefault3()
        {
            UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted3);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 3);
            }
        }


        void ws_updateUserConfigDefaultCompleted3(object sender, BMA.MiddlewareApp.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    Message.InfoMessage("New default configurating setting have been set");
                }
                else
                {
                    Message.ErrorMessage("Error!, Fail to set new default settings");
                }
            }
            else
            {
                Message.ErrorMessage("An Error has occurred while processing your request!");
            }
        }



        private void GetColumnData3(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted3);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted3(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView3.Columns[dropDownName3]).ItemsSource = list;

                    if (fieldIndex < indexMax)
                    {
                        LoadDropDowns3();
                    }


                }

            }

        }





        private void GetExendedPropertiesData3(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetExendedPropertiesDataCompleted3);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetExendedPropertiesDataCompleted3(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    columnNames3 = new List<string>();
                    foreach (DataObject dataObject in list)
                    {

                        propertiesList3.Add(dataObject);
                        string column = dataObject.GetFieldValue("ColumnName").ToString();
                        columnNames3.Add(column);
                    }




                }


            }
          //  GenerateColumns3();

            //LoadConfigSetting3();

        }

        void DeleteDataTable3(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted3);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName3, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted3(object sender, BMA.MiddlewareApp.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList3 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }

        #endregion



        #region GridView Events
        private void radGridView_AddingNewDataItem3(object sender, GridViewAddingNewEventArgs e)
        {

        }

        private void radGridView_RowEditEnded3(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList3.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList3.Contains(dataObject))
                        {
                            if (newDataObjectList3.Contains(dataObject))
                            {
                                this.newDataObjectList3.Remove(dataObject);
                                this.newDataObjectList3.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList3.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList3.Remove(dataObject);
                            this.editedDataObjectList3.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void radGridView_Filtered3(object sender, GridViewFilteredEventArgs e)
        {


            PullData3();
        }


        private void createSqlWithNewFilters3()
        {
            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;

            string newSql = "";
            string gridrship = "";

            //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)

            foreach (GridRelationship rship in GridRelationshipList3)
            {
                try
                {
                    ViewFieldSummary field = fieldList2.Where(f => f.FieldID == rship.ViewField1).FirstOrDefault();
                    ViewFieldSummary field2 = fieldList3.Where(f => f.FieldID == rship.ViewField2).FirstOrDefault();
                    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                    gridrship += " " + field2.TableField + " = '" + parameterValue + "' and";
                }
                catch { }
            }
            if (gridrship != string.Empty)
            {
                gridrship = gridrship.TrimEnd('d');
                gridrship = gridrship.TrimEnd('n');
                gridrship = gridrship.TrimEnd('a');
            }



            if (sqlText3.Contains("Where"))
            {
                newSqlText3 = sqlText3 + " and " + gridrship;
            }
            else
            {
                newSqlText3 = sqlText3 + " Where " + gridrship;
            }




            if (radGridView3.FilterDescriptors.Count > 0)
            {


                newSql += OperatorHandler.BulidQuery(radGridView3);



            }

            if (newSql.Length > 0)
            {
                newSql = newSql.Remove(newSql.Length - 3, 3);

                if (newSqlText3.Contains("Where"))
                {
                    newSqlText3 = newSqlText3 + " and " + newSql;
                }
                else
                {
                    newSqlText3 = newSqlText3 + " Where " + newSql;
                }
            }
            //  else
            // {
            // newSqlText2 = sqlText2;
            // }
            // GetTotalRows2(newSqlText2);
            pageNumber3 = 1;
            // GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
        }

       

        void gridView_Deleted3(object sender, GridViewDeletedEventArgs e)
        {
            DataObject dataObject = e.Items as DataObject;
            deletedDataObjectList2.Add(dataObject);



        }


        private void gridView_LoadingRowDetails3(object sender, GridViewRowDetailsEventArgs e)
        {
            //RadComboBox countries = e.DetailsElement.FindName("rcbCountries") as RadComboBox;
            //countries.ItemsSource = GetCountries();
            // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }
        #endregion


        #region GridMenu Events & Methods
        public MouseButtonEventHandler gridRightClick3 { get; set; }








        void tab_MouseRightButtonDown3(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            // tab_MouseRightButtonUp(sender, e);
            tab_MouseRightButtonUp3(sender, e);
            cMenu3.StaysOpen = true;
        }
        RadContextMenu cMenu3;
        RadTabItem selectedTab3;
        void tab_MouseRightButtonUp3(object sender, MouseButtonEventArgs e)
        {
            var element = sender as UIElement;


            cMenu3 = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Changes";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Add";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Selected Rows";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
            exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "ExcelML";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Word";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Csv";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings As";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Set Current Filter Settings as Defualt";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
            cMenu3.Items.Add(menuItem);
            cMenu3.PlacementTarget = Docking;

            Point p = e.GetPosition(this.Docking);


            cMenu3.Placement = PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu3.HorizontalOffset = p.X-10;
            cMenu3.VerticalOffset = p.Y+50;


            cMenu3.IsOpen = true;



        }
        void RadGridView1_RowLoaded3(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {

                //((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp3);
              //  ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown3);

            }
        }


        void menuItem_Click3(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges3();
                        break;
                    case "Add New":
                        radGridView3.BeginInsert();
                        break;
                    case "Edit":
                        radGridView3.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows3();
                        break;

                    case "Save Filter Settings":
                        SaveConfig3();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs3();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig3();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault3();
                        break;


                    case "Excel":
                        Export3(header);
                        break;

                    case "Word":
                        Export3(header);
                        break;

                    case "ExcelML":
                        Export3(header);
                        break;
                    case "Csv":
                        Export3(header);
                        break;
                    case "Cancel":
                        Cancel3();
                        break;
                    default:
                        break;
                }

            }
        }

        private void RadContextMenu_ItemClick3(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges3();
                        break;
                    case "Add New":
                        radGridView3.BeginInsert();
                        break;
                    case "Edit":
                        radGridView3.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows3();
                        break;

                    case "Save Filter Settings":
                        SaveConfig3();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs3();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig3();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault3();
                        break;


                    case "Excel":
                        Export3(header);
                        break;

                    case "Word":
                        Export3(header);
                        break;

                    case "ExcelML":
                        Export3(header);
                        break;
                    case "Csv":
                        Export3(header);
                        break;
                    case "Cancel":
                        Cancel2();
                        break;
                    default:
                        break;
                }
            }
            // cMenu.IsOpen = false;
        }

        private void Export3(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView3.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }


        private void RadContextMenu_Opened3(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion


        #region MyRegion

        private void Attach3()
        {
            if (radGridView3 != null)
            {

                // create menu
                RadContextMenu contextMenu3 = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu3, StyleManager.GetTheme(radGridView3));

                contextMenu3.Opened += OnMenuOpened3;
                contextMenu3.ItemClick += OnMenuItemClick3;

                RadContextMenu.SetContextMenu(radGridView3, contextMenu3);
            }
        }
        void OnMenuOpened3(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);

                // create menu items
                foreach (GridViewColumn column in radGridView3.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.Header;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Changes";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Add";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Edit";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Selected Rows";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }

        void OnMenuItemClick3(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView3.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView3.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView3.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView3.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView3.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView3.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView3.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView3.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView3.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView3.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView3.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }
        #endregion

        #endregion
    }


    public class DummyModel
    {
        public long ModelID { get; set; }
        public object Date { get; set; }
        public object Time { get; set; }
    }
}
