﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using BMA.MiddlewareApp.Web.Services;


namespace BMA.MiddlewareApp.Web
{
   
    public class CustomValidation
    {

        public static ValidationResult EnsureValidUsername(string username,
          ValidationContext ctx
         )
        {
            
           //UserInformationService userservice = new UserInformationService();
           // LoginUserInfo cust = (LoginUserInfo)ctx.ObjectInstance;
           // LoginUserInfo user = userservice.GetLoginUserInfoes().Where(u => u.UserName == cust.UserName).FirstOrDefault();

            if (username == "Prince")
                return new ValidationResult("The username already exists!");

            return ValidationResult.Success;
        }

        public static ValidationResult ConfirmPassword(string passwordSalt,
         ValidationContext ctx
         )
        {
           

            LoginUserInfo cust = (LoginUserInfo)ctx.ObjectInstance;


            if (passwordSalt != cust.PasswordHash)

                return new ValidationResult("Password doesnot match!");
            return ValidationResult.Success;

        }
    }
}