﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace BMA.MiddlewareApp
{
    public class DateTimeConverter : IValueConverter
	{
		public object Convert(object value,
						   Type targetType,
						   object parameter,
						   System.Globalization.CultureInfo culture)
		{
			try
			{
				DateTime date = (DateTime)value;
				if (parameter is string && parameter != null)
					return date.ToString((string)parameter);
				else
					return date.ToShortTimeString();
			}
			catch (Exception e)
			{
			}
			return "";
		}

		public object ConvertBack(object value,
								  Type targetType,
								  object parameter,
								  System.Globalization.CultureInfo culture)
		{
			string strValue = value.ToString();
			DateTime resultDateTime;
			if (DateTime.TryParse(strValue, out resultDateTime))
			{
				return resultDateTime;
			}
			return value;
		}
	}
}

    
