﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Views
{
    public partial class SaveConfig : ChildWindow
    {
        string xml;
        int gridNo;
        public SaveConfig(string settingsXML, int gridNumber)
        {
            InitializeComponent();
            xml = settingsXML;
            gridNo = gridNumber;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            SaveSetting();
           // this.DialogResult = true;
        }



        private void SaveSetting()
        {
            bool isDefault = false;
            int DisplyDefID = MainPage.DisplayID;
            int UserID = MainPage.userID;
            EditorContext context = new EditorContext();

            Setting setting = new Setting();
            setting.Settings = xml;

            context.Settings.Add(setting);
            context.SubmitChanges(so =>
            {
                if (so.HasError)
                {
                    Message.ErrorMessage(so.Error.Message + " Error adding setting...");
                }
                else
                {
                    context = new EditorContext();
                    UserGridConfig config = new UserGridConfig();
                    config.DisplayDefinition_ID = DisplyDefID;
                    config.ConfigurationName = txtConfigName.Text;
                    config.GridNumber = gridNo;
                    if (checkBox1.IsChecked == true)
                    {
                        config.DefaultConfiguration = 0;
                        isDefault = true;
                    }
                    else
                    {
                        config.DefaultConfiguration = 0;
                    }

                    config.Settings_ID = setting.ID;
                    config.UserInformation_ID = UserID;

                    context.UserGridConfigs.Add(config);
                    context.SubmitChanges(sa =>
            {
                if (sa.HasError)
                {
                    Message.ErrorMessage(so.Error.Message + " Error adding setting...");
                }
                else
                {
                   
                    if (isDefault)
                    {
                        var ws = WCF.GetService();
                        ws.updateUserConfigDefaultAsync(UserID, DisplyDefID, config.ID, gridNo);
                    }
                    Message.InfoMessage("Successfully Saved");
                    this.DialogResult = false;

                }
            }, null);
                }
            }, null);
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

