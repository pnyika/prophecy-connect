﻿#pragma checksum "C:\Users\Prince\Documents\Visual Studio 2010\Projects\BMA.EOMiddleware\BMA.MiddlewareApp\Admin\EditControlFunction.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "536F78301E887A914AF5A1EF71042775"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace BMA.MiddlewareApp.Admin {
    
    
    public partial class EditControlFunction : System.Windows.Controls.ChildWindow {
        
        internal RadBusyIndicator radBusyIndicator;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal RadButton CancelButton;
        
        internal RadButton OKButton;
        
        internal System.Windows.Controls.TextBlock textBlock3;
        
        internal System.Windows.Controls.TextBlock textBlock7;
        
        internal RadComboBox ddlDatabase;
        
        internal RadComboBox DDLFunctionType;
        
        internal RadComboBox ddlStoreProc;
        
        internal System.Windows.Controls.TextBox txtSQLText;
        
        internal System.Windows.Controls.TextBlock textBlock1;
        
        internal System.Windows.Controls.TextBlock strProc;
        
        internal System.Windows.Controls.TextBlock sqlT;
        
        internal RadComboBox ddlDisplayControl;
        
        internal System.Windows.Controls.TextBlock textBlock8;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/BMA.MiddlewareApp;component/Admin/EditControlFunction.xaml", System.UriKind.Relative));
            this.radBusyIndicator = ((RadBusyIndicator)(this.FindName("radBusyIndicator")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.CancelButton = ((RadButton)(this.FindName("CancelButton")));
            this.OKButton = ((RadButton)(this.FindName("OKButton")));
            this.textBlock3 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock3")));
            this.textBlock7 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock7")));
            this.ddlDatabase = ((RadComboBox)(this.FindName("ddlDatabase")));
            this.DDLFunctionType = ((RadComboBox)(this.FindName("DDLFunctionType")));
            this.ddlStoreProc = ((RadComboBox)(this.FindName("ddlStoreProc")));
            this.txtSQLText = ((System.Windows.Controls.TextBox)(this.FindName("txtSQLText")));
            this.textBlock1 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock1")));
            this.strProc = ((System.Windows.Controls.TextBlock)(this.FindName("strProc")));
            this.sqlT = ((System.Windows.Controls.TextBlock)(this.FindName("sqlT")));
            this.ddlDisplayControl = ((RadComboBox)(this.FindName("ddlDisplayControl")));
            this.textBlock8 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock8")));
        }
    }
}

