﻿#pragma checksum "C:\Users\Prince\Documents\Visual Studio 2010\Projects\BMA.EOMiddleware\BMA.MiddlewareApp\Admin\ViewGroups.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "19F86C5F0BFF70186B310A9D417BFFBE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace BMA.MiddlewareApp.Admin {
    
    
    public partial class ViewGroups : System.Windows.Controls.Page {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal RadDomainDataSource usersDomainDataSource;
        
        internal RadGridView radGridView;
        
        internal RadDataPager radDataPager;
        
        internal RadButton submitChangesButton;
        
        internal RadButton rejectChangesButton;
        
        internal RadButton btnAddGroup;
        
        internal RadButton btnModifyGroup;
        
        internal RadButton btnAssignModel;
        
        internal RadButton btnFilterViewGroup;
        
        internal System.Windows.Controls.ListBox lstUsers;
        
        internal RadButton btnShowUsers;
        
        internal RadButton btnSelectUser;
        
        internal RadButton btnUnassign;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/BMA.MiddlewareApp;component/Admin/ViewGroups.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.usersDomainDataSource = ((RadDomainDataSource)(this.FindName("usersDomainDataSource")));
            this.radGridView = ((RadGridView)(this.FindName("radGridView")));
            this.radDataPager = ((RadDataPager)(this.FindName("radDataPager")));
            this.submitChangesButton = ((RadButton)(this.FindName("submitChangesButton")));
            this.rejectChangesButton = ((RadButton)(this.FindName("rejectChangesButton")));
            this.btnAddGroup = ((RadButton)(this.FindName("btnAddGroup")));
            this.btnModifyGroup = ((RadButton)(this.FindName("btnModifyGroup")));
            this.btnAssignModel = ((RadButton)(this.FindName("btnAssignModel")));
            this.btnFilterViewGroup = ((RadButton)(this.FindName("btnFilterViewGroup")));
            this.lstUsers = ((System.Windows.Controls.ListBox)(this.FindName("lstUsers")));
            this.btnShowUsers = ((RadButton)(this.FindName("btnShowUsers")));
            this.btnSelectUser = ((RadButton)(this.FindName("btnSelectUser")));
            this.btnUnassign = ((RadButton)(this.FindName("btnUnassign")));
        }
    }
}

