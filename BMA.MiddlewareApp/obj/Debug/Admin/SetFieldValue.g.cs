﻿#pragma checksum "C:\Users\Prince\Documents\Visual Studio 2010\Projects\BMA.EOMiddleware\BMA.MiddlewareApp\Admin\SetFieldValue.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3550B5CE06FEA33A7B710E8628176B5E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace BMA.MiddlewareApp.Admin {
    
    
    public partial class SetFieldValue : System.Windows.Controls.ChildWindow {
        
        internal Telerik.Windows.Controls.RadBusyIndicator busyIndicator;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Telerik.Windows.Controls.RadButton CancelButton;
        
        internal Telerik.Windows.Controls.RadButton OKButton;
        
        internal System.Windows.Controls.ListBox lstTable;
        
        internal System.Windows.Controls.TextBlock textBlock2;
        
        internal System.Windows.Controls.TextBlock lblFieldTable;
        
        internal System.Windows.Controls.TextBlock textBlock21;
        
        internal System.Windows.Controls.TextBlock textBlock22;
        
        internal System.Windows.Controls.ListBox lstField;
        
        internal Telerik.Windows.Controls.RadComboBox ddlField;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/BMA.MiddlewareApp;component/Admin/SetFieldValue.xaml", System.UriKind.Relative));
            this.busyIndicator = ((Telerik.Windows.Controls.RadBusyIndicator)(this.FindName("busyIndicator")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.CancelButton = ((Telerik.Windows.Controls.RadButton)(this.FindName("CancelButton")));
            this.OKButton = ((Telerik.Windows.Controls.RadButton)(this.FindName("OKButton")));
            this.lstTable = ((System.Windows.Controls.ListBox)(this.FindName("lstTable")));
            this.textBlock2 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock2")));
            this.lblFieldTable = ((System.Windows.Controls.TextBlock)(this.FindName("lblFieldTable")));
            this.textBlock21 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock21")));
            this.textBlock22 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock22")));
            this.lstField = ((System.Windows.Controls.ListBox)(this.FindName("lstField")));
            this.ddlField = ((Telerik.Windows.Controls.RadComboBox)(this.FindName("ddlField")));
        }
    }
}

