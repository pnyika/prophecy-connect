﻿#pragma checksum "C:\Users\Prince\Documents\Visual Studio 2010\Projects\BMA.MiddlewareApp\BMA.MiddlewareApp\Page2.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "13CC1C692D69A27B928AC83DCD9852F5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using BMA.MiddlewareApp.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace BMA.MiddlewareApp {
    
    
    public partial class Page2 : System.Windows.Controls.Page {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Telerik.Windows.Controls.RadButton btnDelete;
        
        internal Telerik.Windows.Controls.RadButton btnAddNew;
        
        internal Telerik.Windows.Controls.RadButton btnSave;
        
        internal Telerik.Windows.Controls.RadButton btnCancel;
        
        internal Telerik.Windows.Controls.RadButton btnSaveConfig;
        
        internal Telerik.Windows.Controls.RadButton btnSetDefault;
        
        internal Telerik.Windows.Controls.RadComboBox ddlConfig;
        
        internal Telerik.Windows.Controls.RadGridView radGridView1;
        
        internal BMA.MiddlewareApp.Controls.BusyIndicator busyIndicator1;
        
        internal Telerik.Windows.Controls.RadDomainDataSource radDomainDataSource1;
        
        internal System.Windows.Controls.CheckBox CanUserDeleteRowsCheckBox;
        
        internal System.Windows.Controls.CheckBox IsReadOnlyCheckBox;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/BMA.MiddlewareApp;component/Page2.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.btnDelete = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnDelete")));
            this.btnAddNew = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnAddNew")));
            this.btnSave = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnSave")));
            this.btnCancel = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnCancel")));
            this.btnSaveConfig = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnSaveConfig")));
            this.btnSetDefault = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnSetDefault")));
            this.ddlConfig = ((Telerik.Windows.Controls.RadComboBox)(this.FindName("ddlConfig")));
            this.radGridView1 = ((Telerik.Windows.Controls.RadGridView)(this.FindName("radGridView1")));
            this.busyIndicator1 = ((BMA.MiddlewareApp.Controls.BusyIndicator)(this.FindName("busyIndicator1")));
            this.radDomainDataSource1 = ((Telerik.Windows.Controls.RadDomainDataSource)(this.FindName("radDomainDataSource1")));
            this.CanUserDeleteRowsCheckBox = ((System.Windows.Controls.CheckBox)(this.FindName("CanUserDeleteRowsCheckBox")));
            this.IsReadOnlyCheckBox = ((System.Windows.Controls.CheckBox)(this.FindName("IsReadOnlyCheckBox")));
        }
    }
}

