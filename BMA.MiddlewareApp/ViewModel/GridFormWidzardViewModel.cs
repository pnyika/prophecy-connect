﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using MetroWizard.Commands;

namespace BMA.MiddlewareApp.ViewModel
{
    public class GridFormWidzardViewModel : ViewModelBase
    {
        public GridFormWidzardViewModel()
        {
            PageCount = 2;
            
            CancelCommand = new DelegateCommand((x) => CurrentPage = 0);
            PreviousCommand = new DelegateCommand((x) => CurrentPage--);
            NextCommand = new DelegateCommand((x) => CurrentPage++);
            FinishCommand = new DelegateCommand((x) => CurrentPage = 0);
        }

        public ICommand CancelCommand { get; set; }
        public ICommand PreviousCommand { get; set; }
        public ICommand NextCommand { get; set; }
        public ICommand FinishCommand { get; set; }

        private int currentPage;
        public int CurrentPage 
        { 
            get { return currentPage; } 
            set 
            { 
                if (currentPage == value) return;
                currentPage = value;
                NotifyPropertyChanged("CurrentPage");
            } 
        }

        public int PageCount { get; set; }
    }
}

    