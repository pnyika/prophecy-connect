﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client.ApplicationServices;

namespace BMA.MiddlewareApp
{
    public partial class LoginCustomControl : UserControl
    {

       
        public event EventHandler LoginClick;
        public event EventHandler LoginComplete;
        public LoginCustomControl()
        {
            InitializeComponent();
            btnLogin.IsEnabled = false;
        }

     
      
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        

        private void Login()
        {
            btnLogin.Content = "Loging....";
            btnLogin.IsEnabled = false;
            busyIndicator.IsBusy = true;

            // LoginComplete(this, null);
            LoginParameters para = new LoginParameters();

            WebContext.Current.Authentication.Login(new LoginParameters(txtUsername.Text,
            txtPassword.Password), LoginCompleteCallback, null);
        }

        #region Control Event Handlers

        private void LoginCompleteCallback(LoginOperation result)
        {
            try
            {
                if (!result.HasError)
                {
                    if (result.LoginSuccess)
                    {

                        // Let the parent control know that the login was successful

                        if (LoginComplete != null)
                        {
                          
                            LoginComplete(this, null);
                        }

                        // Clearing the error text here prevents the login failed message from
                        // being displayed again if the user did fail a login attempt and then 
                        // eventually succeeded.  

                        //Without doing this after the user logs out the previous error
                        // message would still be visible on the home page

                        busyIndicator.IsBusy = false;
                        loginError.Text = "";
                    }
                    else
                    {

                        btnLogin.Content = "Login";
                        btnLogin.IsEnabled = true;
                        loginError.Text = "Incorrect username or password!";
                        busyIndicator.IsBusy = false;
                        //busyIndicator.IsBusy = false;
                        //busyIndicator.Content = "";


                    }
                }
                else
                {
                    string error = result.Error.Message;
                    btnLogin.Content = "Login";
                    busyIndicator.IsBusy = false;
                    btnLogin.IsEnabled = true;
                    loginError.Text = "Incorrect username or password!";
                    //busyIndicator.IsBusy = false;
                    //busyIndicator.Content = "";
                    result.MarkErrorAsHandled();
                }
            }
            catch
            {
                busyIndicator.IsBusy = false;
            }
            busyIndicator.IsBusy = false;
        }

        public void EnableOrDisableOKButton(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text.Length < 1 || txtPassword.Password.Length < 1)
                btnLogin.IsEnabled = false;
            else
            {
                btnLogin.IsEnabled = true;
               // btnLogin.Focus();
            }
        }

        #endregion

        private void txtUsername_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtUsername.Text.Length < 1 || txtPassword.Password.Length < 1)
                    btnLogin.IsEnabled = false;
                else
                {
                    Login();
                }
            }
        }

   
    }
}
