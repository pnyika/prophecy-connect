﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.Navigation
{
    public class NavigationService : INavigationService
    {
        private readonly System.Windows.Navigation.NavigationService _navigationService;

        public NavigationService(System.Windows.Navigation.NavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void Navigate(string url)
        {
            _navigationService.Navigate(new Uri(url, UriKind.RelativeOrAbsolute));
        }

        public void Back()
        {
            if (_navigationService.CanGoBack)
                _navigationService.GoBack();
        }
    }

}
