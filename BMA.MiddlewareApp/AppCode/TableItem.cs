﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace BMA.MiddlewareApp
{
    public class TableItem
    {
        private string nodeText;
        private int nodeID;
        private System.Nullable<int> parentID;
        private List<TableItem> children;

        public TableItem(string nodeText, int nodeID, System.Nullable<int> parentID)
        {
            this.nodeText = nodeText;
            this.nodeID = nodeID;
            this.parentID = parentID;

            this.children = new List<TableItem>();
        }

        public string NodeText
        {
            get
            {
                return this.nodeText;
            }
        }
        public System.Nullable<int> ParentID
        {
            get
            {
                return this.parentID;
            }
        }
        public int NodeID
        {
            get
            {
                return this.nodeID;
            }
        }
        public List<TableItem> Children
        {
            get
            {
                return this.children;
            }
        }
    }
}
