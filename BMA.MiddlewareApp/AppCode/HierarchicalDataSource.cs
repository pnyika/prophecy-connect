﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using BMA.MiddlewareApp.Web;

namespace BMA.MiddlewareApp
{
    public class HierarchicalDataSource : ObservableCollection<TableItem>
	{
		// This list holds all the items that come from the web service result
		private List<TableItem> unsortedList = new List<TableItem>();

		public HierarchicalDataSource()
		{
			// Create a new instance of the web service and get the data from the table

            
		
			// transfer all the items from the result to the unsorted list
            EditorContext context = new EditorContext();
            List<BMA.MiddlewareApp.Web.MenuItem> menuList = context.GetMenuItemsQuery().Query.Cast<BMA.MiddlewareApp.Web.MenuItem>().Where(u => u.MenuStructure_ID == 1).ToList();
            foreach (BMA.MiddlewareApp.Web.MenuItem item in menuList)
			{
				TableItem genericItem = new TableItem(item.DisplayName, item.ID, item.ParentMenu_ID);
				this.unsortedList.Add(genericItem);
			}

			// Get all the first level nodes. In our case it is only one - House M.D.
			var rootNodes = this.unsortedList.Where(x => x.ParentID ==0 );

			// Foreach root node, get all its children and add the node to the HierarchicalDataSource.
			// see bellow how the FindChildren method works
			foreach (TableItem node in rootNodes)
			{
				this.FindChildren(node);
				this.Add(node);
			}
		}
		
		private void FindChildren(TableItem item)
		{
			// find all the children of the item
			var children = unsortedList.Where(x => x.ParentID == item.NodeID && x.NodeID != item.NodeID);

			// add the child to the item's children collection and call the FindChildren recursively, in case the child has children
			foreach (TableItem child in children)
			{
                // By not calling iteratively FindChildren() here we prevent
                // the automatic loading of all items in the data
                // source and load only the next level in the hierarchy
				item.Children.Add(child);
			}
		}

        public void LoadItemChildren(TableItem item)
        {
            foreach (TableItem i in item.Children)
            {
                FindChildren(i);
            }
        }
	}
}

 