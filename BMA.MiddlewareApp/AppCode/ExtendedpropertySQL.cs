﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.AppCode
{
    public static class ExtendedPropertySQL
    {
        public static string ExtendedPropertiesSQl(string DB, string tableName)
        {
            string sql = " ";
            sql += "   Declare @SqlString nvarchar(max), @ParmDefinition nvarchar(500),@OutString varchar(3000), @cn Varchar(300), @EP nvarchar(max) ";
            sql += " Declare @temp Table (ColumnName Varchar(300),[ExtendedPropertyValue] nvarchar(max))  ";
            sql += " insert into @temp ";

            sql += " SELECT   c.name AS [ColumnName], ltrim(convert(nvarchar(max),ep.value)) AS [ExtendedPropertyValue] ";
            sql += " FROM sys.extended_properties AS ep ";
            sql += " INNER JOIN sys.tables AS t ON ep.major_id = t.object_id ";
            sql += " INNER JOIN sys.columns AS c ON ep.major_id = c.object_id AND ep.minor_id = c.column_id ";
            sql += " WHERE class = 1 and ep.name = 'MS_RowSource' and t.name = '" + tableName + "';";
            sql += " SET @ParmDefinition = N'@OutString varchar(3000) OUTPUT'; ";
            sql += " Declare myCursor Cursor for  ";
            sql += "Select ColumnName,[ExtendedPropertyValue] from @temp where [ExtendedPropertyValue] like 'Select%' ";
            sql += "OPEN myCursor ";
            sql += "FETCH NEXT FROM myCursor INTO @cn, @ep ";
            sql += "WHILE @@FETCH_STATUS = 0 ";
            sql += "BEGIN ";
            sql += "Set @SqlString = replace(@ep , 'select ', 'SELECT @OutString = coalesce(@OutString + '';'' ,'''') +') ";
            sql += "exec sp_executesql @SqlString, @ParmDefinition, @OutString=@OutString OUTPUT; ";
            sql += "update @temp set [ExtendedPropertyValue] = @OutString where ColumnName = @cn ";
            sql += "	FETCH NEXT FROM myCursor INTO @cn, @ep ";
            sql += "END ";
            sql += "CLOSE myCursor; ";
            sql += "DEALLOCATE myCursor; ";
            sql += "Select * from @temp ";
            return sql;

        }
    }
}
