﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
//using System.Collections.Generic;
//using System.Collections.Generic;

namespace BMA.MiddlewareApp
{
    public class CustomKeyboardCommandProvider : DefaultKeyboardCommandProvider
    {
        private GridViewDataControl parentGrid;
        private DefaultKeyboardCommandProvider defaultKeyboardProvider;
        private CustomKeyboardCommandProvider customKeyboardProvider;
        public CustomKeyboardCommandProvider(GridViewDataControl grid)
            : base(grid)
        {
            this.parentGrid = grid;
        }
        public override IEnumerable<ICommand> ProvideCommandsForKey(Key key)
        {
            List<ICommand> commandsToExecute = base.ProvideCommandsForKey(key).ToList();
          
          
            if (key == Key.Up)
            {
                commandsToExecute.Clear();
                commandsToExecute.Add(RadGridViewCommands.CommitEdit);
                commandsToExecute.Add(RadGridViewCommands.MoveUp);
                commandsToExecute.Add(RadGridViewCommands.BeginEdit);
            }
            else if (key == Key.Down)
            {
                commandsToExecute.Clear();
                commandsToExecute.Add(RadGridViewCommands.CommitEdit);
                commandsToExecute.Add(RadGridViewCommands.MoveDown);
                commandsToExecute.Add(RadGridViewCommands.BeginEdit);
            }

            return commandsToExecute;
        }
    }
}
