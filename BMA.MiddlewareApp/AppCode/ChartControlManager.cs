﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Text;
using Telerik.Windows.Controls;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using AmCharts.Windows.QuickCharts;

namespace BMA.MiddlewareApp
{
    public static class ChartControlManager
    {


        public static string CreateControlXmlString(Control control,  Control compareControl, string objectType)
        {
            StringBuilder xml = new StringBuilder("");
           
            string amq = "http://schemas.amcharts.com/quick/wpf/2010/xaml";

         

            xml.Append("<" + objectType);
            
            
                xml.Append(" xmlns='" + amq + "' ");


            Type t = control.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {
                if (prp.CanWrite)
                {
                    try{

                    var name = prp.Name;
                   
                    var value = prp.GetValue(control, null);


                    if ((name == "Foreground") || (name == "Background") || (name == "BoardBrush") || (name == "Brush") || (name == "AxisForeground"))
                    {
                        var colorcode = new System.Windows.Media.SolidColorBrush();
                        colorcode = (SolidColorBrush)value;
                       value = colorcode.Color;
                    }
                    if (value != null)
                    {
                        //  if (prp.PropertyType.ToString() == "System.Windows.Input.ICommand")
                        if ((value.ToString().Length < 25) || (name == "Text") || (name == "Content"))
                        {


                            Type tt = compareControl.GetType();
                            PropertyInfo[] newProps = tt.GetProperties();
                            PropertyInfo newprp = newProps.Where(p => p.Name == name).FirstOrDefault();
                            if (newprp != null)
                            {
                                var value2 = newprp.GetValue(compareControl, null);
                                if (value2 == null)
                                    value2 = "";
                                    if (value.ToString() != value2.ToString())
                                    {
                                        string type = prp.PropertyType.Name;
                                        string strValue = value.ToString();
                                        if(name != "IsEnabled")
                                        xml.Append(name + "='" + strValue + "' ");
                                    }
                                
                            }
                        }


                    }
                }catch{}
                }
            }
            //end serial primary propertis
            xml.Append(">");

            //add child graphs

            xml.Append("<SerialChart.Graphs>");

            SerialChart graphs = control as SerialChart;
            string xmlString = "";
            foreach (var item in graphs.Graphs.OrderBy(g=>g.ToString()))
            {
                string itemString = item.ToString();
                string type = itemString;//(item.Template.TargetType).FullName;
                if (type == "AmCharts.Windows.QuickCharts.LineGraph")
                {
                    Control lineControl = item as Control;
                    LineGraph lineGraph = new LineGraph();
                    xmlString += CreateChildControlXmlString(lineControl, lineGraph, "LineGraph");
                }

                if (type == "AmCharts.Windows.QuickCharts.AreaGraph")
                {
                    Control areaControl = item as Control;
                    AreaGraph areaGraph = new AreaGraph();
                    xmlString += CreateChildControlXmlString(areaControl, areaGraph, "AreaGraph");
                }

                if (type == "AmCharts.Windows.QuickCharts.ColumnGraph")
                {
                    Control columnControl = item as Control;
                    ColumnGraph columnGraph = new ColumnGraph();
                    xmlString += CreateChildControlXmlString(columnControl, columnGraph, "ColumnGraph");
                }
            }

            xml.Append(xmlString);
            //end serial graph collections
            xml.Append("</SerialChart.Graphs>");

            xml.Append("</" + objectType+">");
            string xmlOutput = xml.ToString();
            return xmlOutput;
        }



        public static string CreateChildControlXmlString(Control control, Control compareControl, string objectType)
        {
            StringBuilder xml = new StringBuilder("");

         //   string amq = "http://schemas.amcharts.com/quick/wpf/2010/xaml";



            xml.Append("<" + objectType +" ");


            //xml.Append(" xmlns='" + amq + "' ");


            Type t = control.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {
                if (prp.CanWrite)
                {
                    try
                    {

                        var name = prp.Name;

                        var value = prp.GetValue(control, null);


                        if ((name == "Foreground") || (name == "Background") || (name == "BoardBrush") || (name == "Brush") || (name == "AxisForeground"))
                        {
                            var colorcode = new System.Windows.Media.SolidColorBrush();
                            colorcode = (SolidColorBrush)value;
                            value = colorcode.Color;
                        }
                        if (value != null)
                        {
                            //  if (prp.PropertyType.ToString() == "System.Windows.Input.ICommand")
                            if ((value.ToString().Length < 25) || (name == "Text") || (name == "Content"))
                            {


                                Type tt = compareControl.GetType();
                                PropertyInfo[] newProps = tt.GetProperties();
                                PropertyInfo newprp = newProps.Where(p => p.Name == name).FirstOrDefault();
                                if (newprp != null)
                                {
                                    var value2 = newprp.GetValue(compareControl, null);
                                    if (value2 == null)
                                        value2 = "";
                                    if (value.ToString() != value2.ToString())
                                    {
                                        string type = prp.PropertyType.Name;
                                        string strValue = value.ToString();
                                        if (name != "IsEnabled")
                                            xml.Append(name + "='" + strValue + "' ");
                                    }

                                }
                            }


                        }
                    }
                    catch { }
                }
            }
            //end serial primary propertis
            xml.Append(">");

           


            xml.Append("</" + objectType + ">");
            string xmlOutput = xml.ToString();
            return xmlOutput;
        }



        public static string CreatePieControlXmlString(Control control, Control compareControl, string objectType)
        {
            StringBuilder xml = new StringBuilder("");

            string amq = "http://schemas.amcharts.com/quick/wpf/2010/xaml";



            xml.Append("<" + objectType);


            xml.Append(" xmlns='" + amq + "' ");


            Type t = control.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {
                if (prp.CanWrite)
                {
                    try
                    {

                        var name = prp.Name;

                        var value = prp.GetValue(control, null);


                        if ((name == "Foreground") || (name == "Background") || (name == "BoardBrush") || (name == "Brush") || (name == "AxisForeground"))
                        {
                            var colorcode = new System.Windows.Media.SolidColorBrush();
                            colorcode = (SolidColorBrush)value;
                            value = colorcode.Color;
                        }
                        if (value != null)
                        {
                            //  if (prp.PropertyType.ToString() == "System.Windows.Input.ICommand")
                            if ((value.ToString().Length < 25) || (name == "Text") || (name == "Content"))
                            {


                                Type tt = compareControl.GetType();
                                PropertyInfo[] newProps = tt.GetProperties();
                                PropertyInfo newprp = newProps.Where(p => p.Name == name).FirstOrDefault();
                                if (newprp != null)
                                {
                                    var value2 = newprp.GetValue(compareControl, null);
                                    if (value2 == null)
                                        value2 = "";
                                    if (value.ToString() != value2.ToString())
                                    {
                                        string type = prp.PropertyType.Name;
                                        string strValue = value.ToString();
                                        if (name != "IsEnabled")
                                            xml.Append(name + "='" + strValue + "' ");
                                    }

                                }
                            }


                        }
                    }
                    catch { }
                }
            }
            //end serial primary propertis
            xml.Append(">");

            //add child graphs

          


            xml.Append("</" + objectType + ">");
            string xmlOutput = xml.ToString();
            return xmlOutput;
        }

        private static string ByteToString(Byte[] buffer)
        {
            string xmlOutput = System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            return xmlOutput;
        }


        /// <summary>
        /// Compares the properties of two objects of the same type and returns if all properties are equal.
        /// </summary>
        /// <param name="objectA">The first object to compare.</param>
        /// <param name="objectB">The second object to compre.</param>
        /// <param name="ignoreList">A list of property names to ignore from the comparison.</param>
        /// <returns><c>true</c> if all property values
        ///           are equal, otherwise <c>false</c>.</returns>
        public static bool AreObjectsEqual(object objectA, object objectB, params string[] ignoreList)
        {
            bool result;

            if (objectA != null && objectB != null)
            {
                Type objectType;

                objectType = objectA.GetType();

                result = true; // assume by default they are equal

                foreach (PropertyInfo propertyInfo in objectType.GetProperties(
                  BindingFlags.Public | BindingFlags.Instance).Where(
                  p => p.CanRead && !ignoreList.Contains(p.Name)))
                {
                    object valueA;
                    object valueB;

                    valueA = propertyInfo.GetValue(objectA, null);
                    valueB = propertyInfo.GetValue(objectB, null);

                    // if it is a primative type, value type or implements
                    // IComparable, just directly try and compare the value
                    if (CanDirectlyCompare(propertyInfo.PropertyType))
                    {
                        if (!AreValuesEqual(valueA, valueB))
                        {
                            Console.WriteLine("Mismatch with property '{0}.{1}' found.",
                                        objectType.FullName, propertyInfo.Name);
                            result = false;
                        }
                    }
                    // if it implements IEnumerable, then scan any items
                    else if (typeof(IEnumerable).IsAssignableFrom(propertyInfo.PropertyType))
                    {
                        IEnumerable<object> collectionItems1;
                        IEnumerable<object> collectionItems2;
                        int collectionItemsCount1;
                        int collectionItemsCount2;

                        // null check
                        if (valueA == null && valueB != null || valueA != null && valueB == null)
                        {
                            Console.WriteLine("Mismatch with property '{0}.{1}' found.",
                                                     objectType.FullName, propertyInfo.Name);
                            result = false;
                        }
                        else if (valueA != null && valueB != null)
                        {
                            collectionItems1 = ((IEnumerable)valueA).Cast<object>();
                            collectionItems2 = ((IEnumerable)valueB).Cast<object>();
                            collectionItemsCount1 = collectionItems1.Count();
                            collectionItemsCount2 = collectionItems2.Count();

                            // check the counts to ensure they match
                            if (collectionItemsCount1 != collectionItemsCount2)
                            {
                                Console.WriteLine("Collection counts for property '{0}.{1}' do not match.",
                                                    objectType.FullName, propertyInfo.Name);
                                result = false;
                            }
                            // and if they do, compare each item...
                            // this assumes both collections have the same order
                            else
                            {
                                for (int i = 0; i < collectionItemsCount1; i++)
                                {
                                    object collectionItem1;
                                    object collectionItem2;
                                    Type collectionItemType;

                                    collectionItem1 = collectionItems1.ElementAt(i);
                                    collectionItem2 = collectionItems2.ElementAt(i);
                                    collectionItemType = collectionItem1.GetType();

                                    if (CanDirectlyCompare(collectionItemType))
                                    {
                                        if (!AreValuesEqual(collectionItem1, collectionItem2))
                                        {
                                            Console.WriteLine("Item {0} in property collection '{1}.{2}' does not match.",
                                                       i, objectType.FullName, propertyInfo.Name);
                                            result = false;
                                        }
                                    }
                                    else if (!AreObjectsEqual(collectionItem1, collectionItem2, ignoreList))
                                    {
                                        Console.WriteLine("Item {0} in property collection '{1}.{2}' does not match.",
                                                            i, objectType.FullName, propertyInfo.Name);
                                        result = false;
                                    }
                                }
                            }
                        }
                    }
                    else if (propertyInfo.PropertyType.IsClass)
                    {
                        if (!AreObjectsEqual(propertyInfo.GetValue(objectA, null),
                                                 propertyInfo.GetValue(objectB, null), ignoreList))
                        {
                            Console.WriteLine("Mismatch with property '{0}.{1}' found.",
                                                    objectType.FullName, propertyInfo.Name);
                            result = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Cannot compare property '{0}.{1}'.",
                                                  objectType.FullName, propertyInfo.Name);
                        result = false;
                    }
                }
            }
            else
                result = object.Equals(objectA, objectB);

            return result;
        }

        /// <summary>
        /// Determines whether value instances of the specified type can be directly compared.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///     <c>true</c> if this value instances of the specified
        ///           type can be directly compared; otherwise, <c>false</c>.
        /// </returns>
        private static bool CanDirectlyCompare(Type type)
        {
            return typeof(IComparable).IsAssignableFrom(type) || type.IsPrimitive || type.IsValueType;
        }

        /// <summary>
        /// Compares two values and returns if they are the same.
        /// </summary>
        /// <param name="valueA">The first value to compare.</param>
        /// <param name="valueB">The second value to compare.</param>
        /// <returns><c>true</c> if both values match,
        //              otherwise <c>false</c>.</returns>
        private static bool AreValuesEqual(object valueA, object valueB)
        {
            bool result;
            IComparable selfValueComparer;

            selfValueComparer = valueA as IComparable;

            if (valueA == null && valueB != null || valueA != null && valueB == null)
                result = false; // one of the values is null
            else if (selfValueComparer != null && selfValueComparer.CompareTo(valueB) != 0)
                result = false; // the comparison using IComparable failed
            else if (!object.Equals(valueA, valueB))
                result = false; // the comparison using Equals failed
            else
                result = true; // match

            return result;
        }
       
    }
}
