﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using System.ComponentModel;


namespace BMA.MiddlewareApp
{
    public class TabViewModel : INotifyPropertyChanged
	{
		private bool isSelected;
		private MainViewModel mainViewModel;

		public TabViewModel(MainViewModel mainViewModel)
		{
			this.mainViewModel = mainViewModel;
			this.mainViewModel.Tabs.CollectionChanged += this.Tabs_CollectionChanged;

			this.AddItemCommand = new DelegateCommand(
				delegate
				{
					this.mainViewModel.AddItem(this);
				},
				delegate
				{
					return this.mainViewModel.Tabs.Count < 50;
				});

			this.RemoveItemCommand = new DelegateCommand(
				delegate
				{
					this.mainViewModel.RemoveItem(this);
				},
				delegate
				{
					return this.mainViewModel.Tabs.Count > 1;
				});
		}

		void Tabs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			this.AddItemCommand.InvalidateCanExecute();
			this.RemoveItemCommand.InvalidateCanExecute();
		}

		public string Header
		{
			get;
			set;
		}

		public bool IsSelected
		{
			get
			{
				return this.isSelected;
			}
			set
			{
				if (this.isSelected != value)
				{
					this.isSelected = value;
					this.OnPropertyChanged("IsSelected");
				}
			}
		}


        public UserControl userControl
        {
            get;
            set;
        }

		public DelegateCommand AddItemCommand { get; set; }
		public DelegateCommand RemoveItemCommand { get; set; }

		#region INotifyPropertyChanged
		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion
	}
}

