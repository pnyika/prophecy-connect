﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace BMA.MiddlewareApp
{
    public class CharacterHandler
    {

        public static string ReplaceSpecialCharacter(string SourceString)
        {
            StringBuilder RetString = new StringBuilder();

            Char[] test = SourceString.ToCharArray();
            for (int i = 0; i < test.Length; i++)
            {
                int Value = Convert.ToInt16(test[i]);
                if ((Value < 48) || (Value > 57 && Value < 65) || (Value > 90 && Value < 95) || (Value == 96) || (Value > 123))
                {
                    RetString.Append("_x00").Append(Value.ToString("X")).Append("_");
                }
                else
                {
                    RetString.Append(test[i]);
                }
                /// Hex to Decimal: 
                /// int decValue = Convert.ToInt32(hexValue, 16);
            }
            return RetString.ToString();
        }

        public static string ReplaceHexCharacter(string SourceString)
        {
            StringBuilder RetString = new StringBuilder();
            string NewString = SourceString;
            while (NewString.Contains("_x00"))
            {
                RetString.Append(NewString.Substring(0, NewString.IndexOf("_x00")));
                String TempHex = NewString.Substring(NewString.IndexOf("_x00") + 4,2);
                NewString = NewString.Substring(NewString.IndexOf("_x00") + 7);
                RetString.Append(Convert.ToChar(Convert.ToInt32(TempHex, 16)).ToString());
            }
            RetString.Append(NewString);
            return RetString.ToString();
        }


        public static string ReplaceSpecialCharacter2(string SourceString)
        {
            StringBuilder RetString = new StringBuilder();

            Char[] test = SourceString.ToCharArray();
            for (int i = 0; i < test.Length; i++)
            {
                int Value = Convert.ToInt16(test[i]);
                if ((Value < 48) || (Value > 57 && Value < 65) || (Value > 90 && Value < 95) || (Value == 96) || (Value > 123))
                {
                    RetString.Append("zx00").Append(Value.ToString("X")).Append("z");
                }
                else
                {
                    RetString.Append(test[i]);
                }
                /// Hex to Decimal: 
                /// int decValue = Convert.ToInt32(hexValue, 16);
            }
            return RetString.ToString();
        }

        public static string ReplaceHexCharacter2(string SourceString)
        {
            StringBuilder RetString = new StringBuilder();
            string NewString = SourceString;
            while (NewString.Contains("zx00"))
            {
                RetString.Append(NewString.Substring(0, NewString.IndexOf("zx00")));
                String TempHex = NewString.Substring(NewString.IndexOf("zx00") + 4, 2);
                NewString = NewString.Substring(NewString.IndexOf("zx00") + 7);
                RetString.Append(Convert.ToChar(Convert.ToInt32(TempHex, 16)).ToString());
            }
            RetString.Append(NewString);
            return RetString.ToString();
        }
 
    }
}
