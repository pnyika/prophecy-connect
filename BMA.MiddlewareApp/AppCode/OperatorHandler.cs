﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Text;

namespace BMA.MiddlewareApp.AppCode
{
    public class OperatorHandler
    {

        public static string BulidQuery(Telerik.Windows.Controls.RadGridView grid)
        {
            StringBuilder RetString = new StringBuilder();
            foreach (Telerik.Windows.Controls.GridViewColumn column in grid.Columns)
            {
                Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;

                if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                {

                    RetString.Append(" " +column.UniqueName + " " + ReplaceExpression(filterDescriptors.FieldFilter.Filter1.Operator.ToString(), filterDescriptors.FieldFilter.Filter1.Value.ToString()));
                    if (filterDescriptors.FieldFilter.Filter2.Value.ToString() != "")
                    {
                        RetString.Append(filterDescriptors.FieldFilter.LogicalOperator + " " + column.UniqueName + " " + ReplaceExpression(filterDescriptors.FieldFilter.Filter2.Operator.ToString(), filterDescriptors.FieldFilter.Filter2.Value.ToString()));
                    }
                    RetString.Append(" and");
                }
                
            }
            return RetString.ToString();
        }


        private static string ReplaceExpression(string expressionOp, string value)
        {
            string sqlOperator = "";

            switch (expressionOp)
            {
                case "IsEqualTo":
                    sqlOperator = "='" + value + "' ";
                    break;

                case "IsNotEqualTo":
                    sqlOperator = "!='" + value + "' ";
                    break;

                case "StartsWith":
                    sqlOperator = "like " + "'" + value + "%' ";
                    break;


                case "EndsWith":
                    sqlOperator = "like " + "'%" + value + "'";
                    break;

                case "Contains":
                    sqlOperator = "in ('" + value + "') ";
                    break;

                case "DoesNotContains":
                    sqlOperator = "not in ('" + value + "') ";
                    break;

                case "IsContainedIn":
                    sqlOperator = "in ('" + value + "') ";
                    break;

                case "IsNotContainedIn":
                    sqlOperator = "not in ('" + value + "')";
                    break;

                case "IsLessThan":
                    sqlOperator = "< '" + value + "' ";
                    break;

                case "IsLessThanOrEqualTo":
                    sqlOperator = "<= '" + value + "' ";
                    break;

                case "IsGreaterThan":
                    sqlOperator = "> '" + value + "' ";
                    break;
                case "IsGreaterThanOrEqualTo":
                    sqlOperator = ">= '" + value + "' ";
                    break;


            
            }

            return sqlOperator.ToString();
        }
    }
}
