﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.Controls
{
    public class RightClickContentMenu : Dialog
    {

     public event EventHandler TestClick;
        StackPanel rectangle;

        public RightClickContentMenu()
        {
           // rectangle = rect;
        }

        protected override void OnClickOutside()
        {
            Close();
        }
        public void TestClose()
        {
            Close();
        }


        protected override FrameworkElement GetContent()
        {
            Grid grid = new Grid() { Width = 131, Height = 198 };
            Border border = new Border() {  BorderBrush = new SolidColorBrush(Colors.Gray), BorderThickness = new Thickness(1,1,1,1), VerticalAlignment = VerticalAlignment.Stretch };
            grid.Children.Add(border);

         TextBlock red = new TextBlock() { Text = "Save", Width = 90 };
            //System.Windows.Controls.Label red = new System.Windows.Controls.Label() { Content = "Save", BorderBrush = new SolidColorBrush(Colors.Gray), BorderThickness = new Thickness(2, 2, 2, 2),Width = 90, Height = 28 };
            red.MouseLeftButtonUp +=new MouseButtonEventHandler(red_MouseLeftButtonUp);

            TextBlock blue = new TextBlock() { Text = "Add New", Width = 90 };
            blue.MouseLeftButtonUp += new MouseButtonEventHandler(blue_MouseLeftButtonUp);

            TextBlock green = new TextBlock() { Text = "Delete", Width = 90 };
            green.MouseLeftButtonUp += new MouseButtonEventHandler(green_MouseLeftButtonUp);

            TextBlock yellow = new TextBlock() { Text = "Save Settings", Width = 90 };
            yellow.MouseLeftButtonUp += new MouseButtonEventHandler(yellow_MouseLeftButtonUp);

            TextBlock current = new TextBlock() { Text = "Set as Default", Width = 90 };
            yellow.MouseLeftButtonUp += new MouseButtonEventHandler(yellow_MouseLeftButtonUp);

            TextBlock cancel = new TextBlock() { Text = "Cancel", Width = 90 };
            cancel.MouseLeftButtonUp += new MouseButtonEventHandler(CancelContextMenu);


            RightClickMenu right = new RightClickMenu();
            System.Windows.Controls.ListBox options = new System.Windows.Controls.ListBox();
            //options.Items.Add(red);
            //options.Items.Add(blue);
            //options.Items.Add(green);
            //options.Items.Add(yellow);
            options.Items.Add(right);
           // options.Items.Add(cancel);

            grid.Children.Add(options);
          grid.MouseLeave += new MouseEventHandler(grid_MouseLeave);
            return grid;

        }

        void grid_MouseLeave(object sender, MouseEventArgs e)
        {
            Close();
        }


        void CancelContextMenu(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

       void red_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Close();
            OneGrid.color_TestClick();
           // TestClick(sender, e);
           // SolidColorBrush solidColor = new SolidColorBrush();
           //solidColor.Color = Colors.Red;
           //rectangle.Background = solidColor;
       

        }

         void green_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SolidColorBrush solidColor = new SolidColorBrush();
           solidColor.Color = Colors.Green;
         //  rectangle.Background = solidColor;
           Close();

        }



         void blue_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SolidColorBrush solidColor = new SolidColorBrush();
           solidColor.Color = Colors.Blue;
           //rectangle.Background = solidColor;
           Close();

        }

         void yellow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SolidColorBrush solidColor = new SolidColorBrush();
           solidColor.Color = Colors.Yellow;
          // rectangle.Background = solidColor;
           Close();

        }
    }
}
