﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using BMA.MiddlewareApp.Navigation;
using System.Collections.ObjectModel;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewListsp : Page
    {
        EditorContext context = new EditorContext();
       public ObservableCollection<View> viewList{get;set;}
        public ViewListsp()
        {
            InitializeComponent();
           
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GetViews();
        }



        private void GetViews()
        {
            radGridView1.IsBusy = true;
           LoadOperation<View> loadOp = context.Load(context.GetViewsQuery(), CallbackViews, null);

                
           
          
        }


        private void CallbackViews(LoadOperation<View> result)
        {


            if (result != null)
            {
                viewList = new ObservableCollection<View>();
                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                viewList = new ObservableCollection<View>(result.Entities.Where(v => v.GroupViews.Any(ug => ug.Group_ID == Globals.CurrentUser.Group_ID)).OrderBy(si => si.ViewName));
                else
                    viewList = new ObservableCollection<View>(result.Entities.OrderBy(si => si.ViewName));


               // usersDomainDataSource.DataContext = viewList;
                radGridView1.ItemsSource = viewList;
                radGridView1.IsBusy = false;

            }
        }
        void MyRadDomainDataSource_SubmittedChanges(object sender, Telerik.Windows.Controls.DomainServices.DomainServiceSubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Avoids displaying errors, when deletion is prevented by DataBase constraints
                e.MarkErrorAsHandled();
            }
            else
            {
                Message.InfoMessage("Successfully saved!");
            }
        }
            

             private void radGridView1_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
             {
                 try
                 {
                     View view = radGridView1.SelectedItem as View;
                   
                     this.NavigationService.Navigate(new Uri("/SpecificView?viewID=" + view.ID.ToString(), UriKind.Relative));
                   
                 }
                 catch(Exception ex)
                 {

                 }

             }

             private void btnAddNew_Click(object sender, RoutedEventArgs e)
             {
                 AddView view = new AddView();
                 view.Closed +=new EventHandler(view_Closed);
                 view.Show();
             }
             void view_Closed(object sender, EventArgs e)
             {
                 
             }

             private void btnModify_Click(object sender, RoutedEventArgs e)
             {
                 try
                 {
                     View view = radGridView1.SelectedItem as View;
                     if (view == null)
                         return;

                     this.NavigationService.Navigate(new Uri("/SpecificView?viewID=" + view.ID.ToString(), UriKind.Relative));

                 }
                 catch (Exception ex)
                 {

                 }
             }
//       
    }

   
}
