﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ModifyMenuItems : Page
    {
        EditorContext context = new EditorContext();
        ObservableCollection<BMA.MiddlewareApp.Web.MenuItem> menuItemsList;
        ObservableCollection<DummyDataRow> grids;
        private ObservableCollection<DisplayDefinition> ViewList = new ObservableCollection<DisplayDefinition>();
        int menuID;
        public ModifyMenuItems()
        {

            GetViews();
            this.Resources.Add("ViewList", ViewList);
            
            InitializeComponent();
            LoadMenu();
            menuID = ViewMenuStructure.menuStructure.ID;
           
        }


        private void GetViews()
        {

            LoadOperation<DisplayDefinition> loadOp = context.Load(context.GetDisplayDefinitionsQuery(), CallbackViews, null);


        }
        private void CallbackViews(LoadOperation<DisplayDefinition> result)
        {


            if (result != null)
            {
                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                {
                    ObservableCollection<DisplayDefinition> thisviewList = new ObservableCollection<DisplayDefinition>(result.Entities.Where(v => v.CreatedUser_ID == Globals.CurrentUser.ID).OrderBy(si => si.Description));

                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }
                else
                {
                    ObservableCollection<DisplayDefinition> thisviewList = new ObservableCollection<DisplayDefinition>(result.Entities);

                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }

                //GetGridDefinition(displayDefinition);
                MenuItems();
            }
        }

        private void MenuItems()
        {
            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
            {
                LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadOperation = context.Load(context.GetMenuItemsQuery().Where(x => x.MenuStructure_ID == menuID && x.IsParent == false && x.User_ID == Globals.CurrentUser.ID), CallbackGridDef, null);
            }
            else
            {
                LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadOperation = context.Load(context.GetMenuItemsQuery().Where(x => x.MenuStructure_ID == menuID && x.IsParent == false), CallbackGridDef, null);
            }
        }

        private void LoadMenu()
        {
            textMenu.Text = ViewMenuStructure.menuStructure.MenuName;
            LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => m.MenuStructure_ID == ViewMenuStructure.menuStructure.ID), Callback, null);
        }

        private void Callback(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadMenus)
        {
            if (loadMenus != null)
            {
                menuItemsList = new ObservableCollection<BMA.MiddlewareApp.Web.MenuItem>();
                foreach(var menu in loadMenus.Entities)
                {
                    menuItemsList.Add(menu);
                }

                List<BMA.MiddlewareApp.Web.MenuItem> orig = loadMenus.Entities.ToList().OrderBy(x => x.SequenceNumber).ToList();

                this.tvHierarchyView.ItemsSource = orig;
                LoadData(orig);
            }
        }

        private void LoadData(List<BMA.MiddlewareApp.Web.MenuItem> elements)
        {
            List<Category> categories = new List<Category>();
            categories = this.GetCategories(elements);
            this.tvHierarchyView.ItemsSource = categories;
        }




        private List<Category> GetCategories(List<BMA.MiddlewareApp.Web.MenuItem> element)
        {
            int id = 0;
            return (from category in element.Where(p => p.ParentMenu_ID == 0)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        ImageUrl = category.ImageUrl,
                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private List<Category> GetChild(List<BMA.MiddlewareApp.Web.MenuItem> element, int menuLevel)
        {

            return (from category in element.Where(p => p.ParentMenu_ID == menuLevel)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        ImageUrl = category.ImageUrl,
                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }
        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            int parentID = 0;
           int sequence = 0;
            Category selectedItem = tvHierarchyView.SelectedItem as Category;
            if (selectedItem != null)
            {
                if (menuItemsList != null)
                {
                    var query = (from m in menuItemsList
                                 where m.ID == selectedItem.ID
                                 select m).FirstOrDefault();
                    if (query != null)
                    {
                        if (query.IsParent)
                        {
                            parentID = query.ID;

                            sequence = (from m in menuItemsList
                                        where m.ParentMenu_ID == query.ID
                                        select m).ToList().Count();
                        }

                    }


                }
            }
            else
            {
                if (menuItemsList != null)
                {
                    sequence = (from m in menuItemsList
                                where m.IsParent == true && m.ParentMenu_ID == 0
                                select m).ToList().Count();
                }

                // if(selectedItem.)
            }

            AddMenuItem menu = new AddMenuItem(menuID, parentID, sequence);
            menu.Closed +=new EventHandler(menu_Closed);
            menu.Show();
        }


        private void GetDisplayDefinitions(object sender, RoutedEventArgs e)
        {

            LoadOperation<DisplayDefinition> loadOp = context.Load(context.GetDisplayDefinitionsQuery(), CallbackViews, null);


        }


        private void GetDisplayDefinitions1()
        {

            LoadOperation<DisplayDefinition> loadOp = context.Load(context.GetDisplayDefinitionsQuery(), CallbackViews, null);


        }



        private void CallbackGridDef(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadOperation)
        {
            if (loadOperation != null)
            {
                grids = new ObservableCollection<DummyDataRow>();
                BMA.MiddlewareApp.Web.MenuItem gridDef = loadOperation.Entities.FirstOrDefault();

                foreach (BMA.MiddlewareApp.Web.MenuItem grid in loadOperation.Entities)
                {
                    DummyDataRow test = new DummyDataRow();
                    DisplayDefinition display = new DisplayDefinition();
                    if ((grid.DisplayDefinition_ID != null) || (grid.DisplayDefinition_ID == 0))
                    {
                        display.ID = grid.DisplayDefinition_ID.Value;
                    }
                    else
                    {
                        display.ID = 0;
                    }
                    var vw = (from v in ViewList where v.ID == display.ID select v).FirstOrDefault();
                    if (vw != null)
                    {
                        display.Description = vw.Description;
                    }
                    else
                    {
                        display.Description = "";
                    }
                   
                    test.Name = grid.DisplayName;
                    test.ViewName = display;
                    test.ID = grid.ID;
                    grids.Add(test);
                }
                //lstForms.SelectedValue = gridDef.DisplayDefinition.FormType_ID.Value;
                dgMyDataGrid.ItemsSource = grids;
            }
        }



        private void CallbackRefresh(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadOperation)
        {
            if (loadOperation != null)
            {
              //  grids = new ObservableCollection<DummyDataRow>();
                BMA.MiddlewareApp.Web.MenuItem gridDef = loadOperation.Entities.FirstOrDefault();

                foreach (BMA.MiddlewareApp.Web.MenuItem grid in loadOperation.Entities)
                {
                    DummyDataRow test = new DummyDataRow();
                    DisplayDefinition display = new DisplayDefinition();
                    if ((grid.DisplayDefinition_ID != null) || (grid.DisplayDefinition_ID == 0))
                    {
                        display.ID = grid.DisplayDefinition_ID.Value;
                    }
                    else
                    {
                        display.ID = 0;
                    }
                    var vw = (from v in ViewList where v.ID == display.ID select v).FirstOrDefault();
                    if (vw != null)
                    {
                        display.Description = vw.Description;
                    }
                    else
                    {
                        display.Description = "";
                    }

                    test.Name = grid.DisplayName;
                    test.ViewName = display;
                    test.ID = grid.ID;
                    if (!(grids.Contains(((grids.Where(x => x.ID == test.ID)).FirstOrDefault()))))
                    grids.Add(test);
                }
                //lstForms.SelectedValue = gridDef.DisplayDefinition.FormType_ID.Value;
                //dgMyDataGrid.ItemsSource = grids;
            }
        }
        private void dgMyDataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            dgMyDataGrid.BeginEdit();
        }


        private void menu_Closed(object sender, EventArgs e)
        {
            LoadMenu();
            MenuItems();

           // LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadOperation = context.Load(context.GetMenuItemsQuery().Where(x => x.MenuStructure_ID == menuID && x.IsParent == false), CallbackRefresh, null);

        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {

            int parentID = 0;
           int sequence = 0;
            Category selectedItem = tvHierarchyView.SelectedItem as Category;
            if (selectedItem != null)
            {
                if (menuItemsList != null)
                {
                    var query = (from m in menuItemsList
                                 where m.ID == selectedItem.ID
                                 select m).FirstOrDefault();
                    if (query != null)
                    {
                      
                            parentID = query.ParentMenu_ID;

                            sequence = (from m in menuItemsList
                                        where m.ParentMenu_ID == parentID
                                        select m).ToList().Count();
                       

                        EditMenuItem menu = new EditMenuItem(query, menuID, parentID, sequence);
                        menu.Closed += new EventHandler(menu_Closed);
                        menu.Show();
                    }



                }
            }
           
            

            }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Category selectedItem = tvHierarchyView.SelectedItem as Category;
            if (selectedItem == null)
                return;

            CustomMessage customMessage = new CustomMessage("Do you want to delete  " + selectedItem.Name + " Menu item?", CustomMessage.MessageType.Confirm);

            customMessage.OKButton.Click += (obj, args) =>
             {

                 if (selectedItem != null)
                 {
                     if (menuItemsList != null)
                     {
                         var query = (from m in menuItemsList
                                      where m.ID == selectedItem.ID
                                      select m).FirstOrDefault();
                         if (query != null)
                         {

                             context.MenuItems.Remove(query);
                             context.SubmitChanges(submit => { if (submit.Error == null) { Message.InfoMessage("Successfully Deleted"); LoadMenu(); MenuItems(); } else { Message.ErrorMessage(submit.Error.Message); } }, null);
                         }
                     }
                 }
             };
            customMessage.Show();
        }

        

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<DataTableService.ListMenu> menuItemsList = new ObservableCollection<DataTableService.ListMenu>();

          
            foreach (DataGridRow rowItem in DataGridExtensions.GetRows(dgMyDataGrid))
            {
                DummyDataRow data = rowItem.DataContext as DummyDataRow;
                if(data.ViewName.Description != "")
                {

                    var query = (from d in ViewList where d.Description == data.ViewName.Description select d).FirstOrDefault();
                    DataTableService.ListMenu menu = new DataTableService.ListMenu();
                    menu.MenuItemID = data.ID;
                    menu.DisplayDefinitionID = query.ID;
                    menuItemsList.Add(menu);
                }


            }

            if (menuItemsList.Count<DataTableService.ListMenu>() != 0)
            {
                var ws = WCF.GetService();
                ws.UpdateMenuItemsCompleted +=new EventHandler<DataTableService.UpdateMenuItemsCompletedEventArgs>(ws_UpdateMenuItemsCompleted);
                ws.UpdateMenuItemsAsync(menuItemsList);
            }
        }

        void ws_UpdateMenuItemsCompleted(object sender, DataTableService.UpdateMenuItemsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else
            {
                if(e.Result)
                    Message.InfoMessage("Changes Saved");

                else
                  Message.ErrorMessage("An error occurred while processing your request!");
            }
        }



        private void radTreeView_DragEnded(object sender, Telerik.Windows.Controls.RadTreeViewDragEndedEventArgs e)
        {
            // Get the dragged items.
            Collection<System.Object> draggedItems = e.DraggedItems;
            // Get the drop position.
            Telerik.Windows.Controls.DropPosition dropPosition = e.DropPosition;
            switch (dropPosition)
            {
                case DropPosition.After:
                    MessageBox.Show("After");
                    break;
                case DropPosition.Before:
                    MessageBox.Show("Before");
                    break;
                case DropPosition.Inside:
                    MessageBox.Show("Inside");
                    break;
            }
            // Get is canceled
            bool isCanceled = e.IsCanceled;
            // Target drop item
            RadTreeViewItem targetDropItem = e.TargetDropItem;
            if (targetDropItem.Header.ToString() == "Exit")
            {
                // Do something
                Message.InfoMessage("Moved");
            }
        }

    }



    public class DummyDataRow
    {
        public int ID { get; set; }       
        public string Name { get; set; }
        public DisplayDefinition ViewName { get; set; }
    }
}
