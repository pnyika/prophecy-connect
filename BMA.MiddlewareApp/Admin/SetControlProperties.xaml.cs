﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;
using System.ServiceModel.DomainServices.Client;
using System.Drawing;
using Telerik.Windows.Controls;
using AmCharts.Windows.QuickCharts;



namespace BMA.MiddlewareApp.Admin
{
    public partial class SetControlProperties : ChildWindow
    {
        public delegate void ObjectDefinitionCreatedEventHandler(object sender, PropertiesCreatedEventArgs e);
        public event ObjectDefinitionCreatedEventHandler ObjectDefinitionCreated;
        public event EventHandler ObjectDefinitionCanceled;
        public static string XmlString;
        private int formID;
        public SetControlProperties(int formid, double x, double y, double height, double width)
        {
            InitializeComponent();
            chkUseTimer.Visibility = Visibility.Collapsed;
            tmrInterval.Visibility = Visibility.Collapsed;
            lblInterval.Visibility = Visibility.Collapsed;
            XmlString = null;
            txtX.Text = x.ToString();
            txtY.Text = y.ToString();
            txtHeight.Text = height.ToString();
            txtWidth.Text = width.ToString();
            formID = formid;
            loadTypes();
            loadColors();
            loadFontFamily();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Save();
            }
            catch
            {
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            ObjectDefinitionCanceled(this,null);
            this.DialogResult = false;
        }

        private void Save()
        {
            EditorContext context = new EditorContext();
            ControlType control = ddlControlType.SelectedItem as ControlType;
            if(control == null)
              Message.ErrorMessage("Select object type!");
            else
            {
                if (XmlString == null)
                {
                    XmlString = GetXaml();
                }
            ObjectDefintion obj = new ObjectDefintion();
            obj.ObjectForm_ID = formID;
            obj.ObjectName = txtName.Text.ToString();
            obj.ObjectType = control.Value;
            obj.PositionX = Convert.ToDouble(txtX.Text.ToString());
            obj.PositionY = Convert.ToDouble(txtY.Text.ToString());
                decimal hght = Convert.ToDecimal(txtHeight.Text.ToString());
            obj.SizeHeight = Convert.ToInt32(hght);
            decimal wdth = Convert.ToDecimal(txtWidth.Text.ToString());
            obj.SizeWidth = Convert.ToInt32(wdth);
            obj.Text = txtText.Text.ToString();
            obj.IsDeleted = false;
            if (control.Value =="RadButton")
              {
                  if (control.Display == "Timer")
                  {
                      obj.UseTimer = 2;
                      obj.Interval = Convert.ToInt32(tmrInterval.Value.Value);
                  }
                  else if (control.Display == "Button")
                  {
                      if (chkUseTimer.IsChecked.Value == true)
                      {
                          obj.UseTimer = 1;
                          obj.Interval = Convert.ToInt32(tmrInterval.Value.Value);
                      }
                      else
                      {
                          obj.UseTimer = 0;
                      }
                  }
              }
            context.ObjectDefintions.Add(obj);
            context.SubmitChanges(submit => { if (!submit.HasError)
            {
                ObjectProperty property = new ObjectProperty();

              

                property.XmlString = XmlString;
                property.ObjectDefinition_ID = obj.ID;
                context.ObjectProperties.Add(property);
                context.SubmitChanges(sub => { if (!sub.HasError)
            {
                ObjectDefinitionCreated(this, new PropertiesCreatedEventArgs(obj));

            }
                else
                { Message.ErrorMessage(sub.Error.Message); }
                },
                                                     null);
            
            
            
            } 
                                                else
                                                    { Message.ErrorMessage(submit.Error.Message); } },
                                                     null);
               
            }
        }

        private void loadTypes()
        {
            List<ControlType> data = new List<ControlType>();
            data.Add(new ControlType{Value ="TextBlock", Display= "TextBlock"});
            data.Add(new ControlType{Value ="TextBox", Display= "TextBox"});
            data.Add(new ControlType { Value = "RadDatePicker", Display = "DatePicker" });
            data.Add(new ControlType{Value ="RadButton", Display= "Button"});
            data.Add(new ControlType { Value = "RadButton", Display = "Timer" });
            data.Add(new ControlType { Value = "RadComboBox", Display = "DropDown" });
            data.Add(new ControlType { Value = "RadGridView", Display = "Data Grid" });
           // data.Add(new ControlType { Value = "RadCartesianChart", Display = "Graph/Chart" });
            data.Add(new ControlType { Value = "SerialChart", Display = "Graph" });
           // data.Add(new ControlType { Value = "PieChart", Display = "PieChart" });
            //data.Add(new ControlType { Value = "Label", Display = "Label" });
            ddlControlType.ItemsSource = data;
            ddlControlType.DisplayMemberPath = "Display";
            ddlControlType.SelectedValuePath = "Value";
            ddlControlType.ItemsSource = data;
        }

        private void loadColors()
        {
            List<ControlType> data = new List<ControlType>();

            System.Array colorsArray = Enum.GetValues(typeof(KnownColor));
              KnownColor[] allColors = new KnownColor[colorsArray.Length];

            Array.Copy(colorsArray, allColors, colorsArray.Length);

            foreach (KnownColor knowColor in allColors)
            {
                
                data.Add(new ControlType { Value = knowColor.ToString(), Display = knowColor.ToString() });
             

            }

           
            //ddlColor.ItemsSource = data;
            //ddlColor.DisplayMemberPath = "Display";
            //ddlColor.SelectedValuePath = "Value";
            //ddlColor.ItemsSource = data;

            //ddlBgColor.ItemsSource = data;
            //ddlBgColor.DisplayMemberPath = "Display";
            //ddlBgColor.SelectedValuePath = "Value";
            //ddlBgColor.ItemsSource = data;
        }
 
    

        private void loadFontFamily()
        {
            List<ControlType> data = new List<ControlType>();
            //         System.Array familiesArray = Enum.GetValues(typeof(Fonts));
            //          FontFamily[] allFamilies = new FontFamily[familiesArray.Length];
            //         foreach ( FontFamily oneFontFamily in FontFamily.Families )
            //{
            //    listBox1.Items.Add(oneFontFamily.Name);
            //}
        
            //        //Loop through all the fonts in the system.

            //          foreach (var family in Fonts.SystemTypefaces)
            //        {
            //            data.Add(new ControlType { Value = family.ge()., Display = family.Source });
               
            //        }            
            // data.Add(new ControlType { Value = "Arial", Display = "Arial" });

            //ddlFamily.ItemsSource = data;
            //ddlFamily.DisplayMemberPath = "Display";
            //ddlFamily.SelectedValuePath = "Value";
            //ddlFamily.ItemsSource = data;
        }

        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            Control newControl;

             ControlType control = ddlControlType.SelectedItem as ControlType;
             if (control == null)
                 Message.ErrorMessage("Select object type!");
             else
             {

                double PositionX = Convert.ToDouble(txtX.Text.ToString());
                double PositionY = Convert.ToDouble(txtY.Text.ToString());
                 decimal hght = Convert.ToDecimal(txtHeight.Text.ToString());
                 int SizeHeight = Convert.ToInt32(hght);
                 decimal wdth = Convert.ToDecimal(txtWidth.Text.ToString());
                int SizeWidth = Convert.ToInt32(wdth);
                 string Text = txtText.Text.ToString();
                 string Name = txtName.Text.ToString();
            
                if (control.Value == "TextBlock")
                {
                    System.Windows.Controls.Label txtbox = new System.Windows.Controls.Label();
                    txtbox.Height = SizeHeight;
                    txtbox.Width = SizeWidth;
                    txtbox.Name = Name;
                    txtbox.Content = Text;

                    newControl = txtbox as Control;
                    AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                    window.Show();
                }
                if (control.Value == "TextBox")
                {
                    TextBox txtbox = new TextBox();
                    txtbox.Height = SizeHeight;
                    txtbox.Width = SizeWidth;
                    txtbox.Name = Name;
                    txtbox.Text = Text;

                    newControl = txtbox as Control;
                    AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                    window.Show();
                   
                  
                    
                }
                
                if (control.Value == "RadButton")
                {

                    RadButton button = new RadButton();
                    button.Height = SizeHeight;
                    button.Width = SizeWidth;
                    button.Name = Name;
                    button.Content = Text;
                    newControl = button as Control;
                    AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                    window.Show();
 

                }

                if (control.Value == "RadComboBox")
                {

                    RadComboBox button = new RadComboBox();
                    button.Height = SizeHeight;
                    button.Width = SizeWidth;
                    button.Name = Name;
                  //  button.Content = Text;
                    newControl = button as Control;
                    AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                    window.Show();


                }

                if (control.Value == "RadDatePicker")
                {

                    RadDatePicker button = new RadDatePicker();
                    button.Height = SizeHeight;
                    button.Width = SizeWidth;
                    button.Name = Name;
                    //  button.Content = Text;
                    newControl = button as Control;
                    AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                    window.Show();


                }
                if (control.Value == "RadGridView")
                {
                    RadGridView grid = new RadGridView();
                    grid.Height = SizeHeight;
                    grid.Width = SizeWidth;
                    grid.Name = Name;
                    newControl = grid as Control;
                    AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                    window.Show();
                }
                
               
                    if (control.Value == "RadCartesianChart")
                {
                    RadCartesianChart chart = new RadCartesianChart();
                    chart.Height = SizeHeight;
                    chart.Width = SizeWidth;
                    chart.Name = Name;
                    newControl = chart as Control;
                    AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                    window.Show();
                }

                    if (control.Value == "SerialChart")
                    {
                        SerialChart serialChart = new SerialChart();
                        serialChart.Height = SizeHeight;
                        serialChart.Width = SizeWidth;
                        serialChart.Name = Name;
                        newControl = serialChart as Control;
                        AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                        window.Show();
                    }

                    if (control.Value == "PieChart")
                    {
                        PieChart pieChart = new PieChart();
                        pieChart.Height = SizeHeight;
                        pieChart.Width = SizeWidth;
                        pieChart.Name = Name;
                        newControl = pieChart as Control;
                        AddPropertyWindow window = new AddPropertyWindow(newControl, control.Value);
                        window.Show();
                    }
             }
            


        }



        private string GetXaml()
        {
            string xmlString = "";
            string objectType;
            Control newControl;
            Control compareControl;

            ControlType control = ddlControlType.SelectedItem as ControlType;
            if (control == null)
                Message.ErrorMessage("Select object type!");
            else
            {

                double PositionX = Convert.ToDouble(txtX.Text.ToString());
                double PositionY = Convert.ToDouble(txtY.Text.ToString());
                decimal hght = Convert.ToDecimal(txtHeight.Text.ToString());
                int SizeHeight = Convert.ToInt32(hght);
                decimal wdth = Convert.ToDecimal(txtWidth.Text.ToString());
                int SizeWidth = Convert.ToInt32(wdth);
                string Text = txtText.Text.ToString();
                string Name = txtName.Text.ToString();

                if (control.Value == "TextBlock")
                {
                    System.Windows.Controls.Label txtlbl= new System.Windows.Controls.Label();
                    txtlbl.Height = SizeHeight;
                    txtlbl.Width = SizeWidth;
                    txtlbl.Name = Name;
                    txtlbl.Content = Text;

                    txtlbl.Background = new SolidColorBrush(Colors.White);
                    newControl = txtlbl as Control;
                    objectType = "Label";

                    System.Windows.Controls.Label newlabel= new System.Windows.Controls.Label();
                    xmlString = ControlManager.CreateControlXmlString(newControl, newlabel, objectType);
                    
                    
                }
                if (control.Value == "TextBox")
                {
                    TextBox txtbox = new TextBox();
                    txtbox.Height = SizeHeight;
                    txtbox.Width = SizeWidth;
                    txtbox.Name = Name;
                    txtbox.Text = Text;

                    newControl = txtbox as Control;
                    objectType = "TextBox";
                    TextBox newTxtbox = new TextBox();
                    xmlString = ControlManager.CreateControlXmlString(newControl, newTxtbox, objectType);


                }

                if (control.Value == "RadButton")
                {

                    RadButton button = new RadButton();
                    button.Height = SizeHeight;
                    button.Width = SizeWidth;
                    button.Name = Name;
                    button.Content = Text;
                    newControl = button as Control;
                    objectType = "RadButton";

                    RadButton newButton = new RadButton();
                     xmlString = ControlManager.CreateControlXmlString(newControl, newButton, objectType);
                }
                if (control.Value == "RadComboBox")
                {

                    RadComboBox button = new RadComboBox();
                    button.Height = SizeHeight;
                    button.Width = SizeWidth;
                    button.Name = Name;
                    //button.Content = Text;
                    newControl = button as Control;
                    objectType = "RadComboBox";

                    RadComboBox newButton = new RadComboBox();
                    xmlString = ControlManager.CreateControlXmlString(newControl, newButton, objectType);
                }

                if (control.Value == "RadDatePicker")
                {

                    RadDatePicker button = new RadDatePicker();
                    button.Height = SizeHeight;
                    button.Width = SizeWidth;
                    button.Name = Name;
                    //button.Content = Text;
                    newControl = button as Control;
                    objectType = "RadDatePicker";

                    RadDatePicker newButton = new RadDatePicker();
                    xmlString = ControlManager.CreateControlXmlString(newControl, newButton, objectType);
                }

                if (control.Value == "RadGridView")
                {
                    RadGridView grid = new RadGridView();
                    grid.Height = SizeHeight;
                    grid.Width = SizeWidth;
                    grid.Name = Name;
                    newControl = grid as Control;
                    objectType = "RadGridView";
                    RadGridView newGrid = new RadGridView();
                     xmlString = ControlManager.CreateControlXmlString(newControl, newGrid, objectType);
                }


                if (control.Value == "RadCartesianChart")

                {
                    RadCartesianChart chart = new RadCartesianChart();
                    chart.Height = SizeHeight;
                    chart.Width = SizeWidth;
                    chart.Name = Name;
                    newControl = chart as Control;
                    objectType = "RadCartesianChart";
                    RadCartesianChart newGrid = new RadCartesianChart();
                    xmlString = ControlManager.CreateControlXmlString(newControl, newGrid, objectType);
                }


                if (control.Value == "SerialChart")
                {
                    SerialChart serialChart = new SerialChart();
                    serialChart.Height = SizeHeight;
                    serialChart.Width = SizeWidth;
                    serialChart.Name = Name;
                    newControl = serialChart as Control;
                    objectType = "SerialChart";
                    SerialChart newserialChart = new SerialChart();
                    xmlString = ChartControlManager.CreateControlXmlString(newControl, newserialChart, objectType);
                }

                if (control.Value == "PieChart")
                {
                    PieChart pieChart = new PieChart();
                    pieChart.Height = SizeHeight;
                    pieChart.Width = SizeWidth;
                    pieChart.Name = Name;
                    newControl = pieChart as Control;
                    objectType = "PieChart";
                    PieChart newpieChart = new PieChart();
                    xmlString = ChartControlManager.CreatePieControlXmlString(newControl, newpieChart, objectType);
                }


            }


           

            return xmlString;

        }

        private void ddlControlType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            chkUseTimer.IsChecked = false;
            chkUseTimer.Visibility = Visibility.Collapsed;
            tmrInterval.Visibility = Visibility.Collapsed;
            lblInterval.Visibility = Visibility.Collapsed;
            txtText.Text = "";
            tmrInterval.Value = 4;
            ControlType control = ddlControlType.SelectedItem as ControlType;
            if (control != null)
            {
                if (control.Value == "RadButton")
                {
                    if (control.Display == "Button")
                    {
                        chkUseTimer.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        tmrInterval.Visibility = Visibility.Visible;
                        lblInterval.Visibility = Visibility.Visible;
                        txtText.Text = "Timer";
                       
                    }
                    //tmrInterval.Visibility = Visibility.Collapsed;
                    //lblInterval.Visibility = Visibility.Collapsed;
                }
                else
                {
                    chkUseTimer.Visibility = Visibility.Collapsed;
                    tmrInterval.Visibility = Visibility.Collapsed;
                    lblInterval.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void chkUseTimer_Checked(object sender, RoutedEventArgs e)
        {
            if (chkUseTimer.IsChecked.Value)
            {
                tmrInterval.Visibility = Visibility.Visible;
                lblInterval.Visibility = Visibility.Visible;
            }
            else
            {
                tmrInterval.Visibility = Visibility.Collapsed;
                lblInterval.Visibility = Visibility.Collapsed;
            }
           
        }
    }

    public class PropertiesCreatedEventArgs
    {
        private ObjectDefintion objectDefintion = null;

        public PropertiesCreatedEventArgs() { }
        public PropertiesCreatedEventArgs(ObjectDefintion objectDefintion)
        {
            this.objectDefintion = objectDefintion;
        }
        
        public ObjectDefintion CreatedObject { get { return objectDefintion; } }
    }

    public class ControlType
    {
        public string Value { get; set; }
        public string Display { get; set; }
        public int index { get; set; }
    }

}

