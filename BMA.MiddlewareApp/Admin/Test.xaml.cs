﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace BMA.MiddlewareApp.Admin
{
    public partial class Test : Page
    {
        
        double _mouseOrigX;
        double _mouseOrigY;
        bool _mouseMove = false;
        string _mouseResizeEdge = "";

        public Test()
        {
            InitializeComponent();
        }

        public string Text
        {
            get
            {
                return mainTextBlock.Text;
            }
            set
            {
                mainTextBlock.Text = value;
            }
        }

        public void BeginBorderGlow()
        {
            glowBorderStoryboard.Begin();
        }

        public void StopBorderGlow()
        {
            glowBorderStoryboard.Stop();
        }



        private void LayoutRoot_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Canvas parent = (Canvas)this.Parent;
            var mousePosition = e.GetPosition(parent);

            _mouseOrigX = mousePosition.X;
            _mouseOrigY = mousePosition.Y;

            ((FrameworkElement)sender).CaptureMouse();

            _mouseResizeEdge = GetEdgeMouseIsOver(this, e);
            if (_mouseResizeEdge == "")
            {
                //If the mouse is not over an edge, we drag the field
                _mouseMove = true;
            }
            else
            {
                //If we're over an edge, we don't want to drag the box - we want to resize the object
                _mouseMove = false;
            }

        }

        private void LayoutRoot_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ((FrameworkElement)sender).ReleaseMouseCapture();
        }

        private void LayoutRoot_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseMove)
            {
                HandleDrag(e);
            }
            else if (_mouseResizeEdge != "")
            {
                HandleResize(e);
            }
            else
            {
                //If the object is not in drag mode, show arrows if needed
                switch (GetEdgeMouseIsOver(this, e))
                {
                    case "NW":
                    case "SE":
                        Cursor = Cursors.SizeNWSE;
                        break;
                    case "SW":
                    case "NE":
                        Cursor = Cursors.SizeNESW;
                        break;
                    case "N":
                    case "S":
                        Cursor = Cursors.SizeNS;
                        break;
                    case "E":
                    case "W":
                        Cursor = Cursors.SizeWE;
                        break;
                    default:
                        Cursor = Cursors.Hand;
                        break;
                }

            }
        }

        private void HandleResize(MouseEventArgs e)
        {
            Canvas parent = (Canvas)this.Parent;
            var mousePosition = e.GetPosition(parent);

            double mouseChangeX = mousePosition.X - _mouseOrigX;
            double mouseChangeY = mousePosition.Y - _mouseOrigY;

            if (_mouseResizeEdge.Contains("N"))
            {
                var newY = mouseChangeY + (double)this.GetValue(Canvas.TopProperty);

                var newHeight = this.Height - mouseChangeY;
                if (newHeight >= 20)
                {
                    this.Height = newHeight;
                    this.SetValue(Canvas.TopProperty, newY);
                    _mouseOrigY = mousePosition.Y;
                }
                else
                {
                    var diffTo20 = this.Height - 20;
                    _mouseOrigY = _mouseOrigY + diffTo20;
                    this.Height = 20;

                    newY = newY - mouseChangeY + diffTo20;
                    this.SetValue(Canvas.TopProperty, newY);
                }
            }
            else if (_mouseResizeEdge.Contains("S"))
            {
                var newHeight = this.Height + mouseChangeY;
                if (newHeight >= 20)
                {
                    this.Height = newHeight;
                    _mouseOrigY = mousePosition.Y;
                }
                else
                {
                    var diffTo20 = this.Height - 20;
                    _mouseOrigY = _mouseOrigY - diffTo20;
                    this.Height = 20;
                }

            }

            if (_mouseResizeEdge.Contains("W"))
            {
                var newX = mouseChangeX + (double)this.GetValue(Canvas.LeftProperty);
                this.SetValue(Canvas.LeftProperty, newX);
                var newWidth = this.Width - mouseChangeX;
                if (newWidth > 20)
                {
                    this.Width = newWidth;
                    _mouseOrigX = mousePosition.X;
                }
                else
                {
                    var diffTo20 = this.Width - 20;
                    _mouseOrigX = _mouseOrigX + diffTo20;
                    this.Width = 20;

                    newX = newX - mouseChangeX + diffTo20;
                    this.SetValue(Canvas.LeftProperty, newX);
                }

            }
            else if (_mouseResizeEdge.Contains("E"))
            {
                var newWidth = this.Width + mouseChangeX;
                if (newWidth > 20)
                {
                    this.Width = newWidth;
                    _mouseOrigX = mousePosition.X;
                }
                else
                {
                    var diffTo20 = this.Width - 20;
                    _mouseOrigX = _mouseOrigX - diffTo20;
                    this.Width = 20;
                }
            }
        }

        private void HandleDrag(MouseEventArgs e)
        {
            Canvas parent = (Canvas)this.Parent;
            var mousePosition = e.GetPosition(parent);

            double x = mousePosition.X - _mouseOrigX;
            double y = mousePosition.Y - _mouseOrigY;

            x += (double)this.GetValue(Canvas.LeftProperty);
            y += (double)this.GetValue(Canvas.TopProperty);

            this.SetValue(Canvas.LeftProperty, x);
            this.SetValue(Canvas.TopProperty, y);

            _mouseOrigX = mousePosition.X;
            _mouseOrigY = mousePosition.Y;
        }




        private string GetEdgeMouseIsOver(UIElement element, MouseEventArgs e)
        {
            //If the object is not in drag mode, show arrows if needed
            var mouseInBorder = e.GetPosition(element);
            bool northRow =
                (mouseInBorder.Y < 7);
            bool southRow =
                (mouseInBorder.Y > (this.Height - 8));
            bool westCol =
                (mouseInBorder.X < 7);
            bool eastCol =
                (mouseInBorder.X > (this.Width - 8));

            if (northRow && westCol)
            {
                return "NW";
            }
            else if (northRow && eastCol)
            {
                return "NE";
            }
            else if (southRow && westCol)
            {
                return "SW";
            }
            else if (southRow && eastCol)
            {
                return "SE";
            }
            else if (southRow)
            {
                return "S";
            }
            else if (northRow)
            {
                return "N";
            }
            else if (eastCol)
            {
                return "E";
            }
            else if (westCol)
            {
                return "W";
            }

            return "";
        }


        private void LayoutRoot_LostMouseCapture(object sender, MouseEventArgs e)
        {
            _mouseMove = false;
            _mouseResizeEdge = "";
            _mouseOrigX = 0;
            _mouseOrigY = 0;
        }

        private void LayoutRoot_MouseEnter(object sender, MouseEventArgs e)
        {
        }

        private void LayoutRoot_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_mouseResizeEdge == "")
                Cursor = Cursors.Arrow;
        }


    }
}