﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;

namespace BMA.MiddlewareApp.Admin
{
    public partial class FunctionParameters : ChildWindow
    {
        public FunctionParameters()
        {
            InitializeComponent();

            functionForm.SetParameters += (se, ev) => { GotoParameterForm(); };
            functionForm.Close += (se, ev) => { this.DialogResult = false; };
          //  parameterForm.Successful += (se, ev) => { this.DialogResult = false; };

        }

        void functionForm_SetParameters(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            Flipper.Page2Visible = true;
        }

        


        private void GotoParameterForm()
        {
            SetFunctionParameter child = new SetFunctionParameter(); 
         
           child.Show();

           // Flipper.Page2Visible = true;
        }
       
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

       
    }
}

