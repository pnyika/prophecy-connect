﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using SilverlightMessageBox;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewGroups : Page
    {
        public static ObservableCollection<UserInformation> userInformationList;
        EditorContext context = new EditorContext();
        public ViewGroups()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void MyRadDomainDataSource_SubmittedChanges(object sender, Telerik.Windows.Controls.DomainServices.DomainServiceSubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Avoids displaying errors, when deletion is prevented by DataBase constraints
                e.MarkErrorAsHandled();
            }
            else
            {
                Message.InfoMessage("Successfully saved!");
            }
        }

        private void btnAddGroup_Click(object sender, RoutedEventArgs e)
        {
            Group group = new Group();

            AddViewGroup addGroup = new AddViewGroup(group);
            addGroup.Show();
        }

        private void btnModifyGroup_Click(object sender, RoutedEventArgs e)
        {
          Group group =   radGridView.SelectedItem as Group;
            if(group ==null)
                return;

           
            AddViewGroup addGroup = new AddViewGroup(group);
            addGroup.Show();
        }

        private void btnFilterViewGroup_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;


            SetViewGroupFilter filterViewGroup = new SetViewGroupFilter(group);
            filterViewGroup.Show();
        }



        private void btnShowUsers_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;

         
            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(u=>u.Group_ID==group.ID), CallbackUsers, null);
        }
        private void ShowUsers()
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;


            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(u => u.Group_ID == group.ID), CallbackUsers, null);
        }
        private void CallbackUsers(LoadOperation<UserInformation> loadUsers)
        {
            userInformationList = new ObservableCollection<UserInformation>();

            if (loadUsers != null)
            {

                foreach (UserInformation user in loadUsers.Entities)
                {
                    userInformationList.Add(user);
                }

            }
            lstUsers.ItemsSource = userInformationList;
        }

        private void btnChangeUserMenu_Click(object sender, RoutedEventArgs e)
        {
            UserInformation user = lstUsers.SelectedItem as UserInformation;
            if (user == null)
                return;

            ChangeUserMenu change = new ChangeUserMenu(user);
            change.Closed += new EventHandler(user_Closed);
            change.Show();

        }
        void user_Closed(object sender, EventArgs e)
        {

            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;

            ShowUsers();
        }

        private void btnSelectUser_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;
            AssignUsersGroup assign = new AssignUsersGroup(group.ID);
            assign.Closed += new EventHandler(user_Closed);
            assign.Show();
        }

        private void btnUnassign_Click(object sender, RoutedEventArgs e)
        {

            UserInformation user = lstUsers.SelectedItem as UserInformation;
            if (user == null)
                return;

            user.Group_ID = null;

            context.SubmitChanges(s => { if (!s.HasError) {
                ShowUsers();
            
            } else { } }, null);


        }

        private void btnAssignModel_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;


            AddModelGroup addGroup = new AddModelGroup(group);
            addGroup.Show();
        }

        private void btnAddGroup_Click_1(object sender, RoutedEventArgs e)
        {
               AddGroup view = new AddGroup();
                 view.Closed +=new EventHandler(view_Closed);
                 view.Show();
        }

      
           
             void view_Closed(object sender, EventArgs e)
             {

                 usersDomainDataSource.Load();
                 
             }

    }
}
