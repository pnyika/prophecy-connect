﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SelectUsers : ChildWindow
    {
        EditorContext context = new EditorContext();
        public static bool newUser;
      private   int menuID;
        public SelectUsers(int menuid)
        {
            InitializeComponent();
            newUser = false;
            menuID = menuid;
        }



        private void GetUnAssignedUsers(object sender, RoutedEventArgs e)
        {

            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(m => m.MenuStructure_ID == null), CallbackUsers, null);
        }

        private void CallbackUsers(LoadOperation<UserInformation> loadUsers)
        {
            //if(ViewMenuStructure.userInformationList == null)
            //    ViewMenuStructure.userInformationList = new ObservableCollection<UserInformation>();

            if (loadUsers != null)
            {
                lstUsers.ItemsSource = loadUsers.Entities;
                //foreach (UserInformation user in loadUsers.Entities)
                //{
                //    ViewMenuStructure.userInformationList.Add(user);
                //}

            }
           
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.lstUsers.SelectedItems.Count == 0)
            
                return;



            if (ViewMenuStructure.userInformationList == null)
                ViewMenuStructure.userInformationList = new ObservableCollection<UserInformation>();
            ObservableCollection<int> userIDs = new ObservableCollection<int>();
            foreach (UserInformation user in this.lstUsers.SelectedItems)
            {
                userIDs.Add(user.ID);
                ViewMenuStructure.userInformationList.Add(user);
            }
            var ws = WCF.GetService();
            ws.UpdateUserMenuCompleted +=new EventHandler<DataTableService.UpdateUserMenuCompletedEventArgs>(ws_UpdateUserMenuCompleted);
            ws.UpdateUserMenuAsync(userIDs, menuID);

          
        }

        void ws_UpdateUserMenuCompleted(object sender, DataTableService.UpdateUserMenuCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                MessageBox.Show("Successfully Assigned");
                this.DialogResult = true;
            }
            else{
                MessageBox.Show("");
        }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            newUser = true;
             this.DialogResult = true;
           
        }
        }
    }


