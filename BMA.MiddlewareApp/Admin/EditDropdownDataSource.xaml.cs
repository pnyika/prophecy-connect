﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using System.Collections;
using SilverlightMessageBox;
using System.ServiceModel.DomainServices.Client;

namespace BMA.MiddlewareApp.Admin
{
    public partial class EditDropdownDataSource : ChildWindow 
    {
       
        IEnumerable _lookup;
        EditorContext context = new EditorContext();
        public event EventHandler SetParameters;
        public event EventHandler Close;
        private string connString = "";
        private  int fID =0;
        private int objID = 0;
        private string Db = "";
        ObservableCollection<ObjectDefintion> objectList = new ObservableCollection<ObjectDefintion>();
        ObjectFunction function = new ObjectFunction();
        public EditDropdownDataSource(int objectID, int formID)
        {
            InitializeComponent();
            this.OKButton.IsEnabled = false;
            getServer();
            objID = objectID;
            fID = formID;
            loadFunctionTypes();
            GetObjects();
           

        }
        private void getServer()
        {
            //busyIndicator1.IsBusy = true;
            App app = (App)Application.Current;
            LoadOperation<Server> loadOp = context.Load(context.GetServersQuery(), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {


            if (loadOp != null)
            {

                ddlDatabase.ItemsSource = loadOp.Entities.ToList();

            }
        }
        private void GetObjectFuction(int ID)
        {

            LoadOperation<ObjectFunction> loadOp = context.Load(context.GetObjectFunctionsQuery().Where(o => o.ObjectDefinition_ID == ID), CallbackObjectFunction, null);


        }


        string displayControlName = "";
        string storeProcName = "";
        bool isStore = false;
        string display = "";
        string value = "";
        void CallbackObjectFunction(LoadOperation<ObjectFunction> results)
        {

            if (results != null)
            {
                function = results.Entities.FirstOrDefault();
                ddlDatabase.SelectedValue = function.ServerID;
                
                DDLFunctionType.SelectedValue = function.FunctionType;
               
                connString = function.Server.ConnectionString;

             //   ddlDisplay.DisplayMemberPath ="" ;
              //  ddlValue.SelectedValuePath =function;
               display =function.DisplayField ;
              value= function.ValueField;
               
             
                if (function.FunctionType == "Sql Text")
                {
                    txtSQLText.Text = function.Query;

                    string sql = "SET FMTONLY ON " + txtSQLText.Text + "  SET FMTONLY OFF";
                    GetStoreProcColumns(sql);
                }
                else
                {
                    //ddlDatabase.SelectedValue = function.Database;

                    storeProcName = function.Query;
                    isStore = true;
                   
                        string sql = GetStoreProcSql(Db);
                       
                        Db = function.Server.DatabaseName;
                        connString = function.Server.ConnectionString;
                        GetStoreProcs(sql, Db, connString);
                }
                ddlDisplayControl.SelectedValue = function.TargetDisplayObject_ID;

                //"Password=" + txtpass.Password + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + ddlDatabase.SelectedValue.ToString() + ";Data Source=" + txtSource.Text;           

                //  LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackObjectFunctionParameter, null);



            }
        }

        private void loadFunctionTypes()
        {
            List<DB> types = new List<DB>();
            types.Add(new DB { Name = "Sql Text" });
            types.Add(new DB { Name = "Stored Procedure" });
            DDLFunctionType.ItemsSource = types;
        }


        private void GetObjects()
        {

            LoadOperation<ObjectDefintion> loadOp = context.Load(context.GetObjectDefintionsQuery().Where(o => o.ObjectForm_ID == fID), CallbackObject, null);


        }




        private void CallbackObject(LoadOperation<ObjectDefintion> results)
        {


            if (results != null)
            {

                ObservableCollection<ObjectDefintion> thisviewList = new ObservableCollection<ObjectDefintion>(results.Entities.Where(c => c.ObjectType == "RadGridView" || c.ObjectType == "SerialChart" || c.ObjectType == "PieChart"));
                foreach (var parameter in thisviewList)
                {

                    objectList.Add(parameter);
                }

                ddlDisplayControl.ItemsSource = objectList;

            }
            GetObjectFuction(objID);

        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            Save(sender, e);
            //SetParameters(this, null);
        }

        private bool checkParameters = false;
        private void Save(object sender, RoutedEventArgs e)
        {
            Server catalog = ddlDatabase.SelectedItem as Server;
            DB type = DDLFunctionType.SelectedItem as DB;

       
            ObjectDefintion obj = ddlDisplayControl.SelectedItem as ObjectDefintion;
         //   function.ConnectionString = connString;
          //  function.Database = ddlDatabase.SelectedValue.ToString();
                      if (type.Name == "Sql Text")
            {string sqlString = txtSQLText.Text;
                function.Query = sqlString;
                if (sqlString.Contains("where"))
                {
                    string[] Split = sqlString.Split(new Char[] { ' ' });
                    foreach (var item in Split)
                    {

                        if ((item.StartsWith("@"))|| (item.StartsWith("=@")))
                        {
                            checkParameters = true;
                        }
                    }

                }
            }
            else
            {
                DataObject proc = ddlStoreProc.SelectedItem as DataObject;
                function.Query = proc.GetFieldValue("Name").ToString();
                checkParameters = true;
            }
                      TempDisplay selectedValue = ddlValue.SelectedItem as TempDisplay;
                      TempDisplay selectedDisplay = ddlDisplay.SelectedItem as TempDisplay;
                      if (selectedValue != null)
                      {
                          function.ValueField = selectedValue.Value;
                      }
                      else
                      {
                          function.ValueField = "Value";
                      }

                      if (selectedDisplay != null)
                      {
                          function.DisplayField = selectedDisplay.Value;
                      }
                      else
                      {
                          function.DisplayField = "Display";

                      }
            function.ObjectDefinition_ID = objID;
            function.ServerID = catalog.ID;
            function.FunctionType = type.Name;
            if (obj != null)
                function.TargetDisplayObject_ID = obj.ID;
           // context.ObjectFunctions.Add(function);
            context.SubmitChanges(submit =>
            {
                if (!submit.HasError)
                {
                    if (checkParameters)
                    {
                        EditFunctionParameter.ID = function.ID;

                        EditFunctionParameter.objectFunction = function;
                       
                        EditFunctionParameter set = new EditFunctionParameter();
                        

                      
                        set.Show();
                        this.DialogResult = false;
                      //  Close(this, null);
                    }
                    else
                    {
                        Message.InfoMessage("Successfully set");
                       // Close(this, null);
                    }
                }
                else
                {
                    Message.ErrorMessage(submit.Error.Message);
                }

            }, null);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

      
        
        private void ddlDatabase_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
              
                this.OKButton.IsEnabled = true;
            }
            catch { }
        }

        private void DDLFunctionType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                DB type = DDLFunctionType.SelectedItem as DB;
                if (type != null)
                {
                    if (type.Name == "Sql Text")
                    {
                        sqlT.Visibility = Visibility.Visible;
                        txtSQLText.Visibility = Visibility.Visible;

                        ddlStoreProc.Visibility = Visibility.Collapsed;
                        strProc.Visibility = Visibility.Collapsed;
                    }
                    if (type.Name == "Stored Procedure")
                    {
                        sqlT.Visibility = Visibility.Collapsed;
                        txtSQLText.Visibility = Visibility.Collapsed;

                        ddlStoreProc.Visibility = Visibility.Visible;
                        strProc.Visibility = Visibility.Visible;
                        Server server = ddlDatabase.SelectedItem as Server;
                        string catalog = server.DatabaseName;
                        string sql = GetStoreProcSql(catalog);
                        GetStoreProcs(sql, catalog);
                    }

                }
            }

            catch
            {
            }
        }

        private string GetStoreProcSql(string DBName)
        {
            string sql = " ";
            sql += "SELECT SCHEMA_NAME(SCHEMA_ID) AS [Schema], ";
            sql += "SO.name AS [Name],";
            sql += "SO.Type_Desc AS [ObjectType (UDF/SP)] ";
            sql += "FROM " + DBName + ".sys.objects AS SO ";
            sql += "WHERE SO.TYPE IN ('P','FN') ";
            sql += "ORDER BY [Schema], SO.name ";
            return sql;
        }


        private void GetStoreProcs(string sql, string catalog)
        {

            Server server = ddlDatabase.SelectedItem as Server;
            connString = server.ConnectionString;
            this.radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcsCompleted);
            ws.GetDataSetDataAsync(connString, server.DatabaseName, sql, 1, 1000, null);
        }


        //void ws_GetStoreProcsCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //        Message.ErrorMessage(e.Error.Message);
        //    else if (e.ServiceError != null)
        //        Message.ErrorMessage(e.ServiceError.Message);
        //    else
        //    {
        //        // _tables = e.Result.Tables;
        //        IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
        //        if (e.UserState as string == "Lookup")
        //            _lookup = list;
        //        //radGridView1.ItemsSource = list;
        //        else
        //        {


        //            ddlStoreProc.ItemsSource = list;



        //        }
        //    }
        //    this.radBusyIndicator.IsBusy = false;
        //}
        private void GetStoreProcs(string connString, string database, string sql)
        {
            if (database != null)
            {

                //   string connString = objectFunction.Server.ConnectionString;
                var ws = WCF.GetService();
                ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcParaCompleted);
                ws.GetDataSetDataAsync(connString, database, sql, 1, 1000, "");
            }
        }


        void ws_GetStoreProcParaCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                // _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);

                List<ParameterData> parameterList = new List<ParameterData>();
                foreach (DataObject item in list)
                {

                    ParameterData para = new ParameterData();
                    ObjectDefintion obj = new ObjectDefintion();
                    string name = item.GetFieldValue("ParameterName").ToString().Replace('@', ' ');
                    para.Name = name.TrimStart();
                    para.DataType = item.GetFieldValue("ParameterDataType").ToString();
                    para.isOutputParamter = Convert.ToBoolean(item.GetFieldValue("IsOutPutParameter"));
                    para.MaxSize = Convert.ToInt32(item.GetFieldValue("ParameterMaxBytes"));

                    para.ObjectDef = obj;
                    parameterList.Add(para);
                }

                DataObject proc = ddlStoreProc.SelectedItem as DataObject;

                string procname = proc.GetFieldValue("Name").ToString();

                foreach (var item in parameterList)
                {
                    procname += "'',";
                }

                procname = procname.TrimEnd(',');
                string sql = "SET FMTONLY ON exec dbo." + procname + "  SET FMTONLY OFF";
                GetStoreProcColumns(sql);


                //    dgMyDataGrid.ItemsSource = parameters;

            }
            // this.radBusyIndicator.IsBusy = false;
        }


        void ws_GetStoreProcsCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                // _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ddlStoreProc.ItemsSource = list;
                    ddlStoreProc.SelectedValue = storeProcName;


                }
            }
            this.radBusyIndicator.IsBusy = false;
        }




        private void ddlStoreProc_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataObject proc = ddlStoreProc.SelectedItem as DataObject;
            if (proc != null)
            {
                string procname = proc.GetFieldValue("Name").ToString();
                Server server = ddlDatabase.SelectedItem as Server;
                string sql = SqlQuery(server.DatabaseName, procname);

                GetStoreProcs(server.ConnectionString, server.DatabaseName, sql);
            }
        }


        private void GetStoreProcColumns(string sql)
        {

            Server server = ddlDatabase.SelectedItem as Server;
            connString = server.ConnectionString;
            this.radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcColumnsCompleted);
            ws.GetDataSetDataAsync(connString, server.DatabaseName, sql, 1, 1000, null);
        }


        void ws_GetStoreProcColumnsCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                // _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {
                    List<TempDisplay> tempList = new List<TempDisplay>();
                    foreach (var item in e.Result.Tables)
                    {
                        foreach (var column in item.Columns)
                        {
                            tempList.Add(new TempDisplay { Value = column.ColumnName, Display = column.ColumnName });
                        }
                    }

                    ddlDisplay.ItemsSource = tempList;
                    ddlValue.ItemsSource = tempList;
                    ddlDisplay.SelectedValue = display;
                    ddlValue.SelectedValue = value;


                }
            }
            this.radBusyIndicator.IsBusy = false;
        }

        private void txtSQLText_LostFocus(object sender, RoutedEventArgs e)
        {
            string sqlString = txtSQLText.Text;
            string subSql = "";

            if (sqlString.Contains("where"))
            {
                subSql = sqlString.Substring(0, sqlString.IndexOf("where"));
                string[] Split = sqlString.Split(new Char[] { ' ' });
                foreach (var item in Split)
                {

                    if (item.StartsWith("@"))
                    {
                        checkParameters = true;
                    }
                }

            }
            else
            {
                subSql = sqlString;
            }

            string sql = "SET FMTONLY ON " + subSql + "  SET FMTONLY OFF";
            GetStoreProcColumns(sql);
        }


        private string SqlQuery(string database, string procName)
        {

            string sql = "";
            sql += " SELECT SCHEMA_NAME(SCHEMA_ID) AS [Schema], ";
            sql += "   SO.name AS [ObjectName], ";
            sql += "  SO.Type_Desc AS [ObjectType (UDF/SP)],";
            sql += "  P.parameter_id AS [ParameterID],";
            sql += "  P.name AS [ParameterName],";
            sql += "  TYPE_NAME(P.user_type_id) AS [ParameterDataType],";
            sql += " P.max_length AS [ParameterMaxBytes],";
            sql += "  P.is_output AS [IsOutPutParameter]";
            sql += "  FROM " + database + ".sys.objects AS SO";
            sql += "  Left Outer JOIN " + database + ".sys.parameters AS P  ON SO.OBJECT_ID = P.OBJECT_ID";
            sql += " WHERE SO.TYPE IN ('P','FN')";
            sql += "  and SO.name = '" + procName + "'";
            sql += "  ORDER BY [Schema], SO.name, P.parameter_id";
            return sql;
        }

        //private void GetStoreProcs(string connString, string database, string sql)
        //{
        //    if (database != null)
        //    {

        //        //   string connString = objectFunction.Server.ConnectionString;
        //        var ws = WCF.GetService();
        //        ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcParaCompleted);
        //        ws.GetDataSetDataAsync(connString, database, sql, 1, 1000, "");
        //    }
        //}


       
        
       
    }
}

