﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AssignUsersGroup : ChildWindow
    {
        EditorContext context = new EditorContext();
        public static bool newUser;
      private   int thisGroupID;
      public AssignUsersGroup(int groupID)
        {
            InitializeComponent();
            newUser = false;
            thisGroupID = groupID;
        }



        private void GetUnAssignedUsers(object sender, RoutedEventArgs e)
        {

            GetAllUnAssignedUsers();
        }


        private void GetAllUnAssignedUsers()
        {
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(m => m.Group_ID == null), CallbackUsers, null);

        }
        private void CallbackUsers(LoadOperation<UserInformation> loadUsers)
        {
            //if(ViewMenuStructure.userInformationList == null)
            //    ViewMenuStructure.userInformationList = new ObservableCollection<UserInformation>();

            if (loadUsers != null)
            {
                lstUsers.ItemsSource = loadUsers.Entities;
                //foreach (UserInformation user in loadUsers.Entities)
                //{
                //    ViewMenuStructure.userInformationList.Add(user);
                //}

            }
           
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.lstUsers.SelectedItems.Count == 0)
            
                return;



            foreach (UserInformation user in this.lstUsers.SelectedItems)
            {
                user.Group_ID = thisGroupID;
            }

            context.SubmitChanges(submit => { if (!submit.HasError) { Message.InfoMessage("Successfully Assigned"); GetAllUnAssignedUsers(); } else { Message.ErrorMessage(submit.Error.Message); } }, null);
              
        }

       

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            AddUser user = new AddUser();
            user.Closed += new EventHandler(user_Closed);
            user.Show();
           
        }

        void user_Closed(object sender, EventArgs e)
        {

            GetAllUnAssignedUsers();
        }

        }
    }


