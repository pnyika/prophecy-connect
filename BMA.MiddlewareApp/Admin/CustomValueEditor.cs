﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SLPropertyGrid;

namespace BMA.MiddlewareApp.Admin
{
    public class CustomValueEditor  : ValueEditorBase
	{
		TextBox txt;

		public CustomValueEditor(PropertyGridLabel label, PropertyItem property)
			: base(label, property)
		{
			if (property.PropertyType == typeof(Char))
			{
				if ((char)property.Value == '\0')
					property.Value = "";
			}

			property.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(property_PropertyChanged);
			property.ValueError += new EventHandler<ExceptionEventArgs>(property_ValueError);


			txt = new TextBox();
			if (null != property.Value)
				txt.Text = property.Value.ToString();
			txt.IsReadOnly = !this.Property.CanWrite;
			txt.Foreground = this.Property.CanWrite ? new SolidColorBrush(Colors.Black) : new SolidColorBrush(Colors.Gray);
			txt.BorderThickness = new Thickness(0);
			txt.Margin = new Thickness(1, 0, 1, 0);
			txt.Padding = new Thickness(1, 0, 1, 0);
			txt.Background = new SolidColorBrush(Colors.Red);
			txt.VerticalAlignment = System.Windows.VerticalAlignment.Center;

			if (this.Property.CanWrite)
				txt.TextChanged += new TextChangedEventHandler(Control_TextChanged);

			this.Content = txt;
			this.GotFocus += new RoutedEventHandler(StringValueEditor_GotFocus);
		}

		void property_ValueError(object sender, ExceptionEventArgs e)
		{
			MessageBox.Show(e.EventException.Message);
		}
		void property_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Value")
			{
				if (null != this.Property.Value)
					txt.Text = this.Property.Value.ToString();
				else
					txt.Text = string.Empty;
			}

			if (e.PropertyName == "CanWrite")
			{
				if (!this.Property.CanWrite)
					txt.TextChanged -= new TextChangedEventHandler(Control_TextChanged);
				else
					txt.TextChanged += new TextChangedEventHandler(Control_TextChanged);
				txt.IsReadOnly = !this.Property.CanWrite;
				txt.Foreground = this.Property.CanWrite ? new SolidColorBrush(Colors.Black) : new SolidColorBrush(Colors.Gray);
			}
		}

		void StringValueEditor_GotFocus(object sender, RoutedEventArgs e)
		{
			this.txt.Focus();
		}

		void Control_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (this.Property.CanWrite)
				this.Property.Value = txt.Text;
		}
	}
}

   