﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.IO;
using System.Windows.Media.Imaging;
using BMA.MiddlewareApp.Web;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddForm : Page
    {
        byte[] photo = null;
        public AddForm()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            if (openFileDialog.ShowDialog() == true)
            {
                Stream stream = (Stream)openFileDialog.File.OpenRead();
                byte[] imageBytes = new byte[stream.Length];
                stream.Read(imageBytes, 0, (int)stream.Length);
                photo = imageBytes;

                if (imageBytes.Length > 0)
                {
                    using (MemoryStream ms = new MemoryStream(imageBytes, 0,
                      imageBytes.Length))
                    {
                        BitmapImage im = new BitmapImage();
                        im.SetSource(ms);
                        this.imgPhoto.Source = im;
                    }
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            EditorContext context = new EditorContext();

            FormType form = new FormType();
            form.Description = txtType.Text;
            form.NumberOfGrids = Convert.ToInt32(txtNumber.Text);
            form.DisplayImage = photo;
            context.FormTypes.Add(form);

            context.SubmitChanges(so =>
            {
                if (so.HasError)
                {
                    MessageBox.Show(so.Error.Message);
                }
                else
                {
                    MessageBox.Show("Successfully Saved!");
                }
            }, null);

        }

    }
}
