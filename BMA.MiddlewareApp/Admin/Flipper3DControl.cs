﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.Admin
{
    public class Flipper3DControl : Control
    {
        private bool _isFrontSideVisible = true;

        public Flipper3DControl()
        {
            this.DefaultStyleKey = typeof(Flipper3DControl);
        }


        public override void OnApplyTemplate()
        {
            Grid mainGrid = GetTemplateChild("PART_MainGrid") as Grid;

            mainGrid.MouseLeftButtonDown += new MouseButtonEventHandler(mainGrid_MouseLeftButtonDown);

            base.OnApplyTemplate();
        }

        void mainGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_isFrontSideVisible)
                VisualStateManager.GoToState(this, "rotateStart", true);
            else
                VisualStateManager.GoToState(this, "rotateEnd", true);

            _isFrontSideVisible = !_isFrontSideVisible;
        }


        public UIElement FrontContent
        {
            get { return (UIElement)GetValue(FrontContentProperty); }
            set { SetValue(FrontContentProperty, value); }
        }

        public static readonly DependencyProperty FrontContentProperty =
            DependencyProperty.Register("FrontContent", typeof(UIElement), typeof(Flipper3DControl), new PropertyMetadata(null));


        public UIElement BackContent
        {
            get { return (UIElement)GetValue(BackContentProperty); }
            set { SetValue(BackContentProperty, value); }
        }

        public static readonly DependencyProperty BackContentProperty =
            DependencyProperty.Register("BackContent", typeof(UIElement), typeof(Flipper3DControl), new PropertyMetadata(null));


    }
}
