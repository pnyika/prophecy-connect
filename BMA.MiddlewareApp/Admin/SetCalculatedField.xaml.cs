﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SetCalculatedField : ChildWindow
    {
        EditorContext context = new EditorContext();
        ObservableCollection<Server> serverList;
        ObservableCollection<Table> tableList;
        List<Table> tables;
        private int viewID;
        ViewFieldSummary fld;
        Field fieldContent;
        ViewField viewField;
        public SetCalculatedField(int vwID)
        {
            InitializeComponent();
            viewID = vwID;
            LoadOperator();
            GetTables();
         
           

            lstOperator.AddHandler(ListBox.MouseLeftButtonDownEvent, new MouseButtonEventHandler(lstOperator_MouseLeftButtonDown), true);
            lstField.AddHandler(ListBox.MouseLeftButtonDownEvent, new MouseButtonEventHandler(lstField_MouseLeftButtonDown), true);
        }

        void LayoutRoot_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            Message.InfoMessage(e.ClickCount.ToString());
        }

        private void GetTables()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackTableB, null);


        }
        private void CallbackTableB(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {


                lstTable.ItemsSource = loadOp.Entities;

            }
        }

        private void lstTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Table table = lstTable.SelectedItem as Table;
            if (table == null)
                return;

           
            LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFields, null);

        }





        private void CallbackFields(LoadOperation<Field> loadOp)
        {


            if (loadOp != null)
            {
                lstField.ItemsSource = loadOp.Entities;



            }
        }
        private void LoadField(int fieldID, int viewFieldID)
        {
          //  LoadOperation<Field> loadOper = context.Load(context.GetFieldsQuery().Where(f => f.ID == fieldID), CallbackField, null);
            LoadOperation<ViewField> loadOp = context.Load(context.GetViewFieldsQuery().Where(f => f.ID == viewFieldID), CallbackViewField, null);
        }


        private void LoadOperator()
        {
            List<ControlItem> opList = new List<ControlItem>();
            opList.Add(new ControlItem { Display = "*", Value = "Multiply", index = 1 });
            opList.Add(new ControlItem { Display = "+", Value = "Add", index = 1 });
            opList.Add(new ControlItem { Display = "-", Value = "Subract", index = 1 });
            opList.Add(new ControlItem { Display = "/", Value = "Divide", index = 1 });
            opList.Add(new ControlItem { Display = "^", Value = "Power", index = 1 });
            opList.Add(new ControlItem { Display = "Mod", Value = "Mod", index = 1 });
            lstOperator.DisplayMemberPath = "Display";
            lstOperator.SelectedValuePath = "Value";
            lstOperator.ItemsSource = opList;

        }

     

        private void CallbackViewField(LoadOperation<ViewField> loadOp)
        {


            if (loadOp != null)
            {
                viewField = loadOp.Entities.FirstOrDefault();

            }
        }


        private void CheckTables()
        {
            if (tables.Count > 0)
            {
               
                var query = (from t in tables
                             select t).FirstOrDefault();
               
                
                LoadOperation<Table> loadOper = context.Load(context.GetTablesQuery().Where(x => x.Server_ID == query.Server_ID), CallbackConnTable, null);

            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            CalculatedField calcField = new CalculatedField();
            calcField.DisplayName = txtFieldName.Text;
            calcField.Expression = txtExpression.Text;
            calcField.View_ID = viewID;
            context.CalculatedFields.Add(calcField);

            context.SubmitChanges(submit => { if (submit.HasError) { Message.ErrorMessage(submit.Error.Message); } else { Message.InfoMessage("Successfully saved!"); this.DialogResult = true; } }, null);
            
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

      

        private void CallbackConnTable(LoadOperation<Table> loadOp)
        {


            tableList = new ObservableCollection<Table>();

            if (loadOp.Entities != null)
            {
                foreach (Table tbl in loadOp.Entities)
                {
                    tableList.Add(tbl);
                }


                lstTable.ItemsSource = tableList;
            }
        }

        private void lstField_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
              
                Field field = lstField.SelectedItem as Field;
                if (field != null)
                {
                    Table table = lstTable.SelectedItem as Table;
                    string strTable = "[" + table.TableName + "].";
                    string current = txtExpression.Text;
                    txtExpression.Text = current + strTable + "[" + field.FieldName + "] ";

                }
            }

        }

        private void lstOperator_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                ControlItem opr = lstOperator.SelectedItem as ControlItem;
                if (opr != null)
                {
                    string current = txtExpression.Text;
                    txtExpression.Text = current + opr.Display + " ";
                }
            }
        }
       


        //  private void lstTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        //{
        //    Table table = lstTable.SelectedItem as Table;
        //                if (table == null)
        //                    return;

        //                lblFieldTable.Text = fld.FieldName +" Field Ralated To " + table.TableName;
                      
        //         LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFields, null);

        // }



        

   
    }


    public class ControlItem
    {
        public string Value { get; set; }
        public string Display { get; set; }
        public int index { get; set; }
        public string Type { get; set; }
    }
    }


