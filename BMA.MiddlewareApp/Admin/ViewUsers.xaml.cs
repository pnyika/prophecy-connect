﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Telerik.Windows.Controls;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewUsers : Page
    {
        EditorContext context = new EditorContext();
        public ViewUsers()
        {
            InitializeComponent();
            usersDomainDataSource.SubmittedChanges += new EventHandler<Telerik.Windows.Controls.DomainServices.DomainServiceSubmittedChangesEventArgs>(MyRadDomainDataSource_SubmittedChanges);
            LoadOperation ldop = context.Load<MenuStructure>(context.GetMenuStructuresQuery(), Callback, null);
            LoadOperation groupOperation = context.Load<Group>(context.GetGroupsQuery(), CallbackGroups, null);
        }

        private void Callback(LoadOperation<MenuStructure> loadMenus)
        {
            if (loadMenus != null)
            {

                ((GridViewComboBoxColumn)this.radGridView.Columns["MenuStructure_ID"]).ItemsSource = loadMenus.Entities;
            }
        }


        private void CallbackGroups(LoadOperation<Group> results)
        {
            if (results != null)
            {

                ((GridViewComboBoxColumn)this.radGridView.Columns["Group_ID"]).ItemsSource = results.Entities;
            }
        }
        void MyRadDomainDataSource_SubmittedChanges(object sender, Telerik.Windows.Controls.DomainServices.DomainServiceSubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Avoids displaying errors, when deletion is prevented by DataBase constraints
                e.MarkErrorAsHandled();
            }
            else
            {
                Message.InfoMessage("Successfully saved!");
            }
        }


        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        //private void MyRadDataForm_EditEnding(object sender, Telerik.Windows.Controls.Data.DataForm.EditEndingEventArgs e)
        //{

        //    if (e.EditAction == Telerik.Windows.Controls.Data.DataForm.EditAction.Commit)
        //    {
        //        if (((sender as RadDataForm).CurrentItem as UserInformation).FirstName == null || ((sender as RadDataForm).CurrentItem as UserInformation).LastName == null)
        //        {
        //            e.Cancel = true;
        //        }
        //    }
        //    if (MyRadDataForm.ValidateItem())
        //        usersDomainDataSource.SubmitChanges();
        //}

        //private void MyRadDataForm_DeletedItem(object sender, Telerik.Windows.Controls.Data.DataForm.ItemDeletedEventArgs e)
        //{
        //    usersDomainDataSource.SubmitChanges();
            
        //}

        private void btnAddUser_Click(object sender, RoutedEventArgs e)
        {
            AddUser user = new AddUser();
            user.Closed += new EventHandler(user_Closed);
            user.Show();
        }

        void user_Closed(object sender, EventArgs e)
        {
            usersDomainDataSource.Load();
           
        }

        private void btnDeleteUser_Click(object sender, RoutedEventArgs e)
        {

            if(radGridView.SelectedItems.Count <1)
            return;

            foreach (var item in radGridView.SelectedItems)
            {
                UserInformation user = item as UserInformation;
                if (user != null)
                    user.IsDeleted = true;
            }
            usersDomainDataSource.SubmitChanges();
            
        }


    }
}
