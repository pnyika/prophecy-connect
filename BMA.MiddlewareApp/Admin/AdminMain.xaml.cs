﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
  using Telerik.Windows;
using System.ServiceModel.DomainServices.Client.ApplicationServices;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AdminMain : UserControl
    {
        public static string header;
      
        public AdminMain()
        {

            InitializeComponent();
            if (WebContext.Current.Authentication.User.IsInRole("User"))
            {
                this.Content = new MainPage();
            }
            else
            {

                if (WebContext.Current.Authentication.User.IsInRole("Developer"))
                    lblRole.Text = "Application Developer ";
                else
                      lblRole.Text = "Super User";

                lblUsername.Text = WebContext.Current.Authentication.User.Identity.Name;
                lblInfo.Text = "";
                  

                LoadData();
               // this.tvHierarchyView.AddHandler(Telerik.Windows.Controls.RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.radTreeView_MouseLeftButtonDown), true);

            }

            LayoutRoot.LayoutUpdated += new EventHandler(LayoutRoot_LayoutUpdated);
        }

        void LayoutRoot_LayoutUpdated(object sender, EventArgs e)
        {
           // NavigationGrid.Height = LayoutRoot.Height -30;
        }

        // Executes when the user navigates to this page.
        //protected override void OnNavigatedTo(NavigationEventArgs e)
        //{
        //}

         
      
           
                private void radTreeView_MouseLeftButtonDown( object sender, MouseButtonEventArgs e )
                {
                    Telerik.Windows.Controls.RadTreeView treeView = sender as Telerik.Windows.Controls.RadTreeView;

                    Category selectedItem = treeView.SelectedItem as Category;
                    if (selectedItem == null)
                        return;
                    try
                    {

                        string selected = selectedItem.Name.ToString();
                        // MessageBox.Show(select   ed);
                        header = selected;
                        lblTitle2.Text = selected;
                    //    NavContent.Header = header;
                        switch (selected)
                        {
                            case "Views":
                                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                                    this.ContentFrame.Navigate(new Uri("/ViewListsp", UriKind.Relative));
                                else
                                    this.ContentFrame.Navigate(new Uri("/ViewList", UriKind.Relative)); 
                                break;


                            case "Databases":

                                this.ContentFrame.Navigate(new Uri("/ConfigPage", UriKind.RelativeOrAbsolute));
                                break;

                            case "Forms":

                                this.ContentFrame.Navigate(new Uri("/AddForm", UriKind.RelativeOrAbsolute));
                                break;
                            case "Grid Form":

                                this.ContentFrame.Navigate(new Uri("/ViewDisplayDefinition", UriKind.RelativeOrAbsolute));
                                break;
                            case "View Users":

                                this.ContentFrame.Navigate(new Uri("/ViewUsers", UriKind.RelativeOrAbsolute));
                                break;

                            case "Group Filtering":

                                this.ContentFrame.Navigate(new Uri("/ViewGroups", UriKind.RelativeOrAbsolute));
                                break;


                            case "Menu Settings":

                                this.ContentFrame.Navigate(new Uri("/ViewMenuStructure", UriKind.RelativeOrAbsolute));

                                break;

                            case "Custom Form":

                                this.ContentFrame.Navigate(new Uri("/ViewForm", UriKind.RelativeOrAbsolute));

                                break;

                            case "Model Interface":

                                this.ContentFrame.Navigate(new Uri("/ViewEOForm", UriKind.RelativeOrAbsolute));

                                break;

                            case "Back":

                                this.Content = new MainPage();
                                break;

                            case "Logout":
                               /// FormAuthentication()
                                WebContext.Current.Authentication.Logout(LogoutCompleteCallback, null);
                               
                                break;
                            
                        
                        }

                    }
                    catch (Exception ex)
                    {
                        this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));
                        //MessageBox.Show(ex.Message);
                    }
                }


                private void LogoutCompleteCallback(LogoutOperation result)
                {
                    this.Content = new MainPage();
                }


        private void AddItems()
        {
            TreeViewItem item1 = new TreeViewItem();
            item1.Header = "Products";
            item1.Items.Add(new TreeViewItem() { Header = "Controls" });
            item1.Items.Add(new TreeViewItem() { Header = "Media Players" });
            item1.Items.Add(new TreeViewItem() { Header = "Games" });
            item1.Items.Add(new TreeViewItem() { Header = "Charts" });
          //  tvHierarchyView.Items.Add(item1);
        }

        private void LoadData()
        {
            List<Category> categories = new List<Category>();
            XDocument categoriesXML = XDocument.Load("MenuFile.xml");

            categories = this.GetCategories(categoriesXML.Element("categories"));

           // this.tvHierarchyView.ItemsSource = categories;
        }

        private List<Category> GetCategories(XElement element)
        {
           
                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                { 
            
                    return (from category in element.Elements("category")
                            select new Category()
                            {
                                Name = category.Attribute("name").Value,
                                Value = category.Attribute("value").Value,
                                SubCategories = this.GetCategories(category)
                            }).Where(x => x.Value == "1" || x.Value =="2").ToList();
           
                }
                else
                {
                    return (from category in element.Elements("category")
                            select new Category()
                            {
                                Name = category.Attribute("name").Value,
                                Value = category.Attribute("value").Value,
                                SubCategories = this.GetCategories(category)
                            }).Where(x => x.Value == "0" || x.Value == "1").ToList();
                }
           
        }

        private void tvHierarchyView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            // this.Content = new Page2();
          //  Category selectedItem = tvHierarchyView.SelectedItem as Category;

            try
            {

                string selected = "";//selectedItem.Name.ToString();
                // MessageBox.Show(selected);
                //NavContent.Header = selected;
                if (selected == "Views")
                {

                    if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                    this.ContentFrame.Navigate(new Uri("/ViewListsp", UriKind.Relative)); 
                    else
                        this.ContentFrame.Navigate(new Uri("/ViewList", UriKind.Relative)); 

                }
                if (selected == "Databases")
                {
                    this.ContentFrame.Navigate(new Uri("/ConfigPage", UriKind.RelativeOrAbsolute));
                }
                if (selected == "Forms")
                {
                    this.ContentFrame.Navigate(new Uri("/AddForm", UriKind.RelativeOrAbsolute));
                }
                if (selected == "Display Definitions")
                {
                    this.ContentFrame.Navigate(new Uri("/ViewDisplayDefinition", UriKind.RelativeOrAbsolute));
                }
                if (selected == "View Users")
                {
                    this.ContentFrame.Navigate(new Uri("/ViewUsers", UriKind.RelativeOrAbsolute));
                }


                if (selected == "Menu Settings")
                {
                    this.ContentFrame.Navigate(new Uri("/ViewMenuStructure", UriKind.RelativeOrAbsolute));
                }
                //else
                //{
                //   this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));

                //}


            }
            catch (Exception ex)
            {
                this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));
               //MessageBox.Show(ex.Message);
            }
        }



        private void ContentFrame_NavigationFailed(object sender, System.Windows.Navigation.NavigationFailedEventArgs e)
        {
            e.Handled = true;
            ContentFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }


        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            //foreach (UIElement child in LinksStackPanel.Children)
            //{
            //    HyperlinkButton hb = child as HyperlinkButton;
            //    if (hb != null && hb.NavigateUri != null)
            //    {
            //        if (hb.NavigateUri.ToString().Equals(e.Uri.ToString()))
            //        {
            //            VisualStateManager.GoToState(hb, "ActiveLink", true);
            //        }
            //        else
            //        {
            //            VisualStateManager.GoToState(hb, "InactiveLink", true);
            //        }
            //    }
            //}
        }

        private void homeMenu_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
           

            try
            {

                string selected = homeMenu.SelectedItem.ToString();
                // MessageBox.Show(select   ed);
                header = selected;
                lblLabel.ApplicationName = header;
                lblTitle2.Text = header;
                if (header != "Custom Form")
                {
                  ///  toolBox.Visibility = System.Windows.Visibility.Collapsed; ;
                }
                switch (selected)
                {
                      
                    case "Views":
                        if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                            this.ContentFrame.Navigate(new Uri("/ViewListsp", UriKind.Relative));
                        else
                            this.ContentFrame.Navigate(new Uri("/ViewList", UriKind.Relative));
                        break;


                    case "Databases":

                        this.ContentFrame.Navigate(new Uri("/ConfigPage", UriKind.RelativeOrAbsolute));
                        break;

                    case "Forms":

                        this.ContentFrame.Navigate(new Uri("/AddForm", UriKind.RelativeOrAbsolute));
                        break;
                    case "Grid Form":

                        this.ContentFrame.Navigate(new Uri("/ViewDisplayDefinition", UriKind.RelativeOrAbsolute));
                        break;
                    case "View Users":

                        this.ContentFrame.Navigate(new Uri("/ViewUsers", UriKind.RelativeOrAbsolute));
                        break;

                    case "Group Filtering":

                        this.ContentFrame.Navigate(new Uri("/ViewGroups", UriKind.RelativeOrAbsolute));
                        break;


                    case "Menu Settings":

                        this.ContentFrame.Navigate(new Uri("/ViewMenuStructure", UriKind.RelativeOrAbsolute));

                        break;

                    case "Custom Form":
                      //  toolBox.Visibility = System.Windows.Visibility.Visible;
                        this.ContentFrame.Navigate(new Uri("/ViewForm", UriKind.RelativeOrAbsolute));

                        break;

                    case "Model Interface":

                        this.ContentFrame.Navigate(new Uri("/ViewEOForm", UriKind.RelativeOrAbsolute));

                        break;

                    case "Back":

                        this.Content = new MainPage();
                        break;

                    case "Logout":
                        /// FormAuthentication()
                        WebContext.Current.Authentication.Logout(LogoutCompleteCallback, null);

                        break;


                }

            }
            catch (Exception ex)
            {
                this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));
                //MessageBox.Show(ex.Message);
            }
        }
    }
    public class Category
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int ID{ get; set; }
        public string ImageUrl { get; set; }
        public List<Category> SubCategories { get; set; }
    }
}
