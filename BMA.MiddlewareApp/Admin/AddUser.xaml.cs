﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using SilverlightMessageBox;
using System.Collections.ObjectModel;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddUser : ChildWindow
    {
        EditorContext context = new EditorContext();
        LoginUserInfo user = new LoginUserInfo();
        RadComboBox ddlRoles;
        ObservableCollection<TempDisplay> Names = new ObservableCollection<TempDisplay>();

        Web.Services.UserInformationContext dataContext = new Web.Services.UserInformationContext();
        public AddUser()
        {
            InitializeComponent();


            Loaded += (s, e) =>
                {
                    user.ModifiedDate = DateTime.Now;
                    user.IsDeleted = false;
                    UserForm.CurrentItem = user;

                    UserForm.CommandButtonsVisibility = DataFormCommandButtonsVisibility.Commit | DataFormCommandButtonsVisibility.Cancel;
                    UserForm.EditEnded += new EventHandler<DataFormEditEndedEventArgs>(UserForm_EditEnded);
                    UserForm.ContentLoaded += new EventHandler<DataFormContentLoadEventArgs>(UserForm_ContentLoaded);
                };

           
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void UserForm_ContentLoaded(object sender, DataFormContentLoadEventArgs e)
        {
            RadComboBox ddlMenu = UserForm.FindNameInContent("ddlMenu") as RadComboBox;

           ddlRoles = UserForm.FindNameInContent("ddlRole") as RadComboBox;

           RadComboBox ddlGroup = UserForm.FindNameInContent("ddlGroup") as RadComboBox;
           
            context.Load<MenuStructure>(context.GetMenuStructuresQuery(), LoadBehavior.RefreshCurrent, (MenuLoaded) =>
            {
                if (!MenuLoaded.HasError)
                {
                    ddlMenu.ItemsSource = MenuLoaded.Entities;
                    
                }

            }, null);


            context.Load<Group>(context.GetGroupsQuery(), LoadBehavior.RefreshCurrent, (GroupLoaded) =>
            {
                if (!GroupLoaded.HasError)
                {
                    ddlGroup.ItemsSource = GroupLoaded.Entities;

                }

            }, null);
           
            LoadOperation ldop = dataContext.Load<Role>(dataContext.LoadRolesQuery(), Callback, null);
            GetData("");
        }



        private void Callback(LoadOperation<Role> loadMenus)
        {
            if (loadMenus != null)
            {

             //   ((GridViewComboBoxColumn)this.radGridView.Columns["MenuStructure_ID"]).ItemsSource = loadMenus.Entities;
                ddlRoles.ItemsSource = loadMenus.Entities;
            }
        }

        private void UserForm_EditEnded(object sender, DataFormEditEndedEventArgs e)
        {
            if (e.EditAction == DataFormEditAction.Cancel)
            {
                if (btnCancel.IsChecked == false)
                    btnCancel.IsChecked = true;

                else
                    btnCancel.IsChecked = false;

            }
            else
            {
                if (UserForm.ValidateItem())
                {
                    dataContext.LoginUserInfos.Add(UserForm.CurrentItem as LoginUserInfo);
                    dataContext.SubmitChanges(submit => { if (submit.HasError &&
                submit.EntitiesInError.All(t => t.HasValidationErrors))
                    {
                       
                       string errors = "";
                       submit.MarkErrorAsHandled();
                       // UserForm.CurrentItem = submit.EntitiesInError;
                       foreach (var item in submit.EntitiesInError)
                       {
                          foreach (var listitem in item.ValidationErrors)
                          {
                             errors += listitem.ErrorMessage;
                           }
                       }

                       Message.ErrorMessage(errors);
                       }

                    else { Message.InfoMessage("Successfully saved"); DialogResult = false; }
                    }, null);

                }
            }
        }

        
       private void GetData(string path)
        {
           
            var ws = WCF.GetService();
            ws.GetUsersOnADCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetUsersOnADCompletedEventArgs>(ws_GetUsersOnADCompleted);
            ws.GetUsersOnADAsync(path, "");
        }


       void ws_GetUsersOnADCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetUsersOnADCompletedEventArgs e)
            {
                if (e.Error != null)
                {
                    
                }


                else
                {
                    
                    //Names = new ObservableCollection<TempDisplay>();
                    //foreach (var item in e.Result)
                    //{
                    //    Names.Add(new TempDisplay {Display = item, Value = item });
                    //}
                    AutoCompleteBox NameBox = UserForm.FindNameInContent("NameBox") as AutoCompleteBox;
                    //NameBox.ValueMemberPath = "Display";
                    NameBox.ItemsSource = e.Result;
                }
            }

       private void NameBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
       {
           AutoCompleteBox NameBox = UserForm.FindNameInContent("NameBox") as AutoCompleteBox;
           TextBox txtFirstName = UserForm.FindNameInContent("txtFirstName") as TextBox;

           string name = NameBox.SelectedItem as string;
           txtFirstName.Text = name;
       }
    }
}

