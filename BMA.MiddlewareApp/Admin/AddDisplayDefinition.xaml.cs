﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddDisplayDefinition : Page
    {

        ObservableCollection<TestDataRow> grids;
        private ObservableCollection<View> ViewList = new ObservableCollection<View>();
        DisplayDefinition displayDefinition = new DisplayDefinition();
        EditorContext context = new EditorContext();
        public AddDisplayDefinition()
        {
            GetViews();
            this.Resources.Add("ViewList", ViewList);
            InitializeComponent();

            LoadPageSizes();
           
        }



        private void LoadPageSizes()
        {
            List<PageSize> pageSizeList = new List<PageSize>();
            for (int i = 10; i <= 1000; i+=5)
            {

                pageSizeList.Add(new PageSize { Size = i });
            }
            ddlPageSize.ItemsSource = pageSizeList;
            ddlPageSize.SelectedValue = 25;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void GetViews()
        {

            LoadOperation<View> loadOp = context.Load(context.GetViewsQuery(), CallbackViews, null);


        }




        private void CallbackViews(LoadOperation<View> result)
        {


            if (result != null)
            {

                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                {
                    ObservableCollection<View> thisviewList = new ObservableCollection<View>(result.Entities.Where(v => v.GroupViews.Any(ug => ug.Group_ID == Globals.CurrentUser.Group_ID)).OrderBy(si => si.ViewName));
                   
                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }
                else
                {
                    ObservableCollection<View> thisviewList = new ObservableCollection<View>(result.Entities.OrderBy(si => si.ViewName));

                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }
             //  ComboBox cc =  dgMyDataGrid.col
            }
        }

        private void GetForms(object sender, RoutedEventArgs e)
          {
         
            LoadOperation<FormType> loadOp = context.Load(context.GetFormTypesQuery(), CallbackDisplayDef, null);


        }



        private void CallbackDisplayDef(LoadOperation<FormType> loadOp)
        {


            if (loadOp != null)
            {
                lstForms.ItemsSource = loadOp.Entities;

                lstForms.SelectedValue = displayDefinition.FormType_ID;


            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            grids =  new ObservableCollection<TestDataRow>();
            FormType form = lstForms.SelectedItem as FormType;
            if (form == null)
                return;
            int x = form.NumberOfGrids.Value;
            for (int i = 1; i <= x; i++)
            {
                TestDataRow test = new TestDataRow();
                View view = new View();
                test.GridNumber = i;
                test.ViewName = view;
                test.ID = i;
                grids.Add(test);
            }

            dgMyDataGrid.ItemsSource = grids;
        }

        private void lstForms_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
          
                grids = null;// new ObservableCollection<TestDataRow>();
                dgMyDataGrid.ItemsSource = grids;
           
            FormType form = lstForms.SelectedItem as FormType;
            if (form == null)
                return;

            lblNumberOfGrids.Content = "Number of grids: " + form.NumberOfGrids.ToString();
        }



        private void dgMyDataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            dgMyDataGrid.BeginEdit();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool nullExist = false;

                FormType form = lstForms.SelectedItem as FormType;
                if (form == null)
                    return;


                foreach (DataGridRow rowItem in DataGridExtensions.GetRows(dgMyDataGrid))
                {
                    TestDataRow data = rowItem.DataContext as TestDataRow;

                    if (data.ViewName == null)
                    {
                        nullExist = true;
                    }
                }

                if (nullExist)
                {
                    MessageBoxResult result = MessageBox.Show("Some grids have not been asigned  to view, do you want contiue saving ?", "Confirm", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        SaveDefinition();
                    }
                }
                else
                {
                    SaveDefinition();
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
           
        }

        private void SaveDefinition()
        {
            FormType form = lstForms.SelectedItem as FormType;
            PageSize page = ddlPageSize.SelectedItem as PageSize; 
            DisplayDefinition display = new DisplayDefinition();
            display.Description = txtDesription.Text;
            display.FormType_ID = form.ID;
            display.PageSize = page.Size;
            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                display.CreatedUser_ID = Globals.CurrentUser.ID;
            else
                display.CreatedUser_ID = 0;

            context.DisplayDefinitions.Add(display);
            context.SubmitChanges(submit =>
            {
                if (submit.HasError) { Message.ErrorMessage("An error have while processing your request!"); }
                else
                {



                    foreach (DataGridRow rowItem in DataGridExtensions.GetRows(dgMyDataGrid))
                    {
                        TestDataRow data = rowItem.DataContext as TestDataRow;

                        if (data.ViewName != null)
                        {
                            GridDefinition gridDef = new GridDefinition();
                            gridDef.DisplayDefinition_ID = display.ID;

                            if (data.ViewName.ID != 0)
                            {
                                gridDef.View_ID = data.ViewName.ID;
                            }
                            gridDef.GridNumber = data.GridNumber.ToString();
                            context.GridDefinitions.Add(gridDef);



                        }

                    }
                    context.SubmitChanges(sub => { if (sub.HasError) { Message.ErrorMessage(sub.Error.Message); } else { Message.InfoMessage("Successfully saved!"); } }, null);
                }
            }, null);

        }

        private void btnNewView_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/ViewList", UriKind.Relative));
        }

    }

    public class TestDataRow
    {
        public int ID { get; set; }
        public int GridNumber { get; set; }
        public string Name { get; set; }
        public View ViewName { get; set; }
    }


    public class PageSize
    {
        public int Size { get; set; }
        
    }

    /// <summary> 
    /// Extends the DataGrid. 
    /// </summary> 
    public static  class DataGridExtensions
    {
        /// <summary> 
        /// Gets the list of DataGridRow objects. 
        /// </summary> 
        /// <param name="grid">The grid wirhrows.</param> 
        /// <returns>List of rows of the grid.</returns> 
        public static ICollection<DataGridRow> GetRows(this DataGrid grid)
        {
            List<DataGridRow> rows = new List<DataGridRow>();

            foreach (var rowItem in grid.ItemsSource)
            {
                // Ensures that all rows are loaded. 
                grid.ScrollIntoView(rowItem, grid.Columns.Last());

                // Get the content of the cell. 
                FrameworkElement el = grid.Columns.Last().GetCellContent(rowItem);

                // Retrieve the row which is parent of given element. 
                DataGridRow row = DataGridRow.GetRowContainingElement(el.Parent as FrameworkElement);

                // Sometimes some rows for some reason can be null. 
                if (row != null)
                    rows.Add(row);
            }

            return rows;
        }
    }
}
