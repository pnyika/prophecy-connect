﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewDisplayDefinition : Page
    {
        public static DisplayDefinition publicDisplayDef;
        EditorContext context = new EditorContext();
        List<FormType> formList = new List<FormType>();
        List<DisplayDefinition> DisplayDefList = new List<DisplayDefinition>();
        public ViewDisplayDefinition()
        {
            InitializeComponent();
            GetForms();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        private void GetDisplayDef(object sender, RoutedEventArgs e)
        {
         
            LoadOperation<DisplayDefinition> loadOp = context.Load(context.GetDisplayDefinitionsQuery(), CallbackDisplayDef, null);


        }



        private void CallbackDisplayDef(LoadOperation<DisplayDefinition> loadOp)
        {


            if (loadOp != null)
            {
                radGridView1.ItemsSource = loadOp.Entities.Where(d=>d.FormType_ID != 8);

                DisplayDefList = loadOp.Entities.Where(d => d.FormType_ID != 8).ToList();


            }
        }

        private void Item_LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        { 

            StackPanel ItemRef = sender as StackPanel;      // get the reference to the control
            SolidColorBrush brush1 = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));      //base colour
            SolidColorBrush brush2 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));  //alternate colour

           // if (_useAlternate)
                //ItemRef.Background = brush1;
           // else
                ItemRef.Background = brush2;

            //_useAlternate = !_useAlternate;
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/AddDisplayDefinition", UriKind.Relative));
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {

            DisplayDefinition display = radGridView1.SelectedItem as DisplayDefinition;
            if (display == null)
                return;
             publicDisplayDef = display;
             if (WebContext.Current.Authentication.User.IsInRole("Super User"))
             {
                 if (display.CreatedUser_ID != Globals.CurrentUser.ID)
               
                     Message.ErrorMessage("You have no priledges to modify the selected item");
                
                 else
                     this.NavigationService.Navigate(new Uri("/EditDisplayDefinition", UriKind.Relative));
             }
             else
             {
                 this.NavigationService.Navigate(new Uri("/EditDisplayDefinition", UriKind.Relative));
             }
            //this.NavigationService.Navigate(new Uri("/TestD", UriKind.Relative));
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            DisplayDefinition display = radGridView1.SelectedItem as DisplayDefinition;
            if (display == null)
                return;
            publicDisplayDef = DisplayDefList.Where(d => d.ID == display.ID).FirstOrDefault() ;
            SetGridRelationship grid = new SetGridRelationship();

            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
            {
                if (display.CreatedUser_ID != Globals.CurrentUser.ID)

                    Message.ErrorMessage("You have no priledges to modify the selected item");

                else
                {


                    if (formList.Where(f => f.ID == display.FormType_ID.Value).FirstOrDefault().NumberOfGrids > 1)
                        grid.Show();
                    else
                        Message.ErrorMessage("Single grid was specified. Therefore no relationships can be defined.");
                }
            }
            else
            {
                if (formList.Where(f => f.ID == display.FormType_ID.Value).FirstOrDefault().NumberOfGrids > 1)
                grid.Show();
                else
                    Message.ErrorMessage("Single grid was specified. Therefore no relationships can be defined.");
            }

           
        }
        private void GetForms()
        {

            LoadOperation<FormType> loadOp = context.Load(context.GetFormTypesQuery(), CallbackDisplayDef, null);


        }



        private void CallbackDisplayDef(LoadOperation<FormType> loadOp)
        {


            if (loadOp != null)
            {
                formList = loadOp.Entities.ToList();

            }
        }

        private void btnSetupWiz_Click(object sender, RoutedEventArgs e)
        {

            this.NavigationService.Navigate(new Uri("/GridFormWizard", UriKind.Relative));
        }
    }
}
