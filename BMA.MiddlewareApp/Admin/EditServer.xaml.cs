﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using System.Collections;
using System.ServiceModel.DomainServices.Client;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class EditServer : ChildWindow
    {
        IEnumerable _lookup;
        ObservableCollection<DataTableService.DataTableInfo> _tables;
        EditorContext context = new EditorContext();
        private string connString = "";
        public static int serverID;
        bool isSave = false;
        Server server = new Server();
        public EditServer(int ServerID)
        {
            InitializeComponent();
            loadServer(ServerID);
            this.OKButton.IsEnabled = false;
        }

        private void loadServer(int serverid)
        {
            context.Load<Server>(context.GetServersQuery().Where(s=>s.ID==serverid), LoadBehavior.RefreshCurrent, (SitesLoaded) =>
            {
                if (!SitesLoaded.HasError)
                {
                    server = SitesLoaded.Entities.FirstOrDefault();

                    if (server != null)
                    {
                        txtSource.Text = server.ServerName.Replace(server.DatabaseName, "");

                        txtID.Text = server.Usename;
                        txtpass.Password = server.Password;
                        ddlDatabase.SelectedValue = server.DatabaseName;
                        GetDatabases();
                    }

                }

            }, null);

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            isSave = true;
            SendTestConnection();
           
            
        }
        private void Save()
        {

            server.ServerName = txtSource.Text + " " + ddlDatabase.SelectedValue.ToString();
            server.ServerType = "SQL SERVER 2008 R2";
            server.Usename = txtID.Text;
            server.Password = txtpass.Password;
            server.DatabaseName = ddlDatabase.SelectedValue.ToString();
            server.ConnectionString = "Password=" + txtpass.Password + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + ddlDatabase.SelectedValue.ToString() + ";Data Source=" + txtSource.Text;
            //   app.serverList.Add(server);

            context.SubmitChanges(submit => { if (submit.Error == null) { Message.InfoMessage("Successfully Saved"); this.DialogResult = false; } else { Message.ErrorMessage("Unsuccessfully Saved"); } }, null);
           
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void radButton1_Click(object sender, RoutedEventArgs e)
        {
            isSave = false;
            SendTestConnection();

        }


        private void SendTestConnection()
        {
            if (ddlDatabase.SelectedValue.ToString() == "")
            {
                Message.ErrorMessage("Select database!");
            }
            else
            {
                this.radBusyIndicator.IsBusy = true;
                string pass = txtpass.Password;
                string catalog = "Master";
                if (ddlDatabase.SelectedValue.ToString() == "")
                    catalog = ddlDatabase.SelectedValue.ToString();
                string source = txtSource.Text;
                string id = txtID.Text;

                connString = "Password=" + pass + ";Persist Security Info=True;User ID=" + id + ";Initial Catalog=" + catalog + ";Data Source=" + source;
                string connString1 = "Provider=SQLOLEDB.1;Password=" + pass + ";Persist Security Info=True;User ID=" + id + ";Initial Catalog=" + catalog + ";Data Source=" + source;


                TestConnection(connString1);
            }
        }

        private void TestConnection(string connString)
        {
            var ws = WCF.GetService();
            ws.TestConnectionCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.TestConnectionCompletedEventArgs>(ws_TestCompleted);
            ws.TestConnectionAsync(connString);
        }


        void ws_TestCompleted(object sender, BMA.MiddlewareApp.DataTableService.TestConnectionCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                   // this.OKButton.IsEnabled = true;
                    if (isSave)

                        Save();

                    else
                        Message.InfoMessage("Successful");
                }

                else
                    Message.ErrorMessage("Fail");
            }
            else
            {
                if(isSave)
                    Message.ErrorMessage("Unsucessful, Please click Test Connection Button to test your server details and continue!");
                   
                else

                Message.ErrorMessage(e.Error.Message);


            }

            this.radBusyIndicator.IsBusy = false;
        }



        private void GetData(string sql, int pagenumber, int pagesize, object userState)
        {
            string catalog = "master";

            connString = "Password=" + txtpass.Password + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + catalog + ";Data Source=" + txtSource.Text;
            this.radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, "master", sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                System.Windows.Browser.HtmlPage.Window.Alert(e.Error.Message);
            else if (e.ServiceError != null)
                System.Windows.Browser.HtmlPage.Window.Alert(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ddlDatabase.ItemsSource = list;
                 


                }
            }
            this.radBusyIndicator.IsBusy = false;
        }




        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            GetDatabases();
        }
        private void GetDatabases()
        {

            string sql = " SELECT [name] FROM master.dbo.sysdatabases WHERE dbid > 4  order by [name] ";
            GetData(sql, 1, 1000, null);
        }

        private void ddlDatabase_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            connString = "Password=" + txtpass.Password + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + ddlDatabase.SelectedValue.ToString() + ";Data Source=" + txtSource.Text;
            this.OKButton.IsEnabled = true;
        }
    }

   
}

