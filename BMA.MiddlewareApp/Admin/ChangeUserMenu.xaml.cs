﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ChangeUserMenu : ChildWindow
    {
        EditorContext context = new EditorContext();
        UserInformation user = new UserInformation();
        ObservableCollection<int> userList;
        public ChangeUserMenu(UserInformation userInfo)
        {
            InitializeComponent();

            user = userInfo;
            string name = user.FirstName + " " + user.LastName;
            lblName.Text = name;
         LoadOperation ldop = context.Load<MenuStructure>(context.GetMenuStructuresQuery(), Callback, null);
        }

        private void Callback(LoadOperation<MenuStructure> loadMenus)
        {
            if (loadMenus != null)
            {
               lstMenu.ItemsSource = loadMenus.Entities;
                
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        { 
            MenuStructure menu = lstMenu.SelectedItem as MenuStructure;
            if(menu == null)
                return;
            var ws = WCF.GetService();
            ws.UpdateUserMenuCompleted +=new EventHandler<DataTableService.UpdateUserMenuCompletedEventArgs>(ws_UpdateUserMenuCompleted);
            userList = new ObservableCollection<int>();
            userList.Add(user.ID);
            ws.UpdateUserMenuAsync(userList, menu.ID);

          
        }

        void ws_UpdateUserMenuCompleted(object sender, DataTableService.UpdateUserMenuCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                MessageBox.Show("Successfully Assigned");
                this.DialogResult = true;
            }
            else{
                MessageBox.Show("Error Occured while proccessing your request!");
        }
        }
            
        

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

