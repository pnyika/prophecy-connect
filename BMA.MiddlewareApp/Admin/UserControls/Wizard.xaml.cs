﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.ViewModel;


namespace BMA.MiddlewareApp.Controls
{
    public partial class Wizard : UserControl
    {

        private readonly WidzardViewModel vm;
        public Wizard()
        {
            InitializeComponent();

            vm = new WidzardViewModel();
            DataContext = vm;
        }
    }
}
