﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls.GridView;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin.UserControls
{
    public partial class VRelationshipControl : UserControl
    {
        EditorContext context = new EditorContext();
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList = new List<ViewFieldSummary>();

        List<ViewField> viewfieldList = new List<ViewField>();
        List<Table> tableList = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary;
        public static int viewID;
        private int tableID;
        public VRelationshipControl()
        {
            InitializeComponent();
        }

        void radGridView1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                MessageBox.Show("2");
            }
        }

        // Executes when the user navigates to this page.
     
        private void radGridView_RowEditEnded(object sender, Telerik.Windows.Controls.GridViewRowEditEndedEventArgs e)
        {
            try
            {
                ViewFieldSummary dataObject = e.EditedItem as ViewFieldSummary;

                if (dataObject == null)
                    return;

                if (dataObject.DisplayName == string.Empty)
                    return;


                CustomMessage customMessage = new CustomMessage("Do you want to save the changes on this row? ", CustomMessage.MessageType.Confirm);

                customMessage.OKButton.Click += (obj, args) =>
                {
                    if ((dataObject != null))
                    {

                        if (e.EditOperationType == GridViewEditOperationType.Edit)
                        {

                            var viewfld = viewfieldList.Where(v => v.ID == dataObject.ID).FirstOrDefault();
                            if (viewfld != null)
                            {
                                viewfld.DisplayName = dataObject.DisplayName;
                                viewfld.UseValueField = dataObject.UseValueField.Value;
                            }
                            context.SubmitChanges(s => { }, null);
                        }
                    }
                };


                customMessage.Show();



            }
            catch
            {

            }
        }

       


     

        private void GetRelationships(object sender, RoutedEventArgs e)
        {
          //  viewID = 1;//Convert.ToInt32(NavigationContext.QueryString["viewID"]);
            LoadOperation<ViewRelationship> loadOp = context.Load(context.GetViewRelationshipByViewIDQuery(viewID), CallbackRelationships, null);


        }

        private void GetRelationshipsRefresh()
        {
            //viewID = 1;// Convert.ToInt32(NavigationContext.QueryString["viewID"]);
            LoadOperation<ViewRelationship> loadOp = context.Load(context.GetViewRelationshipByViewIDQuery(viewID), CallbackRelationships, null);

        }

        private void CallbackRelationships(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {

                lstRelationship.ItemsSource = loadOp.Entities;
                relationshipList = new List<ViewRelationship>();
                relationshipList = loadOp.Entities.ToList();


            }
        }


        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            ModifyRelationship modify = new ModifyRelationship(viewID, relationshipList);
            modify.Closed += new EventHandler(ChildWin_Closed);
            modify.Show();
        }
        void ChildWin_Closed(object sender, EventArgs e)
        {
            GetRelationshipsRefresh();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

            ViewRelationship rship = lstRelationship.SelectedItem as ViewRelationship;
            if (rship == null)
                return;

            string relation = rship.Summary;

            CustomMessage customMessage = new CustomMessage("Do you want to delete this relationship:\n " + relation + " ?", CustomMessage.MessageType.Confirm);

            customMessage.OKButton.Click += (obj, args) =>
            {
                var ws = WCF.GetService();
                ws.DeleteViewTableRelationshipAsync(rship.ID);
                GetRelationshipsRefresh();
            };
            customMessage.Show();


        }

    

        private void DeleteViewTable()
        {
            LoadOperation<ViewTable> loadOp = context.Load(context.GetViewTablesQuery().Where(vt => vt.Table_ID == tableID && vt.View_ID == viewID), CallbackViewTabels, null);
        }

        private void DeleteViewFieds()
        {
            LoadOperation<ViewField> loadOp = context.Load(context.GetViewFieldsQuery().Where(vt => vt.Table_ID == tableID && vt.View_ID == viewID), CallbackViewFields, null);
        }


        private void DeleteViewReleationship()
        {
            LoadOperation<ViewTableRelationship> loadOp = context.Load(context.GetViewTableRelationshipsQuery().Where(vt => (vt.Table_ID_A == tableID || vt.Table_ID_B == tableID) && vt.View_ID == viewID), CallbackViewReleationships, null);
        }

        private void CallbackViewTabels(LoadOperation<ViewTable> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewTable viewTable in loadOp.Entities)
                    {
                        context.ViewTables.Remove(viewTable);
                    }


                }
                DeleteViewFieds();
            }
            catch
            {
            }


        }


        private void CallbackViewFields(LoadOperation<ViewField> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewField viewTable in loadOp.Entities)
                    {
                        context.ViewFields.Remove(viewTable);
                    }


                }
                DeleteViewReleationship();
            }
            catch
            {
            }

        }


        private void CallbackViewReleationships(LoadOperation<ViewTableRelationship> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewTableRelationship viewRship in loadOp.Entities)
                    {
                        context.ViewTableRelationships.Remove(viewRship);
                    }


                }
            }
            catch
            {
            }

            context.SubmitChanges(submit => { if (submit.HasError) { Message.ErrorMessage(submit.Error.Message); context.RejectChanges(); } else { Message.InfoMessage("Successfully Saved!"); } }, null);

        }

      


    }
}
