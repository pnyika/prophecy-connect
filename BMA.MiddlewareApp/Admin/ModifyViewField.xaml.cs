﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ModifyViewField : ChildWindow
    {
        EditorContext context = new EditorContext();
        private int viewID;
        ObservableCollection<TempViewField> tempViewFields;
        private int tableID;
        private string tableName;
        List<ViewFieldSummary> fieldList;
        public ModifyViewField(int viewid, List<ViewFieldSummary> fields)
        {
            InitializeComponent();
            viewID = viewid;
            fieldList = fields;
           
            
            
        }



        private void radGridView_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                TempViewField dataObject = e.EditedItem as TempViewField;
               
                if ((dataObject != null))
                {
                
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.tempViewFields.Contains(dataObject))
                        {
                           
                                this.tempViewFields.Remove(dataObject);
                                this.tempViewFields.Add(dataObject);
                            
                        }
                        else
                        {
                            this.tempViewFields.Remove(dataObject);
                            this.tempViewFields.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (tempViewFields == null)
                return;
            EditorContext conext = new EditorContext();
            //foreach (TempViewField temp in tempViewFields)
            //{
            //    var query = from f in tempViewFields
            //                where f.DisplayName == temp.DisplayName
            //                select f;
            //    if (query.Count<TempViewField>() > 1)
            //        MessageBox.Show(temp.DisplayName +" is duplicated , display name must be unique");
            //}


            foreach(TempViewField temp in tempViewFields)
            {

                

                ViewField viewField = new ViewField();
                viewField.Table_ID = temp.TableID;
                viewField.View_ID = temp.ViewID;
                viewField.Feild_ID = temp.FeildID;
                viewField.DisplayName = temp.DisplayName;
                context.ViewFields.Add(viewField);

            }

            context.SubmitChanges(so =>
            {
                if (so.HasError)
                {
                    Message.ErrorMessage("An error has occurred while processing your request");
                }
                else
                {
                    Message.InfoMessage("Successfully saved! ");



                   
                        this.DialogResult = true;
                    
                }
            }, null);
           
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }



        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var items = lstFields.SelectedItems;

            if (items == null)
                return;
            if (tempViewFields == null)
                tempViewFields = new ObservableCollection<TempViewField>();

          
            foreach(var item in items)
            {

                Field field = item as Field;
                TempViewField temp = new TempViewField();
                temp.ViewID = viewID;
                temp.TableID = tableID;
                string dname = field.FieldName; ;
                dname = System.Text.RegularExpressions.Regex.Replace(dname, @"\B([A-Z])", " $1");
                dname = dname.Replace("_", " ");
                temp.DisplayName = dname;
                temp.Summry = tableName + "." + field.FieldName;
                temp.FeildID = field.ID;

               //check if field already exists
                var query = from f in tempViewFields
                            where f.FeildID == field.ID
                            select f;

                var query2 = from f in fieldList
                            where f.FieldID == field.ID
                            select f;

                if (!((query.Count<TempViewField>() > 0) || (query2.Count<ViewFieldSummary>() > 0)))
                
                    tempViewFields.Add(temp);
                

            }
            radGridView1.ItemsSource = tempViewFields;

        }




        private void GetTablesB(object sender, RoutedEventArgs e)
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackTableB, null);


        }
        private void CallbackTableB(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {


                lstTable.ItemsSource = loadOp.Entities;

            }
        }

        private void lstTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Table table = lstTable.SelectedItem as Table;
                        if (table == null)
                            return;

                        tableID = table.ID;
                        tableName = table.TableName;
          LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFields, null);

         }



        

        private void CallbackFields(LoadOperation<Field> loadOp)
        {


            if (loadOp != null)
            {
                lstFields.ItemsSource = loadOp.Entities;

               

            }
        }
    }
    public class TempViewField
    {
        public int ViewID {get;set;}
         public int TableID {get;set;}
        public int FeildID {get;set;}
        public string DisplayName {get;set;}
        public string Summry { get; set; }
    }
}

