﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewEOForm : Page
    {
        public ViewEOForm()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
      

          EditorContext context = new EditorContext();
      
     
        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GetForms();
        }

        private void GetForms()
        {
             
            LoadOperation<ObjectForm> loadOp = context.Load(context.GetObjectFormsQuery(), CallbackObjectForm, null);


        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            ObjectForm form = radGridView1.SelectedItem as ObjectForm;
            if (form == null)
                return;
            DesignForm.formID = form.ID;
            DesignForm.formName = form.FormName;
            AdminMain admin = new AdminMain();
            //admin.NavContent.Header = form.FormName;
            this.NavigationService.Navigate(new Uri("/DesignForm", UriKind.Relative));
        
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            CreateEOFrom form = new CreateEOFrom();
           form.CreateFormComplete += (s, ev) => { GetForms()
        ; };
           form.Show();
        }



        private void CallbackObjectForm(LoadOperation<ObjectForm> loadOp)
        {


            if (loadOp != null)
            {
                radGridView1.ItemsSource = loadOp.Entities;

            }
        }

        private void radButton1_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Test", UriKind.Relative));
        }

    }
}
