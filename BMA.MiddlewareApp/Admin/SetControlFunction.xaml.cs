﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using System.Collections;
using SilverlightMessageBox;
using System.ServiceModel.DomainServices.Client;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SetControlFunction : UserControl
    {
       
        IEnumerable _lookup;
        EditorContext context = new EditorContext();
        public event EventHandler SetParameters;
        public event EventHandler Close;
        private string connString = "";
        public static int ID;
        private int objectID = 0;
        private string Db = "";
        ObservableCollection<ObjectDefintion> objectList = new ObservableCollection<ObjectDefintion>();
        
        public SetControlFunction()
        {
            InitializeComponent();
            this.OKButton.IsEnabled = false;
            objectID = ID;
            getServer();
            loadFunctionTypes();
            GetObjects();

        }
        private void getServer()
        {
            //busyIndicator1.IsBusy = true;
            App app = (App)Application.Current;
            LoadOperation<Server> loadOp = context.Load(context.GetServersQuery(), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {

            if (loadOp != null)
            {
               
                ddlDatabase.ItemsSource = loadOp.Entities.ToList();

            }
        }

        private void loadFunctionTypes()
        {
            List<DB> types = new List<DB>();
            types.Add(new DB { Name = "Sql Text" });
            types.Add(new DB { Name = "Stored Procedure" });
            DDLFunctionType.ItemsSource = types;
        }


        private void GetObjects()
        {

            LoadOperation<ObjectDefintion> loadOp = context.Load(context.GetObjectDefintionsQuery().Where(o => o.ObjectForm_ID == DesignForm.formID), CallbackObject, null);


        }




        private void CallbackObject(LoadOperation<ObjectDefintion> results)
        {


            if (results != null)
            {

                ObservableCollection<ObjectDefintion> thisviewList = new ObservableCollection<ObjectDefintion>(results.Entities.Where(c => c.ObjectType == "RadGridView" || c.ObjectType == "SerialChart" || c.ObjectType == "PieChart"));
                foreach (var parameter in thisviewList)
                {

                    objectList.Add(parameter);
                }

                ddlDisplayControl.ItemsSource = objectList;

            }


        }



        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            Save(sender, e);
            //SetParameters(this, null);
        }


        private bool checkParameters = false;
        private void Save(object sender, RoutedEventArgs e)
        {
            Server server = ddlDatabase.SelectedItem as Server;
            DB type = DDLFunctionType.SelectedItem as DB;

            ObjectFunction function = new ObjectFunction();
            ObjectDefintion obj = ddlDisplayControl.SelectedItem as ObjectDefintion;
            function.ServerID = server.ID;

          //  function.Database = ddlDatabase.SelectedValue.ToString();
            if (type.Name == "Sql Text")
            {string sqlString = txtSQLText.Text;
                function.Query = sqlString;
                if (sqlString.Contains("where"))
                {
                    string[] Split = sqlString.Split(new Char[] { ' ' });
                    foreach (var item in Split)
                    {

                        if (item.StartsWith("@"))
                        {
                            checkParameters = true;
                        }
                    }

                }
            }
            else
            {
                DataObject proc = ddlStoreProc.SelectedItem as DataObject;
                function.Query = proc.GetFieldValue("Name").ToString();
                checkParameters = true;
            }

            function.ObjectDefinition_ID = objectID;
           // function.Server = txtSource.Text;
            function.FunctionType = type.Name;
            if (obj != null)
                function.TargetDisplayObject_ID = obj.ID;
            context.ObjectFunctions.Add(function);
            context.SubmitChanges(submit =>
            {
                if (!submit.HasError)
                {
                    if (checkParameters)
                    {
                        SetFunctionParameter.ID = function.ID;

                        SetFunctionParameter.objectFunction = function;
                       
                        SetFunctionParameter set = new SetFunctionParameter();
                        

                      
                        set.Show();
                        Close(this, null);
                    }
                    else
                    {
                        Message.InfoMessage("Successfully set");
                        Close(this, null);
                    }
                }
                else
                {
                    Message.ErrorMessage(submit.Error.Message);
                }

            }, null);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
           // this.DialogResult = false;
        }

     

 

        private void ddlDatabase_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                this.OKButton.IsEnabled = true;
               // connString = "Password=" + txtpass.Password + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + ddlDatabase.SelectedValue.ToString() + ";Data Source=" + txtSource.Text;
                //this.OKButton.IsEnabled = true;
            }
            catch { }
        }

        private void DDLFunctionType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            DB type = DDLFunctionType.SelectedItem as DB;
            if (type != null)
            {
                if (type.Name == "Sql Text")
                {
                    sqlT.Visibility = Visibility.Visible;
                    txtSQLText.Visibility = Visibility.Visible;

                    ddlStoreProc.Visibility = Visibility.Collapsed;
                    strProc.Visibility = Visibility.Collapsed;
                }
                if (type.Name == "Stored Procedure")
                {
                    Server server = ddlDatabase.SelectedItem as Server;
                    sqlT.Visibility = Visibility.Collapsed;
                    txtSQLText.Visibility = Visibility.Collapsed;

                    ddlStoreProc.Visibility = Visibility.Visible;
                    strProc.Visibility = Visibility.Visible;
                    string catalog = server.DatabaseName;
                    string sql = GetStoreProcSql(catalog);
                    GetStoreProcs(sql, catalog);
                }

            }
        }

        private string GetStoreProcSql(string DBName)
        {
            string sql = " ";
            sql += "SELECT SCHEMA_NAME(SCHEMA_ID) AS [Schema], ";
            sql += "SO.name AS [Name],";
            sql += "SO.Type_Desc AS [ObjectType (UDF/SP)] ";
            sql += "FROM " + DBName + ".sys.objects AS SO ";
            sql += "WHERE SO.TYPE IN ('P','FN') ";
            sql += "ORDER BY [Schema], SO.name ";
            return sql;
        }


        private void GetStoreProcs(string sql, string catalog)
        {

            Server server = ddlDatabase.SelectedItem as Server;
            connString = server.ConnectionString;
            this.radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcsCompleted);
            ws.GetDataSetDataAsync(connString, server.DatabaseName, sql, 1, 1000, null);
        }


        void ws_GetStoreProcsCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                // _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ddlStoreProc.ItemsSource = list;



                }
            }
            this.radBusyIndicator.IsBusy = false;
        }



       

        


       
    }
}

