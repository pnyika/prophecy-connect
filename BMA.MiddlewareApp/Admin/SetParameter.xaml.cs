﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using SilverlightMessageBox;
using System.Collections;
using System.Collections.ObjectModel;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SetParameter : UserControl
    {
        public static int ID;
        public static ObjectFunction objectFunction;
        public event EventHandler Successful;
        //public event EventHandler Load;
        EditorContext context = new EditorContext();
        ObservableCollection<ParameterData> parameters = new ObservableCollection<ParameterData>();
        private ObservableCollection<ObjectDefintion> parameterList = new ObservableCollection<ObjectDefintion>();
        public SetParameter()
        {

            this.Resources.Add("parameterList", parameterList);
           
            InitializeComponent();
           
            if (objectFunction != null)
            {
                GetStoreProcs(SqlQuery(objectFunction.Query));
            }

        }


        private void dgMyDataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            dgMyDataGrid.BeginEdit();
        }
        private void GetObjects()
        {

            LoadOperation<ObjectDefintion> loadOp = context.Load(context.GetObjectDefintionsQuery().Where(o => o.ObjectForm_ID == DesignForm.formID), CallbackObject, null);


        }




        private void CallbackObject(LoadOperation<ObjectDefintion> results)
        {


            if (results != null)
            {
                parameterList = new ObservableCollection<ObjectDefintion>();
                foreach (var parameter in results.Entities.Where(c => c.ObjectType == "TextBox" || c.ObjectType=="RadComboBox"))
                {

                    parameterList.Add(parameter);
                }

                
            }
            

        }



        private string SqlQuery(string procName)
        {
            
            string sql = "";
                    sql += " SELECT SCHEMA_NAME(SCHEMA_ID) AS [Schema], ";
                     sql += "   SO.name AS [ObjectName], ";
                      sql += "  SO.Type_Desc AS [ObjectType (UDF/SP)],";
                      sql += "  P.parameter_id AS [ParameterID],";
                      sql += "  P.name AS [ParameterName],";
                      sql += "  TYPE_NAME(P.user_type_id) AS [ParameterDataType],";
                       sql += " P.max_length AS [ParameterMaxBytes],";
                      sql += "  P.is_output AS [IsOutPutParameter]";
                      sql += "  FROM " + objectFunction.Server.DatabaseName + ".sys.objects AS SO";
                      sql += "  Left Outer JOIN " + objectFunction.Server.DatabaseName + ".sys.parameters AS P  ON SO.OBJECT_ID = P.OBJECT_ID";
                       sql += " WHERE SO.TYPE IN ('P','FN')";
                       sql += "  and SO.name = '"+procName+"'";
                      sql += "  ORDER BY [Schema], SO.name, P.parameter_id";
            return sql;
        }

        private void GetStoreProcs(string sql)
        {
           if (objectFunction != null)
            {

            string connString = objectFunction.Server.ConnectionString;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcsCompleted);
            ws.GetDataSetDataAsync(connString, objectFunction.Server.DatabaseName, sql, 1, 1000, "");
        }
        }


        void ws_GetStoreProcsCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                // _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);

                parameters = new ObservableCollection<ParameterData>();
                foreach (DataObject item in list)
                {

                    ParameterData para = new ParameterData();
                    ObjectDefintion obj = new ObjectDefintion();
                    string name = item.GetFieldValue("ParameterName").ToString().Replace('@',' ');
                    para.Name = name.TrimStart();
                    para.DataType = item.GetFieldValue("ParameterDataType").ToString();
                    para.isOutputParamter = Convert.ToBoolean(item.GetFieldValue("IsOutPutParameter"));
                    para.MaxSize = Convert.ToInt32(item.GetFieldValue("ParameterMaxBytes"));
                  
                    para.ObjectDef = obj;
                    parameters.Add(para);
                }

                dgMyDataGrid.ItemsSource = parameters;
                GetObjects();
            }
            // this.radBusyIndicator.IsBusy = false;
        }

        private void Save()
        {

            foreach (DataGridRow rowItem in DataGridExtensions.GetRows(dgMyDataGrid))
            {
                ParameterData data = rowItem.DataContext as ParameterData;

                if (data.ObjectDef != null)
                {


                    ObjectFunctionParameter para = new ObjectFunctionParameter();
                    para.DataType = data.DataType;
                    para.isOutput = data.isOutputParamter;
                    para.MaxSize = data.MaxSize;
                    para.ObjectDefinition_ID = data.ObjectDef.ID;
                    para.ObjectFunction_ID = ID;
                    para.ParameterName = para.ParameterName;

                }

            }
            context.SubmitChanges(sub =>
            {
                if (sub.HasError) { Message.ErrorMessage(sub.Error.Message); }
                else
                {
                    Message.InfoMessage("Successfully saved!");
                    Successful(this, null);
                }
            }, null);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
           
            Save();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
         
           // this.Resources.Add("ViewList", parameterList);
            if (objectFunction != null)
            {
                GetStoreProcs(SqlQuery(objectFunction.Query));
            }
           // Successful(this, null);
        }

        private void dgMyDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            //ComboBox ddlObject = dgMyDataGrid.FindName("cbxFuelType") as ComboBox;
            //ddlObject.ItemsSource = parameterList;
        }

        private void dgMyDataGrid_LoadingRowDetails(object sender, DataGridRowDetailsEventArgs e)
        {
          
        }

    }




    public class ParameterData
    {
        public int ID { get; set; }
        public int MaxSize { get; set; }
        public string Name { get; set; }
        public string DataType { get; set; }
        public string ObjectName { get; set; }
        public bool isOutputParamter { get; set; }
        public ObjectDefintion ObjectDef { get; set; }
    }
}
