﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SetViewGroupFilter : ChildWindow
    {
        EditorContext context = new EditorContext();
        ObservableCollection<View> viewList = new ObservableCollection<View>();
        ObservableCollection<View> currentView = new ObservableCollection<View>();
        ObservableCollection<Field> fieldList = new ObservableCollection<Field>();
        Field thisField = new Field();
        ObservableCollection<GroupViewFilter> FilterList = new ObservableCollection<GroupViewFilter>();
        View seletedView = new View();
        List<View> addedView = new List<View>();
        List<GroupViewFilter> removedViewFilter = new List<GroupViewFilter>();
        List<GroupViewFilter> newGroupViewFilterList = new List<GroupViewFilter>();
        int groupID = 0;

        List<TempDisplay> tempDisplay1 = new List<TempDisplay>();
        List<TempDisplay> tempDisplay2 = new List<TempDisplay>();
        List<TempDisplay> tempDisplay3 = new List<TempDisplay>();
        public SetViewGroupFilter(Group thisGroup)
        {
            InitializeComponent();
            groupID= thisGroup.ID;
            if (thisGroup.GroupName != string.Empty)
            {
                txtGroupName.Text = thisGroup.GroupName;
            }

            loadViews();
            PopulateDropdowns();
        }


        private void PopulateDropdowns()
        {
            string lessThan = "lessThan";
            string lessThanEqual = "lessThanEqual";
            string greaterThan = "greaterThan";
            string greaterThanEqual = "greaterThanEqual";
            string notEquals = "notEquals";
            string equals = "Equals";

            tempDisplay1 = new List<TempDisplay>();
            tempDisplay1.Add(new TempDisplay { Display = "Is equals to", Value = equals });
            tempDisplay1.Add(new TempDisplay { Display = "Is not equals to", Value = notEquals });
            tempDisplay1.Add(new TempDisplay { Display = "Contains", Value = "in" });
            tempDisplay1.Add(new TempDisplay { Display = "Does not contain", Value = "not in" });
            tempDisplay1.Add(new TempDisplay { Display = "Is less than", Value = lessThan });
            tempDisplay1.Add(new TempDisplay { Display = "Is less than or equal to", Value = lessThanEqual });
            tempDisplay1.Add(new TempDisplay { Display = "Is greater than", Value = greaterThan });
            tempDisplay1.Add(new TempDisplay { Display = "Is greater than or equal to", Value = greaterThanEqual });
            ddlFilter1.ItemsSource = tempDisplay1;

            //second
            tempDisplay2 = new List<TempDisplay>();
            tempDisplay2.Add(new TempDisplay { Display = "", Value = "" });
            tempDisplay2.Add(new TempDisplay { Display = "And", Value = "and" });
            tempDisplay2.Add(new TempDisplay { Display = "Or", Value = "or" });
            ddlFilter2.ItemsSource = tempDisplay2;

            //third
            tempDisplay3 = new List<TempDisplay>();
            tempDisplay3.Add(new TempDisplay { Display = "", Value = "" });
            tempDisplay3.Add(new TempDisplay { Display = "Is equals to", Value = equals });
            tempDisplay3.Add(new TempDisplay { Display = "Is not equals to", Value = notEquals });
            tempDisplay3.Add(new TempDisplay { Display = "Contains", Value = "in" });
            tempDisplay3.Add(new TempDisplay { Display = "Does not contain", Value = "not in" });
            tempDisplay3.Add(new TempDisplay { Display = "Is less than", Value = lessThan });
            tempDisplay3.Add(new TempDisplay { Display = "Is less than or equal to", Value = lessThanEqual });
            tempDisplay3.Add(new TempDisplay { Display = "Is greater than", Value = greaterThan });
            tempDisplay3.Add(new TempDisplay { Display = "Is greater than or equal to", Value = greaterThanEqual });
            ddlFilter3.ItemsSource = tempDisplay3;


        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //Group group = new Group();
            //group.GroupName = txtGroupName.Text;
            //group.Description = "";
            //context.Groups.Add(group);


            foreach (GroupViewFilter viewFilter in newGroupViewFilterList)
            {

                context.GroupViewFilters.Add(viewFilter);
            }

            foreach (GroupViewFilter viewFilter in removedViewFilter)
            {
                if(viewFilter.ID > 0)
                    context.GroupViewFilters.Remove(viewFilter);
            }       
            context.SubmitChanges(submit =>
            {
                if (!submit.HasError)
                {
                    Message.InfoMessage("Successful");
                    this.DialogResult = true;
                }
                else
                {
                    Message.ErrorMessage(submit.Error.Message);
                }
            }, null);

        }

        private void CreateFilterSummary(GroupViewFilter filter)
        
        {
            string summary = "";
           // Field field = fieldList.Where(f => f.ID == filter.Field_ID).FirstOrDefault();
            summary += filter.Field.FieldName + " " + filter.Operator + " " + filter.Value ;
            if (filter.OrAnd != null)
            {
                summary += " " + filter.OrAnd + " " + filter.Field.FieldName + " " + filter.SecondOperator + " " + filter.SecondValue ;
            }
            filter.Summary = summary;
            if (FilterList == null)
             FilterList = new ObservableCollection<GroupViewFilter>();
            FilterList.Add(filter);
        }


        private string ReplaceOperator(string operatr)
        {
            string optr = operatr;

            if (operatr == "lessThan")
            {
                optr = "<";
            }
            if (operatr == "lessThanEqual")
            {
                optr = "<=";
            }
             if (operatr == "greaterThan")
            {
                optr = ">";
            }
                
            if (operatr == "greaterThanEqual")
            {
                optr = ">=";
            }
            if (operatr == "notEquals")
            {
                optr = "!=";
            }
            if (operatr == "Equals")
            {
                optr = "=";
            }

            return optr;
            

        }
   
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        private void LoadViewFilter(int groupID, int viewID)
        {
            LoadOperation<GroupViewFilter> loadOp = context.Load(context.GetGroupViewFiltersByGroupIDViewIDQuery(groupID, viewID), CallbackGroupViewFilter, null);
           
        }

        private void CallbackGroupViewFilter(LoadOperation<GroupViewFilter> result)
        {


            if (result != null)
            {
               
               foreach(GroupViewFilter filter in result.Entities)
               {
                   CreateFilterSummary(filter);
               }
                lstFilter.ItemsSource = FilterList;

            }
        }

        private void loadViews()
        {

            LoadOperation<View> loadOp = context.Load(context.GetViewsQuery(), CallbackViews, null);




        }


        private void CallbackViews(LoadOperation<View> result)
        {

            
            if (result != null)
            {
               currentView = new ObservableCollection<View>(result.Entities.Where(v => v.GroupViews.Any(ug => ug.Group_ID== groupID)).OrderBy(si => si.ViewName));

               
                lstView.ItemsSource = currentView;

            }
        }


        private void GetTables(int viewID)
        {
            radBusyIndicator.IsBusy = true;
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackTable, null);


        }
        private void CallbackTable(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                ddlTable.ItemsSource = loadOp.Entities ;



            }
            radBusyIndicator.IsBusy = false;
        }


        private void ddlTable_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            Table table = ddlTable.SelectedItem as Table;
            if (table != null)
            {
              
                LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFieldA, null);
            }



        }

        private void CallbackFieldA(LoadOperation<Field> result)
        {


            if (result != null)
            {
               
                fieldList = new ObservableCollection<Field>(result.Entities);
                lstField.ItemsSource = fieldList;

            }
        }
  

 

        private void lstView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            View view = lstView.SelectedItem as View;
            if(view == null)
                return;
            seletedView = view;
            ClearData();
            fieldList.Clear();// = new ObservableCollection<Field>();
            FilterList.Clear();
            GetTables(view.ID);
            LoadViewFilter(groupID, view.ID);
        }



        private void btnRemoveFilter_Click(object sender, RoutedEventArgs e)
        {
            GroupViewFilter filter = lstFilter.SelectedItem as GroupViewFilter;
            if (filter != null)
            {
                removedViewFilter.Add(filter);
                FilterList.Remove(filter);
            }
        }



        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            GroupViewFilter filter = new GroupViewFilter();
            Field field = lstField.SelectedItem as Field;
            if (field != null)

                if (ddlFilter1.SelectedItem != null &&   txtField1.Text.Trim() != "")
                {
                    thisField = field;
                    TempDisplay display = ddlFilter1.SelectedItem as TempDisplay;
                    if (display != null)
                    {
                        filter.Field_ID = field.ID;
                        filter.GroupView_ID = seletedView.GroupViews.Where(v => v.Group_ID == groupID).FirstOrDefault().ID;
                        filter.Operator = ReplaceOperator(display.Value);
                        filter.Value = txtField1.Text;
                        filter.Field = field;

                        if (ddlFilter2.SelectedItem != null && ddlFilter3.SelectedItem != null && txtField2.Text.Trim() != "")
                        {
                            TempDisplay display2 = ddlFilter2.SelectedItem as TempDisplay;
                            if (display2 != null)
                            {
                               

                                TempDisplay display3 = ddlFilter3.SelectedItem as TempDisplay;
                                if (display3 != null)
                                {
                                    if (display2.Value != "" && display3.Value != "")
                                    {
                                        filter.OrAnd = display2.Value;
                                        filter.SecondOperator = ReplaceOperator(display3.Value);
                                        filter.SecondValue = txtField2.Text;
                                    }
                                }
                            }
                        }

                         newGroupViewFilterList.Add(filter);
                         CreateFilterSummary(filter);
                    }
                }




            ClearData();
        }

        private void lstField_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Field field = lstField.SelectedItem as Field;
            if (field != null)
                tbkField.Text = field.FieldName;
           
        }

        private void ClearData()
        {
            tbkField.Text = "";
            
            txtField1.Text = "";
            txtField2.Text = "";

        }
    }
}

