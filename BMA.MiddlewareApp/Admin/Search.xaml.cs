﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.Admin
{
    public partial class Search : UserControl
    {
        public Search()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(Search_Loaded);
            Advanced.Click += new RoutedEventHandler(Advanced_Click);
            Basic.Click += new RoutedEventHandler(Basic_Click);
        }

        void Search_Loaded(object sender, RoutedEventArgs e)
        {
            StartingPosition.Begin();
            //TODO: Load comboboxes..
        }

        void Advanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedSearch.Begin();
        }
        void Basic_Click(object sender, RoutedEventArgs e)
        {
            BasicSearch.Begin();
        }
    }
}

