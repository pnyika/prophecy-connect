﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.Collections;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
//using BMA.MiddlewareApp.DataTableService;


namespace BMA.MiddlewareApp.Admin
{
    public partial class SelectTables : ChildWindow
    {
        EditorContext context = new EditorContext();
        IEnumerable _lookup;
        ObservableCollection<DataTableService.DataTableInfo> _tables;
       
        private string databaseName = "Middleware";
        private string connString;
        private string serverName;
        private int serverID;
        public SelectTables(string connstr, string db, string server, int sID)
        {
            InitializeComponent();
            this.connString = connstr;
            this.databaseName = db ;
            this.serverID = sID;
            string sql = "use [" + db + "]; Select name,Object_ID, 1 as IsTable from sys.tables where type_desc = 'user_table' union  Select name,Object_ID, 0 as IsTable from sys.views  order by IsTable desc,name asc ";
            GetTables(sql, 1, 500, "Tables");
        }



        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            App app = (App)Application.Current;
            if (app.tableList == null)
            {
                   List<Table> tlist = new List<Table>();
                   app.tableList = tlist;
            }

            if (app.tablebjectList == null)
                app.tablebjectList = new ObservableCollection<Table>();
            try
            {
                if (this.radGridView1.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();

              
             
                foreach (var item in radGridView1.SelectedItems)
                {

                    DataObject it = item as DataObject;
                    //  string test = it.GetFieldValue("name").ToString();

                    Table table = new Table();
                    table.ID = app.tableID + 1;
                    app.tableID = table.ID;
                    table.DBName = "databaseName";
                    table.Server_ID = serverID;
                    if (it.GetFieldValue("IsTable").ToString() == "1")
                        table.IsTable = true;
                    else
                        table.IsTable = false;

                    string tname = it.GetFieldValue("name").ToString();
                    table.TableName = tname;

                    var tbls = from t in app.tablebjectList
                               where t.TableName == tname
                               select t;
                    if (tbls.Count<Table>() == 0)
                    {
                        app.tablebjectList.Add(table);

                        app.tableList.Add(table);
                    }

                }


            }
            catch(Exception ex)
            {
              Message.ErrorMessage(ex.Message);
            }

            this.DialogResult = false;
        }



        private void InsertServer()
        {
            Server server = new Server();
           // int serverID = context.
            
        }



        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }



        private void GetTables(string sql, int pagenumber, int pagesize, object userState)
        {
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }





        void ws_GetDataSetDataCompleted(object sender, BMA.MiddlewareApp.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;

                else
                {


                    radGridView1.ItemsSource = list;



                }
            }

        }
    }


    public class Item
    {
        public string name { get; set; }
        public string Object_ID { get; set; }
        public int IsTable { get; set; }

    }
}

