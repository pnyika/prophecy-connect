﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddView : ChildWindow
    {
        public AddView()
        {
            InitializeComponent();
            OKButton.IsEnabled = false;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            addView();
          
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        private void addView()
        {
            bool isReadOnly = false;
            bool isSingle = false;
            if (chkIsReadyOnly.IsChecked == true)
            {
                isReadOnly = true;
            }
            if (chkSingleTable.IsChecked == true)
            {
                isSingle = true;
            }
            EditorContext context = new EditorContext();
                View view = new View();
                view.ViewName = txtViewName.Text;
                view.ReadOnly = isReadOnly;
                view.SingleTable = isSingle;
                context.Views.Add(view);
           
          try
           {
               context.SubmitChanges(submit =>
               {
                   if (submit.HasError) { MessageBox.Show("An Error occured"); }
                   else
                   {
                       ViewList viewList = new ViewList();
                       viewList.viewList = new System.Collections.ObjectModel.ObservableCollection<View>();
                       viewList.viewList.Add(view);
                       MessageBox.Show("Successfully saved");
                       this.DialogResult = true;
                   }
               }, null);
            }
            catch
            {
            }




        }

        private void txtViewName_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);

        }
        public void EnableOrDisableOKButton(object sender, RoutedEventArgs e)
        {
            if (txtViewName.Text.Length < 1)
                OKButton.IsEnabled = false;
            else
            {
                OKButton.IsEnabled = true;
               
            }
        }
    }
}

