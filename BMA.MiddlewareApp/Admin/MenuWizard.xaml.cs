﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BMA.MiddlewareApp.ViewModel;

namespace BMA.MiddlewareApp.Admin
{
    public partial class MenuWizard : Page
    {
      private readonly MenuWidzardViewModel vm;
      public MenuWizard()
        {
            InitializeComponent();

            vm = new MenuWidzardViewModel();
            DataContext = vm;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

    }
}
