﻿
namespace BMA.MiddlewareApp.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // Implements application logic using the MiddlewareEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class DomainService1 : LinqToEntitiesDomainService<MiddlewareEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'StoredProcedureNames' query.
        public IQueryable<StoredProcedureName> GetStoredProcedureNames()
        {
            return this.ObjectContext.StoredProcedureNames;
        }

        public void InsertStoredProcedureName(StoredProcedureName storedProcedureName)
        {
            if ((storedProcedureName.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureName, EntityState.Added);
            }
            else
            {
                this.ObjectContext.StoredProcedureNames.AddObject(storedProcedureName);
            }
        }

        public void UpdateStoredProcedureName(StoredProcedureName currentStoredProcedureName)
        {
            this.ObjectContext.StoredProcedureNames.AttachAsModified(currentStoredProcedureName, this.ChangeSet.GetOriginal(currentStoredProcedureName));
        }

        public void DeleteStoredProcedureName(StoredProcedureName storedProcedureName)
        {
            if ((storedProcedureName.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureName, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.StoredProcedureNames.Attach(storedProcedureName);
                this.ObjectContext.StoredProcedureNames.DeleteObject(storedProcedureName);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'StoredProcedureParameters' query.
        public IQueryable<StoredProcedureParameter> GetStoredProcedureParameters()
        {
            return this.ObjectContext.StoredProcedureParameters;
        }

        public void InsertStoredProcedureParameter(StoredProcedureParameter storedProcedureParameter)
        {
            if ((storedProcedureParameter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureParameter, EntityState.Added);
            }
            else
            {
                this.ObjectContext.StoredProcedureParameters.AddObject(storedProcedureParameter);
            }
        }

        public void UpdateStoredProcedureParameter(StoredProcedureParameter currentStoredProcedureParameter)
        {
            this.ObjectContext.StoredProcedureParameters.AttachAsModified(currentStoredProcedureParameter, this.ChangeSet.GetOriginal(currentStoredProcedureParameter));
        }

        public void DeleteStoredProcedureParameter(StoredProcedureParameter storedProcedureParameter)
        {
            if ((storedProcedureParameter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureParameter, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.StoredProcedureParameters.Attach(storedProcedureParameter);
                this.ObjectContext.StoredProcedureParameters.DeleteObject(storedProcedureParameter);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ViewFields' query.
        public IQueryable<ViewField> GetViewFields()
        {
            return this.ObjectContext.ViewFields;
        }

        public void InsertViewField(ViewField viewField)
        {
            if ((viewField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewField, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ViewFields.AddObject(viewField);
            }
        }

        public void UpdateViewField(ViewField currentViewField)
        {
            this.ObjectContext.ViewFields.AttachAsModified(currentViewField, this.ChangeSet.GetOriginal(currentViewField));
        }

        public void DeleteViewField(ViewField viewField)
        {
            if ((viewField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewField, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ViewFields.Attach(viewField);
                this.ObjectContext.ViewFields.DeleteObject(viewField);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ViewTables' query.
        public IQueryable<ViewTable> GetViewTables()
        {
            return this.ObjectContext.ViewTables;
        }

        public void InsertViewTable(ViewTable viewTable)
        {
            if ((viewTable.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewTable, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ViewTables.AddObject(viewTable);
            }
        }

        public void UpdateViewTable(ViewTable currentViewTable)
        {
            this.ObjectContext.ViewTables.AttachAsModified(currentViewTable, this.ChangeSet.GetOriginal(currentViewTable));
        }

        public void DeleteViewTable(ViewTable viewTable)
        {
            if ((viewTable.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewTable, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ViewTables.Attach(viewTable);
                this.ObjectContext.ViewTables.DeleteObject(viewTable);
            }
        }
    }
}


