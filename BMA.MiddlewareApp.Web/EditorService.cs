﻿
namespace BMA.MiddlewareApp.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using System.Data.Linq;
    using BMA.DataSetSerialiser;
    using System.Web.Security;
    using System.Web;


    // Implements application logic using the MiddlewareEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class EditorService : LinqToEntitiesDomainService<MiddlewareEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'DisplayDefinitions' query.
        public IQueryable<DisplayDefinition> GetDisplayDefinitions()
        {

         var listDisplayDef =   this.ObjectContext.DisplayDefinitions.Include("FormType");
         return listDisplayDef;
        }

        public void InsertDisplayDefinition(DisplayDefinition displayDefinition)
        {
            if ((displayDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(displayDefinition, EntityState.Added);
            }
            else
            {
                this.ObjectContext.DisplayDefinitions.AddObject(displayDefinition);
            }
        }

        public void UpdateDisplayDefinition(DisplayDefinition currentDisplayDefinition)
        {
            this.ObjectContext.DisplayDefinitions.AttachAsModified(currentDisplayDefinition, this.ChangeSet.GetOriginal(currentDisplayDefinition));
        }

        public void DeleteDisplayDefinition(DisplayDefinition displayDefinition)
        {
            if ((displayDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(displayDefinition, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.DisplayDefinitions.Attach(displayDefinition);
                this.ObjectContext.DisplayDefinitions.DeleteObject(displayDefinition);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Fields' query.
        public IQueryable<Field> GetFields()
        {
            return this.ObjectContext.Fields;
        }

        public void InsertField(Field field)
        {
            if ((field.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(field, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Fields.AddObject(field);
            }
        }

        public void UpdateField(Field currentField)
        {
            this.ObjectContext.Fields.AttachAsModified(currentField, this.ChangeSet.GetOriginal(currentField));
        }

        public void DeleteField(Field field)
        {
            if ((field.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(field, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Fields.Attach(field);
                this.ObjectContext.Fields.DeleteObject(field);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'FormTypes' query.
        public IQueryable<FormType> GetFormTypes()
        {
            return this.ObjectContext.FormTypes;
        }


        [Query(IsComposable = false)]
        public IQueryable<FormType> GetFormTypeByMenuID(int MenuID)
        {
         

            return   this.ObjectContext.MenuStructures.Join(ObjectContext.MenuItems, c => (c.ID), a => a.MenuStructure_ID, (c, a) => new { c = c, a = a }).Join(ObjectContext.DisplayDefinitions, 
                       r => (r.a.DisplayDefinition_ID), co => co.ID,(r, co) => new { r = r, co = co }).Where(temp1 => (temp1.r.a.ID == MenuID)).Select(temp1 => temp1.co.FormType);
        }





        public void InsertFormType(FormType formType)
        {
            if ((formType.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(formType, EntityState.Added);
            }
            else
            {
                this.ObjectContext.FormTypes.AddObject(formType);
            }
        }

        public void UpdateFormType(FormType currentFormType)
        {
            this.ObjectContext.FormTypes.AttachAsModified(currentFormType, this.ChangeSet.GetOriginal(currentFormType));
        }

        public void DeleteFormType(FormType formType)
        {
            if ((formType.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(formType, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.FormTypes.Attach(formType);
                this.ObjectContext.FormTypes.DeleteObject(formType);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'GridDefinitions' query.
        public IQueryable<GridDefinition> GetGridDefinitions()
        {
            return this.ObjectContext.GridDefinitions;
        }

        public void InsertGridDefinition(GridDefinition gridDefinition)
        {
            if ((gridDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(gridDefinition, EntityState.Added);
            }
            else
            {
                this.ObjectContext.GridDefinitions.AddObject(gridDefinition);
            }
        }

        public void UpdateGridDefinition(GridDefinition currentGridDefinition)
        {
            this.ObjectContext.GridDefinitions.AttachAsModified(currentGridDefinition, this.ChangeSet.GetOriginal(currentGridDefinition));
        }

        public void DeleteGridDefinition(GridDefinition gridDefinition)
        {
            if ((gridDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(gridDefinition, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.GridDefinitions.Attach(gridDefinition);
                this.ObjectContext.GridDefinitions.DeleteObject(gridDefinition);
            }
        }







        public IQueryable<GridRelationship> GetGridRelationship()
        {
            return this.ObjectContext.GridRelationships;
        }

        public void InsertGridRelationship(GridRelationship gridRelationship)
        {
            if ((gridRelationship.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(gridRelationship, EntityState.Added);
            }
            else
            {
                this.ObjectContext.GridRelationships.AddObject(gridRelationship);
            }
        }

        public void UpdateGridRelationship(GridRelationship currentGridRelationship)
        {
            this.ObjectContext.GridRelationships.AttachAsModified(currentGridRelationship, this.ChangeSet.GetOriginal(currentGridRelationship));
        }

        public void DeleteGridRelationship(GridRelationship gridRelationship)
        {
            if ((gridRelationship.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(gridRelationship, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.GridRelationships.Attach(gridRelationship);
                this.ObjectContext.GridRelationships.DeleteObject(gridRelationship);
            }
        }
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'MenuItems' query.
        public IQueryable<MenuItem> GetMenuItems()
        {
            return this.ObjectContext.MenuItems.Include("DisplayDefinition");
        }

        public void InsertMenuItem(MenuItem menuItem)
        {
            if ((menuItem.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(menuItem, EntityState.Added);
            }
            else
            {
                this.ObjectContext.MenuItems.AddObject(menuItem);
            }
        }

        public void UpdateMenuItem(MenuItem currentMenuItem)
        {
            this.ObjectContext.MenuItems.AttachAsModified(currentMenuItem, this.ChangeSet.GetOriginal(currentMenuItem));
        }

        public void DeleteMenuItem(MenuItem menuItem)
        {
            if ((menuItem.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(menuItem, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.MenuItems.Attach(menuItem);
                this.ObjectContext.MenuItems.DeleteObject(menuItem);
            }
        }



        public IQueryable<FieldCompletionOption> GetFieldCompletionOptions()
        {
            return this.ObjectContext.FieldCompletionOptions;
        }

        public void InsertFieldCompletionOption(FieldCompletionOption fieldCompletionOption)
        {
            if ((fieldCompletionOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(fieldCompletionOption, EntityState.Added);
            }
            else
            {
                this.ObjectContext.FieldCompletionOptions.AddObject(fieldCompletionOption);
            }
        }

        public void UpdateFieldCompletionOption(FieldCompletionOption currentFieldCompletionOption)
        {
            this.ObjectContext.FieldCompletionOptions.AttachAsModified(currentFieldCompletionOption, this.ChangeSet.GetOriginal(currentFieldCompletionOption));
        }

        public void DeleteFieldCompletionOption(FieldCompletionOption fieldCompletionOption)
        {
            if ((fieldCompletionOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(fieldCompletionOption, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.FieldCompletionOptions.Attach(fieldCompletionOption);
                this.ObjectContext.FieldCompletionOptions.DeleteObject(fieldCompletionOption);
            }
        }
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'MenuStructures' query.
        public IQueryable<MenuStructure> GetMenuStructures()
        {
            return this.ObjectContext.MenuStructures;
        }

        public void InsertMenuStructure(MenuStructure menuStructure)
        {
            if ((menuStructure.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(menuStructure, EntityState.Added);
            }
            else
            {
                this.ObjectContext.MenuStructures.AddObject(menuStructure);
            }
        }

        public void UpdateMenuStructure(MenuStructure currentMenuStructure)
        {
            this.ObjectContext.MenuStructures.AttachAsModified(currentMenuStructure, this.ChangeSet.GetOriginal(currentMenuStructure));
        }

        public void DeleteMenuStructure(MenuStructure menuStructure)
        {
            if ((menuStructure.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(menuStructure, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.MenuStructures.Attach(menuStructure);
                this.ObjectContext.MenuStructures.DeleteObject(menuStructure);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Servers' query.
        public IQueryable<Server> GetServers()
        {
            return this.ObjectContext.Servers;
        }

        public IQueryable<Server> GetServersByTableID(int tableID)
        {
            return this.ObjectContext.Servers.Join(this.ObjectContext.Tables, s => (s.ID), t => t.Server_ID, (s, t) => new { s = s, t = t }).Where(temp1 => (temp1.t.ID == tableID)).Select(temp1 => temp1.s); ;
        }


        public void InsertServer(Server server)
        {
            if ((server.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(server, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Servers.AddObject(server);
            }
          
        }

        public void UpdateServer(Server currentServer)
        {
            this.ObjectContext.Servers.AttachAsModified(currentServer, this.ChangeSet.GetOriginal(currentServer));
        }

        public void DeleteServer(Server server)
        {
            if ((server.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(server, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Servers.Attach(server);
                this.ObjectContext.Servers.DeleteObject(server);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Settings' query.
        public IQueryable<Setting> GetSettings()
        {
            return this.ObjectContext.Settings;
        }

        public void InsertSetting(Setting setting)
        {
            if ((setting.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(setting, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Settings.AddObject(setting);
            }
        }

        public void UpdateSetting(Setting currentSetting)
        {
            this.ObjectContext.Settings.AttachAsModified(currentSetting, this.ChangeSet.GetOriginal(currentSetting));
        }

        public void DeleteSetting(Setting setting)
        {
            if ((setting.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(setting, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Settings.Attach(setting);
                this.ObjectContext.Settings.DeleteObject(setting);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Tables' query.

        public IQueryable<Table> GetTables()
        {
            return this.ObjectContext.Tables.Include("Server");
        }

        [Query(IsComposable = false)]
        public IQueryable<Table> GetTablesByFormID(int formID)
        {

            return        this.ObjectContext.DisplayDefinitions.Join(this.ObjectContext.GridDefinitions, c => (c.ID), a => a.DisplayDefinition_ID, (c, a) =>
                            new { c = c, a = a }).Where(a => a.c.FormType_ID == formID).Join(this.ObjectContext.Views, r => (r.a.View_ID), co => co.ID, (r, co) => new
                         {r = r, co = co }).Join(this.ObjectContext.ViewTables, v => (v.co.ID), vt => vt.View_ID, (v, vt) => new { v = v, vt = vt }).Select(temp1 => temp1.vt.Table);
           
        }


        [Query(IsComposable = false)]
        public IQueryable<Table> GetTablesByDisplayDefID(int displayDefID)
        {

            //return this.ObjectContext.GridDefinitions.Join(this.ObjectContext.ViewTables, c => (c.ID), a => a.View_ID, (c, a) =>
            //                new { c = c, a = a }).Where(a => a.c.DisplayDefinition_ID == displayDefID).Join(this.ObjectContext.Views, r => (r.a.View_ID), co => co.ID, (r, co) => new { r = r, co = co }).Join(this.ObjectContext.ViewTables, v => (v.co.ID), vt => vt.View_ID, (v, vt) => new { v = v, vt = vt }).Select(temp1 => temp1.vt.Table);


            return this.ObjectContext.GridDefinitions.Join(this.ObjectContext.ViewTables, v => (v.View_ID), vt => vt.View_ID, (v, vt) => new { v = v, vt = vt }).Where(v => v.v.DisplayDefinition_ID == displayDefID).Select(temp1 => temp1.vt.Table);
        }

        [Query(IsComposable = false)]
        public IQueryable<Table> GetTablesByViewID(int viewID)
        {

            //return this.ObjectContext.Vi.Join(this.ObjectContext.ViewTables, v => (v.ID), vt => vt.View_ID, (v, vt) => new { v = v, vt = vt }).Where(v => v.vt.View_ID ==viewID).Select(temp1 => temp1.vt.Table);

             return this.ObjectContext.Tables.Include("Server").Where(t => t.ViewTables.Any(c => c.View_ID== viewID));
        }


        public void InsertTable(Table table)
        {
            if ((table.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(table, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Tables.AddObject(table);
            }
           
        }

        public void UpdateTable(Table currentTable)
        {
            this.ObjectContext.Tables.AttachAsModified(currentTable, this.ChangeSet.GetOriginal(currentTable));
        }

        public void DeleteTable(Table table)
        {
            if ((table.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(table, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Tables.Attach(table);
                this.ObjectContext.Tables.DeleteObject(table);
            }
        }



        public IQueryable<DropDownPairResult> GetDropDownPairResult(int fieldID)
        {
            return this.ObjectContext.GetDropDownPair(fieldID).AsQueryable();
        }

         [Query]
        public IEnumerable<DropDownPairResult> LoadDropDownPairResult(int fieldID)
        {
            List<DropDownPairResult> dropList = new List<DropDownPairResult>();
            var list = this.ObjectContext.GetDropDownPair(fieldID).ToList();
            if (list != null)
            {
               var first = list.FirstOrDefault();
               if (first != null)
               {
                   if (first.ConnectionString != null)
                   {
                       dropList = list;
                   }
               }
            }
            return dropList.AsQueryable();
        }
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'UserGridConfigs' query.
        public IQueryable<UserGridConfig> GetUserGridConfigs()
        {
            return this.ObjectContext.UserGridConfigs.Include("Setting");
        }

        public void InsertUserGridConfig(UserGridConfig userGridConfig)
        {
            if ((userGridConfig.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(userGridConfig, EntityState.Added);
            }
            else
            {
                this.ObjectContext.UserGridConfigs.AddObject(userGridConfig);
            }
        }

        public void UpdateUserGridConfig(UserGridConfig currentUserGridConfig)
        {
            this.ObjectContext.UserGridConfigs.AttachAsModified(currentUserGridConfig, this.ChangeSet.GetOriginal(currentUserGridConfig));
        }

        public void DeleteUserGridConfig(UserGridConfig userGridConfig)
        {
            if ((userGridConfig.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(userGridConfig, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.UserGridConfigs.Attach(userGridConfig);
                this.ObjectContext.UserGridConfigs.DeleteObject(userGridConfig);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'UserInformations' query.

        public IQueryable<UserInformation> GetUserInformations()
        {
            return this.ObjectContext.UserInformations.Where(u=>u.IsDeleted==false);
        }

        [Invoke]
        public bool CheckPassword(string password)
        {
            bool result = false;

            string pass = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
            string username = HttpContext.Current.User.Identity.Name;
            var  user = this.ObjectContext.UserInformations.Where(u => u.UserName == username && u.PasswordHash == pass).FirstOrDefault();
            if (user != null)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public IQueryable<UserInformation> GetUserInformationsByGroupID(int groupID)
        {
            return this.ObjectContext.UserInformations.Where(u => u.UserGroups.FirstOrDefault().Group_ID == groupID && u.IsDeleted == false);
        }

        public IQueryable<UserInformation> GetUserInformationWithMenuStructure()
        {
            return this.ObjectContext.UserInformations.Include("MenuStructure").Where(u => u.IsDeleted == false); 
        }

        public void InsertUserInformation(UserInformation userInformation)
        {
            if ((userInformation.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(userInformation, EntityState.Added);
            }
            else
            {
                this.ObjectContext.UserInformations.AddObject(userInformation);
            }
        }




        public void UpdateUserInformation(UserInformation currentUserInformation)
        {
            this.ObjectContext.UserInformations.AttachAsModified(currentUserInformation, this.ChangeSet.GetOriginal(currentUserInformation));
        }

        public void DeleteUserInformation(UserInformation userInformation)
        {
            if ((userInformation.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(userInformation, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.UserInformations.Attach(userInformation);
                this.ObjectContext.UserInformations.DeleteObject(userInformation);
            }
        }

        public IQueryable<GroupViewFilter> GetGroupViewFilters()
        {
            return this.ObjectContext.GroupViewFilters.Include("Field"); 
        }

        public IQueryable<GroupViewFilter> GetGroupViewFiltersByGroupIDViewID(int groupID, int viewID)
        {
            return this.ObjectContext.GroupViewFilters.Include("Field").Where(gv => gv.GroupView.Group_ID == groupID && gv.GroupView.View_ID == viewID);
        }


        [Query(IsComposable = false)]
        public List<GroupViewFilter> GetGroupViewFiltersByGroupViewID(int groupID, int viewID)
        {
             List<GroupViewFilter> viewFilter = new  List<GroupViewFilter>();
             MiddlewareEntities context = new MiddlewareEntities();
             var query = (from g in context.GroupViewFilters                       
                          join f in context.Fields on g.Field_ID equals f.ID
                          join t in context.Tables on f.Table_ID equals t.ID
                          where g.GroupView_ID == (from gv in context.GroupViews where gv.Group_ID == groupID && gv.View_ID == viewID select gv.ID).FirstOrDefault()
                          select new { id = g.ID, tableID = t.ID, tableName = t.TableName, fieldID = f.ID, fieldName = f.FieldName, operatr = g.Operator, value = g.Value, OrAnd = g.OrAnd, secondOperator = g.SecondOperator, secondvalue = g.SecondValue});

             foreach (var qry in query)
             {
                 GroupViewFilter filter = new GroupViewFilter();
                 filter.ID = qry.id;
                 filter.Field_ID = qry.fieldID;
                 filter.Operator = qry.operatr;
                 filter.Value = qry.value;
                 filter.OrAnd = qry.OrAnd;
                 filter.SecondOperator = qry.secondOperator;
                 filter.SecondValue = qry.secondvalue;

                 string summary = "";

                 summary += qry.tableName.ToString().Replace(" ", "") + ".[" + qry.fieldName + "]" + " " + filter.Operator + " ";
                 if ((filter.Operator == "in") || ((filter.Operator == "not in")))
                 {
                     summary += "(" + filter.Value + ") ";
                 }
                 else
                 {
                     summary += "'" + filter.Value + "' ";
                 }

                 if (filter.OrAnd != null)
                 {
                     summary += " " + filter.OrAnd + " " + qry.tableName.ToString().Replace(" ", "") + ".[" + qry.fieldName + "]" + " " + filter.SecondOperator + " ";

                     if ((filter.SecondOperator == "in") || ((filter.SecondOperator == "not in")))
                         {
                             summary += "(" +  filter.SecondValue + ") ";
                         }
                         else
                         {
                             summary += "'" +  filter.SecondValue + "' ";
                         }
                                 
                 }
                 filter.Summary = summary;

                 viewFilter.Add(filter);

             }  

             return viewFilter;

        }

        public IQueryable<GroupModel> GetGroupModels()
        {
            return this.ObjectContext.GroupModels;
        }

        public void InsertGroupModel(GroupModel groupModel)
        {
            if ((groupModel.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(groupModel, EntityState.Added);
            }
            else
            {
                this.ObjectContext.GroupModels.AddObject(groupModel);
            }
        }

        public void UpdateGroupModel(GroupModel currentGroupModel)
        {
            this.ObjectContext.GroupModels.AttachAsModified(currentGroupModel, this.ChangeSet.GetOriginal(currentGroupModel));
        }

        public void DeleteGroupModel(GroupModel groupModel)
        {
            if ((groupModel.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(groupModel, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.GroupModels.Attach(groupModel);
                this.ObjectContext.GroupModels.DeleteObject(groupModel);
            }
        }

        public void InsertGroupViewFilter(GroupViewFilter groupViewFilter)
        {
            if ((groupViewFilter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(groupViewFilter, EntityState.Added);
            }
            else
            {
                this.ObjectContext.GroupViewFilters.AddObject(groupViewFilter);
            }
        }

        public void UpdateGroupViewFilter(GroupViewFilter currentGroupViewFilter)
        {
            this.ObjectContext.GroupViewFilters.AttachAsModified(currentGroupViewFilter, this.ChangeSet.GetOriginal(currentGroupViewFilter));
        }

        public void DeleteGroupViewFilter(GroupViewFilter groupViewFilter)
        {
            if ((groupViewFilter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(groupViewFilter, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.GroupViewFilters.Attach(groupViewFilter);
                this.ObjectContext.GroupViewFilters.DeleteObject(groupViewFilter);
            }
        }
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Views' query.
        public IQueryable<View> GetViews()
        {
            return this.ObjectContext.Views.Include("GroupViews"); 
        }


        [Query(IsComposable = false)]
        public IQueryable<View> GetViewsByDisplayDefinitionID(int DefinitionID)
        {
            return this.ObjectContext.GridDefinitions.Join(this.ObjectContext.Views, v => (v.View_ID), vt => vt.ID, (v, vt) => new { v = v, vt = vt }).Where(v => v.v.DisplayDefinition_ID == DefinitionID).Select(temp1 => temp1.v.View);
          
        }
        [Query(IsComposable = false)]
        public IQueryable<GridDefinition> GetGridDefinitionsByDisplayDefinitionID(int DefinitionID)
        {
            return this.ObjectContext.GridDefinitions.Include("View").Where(g => g.DisplayDefinition_ID == DefinitionID);

        }


        public void InsertView(View view)
        {
            if ((view.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(view, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Views.AddObject(view);
            }
        }

        public void UpdateView(View currentView)
        {
            this.ObjectContext.Views.AttachAsModified(currentView, this.ChangeSet.GetOriginal(currentView));
        }

        public void DeleteView(View view)
        {
            if ((view.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(view, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Views.Attach(view);
                this.ObjectContext.Views.DeleteObject(view);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ViewTables' query.
        public IQueryable<ViewTable> GetViewTables()
        {
            return this.ObjectContext.ViewTables;
        }


        //public IQueryable<TableSummaryData> GetTableDataSummary()
        //{
        //    var query =
        //}

        public void InsertViewTable(ViewTable viewTable)
        {
            if ((viewTable.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewTable, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ViewTables.AddObject(viewTable);
            }
        }

        public void UpdateViewTable(ViewTable currentViewTable)
        {
            this.ObjectContext.ViewTables.AttachAsModified(currentViewTable, this.ChangeSet.GetOriginal(currentViewTable));
        }

        public void DeleteViewTable(ViewTable viewTable)
        {
            if ((viewTable.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewTable, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ViewTables.Attach(viewTable);
                this.ObjectContext.ViewTables.DeleteObject(viewTable);
            }
        }


        public IQueryable<ViewField> GetViewFields()
        {
            return this.ObjectContext.ViewFields;
        }


       


        [Query(IsComposable = false)]
        public List<ViewFieldSummary> GetViewFieldByViewID(int viewID)
        {
            List<ViewFieldSummary> ViewFieldSummaryList = new List<ViewFieldSummary>();


            try
            {

                MiddlewareEntities context = new MiddlewareEntities();
                var query = (from vr in context.ViewFields
                             join t in context.Tables on vr.Table_ID equals t.ID
                             
                             join f in context.Fields on vr.Feild_ID equals f.ID
                            
                             where vr.View_ID == viewID
                             select new { id = vr.ID, tableID = t.ID, tableName = t.TableName, fieldID = f.ID, fieldName = f.FieldName, f.Type, displayName = vr.DisplayName, vr.UseValueField, vr.ForCreate, vr.ForDelete,vr.ForRead,vr.ForUpdate, vr.Hidden,vr.UseValueStoredProcedure, vr.UsingTable, vr.StoredProcedureNames_ID,vr.ReadOnly, vr.FieldOptionFilter, vr.DefaultValue, vr.ForTSAdjust, vr.AutoPopulate});

                foreach (var qry in query)
                {
                    ViewFieldSummary viewField = new ViewFieldSummary();
                    viewField.ID = qry.id;
                    viewField.DisplayName = qry.displayName;
                    viewField.FieldID = qry.fieldID;
                    viewField.TableID = qry.tableID;
                    viewField.FieldName = qry.fieldName;
                    viewField.UseValueField = qry.UseValueField;
                    viewField.IsCalculatedField = false;
                    viewField.DataType = qry.Type;
                    viewField.ForCreate = qry.ForCreate;
                    viewField.ForDelete = qry.ForDelete;
                    viewField.ForRead = qry.ForRead;
                    viewField.ForUpdate = qry.ForUpdate;
                    viewField.ReadOnly = qry.ReadOnly;
                    viewField.Hidden = qry.Hidden;
                    viewField.StoredProcedureNames_ID = qry.StoredProcedureNames_ID;
                    viewField.UseValueStoredProcedure = qry.UseValueStoredProcedure;
                    viewField.UsingTable = qry.UsingTable;
                    viewField.FieldOptionFilter = qry.FieldOptionFilter;
                    viewField.ForTSAdjust = qry.ForTSAdjust;
                    viewField.AutoPopulate = qry.AutoPopulate;
                        viewField.DefaultValue = qry.DefaultValue;
                    viewField.TableField = "[" + qry.tableName.ToString() + "]" + ".[" + qry.fieldName + "]";

                    ViewFieldSummaryList.Add(viewField);

                }

                var queryCalc = (from vr in context.CalculatedFields
                            
                             where vr.View_ID == viewID
                             select new { id = vr.ID, displayName = vr.DisplayName});

                foreach (var item in queryCalc)
                {
                     ViewFieldSummary viewField = new ViewFieldSummary();
                    viewField.ID = item.id;
                    viewField.DisplayName = item.displayName;
                    viewField.FieldID = item.id;
                    viewField.TableID = 0;
                  
                    viewField.FieldName = item.displayName;
                 //   viewField.UseValueField = false;
                    viewField.IsCalculatedField = true;
                    viewField.TableField = item.displayName;
                  //  viewField.Expression = item.expression;
                    ViewFieldSummaryList.Add(viewField);

                }


                return ViewFieldSummaryList;
            }
            catch
            {
                return ViewFieldSummaryList;
            }

        }


        [Query(IsComposable = false)]

        public List<ViewGridRelationship> GetRelationshipByDefinitionID(int DefinitionID)
        {

            List<ViewGridRelationship> gridRshipList = new List<ViewGridRelationship>();
            var query = (from gr in this.ObjectContext.GridRelationships
                         join g in this.ObjectContext.GridDefinitions on gr.GridID1 equals g.ID
                         join g2 in this.ObjectContext.GridDefinitions on gr.GridID2 equals g2.ID
                         join f in this.ObjectContext.Fields on gr.ViewField1 equals f.ID
                         join f2 in this.ObjectContext.Fields on gr.ViewField2 equals f2.ID
                         where gr.DisplayDefinition_ID == DefinitionID
                         select new { id = gr.ID, grid1ID = g.ID, gridNumber1 = g.GridNumber, grid2ID = g2.ID, gridNumber2 = g2.GridNumber, fieldID = f.ID, fieldName = f.FieldName, fieldID2 = f2.ID, fieldName2 = f2.FieldName });
            foreach (var item in query)
            {
                ViewGridRelationship rship = new ViewGridRelationship();
                rship.ID = item.id;
                rship.Grid_ID = item.grid1ID;
                rship.GridNumber = item.gridNumber1;
                rship.Grid2_ID = item.grid2ID;
                rship.Grid2Number = item.gridNumber2;
                rship.Field1_ID = item.fieldID;
                rship.Field1_Name = item.fieldName;
                rship.Field2_ID = item.fieldID2;
                rship.Field2_Name = item.fieldName2;

                gridRshipList.Add(rship);
            }

            return gridRshipList;
        }


        public void UpdateViewField(ViewFieldSummary currentViewField)
        {
           // this.ObjectContext.ViewFields.AttachAsModified(currentViewField, this.ChangeSet.GetOriginal(currentViewField));
        }



        public void InsertViewField(ViewField viewField)
        {
            if ((viewField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewField, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ViewFields.AddObject(viewField);
            }
        }

        public void UpdateViewField(ViewField currentViewField)
        {
            this.ObjectContext.ViewFields.AttachAsModified(currentViewField, this.ChangeSet.GetOriginal(currentViewField));
        }

        public void DeleteViewTable(ViewField viewTable)
        {
            if ((viewTable.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewTable, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ViewFields.Attach(viewTable);
                this.ObjectContext.ViewFields.DeleteObject(viewTable);
            }
        }
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ViewTableRelationships' query.
        public IQueryable<ViewTableRelationship> GetViewTableRelationships()
        {
            return this.ObjectContext.ViewTableRelationships;
        }

        public void InsertViewTableRelationship(ViewTableRelationship viewTableRelationship)
        {
            if ((viewTableRelationship.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewTableRelationship, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ViewTableRelationships.AddObject(viewTableRelationship);
            }
        }

        public void UpdateViewTableRelationship(ViewTableRelationship currentViewTableRelationship)
        {
            this.ObjectContext.ViewTableRelationships.AttachAsModified(currentViewTableRelationship, this.ChangeSet.GetOriginal(currentViewTableRelationship));
        }

        public void DeleteViewTableRelationship(ViewTableRelationship viewTableRelationship)
        {
            if ((viewTableRelationship.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(viewTableRelationship, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ViewTableRelationships.Attach(viewTableRelationship);
                this.ObjectContext.ViewTableRelationships.DeleteObject(viewTableRelationship);
            }
        }


        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Groups' query.
        public IQueryable<Group> GetGroups()
        {
            return this.ObjectContext.Groups;
        }

        public void InsertGroup(Group group)
        {
            if ((group.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(group, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Groups.AddObject(group);
            }
        }

        public void UpdateGroup(Group currentGroup)
        {
            this.ObjectContext.Groups.AttachAsModified(currentGroup, this.ChangeSet.GetOriginal(currentGroup));
        }

        public void DeleteGroup(Group group)
        {
            if ((group.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(group, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Groups.Attach(group);
                this.ObjectContext.Groups.DeleteObject(group);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'GroupViews' query.
        public IQueryable<GroupView> GetGroupViews()
        {
            return this.ObjectContext.GroupViews;
        }

        public void InsertGroupView(GroupView groupView)
        {
            if ((groupView.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(groupView, EntityState.Added);
            }
            else
            {
                this.ObjectContext.GroupViews.AddObject(groupView);
            }
        }

        public void UpdateGroupView(GroupView currentGroupView)
        {
            this.ObjectContext.GroupViews.AttachAsModified(currentGroupView, this.ChangeSet.GetOriginal(currentGroupView));
        }

        public void DeleteGroupView(GroupView groupView)
        {
            if ((groupView.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(groupView, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.GroupViews.Attach(groupView);
                this.ObjectContext.GroupViews.DeleteObject(groupView);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'UserGroups' query.
        public IQueryable<UserGroup> GetUserGroups()
        {
            return this.ObjectContext.UserGroups;
        }

        public void InsertUserGroup(UserGroup userGroup)
        {
            if ((userGroup.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(userGroup, EntityState.Added);
            }
            else
            {
                this.ObjectContext.UserGroups.AddObject(userGroup);
            }
        }

        public void UpdateUserGroup(UserGroup currentUserGroup)
        {
            this.ObjectContext.UserGroups.AttachAsModified(currentUserGroup, this.ChangeSet.GetOriginal(currentUserGroup));
        }

        public void DeleteUserGroup(UserGroup userGroup)
        {
            if ((userGroup.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(userGroup, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.UserGroups.Attach(userGroup);
                this.ObjectContext.UserGroups.DeleteObject(userGroup);
            }
        }

        [Query(IsComposable = false)]
        public List<ViewRelationship> GetViewRelationshipByViewID(int viewID)
        {
            List<ViewRelationship> relationshipList= new List<ViewRelationship>();
    
                    
            try
            {
                
                MiddlewareEntities context = new MiddlewareEntities();
                var query = (from vr in context.ViewTableRelationships
                             join t in context.Tables on vr.Table_ID_A equals t.ID
                             join tb in context.Tables on vr.Table_ID_B equals tb.ID
                             join f in context.Fields on vr.Field_ID_A equals f.ID
                             join fb in context.Fields on vr.Field_ID_B equals fb.ID
                             where vr.View_ID == viewID
                             select new { id = vr.ID, tableA_ID = t.ID, tableA_Name = t.TableName, tableB_ID = tb.ID, tableB_Name = tb.TableName, fieldA_ID = f.ID, fieldA_Name = f.FieldName, fieldB_ID = fb.ID, fieldB_Name = fb.FieldName });

                             foreach(var qry in query)
                             {
                                 ViewRelationship relationship = new ViewRelationship();
                                 relationship.ID = qry.id;
                                 relationship.TableA_ID = qry.tableA_ID;
                                 relationship.FieldA_ID = qry.fieldA_ID;
                                 relationship.TableB_ID = qry.tableB_ID;
                                 relationship.FieldB_ID = qry.fieldB_ID;
                                 relationship.Summary = "["+qry.tableA_Name.ToString()+"]" + ".[" + qry.fieldA_Name + "] = [" + qry.tableB_Name.ToString() + "].[" + qry.fieldB_Name+"]";

                                 relationshipList.Add(relationship);

                             }
                             return relationshipList;
            }
            catch
            {
                return relationshipList;
            }

        }

        public IQueryable<ObjectDefintion> GetObjectDefintions()
        {
           
            return this.ObjectContext.ObjectDefintions.Include("ObjectProperties");

        }

        [Query]
        public IQueryable<ObjectDefintion> GetObjectDefintionsByFormID(int formID)
        {

            var objects = this.ObjectContext.ObjectDefintions.Include("ObjectProperties").Where(o => o.ObjectForm_ID == formID && o.IsDeleted==false);
           

            return objects.AsQueryable();

        }

        public void InsertObjectDefintion(ObjectDefintion objectDefintion)
        {
            if ((objectDefintion.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectDefintion, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ObjectDefintions.AddObject(objectDefintion);
            }
        }

        public void UpdateObjectDefintion(ObjectDefintion currentObjectDefintion)
        {
            this.ObjectContext.ObjectDefintions.AttachAsModified(currentObjectDefintion, this.ChangeSet.GetOriginal(currentObjectDefintion));
        }

        public void DeleteObjectDefintion(ObjectDefintion objectDefintion)
        {
            if ((objectDefintion.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectDefintion, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ObjectDefintions.Attach(objectDefintion);
                this.ObjectContext.ObjectDefintions.DeleteObject(objectDefintion);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ObjectForms' query.
        public IQueryable<ObjectForm> GetObjectForms()
        {
            return this.ObjectContext.ObjectForms;
        }

        public void InsertObjectForm(ObjectForm objectForm)
        {
            if ((objectForm.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectForm, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ObjectForms.AddObject(objectForm);
            }
        }

        public void UpdateObjectForm(ObjectForm currentObjectForm)
        {
            this.ObjectContext.ObjectForms.AttachAsModified(currentObjectForm, this.ChangeSet.GetOriginal(currentObjectForm));
        }

        public void DeleteObjectForm(ObjectForm objectForm)
        {
            if ((objectForm.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectForm, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ObjectForms.Attach(objectForm);
                this.ObjectContext.ObjectForms.DeleteObject(objectForm);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ObjectFunctions' query.
        public IQueryable<ObjectFunction> GetObjectFunctions()
        {
            return this.ObjectContext.ObjectFunctions.Include("Server");
        }

        public void InsertObjectFunction(ObjectFunction objectFunction)
        {
            if ((objectFunction.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectFunction, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ObjectFunctions.AddObject(objectFunction);
            }
        }

        public void UpdateObjectFunction(ObjectFunction currentObjectFunction)
        {
            this.ObjectContext.ObjectFunctions.AttachAsModified(currentObjectFunction, this.ChangeSet.GetOriginal(currentObjectFunction));
        }

        public void DeleteObjectFunction(ObjectFunction objectFunction)
        {
            if ((objectFunction.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectFunction, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ObjectFunctions.Attach(objectFunction);
                this.ObjectContext.ObjectFunctions.DeleteObject(objectFunction);
            }
        }


        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ObjectFunctionParameters' query.
        public IQueryable<ObjectFunctionParameter> GetObjectFunctionParameters()
        {
            return this.ObjectContext.ObjectFunctionParameters;
        }

        public void InsertObjectFunctionParameter(ObjectFunctionParameter objectFunctionParameter)
        {
            if ((objectFunctionParameter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectFunctionParameter, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ObjectFunctionParameters.AddObject(objectFunctionParameter);
            }
        }

        public void UpdateObjectFunctionParameter(ObjectFunctionParameter currentObjectFunctionParameter)
        {
            this.ObjectContext.ObjectFunctionParameters.AttachAsModified(currentObjectFunctionParameter, this.ChangeSet.GetOriginal(currentObjectFunctionParameter));
        }

        public void DeleteObjectFunctionParameter(ObjectFunctionParameter objectFunctionParameter)
        {
            if ((objectFunctionParameter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectFunctionParameter, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ObjectFunctionParameters.Attach(objectFunctionParameter);
                this.ObjectContext.ObjectFunctionParameters.DeleteObject(objectFunctionParameter);
            }
        }
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ObjectProperties' query.
        public IQueryable<ObjectProperty> GetObjectProperties()
        {
            return this.ObjectContext.ObjectProperties;
        }

        public void InsertObjectProperty(ObjectProperty objectProperty)
        {
            if ((objectProperty.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectProperty, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ObjectProperties.AddObject(objectProperty);
            }
        }

        public void UpdateObjectProperty(ObjectProperty currentObjectProperty)
        {
            this.ObjectContext.ObjectProperties.AttachAsModified(currentObjectProperty, this.ChangeSet.GetOriginal(currentObjectProperty));
        }

        public void DeleteObjectProperty(ObjectProperty objectProperty)
        {
            if ((objectProperty.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(objectProperty, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ObjectProperties.Attach(objectProperty);
                this.ObjectContext.ObjectProperties.DeleteObject(objectProperty);
            }
        }


     

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'PropertyDefinitions' query.
        public IQueryable<PropertyDefinition> GetPropertyDefinitions()
        {
            return this.ObjectContext.PropertyDefinitions;
        }

        public void InsertPropertyDefinition(PropertyDefinition propertyDefinition)
        {
            if ((propertyDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(propertyDefinition, EntityState.Added);
            }
            else
            {
                this.ObjectContext.PropertyDefinitions.AddObject(propertyDefinition);
            }
        }

        public void UpdatePropertyDefinition(PropertyDefinition currentPropertyDefinition)
        {
            this.ObjectContext.PropertyDefinitions.AttachAsModified(currentPropertyDefinition, this.ChangeSet.GetOriginal(currentPropertyDefinition));
        }

        public void DeletePropertyDefinition(PropertyDefinition propertyDefinition)
        {
            if ((propertyDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(propertyDefinition, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.PropertyDefinitions.Attach(propertyDefinition);
                this.ObjectContext.PropertyDefinitions.DeleteObject(propertyDefinition);
            }
        }

        [Invoke]
        public void DeleteServerAndDepends(int serverID)
        {
            this.ObjectContext.DeleteServerTableField(serverID);
        }


        public IQueryable<CalculatedField> GetCalculatedFields()
        {
            return this.ObjectContext.CalculatedFields;
        }

        public void InsertCalculatedField(CalculatedField calculatedField)
        {
            if ((calculatedField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calculatedField, EntityState.Added);
            }
            else
            {
                this.ObjectContext.CalculatedFields.AddObject(calculatedField);
            }
        }

        public void UpdateCalculatedField(CalculatedField currentCalculatedField)
        {
            this.ObjectContext.CalculatedFields.AttachAsModified(currentCalculatedField, this.ChangeSet.GetOriginal(currentCalculatedField));
        }

        public void DeleteCalculatedField(CalculatedField calculatedField)
        {
            if ((calculatedField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calculatedField, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.CalculatedFields.Attach(calculatedField);
                this.ObjectContext.CalculatedFields.DeleteObject(calculatedField);
            }
        }


        public IQueryable<VauleFiedOption> GetVauleFiedOptions()
        {
            return this.ObjectContext.VauleFiedOptions;
        }

        public void InsertVauleFiedOption(VauleFiedOption vauleFiedOption)
        {
            if ((vauleFiedOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(vauleFiedOption, EntityState.Added);
            }
            else
            {
                this.ObjectContext.VauleFiedOptions.AddObject(vauleFiedOption);
            }
        }

        public void UpdateVauleFiedOption(VauleFiedOption currentVauleFiedOption)
        {
            this.ObjectContext.VauleFiedOptions.AttachAsModified(currentVauleFiedOption, this.ChangeSet.GetOriginal(currentVauleFiedOption));
        }

        public void DeleteVauleFiedOption(VauleFiedOption vauleFiedOption)
        {
            if ((vauleFiedOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(vauleFiedOption, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.VauleFiedOptions.Attach(vauleFiedOption);
                this.ObjectContext.VauleFiedOptions.DeleteObject(vauleFiedOption);
            }
        }

        public IQueryable<DefinitionOption> GetDefinitionOptions()
        {
            return this.ObjectContext.DefinitionOptions;
        }

        public void InsertDefinitionOption(DefinitionOption definitionOption)
        {
            if ((definitionOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(definitionOption, EntityState.Added);
            }
            else
            {
                this.ObjectContext.DefinitionOptions.AddObject(definitionOption);
            }
        }

        public void UpdateDefinitionOption(DefinitionOption currentDefinitionOption)
        {
            this.ObjectContext.DefinitionOptions.AttachAsModified(currentDefinitionOption, this.ChangeSet.GetOriginal(currentDefinitionOption));
        }

        public void DeleteDefinitionOption(DefinitionOption definitionOption)
        {
            if ((definitionOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(definitionOption, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.DefinitionOptions.Attach(definitionOption);
                this.ObjectContext.DefinitionOptions.DeleteObject(definitionOption);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'DisplayDefinitionOptions' query.
        public IQueryable<DisplayDefinitionOption> GetDisplayDefinitionOptions()
        {
            return this.ObjectContext.DisplayDefinitionOptions.Include("FormType");
        }

        [Query]
        public IQueryable<DisplayDefinitionOption> GetDisplayDefinitionOptionsByDisplayDefID(int displayDefID)
        {
            return this.ObjectContext.DisplayDefinitionOptions.Include("Server").Include("DefinitionOption").Where(o => o.DisplayDefinition_ID == displayDefID);
        }
        public void InsertDisplayDefinitionOption(DisplayDefinitionOption displayDefinitionOption)
        {
            if ((displayDefinitionOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(displayDefinitionOption, EntityState.Added);
            }
            else
            {
                this.ObjectContext.DisplayDefinitionOptions.AddObject(displayDefinitionOption);
            }
        }

        public void UpdateDisplayDefinitionOption(DisplayDefinitionOption currentDisplayDefinitionOption)
        {
            this.ObjectContext.DisplayDefinitionOptions.AttachAsModified(currentDisplayDefinitionOption, this.ChangeSet.GetOriginal(currentDisplayDefinitionOption));
        }

        public void DeleteDisplayDefinitionOption(DisplayDefinitionOption displayDefinitionOption)
        {
            if ((displayDefinitionOption.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(displayDefinitionOption, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.DisplayDefinitionOptions.Attach(displayDefinitionOption);
                this.ObjectContext.DisplayDefinitionOptions.DeleteObject(displayDefinitionOption);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'GridStoreProcedures' query.
        public IQueryable<GridStoreProcedure> GetGridStoreProcedures()
        {
            return this.ObjectContext.GridStoreProcedures;
        }

        public void InsertGridStoreProcedure(GridStoreProcedure gridStoreProcedure)
        {
            if ((gridStoreProcedure.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(gridStoreProcedure, EntityState.Added);
            }
            else
            {
                this.ObjectContext.GridStoreProcedures.AddObject(gridStoreProcedure);
            }
        }

        public void UpdateGridStoreProcedure(GridStoreProcedure currentGridStoreProcedure)
        {
            this.ObjectContext.GridStoreProcedures.AttachAsModified(currentGridStoreProcedure, this.ChangeSet.GetOriginal(currentGridStoreProcedure));
        }

        public void DeleteGridStoreProcedure(GridStoreProcedure gridStoreProcedure)
        {
            if ((gridStoreProcedure.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(gridStoreProcedure, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.GridStoreProcedures.Attach(gridStoreProcedure);
                this.ObjectContext.GridStoreProcedures.DeleteObject(gridStoreProcedure);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'StoredProcedureNames' query.
        public IQueryable<StoredProcedureName> GetStoredProcedureNames()
        {
            return this.ObjectContext.StoredProcedureNames.Include("Server").Include("StoredProcedureParameters");
        }

        public void InsertStoredProcedureName(StoredProcedureName storedProcedureName)
        {
            if ((storedProcedureName.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureName, EntityState.Added);
            }
            else
            {
                this.ObjectContext.StoredProcedureNames.AddObject(storedProcedureName);
            }
        }

        public void UpdateStoredProcedureName(StoredProcedureName currentStoredProcedureName)
        {
            this.ObjectContext.StoredProcedureNames.AttachAsModified(currentStoredProcedureName, this.ChangeSet.GetOriginal(currentStoredProcedureName));
        }

        public void DeleteStoredProcedureName(StoredProcedureName storedProcedureName)
        {
            if ((storedProcedureName.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureName, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.StoredProcedureNames.Attach(storedProcedureName);
                this.ObjectContext.StoredProcedureNames.DeleteObject(storedProcedureName);
            }
        }


       

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'StoredProcedureParameters' query.
        public IQueryable<StoredProcedureParameter> GetStoredProcedureParameters()
        {
            return this.ObjectContext.StoredProcedureParameters;
        }

        public void InsertStoredProcedureParameter(StoredProcedureParameter storedProcedureParameter)
        {
            if ((storedProcedureParameter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureParameter, EntityState.Added);
            }
            else
            {
                this.ObjectContext.StoredProcedureParameters.AddObject(storedProcedureParameter);
            }
        }

        public void UpdateStoredProcedureParameter(StoredProcedureParameter currentStoredProcedureParameter)
        {
            this.ObjectContext.StoredProcedureParameters.AttachAsModified(currentStoredProcedureParameter, this.ChangeSet.GetOriginal(currentStoredProcedureParameter));
        }

        public void DeleteStoredProcedureParameter(StoredProcedureParameter storedProcedureParameter)
        {
            if ((storedProcedureParameter.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(storedProcedureParameter, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.StoredProcedureParameters.Attach(storedProcedureParameter);
                this.ObjectContext.StoredProcedureParameters.DeleteObject(storedProcedureParameter);
            }
        }


        public IQueryable<ReadField> GetReadFields()
        {
            return this.ObjectContext.ReadFields;
        }

        public void InsertReadField(ReadField readField)
        {
            if ((readField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(readField, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ReadFields.AddObject(readField);
            }
        }

        public void UpdateReadField(ReadField currentReadField)
        {
           // this.ObjectContext.ReadFields.AttachAsModified(currentReadField, this.ChangeSet.GetOriginal(currentReadField));
        }

        public void DeleteReadField(ReadField readField)
        {
            if ((readField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(readField, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ReadFields.Attach(readField);
                this.ObjectContext.ReadFields.DeleteObject(readField);
            }
        }

        [Invoke]
        public void IssueAuthenticationToken()
        {
            
            FormsAuthentication.SetAuthCookie(HttpContext.Current.User.Identity.Name, true);
        }

    }



    [EnableClientAccess()]
    public class TableSummaryData
    {
        [Key]
        public int TableID { get; set; }
        public string TableName { get; set; }
        public string GridNumber { get; set; }
        public string ServerName { get; set; }
        public string ServerUserName { get; set; }
        public string ServerPassword { get; set; }


    }

    [EnableClientAccess()]
    public class ViewRelationship
    {
        [Key]
        public int ID { get; set; }
        public int TableA_ID { get; set; }

        public int FieldA_ID { get; set; }
        
        public int TableB_ID { get; set; }
        public int FieldB_ID { get; set; }
        public string Summary { get; set; }
    }

    [EnableClientAccess()]
    
    public class  ViewFieldSummary
    {
        [Key]
        public int ID { get; set; }
        public int FieldID { get; set; }
        public string FieldName { get; set; }
        public string DataType { get; set; }
        public int TableID { get; set; }
        public string TableField { get; set; }
        public string Expression { get; set; }
        public string DisplayName { get; set; }
        public bool? UseValueField { get; set; }
        public bool? IsCalculatedField { get; set; }
        public bool? Hidden { get; set; }
        public bool? ReadOnly { get; set; }
       public bool? UsingTable { get; set; }
       public bool? ForCreate { get; set; }
       public bool? ForRead { get; set; }
       public bool? ForUpdate { get; set; }
       public bool? ForDelete { get; set; }
      public bool? UseValueStoredProcedure { get; set; }
       public int? StoredProcedureNames_ID { get; set; }
       public bool? ForTSAdjust { get; set; }
       public bool? AutoPopulate { get; set; }
       public string DefaultValue { get; set; }
       public string FieldOptionFilter { get; set; }
    }

     [EnableClientAccess()]
    public class ViewGridRelationship
    {

          [Key]
         public int ID { get; set; }
        public int DisplayDefinition_ID { get; set; }
        public int Grid_ID { get; set; }
        public int GridNumber { get; set; }
        public int Field1_ID { get; set; }
        public string Field1_Name { get; set; }

        public int Grid2_ID { get; set; }
        public int Grid2Number { get; set; }
        public int Field2_ID { get; set; }
        public string Field2_Name { get; set; }
    }

}


