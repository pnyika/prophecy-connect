﻿
namespace BMA.MiddlewareApp.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies CalculatedFieldMetadata as the class
    // that carries additional metadata for the CalculatedField class.
    [MetadataTypeAttribute(typeof(CalculatedField.CalculatedFieldMetadata))]
    public partial class CalculatedField
    {

        // This class allows you to attach custom attributes to properties
        // of the CalculatedField class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class CalculatedFieldMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private CalculatedFieldMetadata()
            {
            }

            public string DisplayName { get; set; }

          //  public string Expression { get; set; }

            public int ID { get; set; }

            public int View_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies CalculatedFieldFunctionMetadata as the class
    // that carries additional metadata for the CalculatedFieldFunction class.
    [MetadataTypeAttribute(typeof(CalculatedFieldFunction.CalculatedFieldFunctionMetadata))]
    public partial class CalculatedFieldFunction
    {

        // This class allows you to attach custom attributes to properties
        // of the CalculatedFieldFunction class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class CalculatedFieldFunctionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private CalculatedFieldFunctionMetadata()
            {
            }

            public int CalculatedField_ID { get; set; }

            public int Field_ID_A { get; set; }

            public int Field_ID_B { get; set; }

            public int ID { get; set; }

            public string Operator { get; set; }

            public int Table_ID_A { get; set; }

            public int Table_ID_B { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies DisplayDefinitionMetadata as the class
    // that carries additional metadata for the DisplayDefinition class.
    [MetadataTypeAttribute(typeof(DisplayDefinition.DisplayDefinitionMetadata))]
    public partial class DisplayDefinition
    {

        // This class allows you to attach custom attributes to properties
        // of the DisplayDefinition class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class DisplayDefinitionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private DisplayDefinitionMetadata()
            {
            }

            public Nullable<int> CreatedUser_ID { get; set; }

            public string Description { get; set; }

            [Include]
            public FormType FormType { get; set; }

            public Nullable<int> FormType_ID { get; set; }

         ///   public EntityCollection<GridDefinition> GridDefinitions { get; set; }

           public EntityCollection<GridRelationship> GridRelationships { get; set; }

            public int ID { get; set; }

            public EntityCollection<MenuItem> MenuItems { get; set; }

            public EntityCollection<ObjectForm> ObjectForms { get; set; }

            public Nullable<int> PageSize { get; set; }

            public EntityCollection<UserGridConfig> UserGridConfigs { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies FieldMetadata as the class
    // that carries additional metadata for the Field class.
    [MetadataTypeAttribute(typeof(Field.FieldMetadata))]
    public partial class Field
    {

        // This class allows you to attach custom attributes to properties
        // of the Field class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class FieldMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private FieldMetadata()
            {
            }

            public Nullable<int> DisplayField_ID { get; set; }

            public string DisplayFieldSuffix { get; set; }

            public string FieldName { get; set; }

            public string Formart { get; set; }

            public string FriendlyName { get; set; }

            public EntityCollection<GroupViewFilter> GroupViewFilters { get; set; }

            public int ID { get; set; }

            public Nullable<byte> isDisplayField { get; set; }

            public byte isKey { get; set; }

            public Nullable<byte> isPrimaryKey { get; set; }

            public Nullable<byte> isValueField { get; set; }

            public string LinkCode { get; set; }

            public Table Table { get; set; }

            public int Table_ID { get; set; }

            public string Type { get; set; }

            public Nullable<int> ValueField_ID { get; set; }

            public EntityCollection<ViewField> ViewFields { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies FormTypeMetadata as the class
    // that carries additional metadata for the FormType class.
    [MetadataTypeAttribute(typeof(FormType.FormTypeMetadata))]
    public partial class FormType
    {

        // This class allows you to attach custom attributes to properties
        // of the FormType class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class FormTypeMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private FormTypeMetadata()
            {
            }

            public string Description { get; set; }

            public EntityCollection<DisplayDefinition> DisplayDefinitions { get; set; }

            public byte[] DisplayImage { get; set; }

            public int ID { get; set; }

            public Nullable<int> NumberOfGrids { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies GridDefinitionMetadata as the class
    // that carries additional metadata for the GridDefinition class.
    [MetadataTypeAttribute(typeof(GridDefinition.GridDefinitionMetadata))]
    public partial class GridDefinition
    {

        // This class allows you to attach custom attributes to properties
        // of the GridDefinition class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class GridDefinitionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private GridDefinitionMetadata()
            {
            }

          //  public DisplayDefinition DisplayDefinition { get; set; }

            public Nullable<int> DisplayDefinition_ID { get; set; }

            public string GridNumber { get; set; }

            public int ID { get; set; }

            public Nullable<int> View_ID { get; set; }

            [Include]
            public   View View { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies GridRelationshipMetadata as the class
    // that carries additional metadata for the GridRelationship class.
    [MetadataTypeAttribute(typeof(GridRelationship.GridRelationshipMetadata))]
    public partial class GridRelationship
    {

        // This class allows you to attach custom attributes to properties
        // of the GridRelationship class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class GridRelationshipMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private GridRelationshipMetadata()
            {
            }

            public DisplayDefinition DisplayDefinition { get; set; }

            public int DisplayDefinition_ID { get; set; }

            public int GridID1 { get; set; }

            public int GridID2 { get; set; }

            public int ID { get; set; }

            public int ViewField1 { get; set; }

            public int ViewField2 { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies GroupMetadata as the class
    // that carries additional metadata for the Group class.
    [MetadataTypeAttribute(typeof(Group.GroupMetadata))]
    public partial class Group
    {

        // This class allows you to attach custom attributes to properties
        // of the Group class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class GroupMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private GroupMetadata()
            {
            }

            public string Description { get; set; }

            public string GroupName { get; set; }

            public EntityCollection<GroupView> GroupViews { get; set; }

            public int ID { get; set; }

            public EntityCollection<UserGroup> UserGroups { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies GroupViewMetadata as the class
    // that carries additional metadata for the GroupView class.
    [MetadataTypeAttribute(typeof(GroupView.GroupViewMetadata))]
    public partial class GroupView
    {

        // This class allows you to attach custom attributes to properties
        // of the GroupView class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class GroupViewMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private GroupViewMetadata()
            {
            }

            public Group Group { get; set; }

            public int Group_ID { get; set; }

            public EntityCollection<GroupViewFilter> GroupViewFilters { get; set; }

            public int ID { get; set; }

            public View View { get; set; }

            public int View_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies GroupViewFilterMetadata as the class
    // that carries additional metadata for the GroupViewFilter class.
    [MetadataTypeAttribute(typeof(GroupViewFilter.GroupViewFilterMetadata))]
    public partial class GroupViewFilter
    {

        // This class allows you to attach custom attributes to properties
        // of the GroupViewFilter class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class GroupViewFilterMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private GroupViewFilterMetadata()
            {
            }
            [Include]
            public Field Field { get; set; }

            public int Field_ID { get; set; }

            public GroupView GroupView { get; set; }

            public int GroupView_ID { get; set; }

            public int ID { get; set; }

            public string Operator { get; set; }

            public string OrAnd { get; set; }

            public string SecondOperator { get; set; }

            public string SecondValue { get; set; }

            public string Summary { get; set; }

            public string Value { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies MenuItemMetadata as the class
    // that carries additional metadata for the MenuItem class.
    [MetadataTypeAttribute(typeof(MenuItem.MenuItemMetadata))]
    public partial class MenuItem
    {

        // This class allows you to attach custom attributes to properties
        // of the MenuItem class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class MenuItemMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private MenuItemMetadata()
            {
            }

            public DisplayDefinition DisplayDefinition { get; set; }

            public Nullable<int> DisplayDefinition_ID { get; set; }

            public string DisplayName { get; set; }

            public int ID { get; set; }

            public bool IsParent { get; set; }

            public MenuStructure MenuStructure { get; set; }

            public Nullable<int> MenuStructure_ID { get; set; }

            public int ParentMenu_ID { get; set; }

            public int SequenceNumber { get; set; }

            public Nullable<int> User_ID { get; set; }

            public string ImageUrl { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies MenuStructureMetadata as the class
    // that carries additional metadata for the MenuStructure class.
    [MetadataTypeAttribute(typeof(MenuStructure.MenuStructureMetadata))]
    public partial class MenuStructure
    {

        // This class allows you to attach custom attributes to properties
        // of the MenuStructure class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class MenuStructureMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private MenuStructureMetadata()
            {
            }

            public string Description { get; set; }

            public int ID { get; set; }

            public EntityCollection<MenuItem> MenuItems { get; set; }

            public string MenuName { get; set; }

            public EntityCollection<UserInformation> UserInformations { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ObjectDefintionMetadata as the class
    // that carries additional metadata for the ObjectDefintion class.
    [MetadataTypeAttribute(typeof(ObjectDefintion.ObjectDefintionMetadata))]
    public partial class ObjectDefintion
    {

        // This class allows you to attach custom attributes to properties
        // of the ObjectDefintion class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ObjectDefintionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ObjectDefintionMetadata()
            {
            }

            public int ID { get; set; }

            public Nullable<bool> IsDeleted { get; set; }

            public ObjectForm ObjectForm { get; set; }

            public int ObjectForm_ID { get; set; }

            public EntityCollection<ObjectFunctionParameter> ObjectFunctionParameters { get; set; }

            public EntityCollection<ObjectFunction> ObjectFunctions { get; set; }

            public string ObjectName { get; set; }
            [Include]
            [Association("FK_ObjectProperties_ObjectDefintion", "ID", "ObjectDefinition_ID")]
            public EntityCollection<ObjectProperty> ObjectProperties { get; set; }

            public string ObjectType { get; set; }

            public double PositionX { get; set; }

            public double PositionY { get; set; }

            public Nullable<int> SizeHeight { get; set; }

            public Nullable<int> SizeWidth { get; set; }

            public string Text { get; set; }

            public short UseTimer { get; set; }

            public int Interval { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ObjectFormMetadata as the class
    // that carries additional metadata for the ObjectForm class.
    [MetadataTypeAttribute(typeof(ObjectForm.ObjectFormMetadata))]
    public partial class ObjectForm
    {

        // This class allows you to attach custom attributes to properties
        // of the ObjectForm class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ObjectFormMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ObjectFormMetadata()
            {
            }

            public DisplayDefinition DisplayDefinition { get; set; }

            public Nullable<int> DisplayDefinition_ID { get; set; }

            public string FormName { get; set; }

            public int ID { get; set; }

            public EntityCollection<ObjectDefintion> ObjectDefintions { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ObjectFunctionMetadata as the class
    // that carries additional metadata for the ObjectFunction class.
    [MetadataTypeAttribute(typeof(ObjectFunction.ObjectFunctionMetadata))]
    public partial class ObjectFunction
    {

        // This class allows you to attach custom attributes to properties
        // of the ObjectFunction class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ObjectFunctionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ObjectFunctionMetadata()
            {
            }

           
            public string FunctionType { get; set; }

            public int ID { get; set; }

            public int ObjectDefinition_ID { get; set; }

            public ObjectDefintion ObjectDefintion { get; set; }

             [Include]
            public Server Server { get; set; }
            public EntityCollection<ObjectFunctionParameter> ObjectFunctionParameters { get; set; }

            
            public string Query { get; set; }

            public int ServerID { get; set; }

            public short UseTimer { get; set; }

            public int Interval { get; set; }

            public string DisplayField { get; set; }

            public string ValueField { get; set; }

           public bool IsDropdown { get; set; }


            public Nullable<int> TargetDisplayObject_ID { get; set; }

        
        }
    }

    // The MetadataTypeAttribute identifies ObjectFunctionParameterMetadata as the class
    // that carries additional metadata for the ObjectFunctionParameter class.
    [MetadataTypeAttribute(typeof(ObjectFunctionParameter.ObjectFunctionParameterMetadata))]
    public partial class ObjectFunctionParameter
    {

        // This class allows you to attach custom attributes to properties
        // of the ObjectFunctionParameter class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ObjectFunctionParameterMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ObjectFunctionParameterMetadata()
            {
            }

            public string DataType { get; set; }

            public int ID { get; set; }

            public bool isOutput { get; set; }

            public Nullable<int> MaxSize { get; set; }

            public int ObjectDefinition_ID { get; set; }

            public ObjectDefintion ObjectDefintion { get; set; }

            public ObjectFunction ObjectFunction { get; set; }

            public int ObjectFunction_ID { get; set; }

            public string ParameterName { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ObjectPropertyMetadata as the class
    // that carries additional metadata for the ObjectProperty class.
    [MetadataTypeAttribute(typeof(ObjectProperty.ObjectPropertyMetadata))]
    public partial class ObjectProperty
    {

        // This class allows you to attach custom attributes to properties
        // of the ObjectProperty class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ObjectPropertyMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ObjectPropertyMetadata()
            {
            }

            public int ID { get; set; }

            public Nullable<int> ObjectDefinition_ID { get; set; }

            public ObjectDefintion ObjectDefintion { get; set; }

            public Nullable<int> Parent { get; set; }

            public string XmlString { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies PropertyDefinitionMetadata as the class
    // that carries additional metadata for the PropertyDefinition class.
    [MetadataTypeAttribute(typeof(PropertyDefinition.PropertyDefinitionMetadata))]
    public partial class PropertyDefinition
    {

        // This class allows you to attach custom attributes to properties
        // of the PropertyDefinition class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class PropertyDefinitionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private PropertyDefinitionMetadata()
            {
            }

            public int ID { get; set; }

            public string ObjectType { get; set; }

            public string PropertyName { get; set; }

            public PropertyValueOption PropertyValueOption { get; set; }

            public Nullable<int> PropertyValueOption_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies PropertyValueOptionMetadata as the class
    // that carries additional metadata for the PropertyValueOption class.
    [MetadataTypeAttribute(typeof(PropertyValueOption.PropertyValueOptionMetadata))]
    public partial class PropertyValueOption
    {

        // This class allows you to attach custom attributes to properties
        // of the PropertyValueOption class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class PropertyValueOptionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private PropertyValueOptionMetadata()
            {
            }

            public int ID { get; set; }

            public string OptionValue { get; set; }

            public EntityCollection<PropertyDefinition> PropertyDefinitions { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ServerMetadata as the class
    // that carries additional metadata for the Server class.
    [MetadataTypeAttribute(typeof(Server.ServerMetadata))]
    public partial class Server
    {

        // This class allows you to attach custom attributes to properties
        // of the Server class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ServerMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ServerMetadata()
            {
            }

            public string ConnectionString { get; set; }

            public string DatabaseName { get; set; }

            public int ID { get; set; }

            public Nullable<bool> IsDeleted { get; set; }

            public string Password { get; set; }

            public string ServerName { get; set; }

            public string ServerType { get; set; }

            public EntityCollection<Table> Tables { get; set; }

            public string Usename { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies SettingMetadata as the class
    // that carries additional metadata for the Setting class.
    [MetadataTypeAttribute(typeof(Setting.SettingMetadata))]
    public partial class Setting
    {

        // This class allows you to attach custom attributes to properties
        // of the Setting class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class SettingMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private SettingMetadata()
            {
            }

            public int ID { get; set; }

            public string Settings { get; set; }

            public EntityCollection<UserGridConfig> UserGridConfigs { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies TableMetadata as the class
    // that carries additional metadata for the Table class.
    [MetadataTypeAttribute(typeof(Table.TableMetadata))]
    public partial class Table
    {

        // This class allows you to attach custom attributes to properties
        // of the Table class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TableMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private TableMetadata()
            {
            }

            public string DBName { get; set; }

            public EntityCollection<Field> Fields { get; set; }

            public int ID { get; set; }

            public Nullable<bool> IsTable { get; set; }

            [Include]
            public Server Server { get; set; }

            public Nullable<int> Server_ID { get; set; }

            public string TableName { get; set; }

            public EntityCollection<ViewField> ViewFields { get; set; }

            public EntityCollection<ViewTable> ViewTables { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies UserGridConfigMetadata as the class
    // that carries additional metadata for the UserGridConfig class.
    [MetadataTypeAttribute(typeof(UserGridConfig.UserGridConfigMetadata))]
    public partial class UserGridConfig
    {

        // This class allows you to attach custom attributes to properties
        // of the UserGridConfig class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class UserGridConfigMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private UserGridConfigMetadata()
            {
            }

            public string ConfigurationName { get; set; }

            public Nullable<int> DefaultConfiguration { get; set; }

            public DisplayDefinition DisplayDefinition { get; set; }

            public int DisplayDefinition_ID { get; set; }

            public Nullable<int> GridNumber { get; set; }

            public int ID { get; set; }

            [Include]
            public Setting Setting { get; set; }

            public int Settings_ID { get; set; }

            public UserInformation UserInformation { get; set; }

            public Nullable<int> UserInformation_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies UserGroupMetadata as the class
    // that carries additional metadata for the UserGroup class.
    [MetadataTypeAttribute(typeof(UserGroup.UserGroupMetadata))]
    public partial class UserGroup
    {

        // This class allows you to attach custom attributes to properties
        // of the UserGroup class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class UserGroupMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private UserGroupMetadata()
            {
            }

            public Group Group { get; set; }

            public int Group_ID { get; set; }

            public int ID { get; set; }

            public int User_ID { get; set; }

            public UserInformation UserInformation { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies UserInformationMetadata as the class
    // that carries additional metadata for the UserInformation class.
    [MetadataTypeAttribute(typeof(UserInformation.UserInformationMetadata))]
    public partial class UserInformation
    {

        // This class allows you to attach custom attributes to properties
        // of the UserInformation class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class UserInformationMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private UserInformationMetadata()
            {
            }

            public string EmailAddress { get; set; }

            public string FirstName { get; set; }

            public Nullable<int> Group_ID { get; set; }

            public int ID { get; set; }

            public string LastName { get; set; }

            public MenuStructure MenuStructure { get; set; }

            public Nullable<int> MenuStructure_ID { get; set; }

            public string MiddleName { get; set; }

            public DateTime ModifiedDate { get; set; }

            public string PasswordHash { get; set; }

            public string PasswordSalt { get; set; }

            public string Phone { get; set; }

            public string Title { get; set; }

            public EntityCollection<UserGridConfig> UserGridConfigs { get; set; }

            public EntityCollection<UserGroup> UserGroups { get; set; }

            public string UserName { get; set; }

            public bool IsDeleted { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies VauleFiedOptionMetadata as the class
    // that carries additional metadata for the VauleFiedOption class.
    [MetadataTypeAttribute(typeof(VauleFiedOption.VauleFiedOptionMetadata))]
    public partial class VauleFiedOption
    {

        // This class allows you to attach custom attributes to properties
        // of the VauleFiedOption class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class VauleFiedOptionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private VauleFiedOptionMetadata()
            {
            }

            public int FieldA { get; set; }

            public int FieldB { get; set; }

            public int ID { get; set; }

            public string LinkCode { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ViewMetadata as the class
    // that carries additional metadata for the View class.
    [MetadataTypeAttribute(typeof(View.ViewMetadata))]
    public partial class View
    {

        // This class allows you to attach custom attributes to properties
        // of the View class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ViewMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ViewMetadata()
            {
            }


            [Include]
            [Association("FK_GroupViews_View", "ID", "View_ID")]
            public EntityCollection<GroupView> GroupViews { get; set; }

            public int ID { get; set; }

            public bool ReadOnly { get; set; }

            public bool SingleTable { get; set; }

            public EntityCollection<ViewField> ViewFields { get; set; }

            public string ViewName { get; set; }
            public string DisplayName { get; set; }

            public EntityCollection<ViewTableRelationship> ViewTableRelationships { get; set; }

            public EntityCollection<ViewTable> ViewTables { get; set; }
          
            public Nullable<global::System.Boolean> UseStoredProcedure  { get; set; }
           
            public Nullable<global::System.Int32> CreateSP_ID  { get; set; }
           
            public Nullable<global::System.Int32> ReadSP_ID  { get; set; }
         
            public Nullable<global::System.Int32> UpdateSP_ID  { get; set; }

            public Nullable<global::System.Int32> DeleteSP_ID { get; set; }
           
        }
    }

    // The MetadataTypeAttribute identifies ViewFieldMetadata as the class
    // that carries additional metadata for the ViewField class.
    //[MetadataTypeAttribute(typeof(ViewField.ViewFieldMetadata))]
    //public partial class ViewField
    //{

    //    // This class allows you to attach custom attributes to properties
    //    // of the ViewField class.
    //    //
    //    // For example, the following marks the Xyz property as a
    //    // required property and specifies the format for valid values:
    //    //    [Required]
    //    //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
    //    //    [StringLength(32)]
    //    //    public string Xyz { get; set; }
    //    internal sealed class ViewFieldMetadata
    //    {

    //        // Metadata classes are not meant to be instantiated.
    //        private ViewFieldMetadata()
    //        {
    //        }

    //        public string DisplayName { get; set; }

    //        public int Feild_ID { get; set; }

    //        public Field Field { get; set; }

    //        public int ID { get; set; }

    //        public Table Table { get; set; }

    //        public int Table_ID { get; set; }

    //        public Nullable<bool> UseValueField { get; set; }

    //        public View View { get; set; }

    //        public int View_ID { get; set; }
    //    }
    //}

    // The MetadataTypeAttribute identifies ViewTableMetadata as the class
    // that carries additional metadata for the ViewTable class.
    [MetadataTypeAttribute(typeof(ViewTable.ViewTableMetadata))]
    public partial class ViewTable
    {

        // This class allows you to attach custom attributes to properties
        // of the ViewTable class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ViewTableMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ViewTableMetadata()
            {
            }

            public int ID { get; set; }

            public Table Table { get; set; }

            public int Table_ID { get; set; }

            public View View { get; set; }

            public int View_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ViewTableRelationshipMetadata as the class
    // that carries additional metadata for the ViewTableRelationship class.
    [MetadataTypeAttribute(typeof(ViewTableRelationship.ViewTableRelationshipMetadata))]
    public partial class ViewTableRelationship
    {

        // This class allows you to attach custom attributes to properties
        // of the ViewTableRelationship class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ViewTableRelationshipMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ViewTableRelationshipMetadata()
            {
            }

            public int Field_ID_A { get; set; }

            public int Field_ID_B { get; set; }

            public int ID { get; set; }

            public int Table_ID_A { get; set; }

            public int Table_ID_B { get; set; }

            public View View { get; set; }

            public int View_ID { get; set; }
        }
    }

    [MetadataType(typeof(DropDownPairResult.DropDownPairResultMetadata))]
    public partial class DropDownPairResult
    {
        internal sealed class DropDownPairResultMetadata
        {
            public DropDownPairResultMetadata()
            {

            }
            [Key]
            public global::System.String ConnectionString { get; set; }

            public global::System.String SqlQueryString { get; set; }

        }
    }

    // The MetadataTypeAttribute identifies GroupModelMetadata as the class
    // that carries additional metadata for the GroupModel class.
    [MetadataTypeAttribute(typeof(GroupModel.GroupModelMetadata))]
    public partial class GroupModel
    {

        // This class allows you to attach custom attributes to properties
        // of the GroupModel class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class GroupModelMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private GroupModelMetadata()
            {
            }

            public Group Group { get; set; }

            public int Group_ID { get; set; }

            public int ID { get; set; }

            public long Model_ID { get; set; }

            public string ModelName { get; set; }
        }
    }


    // The MetadataTypeAttribute identifies DefinitionOptionMetadata as the class
    // that carries additional metadata for the DefinitionOption class.
    [MetadataTypeAttribute(typeof(DefinitionOption.DefinitionOptionMetadata))]
    public partial class DefinitionOption
    {

        // This class allows you to attach custom attributes to properties
        // of the DefinitionOption class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class DefinitionOptionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private DefinitionOptionMetadata()
            {
            }

            public string Description { get; set; }

            public EntityCollection<DisplayDefinitionOption> DisplayDefinitionOptions { get; set; }

            public int ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies DisplayDefinitionOptionMetadata as the class
    // that carries additional metadata for the DisplayDefinitionOption class.
    [MetadataTypeAttribute(typeof(DisplayDefinitionOption.DisplayDefinitionOptionMetadata))]
    public partial class DisplayDefinitionOption
    {

        // This class allows you to attach custom attributes to properties
        // of the DisplayDefinitionOption class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class DisplayDefinitionOptionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private DisplayDefinitionOptionMetadata()
            {
            }

             [Include]
            public DefinitionOption DefinitionOption { get; set; }

            public int DefinitionOption_ID { get; set; }

            public DisplayDefinition DisplayDefinition { get; set; }

            public int DisplayDefinition_ID { get; set; }
             [Include]
            public Server Server { get; set; }
            public int Server_ID { get; set; }

            public int ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies GridStoreProcedureMetadata as the class
    // that carries additional metadata for the GridStoreProcedure class.
    [MetadataTypeAttribute(typeof(GridStoreProcedure.GridStoreProcedureMetadata))]
    public partial class GridStoreProcedure
    {

        // This class allows you to attach custom attributes to properties
        // of the GridStoreProcedure class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class GridStoreProcedureMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private GridStoreProcedureMetadata()
            {
            }

            public DisplayDefinition DisplayDefinition { get; set; }

            public Nullable<int> DisplayDefinition_ID { get; set; }

            public int GridNumber { get; set; }

            public int ID { get; set; }

            public StoredProcedureName StoredProcedureName { get; set; }

            public Nullable<int> StoreProcedureNames_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies StoredProcedureNameMetadata as the class
    // that carries additional metadata for the StoredProcedureName class.
    //[MetadataTypeAttribute(typeof(StoredProcedureName.StoredProcedureNameMetadata))]
    //public partial class StoredProcedureName
    //{

    //    // This class allows you to attach custom attributes to properties
    //    // of the StoredProcedureName class.
    //    //
    //    // For example, the following marks the Xyz property as a
    //    // required property and specifies the format for valid values:
    //    //    [Required]
    //    //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
    //    //    [StringLength(32)]
    //    //    public string Xyz { get; set; }
    //    internal sealed class StoredProcedureNameMetadata
    //    {

    //        // Metadata classes are not meant to be instantiated.
    //        private StoredProcedureNameMetadata()
    //        {
    //        }

    //        public string DBName { get; set; }

    //        public EntityCollection<GridStoreProcedure> GridStoreProcedures { get; set; }

    //        public int ID { get; set; }

    //        public int Server_ID { get; set; }

    //        public string SP_Function { get; set; }

    //        public string SP_Name { get; set; }
    //    }
    //}

    // The MetadataTypeAttribute identifies StoredProcedureNameMetadata as the class
    // that carries additional metadata for the StoredProcedureName class.
    [MetadataTypeAttribute(typeof(StoredProcedureName.StoredProcedureNameMetadata))]
    public partial class StoredProcedureName
    {

        // This class allows you to attach custom attributes to properties
        // of the StoredProcedureName class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class StoredProcedureNameMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private StoredProcedureNameMetadata()
            {
            }

            public string DBName { get; set; }

            public EntityCollection<GridStoreProcedure> GridStoreProcedures { get; set; }

            public int ID { get; set; }

            public Nullable<bool> KnownFields { get; set; }

            [Include]
            public Server Server { get; set; }

            public int Server_ID { get; set; }

            public string SP_Function { get; set; }

            public string SP_Name { get; set; }

            [Include]
            [Association("FK_StoredProcedureParameters_StoredProcedureNames", "ID", "StoredProcedureNames_ID")]
            public EntityCollection<StoredProcedureParameter> StoredProcedureParameters { get; set; }

            public EntityCollection<View> Views { get; set; }

            public EntityCollection<View> Views1 { get; set; }

            public EntityCollection<View> Views2 { get; set; }

            public EntityCollection<View> Views3 { get; set; }
        }
    }


    [MetadataTypeAttribute(typeof(ReadField.ReadFieldMetadata))]
    public partial class ReadField
    {

        // This class allows you to attach custom attributes to properties
        // of the ReadField class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ReadFieldMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ReadFieldMetadata()
            {
            }

            public string DisplayName { get; set; }

            public string FieldName { get; set; }

            public string FieldType { get; set; }

            public Nullable<bool> Hidden { get; set; }

            public Nullable<byte> isPrimaryKey { get; set; }

            public Nullable<bool> ReadOnly { get; set; }
            public Nullable<bool> ForCreate { get; set; }
            public Nullable<bool> ForRead { get; set; }
            public Nullable<bool> ForDelete { get; set; }
            public Nullable<bool> ForUpdate { get; set; }


            public Nullable<bool> UseValueStoredProcedure { get; set; } 

            public Nullable<bool> UseValueField { get; set; }

            public string SP_Function { get; set; }

            public Nullable<bool>  AutoPopulate{ get; set; }
            
            public Nullable<int> StoredProcedureNames_ID { get; set; }

            public string TableName { get; set; }

            public int ViewID { get; set; }

            public int ID { get; set; } 

            public string DefaultValue { get; set; }

            public string FieldOptionFilter { get; set; }
        }
    }
    // The MetadataTypeAttribute identifies StoredProcedureParameterMetadata as the class
    // that carries additional metadata for the StoredProcedureParameter class.
    [MetadataTypeAttribute(typeof(StoredProcedureParameter.StoredProcedureParameterMetadata))]
    public partial class StoredProcedureParameter
    {

        // This class allows you to attach custom attributes to properties
        // of the StoredProcedureParameter class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class StoredProcedureParameterMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private StoredProcedureParameterMetadata()
            {
            }

            public string DataType { get; set; }

            public int ID { get; set; }

            public string ParameterName { get; set; }

            public StoredProcedureName StoredProcedureName { get; set; }

            public int StoredProcedureNames_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ViewFieldMetadata as the class
    // that carries additional metadata for the ViewField class.
    [MetadataTypeAttribute(typeof(ViewField.ViewFieldMetadata))]
    public partial class ViewField
    {

        // This class allows you to attach custom attributes to properties
        // of the ViewField class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ViewFieldMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ViewFieldMetadata()
            {
            }

            public string DisplayName { get; set; }

            public Nullable<int> Feild_ID { get; set; }

            public Field Field { get; set; }

            public Nullable<bool> ForCreate { get; set; }

            public Nullable<bool> ForDelete { get; set; }

            public Nullable<bool> ForRead { get; set; }

            public Nullable<bool> ForUpdate { get; set; }

            public Nullable<bool> Hidden { get; set; }

            public int ID { get; set; }

            public Nullable<bool> ReadOnly { get; set; }

            public Table Table { get; set; }

            public int Table_ID { get; set; }

            public Nullable<bool> UseValueField { get; set; }

            public Nullable<bool> UsingTable { get; set; }

            public View View { get; set; }

            public int View_ID { get; set; }

            public string FieldOptionFilter { get; set; }
            public Nullable<bool> AutoPopulate { get; set; }
             public Nullable<bool> ForTSAdjust { get; set; }
             public string DefaultValue{ get; set; }
            
                
            
        }
    }

    [MetadataTypeAttribute(typeof(FieldCompletionOption.FieldCompletionOptionMetadata))]
    public partial class FieldCompletionOption
    {

        // This class allows you to attach custom attributes to properties
        // of the FieldCompletionOption class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class FieldCompletionOptionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private FieldCompletionOptionMetadata()
            {
            }

            public string Filter { get; set; }

            public long ID { get; set; }

            public Nullable<long> Model_ID { get; set; }

            public Nullable<int> SortOrder { get; set; }

            public string Value { get; set; }
        }
    }
}
