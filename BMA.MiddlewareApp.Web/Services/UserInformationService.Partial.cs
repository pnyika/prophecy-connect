﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.ServiceModel.DomainServices.Server;

namespace BMA.MiddlewareApp.Web.Services
{
    public partial class UserInformationService
    {
        public void InsertLoginUserInfo(LoginUserInfo loginUserInfo)
        {
            IsUsernameValid(loginUserInfo.UserName);
            loginUserInfo.PasswordHash = FormsAuthentication.HashPasswordForStoringInConfigFile(loginUserInfo.PasswordHash, "SHA1");
            loginUserInfo.PasswordSalt = FormsAuthentication.HashPasswordForStoringInConfigFile(loginUserInfo.PasswordHash, "SHA1");


                this.ObjectContext.LoginUserInfoes.AddObject(loginUserInfo);
                ObjectContext.SaveChanges();

                int roleID = ObjectContext.Roles.Where(r => r.Name == loginUserInfo.Role).FirstOrDefault().ID;

                LoginUserRole userRole = new LoginUserRole();
                userRole.RoleID = roleID;
                userRole.UserID = loginUserInfo.ID;
                ObjectContext.LoginUserRoles.AddObject(userRole);
                ObjectContext.SaveChanges();


                LoginProfile profile = new LoginProfile();
                profile.CurrentTheme = "Office_SilverTheme";//default
                profile.IM = "";
                profile.UserID = loginUserInfo.ID;
                ObjectContext.LoginProfiles.AddObject(profile);
                ObjectContext.SaveChanges();
                


           
        }

        public void UpdateLoginUserInfo(LoginUserInfo currentLoginUserInfo)
        {
           
            //this.ObjectContext.LoginUserInfoes.AttachAsModified(currentLoginUserInfo, this.ChangeSet.GetOriginal(currentLoginUserInfo));
        }

        //[Invoke]
        //public bool ValidateUser(string username, string password)
        //{
        //    try
        //    {
        //        LoginUserInfo currentUser = ObjectContext.LoginUserInfoes.Where(e => e.UserName == username &&
        //            e.PasswordHash == FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1") 
        //            ).SingleOrDefault();

        //        if (currentUser != null)
        //            return true;
        //    }
        //    catch (Exception)
        //    {
        //    }

        //    return false;
        //}

        public IQueryable<Role> LoadRoles()
        {
            return ObjectContext.Roles.AsQueryable();
        }

    }
}