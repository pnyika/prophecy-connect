﻿
namespace BMA.MiddlewareApp.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BMA.MiddlewareApp.Web;


    // Implements application logic using the MiddlewareEntities1 context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public partial class UserInformationService : LinqToEntitiesDomainService<MiddlewareEntities1>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'LoginMenuStructures' query.
        public IQueryable<LoginMenuStructure> GetLoginMenuStructures()
        {
            return this.ObjectContext.LoginMenuStructures;
        }

        public void InsertLoginMenuStructure(LoginMenuStructure loginMenuStructure)
        {
            if ((loginMenuStructure.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(loginMenuStructure, EntityState.Added);
            }
            else
            {
                this.ObjectContext.LoginMenuStructures.AddObject(loginMenuStructure);
            }
        }

        public void UpdateLoginMenuStructure(LoginMenuStructure currentLoginMenuStructure)
        {
            this.ObjectContext.LoginMenuStructures.AttachAsModified(currentLoginMenuStructure, this.ChangeSet.GetOriginal(currentLoginMenuStructure));
        }

        public void DeleteLoginMenuStructure(LoginMenuStructure loginMenuStructure)
        {
            if ((loginMenuStructure.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(loginMenuStructure, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.LoginMenuStructures.Attach(loginMenuStructure);
                this.ObjectContext.LoginMenuStructures.DeleteObject(loginMenuStructure);
            }
        }



        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'LoginProfiles' query.
        public IQueryable<LoginProfile> GetLoginProfiles()
        {
            return this.ObjectContext.LoginProfiles;
        }

        public void InsertLoginProfile(LoginProfile loginProfile)
        {
            if ((loginProfile.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(loginProfile, EntityState.Added);
            }
            else
            {
                this.ObjectContext.LoginProfiles.AddObject(loginProfile);
            }
        }

        public void UpdateLoginProfile(LoginProfile currentLoginProfile)
        {
            this.ObjectContext.LoginProfiles.AttachAsModified(currentLoginProfile, this.ChangeSet.GetOriginal(currentLoginProfile));
        }

        public void DeleteLoginProfile(LoginProfile loginProfile)
        {
            if ((loginProfile.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(loginProfile, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.LoginProfiles.Attach(loginProfile);
                this.ObjectContext.LoginProfiles.DeleteObject(loginProfile);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'LoginUserInfoes' query.

        
        private void IsUsernameValid(string username)
        {

             LoginUserInfo user = ObjectContext.LoginUserInfoes.Where(login => login.UserName == username).FirstOrDefault();
             if (user !=null)
                 throw new ValidationException("The username already exists!");            
             
        }
        public IQueryable<LoginUserInfo> GetLoginUserInfoes()
        {
            return this.ObjectContext.LoginUserInfoes.AsQueryable();
        }

      
        public void DeleteLoginUserInfo(LoginUserInfo loginUserInfo)
        {
            if ((loginUserInfo.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(loginUserInfo, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.LoginUserInfoes.Attach(loginUserInfo);
                this.ObjectContext.LoginUserInfoes.DeleteObject(loginUserInfo);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'LoginUserRoles' query.
        public IQueryable<LoginUserRole> GetLoginUserRoles()
        {
            return this.ObjectContext.LoginUserRoles;
        }

        public void InsertLoginUserRole(LoginUserRole loginUserRole)
        {
            if ((loginUserRole.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(loginUserRole, EntityState.Added);
            }
            else
            {
                this.ObjectContext.LoginUserRoles.AddObject(loginUserRole);
            }
        }

        public void UpdateLoginUserRole(LoginUserRole currentLoginUserRole)
        {
            this.ObjectContext.LoginUserRoles.AttachAsModified(currentLoginUserRole, this.ChangeSet.GetOriginal(currentLoginUserRole));
        }

        public void DeleteLoginUserRole(LoginUserRole loginUserRole)
        {
            if ((loginUserRole.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(loginUserRole, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.LoginUserRoles.Attach(loginUserRole);
                this.ObjectContext.LoginUserRoles.DeleteObject(loginUserRole);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Roles' query.
        public IQueryable<Role> GetRoles()
        {
            return this.ObjectContext.Roles;
        }

        public void InsertRole(Role role)
        {
            if ((role.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(role, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Roles.AddObject(role);
            }
        }

        public void UpdateRole(Role currentRole)
        {
            this.ObjectContext.Roles.AttachAsModified(currentRole, this.ChangeSet.GetOriginal(currentRole));
        }

        public void DeleteRole(Role role)
        {
            if ((role.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(role, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Roles.Attach(role);
                this.ObjectContext.Roles.DeleteObject(role);
            }
        }
    }
}


