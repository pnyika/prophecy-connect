﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace BMA.MiddlewareApp.Web
{
    public partial class GroupViewFilter
    {
        #region Summary
        private string summary;

        [DataMember]
        public string Summary
        {
            get
            {
                return summary;
            }
            set
            {
                summary = value;
                // RaisePropertyChanged("IsIcashDirty");
            }
        }
        #endregion

    }
}