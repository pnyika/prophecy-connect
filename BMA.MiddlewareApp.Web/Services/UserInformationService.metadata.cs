﻿
namespace BMA.MiddlewareApp.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies LoginMenuStructureMetadata as the class
    // that carries additional metadata for the LoginMenuStructure class.
    [MetadataTypeAttribute(typeof(LoginMenuStructure.LoginMenuStructureMetadata))]
    public partial class LoginMenuStructure
    {

        // This class allows you to attach custom attributes to properties
        // of the LoginMenuStructure class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class LoginMenuStructureMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private LoginMenuStructureMetadata()
            {
            }

            public string Description { get; set; }

            public int ID { get; set; }

            public string MenuName { get; set; }

            public EntityCollection<LoginUserInfo> UserInformations { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies LoginProfileMetadata as the class
    // that carries additional metadata for the LoginProfile class.
    [MetadataTypeAttribute(typeof(LoginProfile.LoginProfileMetadata))]
    public partial class LoginProfile
    {

        // This class allows you to attach custom attributes to properties
        // of the LoginProfile class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class LoginProfileMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private LoginProfileMetadata()
            {
            }

            public string CurrentTheme { get; set; }

            public int ID { get; set; }

            public string IM { get; set; }

            public int UserID { get; set; }

            public int PageSize { get; set; }

            public bool IsDefault { get; set; }

            public LoginUserInfo UserInformation { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies LoginUserInfoMetadata as the class
    // that carries additional metadata for the LoginUserInfo class.
    [MetadataTypeAttribute(typeof(LoginUserInfo.LoginUserInfoMetadata))]
    public partial class LoginUserInfo
    {

        // This class allows you to attach custom attributes to properties
        // of the LoginUserInfo class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class LoginUserInfoMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private LoginUserInfoMetadata()
            {
            }

            [Required]
            [Display(Name = "Email Address:")]
            [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email address")]
            public string EmailAddress { get; set; }

            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [ReadOnly(true)]
            public int ID { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }


            [Include]
            //   [DisplayColumnAttribute("Description")]
            public MenuStructure MenuStructure { get; set; }

            [Display(Name = "Menu")]
            public Nullable<int> MenuStructure_ID { get; set; }

            [Display(Name = "Middle Name")]
            public string MiddleName { get; set; }



            public DateTime ModifiedDate
            {
                get;

                set;
            }

            [Required]
            [Display(Name = "Password:")]
            [StringLength(256, MinimumLength = 6, ErrorMessage = "Password must be at least 6 characters")]
            public string PasswordHash { get; set; }


            [Display(Name = "Confirm Password:")]
            [CustomValidation(typeof(UserValidation), "ConfirmPassword")]
            public string PasswordSalt
            {
              get;
              set;
            }
            


            [StringLength(10)]
            public string Phone { get; set; }

             [Display(Name = "Group:")]
            public Nullable<int> Group_ID { get; set; }
            
            public EntityCollection<LoginProfile> Profiles { get; set; }

            public string Title { get; set; }

             // [CustomValidation(typeof(UserValidation), "EnsureValidUsername")]
             [Required]
            public string UserName { get; set; }

            public EntityCollection<LoginUserRole> UserRoles { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies LoginUserRoleMetadata as the class
    // that carries additional metadata for the LoginUserRole class.
    [MetadataTypeAttribute(typeof(LoginUserRole.LoginUserRoleMetadata))]
    public partial class LoginUserRole
    {

        // This class allows you to attach custom attributes to properties
        // of the LoginUserRole class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class LoginUserRoleMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private LoginUserRoleMetadata()
            {
            }

            public int ID { get; set; }

            public Role Role { get; set; }

            public int RoleID { get; set; }

            public int UserID { get; set; }

            public LoginUserInfo UserInformation { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies RoleMetadata as the class
    // that carries additional metadata for the Role class.
    [MetadataTypeAttribute(typeof(Role.RoleMetadata))]
    public partial class Role
    {

        // This class allows you to attach custom attributes to properties
        // of the Role class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class RoleMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private RoleMetadata()
            {
            }

            public string Description { get; set; }

            public int ID { get; set; }

            public string Name { get; set; }

            public EntityCollection<LoginUserRole> UserRoles { get; set; }
        }
    }

  
}
