﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace BMA.MiddlewareApp.Web
{
    public partial class LoginUserInfo
    {

       


        private string role;

        [DataMember]
        public string Role
        {
            get
            {
                return role;
            }
            set
            {
                role = value;
                // RaisePropertyChanged("IsIcashDirty");
            }
        }


        private string currentTheme;

        [DataMember]
        public string CurrentTheme
        {
            get
            {
                return currentTheme;
            }
            set
            {
                
                currentTheme = value;
               
            }
        }
    }
}