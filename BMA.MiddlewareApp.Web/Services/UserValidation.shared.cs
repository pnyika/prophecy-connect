﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Web;
using System.ComponentModel.DataAnnotations;
using BMA.MiddlewareApp.Web.Services;
namespace BMA.MiddlewareApp.Web
{
    public class UserValidation
    {
        
        public static ValidationResult ConfirmPassword(string passwordSalt,
         ValidationContext ctx
         )
        {


            LoginUserInfo cust = (LoginUserInfo)ctx.ObjectInstance;


            if (passwordSalt != cust.PasswordHash)

                return new ValidationResult("Password doesnot match!");
            return ValidationResult.Success;

        }
    }
}