﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BMA.MiddlewareApp.Web.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="shortcut icon" type="image/png" href="prophecyconnecticon.png"/>
	<link rel="icon" type="image/png" href="prophecyconnecticon.png"/>
    <link href="login-box.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script>
     <script  type="text/javascript">
         // beat the heart 
         // 'times' (int): nr of times to beat
         function resize() {
             $('#container').css({
                 position: 'absolute',
                 left: ($(window).width() - $('#container').outerWidth()) / 2,
                 top: ($(window).height() - $('#container').outerHeight()) / 2
             });

             
         }

         $(function () {

             resize();

         });
</script>


</head>
<body  style="zoom: 100%">
    <form id="form1" runat="server">
   
<div id="container">


<div id="login-box">

<H2>Network Login</H2>
Please supply Network credintial.
<br />
<br />
<div id="login-box-name" style="margin-top:20px;">UserName:</div><div id="login-box-field" style="margin-top:20px;">
<input name="q" id="txtUsername" runat="server" class="form-login" title="Username" value="" size="30" maxlength="2048" />
</div>
<div id="login-box-name">Password:</div><div id="login-box-field">
<input name="q" type="password" id ="txtPassword" class="form-login" title="Password" runat="server"  value="" size="30" maxlength="2048" />
</div>
<br />
<span class="login-box-options"><input type="checkbox" name="1" value="1" runat="server"> Remember Me </span>
<br />
<asp:Label ID="lblmsg" ForeColor="Red" runat="server"></asp:Label>
<br />

<asp:ImageButton src="images/login-btn.png" width="103" height="42" 
        style="margin-left:90px;" runat="server" ID="btnLogin" 
        onclick="btnLogin_Click" />





</div>

</div>
    </form>
</body>
</html>
