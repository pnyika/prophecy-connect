﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace BMA.MiddlewareApp.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DataTableService" in code, svc and config file together.
    public class DataTableService : IDataTableService
    {
        IEnumerable<Dictionary<string, object>> IDataTableService.GetData(string sql)
        {
            var table = GetDataTable(sql);

            var columns = table.Columns.Cast<DataColumn>();

            return table.AsEnumerable().Select(r => columns.Select(c =>
                                 new { Column = c.ColumnName, Value = r[c] })
                             .ToDictionary(i => i.Column, i => i.Value != DBNull.Value ? i.Value : null));
        }





        public DataTable GetDataTable(string sql)
        {

            var connString = ConfigurationManager.ConnectionStrings["MiddlewareConnectionString"].ConnectionString;
            var conn = new SqlConnection(connString);
            var adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(sql, conn);

            var table = new DataTable();

            conn.Open();
            try
            {
                adapter.Fill(table);
            }
            finally
            {
                conn.Close();
            }

            return table;
        }
    }
}


