﻿
namespace BMA.MiddlewareApp.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // Implements application logic using the EOInterfaceEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class EOInterfaceService : LinqToEntitiesDomainService<EOInterfaceEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'ModelDefinitions' query.
        public IQueryable<ModelDefinition> GetModelDefinitions()
        {
            return this.ObjectContext.ModelDefinitions;
        }

        public void InsertModelDefinition(ModelDefinition modelDefinition)
        {
            if ((modelDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(modelDefinition, EntityState.Added);
            }
            else
            {
                this.ObjectContext.ModelDefinitions.AddObject(modelDefinition);
            }
        }

        public void UpdateModelDefinition(ModelDefinition currentModelDefinition)
        {
            this.ObjectContext.ModelDefinitions.AttachAsModified(currentModelDefinition, this.ChangeSet.GetOriginal(currentModelDefinition));
        }

        public void DeleteModelDefinition(ModelDefinition modelDefinition)
        {
            if ((modelDefinition.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(modelDefinition, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.ModelDefinitions.Attach(modelDefinition);
                this.ObjectContext.ModelDefinitions.DeleteObject(modelDefinition);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'MTPAdjustments' query.
        public IQueryable<MTPAdjustment> GetMTPAdjustments()
        {
            return this.ObjectContext.MTPAdjustments;
        }

        public void InsertMTPAdjustment(MTPAdjustment mTPAdjustment)
        {
            if ((mTPAdjustment.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(mTPAdjustment, EntityState.Added);
            }
            else
            {
                this.ObjectContext.MTPAdjustments.AddObject(mTPAdjustment);
            }
        }

        public void UpdateMTPAdjustment(MTPAdjustment currentMTPAdjustment)
        {
            this.ObjectContext.MTPAdjustments.AttachAsModified(currentMTPAdjustment, this.ChangeSet.GetOriginal(currentMTPAdjustment));
        }

        public void DeleteMTPAdjustment(MTPAdjustment mTPAdjustment)
        {
            if ((mTPAdjustment.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(mTPAdjustment, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.MTPAdjustments.Attach(mTPAdjustment);
                this.ObjectContext.MTPAdjustments.DeleteObject(mTPAdjustment);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'TSAdjustments' query.
        public IQueryable<TSAdjustment> GetTSAdjustments()
        {
            return this.ObjectContext.TSAdjustments;
        }

        public void InsertTSAdjustment(TSAdjustment tSAdjustment)
        {
            if ((tSAdjustment.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tSAdjustment, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TSAdjustments.AddObject(tSAdjustment);
            }
        }

        public void UpdateTSAdjustment(TSAdjustment currentTSAdjustment)
        {
            this.ObjectContext.TSAdjustments.AttachAsModified(currentTSAdjustment, this.ChangeSet.GetOriginal(currentTSAdjustment));
        }

        public void DeleteTSAdjustment(TSAdjustment tSAdjustment)
        {
            if ((tSAdjustment.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tSAdjustment, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TSAdjustments.Attach(tSAdjustment);
                this.ObjectContext.TSAdjustments.DeleteObject(tSAdjustment);
            }
        }




        public IQueryable<Attribute> GetAttributes()
        {
            return this.ObjectContext.Attributes;
        }

        public void InsertAttribute(Attribute attribute)
        {
            if ((attribute.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(attribute, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Attributes.AddObject(attribute);
            }
        }

        public void UpdateAttribute(Attribute currentAttribute)
        {
            this.ObjectContext.Attributes.AttachAsModified(currentAttribute, this.ChangeSet.GetOriginal(currentAttribute));
        }

        public void DeleteAttribute(Attribute attribute)
        {
            if ((attribute.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(attribute, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Attributes.Attach(attribute);
                this.ObjectContext.Attributes.DeleteObject(attribute);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Calendars' query.
        public IQueryable<Calendar> GetCalendars()
        {
            return this.ObjectContext.Calendars;
        }

        public void InsertCalendar(Calendar calendar)
        {
            if ((calendar.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calendar, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Calendars.AddObject(calendar);
            }
        }

        public void UpdateCalendar(Calendar currentCalendar)
        {
            this.ObjectContext.Calendars.AttachAsModified(currentCalendar, this.ChangeSet.GetOriginal(currentCalendar));
        }

        public void DeleteCalendar(Calendar calendar)
        {
            if ((calendar.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calendar, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Calendars.Attach(calendar);
                this.ObjectContext.Calendars.DeleteObject(calendar);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'CalendarDetails' query.
        public IQueryable<CalendarDetail> GetCalendarDetails()
        {
            return this.ObjectContext.CalendarDetails;
        }

        public void InsertCalendarDetail(CalendarDetail calendarDetail)
        {
            if ((calendarDetail.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calendarDetail, EntityState.Added);
            }
            else
            {
                this.ObjectContext.CalendarDetails.AddObject(calendarDetail);
            }
        }

        public void UpdateCalendarDetail(CalendarDetail currentCalendarDetail)
        {
            this.ObjectContext.CalendarDetails.AttachAsModified(currentCalendarDetail, this.ChangeSet.GetOriginal(currentCalendarDetail));
        }

        public void DeleteCalendarDetail(CalendarDetail calendarDetail)
        {
            if ((calendarDetail.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calendarDetail, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.CalendarDetails.Attach(calendarDetail);
                this.ObjectContext.CalendarDetails.DeleteObject(calendarDetail);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'CalendarPeriods' query.
        public IQueryable<CalendarPeriod> GetCalendarPeriods()
        {
            return this.ObjectContext.CalendarPeriods;
        }

        public void InsertCalendarPeriod(CalendarPeriod calendarPeriod)
        {
            if ((calendarPeriod.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calendarPeriod, EntityState.Added);
            }
            else
            {
                this.ObjectContext.CalendarPeriods.AddObject(calendarPeriod);
            }
        }

        public void UpdateCalendarPeriod(CalendarPeriod currentCalendarPeriod)
        {
            this.ObjectContext.CalendarPeriods.AttachAsModified(currentCalendarPeriod, this.ChangeSet.GetOriginal(currentCalendarPeriod));
        }

        public void DeleteCalendarPeriod(CalendarPeriod calendarPeriod)
        {
            if ((calendarPeriod.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(calendarPeriod, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.CalendarPeriods.Attach(calendarPeriod);
                this.ObjectContext.CalendarPeriods.DeleteObject(calendarPeriod);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Facilities' query.
        public IQueryable<Facility> GetFacilities()
        {
            return this.ObjectContext.Facilities;
        }

        public void InsertFacility(Facility facility)
        {
            if ((facility.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(facility, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Facilities.AddObject(facility);
            }
        }

        public void UpdateFacility(Facility currentFacility)
        {
            this.ObjectContext.Facilities.AttachAsModified(currentFacility, this.ChangeSet.GetOriginal(currentFacility));
        }

        public void DeleteFacility(Facility facility)
        {
            if ((facility.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(facility, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Facilities.Attach(facility);
                this.ObjectContext.Facilities.DeleteObject(facility);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Materials' query.
        public IQueryable<Material> GetMaterials()
        {
            return this.ObjectContext.Materials;
        }

        public void InsertMaterial(Material material)
        {
            if ((material.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(material, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Materials.AddObject(material);
            }
        }

        public void UpdateMaterial(Material currentMaterial)
        {
            this.ObjectContext.Materials.AttachAsModified(currentMaterial, this.ChangeSet.GetOriginal(currentMaterial));
        }

        public void DeleteMaterial(Material material)
        {
            if ((material.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(material, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Materials.Attach(material);
                this.ObjectContext.Materials.DeleteObject(material);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Models' query.
        public IQueryable<Model> GetModels()
        {
            return this.ObjectContext.Models;
        }

        public void InsertModel(Model model)
        {
            if ((model.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(model, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Models.AddObject(model);
            }
        }

        public void UpdateModel(Model currentModel)
        {
            this.ObjectContext.Models.AttachAsModified(currentModel, this.ChangeSet.GetOriginal(currentModel));
        }

        public void DeleteModel(Model model)
        {
            if ((model.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(model, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Models.Attach(model);
                this.ObjectContext.Models.DeleteObject(model);
            }
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
       

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Objects' query.
        public IQueryable<Object> GetObjects()
        {
            return this.ObjectContext.Objects;
        }

        public void InsertObject(Object @object)
        {
            if ((@object.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(@object, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Objects.AddObject(@object);
            }
        }

        public void UpdateObject(Object currentObject)
        {
            this.ObjectContext.Objects.AttachAsModified(currentObject, this.ChangeSet.GetOriginal(currentObject));
        }

        public void DeleteObject(Object @object)
        {
            if ((@object.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(@object, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Objects.Attach(@object);
                this.ObjectContext.Objects.DeleteObject(@object);
            }
        }
    }
}


