﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Data.OleDb;
using BMA.MiddlewareApp.Web;
using System.Text.RegularExpressions;
using System.Net.Mail;
using BMA.DataSetSerialiser;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using System.Web.Security;
using System.Web;
using System.Linq;
using Microsoft.SqlServer.Server;
using System.Configuration;
using Excel;
using System.Data.SqlTypes;
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GetData" in code, svc and config file together.

    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GetData
    {
        // Change this connection string your need.
        private const string connectionString = @"Data Source=localhost;Initial Catalog=Middleware;Integrated Security=True;";


        [OperationContract]
        public bool AuthenticationUser()
        {

            string user = "Prince"; string pass = "intermerat";

            string currentUser = ServiceSecurityContext.Current.WindowsIdentity.Name; 
            bool valid = false;
            try
            {


                DirectorySearcher ds = new DirectorySearcher(new DirectoryEntry("LDAP://localhost", user, pass));
                ds.Filter = String.Format("(SAMAccountName={0})", currentUser);
                ds.PropertiesToLoad.Add("SAMAccountName");
                SearchResult result = ds.FindOne();
                valid = result != null;
            }
            catch (COMException)
            {
                //wrong pass or expired pass
                valid = false;
            }
            return valid;
        }



        #region Get Services
        [OperationContract]
        public DataSetData GetDataSetData(string connString, string databaseName, string SQL, int PageNumber, int PageSize, out CustomException ServiceError)
        {
            try
            {
                //SQL = SQL.Replace("~", "[");
                //SQL = SQL.Replace("+","]");
                DataSet ds = GetDataSet(connString, databaseName, "use [" + databaseName + "] ;" + SQL, PageNumber, PageSize);
                ServiceError = null;
                return DataSetData.FromDataSet(ds);
            }
            catch (Exception ex)
            {
                ServiceError = new CustomException(ex);
            }
            return null;
        }



        private DataSet GetDataSet(string connString, string databaseName, string SQL, int PageNumber, int PageSize)
        {
            DataSet ds;
            SqlConnection Connection = new SqlConnection(connString);
            try
            {
                Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(SQL);
                adapter.SelectCommand.Connection = Connection;
                if (PageSize > 0)// Set rowcount =PageNumber * PageSize for best performance
                {
                    long RowCount = PageNumber * PageSize;
                    SqlCommand cmd = new SqlCommand("SET ROWCOUNT " + RowCount.ToString() + " SET NO_BROWSETABLE ON", (SqlConnection)Connection);
                    cmd.ExecuteNonQuery();
                }
                ds = new DataSet();
                adapter.Fill(ds, (PageNumber - 1) * PageSize, PageSize, "Data");
                adapter.FillSchema(ds, SchemaType.Mapped, "DataSchema");
                if (PageSize > 0) // Reset Rowcount back to 0
                {
                    SqlCommand cmd = new SqlCommand("SET ROWCOUNT 0 SET NO_BROWSETABLE OFF", (SqlConnection)Connection);
                    cmd.ExecuteNonQuery();
                }
                if (ds.Tables.Count > 1)
                {
                    DataTable data = ds.Tables["Data"];
                    DataTable schema = ds.Tables["DataSchema"];
                    data.Merge(schema);
                    ds.Tables.Remove(schema);
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                Connection.Close();
            }
            return ds;
        }




        //#region Get Services
        //[OperationContract]
        //public DataSetData TestLogin(string username, string pass , out CustomException ServiceError)
        //{
        //    try
        //    {

        //        string connString = ConfigurationManager.ConnectionStrings[""].ToString();
        //        string database = "Middleware";
        //        string SQL = "Select FirstName , LastName from Midddleware where username = " 
        //              string pass = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
        //            int PageNumber= 1;int PageSize = 5;
        //        //SQL = SQL.Replace("~", "[");
        //        //SQL = SQL.Replace("+","]");
        //        DataSet ds = GetDataSet(connString, databaseName, "use [" + databaseName + "] ;" + SQL, PageNumber, PageSize);
        //        ServiceError = null;
        //        return DataSetData.FromDataSet(ds);
        //    }
        //    catch (Exception ex)
        //    {
        //        ServiceError = new CustomException(ex);
        //    }
        //    return null;
        //}



        //private DataSet GetDataSet(string connString, string databaseName, string SQL, int PageNumber, int PageSize)
        //{
        //    DataSet ds;
        //    SqlConnection Connection = new SqlConnection(connString);
        //    try
        //    {
        //        Connection.Open();
        //        SqlDataAdapter adapter = new SqlDataAdapter();
        //        adapter.SelectCommand = new SqlCommand(SQL);
        //        adapter.SelectCommand.Connection = Connection;
        //        if (PageSize > 0)// Set rowcount =PageNumber * PageSize for best performance
        //        {
        //            long RowCount = PageNumber * PageSize;
        //            SqlCommand cmd = new SqlCommand("SET ROWCOUNT " + RowCount.ToString() + " SET NO_BROWSETABLE ON", (SqlConnection)Connection);
        //            cmd.ExecuteNonQuery();
        //        }
        //        ds = new DataSet();
        //        adapter.Fill(ds, (PageNumber - 1) * PageSize, PageSize, "Data");
        //        adapter.FillSchema(ds, SchemaType.Mapped, "DataSchema");
        //        if (PageSize > 0) // Reset Rowcount back to 0
        //        {
        //            SqlCommand cmd = new SqlCommand("SET ROWCOUNT 0 SET NO_BROWSETABLE OFF", (SqlConnection)Connection);
        //            cmd.ExecuteNonQuery();
        //        }
        //        if (ds.Tables.Count > 1)
        //        {
        //            DataTable data = ds.Tables["Data"];
        //            DataTable schema = ds.Tables["DataSchema"];
        //            data.Merge(schema);
        //            ds.Tables.Remove(schema);
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        throw err;
        //    }
        //    finally
        //    {
        //        Connection.Close();
        //    }
        //    return ds;
        //}
       



        //get tables
        [OperationContract]
        public DataSetData GetTableOnDatabase(string connString, string databaseName, string SQL, int PageNumber, int PageSize, out CustomException ServiceError)
        {
            try
            {
                DataSet ds = GetTables(connString, databaseName, SQL, PageNumber, PageSize);
                ServiceError = null;
                return DataSetData.FromDataSet(ds);
            }
            catch (Exception ex)
            {
                ServiceError = new CustomException(ex);
            }
            return null;
        }



        private DataSet GetTables(string connString, string databaseName, string SQL, int PageNumber, int PageSize)
        {
            DataSet ds;
            SqlConnection Connection = new SqlConnection(connString);
            try
            {
                Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(SQL);
                adapter.SelectCommand.Connection = Connection;
                if (PageSize > 0)// Set rowcount =PageNumber * PageSize for best performance
                {
                    long RowCount = PageNumber * PageSize;
                    SqlCommand cmd = new SqlCommand("SET ROWCOUNT " + RowCount.ToString() + " SET NO_BROWSETABLE ON", (SqlConnection)Connection);
                    cmd.ExecuteNonQuery();
                }
                ds = new DataSet();
                adapter.Fill(ds, (PageNumber - 1) * PageSize, PageSize, "Data");
                adapter.FillSchema(ds, SchemaType.Mapped, "DataSchema");
                if (PageSize > 0) // Reset Rowcount back to 0
                {
                    SqlCommand cmd = new SqlCommand("SET ROWCOUNT 0 SET NO_BROWSETABLE OFF", (SqlConnection)Connection);
                    cmd.ExecuteNonQuery();
                }
                if (ds.Tables.Count > 1)
                {
                    DataTable data = ds.Tables["Data"];
                    DataTable schema = ds.Tables["DataSchema"];
                    data.Merge(schema);
                    ds.Tables.Remove(schema);
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                Connection.Close();
            }
            return ds;
        }

        [OperationContract]
        public DataSetData ExecuteStoreProc(string connString, string databaseName, string StoredProcedureName, List<string> parameters,int PageNumber, int PageSize,  out CustomException ServiceError)
        {
            System.Globalization.CultureInfo cultureInfo =
      new System.Globalization.CultureInfo("en-ZA");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");
            System.Globalization.DateTimeFormatInfo dateTimeInfo =
 new System.Globalization.DateTimeFormatInfo();
            dateTimeInfo.LongDatePattern = "dd-MMM-yyyy";
            dateTimeInfo.ShortDatePattern = "dd-MMM-yyyy";
            cultureInfo.DateTimeFormat = dateTimeInfo;

            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = dateTimeInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat = dateTimeInfo;
            int rowsAffected=0;
          
            DataSet ds;
            SqlConnection Connection = new SqlConnection(connString);
            try
            {
                Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(StoredProcedureName);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Connection = Connection;
                foreach (var item in parameters)
                {
                    char[] sep = { ';' };
                    string toList = item;
                    string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    string parameterName = toArray[0].ToString();
                    string parameterValue = toArray[1].ToString();

                    if (parameterName != null)
                    {
                        adapter.SelectCommand.Parameters.Add(new SqlParameter("@" + parameterName, parameterValue));
                    }

                }
                ds = new DataSet();
                if (PageSize > 0)// Set rowcount =PageNumber * PageSize for best performance
                {
                    long RowCount = PageNumber * PageSize;
                 //   SqlCommand cmd = new SqlCommand("SET ROWCOUNT " + RowCount.ToString() + " SET NO_BROWSETABLE ON", (SqlConnection)Connection);
                  //  cmd.ExecuteNonQuery();

                    adapter.Fill(ds, (PageNumber - 1) * PageSize, PageSize, "Data");

                    adapter.FillSchema(ds, SchemaType.Mapped, "DataSchema");
                }
                         
               else
                {

                    adapter.Fill(ds, "Data");

                    adapter.FillSchema(ds, SchemaType.Mapped, "DataSchema");
                }
                if (PageSize > 0) // Reset Rowcount back to 0
                {
                   // SqlCommand cmd = new SqlCommand("SET ROWCOUNT 0 SET NO_BROWSETABLE OFF", (SqlConnection)Connection);
                   // cmd.ExecuteNonQuery();
                }
            //    if (ds.Tables.Count > 1)


              //  {
                //    DataTable data = ds.Tables["Data"];
                 ////   DataTable schema = ds.Tables["DataSchema"];
                  //  if (data != null)
                  ///  {
                       // data.Merge(schema);
                       // ds.Tables.Remove(schema);
             //     //  }
             //   }

                ServiceError = null;
                return DataSetData.FromDataSet(ds);
            }
            catch (Exception ex)
            {
                ServiceError = new CustomException(ex);
            }
            return null;
        }
       
        public DataSet ExecuteStoreProcedure(string connString, string databaseName, string StoredProcedureName, List<string> parameters, int PageNumber, int PageSize)
        {
            System.Globalization.CultureInfo cultureInfo =
      new System.Globalization.CultureInfo("en-ZA");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");
            System.Globalization.DateTimeFormatInfo dateTimeInfo =
 new System.Globalization.DateTimeFormatInfo();
            dateTimeInfo.LongDatePattern = "dd-MMM-yyyy";
            dateTimeInfo.ShortDatePattern = "dd-MMM-yyyy";
            cultureInfo.DateTimeFormat = dateTimeInfo;

            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = dateTimeInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat = dateTimeInfo;
            int rowsAffected = 0;

            DataSet ds;
            SqlConnection Connection = new SqlConnection(connString);
            try
            {
                Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(StoredProcedureName);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Connection = Connection;
                foreach (var item in parameters)
                {
                    char[] sep = { ';' };
                    string toList = item;
                    string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    string parameterName = toArray[0].ToString();
                    string parameterValue = toArray[1].ToString();

                    if (parameterName != null)
                    {
                        adapter.SelectCommand.Parameters.Add(new SqlParameter("@" + parameterName, parameterValue));
                    }

                }
                ds = new DataSet();
                if (PageSize > 0)// Set rowcount =PageNumber * PageSize for best performance
                {
                    long RowCount = PageNumber * PageSize;
                    //   SqlCommand cmd = new SqlCommand("SET ROWCOUNT " + RowCount.ToString() + " SET NO_BROWSETABLE ON", (SqlConnection)Connection);
                    //  cmd.ExecuteNonQuery();

                    adapter.Fill(ds, (PageNumber - 1) * PageSize, PageSize, "Data");

                    adapter.FillSchema(ds, SchemaType.Mapped, "DataSchema");
                }

                else
                {

                    adapter.Fill(ds, "Data");

                    adapter.FillSchema(ds, SchemaType.Mapped, "DataSchema");
                }
                if (PageSize > 0) // Reset Rowcount back to 0
                {
                    // SqlCommand cmd = new SqlCommand("SET ROWCOUNT 0 SET NO_BROWSETABLE OFF", (SqlConnection)Connection);
                    // cmd.ExecuteNonQuery();
                }
                //    if (ds.Tables.Count > 1)


                //  {
                //    DataTable data = ds.Tables["Data"];
                ////   DataTable schema = ds.Tables["DataSchema"];
                //  if (data != null)
                ///  {
                // data.Merge(schema);
                // ds.Tables.Remove(schema);
                //     //  }
                //   }

                
                return ds;
            }
            catch (Exception ex)
            {
                
            }
            return null;
        }
       

        [OperationContract]
        public bool TestConnection(string connString)
        {

            OleDbConnection OleCon = new OleDbConnection();
            ADODB._Connection ADOcon;



            OleCon.ConnectionString = connString;
            OleCon.Open();

            if (OleCon.State.ToString() == "Open")
            {
                return true;
                OleCon.Close();
            }
            else
            {
                return false;
            }

        }

        [OperationContract]
        public List<string> GetUsersOnAD(string path)
        {
            List<string> names = new List<string>();
        var path1 = string.Format("WinNT://{0},Computer", Environment.MachineName);
       
        using (var computerEntry = new DirectoryEntry(path1))
            {
                var userNames = from DirectoryEntry childEntry in computerEntry.Children
                                where childEntry.SchemaClassName == "User"
                                select childEntry.Name;

                foreach (var name in userNames)
                    names.Add(name);
            }

        return names;
        }


        [OperationContract]
        public string GetCurrentUser()
        {
            string names = "";
             names = Environment.UserName;

            return names;
        }
        #endregion



        #region Update Services

        [OperationContract]
        public string Update(string connString, string databaseName, DataSetData d, string tableName, out CustomException ServiceError)
        {
            string result = "";
            try
            {
                DataSet ds = DataSetData.ToDataSet(d);
               result = UpdataDataSet(connString, databaseName,ds, tableName);
                ServiceError = null;
                return result;
            }
            catch (Exception ex)
            {
                ServiceError = new CustomException(ex);
            }
            return result;
        }
       

        private string UpdataDataSet(string connString, string databaseName, DataSet ds, string tableName)
        {
            string result ="";
            int count = 0;
            try
            {


               
                string columnNamesAndValue = "";
               
                DataTable myDataTable = ds.Tables[0];



                SqlConnection sqlConn = default(SqlConnection);
                sqlConn = new SqlConnection(connString);
                System.Data.SqlClient.SqlCommand sqlCmd = null;


                List<Field> keyFieldsDataTable = GetAllTableKeys(tableName);
               // DataTable fieldsDataTable = ExecuteDataTable(connString, tableName, databaseName);

                var fieldList = TableFields(tableName);

               

                // get primary key
                string primaryKey = "";
                string primarykeyDataType = "";
                Field field = PrimaryKeyField(tableName);
                if (field != null)
                {
                    primaryKey = field.FieldName;
                    primarykeyDataType = field.Type;
                }
                
                DataRow custRow = null;

                sqlCmd = sqlConn.CreateCommand();

                foreach (DataRow custRow_loopVariable in myDataTable.Rows)
                {

                    columnNamesAndValue = string.Empty;
                   
                    custRow = custRow_loopVariable; 
                    
                    foreach (DataColumn column in myDataTable.Columns)
                    {
                        // if primary is column is null set unique column to update
                        string actualname = CharacterHandler.ReplaceHexCharacter(column.ColumnName);

                        if (!((column.ReadOnly) || (column.ColumnName == "RowState") || (column.ColumnName == "State") || (column.AutoIncrement)))
                        {
                          
                            if ((column.ColumnName  == primaryKey))
                            {
                                //if (primarykeyDataType.Contains("varchar"))
                                //{
                                //    if (!(primarykeyDataType == "uniqueidentifier"))
                                //    { }
                                //}
                            }
                            else
                            {

                                string newValue = custRow[column.ColumnName].ToString();
                                try
                                {
                                    var data = fieldList.Where(f => f.FieldName == actualname).FirstOrDefault();
                                    if (data.Type == "date")
                                    {
                                        DateTime newDate = Convert.ToDateTime(newValue.ToString());

                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }
                                        string strDate = newDate.ToString("dd-MMM-yyyy");
                                        newValue = strDate;
                                    }
                                    if (data.Type.Contains("time("))
                                    {
                                        newValue = newValue.Substring(0, 7);


                                    }


                                    if (data.Type == "datetime")
                                    {
                                        DateTime newDate = Convert.ToDateTime(newValue.ToString());
                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }
                                        string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                        newValue = strDate;
                                    }
                                }
                                catch
                                {
                                }



                                columnNamesAndValue += "[" + actualname + "]" + "=  '" + newValue + "',";

                            }
                            
                        }

                    }
                   
                    string keys = "";

                    foreach (Field fd in keyFieldsDataTable)
                    {
                        string colName = fd.FieldName;
                        string actualnam = CharacterHandler.ReplaceSpecialCharacter(colName);//GetColumnName(fieldsDataTable, column.ColumnName);


                        keys += " [" + colName + "]" + "  = '" + custRow[actualnam] + "' and";

                    }
                    if (keys != "")
                    {
                        keys = " Where " + keys; 

                        keys = keys.TrimEnd('d');
                        keys = keys.TrimEnd('n');
                        keys = keys.TrimEnd('a');

                        columnNamesAndValue = columnNamesAndValue.TrimEnd(',');
                        
                        string sql = "Use [" + databaseName + "]; Update [" + tableName + "] set " + columnNamesAndValue + keys;
                        sqlCmd = new SqlCommand(sql, sqlConn);
                        sqlCmd.CommandText = sql;
                        sqlConn.Open();
                        sqlCmd.ExecuteNonQuery();
                       
                        sqlConn.Close();
                        count += 1;
                    }
                }
                result = count + " Rows have been updated";
            }
            catch (Exception e)
            {
                result = "An error occured while updating ";
                throw new Exception("Update DataSata Failed", e);
            }

            return result;
        }




        private int UpdateDataTable(string connString, string databaseName, string soredProcName, DataSet d, List<string> parameters)
        {
            int result = 0;
            int count = 0;
            try
            {



                SqlConnection connection = default(SqlConnection);
                connection = new SqlConnection(connString);
                connection.Open();
                using (connection)
                {
                    string viewID = "0";
                    foreach (var item in parameters)
                    {
                        char[] sep = { ';' };
                        string toList = item;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        string parameterName = toArray[0].ToString();
                        string parameterValue = toArray[1].ToString();

                        if (parameterName != null)
                        {
                            if (parameterName.Contains("View_ID"))
                            {
                                viewID = parameterValue;


                            }

                        }

                    }
                    var fieldList = TableFieldByViewID(Convert.ToInt64(viewID));

                    DataTable myDataTable = d.Tables[0];

                    DataTable dt = new DataTable("TSAdjustmentInsertTable");
                    SqlMetaData[] tbltype =
    {
      new SqlMetaData("FieldName", SqlDbType.VarChar, 200),
      new SqlMetaData("ActualFieldName", SqlDbType.VarChar, 200),
      new SqlMetaData("Value",  SqlDbType.VarChar, 200)
    };
                    List<SqlDataRecord> dataRecordList = new List<SqlDataRecord>();




                    foreach (DataRow row in myDataTable.Rows)
                    {
                        foreach (DataColumn column in myDataTable.Columns)
                        {
                            if ((column.ColumnName == "State") || (column.ColumnName == "RowState") || (column.ColumnName == "ID1") || (column.ColumnName == "ID2") || (column.ColumnName == "ID3") || (column.ColumnName == "ID4"))
                            {
                            }
                            else
                            {
                                SqlDataRecord datTable = new SqlDataRecord(tbltype);

                                string columnName = column.ColumnName;
                                string columnData = row[column].ToString();
                                var data = fieldList.Where(f => f.FieldName == columnName).FirstOrDefault();
                                try
                                {
                                  //  var data = fieldList.Where(f => f.FieldName == columnName).FirstOrDefault();

                                    if (data.FieldType == "datetime")
                                    {
                                        DateTime newDate = new DateTime();
                                        try
                                        {
                                            double d2 = double.Parse(columnData.ToString());

                                            DateTime dcDate = Convert.ToDateTime("30-Dec-1899");
                                            newDate = dcDate.AddDays(d2);


                                        }
                                        catch
                                        {
                                            newDate = Convert.ToDateTime(columnData.ToString());
                                        }
                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }


                                        string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                        //  string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                        columnData = strDate;
                                    }

                                    if (data.FieldType == "date")
                                    {
                                        DateTime newDate = new DateTime();
                                        try
                                        {
                                            double d2 = double.Parse(columnData.ToString());

                                            newDate = DateTime.FromOADate(d2);

                                            //    DateTime newDate = Convert.ToDateTime(columnData.ToString());
                                            //                                          string strDate = newDate.ToString("dd-MMM-yyyy");
                                        }
                                        catch
                                        {
                                            newDate = Convert.ToDateTime(columnData.ToString());
                                        }
                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }
                                        string strDate = newDate.ToString("dd-MMM-yyyy");
                                        columnData = strDate;
                                    }

                                    if (data.FieldType.Contains("time("))
                                    {
                                        columnData = columnData.Substring(0, 7);


                                    }
                                }
                                catch
                                {
                                }
                                if (data != null)
                                {
                                    if(data.ForUpdate)
                                    {
                                    datTable.SetString(0, columnName);
                                    datTable.SetString(1, columnName);
                                    datTable.SetString(2, columnData);
                                    dataRecordList.Add(datTable);
                                    }
                                }
                            }
                        }
                    }







                    SqlCommand sqlCommand = new SqlCommand(soredProcName
                     , connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;


                    foreach (var item in parameters)
                    {
                        char[] sep = { ';' };
                        string toList = item;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        string parameterName = toArray[0].ToString();
                        string parameterValue = toArray[1].ToString();

                        if (parameterName != null)
                        {
                            if (parameterValue.Contains("TableType"))
                            {
                                string str = "@" + parameterName;
                                sqlCommand.Parameters.Add(str, SqlDbType.Structured);
                                sqlCommand.Parameters[str].Direction = ParameterDirection.Input;
                                sqlCommand.Parameters[str].TypeName = "dbo." + parameterValue;
                                sqlCommand.Parameters[str].Value = dataRecordList;

                            }
                            else
                            {
                                sqlCommand.Parameters.Add(new SqlParameter("@" + parameterName, parameterValue));
                            }
                        }

                    }





                    // Execute the command.
                 var obj =   sqlCommand.ExecuteScalar();
                    connection.Close();

                 result = Convert.ToInt32(obj.ToString());
                    
                   // result = myDataTable.Rows.Count; ;
                    //   result = "Successfully Added";
                }




            }

            catch
            {
                result = 0;

            }
            return result;
        }



        [OperationContract]
        public int UpdateDataTable(string connString, string databaseName, string soredProcName, DataSetData d, List<string> parameters, out CustomException ServiceError)
        {
            int result = 0;
            DataSet ds = DataSetData.ToDataSet(d);
            try
            {

                result = UpdateDataTable(connString, databaseName, soredProcName, ds, parameters);
                ServiceError = null;
                return result;
            }
            catch (Exception ex)
            {
                //InsertSingleRow(connString, databaseName, ds, tableName);
                //return true;
                ServiceError = new CustomException(ex);
            }
            return result;
        }

        #endregion



        #region Insert Services

        //[OperationContract]
        //public bool InsertAll(string connString, string databaseName, DataSetData d, string tableName, out CustomException ServiceError)
        //{
        //    if()
        //    {
        //    }
        //    else{
        //    }

        //}

        [OperationContract]
        public string Insert(string connString, string databaseName, DataSetData d, string tableName, out CustomException ServiceError)
        {
            string result = "";
            DataSet ds = DataSetData.ToDataSet(d);
            try
            {

                result = InsertSingleRow(connString, databaseName, ds, tableName);
                ServiceError = null;
                return result;
            }
            catch (Exception ex)
            {
                //InsertSingleRow(connString, databaseName, ds, tableName);
                //return true;
               ServiceError = new CustomException(ex);
            }
            return result;
        }



        private string InsertSingleRow(string connString, string databaseName, DataSet ds, string tableName)
        {

            string result = "";
            int count = 0;
            try
            {


                string columnNames = "";
                string dataValues = "";
                DataTable myDataTable = ds.Tables[0];



                SqlConnection nwind = default(SqlConnection);
                nwind = new SqlConnection(connString);
                System.Data.SqlClient.SqlCommand sqlCmd = null;

                string primaryKey = "";
                string primarykeyDataType = "";
                Field field = PrimaryKeyField(tableName);
                if (field != null)
                {
                    primaryKey = field.FieldName;
                    primarykeyDataType = field.Type;
                }

                DataRow custRow = null;

                sqlCmd = nwind.CreateCommand();
               // DataTable fieldsDataTable = ExecuteDataTable(connString, tableName, databaseName);


                var fieldList = TableFields(tableName);
               

                foreach (DataRow custRow_loopVariable in myDataTable.Rows)
                {
                    columnNames = string.Empty;
                    dataValues = string.Empty;
                    custRow = custRow_loopVariable;
                    foreach (DataColumn column in myDataTable.Columns)
                    {

                        if (!((column.ReadOnly) || (column.ColumnName == "RowState") || (column.ColumnName == "State") || (column.AutoIncrement)))
                        {
                            string actualname = CharacterHandler.ReplaceHexCharacter(column.ColumnName);//GetColumnName(fieldsDataTable, column.ColumnName);

                            if ((column.ColumnName == primaryKey))
                            {
                                if (primarykeyDataType.Contains("varchar"))
                                {
                                    if (!(primarykeyDataType == "uniqueidentifier"))
                                    {
                                        columnNames += "[" + actualname + "]" + ",";

                                        dataValues += "'" + custRow[column.ColumnName] + "',";
                                    }
                                }

                            }
                            else
                            {
                                string newValue = custRow[column.ColumnName].ToString();
                                try
                                {
                                    var data = fieldList.Where(f => f.FieldName == actualname).FirstOrDefault();
                                    if (data.Type == "date")
                                    {
                                        DateTime newDate = Convert.ToDateTime(newValue.ToString());

                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }
                                        string strDate = newDate.ToString("dd-MMM-yyyy");
                                        newValue = strDate;
                                    }
                                    if (data.Type.Contains("time("))
                                    {
                                     newValue=   newValue.Substring(0,7);
                                     
                                        
                                    }

                                   
                                    if (data.Type == "datetime")
                                    {
                                        DateTime newDate = Convert.ToDateTime(newValue.ToString());

                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }
                                        string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                        newValue = strDate;
                                    }
                                }
                                catch
                                {
                                }

                                columnNames += "[" + actualname + "]" + ",";


                                dataValues += "'" + newValue + "',";
                            }

                        }

                    }


                    columnNames = columnNames.TrimEnd(',');
                    dataValues = dataValues.TrimEnd(',');
                    string sql = "use [" + databaseName + "]; INSERT INTO [" + tableName + "] (" + columnNames + ") VALUES (" + dataValues + ")";
                    sqlCmd = new SqlCommand(sql, nwind);
                    sqlCmd.CommandText = sql;
                    nwind.Open();
                    sqlCmd.ExecuteNonQuery();
                    nwind.Close();
                    count += 1;
                }

                result = count + " Rows have been added";
            }
            catch (Exception e)
            {
                result = "Adding new rows failed";
                throw new Exception("Adding new rows failed", e);

            }

            return result;

        }



        private void CreateTableType(string sql)
        {
            try
            {



                SqlConnection nwind = default(SqlConnection);
                nwind = new SqlConnection(connectionString);
                System.Data.SqlClient.SqlCommand custsInsCmd = null;


                custsInsCmd = new SqlCommand(sql, nwind);
                custsInsCmd.CommandText = sql;
                nwind.Open();
                custsInsCmd.ExecuteNonQuery();
                nwind.Close();
            }


            catch (Exception e)
            {
                throw new Exception("Failed", e);
            }
        }







        private string InsertDataSet(string connString, string databaseName, DataSet ds, string tableName)
        {
            string result = "";
            int count = 0;
            try
            {
                bool useBulk = false;

               
                SqlConnection connection = default(SqlConnection);
                connection = new SqlConnection(connString);
                connection.Open();
                using (connection)
                {
                    if (useBulk)
                    {
                    // Create a DataTable with the modified rows.
                    string columnNames = string.Empty;
                    string dataValues = string.Empty;
                    string typeValue = string.Empty;

                    string primaryKey = PrimaryKey(tableName);

                    DataTable myDataTable = ds.Tables[0];
                        
                    RemoveUselessFields(ref myDataTable);
                    DataTable fieldsDataTable = ExecuteDataTable(connString, tableName, databaseName);



                    foreach (DataRow dataRow in fieldsDataTable.Rows)
                    {
                        typeValue += dataRow["ColumnName"].ToString().Replace(" ", "_") + " " + dataRow["ColumnType"].ToString() + ",";

                    }

                    foreach (DataColumn column in myDataTable.Columns)
                    {

                        if (!((column.ReadOnly) || (column.ColumnName == "RowState") || (column.ColumnName == "State") || (column.AutoIncrement) || (column.ColumnName == primaryKey)))
                        {
                            string actualname = GetColumnName(fieldsDataTable, column.ColumnName);
                            if (actualname == "")
                            {
                                columnNames += "[" + column.ColumnName + "],";

                                dataValues += "nc." + column.ColumnName + ",";
                            }
                            else
                            {
                                columnNames += "[" + actualname + "],";

                                dataValues += "[nc." + column.ColumnName + "],";
                                useBulk = false;
                            }

                        }


                    }


                   


                        columnNames = columnNames.TrimEnd(',');

                        dataValues = dataValues.TrimEnd(',');
                        typeValue = typeValue.TrimEnd(',');
                        string TableTypeName = tableName.Replace("[", "").Replace("]", "") + "TableType";
                        // Define the INSERT-SELECT statement.
                        string checkSql = "USE [" + databaseName + "] IF EXISTS (SELECT * FROM sys.types WHERE name = '" + TableTypeName + "') DROP TYPE dbo." + TableTypeName + "; ";
                        string createSql = " CREATE TYPE dbo." + TableTypeName + " AS TABLE(" + typeValue + ") ;";
                        string sqlInsert =
                        " INSERT INTO [" + databaseName + "].dbo." + tableName + " (" + columnNames + ")"
                        + " SELECT " + dataValues
                        + " FROM @tvpNewData AS nc;";
                        string combinedSql = checkSql + createSql;


                        //CreateTableType
                        CreateTableType(combinedSql);
                        // Configure the command and parameter.


                        SqlCommand insertCommand = new SqlCommand(
                         sqlInsert, connection);
                        SqlParameter tvpParam = insertCommand.Parameters.AddWithValue(
                        "@tvpNewData", myDataTable);
                        tvpParam.SqlDbType = SqlDbType.Structured;
                        tvpParam.TypeName = "dbo." + TableTypeName;

                        // Execute the command.
                        insertCommand.ExecuteNonQuery();
                        connection.Close();

                        result = "Successfully Added";
                    }



                    else
                    {
                        InsertSingleRow(connString, databaseName, ds, tableName);
                    }
                }
            }
            catch
            {
                result = "An error occured while adding new rows";
                //InsertSingleRow(connString, databaseName, ds, tableName);
            }
            return result;
        }

        [OperationContract]
        public int InsertDataTable(string connString, string databaseName,string soredProcName, DataSetData d,  List<string> parameters, out CustomException ServiceError)
        {
            int result =0;
            DataSet ds = DataSetData.ToDataSet(d);
            try
            {

                result = InsertDataTable(connString, databaseName,soredProcName, ds, parameters);
                ServiceError = null;
                return result;
            }
            catch (Exception ex)
            {
                //InsertSingleRow(connString, databaseName, ds, tableName);
                //return true;
                ServiceError = new CustomException(ex);
            }
            return result;
        }


        private string GetColumnName(DataTable tbl, string column)
        {
            string columnName = "";
       
                foreach (DataRow dataRow in tbl.Rows)
                    {

                        if (dataRow["ColumnName"].ToString() == column.Replace("_", " "))
                        {
                            columnName = (dataRow["ColumnName"].ToString());
                        }

                    }
            return columnName;
        }


        private int InsertDataTable(string connString, string databaseName, string soredProcName, DataSet d, List<string> parameters)
        {
            int result = 0;
            int count = 0;
            try
            {



                SqlConnection connection = default(SqlConnection);
                connection = new SqlConnection(connString);
                connection.Open();
                
                using (connection)
                {

                    string viewID = "0";
                    foreach (var item in parameters)
                    {
                        char[] sep = { ';' };
                        string toList = item;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        string parameterName = toArray[0].ToString();
                        string parameterValue = toArray[1].ToString();

                        if (parameterName != null)
                        {
                            if (parameterName.Contains("View_ID"))
                            {
                                viewID = parameterValue;
                                

                            }
                           
                        }

                    }
                    var fieldList = TableFieldByViewID(Convert.ToInt64(viewID));
                   

                    DataTable myDataTable = d.Tables[0];

                    DataTable dt = new DataTable("TSAdjustmentInsertTable");
                                            SqlMetaData[] tbltype =
                            {
                              new SqlMetaData("FieldName", SqlDbType.VarChar, 200),
                              new SqlMetaData("ActualFieldName", SqlDbType.VarChar, 200),
                              new SqlMetaData("Value",  SqlDbType.VarChar, 200)
                            };
                    List<SqlDataRecord> dataRecordList = new List<SqlDataRecord>();




                    foreach (DataRow row in myDataTable.Rows)
                    {
                        foreach (DataColumn column in myDataTable.Columns)
                        {
                            if ((column.ColumnName == "State") || (column.ColumnName == "RowState") || (column.ColumnName == "ID1") || (column.ColumnName == "ID2") || (column.ColumnName == "ID3") || (column.ColumnName == "ID4"))
                            {
                            }
                            else
                            {
                                SqlDataRecord datTable = new SqlDataRecord(tbltype);

                                string columnName = column.ColumnName;
                                string columnData = row[column].ToString();
                                var data = fieldList.Where(f => f.FieldName == columnName).FirstOrDefault();
                                try
                                {
                                    
                                    
                                    if (data.FieldType == "datetime")
                                    {
                                        DateTime newDate = new DateTime();
                                        try
                                        {
                                            double d2 = double.Parse(columnData.ToString());

                                            DateTime dcDate = Convert.ToDateTime("30-Dec-1899");
                                            newDate = dcDate.AddDays(d2);


                                        }
                                        catch
                                        {
                                             newDate = Convert.ToDateTime(columnData.ToString());
                                        }
                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }


                                        string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                      //  string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                        columnData = strDate;
                                    }

                                    if (data.FieldType == "date")
                                    {
                                        DateTime newDate = new DateTime();
                                        try
                                        {
                                            double d2 = double.Parse(columnData.ToString());

                                            newDate = DateTime.FromOADate(d2);
                                           
                                            //    DateTime newDate = Convert.ToDateTime(columnData.ToString());
                                            //                                          string strDate = newDate.ToString("dd-MMM-yyyy");
                                        }
                                        catch
                                        {
                                            newDate = Convert.ToDateTime(columnData.ToString());
                                        }

                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = SqlDateTime.MinValue.Value;
                                        }
                                        string strDate = newDate.ToString("dd-MMM-yyyy");
                                        columnData = strDate;
                                    }

                                    if (data.FieldType.Contains("time("))
                                    {
                                        columnData = columnData.Substring(0, 7);


                                    }
                                }
                                catch
                                {
                                }
                                if (data != null)
                                {
                                    if (data.ForCreate)
                                    {
                                        datTable.SetString(0, columnName);
                                        datTable.SetString(1, columnName);
                                        datTable.SetString(2, columnData);
                                        dataRecordList.Add(datTable);
                                    }
                                }
                            }
                        }
                    }


              
               



                    SqlCommand sqlCommand = new SqlCommand(soredProcName
                     , connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandTimeout = 900;
                    foreach (var item in parameters)
                    {
                        char[] sep = { ';' };
                        string toList = item;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        string parameterName = toArray[0].ToString();
                        string parameterValue = toArray[1].ToString();

                        if (parameterName != null)
                        {
                            if (parameterValue.Contains("TableType"))
                            {
                                string str = "@" + parameterName;
                                sqlCommand.Parameters.Add(str, SqlDbType.Structured);
                                sqlCommand.Parameters[str].Direction = ParameterDirection.Input;
                                sqlCommand.Parameters[str].TypeName = "dbo." + parameterValue;
                                sqlCommand.Parameters[str].Value = dataRecordList;
                              
                            }
                            else
                            {
                                sqlCommand.Parameters.Add(new SqlParameter("@" + parameterName, parameterValue));
                            }
                        }

                    }

                   



                    // Execute the command.
                    var obj = sqlCommand.ExecuteScalar();
                    connection.Close();
                    result = Convert.ToInt32(obj.ToString());
                   
               //     result = myDataTable.Rows.Count; ;
                    //   result = "Successfully Added";
                }




            }

            catch
            {
                result = 0;

            }
            return result;
        }




        private string  InsertUpdateDataTable(string connString, string databaseName, string soredProcName, DataSet d, List<string> parameters)
        {
            string result = "";
            int count = 0;
            try
            {



                SqlConnection connection = default(SqlConnection);
                connection = new SqlConnection(connString);
                connection.Open();
                using (connection)
                {

                    string viewID = "0";
                    foreach (var item in parameters)
                    {
                        char[] sep = { ';' };
                        string toList = item;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        string parameterName = toArray[0].ToString();
                        string parameterValue = toArray[1].ToString();

                        if (parameterName != null)
                        {
                            if (parameterName.Contains("View_ID"))
                            {
                                viewID = parameterValue;


                            }

                        }

                    }
                    var fieldList = TableFieldByViewID(Convert.ToInt64(viewID));


                    DataTable myDataTable = d.Tables[0];

                    DataTable dt = new DataTable("TSAdjustmentInsertTable");
                    SqlMetaData[] tbltype =
                            {
                              new SqlMetaData("FieldName", SqlDbType.VarChar, 200),
                              new SqlMetaData("ActualFieldName", SqlDbType.VarChar, 200),
                              new SqlMetaData("Value",  SqlDbType.VarChar, 200)
                            };
                    List<SqlDataRecord> dataRecordList = new List<SqlDataRecord>();




                    foreach (DataRow row in myDataTable.Rows)
                    {
                        foreach (DataColumn column in myDataTable.Columns)
                        {
                            if ((column.ColumnName == "State") || (column.ColumnName == "RowState") || (column.ColumnName == "ID1") || (column.ColumnName == "ID2") || (column.ColumnName == "ID3") || (column.ColumnName == "ID4"))
                            {
                            }
                            else
                            {
                                SqlDataRecord datTable = new SqlDataRecord(tbltype);

                                string columnName = column.ColumnName;
                                string columnData = row[column].ToString();
                                var data = fieldList.Where(f => f.FieldName == columnName).FirstOrDefault();
                                try
                                {


                                    if (data.FieldType == "datetime")
                                    {
                                        DateTime newDate = new DateTime();
                                        try
                                        {
                                            double d2 = double.Parse(columnData.ToString());

                                            DateTime dcDate = Convert.ToDateTime("30-Dec-1899");
                                            newDate = dcDate.AddDays(d2);
                                        }
                                        catch
                                        {
                                            newDate = Convert.ToDateTime(columnData.ToString());
                                        }

                                         DateTime compareDate = SqlDateTime.MinValue.Value;
                                         if (newDate < compareDate)
                                         {
                                             newDate = compareDate;
                                         }

                                        string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                        //  string strDate = newDate.ToString("dd-MMM-yyyy HH:mm:ss tt");
                                        columnData = strDate;
                                    }

                                    if (data.FieldType == "date")
                                    {
                                        DateTime newDate = new DateTime();
                                        try
                                        {
                                            double d2 = double.Parse(columnData.ToString());

                                            DateTime dcDate = Convert.ToDateTime("30-Dec-1899");
                                            newDate = dcDate.AddDays(d2);
                                        }
                                        catch
                                        {
                                            newDate = Convert.ToDateTime(columnData.ToString());
                                        }

                                        DateTime compareDate = SqlDateTime.MinValue.Value;
                                        if (newDate < compareDate)
                                        {
                                            newDate = compareDate;
                                        }

                                        string strDate = newDate.ToString("dd-MMM-yyyy");
                                        //    DateTime newDate = Convert.ToDateTime(columnData.ToString());
                                        //                                          string strDate = newDate.ToString("dd-MMM-yyyy");
                                        columnData = strDate;
                                    }

                                    if (data.FieldType.Contains("time("))
                                    {
                                        columnData = columnData.Substring(0, 7);


                                    }
                                }
                                catch
                                {
                                }
                                if (data != null)
                                {
                                    if (data.ForCreate)
                                    {
                                        datTable.SetString(0, columnName);
                                        datTable.SetString(1, columnName);
                                        datTable.SetString(2, columnData);
                                        dataRecordList.Add(datTable);
                                    }
                                }
                            }
                        }
                    }







                    SqlCommand sqlCommand = new SqlCommand(soredProcName
                     , connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;


                    foreach (var item in parameters)
                    {
                        char[] sep = { ';' };
                        string toList = item;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        string parameterName = toArray[0].ToString();
                        string parameterValue = toArray[1].ToString();

                        if (parameterName != null)
                        {
                            if (parameterValue.Contains("TableType"))
                            {
                                string str = "@" + parameterName;
                                sqlCommand.Parameters.Add(str, SqlDbType.Structured);
                                sqlCommand.Parameters[str].Direction = ParameterDirection.Input;
                                sqlCommand.Parameters[str].TypeName = "dbo." + parameterValue;
                                sqlCommand.Parameters[str].Value = dataRecordList;

                            }
                            else
                            {
                                sqlCommand.Parameters.Add(new SqlParameter("@" + parameterName, parameterValue));
                            }
                        }

                    }





                    // Execute the command.
                     SqlDataReader dr  = sqlCommand.ExecuteReader();

                    while (dr.Read())
                    {
                        string update = dr[0].ToString() ;
                        string insert = dr[1].ToString();
                        string error = dr[2].ToString();

                        result += insert + " rows added, " + update + " rows updated, " + error + " errors";
                    }

                    // It will be better if you close DataReader
                    dr.Close();
                    connection.Close();
                    

                    //     result = myDataTable.Rows.Count; ;
                    //   result = "Successfully Added";
                }




            }

            catch
            {
                result = "An error occured while importing";

            }
            return result;
        }


        #endregion


        #region Delete Services
        [OperationContract]
        public int DeleteDataTable(string connString, string databaseName, string soredProcName, DataSetData d, List<string> parameters, out CustomException ServiceError)
        {
            int result = 0;
            DataSet ds = DataSetData.ToDataSet(d);
            try
            {

                result = DeleteDataTable(connString, databaseName, soredProcName, ds, parameters);
                ServiceError = null;
                return result;
            }
            catch (Exception ex)
            {
                //InsertSingleRow(connString, databaseName, ds, tableName);
                //return true;
                ServiceError = new CustomException(ex);
            }
            return result;
        }


       


        private int DeleteDataTable(string connString, string databaseName, string soredProcName, DataSet d, List<string> parameters)
        {
            int result = 0;
            int count = 0;
            try
            {



                SqlConnection connection = default(SqlConnection);
                connection = new SqlConnection(connString);
                connection.Open();
                using (connection)
                {


                    DataTable myDataTable = d.Tables[0];

                    DataTable dt = new DataTable("TSAdjustmentInsertTable");
                    SqlMetaData[] tbltype =
    {
      new SqlMetaData("ID", SqlDbType.BigInt),
     
    };
                    List<SqlDataRecord> dataRecordList = new List<SqlDataRecord>();




                    foreach (DataRow row in myDataTable.Rows)
                    {
                        foreach (DataColumn column in myDataTable.Columns)
                        {
                            if ((column.ColumnName == "ID"))
                            {
                                SqlDataRecord datTable = new SqlDataRecord(tbltype);

                              string columnName = column.ColumnName;
                                string columnData = row[column].ToString();
                                long ID = Convert.ToInt64(columnData);
                                datTable.SetInt64(0, ID);

                                dataRecordList.Add(datTable);
                            }
                          
                        }
                    }







                    SqlCommand sqlCommand = new SqlCommand(soredProcName
                     , connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;


                    foreach (var item in parameters)
                    {
                        char[] sep = { ';' };
                        string toList = item;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        string parameterName = toArray[0].ToString();
                        string parameterValue = toArray[1].ToString();

                        if (parameterName != null)
                        {
                            if (parameterValue.Contains("TableType"))
                            {
                                string str = "@" + parameterName;
                                sqlCommand.Parameters.Add(str, SqlDbType.Structured);
                                sqlCommand.Parameters[str].Direction = ParameterDirection.Input;
                                sqlCommand.Parameters[str].TypeName = "dbo." + parameterValue;
                                sqlCommand.Parameters[str].Value = dataRecordList;

                            }
                            else
                            {
                                sqlCommand.Parameters.Add(new SqlParameter("@" + parameterName, parameterValue));
                            }
                        }

                    }





                    // Execute the command.
                    var obj = sqlCommand.ExecuteScalar();
                    connection.Close();
                    result = Convert.ToInt32(obj.ToString());
                    
                    //result = myDataTable.Rows.Count; ;
                    //   result = "Successfully Added";
                }




            }

            catch
            {
                result = 0;

            }
            return result;
        }


        [OperationContract]
        public string Delete(string connString, string databaseName, DataSetData d, string tableName, out CustomException ServiceError)
        {
            string result = "";

            try
            {
                DataSet ds = DataSetData.ToDataSet(d);
               result= DeleteSingleRows(connString, databaseName, ds, tableName);
                ServiceError = null;
                return result;
            }
            catch (Exception ex)
            {
                ServiceError = new CustomException(ex);
            }
            return result;
        }

        private string DeleteSingleRows(string connString, string databaseName, DataSet ds, string tableName)
        {
            string result = "";
            int count = 0;
            try
            {


                string columnNames = "";
                string dataValues = "";
                DataTable myDataTable = ds.Tables[0];


                             
                                SqlConnection conn = default(SqlConnection);
                                conn = new SqlConnection(connString);
                                System.Data.SqlClient.SqlCommand sqlCmd = null;
                              
                                
                                 DataRow custRow = null;

                                 sqlCmd = conn.CreateCommand();

                                 string primaryKey = PrimaryKey(tableName);
                                // DataColumn[] columns;
                                //columns = myDataTable.PrimaryKey;
                           
                                // for (int i = 0; i < columns.Length; i++)
                                // {
                                //     primaryKey = columns[i].ColumnName ;
                                // }

                                // string s = "primaryKeyValue";
                                // DataRow foundRow = myDataTable.Rows.Find(s);

                                // if (foundRow != null)
                                // {
                                //     string yes = foundRow[1].ToString();
                                //     //MessageBox.Show(foundRow[1].ToString());
                                // }
                                 List<Field> keyFieldsDataTable = GetAllTableKeys(tableName);
                                 foreach (DataRow custRow_loopVariable in myDataTable.Rows)
                                 {
                                     custRow = custRow_loopVariable;

                                     string keys = "";

                                     foreach (Field field in keyFieldsDataTable)
                                     {
                                         string colName = field.FieldName;
                                         string actualnam = CharacterHandler.ReplaceSpecialCharacter(colName);//GetColumnName(fieldsDataTable, column.ColumnName);


                                         keys += " [" + colName + "]" + "  = '" + custRow[actualnam] + "' and";

                                     }
                                     if (keys != "")
                                     {
                                         keys = " Where " + keys; ;

                                         keys = keys.TrimEnd('d');
                                         keys = keys.TrimEnd('n');
                                         keys = keys.TrimEnd('a');

                                         columnNames = string.Empty;
                                         dataValues = string.Empty;
                                      


                                         columnNames = columnNames.TrimEnd(',');
                                         dataValues = dataValues.TrimEnd(',');
                                         string sql = "USE [" + databaseName + "]; Delete [" + tableName + "]  " + keys;
                                         sqlCmd = new SqlCommand(sql, conn);
                                         sqlCmd.CommandText = sql;
                                         conn.Open();
                                         sqlCmd.ExecuteNonQuery();
                                         conn.Close();
                                         count += 1;
                                     }
                                 }
                                 result = count + " Rows have been updated";
            }
            catch (Exception e)
            {
                result = "An error occured while deleting ";
                throw new Exception("Update DataSata Failed", e);
            }

            return result;
        }

        #endregion




        #region Helpers

        private void RemoveUselessFields(ref DataTable dtResults)
        {
           

            for (int i = dtResults.Columns.Count - 1; i >= 0; i--)
            {
                DataColumn column = dtResults.Columns[i];
                if ((column.ColumnName == "State") || (column.ColumnName == "RowState")) 
                {
                    dtResults.Columns.Remove(column);
                }
            }
            dtResults.AcceptChanges();

        }

        private string FieldInfoSql(string tableName)
        {
            string sql = "";
                sql +="  SELECT ";
                   sql +="              t.name TableName,";
                    sql +="               c.name ColumnName,";
                    sql +="               ColumnType = Case typ.name";
                     sql +="                                      when 'varchar' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length) end + ')'";
                     sql +="          when 'char' then typ.name + '(' + Convert(varchar(20),c.max_length)  + ')'";
                      sql +="         when 'nvarchar' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length/2) end + ')'";
                       sql +="        when 'nchar' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length/2) end + ')'";
                       sql +="        when 'varbinary' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length) end + ')'";
                       sql +="        when 'binary' then typ.name + '(' + convert(varchar(10),c.max_length) + ')'";
                        sql +="       when 'decimal' then typ.name + '(' +  convert(varchar(10),c.precision) + ',' +  convert(varchar(10),c.scale) + ')'";
                        sql +="       when 'numeric' then typ.name + '(' +  convert(varchar(10),c.precision) + ',' +  convert(varchar(10),c.scale) + ')'";
                         sql +="      when 'datetime2' then typ.name + '(' +  convert(varchar(10),c.scale) + ')'";
                         sql +="     when 'datetimeoffset' then typ.name + '(' +  convert(varchar(10),c.scale) + ')'";
                         sql +="      when 'time' then typ.name + '(' +  convert(varchar(10),c.scale) + ')'";
                            sql +="   else convert(varchar(50),typ.name)";
                            sql +="   end ,";
                           sql +="        inx.index_column_id IndexColumn,";
                           sql +="        [Readonly] = Case when c.is_identity = 1 or     c.is_computed = 1 then 1 else 0 end ";
                            sql +="     FROM sys.tables t";
                           sql +="      inner join sys.columns c on c.object_id = t.object_id";
                           sql +="      inner join sys.types typ on typ.user_type_id = c.user_type_id";
                            sql +="     Left outer join ";
                            sql +="             (Select i.object_id, ic.column_id , ic.index_column_id";
                             sql +="                  from sys.indexes i";
                             sql +="                  inner join sys.index_columns ic on i.index_id = ic.index_id and i.object_id = ic.object_id";
                              sql +="                 where i.is_primary_key = 1 ) as inx";
                              sql +="     on inx.object_id = t.object_id and inx.column_id = c.column_id";
                             sql +="    WHERE t.name = '" + tableName.Replace("[","").Replace("]","") + "'";


            return sql;
        }



     

     public  DataTable ExecuteDataTable(string conn, string tableName, string databaseName
                                     )  
        {
            string sql = "use [" + databaseName+ "]; "+ FieldInfoSql(tableName);
            DataTable dt = new DataTable();

            // Open the connection 
            using (SqlConnection cnn = new SqlConnection(conn
                   )) 
            { 
                //cnn.Open();
               // DataSet ds;
                // Define the command 

                System.Data.SqlClient.SqlConnection sqlconnection = new System.Data.SqlClient.SqlConnection(conn);

        
                    System.Data.SqlClient.SqlCommand sqlcommand = new System.Data.SqlClient.SqlCommand();
                    sqlcommand.Connection = sqlconnection;
                    sqlcommand.CommandText = sql;
                    sqlcommand.CommandType = CommandType.Text;
                   

                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                    DataSet ds = new DataSet();
                    da.SelectCommand = sqlcommand;   
     

                    sqlconnection.Open();
                    sqlcommand.ExecuteNonQuery();
                    da.Fill(ds);

                if (ds.Tables.Count > 0)
                {
                  dt= ds.Tables[0];
                   
                }
              
                

              
                } 
            
            return dt;
        }


      [OperationContract]
     public int TotalRows(string conn, string sql, string databaseName )
     {
         string sqlText = "use [" + databaseName + "]; select COUNT(*) as Total from (" + sql + ") x ";
        
         // Open the connection 
         using (SqlConnection cnn = new SqlConnection(conn
                ))
         {
             //cnn.Open();
             // DataSet ds;
             // Define the command 

             System.Data.SqlClient.SqlConnection sqlconnection = new System.Data.SqlClient.SqlConnection(conn);


             System.Data.SqlClient.SqlCommand sqlcommand = new System.Data.SqlClient.SqlCommand();
             sqlcommand.Connection = sqlconnection;
             sqlcommand.CommandText = sqlText;
             sqlcommand.CommandType = CommandType.Text;
             sqlconnection.Open();
             Int32 count = (Int32)sqlcommand.ExecuteScalar();
             sqlconnection.Close();
             return count;

         }

        
     }

      [OperationContract]
      public int TotalProcRows(string conn, string procName, string databaseName, List<string> parameters)
      {
          
          //int count;
          // Open the connection 
          DataSet ds;
            SqlConnection Connection = new SqlConnection(conn);
            try
            {
                Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(procName);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Connection = Connection;
                
                foreach (var item in parameters)
                {
                    char[] sep = { ';' };
                    string toList = item;
                    string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    string parameterName = toArray[0].ToString();
                    string parameterValue = toArray[1].ToString();

                    if (parameterName != null)
                    {
                        adapter.SelectCommand.Parameters.Add(new SqlParameter("@" + parameterName, parameterValue));
                    }

                }

                ds = new DataSet();

                //adapter.Fill(ds, (PageNumber - 1) * PageSize, PageSize, "Data");
                int count = adapter.Fill(ds, 1, 10000000, "Data");
                return count;

            }
            catch
            {
                return 0;
            }


      }



      public List<Field> GetAllTableKeys(string tableName)
     {
         List<Field> fieldList = new List<Field>();
         MiddlewareEntities context = new MiddlewareEntities();

          fieldList = (from f in context.Fields 
                       join t in context.Tables on f.Table_ID equals t.ID 
                       where f.isKey > (byte)(0) &&  t.TableName ==tableName
                       select f).ToList();


         return fieldList;
     } 

     public string PrimaryKey(string tableName)
     {
         string primaryKey ="";

         try

         {
             MiddlewareEntities context = new MiddlewareEntities();

             primaryKey = (from f in context.Fields
                           join t in context.Tables on f.Table_ID equals t.ID
                           where f.isKey == (byte)(1) && t.TableName == tableName
                           select f).FirstOrDefault().FieldName;

         }
         catch
         {
         }
         
         return primaryKey;
     }


     public Field PrimaryKeyField(string tableName)
     {
         Field primaryKeyField = new Field();

         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             primaryKeyField = (from f in context.Fields
                           join t in context.Tables on f.Table_ID equals t.ID
                           where f.isKey == (byte)(1) && t.TableName == tableName
                           select f).FirstOrDefault();

         }
         catch
         {
         }

         return primaryKeyField;
     }


     public List<Field> TableFields(string tableName)
     {
         List<Field> tableFieldList = new List<Field>();

         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             tableFieldList = (from f in context.Fields
                               join t in context.Tables on f.Table_ID equals t.ID
                               where t.TableName == tableName
                               select f).ToList();

         }
         catch
         {
         }

         return tableFieldList;
     }

     public List<ReadField> TableFieldByViewID(long ViewID)
     {
         List<ReadField> tableFieldList = new List<ReadField>();

         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             tableFieldList = (from f in context.ReadFields
                              
                               where f.ViewID == ViewID
                               select f).ToList();

         }
         catch
         {
         }

         return tableFieldList;
     }
        #endregion




        #region Insert Fields


     private void InsertDataSet1(string connString, string databaseName, DataSet ds, string tableName)
     {

         SqlConnection connection = default(SqlConnection);
         connection = new SqlConnection(connString);
         connection.Open();
         using (connection)
         {
             // Create a DataTable with the modified rows.
             string columnNames = string.Empty;
             string dataValues = string.Empty;
             string typeValue = string.Empty;

             string primaryKey = PrimaryKey(tableName);

             DataTable myDataTable = ds.Tables[0];
             RemoveUselessFields(ref myDataTable);
             foreach (DataColumn column in myDataTable.Columns)
             {

                 if (!((column.ReadOnly) || (column.ColumnName == "RowState") || (column.ColumnName == "State") || (column.AutoIncrement) || (column.ColumnName == primaryKey)))
                 {
                     columnNames += column.ColumnName + ",";

                     dataValues += "nc." + column.ColumnName + ",";

                 }


             }



             DataTable fieldsDataTable = ExecuteDataTable(connString, tableName, databaseName);

             //0772673122 nyasha , 0713025793
             

             foreach (DataRow dataRow in fieldsDataTable.Rows)
             {
                 typeValue += dataRow["ColumnName"].ToString() + " " + dataRow["ColumnType"].ToString() + ",";

             }


             columnNames = columnNames.TrimEnd(',');

             dataValues = dataValues.TrimEnd(',');
             typeValue = typeValue.TrimEnd(',');
             string TableTypeName = tableName.Replace("[", "").Replace("]", "") + "TableType";
             // Define the INSERT-SELECT statement.
             string checkSql = "USE " + databaseName + " IF EXISTS (SELECT * FROM sys.types WHERE name = '" + TableTypeName + "') DROP TYPE dbo." + TableTypeName + "; ";
             string createSql = " CREATE TYPE dbo." + TableTypeName + " AS TABLE(" + typeValue + ") ;";
             string sqlInsert =
             " INSERT INTO " + databaseName + ".dbo." + tableName + " (" + columnNames + ")"
             + " SELECT " + dataValues
             + " FROM @tvpNewData AS nc;";
             string combinedSql = checkSql + createSql;


             //CreateTableType
             CreateTableType(combinedSql);
             // Configure the command and parameter.


             SqlCommand insertCommand = new SqlCommand(
              sqlInsert, connection);
             SqlParameter tvpParam = insertCommand.Parameters.AddWithValue(
             "@tvpNewData", myDataTable);
             tvpParam.SqlDbType = SqlDbType.Structured;
             tvpParam.TypeName = "dbo." + TableTypeName;

             // Execute the command.
             insertCommand.ExecuteNonQuery();
             connection.Close();
         }
     }



     [OperationContract]
     public bool updateField(int fieldID, string friendlyName, string format)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from f in context.Fields
                         where f.ID == fieldID
                         select f;
             foreach (var item in items)
             {
                 item.FriendlyName = friendlyName;
                 item.Formart = format;
             }
             context.SaveChanges();
            
             return true;
         }
         catch (Exception ex)
         {
             
         }
         return false;
     }




     [OperationContract]
     public bool updateUserConfigDefault(int userID, int displayDefinitionID, int newDefaultID, int gridNumber)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from f in context.UserGridConfigs
                         where f.UserInformation_ID == userID && f.DisplayDefinition_ID == displayDefinitionID && f.ID != newDefaultID && f.GridNumber ==gridNumber
                         select f;
             foreach (var item in items)
             {
                 item.DefaultConfiguration = 0;
                 
             }
             var itms = from f in context.UserGridConfigs
                         where  f.ID == newDefaultID
                         select f;
             foreach (var itm in itms)
             {
                 itm.DefaultConfiguration = 1;

             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }

     [OperationContract]
     public bool DeleteField(int fieldID)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from f in context.Fields
                         where f.ID == fieldID
                         select f;
             foreach (var item in items)
             {
                 context.Fields.DeleteObject(item);
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }



     [OperationContract]
     public bool DeleteViewTableRelationship(int viewTablerelationID)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from f in context.ViewTableRelationships
                         where f.ID == viewTablerelationID
                         select f;
             foreach (var item in items)
             {
                 context.ViewTableRelationships.DeleteObject(item);
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }



     [OperationContract]
     public bool DeleteViewField(int viewFieldID)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from f in context.ViewFields
                         where f.ID == viewFieldID
                         select f;
             foreach (var item in items)
             {
                 context.ViewFields.DeleteObject(item);
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }


     [OperationContract]
     public bool UpdateViewField(int viewFieldID, string displayName)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from f in context.ViewFields
                         where f.ID == viewFieldID
                         select f;
             foreach (var item in items)
             {
                 item.DisplayName = displayName;
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }



     [OperationContract]
     public bool UpdateDisplayDefinition(DisplayDefinition dispayDefinition)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from display in context.DisplayDefinitions
                         where display.ID == dispayDefinition.ID
                         select display;
             foreach (var item in items)
             {
                 item.FormType_ID = dispayDefinition.FormType_ID;
                 item.Description = dispayDefinition.Description;
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }

     [OperationContract]
     public bool UpdateGridDefinition(GridDefinition gridDefinition)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from grid in context.GridDefinitions
                         where grid.ID == gridDefinition.ID
                         select grid;
             foreach (var item in items)
             {
                 item.View_ID = gridDefinition.View_ID;
                 item.GridNumber =gridDefinition.GridNumber;
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }



     [OperationContract]
     public bool DeleteGridDefinition(GridDefinition gridDefinition)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();

             var items = from grid in context.GridDefinitions
                         where grid.ID == gridDefinition.ID
                         select grid;
             foreach (var item in items)
             {
                 context.GridDefinitions.DeleteObject(item);
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }




     [OperationContract]
     public bool UpdateUserMenu(List<int> userIDs, int menuID)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();
             
             var items = from f in context.UserInformations
                         where userIDs.Contains(f.ID)
                         select f;
             foreach (var item in items)
             {
                 item.MenuStructure_ID = menuID;
             }
             context.SaveChanges();

             return true;
         }
         catch (Exception ex)
         {

         }
         return false;
     }

   
    [OperationContract]
     public bool UpdateMenuItems(List<ListMenu> menuItems)
     {
         try
         {

             UpdateMenuItem(menuItems);
             
             return true;
         }
         catch (Exception ex)
         {
         }
         return false;
     }

    
    public void UpdateMenuItem(List<ListMenu> menuItems)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();


             foreach (ListMenu menu in menuItems)
             {
                 var items = from f in context.MenuItems
                             where f.ID == menu.MenuItemID
                             select f;
                 foreach (var item in items)
                 {
                     item.DisplayDefinition_ID = menu.DisplayDefinitionID;
                 }
             }
             context.SaveChanges();

            
         }
         catch (Exception ex)
         {

         }
         
     }

     [OperationContract]
     public void TestMenuItem(int mID, int dID)
     {
         try
         {
             MiddlewareEntities context = new MiddlewareEntities();


            
                 var items = from f in context.MenuItems
                             where f.ID == mID
                             select f;
                 foreach (var item in items)
                 {
                     item.DisplayDefinition_ID = dID;
                 }
           
             context.SaveChanges();


         }
         catch (Exception ex)
         {

         }

     }
        #endregion

     #region Send email

        [OperationContract]
     bool sendMail(string emailTo, string emailFrom, string msgSubject, string msgBody)
     {
         bool success = false;

         try
         {
             MailMessage msg = new MailMessage();

             msg.From = new MailAddress(emailFrom);

             // get all the addresses this message will be sent to. Addresses are to be seperated with
             // the ';' character.
             char[] sep = { ';' };
             string toList = emailTo;
             string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

             foreach (string s in toArray)
             {
                 if (s != null)
                 {
                     if (isEmail(s))
                     {
                         msg.To.Add(new MailAddress(s));
                     }
                 }
             }


             msg.Subject = msgSubject;
             msg.Body = msgBody;
             msg.IsBodyHtml = true;

             SmtpClient SmtpSender = new SmtpClient();
             SmtpSender.Host = "smtp.gmail.com";
             SmtpSender.Port = 587;
             SmtpSender.EnableSsl = true;
             SmtpSender.Credentials = new System.Net.NetworkCredential("pnthree@gmail.com", "intermerat33");
             SmtpSender.Send(msg);
             success = true;
         }
         catch
         {
             success = false;
         }

         return success;
     }



     private bool IsValidEmailAddress(string sEmail)
     {


         int nFirstAT = sEmail.IndexOf('@');
         int nLastAT = sEmail.LastIndexOf('@');

         if ((nFirstAT > 0) && (nLastAT == nFirstAT) &&
         (nFirstAT < (sEmail.Length - 1)))
         {
             // address is ok regarding the single @ sign
             return (Regex.IsMatch(sEmail, @"(\w+)@(\w+)\.(\w+)"));
         }
         else
         {


             return false;
         }
     }


     private bool isEmail(string inputEmail)
     {
         inputEmail = NulltoString(inputEmail);
         string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
               @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
               @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
         Regex re = new Regex(strRegex);
         if (re.IsMatch(inputEmail))
         {
             return (true);
         }
         else
         {

             return (false);
         }
     }

     private string NulltoString(string inputEmail)
     {
         throw new NotImplementedException();
     }
     #endregion


       [OperationContract]
       public void IssueAuthenticationToken()
        {
            FormsAuthentication.SetAuthCookie((HttpContext.Current.User.Identity.Name),true);
        }


       [OperationContract]
       public DataSetData GetExcelData(string filePath, string ext, string sheetName, bool isFirstRowAsColumnNames, List<FieldMapped> fieldMapList, int ViewID, int serverID, int CreateStoreProcID, bool toImport)
       {
         
          
               int pageNum = 1;
               int pageSize = 200;
               var tempFile = HttpContext.Current.Server.MapPath(filePath);
               FileStream stream = File.Open(tempFile, FileMode.Open, FileAccess.Read);
               DataSetData dataset = new DataSetData();
               //1. Reading from a binary Excel file ('97-2003 format; *.xls)

               IExcelDataReader excelReader = null;
               if (ext == "xls")
                   excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
               //...
               //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
               if (ext == "xlsx")
                   excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
               //...
               //3. DataSet - The result of each spreadsheet will be created in the result.Tables
               DataSet result = excelReader.AsDataSet();
               //...
               //4. DataSet - Create column names from first row
               DataSet ds = new DataSet("DataSet");
               DataTable dirtyDT = new DataTable();
               foreach (var item in result.Tables)
               {
                   DataTable dt = item as DataTable;
                   if (dt.TableName == sheetName)
                   {

                       dirtyDT = dt.Rows.Cast<System.Data.DataRow>().CopyToDataTable();
                       // ds.Tables.Add(dtPage);


                   }


                   // columnnameList.Add(dt.Rows);
               }

               //5. Data Reader methods
               while (excelReader.Read())
               {
                   //excelReader.GetInt32(0);
               }

               //6. Free resources (IExcelDataReader is IDisposable)
               excelReader.Close();
               if (serverID == 0)
               {
                   DataTable alldirtyDT = dirtyDT.Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(pageSize).CopyToDataTable();
                   ds.Tables.Add(alldirtyDT);
               }
               else
               {
                   DataTable validated = ValidateData(fieldMapList, ViewID, serverID, dirtyDT, toImport);

                   if (validated.Rows.Count > 0)
                   {

                       DataTable newdirtyDT = validated.Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(pageSize).CopyToDataTable();
                       ds.Tables.Add(newdirtyDT);
                   }
                   else
                   {
                       DataTable emptyDataTable = validated.Clone();
                       ds.Tables.Add(emptyDataTable);
                   }
               }
               
          
               return DataSetData.FromDataSet(ds);
           
         
       }

       [OperationContract]
       public DataSetData GetAccessData(string source, string db, string tableName, string authentication, List<FieldMapped> fieldMapList, int ViewID, int serverID, int CreateStoreProcID, bool toImport)
       {


           int pageNum = 1;
           int pageSize = 200;
           
           //...
           //4. DataSet - Create column names from first row
           DataSet ds = new DataSet("DataSet");
           DataTable dirtyDT = new DataTable();
           string strAccessConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Databases\" + db+ ";"+ authentication;
                   // ds.Tables.Add(dtPage);
                      OleDbConnection conn = new OleDbConnection(strAccessConnectionString);
                      try
                      {

                          string sql = "select * from " + tableName;


                          // retrieving schema for a single table

                          conn.Open();
                          OleDbCommand commonCommand = new OleDbCommand();
                          commonCommand.Connection = conn;
                          commonCommand.CommandType = CommandType.Text;
                          commonCommand.CommandText = sql;
                          OleDbDataAdapter commonAdapter = new OleDbDataAdapter();
                          commonAdapter.SelectCommand = commonCommand;
                          DataSet dtSet = new DataSet();
                          commonAdapter.Fill(dtSet, tableName);
                          dirtyDT = dtSet.Tables[0].Rows.Cast<System.Data.DataRow>().CopyToDataTable();
                         

                          conn.Close();
                      }
                      catch
                      {
                          conn.Close();
                      }

              
           //5. Data Reader methods
        
           if (serverID == 0)
           {
               DataTable alldirtyDT = dirtyDT.Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(pageSize).CopyToDataTable();
               ds.Tables.Add(alldirtyDT);
           }
           else
           {
               DataTable testdirtyDT = dirtyDT.Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(10).CopyToDataTable();

               DataTable validated = ValidateData(fieldMapList, ViewID, serverID, testdirtyDT, toImport);

               if (validated.Rows.Count > 0)
               {
                   DataTable newdirtyDT = validated.Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(pageSize).CopyToDataTable();
                   ds.Tables.Add(newdirtyDT);
               }
               else
               {
                   DataTable emptyDataTable = validated.Clone();
                   ds.Tables.Add(emptyDataTable);
               }
             
           }


           return DataSetData.FromDataSet(ds);


       }

       [OperationContract]
       public void RemoveTempFile(string filePath)
       {
           try
           {
               var tempFile = HttpContext.Current.Server.MapPath(filePath);
               if (File.Exists(tempFile))
                   File.Delete(tempFile);
           }
           catch
           {
           }
       }

       [OperationContract]
       public string InsertExcelData(string filePath, string ext, string sheetName, bool isFirstRowAsColumnNames, List<FieldMapped> fieldMapList, int ViewID, int serverID, int CreateStoreProcID,bool isInsertOnly, out CustomException ServiceError)
       {
           string intResult = "";

           try
           {
               int pageNum = 1;
               int pageSize = 200;
               var tempFile = HttpContext.Current.Server.MapPath(filePath);
               FileStream stream = File.Open(tempFile, FileMode.Open, FileAccess.Read);
               DataSetData dataset = new DataSetData();
               //1. Reading from a binary Excel file ('97-2003 format; *.xls)

               IExcelDataReader excelReader = null;
               if (ext == "xls")
                   excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
               //...
               //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
               if (ext == "xlsx")
                   excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
               //...
               //3. DataSet - The result of each spreadsheet will be created in the result.Tables
               DataSet result = excelReader.AsDataSet();
               //...
               //4. DataSet - Create column names from first row
               DataSet ds = new DataSet("DataSet");
               DataTable dirtyDT = new DataTable();
               foreach (var item in result.Tables)
               {
                   DataTable dt = item as DataTable;
                   if (dt.TableName == sheetName)
                   {

                       dirtyDT = dt.Rows.Cast<System.Data.DataRow>().CopyToDataTable();
                       // ds.Tables.Add(dtPage);


                   }


                   // columnnameList.Add(dt.Rows);
               }

               //5. Data Reader methods
               while (excelReader.Read())
               {
                   //excelReader.GetInt32(0);
               }

               //6. Free resources (IExcelDataReader is IDisposable)
               excelReader.Close();
               DataTable validated = ValidateData(fieldMapList, ViewID, serverID, dirtyDT, true);



              intResult= InsertImportData(fieldMapList, ViewID, serverID, CreateStoreProcID, validated, isInsertOnly);
             
               ServiceError = null;
           }
           catch (Exception ex)
           {
                  ServiceError = new CustomException(ex);
           }
         
           return intResult;

       }


       [OperationContract]
       public string InsertAccessData(string source, string db, string tableName, string authentication, List<FieldMapped> fieldMapList, int ViewID, int serverID, int CreateStoreProcID, bool isInsertOnly, out CustomException ServiceError)
       {
           string intResult = "";

           try
           {
               int pageNum = 1;
               int pageSize = 200;
               DataSet ds = new DataSet("DataSet");
               DataTable dirtyDT = new DataTable();
               string strAccessConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Databases\" + db +";"+ authentication;
               // ds.Tables.Add(dtPage);
               OleDbConnection conn = new OleDbConnection(strAccessConnectionString);
               try
               {

                   string sql = "select * from " + tableName;


                   // retrieving schema for a single table

                   conn.Open();
                   OleDbCommand commonCommand = new OleDbCommand();
                   commonCommand.Connection = conn;
                   commonCommand.CommandType = CommandType.Text;
                   commonCommand.CommandText = sql;
                   OleDbDataAdapter commonAdapter = new OleDbDataAdapter();
                   commonAdapter.SelectCommand = commonCommand;
                   DataSet dtSet = new DataSet();
                   commonAdapter.Fill(dtSet, tableName);
                   dirtyDT = dtSet.Tables[0].Rows.Cast<System.Data.DataRow>().CopyToDataTable();


                   conn.Close();
               }
               catch
               {
                   conn.Close();
               }

               DataTable validated = ValidateData(fieldMapList, ViewID, serverID, dirtyDT, true);



               intResult = InsertImportData(fieldMapList, ViewID, serverID, CreateStoreProcID, validated, isInsertOnly);

               ServiceError = null;
           }
           catch (Exception ex)
           {
               ServiceError = new CustomException(ex);
           }

           return intResult;

       }


       private string InsertImportData(List<FieldMapped> fieldMapList, int ViewID, int serverID,int CreateStoreProcID, DataTable dt, bool isInsertOnly)
       {
           string  result = "";
           try
           {
               DataSet ds = new DataSet("Dataset");
               foreach (var item in fieldMapList)
               {
                   if (item.SourceField == "ID")
                   {
                   }
                   else
                   {
                       dt.Columns[item.ActualColumnName].ColumnName = item.SourceField;
                   }
               }
               ds.Tables.Add(dt);


               MiddlewareEntities context = new MiddlewareEntities();
               var storedProc = (from s in context.StoredProcedureNames.Include("Server").Include("StoredProcedureParameters") where s.ID == CreateStoreProcID select s).FirstOrDefault();

               List<string> parameters = new List<string>();
               var storeParameters = storedProc.StoredProcedureParameters.ToList();

               foreach (var item in storeParameters)
               {

                   if (item.ParameterName.Contains("View_ID"))
                   {
                       parameters.Add(item.ParameterName + ";" + ViewID.ToString());
                   }
                   if (item.DataType.Contains("TableType"))
                   {
                       parameters.Add(item.ParameterName + ";" + item.DataType);
                   }

               }



               if (isInsertOnly)
               {
                   string insertOnlyresult = "";
                   int strResult = InsertDataTable(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, ds, parameters);
                   if (strResult == -2627)
                   {

                       insertOnlyresult = "Error : Duplicate key fields";

                   }
                   else if (strResult == -515)
                   {
                       insertOnlyresult = "Error : Cannot insert NULL value";

                   }
                   else if (strResult < 0)
                   {
                       insertOnlyresult = " An Sql error had occured, Error: " + strResult.ToString();

                   }
                   else
                   {
                       insertOnlyresult = strResult.ToString()+ " rows added";


                   }

                   result = insertOnlyresult;
               }
               else
               {

                   result = InsertUpdateDataTable(storedProc.Server.ConnectionString, storedProc.DBName, "SP_MergeData", ds, parameters);
               }

               
           }
           catch
           {
           }
           return result;
       }





       private DataTable ValidateData(List<FieldMapped> fieldMapList, int ViewID, int serverID, DataTable dt, bool toImport)
       {

          DataTable newDataTable =dt.Rows.Cast<System.Data.DataRow>().CopyToDataTable();

          DataTable validatedDataTable = dt.Clone();// Rows.Cast<System.Data.DataRow>().Skip((1 - 1) * 1).Take(1).CopyToDataTable();
          // validatedDataTable = dt.Clone();
          // validatedDataTable.Rows.Clear();
           string tableName = "";
           string connString = "";
           string databaseName = "";
           string middlewareConnString = "";
           string middlewareDatabaseName = "";
           string Sql = "";
           FieldMapped source = fieldMapList.Where(f => f.SourceField == "DefinitionType").FirstOrDefault();
           FieldMapped modelSource = fieldMapList.Where(f => f.SourceField == "Model_ID").FirstOrDefault();
           FieldMapped fieldSource = fieldMapList.Where(f => f.SourceField == "Field_ID").FirstOrDefault();
           FieldMapped defmap = fieldMapList.Where(f => f.SourceField == "Definition_ID").FirstOrDefault();

           List<int> ids = new List<int>();
           if (source != null)
           {
               ids.Add(source.FieldID);
           }
           if (modelSource != null)
           {
               ids.Add(modelSource.FieldID);
           }

           if (fieldSource != null)
           {
               ids.Add(fieldSource.FieldID);
           }
           if (defmap != null)
           {
               ids.Add(defmap.FieldID);
           }
           //
         //  FieldMapped timePeriodMap = fieldMapList.Where(f => f.SourceField == "TimePeriodDefinition_ID").FirstOrDefault();
          // FieldMapped profileMap = fieldMapList.Where(f => f.SourceField == "ProfileDefinition_ID").FirstOrDefault();
          // FieldMapped periodTypeMap = fieldMapList.Where(f => f.SourceField == "PeriodType_ID").FirstOrDefault();
           
         
           MiddlewareEntities context = new MiddlewareEntities();

          var server = (from s in context.Servers
                        join t in context.Tables on s.ID equals t.Server_ID
                        join v in context.ViewTables on t.ID equals v.Table_ID
                             where v.View_ID == ViewID
                             select s).FirstOrDefault();

          var table = (from  t in context.Tables 
                        join v in context.ViewTables on t.ID equals v.Table_ID
                        where v.View_ID == ViewID
                        select t).FirstOrDefault();
          if (table != null)
          {
              tableName = table.TableName;
          }

          var middlewareServer = (from s in context.Servers
                     
                        where s.ID== serverID
                        select s).FirstOrDefault();

          if (server != null)
          {
              connString = server.ConnectionString;
                  databaseName = server.DatabaseName;
          }

          if (middlewareServer != null)
          {
              middlewareConnString = middlewareServer.ConnectionString;
              middlewareDatabaseName = middlewareServer.DatabaseName;
          }

           var viewFields = (from v in context.ViewFields
                             where v.View_ID == ViewID select v).ToList();
           DataSet modelDT;
           string modelSql = "select ID , ModelName from Model ";
           modelDT = GetDataSet(connString, databaseName, modelSql, 1, 50);
           string curModel = "";
           string modelName = "";

           string curDef = "";
           string DefType = "";
           int i = 1;
           DataSet definitionDT = new DataSet();
           DataSet AdjFieldsDT = new DataSet(); 
           DataSet TimePeriodDT = new DataSet();
           DataSet ProfilesDT = new DataSet();
           DataSet PeriodTypeDT = new DataSet();
           DataSet virtualDataTable = new DataSet();

           var field = viewFields;
          

           foreach (DataRow row in newDataTable.Rows)
           {
               try
               {
                   modelName = row[modelSource.ActualColumnName].ToString(); 

                   if (source != null)
                   {

                       DefType = row[source.ActualColumnName].ToString();

                       if (curDef != DefType)
                       {

                           List<string> parameterdsAdj = new List<string>();

                           parameterdsAdj.Add("DefinitionType;" + DefType);
                           AdjFieldsDT = ExecuteStoreProcedure(middlewareConnString, middlewareDatabaseName, "SelectRelatedAdjField", parameterdsAdj, 1, 10000);
                       }
                       List<string> parameters = new List<string>();
                       parameters.Add("ModelName;" + modelName);
                       parameters.Add("DefinitionType;" + DefType);

                       if (curModel != modelName)
                       {

                           definitionDT = ExecuteStoreProcedure(connString, databaseName, "SelectDefinitionsForModelName", parameters, 1, 10000);

                         

                       }

                     


                       if (fieldSource != null)
                       {
                           string fieldname = row[fieldSource.ActualColumnName].ToString();
                           if (!isInt(fieldname))
                           {
                               DataRow[] fieldRow = AdjFieldsDT.Tables["Data"].Select("Display = '" + fieldname.Replace(" ", "") + "'");

                               DataRow frow = fieldRow[0];
                               row[fieldSource.ActualColumnName] = fieldRow[0]["Value"].ToString();
                           }
                       }

                       if (defmap != null)
                       {
                           string defname = row[defmap.ActualColumnName].ToString();
                           //   AdjFieldsDT.Tables[0].Rows["Value"]

                           if (!isInt(defname))
                           {
                               DataRow[] defRow = definitionDT.Tables[0].Select("Display = '" + defname + "'");
                               row[defmap.ActualColumnName] = defRow[0]["Value"].ToString();

                           }
                       }

                   }
                                  

                          
                 
                    if (!isInt(modelName))
                    {
                    DataRow[] modelRow = modelDT.Tables[0].Select("ModelName = '" + modelName + "'");
                    row[modelSource.ActualColumnName] = modelRow[0]["ID"].ToString();
                     }

                  

                   foreach (var item in viewFields.Where(f=>f.UseValueStoredProcedure ==true))
                   {
                       if (!ids.Contains(item.Feild_ID.Value))
                       {
                           if (item.UseValueStoredProcedure)
                           {

                               ///lookup if modelname changes
                               if (curModel != modelName)
                               {
                                   var storedProc = (from s in context.StoredProcedureNames.Include("Server").Include("StoredProcedureParameters")
                                                     where s.ID == item.StoredProcedureNames_ID
                                                     select s).FirstOrDefault();

                                   List<string> parameters = new List<string>();
                                   var storeParameters = storedProc.StoredProcedureParameters.ToList();

                                   foreach (var storePara in storeParameters)
                                   {

                                       if (storePara.ParameterName.Contains("View_ID"))
                                       {
                                           parameters.Add(storePara.ParameterName + ";" + ViewID.ToString());

                                       }
                                           if (storePara.ParameterName.Contains("Model_ID"))
                                           {
                                               DataRow[] modelRow1 = modelDT.Tables[0].Select("ModelName = '" + modelName + "'");
                                               string modelID1 = modelRow1[0]["ID"].ToString();
                                               parameters.Add(storePara.ParameterName + ";" + modelID1.ToString());
                                           }

                                           if (storePara.ParameterName.Contains("Type"))
                                           {
                                                  parameters.Add(storePara.ParameterName + ";" + tableName);
                                           }

                                           if (storePara.ParameterName.Contains("Filter"))
                                           {
                                               parameters.Add(storePara.ParameterName + ";" + item.FieldOptionFilter);
                                           }

                                       
                                   }


                                   DataSet newDataset = new DataSet();

                                   newDataset = ExecuteStoreProcedure(storedProc.Server.ConnectionString, storedProc.Server.DatabaseName, storedProc.SP_Function, parameters, 1, 10000);
                                   string newTableName = "Table" + item.Feild_ID.ToString();
                                   newDataset.Tables[0].TableName = newTableName;

                                   //check if datatable exists
                                   if (virtualDataTable.Tables.Contains(newDataset.Tables[0].TableName))
                                       virtualDataTable.Tables.Remove(virtualDataTable.Tables[newTableName]);
                                   virtualDataTable.Tables.Add(newDataset.Tables[newTableName].Copy());

                               }
                               //
                               /// chck 
                               FieldMapped newField = fieldMapList.Where(f => f.FieldID == item.Feild_ID).FirstOrDefault();

                               if (newField != null)
                               {
                                   string curName = row[newField.ActualColumnName].ToString();
                                   //   AdjFieldsDT.Tables[0].Rows["Value"]
                                   string curTableName = "Table" + item.Feild_ID.ToString();
                                   if (!isInt(curName))
                                   {

                                       DataRow[] defRow = virtualDataTable.Tables[curTableName].Select("Display = '" + curName + "'");

                                     //  if(defRow[0] !=)
                                       row[newField.ActualColumnName] = defRow[0]["Value"].ToString();

                                   }
                               }


                           }

                       }
                   }
               }
               catch
               {

                   try
                   {

                       validatedDataTable.ImportRow(row);
                   }
                   catch
                   {
                      
                   }

               }
               curModel = modelName;
               curDef = DefType;
           }

           if (!toImport)
           {

               return validatedDataTable;
           }
           else
           {
             //  if(

               newDataTable.Rows.Remove(newDataTable.Rows[0]);
               return newDataTable;
           }


       }



       private bool isInt(string fieldName)
       {
           bool isI = false;
           try
           {
               int Int = Convert.ToInt32(fieldName);
               isI = true;
           }
           catch
           {
           }
           return isI;
       }


       [OperationContract]
       public DataSetData ValidateDirtyData(DataSetData dirtyData, bool isFirstRowAsColumnNames, List<FieldMapped> fieldMapList, int ViewID, int serverID)
       {

           int pageNum = 1;
           int pageSize = 250;
          
           DataSetData dataset = new DataSetData();
           //1. Reading from a binary Excel file ('97-2003 format; *.xls)

           DataSet result = DataSetData.ToDataSet(dirtyData);
           //...
           //3. DataSet - The result of each spreadsheet will be created in the result.Tables
          
           //...
           //4. DataSet - Create column names from first row
           DataSet ds = new DataSet("DataSet");
           DataTable dirtyDT = new DataTable();

           dirtyDT = result.Tables[0].Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(pageSize).CopyToDataTable();
             
           DataTable validated = ValidateData(fieldMapList, ViewID, serverID, dirtyDT, false);
           validated.Columns.Remove("State");
           validated.Columns.Remove("RowState");
           ds.Tables.Add(validated);
           return DataSetData.FromDataSet(ds);
       }






       [OperationContract]
       public List<string> GetExcelSheetNames(FileStream stream, string ext, bool isFirstRowAsColumnNames)
       {
           // FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

           //1. Reading from a binary Excel file ('97-2003 format; *.xls)
           List<string> listSheetNames = new List<string>();
           IExcelDataReader excelReader = null;
           if (ext == "xls")
               excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
           //...
           //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
           if (ext == "xlsx")
               excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
           //...
           //3. DataSet - The result of each spreadsheet will be created in the result.Tables
           DataSet result = excelReader.AsDataSet();
           foreach (var item in result.Tables)
           {
               DataTable dt = item as DataTable;
               listSheetNames.Add(dt.TableName);
           }
           //...
           //4. DataSet - Create column names from first row
           excelReader.IsFirstRowAsColumnNames = true;
           DataSet resultColumnNames = excelReader.AsDataSet();

           //5. Data Reader methods
           while (excelReader.Read())
           {
               //excelReader.GetInt32(0);
           }

           //6. Free resources (IExcelDataReader is IDisposable)
           excelReader.Close();
           return listSheetNames;

       }


       [OperationContract]
       public List<string> GetExcelAllSheetNames(Stream stream)
       {
           List<string> listSheetNames = new List<string>();

           try
           {
               var dateTimeFormatString = "yyyy-MM-dd--HH-mm-ss";
               var fileUploadName = DateTime.Now.ToLocalTime().ToString(dateTimeFormatString) + ".xls";
               string randomName = fileUploadName;/// Path.GetRandomFileName();
               var tempFile = HttpContext.Current.Server.MapPath("~/TempUploads/" + randomName);
               //  tempFile =HttpContext.Current.Server.MapPath("~/TempUploads/Book11.xslx");// + Path.GetRandomFileName());
               using (var tempStream = File.OpenWrite(tempFile))
               {
                   stream.CopyTo(tempStream);
               }

               //string dataSource = Location + FileName;
               string dataSource = tempFile;
               FileStream fileStream = File.Open(tempFile, FileMode.Open, FileAccess.Read);
               string ext = "";
               //.xls)

               listSheetNames.Add("~/TempUploads/" + randomName);
               IExcelDataReader excelReader = null;

               try
               {

                  /// excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                  excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
               }
               catch
               {

                   // *.xlsx)

                   excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
               }
               //...
               //3. DataSet - The result of each spreadsheet will be created in the result.Tables
               DataSet result = excelReader.AsDataSet();
               foreach (var item in result.Tables)
               {
                   DataTable dt = item as DataTable;
                   listSheetNames.Add(dt.TableName);
               }
               //...
               //4. DataSet - Create column names from first row
               excelReader.IsFirstRowAsColumnNames = true;
               DataSet resultColumnNames = excelReader.AsDataSet();

               //5. Data Reader methods
               while (excelReader.Read())
               {
                   //excelReader.GetInt32(0);
               }

               //6. Free resources (IExcelDataReader is IDisposable)
               excelReader.Close();

               //if (File.Exists(tempFile))
               //  File.Delete(tempFile);
           }
           catch
           {
           }
           return listSheetNames;

       }



       [OperationContract]
       public List<string> GetAccessTableNames(string source, string databaseName, string authentication)
       {
           List<string> listSheetNames = new List<string>();
           DataSet ds;
           string strAccessConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Databases\" + databaseName + ";"  +authentication;
//@"Provider=Microsoft.ACE.Oldedb.12.0;" +
//                                                @"providerName=System.Data.OleDb"+
//                                               @"Data Source=C:\Database\TestDB2.mdb;";

       ///  strAccessConnectionString= @"Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=C:\Database\TestDB2.mdb";
           OleDbConnection conn = new OleDbConnection(strAccessConnectionString);
           try
           {

               string sql = "GRANT SELECT ON MSysObjects TO Admin; SELECT Name FROM MSysObjects WHERE (Left([Name],1)<>'~') AND (Left([Name],4) <> 'MSys') AND ([Type] In (1, 4, 6)) ORDER BY Name";
         

               // retrieving schema for a single table

               conn.Open();
               OleDbCommand commonCommand = new OleDbCommand();
               commonCommand.Connection = conn;
               commonCommand.CommandType = CommandType.Text;
               commonCommand.CommandText = sql;
               OleDbDataAdapter commonAdapter = new OleDbDataAdapter();
               commonAdapter.SelectCommand = commonCommand;
               DataSet dtSet = new DataSet();
               commonAdapter.Fill(dtSet, "table_name");
              // conn.Close();
           }
           catch (Exception err)
           {
              // throw err;
           }
           finally
           {
               conn.Close();
           }
           listSheetNames.Add("Sheet3");
          return listSheetNames;
       }


      [OperationContract]
       public List<string> GetAccessColumnNames(string source, string databaseName, string tableName, string authentication)
       {

           List<string> listSheetNames = new List<string>();
           DataSet ds;
           string strAccessConnectionString = ConfigurationManager.ConnectionStrings["TestDBConnectionString"].ToString();
          // @"Provider=Microsoft.ACE.Oldedb.12.0;" 
          /// @"Data Source=C:\Database\TestDB2.mdb;"+
                                                     //      @"providerName=System.Data.OleDb";


           strAccessConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Databases\" + databaseName + ";" +authentication; ;
           ///  
           OleDbConnection conn = new OleDbConnection(strAccessConnectionString);
           try
           {

               string sql = "select * from "+ tableName;


               // retrieving schema for a single table

               conn.Open();
               OleDbCommand commonCommand = new OleDbCommand();
               commonCommand.Connection = conn;
               commonCommand.CommandType = CommandType.Text;
               commonCommand.CommandText = sql;
               OleDbDataAdapter commonAdapter = new OleDbDataAdapter();
               commonAdapter.SelectCommand = commonCommand;
               DataSet dtSet = new DataSet();
               commonAdapter.Fill(dtSet, tableName);

               foreach (DataColumn col in dtSet.Tables[0].Columns)
               {
                   listSheetNames.Add(col.ColumnName+";"+col.ColumnName);
               }
             
               conn.Close();
           }
           catch (Exception err)
           {
               throw err;
           }
           finally
           {
               conn.Close();
           }
           return listSheetNames;
       }
     

       private DataSet GetAccessDataSet(string connString, string databaseName, string sql)
       {
           DataSet ds = null ;
           OleDbConnection conn = new OleDbConnection(connString);
           try
           {
               
               OleDbCommand cmd = new OleDbCommand(sql, conn);
               cmd.CommandType = CommandType.Text;
               conn.Open();
               OleDbDataReader reader =
                cmd.ExecuteReader(CommandBehavior.SchemaOnly);
               DataTable schemaTable = reader.GetSchemaTable();
              
               reader.Close();
               conn.Close();
           }
           catch (Exception err)
           {
               throw err;
           }
           finally
           {
               conn.Close();
           }
           return ds;
       }

       [OperationContract]
       public List<string> GetExcelXslxSheetNames(Stream stream)
       {
           List<string> listSheetNames = new List<string>();

           try
           {
               var dateTimeFormatString = "yyyy-MM-dd--HH-mm-ss";
               var fileUploadName = DateTime.Now.ToLocalTime().ToString(dateTimeFormatString) + ".xlsx";
               string randomName = fileUploadName;/// Path.GetRandomFileName();
               var tempFile = HttpContext.Current.Server.MapPath("~/TempUploads/" + randomName);
               //  tempFile =HttpContext.Current.Server.MapPath("~/TempUploads/Book11.xslx");// + Path.GetRandomFileName());
               using (var tempStream = File.OpenWrite(tempFile))
               {
                   stream.CopyTo(tempStream);
               }

               //string dataSource = Location + FileName;
               string dataSource = tempFile;
               FileStream fileStream = File.Open(tempFile, FileMode.Open, FileAccess.Read);
               string ext = "";
               //.xls)

               listSheetNames.Add("~/TempUploads/" + randomName);
               IExcelDataReader excelReader = null;

               try
               {

                   excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                   //  excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
               }
               catch
               {

                   // *.xlsx)

                   excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
               }
               //...
               //3. DataSet - The result of each spreadsheet will be created in the result.Tables
               DataSet result = excelReader.AsDataSet();
               foreach (var item in result.Tables)
               {
                   DataTable dt = item as DataTable;
                   listSheetNames.Add(dt.TableName);
               }
               //...
               //4. DataSet - Create column names from first row
               excelReader.IsFirstRowAsColumnNames = true;
               DataSet resultColumnNames = excelReader.AsDataSet();

               //5. Data Reader methods
               while (excelReader.Read())
               {
                   //excelReader.GetInt32(0);
               }

               //6. Free resources (IExcelDataReader is IDisposable)
               excelReader.Close();

               //if (File.Exists(tempFile))
               //  File.Delete(tempFile);
           }
           catch
           {
           }
           return listSheetNames;

       }



       [OperationContract]
       public List<string> GetExcelColumnNames(string filePath, string ext, string sheetName, bool isFirstRowAsColumnNames)
       {

           var tempFile = HttpContext.Current.Server.MapPath(filePath);
           FileStream stream = File.Open(tempFile, FileMode.Open, FileAccess.Read);

           //1. Reading from a binary Excel file ('97-2003 format; *.xls)

           List<string> columnnameList = new List<string>();

           IExcelDataReader excelReader = null;
           if (ext == "xls")
               excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
           //...
           //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
           if (ext == "xlsx")
               excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
           //...
           //3. DataSet - The result of each spreadsheet will be created in the result.Tables
           DataSet result = excelReader.AsDataSet();
           //...
           //4. DataSet - Create column names from first row
           excelReader.IsFirstRowAsColumnNames = true;
           DataSet resultColumnNames = excelReader.AsDataSet();

           foreach (var item in result.Tables)
           {
               DataTable dt = item as DataTable;
               if (dt.TableName == sheetName)
               {
                   DataRow row = dt.Rows[0];
                   
                   string[] list = (from dc in dt.Columns.Cast<DataColumn>()
                                           select dc.ColumnName).ToArray();

                   foreach (var colName in list)
                   {
                       if(isFirstRowAsColumnNames)
                           columnnameList.Add(row[colName].ToString() + ";" + colName);
                       else
                           columnnameList.Add(colName.ToString() + ";" + colName);
                   }
               }


              // columnnameList.Add(dt.Rows);
           }
          // resultColumnNames.Tables.
           //5. Data Reader methods
           while (excelReader.Read())
           {
               //excelReader.GetInt32(0);
           }

           //6. Free resources (IExcelDataReader is IDisposable)
           excelReader.Close();
           return columnnameList;
       }
     

    }


    [DataContract]
    public class CustomException
    {

        [DataMember(Order = 0)]
        public string Message { get; set; }
        [DataMember(Order = 1)]
        public CustomException InnerException;

        public CustomException(Exception ex)
        {
            while (ex.InnerException != null)
                ex = ex.InnerException;
            this.Message = ex.Message + ex.StackTrace;
        }
        public Exception ToException()
        {
            Exception e;
            CustomException ce = this;
            if (ce.InnerException != null)
            {
                Exception inner = ce.InnerException.ToException();
                e = new Exception(ce.Message, inner);
            }
            else
                e = new Exception(ce.Message);
            return e;
        }
    }

    [DataContract]
    public class DataColumnInfo
    {
        [DataMember]
        public string ColumnName { get; set; }

        [DataMember]
        public string ColumnTitle { get; set; }

        [DataMember]
        public string DataTypeName { get; set; }

        [DataMember]
        public bool IsRequired { get; set; }

        [DataMember]
        public bool IsKey { get; set; }

        [DataMember]
        public bool IsReadOnly { get; set; }

        [DataMember]
        public int DisplayIndex { get; set; }

        [DataMember]
        public string EditControlType { get; set; }

        [DataMember]
        public int MaxLength { get; set; }
    }

 [DataContract]
    public class DataSetData
    {
        [DataMember]
        public ObservableCollection<DataTableInfo> Tables { get; set; }
        [DataMember]
        public string DataXML { get; set; }

        public static DataSetData FromDataSet(DataSet ds)
        {
           
            DataSetData dsd = new DataSetData();
            dsd.Tables = new ObservableCollection<DataTableInfo>();
            foreach (DataTable t in ds.Tables)
            {
                DataTableInfo tableInfo = new DataTableInfo { TableName = t.TableName };
                dsd.Tables.Add(tableInfo);
                tableInfo.Columns = new ObservableCollection<DataColumnInfo>();
                foreach (DataColumn c in t.Columns)
                {
                  
                    DataColumnInfo col = new DataColumnInfo { ColumnName = c.ColumnName, ColumnTitle = c.ColumnName, DataTypeName = c.DataType.FullName, MaxLength = c.MaxLength, IsKey = c.Unique, IsReadOnly = (c.Unique || c.ReadOnly), IsRequired = !c.AllowDBNull };
                    if (c.DataType == typeof(System.Guid))
                    {
                        col.IsReadOnly = true;
                        col.DisplayIndex = -1;
                    }
                    tableInfo.Columns.Add(col);
                }
               
            }
            dsd.DataXML += XmlData(ds);
            return dsd;
        }


        private static string XmlData(DataSet ds)
        {
            string delimeter = "|";

            XmlWriterSettings wSettings = new XmlWriterSettings();
            wSettings.Indent = true;
            MemoryStream ms = new MemoryStream();
            XmlWriter xw = XmlWriter.Create(ms, wSettings);// Write Declaration
            xw.WriteStartDocument();
           
            xw.WriteStartElement("Dataset");

            foreach (DataTable dt in ds.Tables)
            {

                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    xw.WriteStartElement(dt.TableName);
                    i += 1;


                    foreach (DataColumn c in dt.Columns)
                    {
                        string rowData = "";

                        if (dr[c.ColumnName] != null)
                        {
                            string name = CharacterHandler.ReplaceSpecialCharacter(c.ColumnName);
                            xw.WriteStartElement(name);
                            if (dr[c.ColumnName].ToString() == "System.Byte[]")
                            {
                                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                                byte[] obj = (Byte[])(dr[c.ColumnName]);

                                string strImg = Convert.ToBase64String(obj).ToString();
                                rowData = strImg;

                            }
                            else
                            {
                                rowData = dr[c.ColumnName].ToString();
                            }
                            xw.WriteString(rowData);
                            xw.WriteEndElement();
                        }




                    }



                    xw.WriteEndElement();



                }

            }
            xw.WriteEndElement();
            xw.WriteEndDocument();
            // Flush the write
            xw.Flush();
            Byte[] buffer = new Byte[ms.Length];
            buffer = ms.ToArray();
            string xmlOutput = System.Text.Encoding.UTF8.GetString(buffer);
            return xmlOutput.Remove(0, 39);
        }

        public static DataSet ToDataSet(DataSetData dsd)
        {
            DataSet ds = new DataSet();
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();
                Byte[] byteArray = encoding.GetBytes(dsd.DataXML);
                MemoryStream stream = new MemoryStream(byteArray);
                XmlReader reader = new XmlTextReader(stream);
                ds.ReadXml(reader);
                XDocument xd = XDocument.Parse(dsd.DataXML);
                foreach (DataTable dt in ds.Tables)
                {
                    var rs = from row in xd.Descendants(dt.TableName)
                             select row;

                    int i = 0;
                    foreach (var r in rs)
                    {
                        DataRowState state = (DataRowState)Enum.Parse(typeof(DataRowState), r.Attribute("RowState").Value);
                        DataRow dr = dt.Rows[i];
                        dr.AcceptChanges();
                        if (state == DataRowState.Deleted)
                            dr.Delete();
                        else if (state == DataRowState.Added)
                            dr.SetAdded();
                        else if (state == DataRowState.Modified)
                            dr.SetModified();
                        i++;
                    }
                }
            }
            catch { 
            }
            return ds;
        }

       


    }


 [DataContract]
 public class DataTableInfo
 {
     [DataMember]
     public string TableName { get; set; }

     [DataMember]
     public ObservableCollection<DataColumnInfo> Columns { get; set; }

 }

 [DataContract]
 public class ListMenu
 {
     [DataMember]
     public int MenuItemID { get; set; }

     [DataMember]
     public int DisplayDefinitionID { get; set; }
 }


 [DataContract]
 public class FieldMapped
 {
      [DataMember]
     public int FieldID { get; set; }
      [DataMember]
     public string SourceField { get; set; }

      [DataMember]
      public string ActualColumnName { get; set; }
      [DataMember]
     public string DestinationField { get; set; }
      [DataMember]
     public string DataType { get; set; }
      [DataMember]
     public bool IsFirstRow { get; set; }
 }


 [Serializable]
 public class TempStorage
 {
     public string Value { get; set;
     }
     public string Display { get; set; }
 }




