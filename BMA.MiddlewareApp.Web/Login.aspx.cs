﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ServiceModel;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;

namespace BMA.MiddlewareApp.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //WindowsPrincipal P = Thread.CurrentPrincipal as WindowsPrincipal;

          
            string currentUser = Environment.UserName;
            if (AuthenticationUser("Prince", "intermerat", currentUser))
            {
                Session["authenticateduser"] = currentUser;
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session["authenticateduser"] = "user";
           Response.Redirect("~/Default.aspx");
             //   txtUsername.Value = currentUser;
                // 
            }

        }




        public bool AuthenticationUser(string user, string pass, string currentUser)
        {

          

          
            bool valid = false;
            try
            {

                var path = string.Format("WinNT://{0},Computer", Environment.MachineName);
                //DirectorySearcher ds = new DirectorySearcher(new DirectoryEntry(path, user, pass));
                //ds.Filter = String.Format("(SAMAccountName={0})", currentUser);
                //ds.PropertiesToLoad.Add("SAMAccountName");
                //SearchResult result = ds.FindOne();



                var computerEntry = new DirectoryEntry(path);
                  DirectoryEntry childEntry = computerEntry.Children.Find(currentUser, "User");



                  valid = childEntry != null;

            }
            catch (COMException)
            {
                //wrong pass or expired pass
                valid = false;
            }
            return valid;
        }

        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
            //string names = "";
            //var path = string.Format("WinNT://{0},Computer", Environment.MachineName);
            //using (var computerEntry = new DirectoryEntry(path))
            //    foreach (DirectoryEntry childEntry in computerEntry.Children)
            //        if (childEntry.SchemaClassName == "User")
            //            names += childEntry.Name;
            //lblmsg.Text = names;
            string username = txtUsername.Value;
            string password = txtPassword.Value;
            if ((AuthenticationUser(username, password, username)))
            {
                Session["authenticateduser"] = username;
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                lblmsg.Text = "You do not have permission to use this application using the credentials that you supplied. Contact your system administrator!";
            }
        }
    }
}