﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Configuration;

namespace BMA.MiddlewareApp.Web.Providers
{
    public class CustomProfileProvider : System.Web.Profile.ProfileProvider
    {
        private MiddlewareEntities1 dataContext = new MiddlewareEntities1();

        private string applicationName = "Middleware";

        #region Public Properties

        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }
        public string FriendlyName { get; set; }
        //public override string FriendlyName
        //{
        //    get { return applicationName; }
        //    set { applicationName = value; }
        //}
        #endregion

        #region Private Methods

        private LoginProfile GetProfile(string username)
        {
            try
            {
                LoginProfile profile = (from p in dataContext.LoginProfiles
                                   where p.UserID == (from u in dataContext.LoginUserInfoes
                                                       where u.UserName == username
                                                       select u.ID).SingleOrDefault<int>()
                                   select p).SingleOrDefault();

                return profile;
            }
            catch(Exception)
            {
            }

            return null;
        }

        #endregion

        #region Public Methods

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
        }

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
        {
            SettingsPropertyValueCollection valueCollection = new SettingsPropertyValueCollection();

            try
            {
                LoginProfile profile = GetProfile(context["UserName"] as string);

                // If a profile was found than loop through all profile properties and 
                // assign appropriate values

                if (profile != null)
                {
                    foreach (SettingsProperty property in collection)
                    {
                        SettingsPropertyValue propertyValue = new SettingsPropertyValue(property);

                        switch (property.Name)
                        {
                            case "CurrentTheme":
                                {
                                    propertyValue.PropertyValue = profile.CurrentTheme;
                                    break;
                                }

                            case "FriendlyName":
                                {
                                    propertyValue.PropertyValue = "Middleware";
                                    break;
                                }
                            case "PageSize":
                                {
                                    propertyValue.PropertyValue = profile.PageSize.Value;
                                    break;
                                }

                        }

                        valueCollection.Add(propertyValue);
                    }
                }

                else
                {
                    foreach (SettingsProperty property in collection)
                    {
                        SettingsPropertyValue propertyValue = new SettingsPropertyValue(property);

                        switch (property.Name)
                        {
                            
                           
                            case "FriendlyName":
                                {
                                    propertyValue.PropertyValue = "Middleware";
                                    break;
                                }
                            case "PageSize":
                                {
                                    if(propertyValue.PropertyValue.ToString() != string.Empty)
                                    profile.PageSize = Convert.ToInt32(propertyValue.PropertyValue.ToString());
                                    break;
                                }
                            //case "CurrentTheme":
                            //    {
                            //        profile.CurrentTheme = propertyValue.PropertyValue.ToString();
                            //        break;
                            //    }
                        }

                        valueCollection.Add(propertyValue);
                    }
                }
            }
            catch (Exception)
            {
            }

            return valueCollection;
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
        {
            SettingsPropertyValueCollection valueCollection = new SettingsPropertyValueCollection();

            // Extract the username to retrieve property values for

            string userName = context["UserName"] as string;

            // Get the profile for current user

            LoginProfile dataProfile = (from p in dataContext.LoginProfiles
                                                               where p.UserID == (from u in dataContext.LoginUserInfoes
                                                                                   where u.UserName == userName
                                                                                   select u.ID).SingleOrDefault<int>()
                                                               select p).SingleOrDefault();

            // If a profile was found than loop through all profile properties and 
            // assign appropriate values

            if (dataProfile != null)
            {
                foreach (SettingsPropertyValue propertyValue in collection)
                {
                    switch (propertyValue.Name)
                    {
                        case "CurrentTheme":
                            {
                                dataProfile.CurrentTheme = propertyValue.PropertyValue.ToString();
                                break;
                            }
                       
                    }
                }

                dataContext.SaveChanges();
            }
        }

        #endregion

        #region Unsupported Operations

        public override int DeleteInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            throw new NotImplementedException();
        }

        public override int DeleteProfiles(ProfileInfoCollection profiles)
        {
            throw new NotImplementedException();
        }

        public override int DeleteProfiles(string[] usernames)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection FindInactiveProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection GetAllInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection GetAllProfiles(ProfileAuthenticationOption authenticationOption, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection FindProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

  