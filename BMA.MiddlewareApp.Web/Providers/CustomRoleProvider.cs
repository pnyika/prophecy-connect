﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.ServiceModel.DomainServices.Server.ApplicationServices;

namespace BMA.MiddlewareApp.Web.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        public override string[] GetRolesForUser(string username)
        {
            List<string> roles = new List<string>();

            try
            {
                MiddlewareEntities1 context = new MiddlewareEntities1();
                LoginUserInfo user = context.LoginUserInfoes.Where(e => e.UserName == username).SingleOrDefault<LoginUserInfo>();

                if (user != null)
                {
                    // The only way to find out what roles have been assigned to the user
                    // is to load the UsersRoles association

                    user.UserRoles.Load();

                    // Once populated any roles that the user has been assigned
                    // to should be available in the UsersRoles property

                    foreach (LoginUserRole role in user.UserRoles)
                        roles.Add(role.Role.Name);
                }
            }
            catch (Exception)
            {
            }

            return roles.ToArray();
        }

        public override string ApplicationName
        {
            get { return "Middleware"; }
            set { }
        }

        // Other overrides not implemented
        #region Not Implemented Overrides
        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}