﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BMA.MiddlewareApp.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDataTableService" in both code and config file together.
    [ServiceContract]
    [ServiceKnownType(typeof(IDataTableService))]
    public interface IDataTableService
    {
        [OperationContract]
        IEnumerable<Dictionary<string, object>> GetData(string sql);
    }
}
