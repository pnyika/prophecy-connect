﻿
namespace BMA.MiddlewareApp.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using System.Data.Objects.DataClasses;


    // The MetadataTypeAttribute identifies ModelDefinitionMetadata as the class
    // that carries additional metadata for the ModelDefinition class.
    [MetadataTypeAttribute(typeof(ModelDefinition.ModelDefinitionMetadata))]
    public partial class ModelDefinition
    {

        // This class allows you to attach custom attributes to properties
        // of the ModelDefinition class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ModelDefinitionMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ModelDefinitionMetadata()
            {
            }

            public string Code { get; set; }

            public Nullable<DateTime> DateOffSet { get; set; }

            public int ID { get; set; }

            public string Name { get; set; }

            public Nullable<int> OutputRounding { get; set; }

            public DateTime PeriodStart { get; set; }

            public int Repeat { get; set; }

            public string RepeatBucket { get; set; }

            public Nullable<int> RoundingMax { get; set; }

            public string SequentialPeriod { get; set; }

            public Nullable<int> UserRounding { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies MTPAdjustmentMetadata as the class
    // that carries additional metadata for the MTPAdjustment class.
    [MetadataTypeAttribute(typeof(MTPAdjustment.MTPAdjustmentMetadata))]
    public partial class MTPAdjustment
    {

        // This class allows you to attach custom attributes to properties
        // of the MTPAdjustment class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class MTPAdjustmentMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private MTPAdjustmentMetadata()
            {
            }

            public string Attribute1 { get; set; }

            public string Attribute2 { get; set; }

            public string Attribute3 { get; set; }

            public string Attribute4 { get; set; }

            public Nullable<decimal> CostUnit { get; set; }

            public Nullable<decimal> FixedCost { get; set; }

            public string Location { get; set; }

            public string Material { get; set; }

            public string MaxTotal { get; set; }

            public Nullable<int> MaxunitsPerPeriod { get; set; }

            public string MinTotal { get; set; }

            public Nullable<int> MinUnitsPerPeriod { get; set; }

            public string Object { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies TSAdjustmentMetadata as the class
    // that carries additional metadata for the TSAdjustment class.
    [MetadataTypeAttribute(typeof(TSAdjustment.TSAdjustmentMetadata))]
    public partial class TSAdjustment
    {

        // This class allows you to attach custom attributes to properties
        // of the TSAdjustment class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TSAdjustmentMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private TSAdjustmentMetadata()
            {
            }

            public string Cal { get; set; }

            public Nullable<DateTime> EffectiveFrom { get; set; }

            public Nullable<DateTime> EffectiveTo { get; set; }

            public string Field { get; set; }

            public string Location { get; set; }

            public string Material { get; set; }

            public string Object { get; set; }

            public string PeriodFactor { get; set; }

            public Nullable<decimal> Value { get; set; }
        }
    }


    [MetadataTypeAttribute(typeof(Attribute.AttributeMetadata))]
    public partial class Attribute
    {

        // This class allows you to attach custom attributes to properties
        // of the Attribute class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class AttributeMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private AttributeMetadata()
            {
            }

            public string AttributeName { get; set; }

            public string AttributeType { get; set; }

            public string ConstraintType { get; set; }

            public Facility Facility { get; set; }

            public int Facility_ID { get; set; }

            public int ID { get; set; }

            public double MaxTotalUnitsOrValue { get; set; }

            public double MaxUnitsOrValuePerPeriod { get; set; }

            public double MinTotalUnitsOrValue { get; set; }

            public double MinUnitsOrValuePerPeriod { get; set; }

            public Model Model { get; set; }

            public int Model_ID { get; set; }

            public string ObjectClassAppliedTo { get; set; }

            public string OnOff { get; set; }

            public double PeriodActivityBias { get; set; }

            public double SoftConstraintBias { get; set; }

            public string Tag1 { get; set; }

            public string Tag2 { get; set; }

            public double TotalActivityBias { get; set; }

            public double TotalFixedAdjustment { get; set; }

            public string TotallerType { get; set; }

            public string VariableType { get; set; }

            public double VariableUnitAdjustment { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies CalendarMetadata as the class
    // that carries additional metadata for the Calendar class.
    [MetadataTypeAttribute(typeof(Calendar.CalendarMetadata))]
    public partial class Calendar
    {

        // This class allows you to attach custom attributes to properties
        // of the Calendar class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class CalendarMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private CalendarMetadata()
            {
            }

            public string CalendarCode { get; set; }

            public EntityCollection<CalendarDetail> CalendarDetails { get; set; }

            public string CalendarName { get; set; }

            public EntityCollection<CalendarPeriod> CalendarPeriods { get; set; }

            public int HistoryDays { get; set; }

            public DateTime HorizonDate { get; set; }

            public int HorizonDays { get; set; }

            public int ID { get; set; }

            public bool InUse { get; set; }

            public Model Model { get; set; }

            public int Model_ID { get; set; }

            public int RepeatBucket { get; set; }

            public int UnitID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies CalendarDetailMetadata as the class
    // that carries additional metadata for the CalendarDetail class.
    [MetadataTypeAttribute(typeof(CalendarDetail.CalendarDetailMetadata))]
    public partial class CalendarDetail
    {

        // This class allows you to attach custom attributes to properties
        // of the CalendarDetail class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class CalendarDetailMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private CalendarDetailMetadata()
            {
            }

            public Calendar Calendar { get; set; }

            public int Calendar_ID { get; set; }

            public int ID { get; set; }

            public DateTime PeriodDateTime { get; set; }

            public double Value { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies CalendarPeriodMetadata as the class
    // that carries additional metadata for the CalendarPeriod class.
    [MetadataTypeAttribute(typeof(CalendarPeriod.CalendarPeriodMetadata))]
    public partial class CalendarPeriod
    {

        // This class allows you to attach custom attributes to properties
        // of the CalendarPeriod class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class CalendarPeriodMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private CalendarPeriodMetadata()
            {
            }

            public Calendar Calendar { get; set; }

            public int Calendar_ID { get; set; }

            public int ID { get; set; }

            public DateTime PeriodDateTime { get; set; }

            public double Value { get; set; }

            public double ValueAdd { get; set; }

            public double ValueFactor { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies FacilityMetadata as the class
    // that carries additional metadata for the Facility class.
    [MetadataTypeAttribute(typeof(Facility.FacilityMetadata))]
    public partial class Facility
    {

        // This class allows you to attach custom attributes to properties
        // of the Facility class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class FacilityMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private FacilityMetadata()
            {
            }

            public Nullable<int> AttributeID1 { get; set; }

            public Nullable<int> AttributeID2 { get; set; }

            public Nullable<int> AttributeID3 { get; set; }

            public Nullable<int> AttributeID4 { get; set; }

            public EntityCollection<Attribute> Attributes { get; set; }

            public string FacilityName { get; set; }

            public int ID { get; set; }

            public Model Model { get; set; }

            public int Model_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies MaterialMetadata as the class
    // that carries additional metadata for the Material class.
    [MetadataTypeAttribute(typeof(Material.MaterialMetadata))]
    public partial class Material
    {

        // This class allows you to attach custom attributes to properties
        // of the Material class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class MaterialMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private MaterialMetadata()
            {
            }

            public Nullable<double> ERPUnitConvFactor { get; set; }

            public int ID { get; set; }

            public bool InUse { get; set; }

            public string MaterialCode { get; set; }

            public Model Model { get; set; }

            public int Model_ID { get; set; }

            public string SKU_Number { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ModelMetadata as the class
    // that carries additional metadata for the Model class.
    [MetadataTypeAttribute(typeof(Model.ModelMetadata))]
    public partial class Model
    {

        // This class allows you to attach custom attributes to properties
        // of the Model class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ModelMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ModelMetadata()
            {
            }

            public EntityCollection<Attribute> Attributes { get; set; }

            public EntityCollection<Calendar> Calendars { get; set; }

            public Nullable<DateTime> DateOffSet { get; set; }

            public EntityCollection<Facility> Facilities { get; set; }

            public int ID { get; set; }

            public EntityCollection<Material> Materials { get; set; }

            public string ModelCode { get; set; }

            public string ModelName { get; set; }

            public EntityCollection<Object> Objects { get; set; }

            public Nullable<int> OutputRounding { get; set; }

            public DateTime PeriodStart { get; set; }

            public int Repeat { get; set; }

            public int RepeatBucket { get; set; }

            public Nullable<int> RoundingMax { get; set; }

            public string SequentialPeriod { get; set; }

            public Nullable<int> UserRounding { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies ObjectMetadata as the class
    // that carries additional metadata for the Object class.
    [MetadataTypeAttribute(typeof(Object.ObjectMetadata))]
    public partial class Object
    {

        // This class allows you to attach custom attributes to properties
        // of the Object class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class ObjectMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private ObjectMetadata()
            {
            }

            public int ID { get; set; }

            public Model Model { get; set; }

            public int Model_ID { get; set; }

            public string ObjectName { get; set; }

            public string ObjectType { get; set; }

            public Nullable<bool> UseInStockBuild { get; set; }
        }
    }
}
