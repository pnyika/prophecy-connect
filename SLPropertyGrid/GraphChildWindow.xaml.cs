﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Resources;
using System.Reflection;

namespace SLPropertyGrid
{
    public partial class GraphChildWindow : ChildWindow
    {
        public delegate void GraphCreatedEventHandler(object sender, GraphCreatedEventArgs e);
        public event GraphCreatedEventHandler GraphDefinitionCreated;
        public event EventHandler GraphCanceled;
        public GraphChildWindow()
        {
            InitializeComponent();
            LoadGraphType();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ControlItem item = new ControlItem();
            item.Display = txtTitle.Text;
            item.Value = txtPath.Text;
            ControlItem itm = ddlGraphType.SelectedItem as ControlItem;
            item.Type = itm.Value;


            GraphDefinitionCreated(this, new GraphCreatedEventArgs(item));
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        private void LoadGraphType()
        {
            
            List<ControlItem> types = new List<ControlItem>();
           
            ddlGraphType.DisplayMemberPath = "Display";
            ddlGraphType.SelectedValuePath = "Value";

         

           // List<Type> typelist = GetList();///GetTypesInNamespace(Assembly.GetExecutingAssembly(), "AmCharts.Windows.QuickCharts");
           // foreach (var item in typelist)
           // {
               // types.Add(new ControlItem { Value = item.Name, Display = item.Name, index = 4 });

          //  }

            types.Add(new ControlItem { Value = "AreaGraph", Display = "AreaGraph", index = 4 });
            types.Add(new ControlItem { Value = "ColumnGraph", Display = "ColumnGraph", index = 5 });
            types.Add(new ControlItem { Value = "LineGraph", Display = "LineGraph", index = 6 });
            ddlGraphType.ItemsSource = types;
          
        }

        public List<Type> GetList()
        {


            List<Assembly> loadedAssemblies = new List<Assembly>();
            foreach (AssemblyPart part in Deployment.Current.Parts)
            {
                StreamResourceInfo info = Application.GetResourceStream(new Uri(part.Source, UriKind.Relative));
                Assembly asm = part.Load(info.Stream);
                loadedAssemblies.Add(asm);
            }


            List<Type> types = new List<Type>();
            foreach (var assembly in loadedAssemblies)
            {


                // var assembly = Assembly.GetExecutingAssembly();
                foreach (var type in assembly.GetTypes())
                {
                    if ((type.Namespace == "AmCharts.Windows.QuickCharts") && (type.IsClass))// && (type.IsAssignableFrom(typeof(Control))))
                    {
                        types.Add(type);
                    }
                }
            }
            return types;
        }

    }

    public class GraphCreatedEventArgs
    {
        private ControlItem controlItem = null;

        public GraphCreatedEventArgs() { }
        public GraphCreatedEventArgs(ControlItem controlItem)
        {
            this.controlItem = controlItem;
        }

        public ControlItem CreatedGraph { get { return controlItem; } }
    }
}

