﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Telerik.Windows.Controls.ColorPicker;
using Telerik.Windows.Controls;

namespace SLPropertyGrid
{

    public class ColorValueEditor : ValueEditorBase
    {
        #region Fields
        object currentValue;
        bool showingDTP;
        StackPanel pnl;
        protected TextBox txt;
        protected RadColorPicker cp;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="label"></param>
        /// <param name="property"></param>
        public ColorValueEditor(PropertyGridLabel label, PropertyItem property)
            : base(label, property)
        {
            currentValue = property.Value;
            property.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(property_PropertyChanged);
            property.ValueError += new EventHandler<ExceptionEventArgs>(property_ValueError);

            pnl = new StackPanel();
            this.Content = pnl;

            cp = new RadColorPicker();
            cp.Visibility = Visibility.Visible;
            cp.Margin = new Thickness(0);
            cp.Padding = new Thickness(2);
            cp.VerticalAlignment = VerticalAlignment.Center;
            cp.HorizontalAlignment = HorizontalAlignment.Stretch;
            cp.DropDownOpened += new Telerik.Windows.RadRoutedEventHandler(cp_DropDownOpened);
            cp.DropDownClosed += new Telerik.Windows.RadRoutedEventHandler(cp_DropDownClosed);
            cp.LostFocus += new RoutedEventHandler(dtp_LostFocus);
            cp.Background = new SolidColorBrush(Colors.White);
            cp.Foreground = this.Property.CanWrite ? new SolidColorBrush(Colors.Black) : new SolidColorBrush(Colors.Gray);

            pnl.Children.Add(cp);
            cp.Focus();

            this.ShowTextBox();
        }

        void cp_DropDownClosed(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            cp.Focus();
        }

        void cp_DropDownOpened(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            showingDTP = true;
        }
        #endregion

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            Debug.WriteLine("ColorValueEditor : OnGotFocus");

            if (showingDTP)
                return;

            base.OnGotFocus(e);

            if (this.Property.CanWrite)
                this.ShowDatePicker();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLostFocus(RoutedEventArgs e)
        {
            Debug.WriteLine("ColorValueEditor : OnLostFocus");

            if (showingDTP)
                return;

            base.OnLostFocus(e);
        }
        #endregion

        #region Methods
        void ShowDatePicker()
		{
		if (null == txt)
        return;

          cp.SelectedColorChanged -= new EventHandler(cp_SelectedColorChanged); ;
			cp.Visibility = Visibility.Visible;
			cp.Focus();

		txt.Visibility = Visibility.Collapsed;
			pnl.Children.Remove(txt);
			txt = null;

         //  cp.SelectedColor = System.Windows.Media.Colors.Black;
           
            cp.SelectedColorChanged += new EventHandler(cp_SelectedColorChanged);
            cp.IsDropDownOpen = true;

		}

        void cp_SelectedColorChanged(object sender, EventArgs e)
        {
            currentValue = cp.SelectedColor;
          ColorToBrushConverter convertor = new ColorToBrushConverter();
            var bushcolor = convertor.Convert(currentValue, null, null, null);
          
            this.Property.Value = bushcolor;

           

        }
        void ShowTextBox()
        {
            if (null != txt)
                return;

            txt = new TextBox();
            txt.Height = 20;
            txt.BorderThickness = new Thickness(0);
            txt.Margin = new Thickness(0);
            txt.Padding = new Thickness(2);
            txt.VerticalAlignment = VerticalAlignment.Center;
            txt.HorizontalAlignment = HorizontalAlignment.Stretch;
            txt.Text = currentValue.ToString();
            txt.IsReadOnly = !this.Property.CanWrite;
            txt.Foreground = this.Property.CanWrite ? new SolidColorBrush(Colors.Black) : new SolidColorBrush(Colors.Gray);
            txt.Background = new SolidColorBrush(Colors.White);
            txt.Text = this.Property.Value.ToString();
            pnl.Children.Add(txt);

            showingDTP = false;
            cp.Visibility = Visibility.Collapsed;
        }
        #endregion

        #region Event Handlers
        void property_ValueError(object sender, ExceptionEventArgs e)
        {
            MessageBox.Show(e.EventException.Message);
        }
        void property_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
                currentValue = this.Property.Value;

            if (e.PropertyName == "CanWrite")
            {
                if (!this.Property.CanWrite && showingDTP)
                    ShowTextBox();
            }
        }


        void dtp_LostFocus(object sender, RoutedEventArgs e)
        {
            currentValue = cp.SelectedColor;
            this.Property.Value = currentValue;
            if (cp.IsDropDownOpen)
                return;
            ShowTextBox();
        }
        #endregion
    }
}