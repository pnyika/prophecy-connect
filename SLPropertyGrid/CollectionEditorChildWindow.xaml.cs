﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Reflection;
using Telerik.Windows.Controls;
using AmCharts.Windows.QuickCharts;
using System.Threading;
using System.Windows.Resources;
using System.Collections.ObjectModel;

namespace SLPropertyGrid
{
	public partial class CollectionEditorChildWindow : ChildWindow
	{
		IList targetList;
		Type collectionType;
        ObservableCollection<ControlItem> controlItems = new ObservableCollection<ControlItem>();
        bool isGraphs = false;
        int i = 0;
		public CollectionEditorChildWindow(IList target)
		{
			var styleObj = Application.Current.Resources["collectionEditorChildWindowStyle"];
			if (null != styleObj)
			{
				var style = styleObj as Style;
				if (null != style)
					this.Style = style;
			}

			InitializeComponent();

			this.targetList = target;

			collectionType = targetList.GetType().GetProperty("Item").PropertyType;

			Title = collectionType.Name + " Collection Editor";
            if (collectionType.Name == "SerialGraph")
            {

                isGraphs = true;
                 
                foreach (var item in target)
               {
                   
                     Type t = item.GetType();
                     PropertyInfo[] props = t.GetProperties();
                     PropertyInfo newprp = props.Where(p => p.Name == "ValueMemberPath").FirstOrDefault();
                     var value = newprp.GetValue(item, null);
                     controlItems.Add(new ControlItem { Value = value.ToString(), Display = value.ToString(), index = i });
                     i += 1;
                }
                CollectionListBox.DisplayMemberPath = "Display";
                CollectionListBox.SelectedValuePath = "Value";
                CollectionListBox.ItemsSource = controlItems;
                if (controlItems.Count > 0)
                    CollectionListBox.SelectedValue = 1;
               

            }
            else
            {

                CollectionListBox.ItemsSource = target;
                if (target.Count > 0)
                    CollectionListBox.SelectedItem = target[0];
            }
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
		}

		private void CollectionListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				//PropertyGrid.SelectedObject = e.AddedItems[0];
                ControlItem item = e.AddedItems[0] as ControlItem;
                PropertyGrid.SelectedObject = targetList[item.index];
			}
			else
				PropertyGrid.SelectedObject = null;
		}

		private void AddButton_Click(object sender, RoutedEventArgs e)
		{
            if (isGraphs)
            {
                GraphChildWindow child = new GraphChildWindow();

                child.GraphDefinitionCreated += (s, ev) =>
                {
                    Add(ev.CreatedGraph);
                    
                    child.Close();
                };

                child.GraphCanceled += (s, ev) => { child.Close(); };
                child.Show();
            }
            else
            {
                var constructor = collectionType.GetConstructor(Type.EmptyTypes);
                if (constructor == null)
                    throw new Exception("No default constructor found.");  // maybe check for a default constructor in the editor?

                var newInstance = constructor.Invoke(null);
                targetList.Add(newInstance);

                // gotta be a better way to refresh here.
                CollectionListBox.ItemsSource = null;
                CollectionListBox.ItemsSource = targetList;

                CollectionListBox.SelectedItem = newInstance;
            }
		}

        private void Add(ControlItem item)
        {
          
            if(item.Type =="LineGraph")
            {
                var graph = new LineGraph();
                graph.ValueMemberPath = item.Value;
                graph.Title = item.Display;
                targetList.Add(graph);
                controlItems.Add(new ControlItem { Value = graph.ValueMemberPath, Display = graph.ValueMemberPath, index = i });
               
            }
            if(item.Type =="AreaGraph")
            {
                var graph = new AreaGraph();
                graph.ValueMemberPath = item.Value;
                graph.Title = item.Display;
                targetList.Add(graph);
                controlItems.Add(new ControlItem { Value = graph.ValueMemberPath, Display = graph.ValueMemberPath, index = i });
            }
            if (item.Type == "ColumnGraph")
            {
                var graph = new ColumnGraph();
                graph.ValueMemberPath = item.Value;
                graph.Title = item.Display;
                targetList.Add(graph);
                controlItems.Add(new ControlItem { Value = graph.ValueMemberPath, Display = graph.ValueMemberPath, index = i });
            }

            i += 1;
          
        }





        void button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Ok");
        }

		private void RemoveButton_Click(object sender, RoutedEventArgs e)
		{
			targetList.Remove(CollectionListBox.SelectedItem);

			// gotta be a better way to refresh here.
			CollectionListBox.ItemsSource = null;
			CollectionListBox.ItemsSource = targetList;

			if (targetList.Count > 0)
				CollectionListBox.SelectedItem = targetList[0];
		}

		private void MoveUpButton_Click(object sender, RoutedEventArgs e)
		{
			if (CollectionListBox.SelectedIndex > 0)
			{
				var selected = CollectionListBox.SelectedItem;
				var position = targetList.IndexOf(selected);
				targetList.Remove(selected);
				position--;
				targetList.Insert(position, selected);

				// gotta be a better way to refresh here.
				CollectionListBox.ItemsSource = null;
				CollectionListBox.ItemsSource = targetList;
				CollectionListBox.SelectedItem = selected;
			}
		}

		private void MoveDownButton_Click(object sender, RoutedEventArgs e)
		{
			if (CollectionListBox.SelectedIndex < targetList.Count - 1)
			{
				var selected = CollectionListBox.SelectedItem;
				var position = targetList.IndexOf(selected);
				targetList.Remove(selected);
				position++;
				targetList.Insert(position, selected);

				// gotta be a better way to refresh here.
				CollectionListBox.ItemsSource = null;
				CollectionListBox.ItemsSource = targetList;
				CollectionListBox.SelectedItem = selected;
			}
		}

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
	}
   
    public class ControlItem
    {
        public string Value { get; set; }
        public string Display { get; set; }
        public int index { get; set; }
        public string Type { get; set; }
    }
}

