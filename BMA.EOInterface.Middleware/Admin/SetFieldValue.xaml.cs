﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SetFieldValue 
    {
        EditorContext context = new EditorContext();
        ObservableCollection<Server> serverList;
        ObservableCollection<Table> tableList;
        List<Table> tables;
        private int viewID;
        ViewFieldSummary fld;
        Field fieldContent;
        ViewField viewField;
        public SetFieldValue(int viewId, ViewFieldSummary field, List<Table> tablelist)
        {
            InitializeComponent();
            viewID = viewId;
            tables = tablelist;
            fld = field;

            LoadField(field.FieldID, field.ID);
            CheckTables();
        }


        private void LoadField(int fieldID, int viewFieldID)
        {
            LoadOperation<Field> loadOper = context.Load(context.GetFieldsQuery().Where(f => f.ID == fieldID), CallbackField, null);
            LoadOperation<ViewField> loadOp = context.Load(context.GetViewFieldsQuery().Where(f => f.ID == viewFieldID), CallbackViewField, null);
        }


        private void CallbackField(LoadOperation<Field> loadOp)
        {


            if (loadOp != null)
            {
                fieldContent = loadOp.Entities.FirstOrDefault();
               


            }
        }
        private void CallbackViewField(LoadOperation<ViewField> loadOp)
        {


            if (loadOp != null)
            {
                viewField = loadOp.Entities.FirstOrDefault();



            }
        }

        private void CheckTables()
        {
            if (tables.Count > 0)
            {
               
                var query = (from t in tables
                             select t).FirstOrDefault();
               
                
                LoadOperation<Table> loadOper = context.Load(context.GetTablesQuery().Where(x => x.Server_ID == query.Server_ID), CallbackConnTable, null);

            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            
            var selectedTable = lstTable.SelectedItems;
            if(selectedTable == null)
                return;

            Field selectedField = ddlField.SelectedItem as Field;
            if(selectedField == null)
                return;

            viewField.UseValueField = true;

            if ((fieldContent.Type == selectedField.Type) || ((fieldContent.Type.Contains("int") && selectedField.Type.Contains("int"))))
            {
                fieldContent.ValueField_ID = selectedField.ID;
                
              
                selectedField.isValueField = byte.Parse("1");
                int i = 1;
                foreach (var item in lstField.SelectedItems)
                {
                    Field fd = item as Field;
                    fieldContent.DisplayField_ID = fd.ID;
                    fd.isDisplayField = byte.Parse(i.ToString());
                   // fd.DisplayFieldSuffix = ", ";
                    i++;
                }

                context.SubmitChanges(submit => { if (submit.HasError) {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
                    Message.ErrorMessage(submit.Error.Message);
                } else { RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content ="Successful" }); this.Close(); } }, null);
            }
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

      

        private void CallbackConnTable(LoadOperation<Table> loadOp)
        {


            tableList = new ObservableCollection<Table>();

            if (loadOp.Entities != null)
            {
                foreach (Table tbl in loadOp.Entities)
                {
                    tableList.Add(tbl);
                }


                lstTable.ItemsSource = tableList;
            }
        }
       


          private void lstTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Table table = lstTable.SelectedItem as Table;
                        if (table == null)
                            return;

                        lblFieldTable.Text = fld.FieldName +" Field Ralated To " + table.TableName;
                      
                 LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFields, null);

         }



        

        private void CallbackFields(LoadOperation<Field> loadOp)
        {


            if (loadOp != null)
            {
                lstField.ItemsSource = loadOp.Entities;
                ddlField.ItemsSource = loadOp.Entities;
               

            }
        }
    }
    }


