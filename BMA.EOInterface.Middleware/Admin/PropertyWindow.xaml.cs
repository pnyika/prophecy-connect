﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using Telerik.Windows.Controls;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using AmCharts.Windows.QuickCharts;

namespace BMA.MiddlewareApp.Admin
{
    public partial class PropertyWindow 
    {
        Person person;
        Control control;
        EditorContext context = new EditorContext();
        ObservableCollection<Control> objectList = new ObservableCollection<Control>();
        List<ControlName> nameList = new List<ControlName>();
        List<ObjectDefintion> objectDefList = new List<ObjectDefintion>();
        public PropertyWindow(Control sender, List<Control> controlList, List<ObjectDefintion> objectDefitionList)
        {
            InitializeComponent();
            control = sender;
            foreach (var item in controlList)
            {
                if (item != null)
                    objectList.Add(item);
            }

        //    objectDefList = objectDefitionList;
            this.Loaded += new RoutedEventHandler(Page_Loaded);

            GetObjects();
        }

        void Page_Loaded(object sender, RoutedEventArgs e)
        {

            control.IsEnabled = true;
            this.propertyGrid.SelectedObject = control;
            LoadObjects();
        }

        private void GetObjects()
        {

            LoadOperation<ObjectDefintion> loadOp = context.Load(context.GetObjectDefintionsQuery().Where(o => o.ObjectForm_ID == DesignForm.formID && o.IsDeleted == false), CallbackObject, null);


        }




        private void CallbackObject(LoadOperation<ObjectDefintion> results)
        {


            if (results != null)
            {
                objectDefList = results.Entities.ToList();

               
            }
            
        }

        private void LoadObjects()
        {
            foreach (var item in objectList)
            {
                if (item != null)
                    nameList.Add(new ControlName { Name = item.Name });
            }
            lstObjects.DisplayMemberPath = "Name";
            lstObjects.SelectedValuePath = "Name";
            lstObjects.ItemsSource = nameList;
            lstObjects.SelectedValue = control.Name;
            lstObjects.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(lstObjects_SelectionChanged);
        }

        void lstObjects_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ControlName contrlName = lstObjects.SelectedItem as ControlName;
            if (contrlName != null)
            {
                Control contrl = objectList.Where(c => c.Name == contrlName.Name).FirstOrDefault();
                if (contrl != null)
                {
                    contrl.IsEnabled = true;
                    this.propertyGrid.SelectedObject = contrl;
                    control = contrl;
                    control.IsEnabled = true;
                }
            }
        }


        private void test_Click(object sender, RoutedEventArgs e)
        {
            if (this.propertyGrid.SelectedObject == this.person)
            {
                try
                {
                    this.propertyGrid.SelectedObject = control;
                }
                catch
                {
                }
            }
            else
            {
                this.propertyGrid.SelectedObject = this.person;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {

           
                    SaveChanges();
                    // string xmlString = ControlManager.CreateControlXmlString(control, "");
               
           

        }



        private void SaveChanges()
        {
            string xmlString = "";
            string objectType;
            Control newControl;
            Control compareControl;
            control.IsEnabled = true;
          
                    int id = Convert.ToInt32(control.Tag.ToString());
                    ObjectDefintion obj = objectDefList.Where(o => o.ID == id).FirstOrDefault();
                   


                    if (obj.ObjectType == "TextBlock")
                    {

                        objectType = "Label";
                        System.Windows.Controls.Label newlabel = new System.Windows.Controls.Label();
                        xmlString = ControlManager.CreateControlXmlString(control, newlabel, objectType);


                    }
                    if (obj.ObjectType == "TextBox")
                    {
                        objectType = "TextBox";
                        TextBox newTxtbox = new TextBox();
                        xmlString = ControlManager.CreateControlXmlString(control, newTxtbox, objectType);


                    }

                    if (obj.ObjectType == "RadButton")
                    {

                        objectType = "RadButton";
                        RadButton newButton = new RadButton();
                        xmlString = ControlManager.CreateControlXmlString(control, newButton, objectType);
                    }
                    if (obj.ObjectType == "RadComboBox")
                    {

                        objectType = "RadComboBox";
                        RadComboBox radComboBox = new RadComboBox();
                        xmlString = ControlManager.CreateControlXmlString(control, radComboBox, objectType);
                    }

                    if (obj.ObjectType == "RadGridView")
                    {
                        objectType = "RadGridView";
                        RadGridView newGrid = new RadGridView();
                        xmlString = ControlManager.CreateControlXmlString(control, newGrid, objectType);
                    }

                    if (obj.ObjectType == "RadCartesianChart")
                    {
                        objectType = "RadCartesianChart";
                        RadCartesianChart radCartesianChart = new RadCartesianChart();
                        xmlString = ControlManager.CreateControlXmlString(control, radCartesianChart, objectType);
                    }

                    if (obj.ObjectType == "SerialChart")
                    {
                        xmlString = "";
                        objectType = "SerialChart";
                        SerialChart radCartesianChart = new SerialChart();
                        xmlString += ChartControlManager.CreateControlXmlString(control, radCartesianChart, objectType);
                       
                       
                    }

                    ObjectProperty prop = obj.ObjectProperties.FirstOrDefault();
                    
                    prop.XmlString = xmlString;
                    context.SubmitChanges(sub =>
                    {
                        if (sub.HasError) { RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = sub.Error.Message }); }
                        else
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                            this.Close();
                        }
                    }, null);


                }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();// = false;
        }






            
        }
   
        public class ControlName
        {
            public string Name { get; set; }
        }
    
}
