﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Telerik.Windows.Controls;
using Telerik.Windows;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using SilverlightMessageBox;
using System.Collections.ObjectModel;
using System.Windows.Controls.Primitives;
using System.Collections;
using BMA.MiddlewareApp.Controls;
using System.Windows.Markup;
using System.Xml.Linq;
using System.Reflection;
using System.IO;
using AmCharts.Windows.QuickCharts;
using SLPropertyGrid;

namespace BMA.MiddlewareApp.Admin
{
    public partial class DesignForm : Page
    {
        public static int formID;

        public static string formName;
        EditorContext context = new EditorContext();
        List<ObjectDefintion> objectList = new List<ObjectDefintion>();
        ObservableCollection<ControlName> nameList = new ObservableCollection<ControlName>();
        private bool setFunction = false;
        List<Control> formControlList = new List<Control>();
        List<ObjectFunctionParameter> parameterList = new List<ObjectFunctionParameter>();
        string connString;
        string databaseName;
        private CustomCursor customcursor;

        double _mouseOrigX;
        double _mouseOrigY;
        bool _mouseMove = false;
        string _mouseResizeEdge = "";
        bool resize = false;
        public DesignForm()
        {
            InitializeComponent();
            AdminMain admin = new AdminMain();
           // admin.NavContent.Header = formName;
           
            // this.form.MouseRightButtonDown += new MouseButtonEventHandler(form_MouseRightButtonDown);
            // this.form.MouseRightButtonUp += new MouseButtonEventHandler(form_MouseRightButtonUp);
            GetObjects();
          
            //customcursor = new CustomCursor();
            //customcursor.SetCursor("cursor.png");

            //this.form.Children.Add(customcursor);
            // this.form.MouseMove += new MouseEventHandler(newGrid_MouseMove);
            // NavigationContext
         // this.form.MouseLeftButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
        //  this.form.MouseLeftButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
      LayoutMain.MouseLeftButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
         LayoutMain.MouseLeftButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);

            //this.controlSet.MouseLeftButtonDown += new MouseButtonEventHandler(controlSet_MouseLeftButtonDown);

            //this.controlSet.MouseLeftButtonUp += new MouseButtonEventHandler(controlSet_MouseLeftButtonUp);
            //this.controlSet.MouseMove += new MouseEventHandler(controlSet_MouseMove);
        }

        void controlSet_MouseMove(object sender, MouseEventArgs e)
        {
           
                Canvas.SetLeft(dropRect, e.GetPosition(this).X - x);
                Canvas.SetTop(dropRect, e.GetPosition(this).Y - y);

            
        }
        int controlCount =1;

        void controlSet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {


            RadGridView newGrid = new RadGridView();

            newGrid.Width = 500;
            newGrid.Height = 300;
            newGrid.Name = "grid" + controlCount.ToString();
            newGrid.ShowColumnFooters = true;
            newGrid.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
            newGrid.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
            newGrid.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
            newGrid.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
            newGrid.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);

            newGrid.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
            form.Children.Add(newGrid);
            Canvas.SetLeft(rec, x );
            Canvas.SetTop(rec, y);
            controlCount += 1;
            dropRect.ReleaseMouseCapture();
            LayoutMain.Children.Remove(dropRect);
        }


        Rectangle dropRect = new Rectangle();
        void controlSet_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            currentObject = sender;
            createMode = true;
            Point position = e.GetPosition(null);
            x = position.X;
            y = position.Y;
            e.Handled = true;
            dropRect = new Rectangle();
            dropRect.Height = 30;
            dropRect.Width = 100;
            dropRect.Fill = new SolidColorBrush(Colors.LightGray);
            dropRect.Stroke = new SolidColorBrush(Colors.Black);
            dropRect.StrokeThickness = 2;
            dropRect.Margin = new Thickness(
                left: 15,
                top: 50,
                right: 0,
                bottom: 0);
            dropRect.Name = "DummyRec";

            //stkMenu.Children.Add(dropRect);
            Canvas.SetLeft(dropRect, x);
            Canvas.SetTop(dropRect, y);
            dropRect.CaptureMouse();
         

           
        }

        bool designMode = true;
        bool isForm = true;
        void form_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {

            Canvas can = sender as Canvas;
            if (!isForm)
                return;
            string header = "";
            if (designMode)
                header = "Design Mode";
            else
                header = "Test Mode";

            cMenu = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = header;
            menuItem.Click += new RadRoutedEventHandler(form_Click);
            cMenu.Items.Add(menuItem);






            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(form_Click);
            cMenu.Items.Add(menuItem);
            cMenu.PlacementTarget = LayoutMain;

            Point p = e.GetPosition(this.LayoutMain);


            cMenu.Placement = Telerik.Windows.Controls.PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu.HorizontalOffset = p.X;
            cMenu.VerticalOffset = p.Y;


            cMenu.IsOpen = true;
        }


        void form_Click(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();
            string test = x.ToString() + "  " + y.ToString();
            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Design Mode":
                        designMode = false;
                        break;
                    case "Test Mode":

                        designMode = true;
                        break;





                    default:
                        break;
                }

            }
        }
        void form_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            //  isForm = true;
            e.Handled = true;
        }


        private void GetObjects()
        {

            LoadOperation<ObjectDefintion> loadOp = context.Load(context.GetObjectDefintionsQuery().Where(o => o.ObjectForm_ID == formID && o.IsDeleted == false), CallbackObject, null);


        }




        private void CallbackObject(LoadOperation<ObjectDefintion> results)
        {


            if (results != null)
            {
                objectList = results.Entities.ToList();

                foreach (var control in results.Entities)
                {

                    DynamicallyDrawControl(control);
                }



            }
            LoadObjects();
            setFunction = true;
        }


        private void LoadObjects()
        {
            nameList = new ObservableCollection<ControlName>();
            formControlList = new List<Control>();
            var items = from el in form.Children

                        select el;
            foreach (var item in items)
            {
                try
                {
                    Control control = item as Control;
                    formControlList.Add(control);
                }
                catch
                {
                }

            }

            foreach (var item in formControlList)
            {
                if (item != null)
                    nameList.Add(new ControlName { Name = item.Name });
            }
            lstObjects.DisplayMemberPath = "Name";
            lstObjects.SelectedValuePath = "Name";
            lstObjects.ItemsSource = nameList;
            //lstObjects.SelectedValue = control.Name;
           lstObjects.SelectionChanged += new Telerik.Windows.Controls.SelectionChangedEventHandler(lstObjects_SelectionChanged);
        }
        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        public void MyFavoriteTimer_Completed(object sender, System.Windows.Input.MouseEventArgs args)
        {
            // If I get here it all worked
        }

        object currentObject = null;
        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            currentObject = sender;
            createMode = true;
            Point position = e.GetPosition(null);
            x = position.X;
            y = position.Y;
            e.Handled = true;
        }
        RadContextMenu cMenu;
        RadTabItem selectedTab;
        double x = 0;
        double y = 0;
        double x2 = 0;
        double y2 = 0;

        bool createMode = true;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {

            if (createMode)
            {

                Point position = e.GetPosition(null);
                x2 = position.X;
                y2 = position.Y;

                createControl();

            }
        }

        object currentControl = null;

        void control_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            currentObject = sender;

            Point position = e.GetPosition(null);
            x2 = position.X;
            y2 = position.Y;


            cMenu = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            try
            {
                RadButton button = (RadButton)sender;
                if (button != null)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Header = "Edit Function";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    cMenu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Edit Timer";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    cMenu.Items.Add(menuItem);
                   
                }

            }
            catch { }

            try
            {
                RadComboBox comboBox = (RadComboBox)sender;
                if (comboBox != null)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Header = "Edit DataSource";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    cMenu.Items.Add(menuItem);

                   

                }

            }
            catch { }

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);



            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.PlacementTarget = LayoutMain;

            Point p = e.GetPosition(this.LayoutMain);


            cMenu.Placement = Telerik.Windows.Controls.PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu.HorizontalOffset = p.X;
            cMenu.VerticalOffset = p.Y;


            cMenu.IsOpen = true;
            cMenu.Closed += new RoutedEventHandler(cMenu_Closed);
            // cMenu.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            //  RadContextMenu.SetContextMenu(this.radGridView1, cMenu);
            //cMenu.IsOpen = true;
            //  cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X + 350;
            //  cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }

        void cMenu_Closed(object sender, RoutedEventArgs e)
        {
            isForm = true;
        }




        void menuItem_Click(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();
            string test = x.ToString() + "  " + y.ToString();
            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Delete":
                        deleteControl();
                        break;
                    case "Edit":
                        Control control = currentObject as Control;
                        editControl(control);
                        break;

                    case "Edit Function":
                        Control controledit = currentObject as Control;
                        editFunction(Convert.ToInt32(controledit.Tag));
                        break;

                    case "Edit Timer":
                        Control controledittimer = currentObject as Control;
                        editTimer(Convert.ToInt32(controledittimer.Tag));
                        break;


                    case "Edit DataSource":
                        Control controleditDropdownDataSource = currentObject as Control;
                        editDropdownDataSource(Convert.ToInt32(controleditDropdownDataSource.Tag));
                        break;

                    default:
                        break;
                }

            }
        }

        private void editControl(Control sender)
        {
            List<Control> controlList = new List<Control>();
            var items = from el in form.Children

                        select el;
            foreach (var item in items)
            {
                try
                {
                    Control control = item as Control;
                    controlList.Add(control);
                }
                catch
                {
                }

            }


            PropertyWindow window =new PropertyWindow(sender, controlList, objectList);

            window.Left = (Application.Current.Host.Content.ActualWidth - window.ActualWidth) / 2;
            window.Top = (Application.Current.Host.Content.ActualHeight - window.ActualHeight) / 2;
            window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            window.ShowDialog();
           
        }


        private void CompareControls(Control control)
        {

            // control.
        }

        private void editFunction(int ID)
        {
            EditControlFunction window = new EditControlFunction(ID, formID);

            window.Left = (Application.Current.Host.Content.ActualWidth - window.ActualWidth) / 2;
            window.Top = (Application.Current.Host.Content.ActualHeight - window.ActualHeight) / 2;
            window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            window.ShowDialog();
           
        }

        private void editTimer(int ID)
        {
            EditControlFunction window = new EditControlFunction(ID, formID);

            window.Left = (Application.Current.Host.Content.ActualWidth - window.ActualWidth) / 2;
            window.Top = (Application.Current.Host.Content.ActualHeight - window.ActualHeight) / 2;
            window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            window.ShowDialog();
            
        }

        private void editDropdownDataSource(int ID)
        {

            EditDropdownDataSource window = new EditDropdownDataSource(ID, formID);


            window.Left = (Application.Current.Host.Content.ActualWidth - window.ActualWidth) / 2;
            window.Top = (Application.Current.Host.Content.ActualHeight - window.ActualHeight) / 2;
            window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            window.ShowDialog();
        }

        private void deleteControl()
        {
            int id = 0;

            string nameStr = currentControl.ToString();



            var element = currentControl as UIElement;

            string confirmText = "Do you want to delete control?";
            RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed));
        }



        

        private void OnConfirmClosed(object sender, WindowClosedEventArgs e)
        {

            if (e.DialogResult == true)
            {
                int id = 0;

                string nameStr = currentControl.ToString();



                var element = currentControl as UIElement;

                if (nameStr == "System.Windows.Controls.TextBlock")
                {
                    TextBlock textblock = currentControl as TextBlock;
                    id = Convert.ToInt32(textblock.Tag);
                }

                else
                {
                    Control control = currentControl as Control;
                    id = Convert.ToInt32(control.Tag);
                }

                ObjectDefintion obj = new ObjectDefintion();

                obj = objectList.Where(o => o.ID == id).FirstOrDefault();


                obj.IsDeleted = true;


                context.SubmitChanges(submit =>
                {
                    if (!submit.HasError)
                    {
                        if (nameStr == "System.Windows.Controls.TextBlock")
                        {
                            TextBlock textblock = currentControl as TextBlock;
                            form.Children.Remove(textblock);

                        }
                        else
                        {
                            var contrl = currentControl as Control;

                            form.Children.Remove(contrl);
                        }
                    }
                }, null);
            }
        }
        Rectangle rec = new Rectangle();


        private void createControl()
        {

            double width = x2 - x;
            double height = y2 - y;
            if (width < 11 || height < 6)
                return;

            rec = new Rectangle();
            rec.Fill = new SolidColorBrush(Colors.LightGray);
            rec.Width = width;
            rec.Height = height;
            rec.Name = "paceholderRec";
            form.Children.Add(rec);
            Canvas.SetLeft(rec, x - 160);
            Canvas.SetTop(rec, y - 35);
            SetControlProperties setProperties = new SetControlProperties(formID, x, y, height, width);
            //setProperties.OverlayOpacity = 0.5;
         //   setProperties.OverlayBrush = new SolidColorBrush(Colors.Transparent);
            setProperties.ObjectDefinitionCreated += (s, ev) =>
            {
                DynamicallyDrawControl(ev.CreatedObject);
                objectList.Add(ev.CreatedObject);
                setProperties.Close();
                LoadObjects();
            };

            setProperties.ObjectDefinitionCanceled += (s, ev) => { form.Children.Remove(rec); };
            setProperties.Closed += setProperties_Closed;
            setProperties.Left = (Application.Current.Host.Content.ActualWidth - setProperties.ActualWidth) / 2;
            setProperties.Top = (Application.Current.Host.Content.ActualHeight - setProperties.ActualHeight) / 2;
            setProperties.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            setProperties.ShowDialog();
        }

        void setProperties_Closed(object sender, WindowClosedEventArgs e)
        {
            try
            {
                form.Children.Remove(rec);
                //   Button ButtoninXAML;
                //   //System.IO.Stream s = this.GetType().Assembly.GetManifestResourceStream("YourNamespace.YourControl.xaml");
                //  // implementationRoot = this.Des(new System.IO.StreamReader(s).ReadToEnd());
                //  // FrameworkElement rootObject = XamlReader.Load(mysr.BaseStream) as FrameworkElement;
                //  // ButtoninXAML = LogicalTreeHelper.FindLogicalNode(rootObject, "btnTest") as Button;
                //  // ButtoninXAML.Click += new RoutedEventHandler(Button_Click1); 
                // //  Canvas rootCanvas = (Canvas)implementationRoot;
                //   //Rectangle r = (Rectangle)XamlReader.Load("<Rectangle Width='50' Height='20' Fill='#FF000000' />");
                //   //form.Children.Add(r);

                //   //Canvas.SetLeft(r, 250);
                //   //Canvas.SetTop(r,  160);

                //   string text = @"<Button Content='test' xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' />";

                //   // Convert to stream
                //   // You can also just stream the xaml from a file, using a FileStream
                ////   MemoryStream stream = new MemoryStream(ASCIIEncoding.UTF8.GetBytes(text));

                //   // Convert to object
                //   Button block = (Button)System.Windows.Markup.XamlReader.Load(text);
                //   block.Click += new RoutedEventHandler(Button_Click);
                //   form.Children.Add(block);
                //  // form.Children.Remove(rec);
                //   Canvas.SetLeft(block, 350);
                //   Canvas.SetTop(block, 160);

            }
            catch
            {
            }
        }



        private void DynamicallyDrawControl(ObjectDefintion control)
        {
            try
            {
                form.Children.Remove(rec);

                double w = control.SizeWidth.Value;
                double h = control.SizeHeight.Value;
                if (h < 5)
                    h = 23;


                string xamlstring = control.ObjectProperties.FirstOrDefault().XmlString;



                switch (control.ObjectType)
                {
                    case "TextBlock":
                        //  TextBlock newControl = new TextBlock();

                        System.Windows.Controls.Label newControl = (System.Windows.Controls.Label)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //if (w > 3)
                        //    newControl.Width = w;
                        //newControl.Height = h;
                        // newControl.Text = control.Text;
                        newControl.Tag = control.ID;
                        newControl.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        newControl.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        newControl.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        newControl.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        newControl.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);

                        // newControl.MouseMove += new MouseEventHandler(newControl_MouseMove);
                        //newControl.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        newControl.Cursor = Cursors.Hand;

                        form.Children.Add(newControl);
                        Canvas.SetLeft(newControl, control.PositionX - 160);
                        Canvas.SetTop(newControl, control.PositionY - 35);


                        break;
                    //0730937773
                    case "Label":
                        System.Windows.Controls.Label newLabel = new System.Windows.Controls.Label();
                        if (w > 3)
                            newLabel.Width = w;
                        newLabel.Height = h;
                        newLabel.Content = control.Text;

                        newLabel.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        newLabel.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        newLabel.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);

                        form.Children.Add(newLabel);
                        Canvas.SetLeft(newLabel, control.PositionX - 160);
                        Canvas.SetTop(newLabel, control.PositionY - 35);
                        break;

                    case "TextBox":
                        //TextBox newTextBox = new TextBox();
                        TextBox newTextBox = (TextBox)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //if (w > 3)
                        //    newTextBox.Width = w;
                        //newTextBox.Height = h;
                        //newTextBox.Text = control.Text;
                        newTextBox.Tag = control.ID;
                        newTextBox.Name = control.ObjectName + control.ID.ToString();
                        newTextBox.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        newTextBox.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        newTextBox.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        newTextBox.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        newTextBox.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);
                        newTextBox.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown), true);
                        newTextBox.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp), true);
                        form.Children.Add(newTextBox);
                        Canvas.SetLeft(newTextBox, control.PositionX - 160);
                        Canvas.SetTop(newTextBox, control.PositionY - 35);
                        break;

                    case "RadButton":
                        // RadButton newRadButton = new RadButton();

                        // Convert to object

                        RadButton newRadButton = (RadButton)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //  newRadButton.Click += new RoutedEventHandler(Button_Click);
                        //  form.Children.Add(block);
                        //form.Children.Remove(rec);
                        // Canvas.SetLeft(block, 350);
                        //Canvas.SetTop(block, 160);
                        // if (w > 3)
                        //  newRadButton.Width = w;
                        // newRadButton.Height = h;
                        // newRadButton.Content = control.Text;
                        newRadButton.Tag = control.ID.ToString();
                        newRadButton.Cursor = Cursors.Hand;
                        newRadButton.Name = control.ObjectName + control.ID.ToString();
                        // newRadButton.IsEnabled = false;
                        newRadButton.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown), true);
                        newRadButton.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp), true);
                        // newRadButton.MouseEnter += new MouseEventHandler(newRadButton_MouseEnter);
                        //newRadButton.Click += new RoutedEventHandler(newRadButton_Click);
                        newRadButton.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        newRadButton.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        newRadButton.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        newRadButton.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        newRadButton.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);
                        newRadButton.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        form.Children.Add(newRadButton);
                        Canvas.SetLeft(newRadButton, control.PositionX - 160);
                        Canvas.SetTop(newRadButton, control.PositionY - 35);
                        if (setFunction)
                        {
                            SetControlFunction.ID = control.ID;
                            SetControlFunction function = new SetControlFunction();

                           function.Show();
                        }
                        break;

                    case "RadComboBox":
                        // RadButton newRadButton = new RadButton();

                        // Convert to object

                        RadComboBox radComboBox = (RadComboBox)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //  newRadButton.Click += new RoutedEventHandler(Button_Click);
                        //  form.Children.Add(block);
                        //form.Children.Remove(rec);
                        // Canvas.SetLeft(block, 350);
                        //Canvas.SetTop(block, 160);
                        // if (w > 3)
                        //  newRadButton.Width = w;
                        // newRadButton.Height = h;
                        // newRadButton.Content = control.Text;
                        radComboBox.Tag = control.ID.ToString();
                        radComboBox.Cursor = Cursors.Hand;
                        radComboBox.Name = control.ObjectName + control.ID.ToString();
                        // newRadButton.IsEnabled = false;
                        radComboBox.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown), true);
                        radComboBox.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp), true);
                        // newRadButton.MouseEnter += new MouseEventHandler(newRadButton_MouseEnter);
                        //newRadButton.Click += new RoutedEventHandler(newRadButton_Click);
                        radComboBox.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        radComboBox.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        radComboBox.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        radComboBox.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        radComboBox.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);
                        radComboBox.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        form.Children.Add(radComboBox);
                        Canvas.SetLeft(radComboBox, control.PositionX - 160);
                        Canvas.SetTop(radComboBox, control.PositionY - 35);
                        if (setFunction)
                        {
                            SetDropdownDataSource.ID = control.ID;
                            SetDropdownDataSource function = new SetDropdownDataSource();

                            function.Show();
                        }
                        break;

                    case "RadDatePicker":
                        // RadButton newRadButton = new RadButton();

                        // Convert to object

                        RadDatePicker radDatePicker = (RadDatePicker)System.Windows.Markup.XamlReader.Load(xamlstring);
                        //  newRadButton.Click += new RoutedEventHandler(Button_Click);
                        //  form.Children.Add(block);
                        //form.Children.Remove(rec);
                        // Canvas.SetLeft(block, 350);
                        //Canvas.SetTop(block, 160);
                        // if (w > 3)
                        //  newRadButton.Width = w;
                        // newRadButton.Height = h;
                        // newRadButton.Content = control.Text;
                        radDatePicker.Tag = control.ID.ToString();
                        radDatePicker.Cursor = Cursors.Hand;
                        radDatePicker.Name = control.ObjectName + control.ID.ToString();
                        // newRadButton.IsEnabled = false;
                        radDatePicker.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown), true);
                        radDatePicker.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp), true);
                        // newRadButton.MouseEnter += new MouseEventHandler(newRadButton_MouseEnter);
                        //newRadButton.Click += new RoutedEventHandler(newRadButton_Click);
                        radDatePicker.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        radDatePicker.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        radDatePicker.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        radDatePicker.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        radDatePicker.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);
                        radDatePicker.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        form.Children.Add(radDatePicker);
                        Canvas.SetLeft(radDatePicker, control.PositionX - 160);
                        Canvas.SetTop(radDatePicker, control.PositionY - 35);
                       
                        break;
                    case "RadGridView"://DataGrid
                        //  RadGridView newGrid = new RadGridView();
                        // string xamlstring = control.ObjectProperties.FirstOrDefault().XmlString;
                        // Convert to object

                        RadGridView newGrid = (RadGridView)System.Windows.Markup.XamlReader.Load(xamlstring);
                        if (h > 250)
                        {
                            newGrid.MaxHeight = 500;
                        }

                        if (h < 250)
                        {
                            newGrid.MinHeight = 250;
                        }
                        newGrid.Height = h;
                        newGrid.Name = control.ObjectName + control.ID.ToString();
                        newGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                        newGrid.Tag = control.ID;
                        newGrid.MaxWidth = form.ActualWidth - 40;
                        newGrid.ShowColumnFooters = true;
                        newGrid.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        newGrid.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        newGrid.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        newGrid.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        newGrid.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);

                        newGrid.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        newGrid.Cursor = Cursors.Hand;
                        form.Children.Add(newGrid);
                        Canvas.SetLeft(newGrid, control.PositionX - 160);
                        Canvas.SetTop(newGrid, control.PositionY - 35);
                        break;





                    case "RadCartesianChart":

                        RadCartesianChart newChart = (RadCartesianChart)System.Windows.Markup.XamlReader.Load(xamlstring);
                        if (h > 250)
                        {
                            newChart.MaxHeight = 500;
                        }

                        if (h < 250)
                        {
                            newChart.MinHeight = 250;
                        }
                        newChart.Height = h;
                        newChart.Name = control.ObjectName + control.ID.ToString();
                        newChart.HorizontalAlignment = HorizontalAlignment.Stretch;
                        newChart.Tag = control.ID;
                        newChart.MaxWidth = form.ActualWidth - 40;

                        newChart.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        newChart.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        newChart.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        newChart.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        newChart.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);

                        newChart.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        newChart.Cursor = Cursors.Hand;
                        form.Children.Add(newChart);
                        Canvas.SetLeft(newChart, control.PositionX - 160);
                        Canvas.SetTop(newChart, control.PositionY - 35);
                        break;



                    case "SerialChart":


                        


                         SerialChart seriesChart = (SerialChart)System.Windows.Markup.XamlReader.Load(xamlstring);
                        if (h > 250)
                        {
                         //   seriesChart.MaxHeight = 500;
                        }

                        if (h < 250)
                        {
                            seriesChart.MinHeight = 250;
                        }
                        //seriesChart.Height = 500;
                        seriesChart.Name = control.ObjectName + control.ID.ToString();
                        seriesChart.HorizontalAlignment = HorizontalAlignment.Stretch;
                        seriesChart.Tag = control.ID;
                        seriesChart.MaxWidth = form.ActualWidth - 40;

                        seriesChart.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        seriesChart.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        seriesChart.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        seriesChart.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        seriesChart.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);

                        seriesChart.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        seriesChart.Cursor = Cursors.Hand;
                        form.Children.Add(seriesChart);
                        Canvas.SetLeft(seriesChart, control.PositionX - 160);
                        Canvas.SetTop(seriesChart, control.PositionY - 35);
                        break;


                    case "PieChart":


                        PieChart pieChart = (PieChart)System.Windows.Markup.XamlReader.Load(xamlstring);
                        if (h > 250)
                        {
                            //   seriesChart.MaxHeight = 500;
                        }

                        if (h < 250)
                        {
                            pieChart.MinHeight = 250;
                        }
                        //seriesChart.Height = 500;
                        pieChart.Name = control.ObjectName + control.ID.ToString();
                        pieChart.HorizontalAlignment = HorizontalAlignment.Stretch;
                        pieChart.Tag = control.ID;
                        pieChart.MaxWidth = form.ActualWidth - 40;

                        pieChart.MouseLeftButtonDown += new MouseButtonEventHandler(RectTitle_MouseLeftButtonDown);
                        pieChart.MouseMove += new MouseEventHandler(RectTitle_MouseMove);
                        pieChart.MouseLeftButtonUp += new MouseButtonEventHandler(RectTitle_MouseLeftButtonUp);
                        pieChart.MouseRightButtonDown += new MouseButtonEventHandler(newControl_MouseRightButtonDown);
                        pieChart.MouseRightButtonUp += new MouseButtonEventHandler(control_MouseRightButtonUp);

                        pieChart.MouseLeave += new MouseEventHandler(newControl_MouseLeave);
                        pieChart.Cursor = Cursors.Hand;
                        form.Children.Add(pieChart);
                        Canvas.SetLeft(pieChart, control.PositionX - 160);
                        Canvas.SetTop(pieChart, control.PositionY - 35);
                        break;






                }
            }
            catch
            {
                //Message.ErrorMessage("Failed to create " + control.ObjectType + "on runtime");
            }
        }


        private void ApplyProperties(string xmlString, Control control)
        {
            XDocument xd = XDocument.Parse(xmlString);
            Type t = control.GetType();
            PropertyInfo[] props = t.GetProperties();
            foreach (var item in xd.Elements("Properties"))
            {
                string name = item.Attribute("name").Value;
                var value = item.Attribute("value").Value;
                string dataType = item.Attribute("value").Value;
            }


        }
        void newRadButton_MouseEnter(object sender, MouseEventArgs e)
        {
            createMode = false;
            UIElement control = sender as UIElement;
            control.CaptureMouse();
            beginX = e.GetPosition(control).X;

            beginY = e.GetPosition(control).Y;
            IsMouseCaptured = true;
        }

        void newGrid_MouseMove(object sender, MouseEventArgs e)
        {

            customcursor.MoveTo(e.GetPosition(null));
        }

        void newControl_MouseLeave(object sender, MouseEventArgs e)
        {
            IsMouseCaptured = false;
            Control control = sender as Control;
            control.ReleaseMouseCapture();
            control.Cursor = Cursors.Arrow;
        }

        void newControl_MouseMove(object sender, MouseEventArgs e)
        {
            Control control = sender as Control;
            control.Cursor = Cursors.SizeNESW;
        }

        void newControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (cMenu != null)
            {
                if (cMenu.IsOpen)
                    cMenu.IsOpen = false;
            }
            currentControl = sender;
            e.Handled = true;
            isForm = false;


        }



        private void newRadButton_Click(object sender, RoutedEventArgs e)
        {

            createMode = false;
            Control control = sender as Control;
            control.CaptureMouse();


            // control
            // control. = p.Y;
            // beginX = e.GetPosition(control).X;

            //    beginY = e.GetPosition(control).Y;
            IsMouseCaptured = true;

            //busyIndicator.IsBusy = true;
            //try
            //{
            //    Button btn = sender as Button;
            //    string txtTag = btn.Tag.ToString();

            //    GetObjectFuction(Convert.ToInt32(txtTag));

            //}
            //catch
            //{

            //}
            //busyIndicator.IsBusy = false;
        }

        private void GetObjectFuction(int ID)
        {

            LoadOperation<ObjectFunction> loadOp = context.Load(context.GetObjectFunctionsQuery().Where(o => o.ObjectDefinition_ID == ID), CallbackObjectFunction, null);


        }


        string displayControlName = "";
        string storeProcName = "";
        void CallbackObjectFunction(LoadOperation<ObjectFunction> results)
        {


            if (results != null)
            {
                ObjectFunction obj = results.Entities.FirstOrDefault();
                if (obj != null)
                {
                    storeProcName = obj.Query;
                    databaseName = obj.Server.DatabaseName;
                    connString = obj.Server.ConnectionString;
                    if (obj.TargetDisplayObject_ID.Value > 0)
                    {
                        displayControlName = objectList.Where(o => o.ID == obj.TargetDisplayObject_ID.Value).FirstOrDefault().ObjectName;
                        if (displayControlName != "")
                        {
                            displayControlName = displayControlName + obj.TargetDisplayObject_ID.Value.ToString();
                            if (obj.FunctionType == "Stored Procedure")
                            {
                                LoadOperation<ObjectFunctionParameter> loadOp = context.Load(context.GetObjectFunctionParametersQuery().Where(o => o.ObjectFunction_ID == obj.ID), CallbackObjectFunctionParameter, null);
                            }
                            else
                            {
                                GetData(storeProcName, 1, 100, "");
                            }
                        }
                        else
                        {
                            

                            string alertText = " display control not found, Please set DataGrid or listview for the function!";

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText });
                        }

                    }
                    else
                    {
                        
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "There no control set to display data!" });
                    }
                }
            }
        }

        void CallbackObjectFunctionParameter(LoadOperation<ObjectFunctionParameter> results)
        {


            if (results != null)
            {
                parameterList = new List<ObjectFunctionParameter>();
                parameterList = results.Entities.ToList();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                foreach (var item in parameterList)
                {
                    string name = objectList.Where(o => o.ID == item.ObjectDefinition_ID).FirstOrDefault().ObjectName;

                    TextBox textBox = FindControl<TextBox>((UIElement)form, typeof(TextBox), name + item.ObjectDefinition_ID);
                    parameters.Add(item.ParameterName + ";" + textBox.Text);
                }


                ExecuteStoreProcedure(storeProcName, parameters);
            }
        }




        void ExecuteStoreProcedure(string procName, ObservableCollection<string> parameters)
        {


            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted);
            ws.ExecuteStoreProcAsync(connString, databaseName, procName, parameters, 1, 25);

            ///.Progress.Start();
        }

        void ws_ExecuteStoreProcedureCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {

                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.Result.Tables.Count > 0)
                {
                    RadGridView dataGrid = FindControl<RadGridView>((UIElement)form, typeof(RadGridView), displayControlName);
                    if (dataGrid != null)
                    {
                        dataGrid.Columns.Clear();
                        dataGrid.ItemsSource = list;
                    }
                }
                else
                {
                   // Message.InfoMessage("Successfully executed!");

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successfully executed!" });
                }
            }
            //this.Progress.Stop();
        }


        private void GetData(string sql, int pagenumber, int pagesize, object userState)
        {
            //this.radGridView1.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {

                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                RadGridView dataGrid = FindControl<RadGridView>((UIElement)form, typeof(RadGridView), displayControlName);
                if (dataGrid != null)
                {
                    dataGrid.Columns.Clear();
                    dataGrid.ItemsSource = list;
                }

            }
            //this.radGridView1.IsBusy = false;
        }

        public Boolean IsMouseCaptured = false;

        double beginX;
        double beginY;

        private void RectTitle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            createMode = false;
            Control contrl = sender as Control;
            lstObjects.SelectedValue = contrl.Name;
            this.propertyGrid.SelectedObject = contrl;
            UIElement control = sender as UIElement;
            control.CaptureMouse();




            //   Canvas parent = (Canvas)this.Parent;
            var mousePosition = e.GetPosition(form);

            _mouseOrigX = mousePosition.X;
            _mouseOrigY = mousePosition.Y;

            ((FrameworkElement)sender).CaptureMouse();

            //  _mouseResizeEdge = GetEdgeMouseIsOver(sender, e);
            // if (_mouseResizeEdge == "")
            //  {
            //If the mouse is not over an edge, we drag the field
            _mouseMove = true;
            resize = false;
            beginX = e.GetPosition(control).X;

            beginY = e.GetPosition(control).Y;
            IsMouseCaptured = true;
            // }
            //else
            //{
            //    //If we're over an edge, we don't want to drag the box - we want to resize the object
            //    _mouseMove = false;
            //    IsMouseCaptured = true;
            //    resize = true;
            //}

        }





        private void RectTitle_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                createMode = false;

                UIElement control = sender as UIElement;




                if (_mouseMove)
                {
                    if (IsMouseCaptured)
                    {
                        Canvas.SetLeft(control, e.GetPosition(this).X - beginX);
                        Canvas.SetTop(control, e.GetPosition(this).Y - beginY);

                    }
                }
                //else if (resize)
                //{

                //    //if (_mouseResizeEdge != "")
                //    if (IsMouseCaptured)

                //        HandleResize(sender, e);
                //}
                //else
                //{
                //    //If the object is not in drag mode, show arrows if needed
                //    switch (GetEdgeMouseIsOver(control, e))
                //    {
                //        case "NW":
                //        case "SE":
                //            Cursor = Cursors.SizeNWSE;
                //            break;
                //        case "SW":
                //        case "NE":
                //            Cursor = Cursors.SizeNESW;
                //            break;
                //        case "N":
                //        case "S":
                //            Cursor = Cursors.SizeNS;
                //            break;
                //        case "E":
                //        case "W":
                //            Cursor = Cursors.SizeWE;
                //            break;

                //        case "":
                //            Cursor = Cursors.Hand;
                //            break;
                //        default:
                //            Cursor = Cursors.Arrow;
                //            break;
                //    }

                //}

            }
            catch
            {
            }
        }

        private void RectTitle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {


            UIElement control = sender as UIElement;

            control.ReleaseMouseCapture();
            ((FrameworkElement)sender).ReleaseMouseCapture();

            if (sender is Control)
            {
                var myControl = sender as Control;

                int id = Convert.ToInt32(myControl.Tag);
                var left = (double)myControl.GetValue(Canvas.LeftProperty);
                var top = (double)myControl.GetValue(Canvas.TopProperty);

                ChangePosition(id, left + 160, top + 60, (int)(myControl.ActualHeight), (int)(myControl.ActualWidth));
                if (resize)
                {

                }
            }
            resize = false;
            _mouseMove = false;
            IsMouseCaptured = false;

        }


        private void HandleResize(object sender, MouseEventArgs e)
        {


            var control = sender as Control;
            var mousePosition = e.GetPosition(form);

            double mouseChangeX = mousePosition.X - _mouseOrigX;
            double mouseChangeY = mousePosition.Y - _mouseOrigY;

            if (_mouseResizeEdge.Contains("N"))
            {
                var newY = e.GetPosition(this).Y;

                var newHeight = control.ActualHeight - mouseChangeY;
                if (newHeight >= 20)
                {
                    control.Height = newHeight;
                    // control.SetValue(Canvas.TopProperty, newY);
                    _mouseOrigY = mousePosition.Y;
                }
                else
                {
                    var diffTo20 = control.Height - 20;
                    _mouseOrigY = _mouseOrigY + diffTo20;
                    control.Height = 20;

                    newY = e.GetPosition(this).Y;
                    //control.SetValue(Canvas.TopProperty, newY);
                }
            }
            else if (_mouseResizeEdge.Contains("S"))
            {
                var newHeight = control.Height + mouseChangeY;
                if (newHeight >= 20)
                {
                    control.Height = newHeight;
                    _mouseOrigY = mousePosition.Y;
                }
                else
                {
                    var diffTo20 = control.Height - 20;
                    _mouseOrigY = _mouseOrigY - diffTo20;
                    control.Height = 20;
                }

            }

            if (_mouseResizeEdge.Contains("W"))
            {
                var newX = e.GetPosition(this).X;
                // control.SetValue(Canvas.LeftProperty, newX);
                var newWidth = control.ActualWidth - mouseChangeX;
                if (newWidth > 20)
                {
                    control.Width = newWidth;
                    _mouseOrigX = mousePosition.X;
                }
                else
                {
                    var diffTo20 = this.Width - 20;
                    _mouseOrigX = _mouseOrigX + diffTo20;
                    control.Width = 20;

                    newX = e.GetPosition(this).X;
                    //this.SetValue(Canvas.LeftProperty, newX);
                }

            }
            else if (_mouseResizeEdge.Contains("E"))
            {
                var newWidth = control.ActualWidth + mouseChangeX;
                if (newWidth > 20)
                {
                    control.Width = newWidth;
                    _mouseOrigX = mousePosition.X;
                }
                else
                {
                    var diffTo20 = control.Width - 20;
                    _mouseOrigX = _mouseOrigX - diffTo20;
                    control.Width = 20;
                }
            }
        }


        private string GetEdgeMouseIsOver(object element, MouseEventArgs e)
        {
            //If the object is not in drag mode, show arrows if needed
            try
            {
                var control = element as Control;

                GeneralTransform gt = control.TransformToVisual(form);
                Point p = gt.Transform(new Point(0, 0));
                var mouseInBorder = e.GetPosition(form);
                bool northRow =
                    ((mouseInBorder.Y < p.Y + 8));
                bool southRow =
                    ((mouseInBorder.Y > (p.Y + control.ActualHeight - 8)) && (mouseInBorder.Y < (p.Y + control.ActualHeight)));
                bool westCol =
                    ((mouseInBorder.X < p.X + 8));
                bool eastCol =
                    ((mouseInBorder.X > (p.X + control.ActualWidth - 8)) && (mouseInBorder.X < (p.X + control.ActualWidth)));

                if (northRow && westCol)
                {
                    return "NW";
                }
                else if (northRow && eastCol)
                {
                    return "NE";
                }
                else if (southRow && westCol)
                {
                    return "SW";
                }
                else if (southRow && eastCol)
                {
                    return "SE";
                }
                else if (southRow)
                {
                    return "S";
                }
                else if (northRow)
                {
                    return "N";
                }
                else if (eastCol)
                {
                    return "E";
                }
                else if (westCol)
                {
                    return "W";
                }

                return "";
            }
            catch { return ""; }
        }



        private void ChangePosition(int id, double xValue, double yValue, int height, int width)
        {
            ObjectDefintion obj = objectList.Where(o => o.ID == id).FirstOrDefault();
            obj.PositionX = xValue;
            obj.PositionY = yValue;
            obj.SizeHeight = height;
            obj.SizeWidth = width;
            context.SubmitChanges();
        }





        public T FindControl<T>(UIElement parent, Type targetType, string ControlName) where T : FrameworkElement
        {

            if (parent == null) return null;

            if (parent.GetType() == targetType && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }
            T result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                if (FindControl<T>(child, targetType, ControlName) != null)
                {
                    result = FindControl<T>(child, targetType, ControlName);
                    break;
                }
            }
            return result;
        }

      

        void lstObjects_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            ControlName contrlName = lstObjects.SelectedItem as ControlName;
            if (contrlName != null)
            {
                Control contrl = formControlList.Where(c => c.Name == contrlName.Name).FirstOrDefault();
                if (contrl != null)
                {
                    contrl.IsEnabled = true;
                    this.propertyGrid.SelectedObject = contrl;
                   
                }
            }
        }


        private void btnSave_Click(object sender, RoutedEventArgs e)
        {


            SaveChanges();
            // string xmlString = ControlManager.CreateControlXmlString(control, "");



        }



        private void SaveChanges()
        {

            ControlName contrlName = lstObjects.SelectedItem as ControlName;
            if (contrlName != null)
            {
                Control control = formControlList.Where(c => c.Name == contrlName.Name).FirstOrDefault();
                if (control != null)
                {

                    string xmlString = "";
                    string objectType;
                    Control newControl;
                    Control compareControl;
                    control.IsEnabled = true;

                    int id = Convert.ToInt32(control.Tag.ToString());
                    ObjectDefintion obj = objectList.Where(o => o.ID == id).FirstOrDefault();



                    if (obj.ObjectType == "TextBlock")
                    {

                        objectType = "Label";
                        System.Windows.Controls.Label newlabel = new System.Windows.Controls.Label();
                        xmlString = ControlManager.CreateControlXmlString(control, newlabel, objectType);


                    }
                    if (obj.ObjectType == "TextBox")
                    {
                        objectType = "TextBox";
                        TextBox newTxtbox = new TextBox();
                        xmlString = ControlManager.CreateControlXmlString(control, newTxtbox, objectType);


                    }

                    if (obj.ObjectType == "RadButton")
                    {

                        objectType = "RadButton";
                        RadButton newButton = new RadButton();
                        xmlString = ControlManager.CreateControlXmlString(control, newButton, objectType);
                    }
                    if (obj.ObjectType == "RadComboBox")
                    {

                        objectType = "RadComboBox";
                        RadComboBox radComboBox = new RadComboBox();
                        xmlString = ControlManager.CreateControlXmlString(control, radComboBox, objectType);
                    }

                    if (obj.ObjectType == "RadDatePicker")
                    {

                        objectType = "RadDatePicker";
                        RadDatePicker radDatePicker = new RadDatePicker();
                        xmlString = ControlManager.CreateControlXmlString(control, radDatePicker, objectType);
                    }

                    if (obj.ObjectType == "RadGridView")
                    {
                        objectType = "RadGridView";
                        RadGridView newGrid = new RadGridView();
                        xmlString = ControlManager.CreateControlXmlString(control, newGrid, objectType);
                    }

                    if (obj.ObjectType == "RadCartesianChart")
                    {
                        objectType = "RadCartesianChart";
                        RadCartesianChart radCartesianChart = new RadCartesianChart();
                        xmlString = ControlManager.CreateControlXmlString(control, radCartesianChart, objectType);
                    }

                    if (obj.ObjectType == "SerialChart")
                    {
                        xmlString = "";
                        objectType = "SerialChart";
                        SerialChart radCartesianChart = new SerialChart();
                        xmlString += ChartControlManager.CreateControlXmlString(control, radCartesianChart, objectType);


                    }

                    ObjectProperty prop = obj.ObjectProperties.FirstOrDefault();

                    prop.XmlString = xmlString;
                    context.SubmitChanges(sub =>
                    {
                        if (sub.HasError) { Message.ErrorMessage(sub.Error.Message); }
                        else
                        {
                          
                         //   this.DialogResult = false;

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                        }
                    }, null);


                }
            }
        }
        //private void Save()
        //{
        //    EditorContext context = new EditorContext();
        //    ControlType control = ddlControlType.SelectedItem as ControlType;
        //    if (control == null)
        //        Message.ErrorMessage("Select object type!");
        //    else
        //    {
        //        if (XmlString == null)
        //        {
        //            XmlString = GetXaml();
        //        }
        //        ObjectDefintion obj = new ObjectDefintion();
        //        obj.ObjectForm_ID = formID;
        //        obj.ObjectName = txtName.Text.ToString();
        //        obj.ObjectType = control.Value;
        //        obj.PositionX = Convert.ToDouble(txtX.Text.ToString());
        //        obj.PositionY = Convert.ToDouble(txtY.Text.ToString());
        //        decimal hght = Convert.ToDecimal(txtHeight.Text.ToString());
        //        obj.SizeHeight = Convert.ToInt32(hght);
        //        decimal wdth = Convert.ToDecimal(txtWidth.Text.ToString());
        //        obj.SizeWidth = Convert.ToInt32(wdth);
        //        obj.Text = txtText.Text.ToString();
        //        obj.IsDeleted = false;
        //        if (control.Value == "RadButton")
        //        {
        //            if (control.Display == "Timer")
        //            {
        //                obj.UseTimer = 2;
        //                obj.Interval = Convert.ToInt32(tmrInterval.Value.Value);
        //            }
        //            else if (control.Display == "Button")
        //            {
        //                if (chkUseTimer.IsChecked.Value == true)
        //                {
        //                    obj.UseTimer = 1;
        //                    obj.Interval = Convert.ToInt32(tmrInterval.Value.Value);
        //                }
        //                else
        //                {
        //                    obj.UseTimer = 0;
        //                }
        //            }
        //        }
        //        context.ObjectDefintions.Add(obj);
        //        context.SubmitChanges(submit =>
        //        {
        //            if (!submit.HasError)
        //            {
        //                ObjectProperty property = new ObjectProperty();



        //                property.XmlString = XmlString;
        //                property.ObjectDefinition_ID = obj.ID;
        //                context.ObjectProperties.Add(property);
        //                context.SubmitChanges(sub =>
        //                {
        //                    if (!sub.HasError)
        //                    {
        //                        ObjectDefinitionCreated(this, new PropertiesCreatedEventArgs(obj));

        //                    }
        //                    else
        //                    { Message.ErrorMessage(sub.Error.Message); }
        //                },
        //                                                     null);



        //            }
        //            else
        //            { Message.ErrorMessage(submit.Error.Message); }
        //        },
        //                                                 null);

        //    }
        //}
    }

    public class DummyData
    {
        // MyShortDate

        public double Collections { get; set; }
        public double Deliveries { get; set; }
        public double Residual { get; set; }
        public double StockLevel { get; set; }

        public DateTime MyShortDate { get; set; }

    }
}
