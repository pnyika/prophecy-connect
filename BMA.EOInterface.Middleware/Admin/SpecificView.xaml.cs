﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls.GridView;
using SilverlightMessageBox;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SpecificView : Page
    {
        EditorContext context = new EditorContext();
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList = new List<ViewFieldSummary>();

        List<ViewField> viewfieldList = new List<ViewField>();
        List<Table> tableList = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary;
        private int viewID;
        private int tableID;
        public SpecificView()
        {
            InitializeComponent();
            radGridView1.MouseLeftButtonDown += new MouseButtonEventHandler(radGridView1_MouseLeftButtonDown);
            LoadOperation groupOperation = context.Load<StoredProcedureName>(context.GetStoredProcedureNamesQuery(), CallbackStoredProcedureNames, null);

            LoadOperation filterOperation = context.Load<FieldCompletionOption>(context.GetFieldCompletionOptionsQuery(), CallbackFieldOptionFilter, null);
        }
        void CallbackStoredProcedureNames(LoadOperation<StoredProcedureName> results)
        {
            if (results != null)
            {

                ((GridViewComboBoxColumn)this.radGridView1.Columns["StoredProcedureNames_ID"]).ItemsSource = results.Entities;
              
            }
        }


        void CallbackFieldOptionFilter(LoadOperation<FieldCompletionOption> results)
        {
            if (results != null)
            {
                List<FieldCompletionOption> resultsList = new List<FieldCompletionOption>();
                foreach (var item in  results.Entities)
                {
                    if (!resultsList.Any(f => string.Equals(f.Filter, item.Filter)))
                    {
                        resultsList.Add(item);
                    }
                   
                }

                ((GridViewComboBoxColumn)this.radGridView1.Columns["FieldOptionFilter"]).ItemsSource = resultsList;

            }
        }
        void radGridView1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                MessageBox.Show("2");
            }
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void radGridView_RowEditEnded(object sender, Telerik.Windows.Controls.GridViewRowEditEndedEventArgs e)
        {
            try
            {
              


            }
            catch
            {

            }
        }



       
        private void GetTables(object sender, RoutedEventArgs e)
        {
            viewID = Convert.ToInt32(NavigationContext.QueryString["viewID"]);
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViews, null);


        }
        private void RefreshTables()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViews, null);


        }


        private void CallbackViews(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {
                gdvTable.ItemsSource = loadOp.Entities;
                tableList = loadOp.Entities.ToList();


            }
        }



        private void GetViewFieldLoad(object sender, RoutedEventArgs e)
        {
            viewID = Convert.ToInt32(NavigationContext.QueryString["viewID"]);
            LoadOperation<ViewFieldSummary> loadOp = context.Load(context.GetViewFieldByViewIDQuery(viewID), CallbackViewFields, null);
            LoadOperation<ViewField> loadOperation = context.Load(context.GetViewFieldsQuery().Where(v => v.View_ID == viewID), CallbackViewField, null);

        }
        private void GetViewFields()
        {

            LoadOperation<ViewFieldSummary> loadOp = context.Load(context.GetViewFieldByViewIDQuery(viewID), CallbackViewFields, null);
            LoadOperation<ViewField> loadOperation = context.Load(context.GetViewFieldsQuery().Where(v=>v.View_ID == viewID), CallbackViewField, null);

        }
        private void CallbackViewField(LoadOperation<ViewField> loadOp)
        {
            if (loadOp != null)
            {
                viewfieldList = loadOp.Entities.ToList();
            }
        }


        private void CallbackViewFields(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                viewFieldSummary = new ObservableCollection<ViewFieldSummary>();
                foreach (ViewFieldSummary summary in loadOp.Entities.OrderBy(f=>f.FieldName))
                {
                    viewFieldSummary.Add(summary);

                }
                radGridView1.ItemsSource = viewFieldSummary;
                fieldList = loadOp.Entities.ToList();


            }
        }

        private void GetRelationships(object sender, RoutedEventArgs e)
        {
            viewID = Convert.ToInt32(NavigationContext.QueryString["viewID"]);
            LoadOperation<ViewRelationship> loadOp = context.Load(context.GetViewRelationshipByViewIDQuery(viewID), CallbackRelationships, null);


        }

        private void GetRelationshipsRefresh()
        {
            viewID = Convert.ToInt32(NavigationContext.QueryString["viewID"]);
            LoadOperation<ViewRelationship> loadOp = context.Load(context.GetViewRelationshipByViewIDQuery(viewID), CallbackRelationships, null);

        }

        private void CallbackRelationships(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {

                gdvRelationship.ItemsSource = loadOp.Entities;
                relationshipList = new List<ViewRelationship>();
                relationshipList = loadOp.Entities.ToList();


            }
        }


        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            ModifyRelationship modify = new ModifyRelationship(viewID, relationshipList);
            modify.Closed += ChildWin_Closed;
            modify.Closed += ViewChildWin_Closed;
            modify.Left = (Application.Current.Host.Content.ActualWidth - modify.ActualWidth) / 2;
            modify.Top = (Application.Current.Host.Content.ActualHeight - modify.ActualHeight) / 2;
            modify.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            modify.ShowDialog();
        }
        void ChildWin_Closed(object sender, EventArgs e)
        {
            GetRelationshipsRefresh();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

            ViewRelationship rship = gdvRelationship.SelectedItem as ViewRelationship;
            if (rship == null)
                return;

            string relation = rship.Summary;

            string confirmText = "Do you want to delete this relationship:\n " + relation + " ?";
            RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmRshipDeleteClosed));

           


        }

        private void OnConfirmRshipDeleteClosed(object sender, WindowClosedEventArgs e)
        {

            if (e.DialogResult == true)
            {
                ViewRelationship rship = gdvRelationship.SelectedItem as ViewRelationship;
                if (rship == null)
                    return;
                var ws = WCF.GetService();
                ws.DeleteViewTableRelationshipAsync(rship.ID);
                GetRelationshipsRefresh();
            }
        }

        private void btnModifyViewFields_Click(object sender, RoutedEventArgs e)
        {


            ModifyViewField modifyViewField = new ModifyViewField(viewID, fieldList);
            modifyViewField.Closed += ViewChildWin_Closed;
            modifyViewField.Left = (Application.Current.Host.Content.ActualWidth - modifyViewField.ActualWidth) / 2;
            modifyViewField.Top = (Application.Current.Host.Content.ActualHeight - modifyViewField.ActualHeight) / 2;
            modifyViewField.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            modifyViewField.ShowDialog();
        }
        void ViewChildWin_Closed(object sender, WindowClosedEventArgs e)
        {
            GetViewFields();
        }

        private void btnDeleteViewField_Click(object sender, RoutedEventArgs e)
        {
            var items = radGridView1.SelectedItems;

            if (items == null)
                return;

            string confirmText = "Do you want to delete  " + items.Count + " item(s)?";
            RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed));

          
        }

        private void OnConfirmClosed(object sender, WindowClosedEventArgs e)
        {
           
            if (e.DialogResult == true)
            {
                var items = radGridView1.SelectedItems;

                if (items == null)
                    return;
                foreach (var item in items)
                {
                    ViewFieldSummary viewField = item as ViewFieldSummary;
                    var viewfld = viewfieldList.Where(v => v.ID == viewField.ID).FirstOrDefault();
                    if (viewfld != null)
                        context.ViewFields.Remove(viewfld);

                }
                context.SubmitChanges(submit =>
                {
                    if (!submit.HasError)
                    {
                        GetViewFields();

                    }
                    else RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An error while removing View Fields" }); 
                       
                }, null);
            }
        }

        void ws_DeleteViewFieldCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.DeleteViewFieldCompletedEventArgs e)
        {
            viewFieldSummary = null;
            GetViewFields();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            viewFieldSummary = null;
            GetViewFields();
        }

        private void btnTable_Click(object sender, RoutedEventArgs e)
        {
            AddTable table = new AddTable(viewID, tableList);
            table.Closed += Table_Closed;
            table.Left = (Application.Current.Host.Content.ActualWidth - table.ActualWidth) / 2;
            table.Top = (Application.Current.Host.Content.ActualHeight - table.ActualHeight) / 2;
            table.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            table.ShowDialog();

        }
        void Table_Closed(object sender, WindowClosedEventArgs e)
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViews, null);
        }

        void Field_Closed(object sender, EventArgs e)
        {

            GetViewFields();
        }

        private void radButton1_Click(object sender, RoutedEventArgs e)
        {
            Table table = gdvTable.SelectedItem as Table;
            if (table == null)
                return;

            tableID = table.ID;

            string confirmText = "Do you want to delete  " + table.TableName + " table?";
            RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmDeleteClosed));

           


        }


        private void OnConfirmDeleteClosed(object sender, WindowClosedEventArgs e)
        {

            if (e.DialogResult == true)
            {
                DeleteViewTable();
            }
        }
        private void DeleteViewTable()
        {
            LoadOperation<ViewTable> loadOp = context.Load(context.GetViewTablesQuery().Where(vt => vt.Table_ID == tableID && vt.View_ID == viewID), CallbackViewTabels, null);
        }

        private void DeleteViewFieds()
        {
            LoadOperation<ViewField> loadOp = context.Load(context.GetViewFieldsQuery().Where(vt => vt.Table_ID == tableID && vt.View_ID == viewID), CallbackViewFields, null);
        }


        private void DeleteViewReleationship()
        {
            LoadOperation<ViewTableRelationship> loadOp = context.Load(context.GetViewTableRelationshipsQuery().Where(vt => (vt.Table_ID_A == tableID || vt.Table_ID_B == tableID) && vt.View_ID == viewID), CallbackViewReleationships, null);
        }

        private void CallbackViewTabels(LoadOperation<ViewTable> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewTable viewTable in loadOp.Entities)
                    {
                        context.ViewTables.Remove(viewTable);
                    }


                }
                DeleteViewFieds();
            }
            catch
            {
            }


        }


        private void CallbackViewFields(LoadOperation<ViewField> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewField viewTable in loadOp.Entities)
                    {
                        context.ViewFields.Remove(viewTable);
                    }


                }
                DeleteViewReleationship();
            }
            catch
            {
            }

        }


        private void CallbackViewReleationships(LoadOperation<ViewTableRelationship> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewTableRelationship viewRship in loadOp.Entities)
                    {
                        context.ViewTableRelationships.Remove(viewRship);
                    }


                }
            }
            catch
            {
            }

            context.SubmitChanges(submit =>
            {
                if (submit.HasError) { RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message }); ; context.RejectChanges(); }
                else {

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" }); RefreshTables();
            }
            }, null);

        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            ViewFieldSummary field = radGridView1.SelectedItem as ViewFieldSummary;

            if (field == null)
                return;

            SetFieldValue set = new SetFieldValue(viewID, field, tableList);
            set.Closed += Field_Closed;
            set.Left = (Application.Current.Host.Content.ActualWidth - set.ActualWidth) / 2;
            set.Top = (Application.Current.Host.Content.ActualHeight - set.ActualHeight) / 2;
            set.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            set.ShowDialog();
        }

        private void btnCalculatedFields_Click(object sender, RoutedEventArgs e)
        {
            SetCalculatedField set = new SetCalculatedField(viewID);
            set.Closed += Field_Closed;
            set.Left = (Application.Current.Host.Content.ActualWidth - set.ActualWidth) / 2;
            set.Top = (Application.Current.Host.Content.ActualHeight - set.ActualHeight) / 2;
            set.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            set.ShowDialog();
        }

        private void radButton2_Click(object sender, RoutedEventArgs e)
        {
            ViewFieldSummary field = radGridView1.SelectedItem as ViewFieldSummary;

            if (field == null)
                return;
            if (field.IsCalculatedField.Value)
            {
                EditCalculatedField set = new EditCalculatedField(viewID, field.ID);
                set.Closed += Field_Closed;
                set.Left = (Application.Current.Host.Content.ActualWidth - set.ActualWidth) / 2;
                set.Top = (Application.Current.Host.Content.ActualHeight - set.ActualHeight) / 2;
                set.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                set.ShowDialog();
            }
        }

        private void btnSaveChanges_Click(object sender, RoutedEventArgs e)
        {

            foreach (var item in radGridView1.Items)
	            {

              ViewFieldSummary dataObject = item as ViewFieldSummary;
             if ((dataObject != null))
                    {

                      
                            var viewfld = viewfieldList.Where(v => v.ID == dataObject.ID).FirstOrDefault();
                            if (viewfld != null)
                            {

                                if(dataObject.DisplayName != string.Empty)
                                viewfld.DisplayName = dataObject.DisplayName;
                               if(dataObject.UseValueField != null)
                                viewfld.UseValueField = dataObject.UseValueField.Value;

                                if(dataObject.Hidden != null)
                                    viewfld.Hidden = dataObject.Hidden.Value;
                                if(dataObject.UseValueField != null)
                                    viewfld.UseValueField = dataObject.UseValueField.Value;
                                 if(dataObject.ForCreate != null)
                                    viewfld.ForCreate = dataObject.ForCreate.Value;
                                 if (dataObject.ReadOnly != null)
                                   viewfld.ReadOnly = dataObject.ReadOnly.Value;
                                if(dataObject.ForRead != null)
                                    viewfld.ForRead = dataObject.ForRead.Value;
                                 if(dataObject.ForUpdate != null)
                                    viewfld.ForUpdate = dataObject.ForUpdate.Value;
                                 if(dataObject.ForDelete != null)
                                    viewfld.ForDelete = dataObject.ForDelete.Value;
                                   if(dataObject.UseValueStoredProcedure != null)
                                    viewfld.UseValueStoredProcedure = dataObject.UseValueStoredProcedure.Value;
                                if(dataObject.StoredProcedureNames_ID != null)
                                    viewfld.StoredProcedureNames_ID = dataObject.StoredProcedureNames_ID;

                                if (dataObject.FieldOptionFilter != null)
                                    viewfld.FieldOptionFilter = dataObject.FieldOptionFilter;
                                if (dataObject.DefaultValue != null)
                                    viewfld.DefaultValue = dataObject.DefaultValue;
                                if (dataObject.ForTSAdjust != null)
                                    viewfld.ForTSAdjust = dataObject.ForTSAdjust.Value;

                                if (dataObject.DefaultValue != null)
                                    viewfld.DefaultValue = dataObject.DefaultValue;
                                if (dataObject.FieldOptionFilter != null)
                                    viewfld.FieldOptionFilter = dataObject.FieldOptionFilter;
                                if (dataObject.AutoPopulate != null)
                                    viewfld.AutoPopulate = dataObject.AutoPopulate.Value;
     
                            }
                        
                        }
        }

            context.SubmitChanges(s =>
            {
                if (s.HasError)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Unsuccessful" });
                    
                }
                else
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                }
            }, null);
    }
}
    }
