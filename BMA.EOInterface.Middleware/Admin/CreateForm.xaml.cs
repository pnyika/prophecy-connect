﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;
using Telerik.Windows.Controls;
using BMA.EOInterface.Middleware;

namespace BMA.MiddlewareApp.Admin
{
    public partial class CreateForm
    {
        EditorContext context = new EditorContext();
        public event EventHandler CreateFormComplete;
        public CreateForm()
        {
            InitializeComponent();
        }

        private void createForm()
        {
            DisplayDefinition display = new DisplayDefinition();
            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                display.CreatedUser_ID = Globals.CurrentUser.ID;
            else
                display.CreatedUser_ID = 0;

            display.Description = txtForm.Text;
            display.FormType_ID = 8;
            display.PageSize = 1;

            context.DisplayDefinitions.Add(display);
            context.SubmitChanges(submit =>
            {
                if (!submit.HasError)
                {

                    ObjectForm form = new ObjectForm();
                    form.DisplayDefinition_ID = display.ID;
                    form.FormName = txtForm.Text;
                    context.ObjectForms.Add(form);
                    context.SubmitChanges(formSubmit => {
                        if (!formSubmit.HasError) {
                            DesignForm.formID = form.ID;
                            this.DialogResult = false;
                            CreateFormComplete(this, null);
                    }
                        else 
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error occured While creating from" });
                    }, null);

                }
                else
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error occured While creating from" });        
            }, null);
            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            OKButton.IsEnabled = false;
            createForm();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

