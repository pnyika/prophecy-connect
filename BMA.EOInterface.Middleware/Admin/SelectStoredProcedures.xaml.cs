﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.Collections;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using BMA.EOInterface.Middleware;
using Telerik.Windows.Controls;
//using BMA.MiddlewareApp.DataTableService;


namespace BMA.MiddlewareApp.Admin
{
    public partial class SelectStoredProcedures 
    {
        EditorContext context = new EditorContext();
        IEnumerable _lookup;
        ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> _tables;
       
        private string databaseName = "Middleware";
        private string connString;
        private string serverName;
        private int serverID;
        public SelectStoredProcedures(string connstr, string db, string server, int sID)
        {
            InitializeComponent();
            this.connString = connstr;
            this.databaseName = db ;
            this.serverID = sID;
           // string sql = "use [" + db + "]; Select name,Object_ID, 1 as IsTable from sys.tables where type_desc = 'user_table' union  Select name,Object_ID, 0 as IsTable from sys.views  order by IsTable desc,name asc ";
          //  GetTables(sql, 1, 500, "Tables");
            GetStoreProcs(db);
        }


       private void GetStoreProcs(string catalog)
       {
           string sql = GetStoreProcSql(catalog);
           GetStoreProcs(sql, catalog);
       }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            SaveStoreProcedures();
            
        }


        private void SaveStoreProcedures()
        {
            radGridView1.IsBusy = true;
            if (radGridView1.SelectedItems.Count < 1)
            {
                return;
            }
            else
            {
                foreach (var item in radGridView1.SelectedItems)
                {

                    DataObject proc = radGridView1.SelectedItem as DataObject;
                      string procName= proc.GetFieldValue("Name").ToString();
                      StoredProcedureName storeProc = new StoredProcedureName();
                      storeProc.DBName = databaseName;
                      storeProc.KnownFields = true;
                      storeProc.Server_ID = serverID;
                      storeProc.SP_Function = procName;
                      storeProc.SP_Name = procName;
                      context.StoredProcedureNames.Add(storeProc);
                    
                }

                context.SubmitChanges(submit =>
                {
                    if (submit.HasError)
                    {
                    }
                    else
                    {
                        procList = new List<ItemProc>();
                        var list = context.StoredProcedureNames.ToList();
                        int i= 1;
                        foreach (var item in list)
                        {
                            procList.Add(new ItemProc { ID =item.ID, Index =i, Name=item.SP_Name});
                            i += 1;
                        }
                        lastIndex = list.Count;
                        AddParameters();
                    }
                }, null);
            }
            radGridView1.IsBusy = false;
        }

        int procID;
        int index =0;
        int lastIndex;
        List<ItemProc> procList = new List<ItemProc>();


        private void AddParameters()
        {
            if (procList.Count > 0)
            {


                ItemProc temp = procList.Where(t => t.Index == index + 1).FirstOrDefault();
                procID = temp.ID;




                string sql = SqlQuery(databaseName, temp.Name);
                //    GetColumnData(connString, sql, 1, 500, "");

                GetStoreProcParameters(sql);
                index = temp.Index;
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                this.Close();// = false;
            }
        }


        private string GetStoreProcSql(string DBName)
        {
            string sql = " ";
            sql += "SELECT SCHEMA_NAME(SCHEMA_ID) AS [Schema], ";
            sql += "SO.name AS [Name],";
            sql += "SO.Type_Desc AS [ObjectType (UDF/SP)] ";
            sql += "FROM " + DBName + ".sys.objects AS SO ";
            sql += "WHERE SO.TYPE IN ('P','FN') ";
            sql += "ORDER BY [Schema], SO.name ";
            return sql;
        }


        private void GetStoreProcs(string sql, string catalog)
        {

          //  Server server = ddlDatabase.SelectedItem as Server;
         
          //  this.radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcsCompleted);
            ws.GetDataSetDataAsync(connString, catalog, sql, 1, 1000, null);
        }


        private void GetStoreProcParameters(string sql)
        {
            radGridView1.IsBusy = true;

                //   string connString = objectFunction.Server.ConnectionString;
                var ws = WCF.GetService();
                ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetStoreProcParaCompleted);
                ws.GetDataSetDataAsync(connString, databaseName, sql, 1, 1000, "");
           
        }


        void ws_GetStoreProcParaCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                // _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);

                List<ParameterData> parameterList = new List<ParameterData>();
                foreach (DataObject item in list)
                {

                    string name = item.GetFieldValue("ParameterName").ToString().Replace('@', ' ');
                 
                    StoredProcedureParameter parameter = new StoredProcedureParameter();
                    parameter.DataType = item.GetFieldValue("ParameterDataType").ToString();
                    parameter.ParameterName =name.TrimStart();
                   
                    parameter.StoredProcedureNames_ID = procID;
                    context.StoredProcedureParameters.Add(parameter);

                }

                context.SubmitChanges(submit =>
                {
                    if (submit.HasError) { }
                    else
                    {
                        if (index < lastIndex)
                        {
                            AddParameters();
                        }
                        else
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                            this.Close();
                        }
                    }
                }, null);

                //if (index < lastIndex+1)
                //{
                //    AddParameters();
                //}

            }
            radGridView1.IsBusy = false;
        }


        void ws_GetStoreProcsCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                // _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    radGridView1.ItemsSource = list;
                 //   ddlStoreProc.SelectedValue = storeProcName;


                }
            }
           // this.radBusyIndicator.IsBusy = false;
        }
        private void InsertServer()
        {
            Server server = new Server();
           // int serverID = context.
            
        }
        private string SqlQuery(string database, string procName)
        {

            string sql = "";
            sql += " SELECT SCHEMA_NAME(SCHEMA_ID) AS [Schema], ";
            sql += "   SO.name AS [ObjectName], ";
            sql += "  SO.Type_Desc AS [ObjectType (UDF/SP)],";
            sql += "  P.parameter_id AS [ParameterID],";
            sql += "  P.name AS [ParameterName],";
            sql += "  TYPE_NAME(P.user_type_id) AS [ParameterDataType],";
            sql += " P.max_length AS [ParameterMaxBytes],";
            sql += "  P.is_output AS [IsOutPutParameter]";
            sql += "  FROM " + database + ".sys.objects AS SO";
            sql += "  Left Outer JOIN " + database + ".sys.parameters AS P  ON SO.OBJECT_ID = P.OBJECT_ID";
            sql += " WHERE SO.TYPE IN ('P','FN')";
            sql += "  and SO.name = '" + procName + "'";
            sql += "  ORDER BY [Schema], SO.name, P.parameter_id";
            return sql;
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();// = false;
        }



        private void GetTables(string sql, int pagenumber, int pagesize, object userState)
        {
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }





        void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;

                else
                {


                    radGridView1.ItemsSource = list;



                }
            }

        }
    }


    public class ItemProc
    {
        public int ID { get; set; }
        public int Index { get; set; }
        public string Name  {get;set;}
       

    }
}

