﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class SetGridRelationship 
    {
        EditorContext context = new EditorContext();
        ObservableCollection<TempGridRelationship> tempRelationships;
        int displayDefID;
        public SetGridRelationship()
        {

            InitializeComponent();

            loadRelationships();
            
        }

        private void loadRelationships()
        {
            LoadOperation<ViewGridRelationship> loadOperation = context.Load(context.GetRelationshipByDefinitionIDQuery(ViewDisplayDefinition.publicDisplayDef.ID), CallbackGridRships, null);
        }


        private void CallbackGridRships(LoadOperation<ViewGridRelationship> loadOperation)
        {
            tempRelationships = new ObservableCollection<TempGridRelationship>();
            if (loadOperation.Entities != null)
            {
                foreach (var item in loadOperation.Entities)
                {
                    
            TempGridRelationship temp = new TempGridRelationship();
            temp.ID = item.ID;
                 temp.Grid_ID = item.Grid_ID;
                temp.Field1_ID = item.Field1_ID;
                temp.Field1_Name = "Grid " + item.GridNumber + "." + item.Field1_Name;
                temp.Grid2_ID = item.Grid2_ID;
                temp.Field2_ID = item.Field2_ID;
                temp.Field2_Name = "Grid " + item.Grid2Number + "." + item.Field2_Name;
                
                if(!(tempRelationships.Contains(temp)))
                tempRelationships.Add(temp);
                }
               

            }

            radGridView1.ItemsSource = tempRelationships; 
           
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (tempRelationships == null)
                return;
            foreach (TempGridRelationship temp in tempRelationships)
            {
                GridRelationship relation = new GridRelationship();
                relation.DisplayDefinition_ID = ViewDisplayDefinition.publicDisplayDef.ID;
                relation.GridID1 = temp.Grid_ID;
                relation.GridID2 = temp.Grid2_ID;
                relation.ViewField1 = temp.Field1_ID;
                relation.ViewField2 = temp.Field2_ID;
                if(temp.ID < 1)
                context.GridRelationships.Add(relation);

            }

            context.SubmitChanges(submit => { if (submit.HasError) { } else { RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" }); this.Close(); } }, null);
           // this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();// = false;
        }


        private void GetGridDefinition(object sender, RoutedEventArgs e)
        {
            LoadOperation<GridDefinition> loadOperation = context.Load(context.GetGridDefinitionsQuery().Where(g => g.DisplayDefinition_ID == ViewDisplayDefinition.publicDisplayDef.ID), CallbackGrids, null);
        }

        private void CallbackGrids(LoadOperation<GridDefinition> loadOperation)
        {
            if (loadOperation.Entities != null)
            {

                ddlGrid1.ItemsSource = loadOperation.Entities;
                ddlGrid2.ItemsSource = loadOperation.Entities.Where(x => x.GridNumber != 1);
               
            }
            LoadGridRelation();
        }


        private void LoadGridRelation()
        {
            if (tempRelationships == null)
                tempRelationships = new ObservableCollection<TempGridRelationship>();

            radGridView1.ItemsSource = tempRelationships;
        }

        private void ddlGrid1_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            GridDefinition grid = ddlGrid1.SelectedItem as GridDefinition;
            if ((grid.View_ID == null) || (grid.View_ID == 0))
            {
              
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "Set the View for this grid first!" });
            }
            else
            {

                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(grid.View_ID.Value), CallbackViewFields, null);
            }
      }

      

        private void CallbackViewFields(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                
                lstA.ItemsSource = loadOp.Entities;

                
                
            }
        }

        private void ddlGrid2_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {

            GridDefinition grid = ddlGrid2.SelectedItem as GridDefinition;
            if ((grid.View_ID == null) || (grid.View_ID == 0))
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "Set the View for this grid first!" });
              
            }
            else
            {

                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(grid.View_ID.Value), CallbackViewFields2, null);
            }
        }



        private void CallbackViewFields2(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {

                lstB.ItemsSource = loadOp.Entities;



            }
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            if ((lstA.SelectedItem == null) || (lstB.SelectedItem == null))
                return;
            if (tempRelationships == null)
                tempRelationships = new ObservableCollection<TempGridRelationship>();


            TempGridRelationship temp = new TempGridRelationship();
            ViewFieldSummary field1 = lstA.SelectedItem as ViewFieldSummary;
            ViewFieldSummary field2 = lstB.SelectedItem as ViewFieldSummary;

            //check if the relationship exists
            //var query = from f in tempRelationships
            //            where f.TableA_ID == tableA_ID && f.TableB_ID == tableB_ID
            //                    && f.FieldA_ID == fieldA.ID && f.FieldB_ID == fieldB.ID
            //            select f;
            //var query2 = from fd in relationshipList
            //             where fd.TableA_ID == tableA_ID && fd.TableB_ID == tableB_ID
            //                     && fd.FieldA_ID == fieldA.ID && fd.FieldB_ID == fieldB.ID
            //             select fd;

            //if ((query.Count<TempRelationship>() > 0) || (query2.Count<ViewRelationship>() > 0))
            //{
            //    MessageBox.Show("This relationship already exists!");
            //}
            //else
            //{
            GridDefinition grid = ddlGrid1.SelectedItem as GridDefinition;
            GridDefinition grid2 = ddlGrid2.SelectedItem as GridDefinition;

            temp.Grid_ID = grid.ID;
                temp.Field1_ID = field1.FieldID;
                temp.Field1_Name = "Grid " + grid.GridNumber + "." + field1.FieldName;
                temp.Grid2_ID = grid2.ID;
                temp.Field2_ID = field2.FieldID;
                temp.Field2_Name = "Grid " + grid2.GridNumber + "." + field2.FieldName;
                
                if(!(tempRelationships.Contains(temp)))
                tempRelationships.Add(temp);
           // }
            
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            TempGridRelationship temp = radGridView1.SelectedItem as TempGridRelationship;
            if (temp == null)
                return;

            tempRelationships.Remove(temp);
            if (temp.ID > 0)
            {
                LoadOperation<GridRelationship> loadOp = context.Load(context.GetGridRelationshipQuery().Where(g => g.ID == temp.ID), CallbackDeleteGridRelationships, null);
            }
        }

        private void CallbackDeleteGridRelationships(LoadOperation<GridRelationship> result)
        {


            if (result != null)
            {


                //  ObservableCollection<GroupModel> viewResults = new ObservableCollection<GroupModel>(result.Entities);




                GridRelationship rship  = result.Entities.FirstOrDefault();
                context.GridRelationships.Remove(rship);
                

                context.SubmitChanges(submit =>
                {
                    if (!submit.HasError)
                    {
                       // Message.InfoMessage("Successful");
                       // this.DialogResult = true;
                    }
                    else
                    {
                       // Message.ErrorMessage(submit.Error.Message);
                    }
                }, null);


            }


        }
    
    }



    public class TempGridRelationship
    {
        public int ID { get; set; }
        public int Grid_ID { get; set; }

        public int Field1_ID { get; set; }
        public string Field1_Name { get; set; }

        public int Grid2_ID { get; set; }
        public int Field2_ID { get; set; }
        public string Field2_Name { get; set; }
    }
}

