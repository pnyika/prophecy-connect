﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddTable 
    {
        EditorContext context = new EditorContext();
        ObservableCollection<Server> serverList;
        ObservableCollection<Table> tableList;
        List<Table> tables;
        private int viewID;
        public AddTable(int viewId, List<Table> tablelist)
        {
            InitializeComponent();
            viewID = viewId;
            tables = tablelist;
            CheckTables();
        }


        private void CheckTables()
        {
            if (tables.Count > 0)
            {
               // lstServer.IsEnabled = false;
                var query = (from t in tables
                             select t).FirstOrDefault();
                lstServer.SelectedValue = query.ID;
                
                LoadOperation<Table> loadOper = context.Load(context.GetTablesQuery().Where(x => x.Server_ID == query.Server_ID), CallbackConnTable, null);

            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            
            var selectedTable = lstTable.SelectedItems;
            if(selectedTable == null)
                return;

            foreach(Table table in selectedTable)
            {
                var query = from t in tables
                            where t.ID == table.ID
                            select t;
                if (query.Count<Table>() > 0) 
                {
                        
                }
                else
                {
                    ViewTable viewTable = new ViewTable();
                    viewTable.View_ID = viewID;
                    viewTable.Table_ID = table.ID;
                    context.ViewTables.Add(viewTable);


                 }
            }
            context.SubmitChanges(submit => { if (submit.HasError) {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
               
            } else { 
              

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = " Successfully saved!" });
                this.Close();
            } }, null);

            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lstServer_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;

            LoadOperation<Table> loadOper = context.Load(context.GetTablesQuery().Where(x => x.Server_ID == server.ID), CallbackConnTable, null);

        }

        private void CallbackConnTable(LoadOperation<Table> loadOp)
        {


            tableList = new ObservableCollection<Table>();

            if (loadOp.Entities != null)
            {
                foreach (Table tbl in loadOp.Entities)
                {
                    tableList.Add(tbl);
                }


                lstTable.ItemsSource = tableList;
            }
        }
        private void getServer(object sender, RoutedEventArgs e)
        {
            //busyIndicator1.IsBusy = true;
           
            LoadOperation<Server> loadOp = context.Load(context.GetServersQuery(), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {


            if (loadOp != null)
            {
               
                serverList = new ObservableCollection<Server>();
                
                foreach (Server srvr in loadOp.Entities)
                {
                    serverList.Add(srvr);
                }
             
                lstServer.ItemsSource =serverList;

            }
        }
    }
}

