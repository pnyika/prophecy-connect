﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;
using System.Collections;
using BMA.EOInterface.Middleware;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddModelGroup 
    {
        EditorContext context = new EditorContext();
        ObservableCollection<GroupModel> viewGroupModel = new ObservableCollection<GroupModel>();
        ObservableCollection<GroupModel> currentGroupModel = new ObservableCollection<GroupModel>();

        ObservableCollection<GroupModel> GroupModelList = new ObservableCollection<GroupModel>();
        List<GroupModel> addedGroupModel = new List<GroupModel>();
        List<GroupModel> removedGroupModel = new List<GroupModel>();
       

        int groupID = 0;
        public AddModelGroup(Group thisGroup)
        {
            InitializeComponent();
            groupID= thisGroup.ID;
            if (thisGroup.GroupName != string.Empty)
            {
                txtGroupName.Text = thisGroup.GroupName;
            }


             viewGroupModel = new ObservableCollection<GroupModel>();
            currentGroupModel = new ObservableCollection<GroupModel>();
             addedGroupModel = new List<GroupModel>();
             removedGroupModel = new List<GroupModel>();
           // loadViews();
            getServer();
            loadGroupModels();
        }



        private void getServer()
        {
            //busyIndicator1.IsBusy = true;
            App app = (App)Application.Current;
            LoadOperation<Server> loadOp = context.Load(context.GetServersQuery(), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {


            if (loadOp != null)
            {
               

                ddlDatabase.ItemsSource = loadOp.Entities;

            }
        }

    
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //Group group = new Group();
            //group.GroupName = txtGroupName.Text;
            //group.Description = "";
            //context.Groups.Add(group);


            foreach (GroupModel gModel in addedGroupModel)
            {
                GroupModel groupModel = new GroupModel();
                groupModel.Group_ID = groupID;
                groupModel.Model_ID = gModel.Model_ID;
                groupModel.ModelName = gModel.ModelName;
                context.GroupModels.Add(groupModel);
            }

         
            LoadOperation<GroupModel> loadOp = context.Load(context.GetGroupModelsQuery(), CallbackDeleteGroupModels, null);
            context.SubmitChanges(submit =>
            {
                if (!submit.HasError)
                {

                    string alertText = "Successfully saved";

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                    this.Close();
                }
                else
                {
                   
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
                }
            }, null);

        }


        

       private void CallbackDeleteGroupModels(LoadOperation<GroupModel> result)
        {

           
            if (result != null)
            {
               

              //  ObservableCollection<GroupModel> viewResults = new ObservableCollection<GroupModel>(result.Entities);



                foreach (GroupModel groupModel in removedGroupModel)
                {
                    GroupModel gmodel = result.Entities.Where(g => g.ID == groupModel.ID).FirstOrDefault();
                    context.GroupModels.Remove(gmodel);
                }

                context.SubmitChanges(submit =>
                {
                    if (!submit.HasError)
                    {
                        string alertText = "Successfully saved";

                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                        this.Close();
                    }
                    else
                    {
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
                    }
                }, null);
              

            }

          
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void loadGroupModels()
        {

            LoadOperation<GroupModel> loadOp = context.Load(context.GetGroupModelsQuery(), CallbackGroupModels, null);




        }


        private void CallbackGroupModels(LoadOperation<GroupModel> result)
        {

            currentGroupModel = new ObservableCollection<GroupModel>();
            GroupModelList = new ObservableCollection<GroupModel>();
            if (result != null)
            {
                currentGroupModel = new ObservableCollection<GroupModel>(result.Entities.Where(g=>g.Group_ID ==groupID));
                GroupModelList = new ObservableCollection<GroupModel>(result.Entities.Where(g => g.Group_ID == groupID));

              //  ObservableCollection<GroupModel> viewResults = new ObservableCollection<GroupModel>(result.Entities);



                foreach (GroupModel groupModel in result.Entities)
                {
                    GroupModel gmodel = viewGroupModel.Where(g => g.Model_ID == groupModel.Model_ID).FirstOrDefault();
                    viewGroupModel.Remove(gmodel);
                }
              

            }

            lstModelList.ItemsSource = viewGroupModel;
            lstModel.ItemsSource = currentGroupModel;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lstModelList.SelectedItems.Count > 0)
                {


                    List<GroupModel> selectedItems = new List<GroupModel>();
                    foreach (var item in lstModelList.SelectedItems)
                    {
                        selectedItems.Add(item as GroupModel);
                    }

                    foreach (var item in selectedItems)
                    {
                        GroupModel view = item as GroupModel;
                        addedGroupModel.Add(view);
                        viewGroupModel.Remove(view);
                        currentGroupModel.Add(view);

                        if (removedGroupModel.Contains(view))
                            removedGroupModel.Remove(view);

                    }
                }
            }
            catch
            {
            }

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lstModel.SelectedItems.Count > 0)
            {
                List<GroupModel> selectedItems = new List<GroupModel>();
                foreach (var item in lstModel.SelectedItems)
                {
                    selectedItems.Add(item as GroupModel);
                }
                foreach (var item in selectedItems)
                {
                    GroupModel view = item as GroupModel;
                    removedGroupModel.Add(view);
                    viewGroupModel.Add(view);
                    currentGroupModel.Remove(view);
                    if (addedGroupModel.Contains(view))
                        addedGroupModel.Remove(view);
                    
                }
            }
        }




        private void GetData(string connString, string databaseName, string sql, int pagenumber, int pagesize, object userState)
        {
            
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
              //  _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                { }
                // _lookup = list;
                
                else
                {
                    viewGroupModel = new ObservableCollection<GroupModel>();
                    foreach (var item in list)
                    {
                           DataObject dataObject = item as DataObject;
                        string strID = dataObject.GetFieldValue("ID").ToString();
                        string strName = dataObject.GetFieldValue("ModelName").ToString();
                        long modelID = Convert.ToInt64(strID);
                        
                        viewGroupModel.Add(new GroupModel{ID =0, Model_ID = modelID, ModelName = strName});
                    }

                    lstModelList.ItemsSource = viewGroupModel;

                    foreach (GroupModel groupModel in currentGroupModel)
                    {
                        GroupModel gmodel = viewGroupModel.Where(g => g.Model_ID == groupModel.Model_ID).FirstOrDefault();
                        viewGroupModel.Remove(gmodel);
                    }

                }

            }
           
        }
        private void ddlDatabase_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            Server server = ddlDatabase.SelectedItem as Server;
            if (server != null)
            {
                string sql = " select ID, ModelName from Model ";
                GetData(server.ConnectionString, server.DatabaseName, sql, 1, 1000, "");
            }
        }
    }
    //public class TempDisplay
    //{
    //    public string Value { get; set; }
    //    public string Display { get; set; }

    //}
}

