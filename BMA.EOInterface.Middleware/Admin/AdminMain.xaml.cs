﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
  using Telerik.Windows;
using System.ServiceModel.DomainServices.Client.ApplicationServices;
using BMA.EOInterface.Middleware;
using System.Windows.Browser;
using Telerik.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using BMA.EOInterface.Middleware;

using BMA.MiddlewareApp.Web;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AdminMain : UserControl
    {
        public static string header;
        public static event EventHandler StatusUpdated;
        public static bool cnUpdate = false;
        EditorContext context = new EditorContext();
        UserInformation activeUser = new UserInformation();
        MenuStructure thisMenuStruct = new MenuStructure();
        public List<BMA.MiddlewareApp.Web.MenuItem> menuList { get; set; }
        public List<BMA.MiddlewareApp.Web.MenuItem> SubMenu { get; set; }
        public static int DisplayID;
        private int pageSize;
        public static int userID;
        public static List<GroupModel> ModelList;

        public static long groupID;
        public static long ModelID;
        public static string ModelName;
        public static DateTime ReferenceDate;
        int menuID;
        // public static int newHeight = outlookBar.Height;
        string title;
        public AdminMain()
        {

            InitializeComponent();
            if (WebContext.Current.Authentication.User.IsInRole("User"))
            {
                this.Content = new MainPage();
            }
            else
            {

                if (WebContext.Current.Authentication.User.IsInRole("Developer"))
                    lblRole.Text = "Application Developer ";
                else
                    lblRole.Text = "Super User";

                lblUsername.Text = WebContext.Current.Authentication.User.Identity.Name;
                lblInfo.Text = "";


                LoadData();
                // this.tvHierarchyView.AddHandler(Telerik.Windows.Controls.RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.radTreeView_MouseLeftButtonDown), true);

            }

            LayoutRoot.LayoutUpdated += new EventHandler(LayoutRoot_LayoutUpdated);
        }

        void LayoutRoot_LayoutUpdated(object sender, EventArgs e)
        {
            // NavigationGrid.Height = LayoutRoot.Height -30;
        }

        // Executes when the user navigates to this page.
        //protected override void OnNavigatedTo(NavigationEventArgs e)
        //{
        //}


        private void radTreeView_MouseLeftButtonDown2(object sender, MouseButtonEventArgs e)
        {
            Telerik.Windows.Controls.RadTreeView treeView = sender as Telerik.Windows.Controls.RadTreeView;


            Category selectedItem = treeView.SelectedItem as Category;
            if (selectedItem == null)
                return;
            menuID = Convert.ToInt32(selectedItem.ID);

            //GetTable(menuID);
            // this.Content = new Page2();
            if (selectedItem.DefID != null)
            {

                DisplayID = selectedItem.DefID.Value;
                if (selectedItem.PageSize != null)
                    pageSize = selectedItem.PageSize.Value;
                else
                    pageSize = 25;
                LoadOperation<FormType> ldop = context.Load(context.GetFormTypeByMenuIDQuery(menuID), CallbackType, null);

                title = selectedItem.Name;
                //  LoadOperation<DisplayDefinition> ldop = context.Load(context.GetDisplayDefinitionsQuery(selectedItem.DefID.Value), CallbackDefDisplay, null);

            }
            else
            {
                if (selectedItem.Name == "Logout")
                {
                    cnUpdate = false;
                    WebContext.Current.Authentication.Logout(LogoutCompleteCallback, null);

                }

                else if (selectedItem.Name == "Design")
                {
                    cnUpdate = false;
                    // myDispatcherTimer.Stop();
                    //   Content = new Admin.AdminMain();
                }
            }

        }

        private void radTreeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Telerik.Windows.Controls.RadTreeView treeView = sender as Telerik.Windows.Controls.RadTreeView;

            string selectedItem = treeView.SelectedItem.ToString();
            if (selectedItem == null)
                return;
            try
            {

                string selected = selectedItem.ToString();
                // MessageBox.Show(select   ed);
                header = selected;
                lblTitle2.Text = selected;
                //    NavContent.Header = header;
                switch (selected)
                {
                    case "Views":
                        if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                            this.ContentFrame.Navigate(new Uri("/ViewListsp", UriKind.Relative));
                        else
                            this.ContentFrame.Navigate(new Uri("/ViewList", UriKind.Relative));
                        break;


                    case "Databases":

                        this.ContentFrame.Navigate(new Uri("/ConfigPage", UriKind.RelativeOrAbsolute));
                        break;

                    case "Forms":

                        this.ContentFrame.Navigate(new Uri("/AddForm", UriKind.RelativeOrAbsolute));
                        break;
                    case "Grid Form":

                        this.ContentFrame.Navigate(new Uri("/ViewDisplayDefinition", UriKind.RelativeOrAbsolute));
                        break;
                    case "View Users":

                        this.ContentFrame.Navigate(new Uri("/ViewUsers", UriKind.RelativeOrAbsolute));
                        break;

                    case "Group Filtering":

                        this.ContentFrame.Navigate(new Uri("/ViewGroups", UriKind.RelativeOrAbsolute));
                        break;


                    case "Menu Settings":

                        this.ContentFrame.Navigate(new Uri("/ViewMenuStructure", UriKind.RelativeOrAbsolute));

                        break;

                    case "Custom Form":

                        this.ContentFrame.Navigate(new Uri("/ViewForm", UriKind.RelativeOrAbsolute));

                        break;

                    case "Model Interface":

                        this.ContentFrame.Navigate(new Uri("/ViewEOForm", UriKind.RelativeOrAbsolute));

                        break;

                    case "Back":

                        this.Content = new MainPage();
                        break;

                    case "Logout":
                        /// FormAuthentication()
                        WebContext.Current.Authentication.Logout(LogoutCompleteCallback, null);

                        break;


                }

            }
            catch (Exception ex)
            {
                this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));
                //MessageBox.Show(ex.Message);
            }
        }


        private void LogoutCompleteCallback(LogoutOperation result)
        {
          //  HtmlPage.Window.Navigate(new Uri(Application.Current.Host.Source, "../Default.aspx"));


            if (Application.Current.IsRunningOutOfBrowser)
            {
                this.Content = new MainPage();
            }
            else
            {

                HtmlPage.Window.Navigate(new Uri(Application.Current.Host.Source, "../Default.aspx"));
            }

        }


        private void AddItems()
        {
            TreeViewItem item1 = new TreeViewItem();
            item1.Header = "Products";
            item1.Items.Add(new TreeViewItem() { Header = "Controls" });
            item1.Items.Add(new TreeViewItem() { Header = "Media Players" });
            item1.Items.Add(new TreeViewItem() { Header = "Games" });
            item1.Items.Add(new TreeViewItem() { Header = "Charts" });
            //  tvHierarchyView.Items.Add(item1);
        }

        private void LoadData()
        {
            List<Category> categories = new List<Category>();
            XDocument categoriesXML = XDocument.Load("MenuFile.xml");

            categories = this.GetCategories(categoriesXML.Element("categories"));

            // this.tvHierarchyView.ItemsSource = categories;

            string username = WebContext.Current.Authentication.User.Identity.Name;
            this.tvHierarchyView.AddHandler(Telerik.Windows.Controls.RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.radTreeView_MouseLeftButtonDown2), true);

            LoadOperation<UserInformation> loadOperation = context.Load(context.GetUserInformationWithMenuStructureQuery().Where(u => u.UserName == username), CallbackUser, null);
        }

        private List<Category> GetCategories(XElement element)
        {

            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
            {

                return (from category in element.Elements("category")
                        select new Category()
                        {
                            Name = category.Attribute("name").Value,
                            Value = category.Attribute("value").Value,
                            SubCategories = this.GetCategories(category)
                        }).Where(x => x.Value == "1" || x.Value == "2").ToList();

            }
            else
            {
                return (from category in element.Elements("category")
                        select new Category()
                        {
                            Name = category.Attribute("name").Value,
                            Value = category.Attribute("value").Value,
                            SubCategories = this.GetCategories(category)
                        }).Where(x => x.Value == "0" || x.Value == "1").ToList();
            }

        }

        private void LoadData(List<BMA.MiddlewareApp.Web.MenuItem> elements)
        {
            List<Category> categories = new List<Category>();
            categories = this.GetCategories(elements);
            this.tvHierarchyView.ItemsSource = categories;
        }




        private List<Category> GetCategories(List<BMA.MiddlewareApp.Web.MenuItem> element)
        {
            int id = 0;
            return (from category in element.Where(p => p.ParentMenu_ID == 0)

                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        DefID = category.DisplayDefinition_ID,
                        ImageUrl = category.ImageUrl,
                        //  if(category.DisplayDefinition != null)
                        // PageSize =category.DisplayDefinition.PageSize.Value,


                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)

                    }).ToList();
        }


        private List<Category> GetChild(List<BMA.MiddlewareApp.Web.MenuItem> element, int menuLevel)
        {
            foreach (var item in element)
            {
                if (item.DisplayDefinition_ID == null)
                {
                    item.DisplayDefinition = new DisplayDefinition { ID = 0, PageSize = 0 };
                }
            }

            return (from category in element.Where(p => p.ParentMenu_ID == menuLevel)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        DefID = category.DisplayDefinition_ID,
                        PageSize = 25,//category.DisplayDefinition.PageSize.Value,
                        ImageUrl = category.ImageUrl,
                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private void tvHierarchyView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            // this.Content = new Page2();
            Category selectedItem = tvHierarchyView.SelectedItem as Category;

            try
            {

                string selected = selectedItem.ID.ToString();
                // MessageBox.Show(selected);
                int menuID = Convert.ToInt32(selected);

                //GetTable(menuID);
                // this.Content = new Page2();

                if (selectedItem.DefID != null)
                {
                    if (selectedItem.Name == "Logout")
                    {
                        MessageBox.Show("Logged out !");
                    }
                    else
                    {
                        LoadOperation<FormType> ldop = context.Load(context.GetFormTypeByMenuIDQuery(menuID), CallbackType, null);
                        DisplayID = selectedItem.DefID.Value;


                        //  LoadOperation<DisplayDefinition> ldop = context.Load(context.GetDisplayDefinitionsQuery(selectedItem.DefID.Value), CallbackDefDisplay, null);
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        //private void tvHierarchyView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        //{

        //    // this.Content = new Page2();
        //  //  Category selectedItem = tvHierarchyView.SelectedItem as Category;

        //    try
        //    {

        //        string selected = "";//selectedItem.Name.ToString();
        //        // MessageBox.Show(selected);
        //        //NavContent.Header = selected;
        //        if (selected == "Views")
        //        {

        //            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
        //            this.ContentFrame.Navigate(new Uri("/ViewListsp", UriKind.Relative)); 
        //            else
        //                this.ContentFrame.Navigate(new Uri("/ViewList", UriKind.Relative)); 

        //        }
        //        if (selected == "Databases")
        //        {
        //            this.ContentFrame.Navigate(new Uri("/ConfigPage", UriKind.RelativeOrAbsolute));
        //        }
        //        if (selected == "Forms")
        //        {
        //            this.ContentFrame.Navigate(new Uri("/AddForm", UriKind.RelativeOrAbsolute));
        //        }
        //        if (selected == "Display Definitions")
        //        {
        //            this.ContentFrame.Navigate(new Uri("/ViewDisplayDefinition", UriKind.RelativeOrAbsolute));
        //        }
        //        if (selected == "View Users")
        //        {
        //            this.ContentFrame.Navigate(new Uri("/ViewUsers", UriKind.RelativeOrAbsolute));
        //        }


        //        if (selected == "Menu Settings")
        //        {
        //            this.ContentFrame.Navigate(new Uri("/ViewMenuStructure", UriKind.RelativeOrAbsolute));
        //        }
        //        //else
        //        //{
        //        //   this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));

        //        //}


        //    }
        //    catch (Exception ex)
        //    {
        //        this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));
        //       //MessageBox.Show(ex.Message);
        //    }
        //}



        private void ContentFrame_NavigationFailed(object sender, System.Windows.Navigation.NavigationFailedEventArgs e)
        {
            e.Handled = true;
            ContentFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }


        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            //foreach (UIElement child in LinksStackPanel.Children)
            //{
            //    HyperlinkButton hb = child as HyperlinkButton;
            //    if (hb != null && hb.NavigateUri != null)
            //    {
            //        if (hb.NavigateUri.ToString().Equals(e.Uri.ToString()))
            //        {
            //            VisualStateManager.GoToState(hb, "ActiveLink", true);
            //        }
            //        else
            //        {
            //            VisualStateManager.GoToState(hb, "InactiveLink", true);
            //        }
            //    }
            //}
        }

        private void homeMenu_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {


            try
            {

                string selected = homeMenu.SelectedItem.ToString();
                // MessageBox.Show(select   ed);
                header = selected;
                lblLabel.ApplicationName = header;
                lblTitle2.Text = header;
                if (header != "Custom Form")
                {
                    ///  toolBox.Visibility = System.Windows.Visibility.Collapsed; ;
                }
                switch (selected)
                {

                    case "Views":
                        if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                            this.ContentFrame.Navigate(new Uri("/ViewListsp", UriKind.Relative));
                        else
                            this.ContentFrame.Navigate(new Uri("/ViewList", UriKind.Relative));
                        break;


                    case "Databases":

                        this.ContentFrame.Navigate(new Uri("/ConfigPage", UriKind.RelativeOrAbsolute));
                        break;

                    case "Forms":

                        this.ContentFrame.Navigate(new Uri("/AddForm", UriKind.RelativeOrAbsolute));
                        break;
                    case "Grid Form":

                        this.ContentFrame.Navigate(new Uri("/ViewDisplayDefinition", UriKind.RelativeOrAbsolute));
                        break;
                    case "View Users":

                        this.ContentFrame.Navigate(new Uri("/ViewUsers", UriKind.RelativeOrAbsolute));
                        break;

                    case "Group Filtering":

                        this.ContentFrame.Navigate(new Uri("/ViewGroups", UriKind.RelativeOrAbsolute));
                        break;


                    case "Menu Settings":

                        this.ContentFrame.Navigate(new Uri("/ViewMenuStructure", UriKind.RelativeOrAbsolute));

                        break;

                    case "Custom Form":
                        //  toolBox.Visibility = System.Windows.Visibility.Visible;
                        this.ContentFrame.Navigate(new Uri("/ViewForm", UriKind.RelativeOrAbsolute));

                        break;

                    case "Model Interface":

                        this.ContentFrame.Navigate(new Uri("/ViewEOForm", UriKind.RelativeOrAbsolute));

                        break;

                    case "Back":

                        this.Content = new MainPage();
                        break;

                    case "Logout":
                        /// FormAuthentication()
                        WebContext.Current.Authentication.Logout(LogoutCompleteCallback, null);

                        break;


                }

            }
            catch (Exception ex)
            {
                this.ContentFrame.Navigate(new Uri("/ErrorPage", UriKind.RelativeOrAbsolute));
                //MessageBox.Show(ex.Message);
            }
        }

        private void homeMenu_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }


        private void CallbackUser(LoadOperation<UserInformation> loadedUser)
        {
            List<UserInformation> orig = loadedUser.Entities.ToList();


            if (orig != null)
            {
                var activeUser = orig.FirstOrDefault();
                Globals.CurrentUser = activeUser;

                userID = activeUser.ID;
                groupID = 0;
                if (activeUser.Group_ID != null)
                    groupID = activeUser.Group_ID.Value;


                LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => (m.MenuStructure_ID == activeUser.MenuStructure_ID || m.DisplayName == "Logout") && (m.User_ID == userID || m.User_ID == 0)), Callback, null);


            }

        }

        private void Callback(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadMenus)
        {
            if (loadMenus != null)
            {
                List<BMA.MiddlewareApp.Web.MenuItem> orig = loadMenus.Entities.ToList().OrderBy(x => x.SequenceNumber).ToList();

                this.tvHierarchyView.ItemsSource = orig;
                LoadData(orig);
                stackPanel221.LayoutUpdated += new EventHandler(LayoutRoot_LayoutUpdated);
            }
        }

        private void CallbackType(LoadOperation<FormType> lo)
        {
            if (lo.Entities.Count<FormType>() == 0)
                return;
            FormType orig = lo.Entities.FirstOrDefault();

            if (orig.Description == "Single")
            {


                try
                {
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        string docPaneName = title.Replace(" ", "");

                        Controls.OneGrid.DisplayID = DisplayID;

                        Controls.OneGrid.pageSize = pageSize;
                        ;
                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.OneGrid();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);

                    }





                }
                catch
                {

                }
            }
            if (orig.Description == "Double")
            {

                try
                {
                    Controls.TwoGrids.DisplayID = DisplayID;
                    Controls.TwoGrids.pageSize = pageSize;
                    Controls.TwoGrids.pageSize2 = pageSize;
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.TwoGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.TwoGrids();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "Tripple")
            {


                Controls.ThreeGrids.DisplayID = DisplayID;
                Controls.ThreeGrids.pageSize = pageSize;
                Controls.ThreeGrids.pageSize2 = pageSize;
                Controls.ThreeGrids.pageSize3 = pageSize;
                string docPaneName = title.Replace(" ", "");





                RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                if (pane != null)
                    MainTab.SelectedItem = pane;
                else
                {
                    //  string docPaneName = title.Replace(" ", "");

                    Controls.ThreeGrids.DisplayID = DisplayID;


                    RadTabItem tab = new RadTabItem();
                    tab.Content = new Controls.ThreeGrids();
                    // tab.Padding = 
                    tab.DropDownContent = title;
                    tab.Header = title;
                    tab.Name = title.Replace(" ", "") + menuID;
                    tab.IsSelected = true;
                    tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                    tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                    this.MainTab.Items.Add(tab);

                }

            }


            if (orig.Description == "Design")
            {

                try
                {
                    Controls.NewForm.DisplayID = DisplayID;

                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.NewForm.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.NewForm();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "EO")
            {

                try
                {
                    Controls.FixedThreeGrids.DisplayID = DisplayID;
                    Controls.FixedThreeGrids.pageSize = pageSize;
                    Controls.FixedThreeGrids.pageSize2 = pageSize;
                    Controls.FixedThreeGrids.pageSize3 = pageSize;
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.FixedThreeGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.FixedThreeGrids();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "Profile")
            {

                try
                {
                    Controls.ProfileGrid.DisplayID = DisplayID;
                    Controls.ProfileGrid.pageSize = pageSize;
                    Controls.ProfileGrid.pageSize2 = pageSize;
                    Controls.ProfileGrid.pageSize3 = pageSize;

                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.ProfileGrid.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.ProfileGrid();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "MTP")
            {

                try
                {
                    Controls.MTPGrids.DisplayID = DisplayID;
                    Controls.MTPGrids.pageSize = pageSize;
                    Controls.MTPGrids.pageSize2 = pageSize;
                    Controls.MTPGrids.pageSize3 = pageSize;
                    Controls.MTPGrids.ModelID = ModelID;
                    Controls.MTPGrids.ModelName = ModelName;
                    //     Controls.DefinitionGrid.Definition = 
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID + ModelID.ToString());

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.MTPGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.MTPGrids();
                        // tab.Padding = 

                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID + ModelID.ToString();
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        RadToolTip tooltip = new RadToolTip();
                        tooltip.Content = ModelName;
                        ToolTipService.SetToolTip(tab, tooltip);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }
        }

        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        ContextMenu cMenu;
        RadTabItem selectedTab;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            selectedTab = sender as RadTabItem;

            cMenu = new ContextMenu();
            System.Windows.Controls.MenuItem menuItem;

            menuItem = new System.Windows.Controls.MenuItem();
            menuItem.Header = "Close";
            menuItem.Click += new RoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new System.Windows.Controls.MenuItem();
            menuItem.Header = "Close All But This";
            menuItem.Click += new RoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.IsOpen = true;
            cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X;
            cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }

        void menuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.MenuItem menu = sender as System.Windows.Controls.MenuItem;

            switch (menu.Header.ToString())
            {
                case "Close":
                    CloseTab();
                    break;

                case "Close All But This":
                    CloseAllTab();
                    break;
                default:
                    break;
            }
            cMenu.IsOpen = false;
        }


        private void CloseTab()
        {
            try
            {
                this.MainTab.Items.Remove(selectedTab);
            }
            catch
            {
            }
        }


        private void CloseAllTab()
        {
            try
            {
                List<RadTabItem> tabs = new List<RadTabItem>();

                foreach (RadTabItem item in MainTab.Items)
                {
                    if (item != selectedTab)
                        tabs.Add(item);
                }
                foreach (RadTabItem item in tabs)
                {

                    this.MainTab.Items.Remove(item);
                }

            }
            catch
            {
            }
        }

        public T FindControl<T>(UIElement parent, Type targetType, string ControlName) where T : FrameworkElement
        {

            if (parent == null) return null;

            if (parent.GetType() == targetType && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }
            T result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                if (FindControl<T>(child, targetType, ControlName) != null)
                {
                    result = FindControl<T>(child, targetType, ControlName);
                    break;
                }
            }
            return result;
        }

        private void RadOutlookBar_SelectionChanged(object sender, RadSelectionChangedEventArgs e)
        {
            try
            {

                RadOutlookBarItem newSelectedItem = (sender as RadOutlookBar).SelectedItem as RadOutlookBarItem;
                if (newSelectedItem != null)
                {
                    string header = newSelectedItem.Title.ToString();

                    switch (header)
                    {
                        case "Admin Menu":
                            CustomContent.Visibility = System.Windows.Visibility.Collapsed;
                            lblLabel.Visibility = System.Windows.Visibility.Visible;
                            gridTitle.Visibility = System.Windows.Visibility.Visible;
                            ContentFrame.Visibility = System.Windows.Visibility.Visible;
                            break;

                        case "Custom Menu":
                            CustomContent.Visibility = System.Windows.Visibility.Visible;
                            lblLabel.Visibility = System.Windows.Visibility.Collapsed;
                            gridTitle.Visibility = System.Windows.Visibility.Collapsed;
                            ContentFrame.Visibility = System.Windows.Visibility.Collapsed;
                            string username = WebContext.Current.Authentication.User.Identity.Name;
                            //this.tvHierarchyView.AddHandler(Telerik.Windows.Controls.RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.radTreeView_MouseLeftButtonDown2), true);       

                            LoadOperation<UserInformation> loadOperation = context.Load(context.GetUserInformationWithMenuStructureQuery().Where(u => u.UserName == username), CallbackUser, null);
                            break;
                        case "User Information":

                            break;
                    }
                }
            }
            catch
            {
            }
        }
    }
    public class Category
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int ID{ get; set; }
        public string ImageUrl { get; set; }
        public int? DefID { get; set; }
        public int? PageSize { get; set; }
        public List<Category> SubCategories { get; set; }
    }
}
