﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using Telerik.Windows.Documents.Model;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ModifyRelationship
    {
        EditorContext context = new EditorContext();
        private int viewID;

        ObservableCollection<Table> tablesA;
        ObservableCollection<Table> tablesB;
        ObservableCollection<TempRelationship> tempRelationships;
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        Table tableA = new Table();
        Table tableB = new Table();
        private int tableA_ID;
        private int tableB_ID;
        private string tableA_Name;
        private string tableB_Name;
        public ModifyRelationship(int viewid,List<ViewRelationship> relationships)
        {
            InitializeComponent();
            viewID = viewid;
            relationshipList = relationships;
            loadRelationships();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (tempRelationships != null)
            {
                EditorContext context = new EditorContext();
                foreach (TempRelationship temp in tempRelationships)
                {
                    ViewTableRelationship vtrship = new ViewTableRelationship();
                    vtrship.View_ID = viewID;
                    vtrship.Table_ID_A = temp.TableA_ID;
                    vtrship.Table_ID_B = temp.TableB_ID;
                    vtrship.Field_ID_A = temp.FieldA_ID;
                    vtrship.Field_ID_B = temp.FieldB_ID;

                    context.ViewTableRelationships.Add(vtrship);

                }
                context.SubmitChanges(so =>
                {
                    if (so.HasError)
                    {
                      
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message + " Error adding new relationships..." });
                    }
                    else
                    {
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                        
                        this.Close();// = false;

                    }
                }, null);

            }


            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ddlTableA_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            Table table = ddlTableA.SelectedItem as Table;
            if (table != null)
            {
                if (tableB != null)
                  tablesB.Add(tableB);
                tableA_ID = table.ID;
                tableA_Name = table.TableName;

                tableB = table;
              tablesB.Remove(table);
                LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFieldA, null);
            }



        }

        private void CallbackFieldA(LoadOperation<Field> loadOp)
        {


            if (loadOp != null)
            {
                lstA.ItemsSource = loadOp.Entities;

               

            }
        }

        private void ddlTableB_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {

            Table table = ddlTableB.SelectedItem as Table;
            if (table != null)
            {
                if (tableA != null)
                    tablesA.Add(tableA);
                tableB_ID = table.ID;
                tableB_Name = table.TableName;
                tableA = table;
                tablesA.Remove(table);
                
                LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFieldB, null);
            }



        }

        private void CallbackFieldB(LoadOperation<Field> loadOp)
        {


            if (loadOp != null)
            {
                lstB.ItemsSource = loadOp.Entities;



            }
        }


        private void GetTables(object sender, RoutedEventArgs e)
        {
          
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViews, null);


        }

        private void GetTablesB(object sender, RoutedEventArgs e)
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackTableB, null);


        }
        private void CallbackTableB(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {
                tablesA = new ObservableCollection<Table>();
                tablesB = new ObservableCollection<Table>();

                foreach (Table tab in loadOp.Entities)
                {
                    
                    tablesB.Add(tab);
                }
                ddlTableB.ItemsSource = tablesB;

             

            }
        }
        private void CallbackViews(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {
                tablesA = new ObservableCollection<Table>();
                tablesB = new ObservableCollection<Table>();

                foreach (Table tab in loadOp.Entities)
                {
                    tablesA.Add(tab);
                    tablesB.Add(tab);
                }
                ddlTableB.ItemsSource = tablesB;

                ddlTableA.ItemsSource = tablesA;

            }
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {

            if ((lstA.SelectedItem == null) || (lstB.SelectedItem == null))
                return;
                if( tempRelationships == null)
                    tempRelationships = new ObservableCollection<TempRelationship>();
              
                
                 TempRelationship temp = new TempRelationship();
                 Field fieldA = lstA.SelectedItem as Field;
                 Field fieldB = lstB.SelectedItem as Field;
            
            //check if the relationship exists
                 var query = from f in tempRelationships
                             where f.TableA_ID == tableA_ID && f.TableB_ID == tableB_ID
                                     && f.FieldA_ID == fieldA.ID && f.FieldB_ID == fieldB.ID
                             select f;
                var query2 =from fd in relationshipList
                            where fd.TableA_ID == tableA_ID && fd.TableB_ID == tableB_ID
                                    && fd.FieldA_ID == fieldA.ID && fd.FieldB_ID == fieldB.ID
                            select fd;
                    
                 if ((query.Count<TempRelationship>() > 0) || (query2.Count<ViewRelationship>() > 0))
                 {
                     RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "This relationship already exists!" });
                    
                 }
                 else
                 {
                 temp.TableA_ID = tableA_ID;
                 temp.FieldA_ID = fieldA.ID;
                 temp.FieldA_Name = tableA_Name + "." + fieldA.FieldName;
           
                 temp.TableB_ID = tableB_ID;
                 temp.FieldB_ID = fieldB.ID;
                 temp.FieldB_Name = tableB_Name + "." + fieldB.FieldName;
                 tempRelationships.Add(temp);
                }
                 

        }

        private void loadRelationships()
        {
            tempRelationships = new ObservableCollection<TempRelationship>();
            radGridView1.ItemsSource = tempRelationships;
        }
    }
    

    public class TempRelationship
    {
        public int TableA_ID { get; set; }
       
        public int FieldA_ID { get; set; }
        public string FieldA_Name { get; set; }

        public int TableB_ID { get; set; }
        public int FieldB_ID { get; set; }
        public string FieldB_Name { get; set; }
    }
}

