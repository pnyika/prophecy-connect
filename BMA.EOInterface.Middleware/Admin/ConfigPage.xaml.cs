﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections;
using System.Collections.ObjectModel;
using System.Globalization;

using Telerik.Windows.Controls.GridView;
using SilverlightMessageBox;
using BMA.MiddlewareApp.Admin.UserControls;
using BMA.EOInterface.Middleware;
using Telerik.Windows.Controls;


namespace BMA.MiddlewareApp.Admin
{
    public partial class ConfigPage : Page
    {

        EditorContext context = new EditorContext();
        IEnumerable _lookup;
        ObservableCollection<DataObject> fieldDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> oldFieldDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<Field> editedFieldList = new ObservableCollection<Field>();
        ObservableCollection<DumpField> newEditedFieldList = new ObservableCollection<DumpField>();
        ObservableCollection<DumpField> newEditedFields = new ObservableCollection<DumpField>();
        ObservableCollection<Field> newFieldList = new ObservableCollection<Field>();
        ObservableCollection<Field> tempFieldList ;
        List<Table> tableLst = new List<Table>();
        List<StoredProcedureName> storedProcedureList= new List<StoredProcedureName>();
        ObservableCollection<Field> fieldObjects= new ObservableCollection<Field>();
        ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> _tables;
        private string tableName;
        private int tableID;
        private string databaseName;
        private string connString;
        private bool exist;
        public ConfigPage()
        {
            InitializeComponent();
            //this.pnlField.Visibility = Visibility.Collapsed;

        //    this.btnGetField.Visibility = Visibility.Collapsed;
            this.btnCheckFields.Visibility = Visibility.Collapsed;
            getServer();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

      
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            App app = (App)Application.Current;

            if ((app.serverObjectList.Where(a => a.ID > 1000000).Count() > 0))
            {
                string confirmText = "This page contains some changes, Do you want to save the changes?";
                RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmNavClosed));

               
            }

            base.OnNavigatingFrom(e);
        }


        private void OnConfirmNavClosed(object sender, WindowClosedEventArgs e)
        {
           
            if (e.DialogResult == true)
            {
                SaveToDB();
            }
            else
            {
            }
        }
        private void lstServer_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            App app = (App)Application.Current;
            app.tablebjectList = new ObservableCollection<Table>();
            //    ObservableCollection<Server>  serverObjectList = loadOp.Entities as ObservableCollection<Server>;
           
            //  app.serverObjectList = loadOp.Entities as ObservableCollection<Server>;
            //app.serverList = loadOp.Entities.ToList();
            lstServer.ItemsSource = app.serverObjectList;
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;

              
               
                if (server.ID > 1000000)
                {

                    connString = server.ConnectionString;
                    databaseName = server.DatabaseName;
                    if (app.tableList != null)
                    {
                        var tbls = from t in app.tableList
                                   where t.Server_ID == server.ID
                                   select t;
                        foreach (Table tbl in tbls)
                        {
                            app.tablebjectList.Add(tbl);
                        }
                     
                    }
                    lstTable.ItemsSource = app.tablebjectList;
                }
                else
                {

                    LoadOperation<Server> loadOp = context.Load(context.GetServersQuery().Where(x => x.ID == server.ID), CallbackConnServer, null);
                 

                }

                LoadSelectStoredProc();
        }
     

        private void CallbackConnServer(LoadOperation<Server> loadOp)
        {
            Server server = loadOp.Entities.FirstOrDefault();

            if (server != null)
            {
                connString = server.ConnectionString;
                databaseName = server.DatabaseName;

                LoadOperation<Table> loadOper = context.Load(context.GetTablesQuery().Where(x => x.Server_ID == server.ID), CallbackConnTable, null);

                //string sql = " Select name,Object_ID from sys.tables where type_desc = 'user_table' " ;
                //GetTables(sql, 1, 50, "Tables");


            }
        }


        private void CallbackConnTable(LoadOperation<Table> loadOp)
        {
            Server server = lstServer.SelectedItem as Server;
            App app = (App)Application.Current;

            if (loadOp.Entities != null)
            {
                foreach (Table tbl in loadOp.Entities)
                {
                    app.tablebjectList.Add(tbl);
                }
               


            }
            lstTable.ItemsSource = app.tablebjectList;
            if (app.tableList != null)
            {
                var tbls = from t in app.tableList
                           where t.Server_ID == server.ID
                           select t;
                foreach (Table tbl in tbls)
                {
                    app.tablebjectList.Add(tbl);
                }
            }

        }

        private void getServer()
        {
            //busyIndicator1.IsBusy = true;
            App app = (App)Application.Current;
            LoadOperation<Server> loadOp = context.Load(context.GetServersQuery(), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {
           

            if (loadOp != null)
            {
                App app = (App)Application.Current;
                app.serverObjectList = new ObservableCollection<Server>();
            //    ObservableCollection<Server>  serverObjectList = loadOp.Entities as ObservableCollection<Server>;
                foreach (Server srvr in loadOp.Entities)
                {
                    app.serverObjectList.Add(srvr);
                }
              //  app.serverObjectList = loadOp.Entities as ObservableCollection<Server>;
                //app.serverList = loadOp.Entities.ToList();
                lstServer.ItemsSource = app.serverObjectList;

            }
        }





        private void GetTables(string sql, int pagenumber, int pagesize, object userState)
        {
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                 Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                  _lookup = list;
              
                else
                {


                    lstTable.ItemsSource = list;



                }
            }
            
        }




        private void GetFields(string sql, int pagenumber, int pagesize, object userState)
        {
            radBusyIndicator.IsBusy = true; 
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetGetFieldsDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetGetFieldsDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                 Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;

                else
                {

                    //radGridView1.ItemsSource = list;

                    List<DumpField> lstF = new List<DumpField>();
                    foreach (DataObject obj in list)
                    {
                        DumpField fld = new DumpField();
                        string fieldName = obj.GetFieldValue("ColumnName").ToString();
                        string friendlyName = obj.GetFieldValue("DisplayName").ToString();

                       

                        string dname = friendlyName;
                        dname = System.Text.RegularExpressions.Regex.Replace(dname, @"\B([A-Z])", " $1");
                        dname = dname.Replace("_", " ");
                        // dname = myTI.ToTitleCase(dname);
                        fld.TableName = obj.GetFieldValue("TableName").ToString();
                        fld.Formart = "";
                        fld.FieldName = fieldName;
                        fld.FriendlyName = dname;
                        fld.Table_ID = 0;
                        string indx = obj.GetFieldValue("IndexColumn").ToString();
                        fld.isKey = byte.Parse(indx);

                        fld.Type = obj.GetFieldValue("ColumnType").ToString();

                        lstF.Add(fld);

                    }

                    radGridView1.ItemsSource = lstF;
                }
            }
            radBusyIndicator.IsBusy = false;
        }


        private string FieldInfoSql(string tableName, string DBName, bool isTable)
        {
            string viewOrTable = "tables";
            if(!isTable)
                viewOrTable = "views";

            string sql = "use [" + DBName+ "]";
            sql += "; Declare @TempTable Table(TableName varchar(100),ColumnName varchar(100),DisplayName varchar(100),ColumnType Varchar(50),IndexColumn int,[Readonly] int) insert into @TempTable  SELECT ";
            sql += "              t.name TableName,";
            sql += "               c.name ColumnName,  c.name DisplayName, ";
            sql += "               ColumnType = Case typ.name";
            sql += "                                      when 'varchar' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length) end + ')'";
            sql += "          when 'char' then typ.name + '(' + Convert(varchar(20),c.max_length)  + ')'";
            sql += "         when 'nvarchar' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length/2) end + ')'";
            sql += "        when 'nchar' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length/2) end + ')'";
            sql += "        when 'varbinary' then typ.name + '(' + case c.max_length when -1 then 'MAX' else Convert(varchar(20),c.max_length) end + ')'";
            sql += "        when 'binary' then typ.name + '(' + convert(varchar(10),c.max_length) + ')'";
            sql += "       when 'decimal' then typ.name + '(' +  convert(varchar(10),c.precision) + ',' +  convert(varchar(10),c.scale) + ')'";
            sql += "       when 'numeric' then typ.name + '(' +  convert(varchar(10),c.precision) + ',' +  convert(varchar(10),c.scale) + ')'";
            sql += "      when 'datetime2' then typ.name + '(' +  convert(varchar(10),c.scale) + ')'";
            sql += "     when 'datetimeoffset' then typ.name + '(' +  convert(varchar(10),c.scale) + ')'";
            sql += "      when 'time' then typ.name + '(' +  convert(varchar(10),c.scale) + ')'";
            sql += "   else convert(varchar(50),typ.name)";
            sql += "   end ,";
            sql += "        inx.index_column_id IndexColumn,";
            sql += "        [Readonly] = Case when c.is_identity = 1 or     c.is_computed = 1 then 1 else 0 end ";
            sql += "     FROM sys. "+viewOrTable+" t";
            sql += "      inner join sys.columns c on c.object_id = t.object_id";
            sql += "      inner join sys.types typ on typ.user_type_id = c.user_type_id";
            sql += "     Left outer join ";
            sql += "             (Select i.object_id, ic.column_id , ic.index_column_id";
            sql += "                  from sys.indexes i";
            sql += "                  inner join sys.index_columns ic on i.index_id = ic.index_id and i.object_id = ic.object_id";
            sql += "                 where i.is_primary_key = 1 ) as inx";
            sql += "     on inx.object_id = t.object_id and inx.column_id = c.column_id";
            sql += "    WHERE t.name = '" + tableName.Replace("[", "").Replace("]", "") + "'   Select * from @TempTable";


            return sql;
        }

        private void btnAddServer_Click(object sender, RoutedEventArgs e)
        {
            App app = (App)Application.Current;
            lstServer.ItemsSource = app.serverObjectList;
            AddServer addServer = new AddServer();
           
            addServer.Closed += ChildWin_Closed;

            addServer.Left = (Application.Current.Host.Content.ActualWidth - addServer.ActualWidth) / 2;
            addServer.Top = (Application.Current.Host.Content.ActualHeight - addServer.ActualHeight) / 2;
            addServer.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            addServer.ShowDialog();
        }

        void ChildWin_Closed(object sender, WindowClosedEventArgs e)
        {
            App app = (App)Application.Current;
          

           
            lstServer.ItemsSource = app.serverObjectList;
            //Server server = lstServer.SelectedItem as Server;
            //if (server == null)
            //    return;
            lstServer.SelectedItem = app.serverObjectList.Where(s => s.ID == AddServer.serverID).FirstOrDefault();
           
        }



        private void lstTable_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            Table table = lstTable.SelectedItem as Table;
            if (table == null)
                return;
          //  this.btnGetField.Visibility = Visibility.Visible;
            GetFields();
           
        }


        private void CallbackFields(LoadOperation<Field> loadOp)
        {
            tempFieldList = new ObservableCollection<Field>();

            if (loadOp != null)
            {
                this.radGridView1.Visibility = Visibility.Visible;
                //this.radGridView1.ItemsSource = loadOp.Entities;

               
                foreach (Field fld in loadOp.Entities)
                {
                   tempFieldList.Add(fld);
                }
                this.radGridView1.ItemsSource = tempFieldList;
            }
        }

        private void SaveToDB()
        {
            radBusyIndicator.IsBusy = true;
            try
            {

                App app = (App)Application.Current;
                foreach (Server server in app.serverObjectList)
                {
                    if (server.ID > 1000000)
                    {
                        Server sr = new Server();

                        sr.ConnectionString = server.ConnectionString;
                        sr.DatabaseName = server.DatabaseName;
                        sr.ServerName = server.ServerName;
                        sr.ServerType = server.ServerType;
                        sr.Usename = server.Usename;
                        sr.Password = server.Password;

                        context.Servers.Add(sr);

                        context.SubmitChanges(so =>
                        {
                            if (so.HasError)
                            {
                                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message }); 
                              
                            }
                            else
                            {
                                
                                

                                //insert associated tables
                                InsertTables(server.ConnectionString,server.DatabaseName, sr.ID, server.DatabaseName, server.ID);
                                app.serverObjectList.Remove(server);
                                app.serverObjectList.Add(sr);
                                lstServer.SelectedItem = sr;

                            }
                        }, null);


                    }



                    else
                    {
                        connString = server.ConnectionString;
                        databaseName = server.DatabaseName;
                        InsertTables(server.ConnectionString, server.DatabaseName, server.ID, server.DatabaseName, server.ID);
                        if (newFieldList.Count < 1)
                        {
                           // Message.InfoMessage("Successfully saved"); 
                        }
                    }
                }

                

                SaveNewField();
                SaveEditedField();
                
                tableLst = new List<Table>();
               
                
            }
            catch(Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message }); 
            }

            radBusyIndicator.IsBusy = false;
        }




        private void SaveNewField()
        {
            if (newFieldList.Count > 0)
            {
                EditorContext context = new EditorContext();

                foreach (Field field in newFieldList)
                {
                    context.Fields.Add(field);
                    
                }

                context.SubmitChanges(so =>
                        {
                            if (so.HasError)
                            {
                                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message }); 
                             
                                radBusyIndicator.IsBusy = false;
                            }
                            else
                            {
                                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successfully saved" }); 
                               radBusyIndicator.IsBusy = false;
                            }
                        }
                        , null);
             
                newFieldList = new ObservableCollection<Field>();
            }
           

        }

        private void SaveEditedField()
        {
            if (editedFieldList.Count > 0)
            {

                var ws = WCF.GetService();

                foreach (Field field in editedFieldList)
                {
                    ws.updateFieldAsync(field.ID, field.FriendlyName, field.Formart);

                }

                

                editedFieldList = new ObservableCollection<Field>();
            }

            radBusyIndicator.IsBusy = false;
        }





        private void InsertTables(string conString, string DBName, int serverID, string DB, int tempServerID)
        {
            try
            {
             
                App app = (App)Application.Current;
                
                if (app.tableList != null)
                {
                    var tbls = from t in app.tableList
                               where t.Server_ID == tempServerID
                               select t;



                    if (tbls.ToList().Count > 0)
                    {

                        foreach (Table table in tbls)
                        {
                          EditorContext  contextt = new EditorContext();
                            Table t = new Table();
                            t.Server_ID = serverID;
                            t.TableName = table.TableName;
                            t.DBName = DB;
                            t.IsTable = table.IsTable.Value;
                           
                           
                            contextt.Tables.Add(t);

                            contextt.SubmitChanges(so =>
                            {
                                if (so.HasError)
                                {
                                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message }); 
                                }
                                else
                                {

                                    Addedtable(conString, DBName, t.ID, t.TableName, t.IsTable.Value);
                                    app.tableList.Remove(table);
                                    app.tablebjectList.Remove(table);
                                    //if (!(app.tablebjectList.Contains(t)))
                                         //app.tablebjectList.Add(t);

                                  //   
                                    //tableID = t.ID;

                                    //tableLst.Add(t);
                                   

                                }
                            }, null);
                        }
                        app.tableList = new List<Table>();
                        app.tablebjectList = new ObservableCollection<Table>();
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" }); 
                      //  lstServer.SelectedIndex = lstServer.Items.Count - 1;
                    }
                    
                }
            }
            catch(Exception  ex)
            {
                
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message }); 
            }
        }




        private void InsertFields(string tablename, int tableID)
        {
            try
            {

                App app = (App)Application.Current;

               
                      
             }
                
           
            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message }); 
            }
        }



        private void Addedtable(string conString, string DBName, int tablID, string tableName, bool isTable)
        {
            if (tablID == 0)
                return;
         
            Table t = new Table();
            t.TableName = tableName;
            t.ID = tablID;
            t.Server_ID = 0;
            t.DBName = "";
            t.IsTable = isTable;
          tableLst.Add(t);


            string sql = FieldInfoSql(tableName, DBName, isTable);
                GetInsertFields(conString, DBName, tablID,sql, 1, 1000, "Data");
           
            //MessageBox.Show("Successfully saved");
        }


        private void GetInsertFields(string conString, string DBName, int ID, string sql, int pagenumber, int pagesize, object userState)
        {
            tableID = ID;
            radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new  EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetInsertFieldsDataSetDataCompleted);
            ws.GetDataSetDataAsync(conString, DBName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetInsertFieldsDataSetDataCompleted(object sender,  BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message }); 
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message }); 
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;

                else
                {
                    // add fields to observation collections

                    fieldObjects = new ObservableCollection<Field>();
                    
                    DataObject first = list.Cast<DataObject>().FirstOrDefault();

                    string tablename = first.GetFieldValue("TableName").ToString();

                    var tbl = (from t in tableLst
                               where t.TableName == tablename
                               select t).FirstOrDefault();
                    int tID = tbl.ID;


                  //  TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

                    foreach (DataObject obj in list)
                    {
                        Field fld = new Field();
                        string friendlyName = obj.GetFieldValue("DisplayName").ToString();
                        string fieldName = obj.GetFieldValue("ColumnName").ToString();

                        
                        if (newEditedFieldList != null)
                        {
                            var query = (from f in newEditedFieldList
                                         where f.TableName.ToString() == tablename && f.FieldName.ToString() == fieldName
                                        select f).FirstOrDefault();

                            if (query != null)
                            {
                                friendlyName = query.FriendlyName.ToString();
                            }
                           
                            
                        }

                        string dname = friendlyName;
                      //  dname = dname.Substring(0, 1).ToUpper() + dname.Substring(1);
                        dname = System.Text.RegularExpressions.Regex.Replace(dname, @"\B([A-Z])", " $1");
                        dname = dname.Replace("_", " ");

       
                        // dname = myTI.ToTitleCase(dname);

                        fld.Formart = "";
                        fld.FieldName = fieldName;
                        fld.FriendlyName = dname;
                        fld.Table_ID = tID;
                        string indx = obj.GetFieldValue("IndexColumn").ToString();
                        fld.isKey = byte.Parse(indx);

                        fld.Type = obj.GetFieldValue("ColumnType").ToString();
                        insertFieldRIA(fld);

                       
                    }

                    
                   

                }
            }
            radBusyIndicator.IsBusy = false;
        }




        private void insertFieldRIA(Field fld)
        {
            EditorContext cont = new EditorContext();
            cont.Fields.Add(fld);
            cont.SubmitChanges(so =>
            {
                if (so.HasError)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content =so.Error.Message }); 
                    
                }
                else
                {



                }
            }, null);
        }


        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                radBusyIndicator.IsBusy = true;
                SaveToDB();
               
               // radBusyIndicator.IsBusy = false;
            }
            catch(Exception ex)
            {
                radBusyIndicator.IsBusy = false;
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
          
        }

        private void btnGetField_Click(object sender, RoutedEventArgs e)
        {
          
        }

        private void GetFields()
        {
            Table table = lstTable.SelectedItem as Table;
            if (table == null)
                return;
            this.pnlField.Visibility = Visibility.Visible;

            App app = (App)Application.Current;
            if (table.ID > 1000000)
            {
                this.pnlField.Visibility = Visibility.Visible;
                string tbl =table.TableName;
                string sql = FieldInfoSql(tbl, databaseName, table.IsTable.Value);
                GetFields(sql, 1, 500, "Data");
                //if (app != null)
                //{
                //    var tbls = from t in app.tableList
                //               where t.Server_ID == server.ID
                //               select t;
                //    lstTable.ItemsSource = tbls;
                //}
                exist = false;

            }
            else
            {
                exist = true;
                LoadOperation<Field> loadOp = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == table.ID), CallbackFields, null);

            }
            this.btnCheckFields.Visibility = Visibility.Visible;
        }




        void InsertDataTable()
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(fieldDataObjectList as IEnumerable, _tables), "Data");

            ///.Progress.Start();
        }

        void ws_InsertCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                fieldDataObjectList = null;
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
            }
            //this.Progress.Stop();
        }

        private void btnAddTable_Click(object sender, RoutedEventArgs e)
        {
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;
            SelectTables tables = new SelectTables(server.ConnectionString, server.DatabaseName, server.DatabaseName, server.ID);
            tables.Left = (Application.Current.Host.Content.ActualWidth - tables.ActualWidth) / 2;
            tables.Top = (Application.Current.Host.Content.ActualHeight - tables.ActualHeight) / 2;
            tables.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            tables.ShowDialog();
           

            //App app = (App)Application.Current;
            //if (server.ID > 1000000)
            //{

            //    connString = server.ConnectionString;
            //    databaseName = server.DatabaseName;
            //    if (app.tableList != null)
            //    {
            //        var tbls = from t in app.tableList
            //                   where t.Server_ID == server.ID
            //                   select t;
            //        lstTable.ItemsSource = tbls;
            //    }

            //}
          
        }

        private void radGridView_RowEditEnded(object sender, Telerik.Windows.Controls.GridViewRowEditEndedEventArgs e)
        {
            try
            {
                
               //DataObject dataObject = e.EditedItem as DataObject;
               DumpField dataObject = e.EditedItem as DumpField;
               Field fieldDataObject = e.EditedItem as Field;
                if ((dataObject != null) || (fieldDataObject != null))
                {
                   
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (exist)
                        {
                           
                            if (!this.editedFieldList.Contains(fieldDataObject))
                            {

                                this.editedFieldList.Add(fieldDataObject);

                            }
                            else
                            {
                                this.editedFieldList.Remove(fieldDataObject);
                                this.editedFieldList.Add(fieldDataObject);
                            }
                        }

                        else
                        {
                            if (!this.newEditedFieldList.Contains(dataObject))
                            {

                                this.newEditedFieldList.Add(dataObject);

                            }
                            else
                            {
                                this.newEditedFieldList.Remove(dataObject);
                                this.newEditedFieldList.Add(dataObject);
                            }
                        }
                    }
                }

            }
            catch
            {

            }
        }

        private void btnCheckFields_Click(object sender, RoutedEventArgs e)
        {
            Table table = lstTable.SelectedItem as Table;
            if (table == null)
                return;
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;
          
            if (exist)
            {
                this.radGridView1.Visibility = Visibility.Visible;
                string tbl = table.TableName;
                string sql = FieldInfoSql(tbl, databaseName, table.IsTable.Value);
                CheckFields(sql, 1, 50, "Data");
               
                
            }
        }



        private void CheckFields(string sql, int pagenumber, int pagesize, object userState)
        {
            radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_CheckFieldsDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_CheckFieldsDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;

                else
                {
                    Table table = lstTable.SelectedItem as Table;
                    var lst = this.radGridView1.ItemsSource as ObservableCollection<DataObject>;

                    //foreach(DataObject dob in this.radGridView1.ItemsSource as ObservableCollection<DataObject>)
                    //{
                    //    existList.Add(dob);
                    //}

                    var query = from tb in list.Cast<DataObject>()
                                where !(from xt in tempFieldList select xt.FieldName).Contains(tb.GetFieldValue("ColumnName"))
                                select tb;


                    var queryback = from xt in tempFieldList
                                    where !(from tb in list.Cast<DataObject>() select tb.GetFieldValue("ColumnName")).Contains(xt.FieldName)
                                    select xt;

                    foreach (DataObject obj in query)
                    {


                        Field fld = new Field();

                        
                        if (query.Count<DataObject>() > 0)

                        fld.FieldName = obj.GetFieldValue("ColumnName").ToString();
                        fld.Formart = "";
                        string dname = obj.GetFieldValue("DisplayName").ToString();
                        dname = System.Text.RegularExpressions.Regex.Replace(dname, @"\B([A-Z])", " $1");
                        dname = dname.Replace("_", " ");
                        // dname = myTI.ToTitleCase(dname);
                        fld.FriendlyName = dname;
                        fld.Table_ID = table.ID;
                        string indx = obj.GetFieldValue("IndexColumn").ToString();
                        fld.isKey = byte.Parse(indx);

                        fld.Type = obj.GetFieldValue("ColumnType").ToString();
                        newFieldList.Add(fld);
                        tempFieldList.Add(fld);
                    }

                    if (queryback != null)
                    {
                        try
                        {
                            var ws = WCF.GetService();
                            foreach (Field field in queryback)
                            {
                                tempFieldList.Remove(field);
                                ws.DeleteFieldAsync(field.ID);
                                //EditorContext context = new EditorContext();
                                //context.Fields.Detach(field);
                                //context.SubmitChanges(so =>
                                //{
                                //    if (so.HasError)
                                //    {
                                       
                                //    }
                                //    else
                                //    {



                                //    }
                                //}, null);
                            }
                        }
                        catch { }
                    }
                    



                }
            }
            radBusyIndicator.IsBusy = false;
        }



        private bool isEdited(string tName, string fieldName)
        {
            var query = from f in newEditedFieldList
                        where f.TableName.ToString() == tName && f.FriendlyName.ToString() == fieldName
                        select f;
            if (query.Count<DumpField>() > 0)
           
                return true;
            
            else
            return false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            radBusyIndicator.IsBusy = false;
            //clear local storage
        }

        private void btnCheckFields_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void btnDeleteServer_Click(object sender, RoutedEventArgs e)
        {
           
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;
            string confirmText = "Do you want to delete  "  + server.DatabaseName + " server?";
            RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed));

            
            
           
          
        }

        private void OnConfirmClosed(object sender, WindowClosedEventArgs e)
        {
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;
            if (e.DialogResult == true)
            {
                radBusyIndicator.IsBusy = true;
                context.DeleteServerAndDepends(server.ID, del =>
                {
                    if (!del.HasError)
                    {

                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "successful" }, Content = "Successfully deleted!" });

                        getServer();
                        radBusyIndicator.IsBusy = false;
                    }
                    else
                    {
                        radBusyIndicator.IsBusy = false;
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = del.Error.Message });

                    }
                }, null);

                radBusyIndicator.IsBusy = false;
            }
        }

        private void btnModifyServer_Click(object sender, RoutedEventArgs e)
        {

            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;

            EditServer edit = new EditServer(server.ID);
            edit.Left = (Application.Current.Host.Content.ActualWidth - edit.ActualWidth) / 2;
            edit.Top = (Application.Current.Host.Content.ActualHeight - edit.ActualHeight) / 2;
            edit.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            edit.ShowDialog();
           
        }

        private void btnUseWizard_Click(object sender, RoutedEventArgs e)
        {
            Server server = lstServer.SelectedItem as Server;
            if (server != null)
            {
                DBSetUpControl.serverID = server.ID;
                DBTableControl.server = server;
            }
            else
            {
                DBSetUpControl.serverID = 0;
                DBTableControl.server = null;
            }
               
              
            this.NavigationService.Navigate(new Uri("/DatabaseWizard", UriKind.Relative));
        }

        private void btnAddSP_Click(object sender, RoutedEventArgs e)
        {
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;

            SelectStoredProcedures SelectStoredProc= new SelectStoredProcedures(server.ConnectionString, server.DatabaseName, server.DatabaseName, server.ID);
            SelectStoredProc.Closed +=SelectStoredProc_Closed;
            SelectStoredProc.Left = (Application.Current.Host.Content.ActualWidth - SelectStoredProc.ActualWidth) / 2;
            SelectStoredProc.Top = (Application.Current.Host.Content.ActualHeight - SelectStoredProc.ActualHeight) / 2;
            SelectStoredProc.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            SelectStoredProc.ShowDialog();
          
        }


      
        void SelectStoredProc_Closed(object sender, WindowClosedEventArgs e)
        {

            LoadSelectStoredProc();

        }

        private void LoadSelectStoredProc()
        {
            Server server = lstServer.SelectedItem as Server;
            if (server == null)
                return;
            LoadOperation<StoredProcedureName> loadOp = context.Load(context.GetStoredProcedureNamesQuery().Where(x => x.Server_ID == server.ID), CallbackConnServer, null);
 
        }


        private void CallbackConnServer(LoadOperation<StoredProcedureName> loadOp)
        {


            if (loadOp != null)
            {

                lstStoredProcedures.ItemsSource = loadOp.Entities;

            }
        }

    }

    public class DumpField
    {
        public int ID { get; set; }
        public int Table_ID { get; set; }
        public string FieldName { get; set; }
        public string TableName { get; set; }
        public string FriendlyName { get; set; }
        public string Type { get; set; }
        public string Formart { get; set; }
        public byte isKey { get; set; }
    }

}
