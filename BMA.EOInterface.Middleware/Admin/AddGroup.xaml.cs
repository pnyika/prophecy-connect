﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddGroup
    {
        public AddGroup()
        {
            InitializeComponent();
            OKButton.IsEnabled = false;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            addGroup();
          
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();// = false;
        }


        private void addGroup()
        {
          
            EditorContext context = new EditorContext();
                Group Group = new Group();
                Group.GroupName = txtGroupName.Text;
                Group.Description = txtDescription.Text;
                context.Groups.Add(Group);
           
          try
           {
               context.SubmitChanges(submit =>
               {
                   if (submit.HasError) {
                       //string alertText = "Successfully saved";

                       RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
                       this.Close();
                   }
                   else
                   {
                       //GroupList GroupList = new GroupList();
                       //GroupList.GroupList = new System.Collections.ObjectModel.ObservableCollection<Group>();
                       //GroupList.GroupList.Add(Group);
                       string alertText = "Successfully saved";

                       RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                       this.Close();
                   }
               }, null);
            }
            catch
            {
            }




        }

        private void txtGroupName_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);

        }
        public void EnableOrDisableOKButton(object sender, RoutedEventArgs e)
        {
            if (txtGroupName.Text.Length < 1)
                OKButton.IsEnabled = false;
            else
            {
                OKButton.IsEnabled = true;
               
            }
        }
    }
}

