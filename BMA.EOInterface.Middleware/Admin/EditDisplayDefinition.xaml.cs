﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using BMA.EOInterface.Middleware;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class EditDisplayDefinition : Page
    {
         ObservableCollection<TestDataRow> grids;
        private ObservableCollection<View> ViewList = new ObservableCollection<View>();
        DisplayDefinition displayDefinition = new DisplayDefinition();
        GridDefinition gridDefinition = new GridDefinition();
        int displayDefinitionID;
        List<FormType> formList = new List<FormType>();
        EditorContext context = new EditorContext();
        int numberOfGrids;
        string formName;
        public EditDisplayDefinition()
        {
            GetViews();
            this.Resources.Add("ViewList", ViewList);
            InitializeComponent();
            GetForms();
            LoadPageSizes();
            displayDefinitionID = ViewDisplayDefinition.publicDisplayDef.ID;
            LoadDisplayDef();
        }

        private void LoadDisplayDef()
        {
            LoadOperation<DisplayDefinition> loadOp = context.Load(context.GetDisplayDefinitionsQuery().Where(d => d.ID == displayDefinitionID), CallBackdisplayDefinitions, null);
        }

        private void CallBackdisplayDefinitions(LoadOperation<DisplayDefinition> result)
        {


            if (result.Error == null)
            {
                if (result.Entities != null)
                {
                    displayDefinition = result.Entities.FirstOrDefault();
                    txtDesription.Text = displayDefinition.Description;
                    formName = displayDefinition.Description; 
                    ddlPageSize.SelectedValue = displayDefinition.PageSize;
                }
            }
        }

        private void LoadPageSizes()
        {
            List<PageSize> pageSizeList = new List<PageSize>();
            for (int i = 10; i <= 1000; i += 5)
            {

                pageSizeList.Add(new PageSize { Size = i });
            }
            ddlPageSize.ItemsSource = pageSizeList;
           // ddlPageSize.SelectedValue = 25;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void GetViews()
        {

            LoadOperation<View> loadOp = context.Load(context.GetViewsQuery(), CallbackViews, null);


        }

        private void GetGridDefinition()
        {
            if (displayDefinitionID != 0)
            {
               
              
                LoadOperation<GridDefinition> loadOperation = context.Load(context.GetGridDefinitionsQuery().Where(x => x.DisplayDefinition_ID == displayDefinitionID), CallbackGridDef, null);
            }
        }

        private void CallbackGridDef(LoadOperation<GridDefinition> loadOperation)
        {
            if (loadOperation != null)
            {
                grids = new ObservableCollection<TestDataRow>();
                GridDefinition gridDef = loadOperation.Entities.FirstOrDefault();
                numberOfGrids = loadOperation.Entities.Count<GridDefinition>();
                foreach (GridDefinition grid in loadOperation.Entities)
                {
                    TestDataRow test = new TestDataRow();
                    View view = new View();
                    if (grid.View_ID != null)
                    {
                        view.ID = grid.View_ID.Value;
                        var vw = (from v in ViewList where v.ID == grid.View_ID select v).FirstOrDefault();
                        if (vw != null)
                        {
                            view.ViewName = vw.ViewName;
                        }
                        else
                        {
                            view.ViewName = "";
                        }
                    }
                    else
                    {
                        view.ID = 0;
                        view.ViewName = "";
                    }
                    test.GridNumber = Convert.ToInt32(grid.GridNumber);
                    test.Name = view.ViewName;
                    test.ViewName = view;
                    test.ID = grid.ID;
                    test.View_ID = grid.View_ID.Value;
                    grids.Add(test);
                }
                //lstForms.SelectedValue = gridDef.DisplayDefinition.FormType_ID.Value;
                dgMyDataGrid.ItemsSource = grids;
            }
        }
        //private void LoadFormType(int ID)
        //{
        //    LoadOperation<FormType> loadOperation = context.Load(context.GetFormTypesQuery().Where(x => x.ID == ID), CallbackForms, null);
        //}
        //private void CallbackForms(LoadOperation<FormType> loadOperation)
        //{
        //    if (loadOperation != null)
        //    {
        //        FormType form = loadOperation.Entities.FirstOrDefault();
        //        lstForms.SelectedValue = form.ID;
        //    }
        //}


        private void CallbackViews(LoadOperation<View> result)
        {


            if (result != null)
            {

                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                {
                    ObservableCollection<View> thisviewList = new ObservableCollection<View>(result.Entities.Where(v => v.GroupViews.Any(ug => ug.Group_ID == Globals.CurrentUser.Group_ID)).OrderBy(si => si.ViewName));

                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }
                else
                {
                    ObservableCollection<View> thisviewList = new ObservableCollection<View>(result.Entities.OrderBy(si => si.ViewName));

                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }
                ((GridViewComboBoxColumn)this.dgMyDataGrid.Columns["View_ID"]).ItemsSource = ViewList;
                GetGridDefinition();


            }
        }
        private void GetForms()
          {
         
            LoadOperation<FormType> loadOp = context.Load(context.GetFormTypesQuery(), CallbackDisplayDef, null);


        }



        private void CallbackDisplayDef(LoadOperation<FormType> loadOp)
        {


            if (loadOp != null)
            {
                lstForms.ItemsSource = loadOp.Entities;

                lstForms.SelectedValue = displayDefinition.FormType_ID;
                formList = loadOp.Entities.ToList();

            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            //grids =  new ObservableCollection<TestDataRow>();
           
            FormType form = lstForms.SelectedItem as FormType;
            if (form == null)
                return;

            
            int x = form.NumberOfGrids.Value;

            if (x > numberOfGrids)
            {
                int y = x - numberOfGrids;
                for (int i = 1; i <= y; i++)
                {
                    TestDataRow test = new TestDataRow();
                    View view = new View();
                    test.ID = 0;
                    test.GridNumber = numberOfGrids + i;
                    test.Name = "";
                    test.ViewName = view;
                    grids.Add(test);
                }
                dgMyDataGrid.ItemsSource = grids;
            }
            else
            {

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "The selected form contains less grid than the current form, You have to delete some grids inorder" });
               
            }
           
        }

        private void lstForms_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (displayDefinition == null)
            {
                grids = null;// new ObservableCollection<TestDataRow>();
                dgMyDataGrid.ItemsSource = grids;
            }
            FormType form = lstForms.SelectedItem as FormType;
            if (form == null)
                return;

            lblNumberOfGrids.Content = "Number of grids: " + form.NumberOfGrids.ToString();
        }



        private void dgMyDataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            dgMyDataGrid.BeginEdit();
        }



        private void CheckName()
        {
            LoadOperation<DisplayDefinition> loadOp = context.Load(context.GetDisplayDefinitionsQuery().Where(x => x.Description == txtDesription.Text), CallbackDisplayDefinition, null);
        }
        private void CallbackDisplayDefinition(LoadOperation<DisplayDefinition> loadOp)
        {


            if (loadOp == null)
            {
                if (loadOp.Entities.Count() > 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "The Grid Form Name already exists" });
                }
                else
                {
                    Save();
                }
            }
            else
            {
                if (loadOp.Entities.Count() > 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "The Grid Form Name already exists" });
                }
                else
                {
                    Save();
                }

            }
        }


        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
           // EditorContext newContext = new EditorContext();
            if (formName == txtDesription.Text)
            {
                Save();
            }
            else
            {
                CheckName();
            }
        }

        private void Save()
        {
            try
            {
                bool nullExist = false;

                FormType form = lstForms.SelectedItem as FormType;
                if (form == null)
                    return;

                var ws = WCF.GetService();
                //   DataTableService.DisplayDefinition displayDf = new DataTableService.DisplayDefinition();

                displayDefinition.FormType_ID = form.ID;
                displayDefinition.Description = txtDesription.Text;
                PageSize page = ddlPageSize.SelectedItem as PageSize;

                displayDefinition.PageSize = page.Size;
                // ws.UpdateDisplayDefinitionAsync(displayDf);

                foreach (var rowItem in dgMyDataGrid.Items)
                {
                    TestDataRow data = rowItem as TestDataRow;
                    if (data.ID == 0)
                    {

                        GridDefinition gridDef = new GridDefinition();
                        gridDef.DisplayDefinition_ID = displayDefinition.ID;
                        //  var vw = (from v in ViewList where v.ViewName == data.ViewName.ViewName select v).FirstOrDefault();
                        if (data.View_ID != null)
                        {
                            gridDef.View_ID = data.View_ID;
                        }
                        else
                        {
                            gridDef.View_ID = 0;
                        }

                        gridDef.GridNumber = data.GridNumber;
                        context.GridDefinitions.Add(gridDef);





                    }
                    else
                    {
                        BMA.EOInterface.Middleware.DataTableService.GridDefinition grid = new BMA.EOInterface.Middleware.DataTableService.GridDefinition();
                        grid.DisplayDefinition_ID = displayDefinition.ID;
                        // var vw = (from v in ViewList where v.ViewName == data.ViewName.ViewName select v).FirstOrDefault();
                        if (data.View_ID != null)
                        {
                            grid.View_ID = data.View_ID;
                        }
                        else
                        {
                            grid.View_ID = 0;
                        }

                        grid.GridNumber = data.GridNumber;
                        grid.ID = data.ID;
                        ws.UpdateGridDefinitionAsync(grid);
                    }

                }
                context.SubmitChanges(sub =>
                {
                    if (sub.HasError)
                    {
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = sub.Error.Message });
                    }
                    else
                    {
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                    }
                }, null);



            }





            catch (Exception ex)
            {

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });

            }
          
           
        }

        private void SaveDefinition()
        {
            FormType form = lstForms.SelectedItem as FormType;
            DisplayDefinition display = new DisplayDefinition();
            PageSize page = ddlPageSize.SelectedItem as PageSize;
            display.Description = txtDesription.Text;
            display.PageSize = page.Size;
            display.FormType_ID = form.ID;
            context.DisplayDefinitions.Add(display);
            context.SubmitChanges(submit =>
            {
                if (submit.HasError) {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An error have while processing your request!" });
                    }
                else
                {



                    foreach (var rowItem in dgMyDataGrid.Items)
                    {
                        TestDataRow data = rowItem as TestDataRow;

                        if (data.ViewName != null)
                        {
                            GridDefinition gridDef = new GridDefinition();
                            gridDef.DisplayDefinition_ID = display.ID;
                            gridDef.View_ID = data.View_ID;
                            gridDef.GridNumber = data.GridNumber;
                            context.GridDefinitions.Add(gridDef);
                            

                        }

                    }
                    context.SubmitChanges(sub => { if (sub.HasError) { RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = sub.Error.Message }); } else {
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                    }
                    }, null);
                }
            }, null);

        }

        private void btnNewView_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/ViewList", UriKind.Relative));
        }


        private void btnSet_Click(object sender, RoutedEventArgs e)
        {


            ViewDisplayDefinition.publicDisplayDef = displayDefinition;
            SetGridRelationship grid = new SetGridRelationship();

            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
            {
                if (displayDefinition.CreatedUser_ID != Globals.CurrentUser.ID)

                    
                     RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content ="You have no priledges to modify the selected item" });

                else
                {

                    if (formList.Where(f => f.ID == displayDefinition.FormType_ID.Value).FirstOrDefault().NumberOfGrids > 1)
                    grid.Show();
                    else
                        
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Single grid was specified. Therefore no relationships can be defined." });
                }
            }
            else
            {
                if (formList.Where(f => f.ID == displayDefinition.FormType_ID.Value).FirstOrDefault().NumberOfGrids > 1)
                grid.Show();
                else
                  
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Single grid was specified. Therefore no relationships can be defined." });
            }


        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                EditorContext cont = new EditorContext();
                TestDataRow data = dgMyDataGrid.SelectedItem as TestDataRow;
                if (data == null)
                    return;

                // confrim and delete from the database

                //remove from the grid
               BMA.EOInterface.Middleware.DataTableService.GridDefinition grid = new BMA.EOInterface.Middleware.DataTableService.GridDefinition();
                grid.ID = data.ID;
                grid.View_ID = data.View_ID;
                grid.DisplayDefinition_ID = displayDefinition.ID;
                grid.GridNumber = numberOfGrids;
                //cont.GridDefinitions.Remove(grid);
                //cont.SubmitChanges((EntriesRemoved) => { MessageBox.Show("Deleted!"); }, null);
                var ws = WCF.GetService();
                ws.DeleteGridDefinitionAsync(grid, null);
                grids.Remove(data);
                numberOfGrids = numberOfGrids - 1;
            }
            catch(Exception ex)
            {

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
                //Message.ErrorMessage(ex.Message);
            }
        }
    }
   

    }

