﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using BMA.EOInterface.Middleware;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddDisplayDefinition : Page
    {

        ObservableCollection<TestDataRow> grids;
        private ObservableCollection<View> ViewList = new ObservableCollection<View>();
        DisplayDefinition displayDefinition = new DisplayDefinition();
        EditorContext context = new EditorContext();
        public AddDisplayDefinition()
        {
            GetViews();
            this.Resources.Add("ViewList", ViewList);
            InitializeComponent();
            ddlDefinitionType.Visibility = System.Windows.Visibility.Collapsed;
            txtDef.Visibility = System.Windows.Visibility.Collapsed;

            ddlServer.Visibility = System.Windows.Visibility.Collapsed;
            txtServer.Visibility = System.Windows.Visibility.Collapsed;

            LoadPageSizes();
            GetDefOptions();
            GetServers();
        }



        private void LoadPageSizes()
        {
            List<PageSize> pageSizeList = new List<PageSize>();
            for (int i = 10; i <= 1000; i+=5)
            {

                pageSizeList.Add(new PageSize { Size = i });
            }
            ddlPageSize.ItemsSource = pageSizeList;
            ddlPageSize.SelectedValue = 25;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void GetViews()
        {

            LoadOperation<View> loadOp = context.Load(context.GetViewsQuery(), CallbackViews, null);


        }




        private void CallbackViews(LoadOperation<View> result)
        {


            if (result != null)
            {

                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                {
                    ObservableCollection<View> thisviewList = new ObservableCollection<View>(result.Entities.Where(v => v.GroupViews.Any(ug => ug.Group_ID == Globals.CurrentUser.Group_ID)).OrderBy(si => si.ViewName));
                   
                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }
                else
                {
                    ObservableCollection<View> thisviewList = new ObservableCollection<View>(result.Entities.OrderBy(si => si.ViewName));

                    foreach (var item in thisviewList)
                    {
                        ViewList.Add(item);
                    }
                }
                ((GridViewComboBoxColumn)this.dgMyDataGrid.Columns["View_ID"]).ItemsSource = ViewList;
            }
        }

        private void GetForms(object sender, RoutedEventArgs e)
          {
         
            LoadOperation<FormType> loadOp = context.Load(context.GetFormTypesQuery(), CallbackDisplayDef, null);


        }



        private void CallbackDisplayDef(LoadOperation<FormType> loadOp)
        {


            if (loadOp != null)
            {
                lstForms.ItemsSource = loadOp.Entities;

              //  lstForms.SelectedValue = displayDefinition.FormType_ID;


            }
        }


        private void GetDefOptions()
        {

            LoadOperation<DefinitionOption> loadOp = context.Load(context.GetDefinitionOptionsQuery(), CallbackDefOptions, null);


        }



        private void CallbackDefOptions(LoadOperation<DefinitionOption> loadOp)
        {


            if (loadOp != null)
            {

                ddlDefinitionType.ItemsSource = loadOp.Entities;

               


            }
        }


        private void GetServers()
        {

            LoadOperation<Server> loadOp = context.Load(context.GetServersQuery(), CallbackDefOptions, null);


        }



        private void CallbackDefOptions(LoadOperation<Server> loadOp)
        {


            if (loadOp != null)
            {

                ddlServer.ItemsSource = loadOp.Entities;




            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            grids =  new ObservableCollection<TestDataRow>();
            FormType form = lstForms.SelectedItem as FormType;
            if (form == null)
                return;

            if (form.Description == "MTP")
            {

                TestDataRow test = new TestDataRow();
                View view = new View();
                test.GridNumber = 1;
                test.ViewName = view;
               // test.View_ID = 0;
                test.ID = 1;
                grids.Add(test);
            }
            else
            {
                int x = form.NumberOfGrids.Value;
                for (int i = 1; i <= x; i++)
                {
                    TestDataRow test = new TestDataRow();
                    View view = new View();
                    test.GridNumber = i;
                    test.ViewName = view;
                    test.ID = i;
                    grids.Add(test);
                }

               
            }

            dgMyDataGrid.ItemsSource = grids;
        }

        private void lstForms_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
          
                grids = null;// new ObservableCollection<TestDataRow>();
                dgMyDataGrid.ItemsSource = grids;
           
            FormType form = lstForms.SelectedItem as FormType;
            if (form == null)
                return;


            if ((form.Description == "MTP"))
            {
                lblNumberOfGrids.Content = " Grids 2 to 5 are prepopulated , Totals Grids : " + form.NumberOfGrids.ToString();
            }
            else
            {
                lblNumberOfGrids.Content = "Number of grids: " + form.NumberOfGrids.ToString();
            }

          

            //if ((form.Description == "MTP"))
            //{
            //    ddlDefinitionType.Visibility = System.Windows.Visibility.Visible;
            //    txtDef.Visibility = System.Windows.Visibility.Visible;
            //    ddlServer.Visibility = System.Windows.Visibility.Visible;
            //    txtServer.Visibility = System.Windows.Visibility.Visible;
            //    lblNumberOfGrids.Visibility = System.Windows.Visibility.Collapsed;
            //    dgMyDataGrid.Visibility = System.Windows.Visibility.Collapsed;
            //    btnNewView.Visibility = System.Windows.Visibility.Collapsed;
            //    btnOK.Visibility = System.Windows.Visibility.Collapsed;

            //}

            //else
            //{

                ddlServer.Visibility = System.Windows.Visibility.Collapsed;
                txtServer.Visibility = System.Windows.Visibility.Collapsed;
                ddlDefinitionType.Visibility = System.Windows.Visibility.Collapsed;
                txtDef.Visibility = System.Windows.Visibility.Collapsed;
                lblNumberOfGrids.Visibility = System.Windows.Visibility.Visible;
                dgMyDataGrid.Visibility = System.Windows.Visibility.Visible;
                btnNewView.Visibility = System.Windows.Visibility.Visible;
                btnOK.Visibility = System.Windows.Visibility.Visible;
          //  }
        }



        private void dgMyDataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            dgMyDataGrid.BeginEdit();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool nullExist = false;

                FormType form = lstForms.SelectedItem as FormType;
                if (form == null)
                    return;

                //if (form.Description == "MTP")
                //{
                //    DefinitionOption def = ddlDefinitionType.SelectedItem as DefinitionOption;
                //    Server server = ddlServer.SelectedItem as Server;
                //    if (def == null)
                //    {
                //        Message.ErrorMessage("Select Definition Option!");
                //        return;
                //    }
                //    else if(server == null)
                //    {
                //        Message.ErrorMessage("Select Server!");
                //        return;
                //     }
                //        else
                //        {
                //             SaveDefinition();

                //        }
                //}
                //else
                //{

                foreach (var rowItem in dgMyDataGrid.Items)
                    {
                        TestDataRow data = rowItem as TestDataRow;

                        if (data.ViewName == null)
                        {
                            nullExist = true;
                        }
                    }

                    if (nullExist)
                    {
                        string confirmText = "Some grids have not been asigned  to view, do you want contiue saving ?";
                        RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed));

                       
                    }
                    else
                    {
                        CheckName();
                    }
               // }

            }
            catch(Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
               
            }
          
           
        }

        private void OnConfirmClosed(object sender, WindowClosedEventArgs e)
        {
           
            if (e.DialogResult == true)
            {
                CheckName();
               
            }
        }

        private void CheckName()
        {
            LoadOperation<DisplayDefinition> loadOp = context.Load(context.GetDisplayDefinitionsQuery().Where(x => x.Description == txtDesription.Text), CallbackDisplayDefinition, null);
        }
        private void CallbackDisplayDefinition(LoadOperation<DisplayDefinition> loadOp)
        {


            if (loadOp == null)
            {
                if (loadOp.Entities.Count() > 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "The Grid Form Name already exists" });
                }
                else
                {
                    SaveDefinition();
                }
            }
            else
            {
                if (loadOp.Entities.Count() > 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "The Grid Form Name already exists" });
                }
                else
                {
                    SaveDefinition();
                }
               
            }
        }

        private void SaveDefinition()
        {
            FormType form = lstForms.SelectedItem as FormType;
            PageSize page = ddlPageSize.SelectedItem as PageSize; 
            DisplayDefinition display = new DisplayDefinition();
            display.Description = txtDesription.Text;
            display.FormType_ID = form.ID;
            display.PageSize = page.Size;
            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                display.CreatedUser_ID = Globals.CurrentUser.ID;
            else
                display.CreatedUser_ID = 0;

            context.DisplayDefinitions.Add(display);
            context.SubmitChanges(submit =>
            {
                if (submit.HasError) { 
                   
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An error have while processing your request!" });
                }
                else
                {

                  //  if (form.Description == "MTP")
                   // {
                        //Server server = ddlServer.SelectedItem as Server;
                        //DefinitionOption def = ddlDefinitionType.SelectedItem as DefinitionOption;
                        //DisplayDefinitionOption option = new DisplayDefinitionOption();
                        //option.DefinitionOption_ID = def.ID;
                        //option.DisplayDefinition_ID = display.ID;
                        //option.Server_ID = server.ID;
                        //context.DisplayDefinitionOptions.Add(option);
                  //  }
                   // else
                 //   {

                    foreach (var rowItem in dgMyDataGrid.Items)
                        {
                            TestDataRow data = rowItem as TestDataRow;

                            if (data != null)
                            {
                                GridDefinition gridDef = new GridDefinition();
                                gridDef.DisplayDefinition_ID = display.ID;

                             //   var vw = (from v in ViewList where v.ViewName == data.ViewName.ViewName select v).FirstOrDefault();
                                if (data.View_ID != null)
                                {
                                    gridDef.View_ID = data.View_ID;
                                }
                                gridDef.GridNumber = data.GridNumber;
                                context.GridDefinitions.Add(gridDef);



                            }

                        }



                        if (form.Description == "MTP")
                             {
                                 GridDefinition gridDef = new GridDefinition();
                                 gridDef.DisplayDefinition_ID = display.ID;                                
                                  gridDef.View_ID = 101;                                 
                                 gridDef.GridNumber =2;
                                 context.GridDefinitions.Add(gridDef);


                                 GridDefinition gridDef3 = new GridDefinition();
                                 gridDef3.DisplayDefinition_ID = display.ID;
                                 gridDef3.View_ID = 110;
                                 gridDef3.GridNumber = 3;
                                 context.GridDefinitions.Add(gridDef3);

                                 GridDefinition gridDef4 = new GridDefinition();
                                 gridDef4.DisplayDefinition_ID = display.ID;
                                 gridDef4.View_ID = 109;
                                 gridDef4.GridNumber = 4;
                                 context.GridDefinitions.Add(gridDef4);

                                 GridDefinition gridDef5 = new GridDefinition();
                                 gridDef5.DisplayDefinition_ID = display.ID;
                                 gridDef5.View_ID = 111;
                                 gridDef5.GridNumber = 5;
                                 context.GridDefinitions.Add(gridDef5);
                              }



                        context.SubmitChanges(sub =>
                        {
                            if (sub.HasError) { RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = sub.Error.Message }); } else {
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                        } }, null);
                }
            }, null);

        }

        private void btnNewView_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/ViewList", UriKind.Relative));
        }

    }

    public class TestDataRow
    {
        public int ID { get; set; }
        public int GridNumber { get; set; }
        public string Name { get; set; }
        public View ViewName { get; set; }
        public int View_ID { get; set; }
    }

    public class ProcDataRow
    {
        public int ID { get; set; }
        public int GridNumber { get; set; }
        public string Name { get; set; }
        public View ViewName { get; set; }
        public int View_ID { get; set; }
    }


    public class PageSize
    {
        public int Size { get; set; }
        
    }

    /// <summary> 
    /// Extends the DataGrid. 
    /// </summary> 
    public static  class DataGridExtensions
    {
        /// <summary> 
        /// Gets the list of DataGridRow objects. 
        /// </summary> 
        /// <param name="grid">The grid wirhrows.</param> 
        /// <returns>List of rows of the grid.</returns> 
        public static ICollection<DataGridRow> GetRows(this DataGrid grid)
        {
            List<DataGridRow> rows = new List<DataGridRow>();

            foreach (var rowItem in grid.ItemsSource)
            {
                // Ensures that all rows are loaded. 
                grid.ScrollIntoView(rowItem, grid.Columns.Last());

                // Get the content of the cell. 
                FrameworkElement el = grid.Columns.Last().GetCellContent(rowItem);

                // Retrieve the row which is parent of given element. 
                DataGridRow row = DataGridRow.GetRowContainingElement(el.Parent as FrameworkElement);

                // Sometimes some rows for some reason can be null. 
                if (row != null)
                    rows.Add(row);
            }

            return rows;
        }
    }
}
