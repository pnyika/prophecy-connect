﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using SilverlightMessageBox;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewGroups : Page
    {
        public static ObservableCollection<UserInformation> userInformationList;
        EditorContext context = new EditorContext();
        public ViewGroups()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void MyRadDomainDataSource_SubmittedChanges(object sender, Telerik.Windows.Controls.DomainServices.DomainServiceSubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Avoids displaying errors, when deletion is prevented by DataBase constraints
                e.MarkErrorAsHandled();
            }
            else
            {
                string alertText = "Successfully saved";

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                //this.Close();
            }
        }

        private void btnAddGroup_Click(object sender, RoutedEventArgs e)
        {
            Group group = new Group();

            AddViewGroup addGroup = new AddViewGroup(group);
            addGroup.Left = (Application.Current.Host.Content.ActualWidth - addGroup.ActualWidth) / 2;
            addGroup.Top = (Application.Current.Host.Content.ActualHeight - addGroup.ActualHeight) / 2;
            addGroup.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            addGroup.ShowDialog();
           
        }

        private void btnModifyGroup_Click(object sender, RoutedEventArgs e)
        {
          Group group =   radGridView.SelectedItem as Group;
            if(group ==null)
                return;


            AddViewGroup addGroup = new AddViewGroup(group);
            addGroup.Left = (Application.Current.Host.Content.ActualWidth - addGroup.ActualWidth) / 2;
            addGroup.Top = (Application.Current.Host.Content.ActualHeight - addGroup.ActualHeight) / 2;
            addGroup.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            addGroup.ShowDialog();
        }

        private void btnFilterViewGroup_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;


            SetViewGroupFilter filterViewGroup = new SetViewGroupFilter(group);


            filterViewGroup.Left = (Application.Current.Host.Content.ActualWidth - filterViewGroup.ActualWidth) / 2;
            filterViewGroup.Top = (Application.Current.Host.Content.ActualHeight - filterViewGroup.ActualHeight) / 2;
            filterViewGroup.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            filterViewGroup.ShowDialog();
           
        }



        private void btnShowUsers_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;

         
            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(u=>u.Group_ID==group.ID), CallbackUsers, null);
        }
        private void ShowUsers()
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;


            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(u => u.Group_ID == group.ID), CallbackUsers, null);
        }
        private void CallbackUsers(LoadOperation<UserInformation> loadUsers)
        {
            userInformationList = new ObservableCollection<UserInformation>();

            if (loadUsers != null)
            {

                foreach (UserInformation user in loadUsers.Entities)
                {
                    userInformationList.Add(user);
                }

            }
            lstUsers.ItemsSource = userInformationList;
        }

        private void btnChangeUserMenu_Click(object sender, RoutedEventArgs e)
        {
            UserInformation user = lstUsers.SelectedItem as UserInformation;
            if (user == null)
                return;

            ChangeUserMenu change = new ChangeUserMenu(user);
            change.Closed += user_Closed;
            change.Left = (Application.Current.Host.Content.ActualWidth - change.ActualWidth) / 2;
            change.Top = (Application.Current.Host.Content.ActualHeight - change.ActualHeight) / 2;
            change.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            change.ShowDialog();

        }
        void user_Closed(object sender, WindowClosedEventArgs e)
        {

            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;

            ShowUsers();
        }

        private void btnSelectUser_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;
            AssignUsersGroup assign = new AssignUsersGroup(group.ID);
            assign.Closed += user_Closed;
            assign.Left = (Application.Current.Host.Content.ActualWidth - assign.ActualWidth) / 2;
            assign.Top = (Application.Current.Host.Content.ActualHeight - assign.ActualHeight) / 2;
            assign.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            assign.ShowDialog();
        }

        private void btnUnassign_Click(object sender, RoutedEventArgs e)
        {

            UserInformation user = lstUsers.SelectedItem as UserInformation;
            if (user == null)
                return;

            user.Group_ID = null;

            context.SubmitChanges(s => { if (!s.HasError) {
                ShowUsers();
            
            } else { } }, null);


        }

        private void btnAssignModel_Click(object sender, RoutedEventArgs e)
        {
            Group group = radGridView.SelectedItem as Group;
            if (group == null)
                return;


            AddModelGroup addGroup = new AddModelGroup(group);

            addGroup.Closed += user_Closed;
            addGroup.Left = (Application.Current.Host.Content.ActualWidth - addGroup.ActualWidth) / 2;
            addGroup.Top = (Application.Current.Host.Content.ActualHeight - addGroup.ActualHeight) / 2;
            addGroup.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            addGroup.ShowDialog();
         
        }

        private void btnAddGroup_Click_1(object sender, RoutedEventArgs e)
        {
               AddGroup view = new AddGroup();
               view.Closed += user_Closed;// +=new RadEventHandler(view_Closed);
               //view.WindowStartupLocation
               // view.Owner = 
               view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
               view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
               view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
               view.ShowDialog();
        }



        void view_Closed(object sender, WindowClosedEventArgs e)
             {

                 usersDomainDataSource.Load();
                 
             }

    }
}
