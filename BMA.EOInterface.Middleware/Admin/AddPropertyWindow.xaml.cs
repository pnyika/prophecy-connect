﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using System.Globalization;
using AmCharts.Windows.QuickCharts;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddPropertyWindow 
    {
       Person person;
       ObservableCollection<Control> controlList = new ObservableCollection<Control>();
       Control control;
       Control controlCompare;
       string objectType = "";
       ObservableCollection<Control> objectList = new ObservableCollection<Control>();
       List<ControlName> nameList = new List<ControlName>();
       public AddPropertyWindow(Control sender, string objType)
		{
			InitializeComponent();
            objectType = objType;
             control = sender;
             controlList.Add(control);
           if (objType == "RadButton")
           {
               RadButton radbutton = new RadButton();
               controlCompare = (Control)radbutton;
           }
           if (objType == "RadGridView")
           {
               RadGridView radGridView = new RadGridView();
               controlCompare = (Control)radGridView;
           }

           if (objType == "TextBox")
           {
               TextBox textBox = new TextBox();
               controlCompare = (Control)textBox;
           }

           if (objType == "TextBlock")
           {
               objectType = "Label";
               System.Windows.Controls.Label textBlock = new System.Windows.Controls.Label();
               controlCompare = (Control)textBlock;
           }
           if (objType == "SerialChart")
           {
               objectType = "SerialChart";
               SerialChart serial = new SerialChart();
               serial.CategoryValueMemberPath = "0";
               controlCompare = (Control)serial;
           }

           if (objType == "PieChart")
           {
               objectType = "PieChart";
               PieChart pie = new PieChart();
               //pie.CategoryValueMemberPath = "0";
               controlCompare = (Control)pie;
           }
			this.Loaded += new RoutedEventHandler(Page_Loaded);
            
             
		}

		void Page_Loaded(object sender, RoutedEventArgs e)
		{


            this.propertyGrid.SelectedObject = controlList.FirstOrDefault();
           
		}

     
   


	

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string xmlString  ="";
           if(objectType =="SerialChart")
             xmlString = ChartControlManager.CreateControlXmlString(controlList.FirstOrDefault(), controlCompare, objectType);
           else if (objectType == "PieChart")
                    xmlString = ChartControlManager.CreatePieControlXmlString(controlList.FirstOrDefault(), controlCompare, objectType);
           else               
             xmlString = ControlManager.CreateControlXmlString(controlList.FirstOrDefault(), controlCompare, objectType);

           SetControlProperties.XmlString = xmlString;
           this.Close();
        }

      
	}

    
}
