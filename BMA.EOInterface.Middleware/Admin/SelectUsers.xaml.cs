﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;


namespace BMA.MiddlewareApp.Admin
{
    public partial class SelectUsers
    {
        EditorContext context = new EditorContext();
        public static bool newUser;
      private   int menuID;
        public SelectUsers(int menuid)
        {
            InitializeComponent();
            newUser = false;
            menuID = menuid;
        }



        private void GetUnAssignedUsers(object sender, RoutedEventArgs e)
        {

            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(m => m.MenuStructure_ID == null), CallbackUsers, null);
        }

        private void CallbackUsers(LoadOperation<UserInformation> loadUsers)
        {
            //if(ViewMenuStructure.userInformationList == null)
            //    ViewMenuStructure.userInformationList = new ObservableCollection<UserInformation>();

            if (loadUsers != null)
            {
                lstUsers.ItemsSource = loadUsers.Entities;
                //foreach (UserInformation user in loadUsers.Entities)
                //{
                //    ViewMenuStructure.userInformationList.Add(user);
                //}

            }
           
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.lstUsers.SelectedItems.Count == 0)
            
                return;



            if (ViewMenuStructure.userInformationList == null)
                ViewMenuStructure.userInformationList = new ObservableCollection<UserInformation>();
            ObservableCollection<int> userIDs = new ObservableCollection<int>();
            foreach (UserInformation user in this.lstUsers.SelectedItems)
            {
                userIDs.Add(user.ID);
                ViewMenuStructure.userInformationList.Add(user);
            }
            var ws = WCF.GetService();
            ws.UpdateUserMenuCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.UpdateUserMenuCompletedEventArgs>(ws_UpdateUserMenuCompleted);
            ws.UpdateUserMenuAsync(userIDs, menuID);

          
        }

        void ws_UpdateUserMenuCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.UpdateUserMenuCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                string alertText = "Successfully saved";

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                this.Close();
             
            }
            else{
               

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
               // this.Close();
        }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();// = false;
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            newUser = true;
             this.DialogResult = true;
           
        }
        }
    }


