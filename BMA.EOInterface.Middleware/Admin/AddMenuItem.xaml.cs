﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;
using BMA.EOInterface.Middleware;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddMenuItem 
    {
        EditorContext context = new EditorContext();
        int menuId;
        int parentId;
        int sequenceMax;
        public AddMenuItem(int menuID, int parentID, int sequence)
        {
            InitializeComponent();
            menuId = menuID;
            parentId = parentID;
            sequenceMax = sequence;
            SetSequence();

        }

        private void SetSequence()
         {
             List<DropDownNumber> numberList = new List<DropDownNumber>();
             sequenceMax = sequenceMax + 1;
             for (int i = 1; i <= sequenceMax; i++)
             {
                 DropDownNumber num = new DropDownNumber();
                 num.Sequence = i;
                 numberList.Add(num);

             }
             comboBox1.ItemsSource = numberList;

                  
             List<ICon> iconList = new List<ICon>();
             iconList.Add(new ICon { IconName = "None", ImageUrl = "" });
             iconList.Add(new ICon {IconName="Table", ImageUrl="../Images/application-table.png" });
             iconList.Add(new ICon { IconName = "Grid", ImageUrl = "../../Images/Table.png" });
             iconList.Add(new ICon { IconName = "Server", ImageUrl = "../../Images/server.png" });
             iconList.Add(new ICon { IconName = "Database", ImageUrl = "../Images/database.png" });
             iconList.Add(new ICon { IconName = "Graph", ImageUrl = "../../Images/chart.png" });
             iconList.Add(new ICon { IconName = "Users", ImageUrl = "../../Images/users.png" });
             ddlIcon.ItemsSource = iconList;
          //   ddlIcon.SelectedIndex = 0;
         }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            bool isParent = false;
            if(chkIsParent.IsChecked == true)
            {
                isParent = true;
            }
            BMA.MiddlewareApp.Web.MenuItem menu = new BMA.MiddlewareApp.Web.MenuItem();
            menu.MenuStructure_ID = menuId;
            menu.ParentMenu_ID = parentId;
            menu.IsParent = isParent;
            menu.SequenceNumber = Convert.ToInt32(comboBox1.SelectedValue);
            menu.DisplayName = txtName.Text;
            ICon icon = ddlIcon.SelectedItem as ICon;
            if(icon != null)
            menu.ImageUrl = icon.ImageUrl;
            else
                menu.ImageUrl = "";

            if (WebContext.Current.Authentication.User.IsInRole("Super User"))
            {
                menu.User_ID = Globals.CurrentUser.ID;
            }
            else
            {
                menu.User_ID = 0;
            }

            context.MenuItems.Add(menu);
            context.SubmitChanges(submit => { if (submit.HasError) {

                
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
               // this.Close();
            } else {
                string alertText = "Successfully saved";

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                this.Close();
            } }, null);
                //0765168812 dense

            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
    public class DropDownNumber
    {
        public int Sequence { get; set; }
    }

    public class ICon
    {
        public string IconName { get; set; }
        public string ImageUrl { get; set; }
    }
}

