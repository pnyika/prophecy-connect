﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using BMA.MiddlewareApp.Admin.UserControls;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewMenuStructure : Page
    {
        EditorContext context = new EditorContext();
        public static ObservableCollection<UserInformation> userInformationList;
        private int menuID;
        public static MenuStructure menuStructure;
        public ViewMenuStructure()
        {
            InitializeComponent();
            btnSelectUser.IsEnabled = false;
            this.btnModify.IsEnabled = false;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void MyRadDomainDataSource_SubmittedChanges(object sender, Telerik.Windows.Controls.DomainServices.DomainServiceSubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Avoids displaying errors, when deletion is prevented by DataBase constraints
                e.MarkErrorAsHandled();
            }
            else
            {
                string alertText = "Successfully saved";

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
            }
        }


        private void btnShowMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuStructure menu = radGridView.SelectedItem as MenuStructure;
            if (menu == null)
                return;
            menuID = menu.ID;
            menuStructure = menu;
            this.btnModify.IsEnabled = true;
            // retrive menu items
            LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => m.MenuStructure_ID == menu.ID), Callback, null);
        }

        private void Callback(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadMenus)
        {
            if (loadMenus != null)
            {
                List<BMA.MiddlewareApp.Web.MenuItem> orig = loadMenus.Entities.ToList().OrderBy(x => x.SequenceNumber).ToList();

                this.tvHierarchyView.ItemsSource = orig;
                LoadData(orig);
            }
        }

        private void LoadData(List<BMA.MiddlewareApp.Web.MenuItem> elements)
        {
            List<Category> categories = new List<Category>();
            categories = this.GetCategories(elements);
            this.tvHierarchyView.ItemsSource = categories;
        }




        private List<Category> GetCategories(List<BMA.MiddlewareApp.Web.MenuItem> element)
        {
            int id = 0;
            return (from category in element.Where(p => p.ParentMenu_ID == 0)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        ImageUrl = category.ImageUrl,
                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private List<Category> GetChild(List<BMA.MiddlewareApp.Web.MenuItem> element, int menuLevel)
        {

            return (from category in element.Where(p => p.ParentMenu_ID == menuLevel)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        ImageUrl = category.ImageUrl,
                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }

        private void btnSelectUser_Click(object sender, RoutedEventArgs e)
        {
            SelectUsers users = new SelectUsers(menuID);
          
            users.Closed += user_Closed;
            users.Left = (Application.Current.Host.Content.ActualWidth - users.ActualWidth) / 2;
            users.Top = (Application.Current.Host.Content.ActualHeight - users.ActualHeight) / 2;
            users.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            users.ShowDialog();

        }
        void users_Closed(object sender, EventArgs e)
        {
            if (SelectUsers.newUser)
            {
                this.NavigationService.Navigate(new Uri("/ViewUsers", UriKind.Relative));
            }
            
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
           
            this.NavigationService.Navigate(new Uri("/ModifyMenuItems", UriKind.Relative));
        }

        private void btnShowUsers_Click(object sender, RoutedEventArgs e)
        {
            MenuStructure menu = radGridView.SelectedItem as MenuStructure;
            if (menu == null)
                return;

            menuID = menu.ID;
            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(m => m.MenuStructure_ID == menu.ID), CallbackUsers, null);
        }

        private void CallbackUsers(LoadOperation<UserInformation> loadUsers)
        {
            userInformationList = new ObservableCollection<UserInformation>();
           
            if (loadUsers != null)
            {
              
                foreach (UserInformation user in loadUsers.Entities)
                {
                    userInformationList.Add(user);
                }
                
            }
            lstUsers.ItemsSource = userInformationList;
        }

        private void btnChangeUserMenu_Click(object sender, RoutedEventArgs e)
        {
            UserInformation user = lstUsers.SelectedItem as UserInformation;
            if(user == null)
            return;

            ChangeUserMenu change = new ChangeUserMenu(user);

            change.Closed += user_Closed;


            change.Left = (Application.Current.Host.Content.ActualWidth - change.ActualWidth) / 2;
            change.Top = (Application.Current.Host.Content.ActualHeight - change.ActualHeight) / 2;
            change.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            change.ShowDialog();

        }

        void user_Closed(object sender, WindowClosedEventArgs e)
        {

            MenuStructure menu = radGridView.SelectedItem as MenuStructure;
            if (menu == null)
                return;

            menuID = menu.ID;
            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(m => m.MenuStructure_ID == menu.ID), CallbackUsers, null);
        }

        private void btnWizard_Click(object sender, RoutedEventArgs e)
        {
            //MenuStructure menu = radGridView.SelectedItem as MenuStructure;
            //if (menu != null)
            //{
            //    MenuSetupControl.menuID = menu.ID;
            //    MenuItemsControl.menuID = menu.ID;
            //    MenuPreviewControl.menuID = menu.ID;
            //}
            //    else{

                    MenuSetupControl.menuID = 0;
                    MenuItemsControl.menuID = 0;
                    MenuPreviewControl.menuID = 0;
              //  }
            this.NavigationService.Navigate(new Uri("/MenuWizard", UriKind.Relative));

        }


        private void btnAddGroup_Click_1(object sender, RoutedEventArgs e)
        {

            AddMenuStructure view = new AddMenuStructure();
            view.Closed += view_Closed;
            view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
            view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
            view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            view.ShowDialog();
        }



        void view_Closed(object sender, WindowClosedEventArgs e)
        {

            usersDomainDataSource.Load();

        }


    }
}
