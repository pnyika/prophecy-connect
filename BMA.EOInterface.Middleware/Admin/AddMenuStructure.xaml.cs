﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddMenuStructure 
    {
        public AddMenuStructure()
        {
            InitializeComponent();
            OKButton.IsEnabled = false;
            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            addGroup();
          
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void addGroup()
        {
          
            EditorContext context = new EditorContext();
            MenuStructure menu = new MenuStructure();
                menu.MenuName = txtGroupName.Text;
                menu.Description = txtDescription.Text;
                context.MenuStructures.Add(menu);
           
          try
           {
               context.SubmitChanges(submit =>
               {
                   if (submit.HasError) {
                       //string alertText = "Successfully saved";

                       RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
                      // this.Close();
                   
                   }
                   else
                   {
                       string alertText = "Successfully saved";

                       RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                       this.Close();
                   }
               }, null);
            }
            catch
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error" });
            }




        }

        private void txtGroupName_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);

        }
        public void EnableOrDisableOKButton(object sender, RoutedEventArgs e)
        {
            if (txtGroupName.Text.Length < 1)
                OKButton.IsEnabled = false;
            else
            {
                OKButton.IsEnabled = true;
               
            }
        }
    }
}

