﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddViewGroup 
    {
        EditorContext context = new EditorContext();
        ObservableCollection<View> viewList = new ObservableCollection<View>();
        ObservableCollection<View> currentView = new ObservableCollection<View>();
        List<View> addedView = new List<View>();
        List<View> removedViewed = new List<View>();
       

        int groupID = 0;
        public AddViewGroup(Group thisGroup)
        {
            InitializeComponent();
            groupID= thisGroup.ID;
            if (thisGroup.GroupName != string.Empty)
            {
                txtGroupName.Text = thisGroup.GroupName;
            }

            loadViews();
        }


    
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //Group group = new Group();
            //group.GroupName = txtGroupName.Text;
            //group.Description = "";
            //context.Groups.Add(group);
           
            
            foreach (View view in addedView)
            {
                GroupView groupView = new GroupView();
                groupView.Group_ID = groupID;
                groupView.View_ID = view.ID;
                context.GroupViews.Add(groupView);
            }

            foreach (View view in removedViewed)
            {

                context.GroupViews.Remove(view.GroupViews.Where(v=>v.View_ID==view.ID && v.Group_ID== groupID).FirstOrDefault());
            }       
            context.SubmitChanges(submit =>
            {
                if (!submit.HasError)
                {

                    string alertText = "Successful";

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                    this.Close();
                }
                else
                {
                    
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });

                }
            }, null);

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void loadViews()
        {

            LoadOperation<View> loadOp = context.Load(context.GetViewsQuery(), CallbackViews, null);




        }


        private void CallbackViews(LoadOperation<View> result)
        {

            
            if (result != null)
            {
               currentView = new ObservableCollection<View>(result.Entities.Where(v => v.GroupViews.Any(ug => ug.Group_ID== groupID)).OrderBy(si => si.ViewName));


               ObservableCollection<View> viewResults = new ObservableCollection<View>(result.Entities);

              
             
                foreach (View view in result.Entities)
                {
                    if (!currentView.Contains(view))
                    viewList.Add(view);
                }
                lstViewList.ItemsSource = viewList;
                lstView.ItemsSource = currentView;

            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lstViewList.SelectedItems.Count > 0)
                {
                  
                    List<View> selectedItems = new List<View>();
                    foreach(var item in lstViewList.SelectedItems)
                    {
                        selectedItems.Add(item as View);
                    }

                    foreach (var item in selectedItems)
                    {
                        View view = item as View;
                        addedView.Add(view);
                        viewList.Remove(view);
                        currentView.Add(view);

                        if (removedViewed.Contains(view))
                            removedViewed.Remove(view);

                    }
                }
            }
            catch
            {
            }

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lstView.SelectedItems.Count > 0)
            {
                List<View> selectedItems = new List<View>();
                foreach (var item in lstView.SelectedItems)
                {
                    selectedItems.Add(item as View);
                }
                foreach (var item in selectedItems)
                {
                    View view = item as View;
                    removedViewed.Add(view);
                    viewList.Add(view);
                    currentView.Remove(view);
                    if (addedView.Contains(view))
                        addedView.Remove(view);
                    
                }
            }
        }
    }
    public class TempDisplay
    {
        public string Value { get; set; }
        public string Display { get; set; }

    }
}

