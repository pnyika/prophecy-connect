﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using System.Collections;
using SilverlightMessageBox;
using System.ServiceModel.DomainServices.Client;
using BMA.EOInterface.Middleware;


namespace BMA.MiddlewareApp.Admin.UserControls
{
    public partial class DBSetUpControl : UserControl
    {
        IEnumerable _lookup;
        ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> _tables;
        EditorContext context = new EditorContext();
        private string connString = "";
        public static int serverID;
        bool isSave = false;
        Server server = new Server();
        public DBSetUpControl()
        {
            InitializeComponent();
            if(serverID >0)
            loadServer(serverID);
             this.OKButton.IsEnabled = false;
        }

        private void loadServer(int serverid)
        {
            context.Load<Server>(context.GetServersQuery().Where(s => s.ID == serverid), LoadBehavior.RefreshCurrent, (SitesLoaded) =>
            {
                if (!SitesLoaded.HasError)
                {
                    server = SitesLoaded.Entities.FirstOrDefault();

                    txtSource.Text = server.ServerName.Replace(server.DatabaseName, "");

                    txtID.Text = server.Usename;
                    txtpass.Password = server.Password;
                    ddlDatabase.SelectedValue = server.DatabaseName;
                    GetDatabases();

                }

            }, null);

        }

        private void GetDatabases()
        {

            string sql = " SELECT [name] FROM master.dbo.sysdatabases WHERE dbid > 4  order by [name] ";
            GetData(sql, 1, 1000, null);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.radBusyIndicator.IsBusy = true;
            string pass = txtpass.Password;
            string catalog = "Master";
            string source = txtSource.Text;
            string id = txtID.Text;

            connString = "Password=" + pass + ";Persist Security Info=True;User ID=" + id + ";Initial Catalog=" + catalog + ";Data Source=" + source;
            string connString1 = "Provider=SQLOLEDB.1;Password=" + pass + ";Persist Security Info=True;User ID=" + id + ";Initial Catalog=" + catalog + ";Data Source=" + source;

            TestConnection2(connString1);
           
           
            
        }
        private void TestConnection2(string connString)
        {
            var ws = WCF.GetService();
            ws.TestConnectionCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TestConnectionCompletedEventArgs>(ws_TestCompleted2);
            ws.TestConnectionAsync(connString);
        }


        void ws_TestCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.TestConnectionCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    // this.OKButton.IsEnabled = true;
                    Save();
                }

                else
                    Message.ErrorMessage("Failed on testing your Database connection");
            }
            else
            {
                Message.ErrorMessage(e.Error.Message);


            }

            this.radBusyIndicator.IsBusy = false;
        }

        private void Save()
        {
            App app = (App)Application.Current;

            Server server = new Server();
            server.ID = app.serverID + 1;
            app.serverID = server.ID;
            server.ServerName = txtSource.Text + " " + ddlDatabase.SelectedValue.ToString();
            server.ServerType = "SQL SERVER 2008 R2";
            server.Usename = txtID.Text;
            server.Password = txtpass.Password;
            server.DatabaseName = ddlDatabase.SelectedValue.ToString();
            server.ConnectionString = connString;
            //   app.serverList.Add(server);
            var tbls = from t in app.serverObjectList
                       where t.DatabaseName == ddlDatabase.SelectedValue.ToString() && t.ServerName == server.ServerName
                       select t;
            if (tbls.Count<Server>() == 0)
            {
                app.serverObjectList.Add(server);
                DBTableControl.server = server;

               // SelectTables tables = new SelectTables(connString, ddlDatabase.SelectedValue.ToString(), txtSource.Text, server.ID);
               // serverID = server.ID;
              //  tables.Show();
            }
            else
            {
                var sr = tbls.FirstOrDefault();
                DBTableControl.server = server;

              //  serverID = sr.ID;
                //SelectTables tables = new SelectTables(sr.ConnectionString, sr.DatabaseName, txtSource.Text, sr.ID);
              //  tables.Show();
            }

           // this.DialogResult = false;
          
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
          //  this.DialogResult = false;
        }

        private void radButton1_Click(object sender, RoutedEventArgs e)
        {
            this.radBusyIndicator.IsBusy = true;
            string pass = txtpass.Password;
            string catalog = "Master";
            string source = txtSource.Text;
            string id = txtID.Text;

             connString = "Password=" + pass + ";Persist Security Info=True;User ID=" + id + ";Initial Catalog=" + catalog + ";Data Source=" + source;
            string connString1 = "Provider=SQLOLEDB.1;Password=" + pass + ";Persist Security Info=True;User ID=" + id + ";Initial Catalog=" + catalog + ";Data Source=" + source;

            TestConnection(connString1);

        }


        private void TestConnection(string connString)
        {
            var ws = WCF.GetService();
            ws.TestConnectionCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TestConnectionCompletedEventArgs>(ws_TestCompleted);
            ws.TestConnectionAsync(connString);
        }


        void ws_TestCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.TestConnectionCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                   // this.OKButton.IsEnabled = true;
                    Message.InfoMessage("Successful");
                }

                else
                    Message.ErrorMessage("Fail");
            }
            else
            {
                Message.ErrorMessage(e.Error.Message);


            }

            this.radBusyIndicator.IsBusy = false;
        }



        private void GetData(string sql, int pagenumber, int pagesize, object userState)
        {
            string catalog = "master";

            connString = "Password=" + txtpass.Password + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + catalog + ";Data Source=" + txtSource.Text;
            this.radBusyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, "master", sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                 
                    ddlDatabase.ItemsSource = list;
                    


                }
            }
            this.radBusyIndicator.IsBusy = false;
        }




        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            string sql =" SELECT [name] FROM master.dbo.sysdatabases WHERE dbid > 4 order by [name] ";
            GetData(sql, 1, 200, null);
        }

        //private void ddlDatabase_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    connString = "Password=" + txtpass.Text + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + ddlDatabase.SelectedValue.ToString() + ";Data Source=" + txtSource.Text;
        //    this.OKButton.IsEnabled = true;
        //}

        private void ddlDatabase_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                connString = "Password=" + txtpass.Password + ";Persist Security Info=True;User ID=" + txtID.Text + ";Initial Catalog=" + ddlDatabase.SelectedValue.ToString() + ";Data Source=" + txtSource.Text;
                this.OKButton.IsEnabled = true;
            }
            catch { }
        }
    }

}