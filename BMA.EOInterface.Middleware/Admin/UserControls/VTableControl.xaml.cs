﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin.UserControls
{
    public partial class VTableControl : UserControl
    {

        EditorContext context = new EditorContext();
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList = new List<ViewFieldSummary>();

        List<ViewField> viewfieldList = new List<ViewField>();
        List<Table> tableList = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary;
        public static int  viewID;
        private int tableID;
        public VTableControl()
        {
            InitializeComponent();
          //  viewID = 74;
        }

        void radGridView1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                MessageBox.Show("2");
            }
        }

        // Executes when the user navigates to this page.
      
  

        private void GetTables(object sender, RoutedEventArgs e)
        {
            //viewID = 1;//Convert.ToInt32(NavigationContext.QueryString["viewID"]);
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViews, null);


        }
        private void RefreshTables()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViews, null);


        }


        private void CallbackViews(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {
                lstTable.ItemsSource = loadOp.Entities;
                tableList = loadOp.Entities.ToList();


            }
        }


        private void btnTable_Click(object sender, RoutedEventArgs e)
        {
            AddTable table = new AddTable(viewID, tableList);
            table.Closed += Table_Closed;
             table.Left = (Application.Current.Host.Content.ActualWidth - table.ActualWidth) / 2;
                 table.Top = (Application.Current.Host.Content.ActualHeight - table.ActualHeight) / 2;
                 table.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                 table.ShowDialog();
           

        }
        void Table_Closed(object sender, WindowClosedEventArgs e)
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViews, null);
        }

      
        private void radButton1_Click(object sender, RoutedEventArgs e)
        {
            Table table = lstTable.SelectedItem as Table;
            if (table == null)
                return;

            tableID = table.ID;

            CustomMessage customMessage = new CustomMessage("Do you want to delete  " + table.TableName + " table?", CustomMessage.MessageType.Confirm);

            customMessage.OKButton.Click += (obj, args) =>
            {
                DeleteViewTable();
            };
            customMessage.Show();


        }

        private void DeleteViewTable()
        {
            LoadOperation<ViewTable> loadOp = context.Load(context.GetViewTablesQuery().Where(vt => vt.Table_ID == tableID && vt.View_ID == viewID), CallbackViewTabels, null);
        }

        private void DeleteViewFieds()
        {
            LoadOperation<ViewField> loadOp = context.Load(context.GetViewFieldsQuery().Where(vt => vt.Table_ID == tableID && vt.View_ID == viewID), CallbackViewFields, null);
        }


        private void DeleteViewReleationship()
        {
            LoadOperation<ViewTableRelationship> loadOp = context.Load(context.GetViewTableRelationshipsQuery().Where(vt => (vt.Table_ID_A == tableID || vt.Table_ID_B == tableID) && vt.View_ID == viewID), CallbackViewReleationships, null);
        }

        private void CallbackViewTabels(LoadOperation<ViewTable> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewTable viewTable in loadOp.Entities)
                    {
                        context.ViewTables.Remove(viewTable);
                    }


                }
                DeleteViewFieds();
            }
            catch
            {
            }


        }


        private void CallbackViewFields(LoadOperation<ViewField> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewField viewTable in loadOp.Entities)
                    {
                        context.ViewFields.Remove(viewTable);
                    }


                }
                DeleteViewReleationship();
            }
            catch
            {
            }

        }


        private void CallbackViewReleationships(LoadOperation<ViewTableRelationship> loadOp)
        {
            try
            {

                if (loadOp != null)
                {
                    foreach (ViewTableRelationship viewRship in loadOp.Entities)
                    {
                        context.ViewTableRelationships.Remove(viewRship);
                    }


                }
            }
            catch
            {
            }

            context.SubmitChanges(submit => { if (submit.HasError) { Message.ErrorMessage(submit.Error.Message); context.RejectChanges(); } else { Message.InfoMessage("Successfully Saved!"); RefreshTables(); } }, null);

        }

       


    }
}
