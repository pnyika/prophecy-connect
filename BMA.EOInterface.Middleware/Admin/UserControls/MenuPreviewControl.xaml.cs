﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using SilverlightMessageBox;

namespace BMA.MiddlewareApp.Admin.UserControls
{
    public partial class MenuPreviewControl : UserControl
    {
        EditorContext context = new EditorContext();
        ObservableCollection<BMA.MiddlewareApp.Web.MenuItem> menuItemsList;
        public static int menuID;
        public MenuPreviewControl()
        {
            InitializeComponent();
            if(menuID != 0)
           LoadMenu();
        }

        private void LoadMenu()
        {
            //textMenu.Text = ViewMenuStructure.menuStructure.MenuName;
            LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => m.MenuStructure_ID == menuID), Callback, null);
        }

        private void Callback(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadMenus)
        {
            if (loadMenus != null)
            {
                menuItemsList = new ObservableCollection<BMA.MiddlewareApp.Web.MenuItem>();
                foreach (var menu in loadMenus.Entities)
                {
                    menuItemsList.Add(menu);
                }

                List<BMA.MiddlewareApp.Web.MenuItem> orig = loadMenus.Entities.ToList().OrderBy(x => x.SequenceNumber).ToList();

                this.tvHierarchyView.ItemsSource = orig;
                LoadData(orig);
            }
        }

        private void LoadData(List<BMA.MiddlewareApp.Web.MenuItem> elements)
        {
            List<Category> categories = new List<Category>();
            categories = this.GetCategories(elements);
            this.tvHierarchyView.ItemsSource = categories;
        }




        private List<Category> GetCategories(List<BMA.MiddlewareApp.Web.MenuItem> element)
        {
            int id = 0;
            return (from category in element.Where(p => p.ParentMenu_ID == 0)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,

                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private List<Category> GetChild(List<BMA.MiddlewareApp.Web.MenuItem> element, int menuLevel)
        {

            return (from category in element.Where(p => p.ParentMenu_ID == menuLevel)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,

                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private void radTreeView_DragEnded(object sender, Telerik.Windows.Controls.RadTreeViewDragEndedEventArgs e)
        {
            // Get the dragged items.
            Collection<System.Object> draggedItems = e.DraggedItems;
            // Get the drop position.
            Telerik.Windows.Controls.DropPosition dropPosition = e.DropPosition;
            switch (dropPosition)
            {
                case DropPosition.After:
                    MessageBox.Show("After");
                    break;
                case DropPosition.Before:
                    MessageBox.Show("Before");
                    break;
                case DropPosition.Inside:
                    MessageBox.Show("Inside");
                    break;
            }
            // Get is canceled
            bool isCanceled = e.IsCanceled;
            // Target drop item
            RadTreeViewItem targetDropItem = e.TargetDropItem;
            if (targetDropItem.Header.ToString() == "Exit")
            {
                // Do something
                Message.InfoMessage("Moved");
            }
        }

        private void btnUseWizard_Click(object sender, RoutedEventArgs e)
        {
            if (menuID != 0)
                LoadMenu();
        }
    }
}
