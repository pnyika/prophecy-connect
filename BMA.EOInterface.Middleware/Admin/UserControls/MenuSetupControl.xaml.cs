﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;

namespace BMA.MiddlewareApp.Admin.UserControls
{
    public partial class MenuSetupControl : UserControl
    {
        EditorContext context = new EditorContext();
        public static int menuID;
        public MenuSetupControl()
        {
            InitializeComponent();

            btnSave.IsEnabled = false;

        }


        private void loadMenu(int menuID)
        {
            context.Load<MenuStructure>(context.GetMenuStructuresQuery().Where(s => s.ID == menuID), LoadBehavior.RefreshCurrent, (MenusLoaded) =>
            {
                if (!MenusLoaded.HasError)
                {
                    MenuStructure menu = MenusLoaded.Entities.FirstOrDefault();
                    if (menu != null)
                    {
                        txtName.Text = menu.MenuName;
                        txtDescription.Text = menu.Description;
                    }
                    else
                    {
                        txtName.Text ="";
                            txtDescription.Text = "";
                    }
                  
                }

            }, null);

        }
   

 


       

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
           // this.DialogResult = false;
        }


        private void addMenu()
        {
           
            EditorContext context = new EditorContext();
            MenuStructure menu = new MenuStructure();
            menu.MenuName = txtName.Text;
            menu.Description = txtDescription.Text;

            context.MenuStructures.Add(menu);

            try
            {
                context.SubmitChanges(submit =>
                {
                    if (submit.HasError) { MessageBox.Show("An Error occured"); }
                    else
                    {
                        MenuItemsControl.menuID = menu.ID;
                        MenuPreviewControl.menuID = menu.ID;
                        MenuAssignUserControl.menuID = menu.ID; ;
                        MessageBox.Show("Successfully saved");
                       // this.DialogResult = true;
                    }
                }, null);
            }
            catch
            {
            }




        }

        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);

        }
        public void EnableOrDisableOKButton(object sender, RoutedEventArgs e)
        {
            if (txtName.Text.Length < 1)
                btnSave.IsEnabled = false;
            else
            {
                btnSave.IsEnabled = true;

            }
        }
 

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            addMenu();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
