﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using Telerik.Windows.Controls;
namespace BMA.MiddlewareApp.Admin.UserControls
   
{
    public partial class MenuAssignUserControl : UserControl
    {
        EditorContext context = new EditorContext();
        public static ObservableCollection<UserInformation> userInformationList;
        public static int menuID;
        public MenuAssignUserControl()
        {
            InitializeComponent();
            LoadUsers();
        }

        private void btnSelectUser_Click(object sender, RoutedEventArgs e)
        {
            SelectUsers users = new SelectUsers(menuID);
            users.Closed += user_Closed;
            users.Left = (Application.Current.Host.Content.ActualWidth - users.ActualWidth) / 2;
            users.Top = (Application.Current.Host.Content.ActualHeight - users.ActualHeight) / 2;
            users.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            users.ShowDialog();

        }
        private void LoadUsers()
        {
          
            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(m => m.MenuStructure_ID == menuID), CallbackUsers, null);
        }


        private void CallbackUsers(LoadOperation<UserInformation> loadUsers)
        {
            userInformationList = new ObservableCollection<UserInformation>();

            if (loadUsers != null)
            {

                foreach (UserInformation user in loadUsers.Entities)
                {
                    userInformationList.Add(user);
                }

            }
            lstUsers.ItemsSource = userInformationList;
        }

        private void btnChangeUserMenu_Click(object sender, RoutedEventArgs e)
        {
            UserInformation user = lstUsers.SelectedItem as UserInformation;
            if (user == null)
                return;

            ChangeUserMenu change = new ChangeUserMenu(user);
            change.Closed += user_Closed;
            change.Left = (Application.Current.Host.Content.ActualWidth - change.ActualWidth) / 2;
            change.Top = (Application.Current.Host.Content.ActualHeight - change.ActualHeight) / 2;
            change.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            change.ShowDialog();

        }

        void user_Closed(object sender, WindowClosedEventArgs e)
        {

           
            btnSelectUser.IsEnabled = true;
            LoadOperation ldop = context.Load<UserInformation>(context.GetUserInformationsQuery().Where(m => m.MenuStructure_ID == menuID), CallbackUsers, null);
        }

    }
}
