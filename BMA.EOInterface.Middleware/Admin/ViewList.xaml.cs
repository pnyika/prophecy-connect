﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using BMA.MiddlewareApp.Navigation;
using System.Collections.ObjectModel;
using SilverlightMessageBox;
using BMA.MiddlewareApp.Admin.UserControls;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class ViewList : Page
    {
        EditorContext context = new EditorContext();
       public ObservableCollection<View> viewList{get;set;}
        public ViewList()
        {
            InitializeComponent();
            LoadOperation groupOperation = context.Load<StoredProcedureName>(context.GetStoredProcedureNamesQuery(), CallbackStoredProcedureNames, null);
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void CallbackStoredProcedureNames(LoadOperation<StoredProcedureName> results)
        {
            if (results != null)
            {

                ((GridViewComboBoxColumn)this.radGridView1.Columns["ReadSP_ID"]).ItemsSource = results.Entities;
                ((GridViewComboBoxColumn)this.radGridView1.Columns["CreateSP_ID"]).ItemsSource = results.Entities;
                ((GridViewComboBoxColumn)this.radGridView1.Columns["DeleteSP_ID"]).ItemsSource = results.Entities;
                ((GridViewComboBoxColumn)this.radGridView1.Columns["UpdateSP_ID"]).ItemsSource = results.Entities;
            }
        }

        private void GetViews(object sender, RoutedEventArgs e)
        {
            radGridView1.IsBusy = true;
           LoadOperation<View> loadOp = context.Load(context.GetViewsQuery(), CallbackViews, null);               
           
          
        }
     

        private void CallbackViews(LoadOperation<View> loadOp)
        {


            if (loadOp != null)
            {

                viewList = new ObservableCollection<View>();
                foreach (View view in loadOp.Entities)
                {
                    viewList.Add(view);
                }
                radGridView1.ItemsSource = viewList;

            }
            radGridView1.IsBusy = false;
        }
        void MyRadDomainDataSource_SubmittedChanges(object sender, Telerik.Windows.Controls.DomainServices.DomainServiceSubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Avoids displaying errors, when deletion is prevented by DataBase constraints
                e.MarkErrorAsHandled();
            }
            else
            {
                string alertText = "Successfully saved";             

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                //Message.InfoMessage("Successfully saved!");
            }
        }
            

             private void radGridView1_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
             {
                 try
                 {
                     View view = radGridView1.SelectedItem as View;
                   
                     this.NavigationService.Navigate(new Uri("/SpecificView?viewID=" + view.ID.ToString(), UriKind.Relative));
                   
                 }
                 catch(Exception ex)
                 {

                 }

             }

             private void btnAddNew_Click(object sender, RoutedEventArgs e)
             {
                 AddView view = new AddView();
                 view.Closed +=view_Closed;// +=new RadEventHandler(view_Closed);
                 //view.WindowStartupLocation
                // view.Owner = 
                 view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
                 view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
                 view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                 view.ShowDialog();
             }

             void view_Closed(object sender, WindowClosedEventArgs e)
             {
                 usersDomainDataSource.Load();
             }
             //void view_Closed(object sender, EventArgs e)
             //{

             //    usersDomainDataSource.Load();
                 
             //}

             private void btnModify_Click(object sender, RoutedEventArgs e)
             {
                 try
                 {
                     View view = radGridView1.SelectedItem as View;
                     if (view == null)
                         return;
                     
                     this.NavigationService.Navigate(new Uri("/SpecificView?viewID=" + view.ID.ToString(), UriKind.Relative));

                 }
                 catch (Exception ex)
                 {

                 }
             }
             AddView view = new AddView();
             private void btnAddView_Click(object sender, RoutedEventArgs e)
             {
                  view = new AddView();
                 view.Closed += view_Closed;// +=new RadEventHandler(view_Closed);
                 //view.WindowStartupLocation
                 // view.Owner = 
                 view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
                 view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
                 view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                 view.ShowDialog();
                // this.NavigationService.Navigate(new Uri("/ViewWizard?viewID=1", UriKind.Relative));
             }

             private void btnWizard_Click(object sender, RoutedEventArgs e)
             {
                 View view = radGridView1.SelectedItem as View;
                 if (view != null)
                 {

                     VTableControl.viewID = view.ID;
                     VRelationshipControl.viewID = view.ID;
                     VFieldControl.viewID = view.ID;
                     ViewDetail.viewID = view.ID;
                 }
                 else
                 {

                     VTableControl.viewID = 0;
                     VRelationshipControl.viewID = 0;
                     VFieldControl.viewID = 0;
                     ViewDetail.viewID = 0;
                 }
                 this.NavigationService.Navigate(new Uri("/ViewWizard", UriKind.Relative));
             }

             private void btnRefresh_Click(object sender, RoutedEventArgs e)
             {
                 usersDomainDataSource.Load();
             }
//       
    }

    public class Page1ViewModel : INavigable
    {
        public ICommand GotoPage2Command { get; set; }

        public Page1ViewModel()
        {
            GotoPage2Command = new DelegateCommand<object>(ExecuteGotoPage2Command);
        }

        private void ExecuteGotoPage2Command(object notUsed)
        {

            NavigationService.Navigate("/Admin/SpecificView.xaml");
        }

        public INavigationService NavigationService { get; set; }
    }
}
