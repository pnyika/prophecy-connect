﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class EditMenuItem 
    {
        EditorContext context = new EditorContext();
        int menuId;
        int parentId;
        int sequenceMax;
        BMA.MiddlewareApp.Web.MenuItem menu = new BMA.MiddlewareApp.Web.MenuItem();
        public EditMenuItem(BMA.MiddlewareApp.Web.MenuItem menuItem, int menuID, int parentID, int sequence)
        {
            InitializeComponent();
            
            menuId = menuID;
            parentId = parentID;
            sequenceMax = sequence;
           
          
            chkIsParent.IsChecked = menuItem.IsParent;
            txtName.Text = menuItem.DisplayName;

            LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => m.ID == menuItem.ID), Callback, null);
            SetSequence();
        }

        private void Callback(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadMenus)
        {
            if (loadMenus != null)
            {


                menu = loadMenus.Entities.FirstOrDefault();
                comboBox1.SelectedValue = menu.SequenceNumber;
                var icon = iconList.Where(i => i.ImageUrl == menu.ImageUrl).FirstOrDefault();
                if (icon != null)
                {
                    ddlIcon.SelectedItem = icon;
                }


               
            }
        }
        List<ICon> iconList = new List<ICon>();
        private void SetSequence()
         {
             List<DropDownNumber> numberList = new List<DropDownNumber>();
             sequenceMax = sequenceMax + 1;
             for (int i = 1; i <= sequenceMax; i++)
             {
                 DropDownNumber num = new DropDownNumber();
                 num.Sequence = i;
                 numberList.Add(num);

             }
             comboBox1.ItemsSource = numberList;
              iconList = new List<ICon>();
              iconList.Add(new ICon { IconName = "None", ImageUrl = "" });
             iconList.Add(new ICon { IconName = "Table", ImageUrl = "../Images/application-table.png" });
             iconList.Add(new ICon { IconName = "Grid", ImageUrl = "../../Images/Table.png" });
             iconList.Add(new ICon { IconName = "Server", ImageUrl = "../../Images/server.png" });
             iconList.Add(new ICon { IconName = "Database", ImageUrl = "../Images/database.png" });
             iconList.Add(new ICon { IconName = "Graph", ImageUrl = "../../Images/chart.png" });
             iconList.Add(new ICon { IconName = "Users", ImageUrl = "../../Images/users.png" });
             ddlIcon.ItemsSource = iconList;
             ddlIcon.SelectedIndex = 0;
         }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            bool isParent = false;
            if(chkIsParent.IsChecked == true)
            {
                isParent = true;
             //   menu.ParentMenu_ID = parentId;

            }
           
            menu.MenuStructure_ID = menuId;
          
            menu.IsParent = isParent;
            menu.SequenceNumber = Convert.ToInt32(comboBox1.SelectedValue);
            menu.DisplayName = txtName.Text;
            ICon icon = ddlIcon.SelectedItem as ICon;

            menu.ImageUrl = icon.ImageUrl;
           //context.MenuItems.Add(menu);
            context.SubmitChanges(submit => { if (submit.HasError) {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = submit.Error.Message });
            } else {
                string alertText = "Successfully saved";

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                this.Close();
            
            } }, null);
                //0765168812 dense

            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();// = false;
        }
    }
   
}

