﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;

using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Admin
{
    public partial class AddView 
    {
        EditorContext context = new EditorContext();
        public AddView()
        {
            InitializeComponent();
            OKButton.IsEnabled = false;
            pnlProc.Visibility = System.Windows.Visibility.Collapsed;
            LoadOperation groupOperation = context.Load<StoredProcedureName>(context.GetStoredProcedureNamesQuery(), CallbackStoredProcedureNames, null);
        }

        private void CallbackStoredProcedureNames(LoadOperation<StoredProcedureName> results)
        {
            if (results != null)
            {
                ddlCreateSP.ItemsSource = results.Entities;
                ddlReadSP.ItemsSource = results.Entities;
                ddlUpdateSP.ItemsSource = results.Entities;
                ddlDeleteSP.ItemsSource = results.Entities;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            addView();
          
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void addView()
        {
            bool isReadOnly = false;
            bool isSingle = false;
            bool useProc = false;
            if (chkIsReadyOnly.IsChecked == true)
            {
                isReadOnly = true;
            }
            if (chkSingleTable.IsChecked == true)
            {
                isSingle = true;
            }
            if (chkUseProc.IsChecked == true)
            {
                useProc = true;
            }


            EditorContext context = new EditorContext();
                View view = new View();
                view.ViewName = txtViewName.Text;
                view.ReadOnly = isReadOnly;
                view.SingleTable = isSingle;
                view.UseStoredProcedure = useProc;
                view.DisplayName = txtDisplayName.Text;
              StoredProcedureName createSP=  ddlCreateSP.SelectedItem as StoredProcedureName;
              if (createSP != null)
              {
                  view.CreateSP_ID = createSP.ID;
              }

              StoredProcedureName updateSP = ddlUpdateSP.SelectedItem as StoredProcedureName;
              if (updateSP != null)
              {
                  view.UpdateSP_ID = updateSP.ID;
              }
              StoredProcedureName readSP = ddlReadSP.SelectedItem as StoredProcedureName;
              if (readSP != null)
              {
                  view.ReadSP_ID = readSP.ID;
              }

              StoredProcedureName deleteSP = ddlDeleteSP.SelectedItem as StoredProcedureName;
              if (deleteSP != null)
              {
                  view.DeleteSP_ID = deleteSP.ID;
              }
                context.Views.Add(view);
           
          try
           {
               context.SubmitChanges(submit =>
               {
                   if (submit.HasError) {
                       MessageBox.Show("An Error occured");
                       string alertText = "Error";

                       RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText });
                   }
                   else
                   {
                       ViewList viewList = new ViewList();
                       viewList.viewList = new System.Collections.ObjectModel.ObservableCollection<View>();
                       viewList.viewList.Add(view);
                    //   MessageBox.Show("Successfully saved");
                       string alertText = "Successfully saved";

                       RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                       this.Close();
                   }
               }, null);
            }
            catch
            {
            }




        }

        private void txtViewName_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);

        }
        public void EnableOrDisableOKButton(object sender, RoutedEventArgs e)
        {
            if (txtViewName.Text.Length < 1)
                OKButton.IsEnabled = false;
            else
            {
                txtDisplayName.Text = txtViewName.Text;
                OKButton.IsEnabled = true;
               
            }
        }

        private void chkUseProc_Checked(object sender, RoutedEventArgs e)
        {
            if (chkUseProc.IsChecked.Value)
            {
                pnlProc.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                pnlProc.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}

