﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BMA.EOInterface.Middleware.DataTableService;
using System.Collections;
using System.ServiceModel.DomainServices.Client;
using BMA.MiddlewareApp.Web;
using System.Linq;
using BMA.EOInterface.Middleware;

namespace BMA.MiddlewareApp.DataSources
{
    public class MiddlewareDataSource
    {
            IEnumerable _lookup;
            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> _tables;
            private  GetDataClient serviceClient;
            EditorContext context = new EditorContext();
       

          public MiddlewareDataSource()
            {
               // serviceClient = new GetDataClient();
                App app = (App)Application.Current;
              //  LoadOperation<Table> loadOp = context.Load(context.GetTablesByFormIDQuery(app.form1ID), CallbackTable, null);

                var serviceClient = WCF.GetService();
                this.DataObject = new ObservableCollection<DataObject>();

                serviceClient.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
                string sql = "select * from  Model ";
                serviceClient.GetDataSetDataAsync("", "",sql, 1, 25, "Model");


            }
          public ObservableCollection<DataObject> DataObject
            {
                get;
                set;
             }


          private void getTableData(object sender, RoutedEventArgs e)
          {
              
             
          }

          private void CallbackTable(LoadOperation<BMA.MiddlewareApp.Web.Table> loadOp)
          {
              BMA.MiddlewareApp.Web.Table table = loadOp.Entities.FirstOrDefault();

              if (table != null)
              {

                  


              }
          }


          private void GetData(string sql, int pagenumber, int pagesize, object userState)
          {
              var ws = WCF.GetService();
              ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
              ws.GetDataSetDataAsync("", "",sql, pagenumber, pagesize, userState);
          }


          void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
          {
              if (e.Error != null)
                  System.Windows.Browser.HtmlPage.Window.Alert(e.Error.Message);
              else if (e.ServiceError != null)
                  System.Windows.Browser.HtmlPage.Window.Alert(e.ServiceError.Message);
              else
              {
                  _tables = e.Result.Tables;
                  IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                  if (e.UserState as string == "Lookup")
                      _lookup = list;
                  //radGridView1.ItemsSource = list;
                  else
                  {


                      foreach (DataObject c in list)
                      {
                          this.DataObject.Add(c);
                      }



                  }
              }
             
          }

    }
}
