﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp
{
   
    public class DataColumnInfo
    {
        
        public string ColumnName { get; set; }

        public string ColumnTitle { get; set; }

       
        public string DataTypeName { get; set; }

       
        public bool IsRequired { get; set; }

       
        public bool IsKey { get; set; }

       
        public bool IsReadOnly { get; set; }

 
        public int DisplayIndex { get; set; }

     
        public string EditControlType { get; set; }

       
        public int MaxLength { get; set; }
    }

   
  
    public class DataTableInfo
    {
       
        public string TableName { get; set; }

       
        public ObservableCollection<DataColumnInfo> Columns { get; set; }

    }
}
