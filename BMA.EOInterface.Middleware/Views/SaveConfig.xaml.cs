﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using SilverlightMessageBox;
using Telerik.Windows.Controls;
using System.ServiceModel.DomainServices.Client;

namespace BMA.MiddlewareApp.Views
{
    public partial class SaveConfig 
    {
        string xml;
        int gridNo;
        long DisplyDefID;
        EditorContext context = new EditorContext();
        public SaveConfig(string settingsXML, int gridNumber, long definitionDisplayID)
        {
            InitializeComponent();
            xml = settingsXML;
            gridNo = gridNumber;
            DisplyDefID = definitionDisplayID;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            OKButton.IsEnabled = false;
            CancelButton.IsEnabled = false;
            CheckName();
           // this.DialogResult = true;
        }

        private void CheckName()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == MainPage.userID && x.DisplayDefinition_ID == DisplyDefID && x.GridNumber == gridNo && x.ConfigurationName == txtConfigName.Text), CallbackSetting, null);
        }
        private void CallbackSetting(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp == null)
            {
                SaveSetting();
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content ="The configuration name already exist in the current grid"});
            }
        }
        private void SaveSetting()
        {
            bool isDefault = false;
          //  int DisplyDefID = MainPage.DisplayID;
            int UserID = MainPage.userID;
            EditorContext context = new EditorContext();

            Setting setting = new Setting();
            setting.Settings = xml;

            context.Settings.Add(setting);
            context.SubmitChanges(so =>
            {
                if (so.HasError)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                }
                else
                {
                    int dID = Convert.ToInt32(DisplyDefID);
                    context = new EditorContext();
                    UserGridConfig config = new UserGridConfig();
                    config.DisplayDefinition_ID = dID;
                    config.ConfigurationName = txtConfigName.Text;
                    config.GridNumber = gridNo;
                    if (checkBox1.IsChecked == true)
                    {
                        config.DefaultConfiguration = 0;
                        isDefault = true;
                    }
                    else
                    {
                        config.DefaultConfiguration = 0;
                    }

                    config.Settings_ID = setting.ID;
                    config.UserInformation_ID = UserID;

                    context.UserGridConfigs.Add(config);
                    context.SubmitChanges(sa =>
            {
                if (sa.HasError)
                {
                   
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                }
                else
                {
                   
                    if (isDefault)
                    {
                        var ws = WCF.GetService();
                        int defID = Convert.ToInt32(DisplyDefID.ToString());
                        ws.updateUserConfigDefaultAsync(UserID, defID, config.ID, gridNo);
                    }
                    string alertText = "Successfully saved";

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                    this.Close();

                }
            }, null);
                }
            }, null);
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

