﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace BMA.MiddlewareApp.Controls
{
    public partial class DateTimePicker : UserControl
    {
        public static readonly DependencyProperty SelectedDateProperty =
       DependencyProperty.Register("SelectedDate", typeof(DateTime?), typeof(DateTimePicker), new PropertyMetadata(null, OnSelectedDateChanged));
        public DateTimePicker()
        {
            this.InitializeComponent();
        }
        public DateTime? SelectedDate
        {
            get
            {
                return (DateTime?)this.GetValue(SelectedDateProperty);
            }
            set
            {
                this.SetValue(SelectedDateProperty, value);
            }
        }
        private static void OnSelectedDateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var picker = (DateTimePicker)d;
            picker.OnSelectedDateChanged((DateTime)e.NewValue);
        }
        private void HandlePickersSelectionChanged()
        {
            this.SelectedDate = this.Calendar.SelectedDate + this.TimePicker.SelectedTime;
        }
        private void OnCalendarSelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            this.HandlePickersSelectionChanged();
        }
        private void OnSelectedDateChanged(DateTime selectedDate)
        {
            this.TimePicker.SelectedTime = selectedDate.TimeOfDay;
            this.Calendar.SelectedDate = selectedDate;
        }
        private void OnTimeChanged(object sender, EventArgs e)
        {
            this.HandlePickersSelectionChanged();
        }

        private void TimePicker_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            this.HandlePickersSelectionChanged();
        }
    }

    public class DateTimePickerColumn : GridViewBoundColumnBase
    {
        public override FrameworkElement CreateCellEditElement(GridViewCell cell, object dataItem)
        {
            this.BindingTarget = DateTimePicker.SelectedDateProperty;
            var picker = new DateTimePicker();
            picker.SetBinding(this.BindingTarget, this.CreateValueBinding());
            return picker;
        }
        private Binding CreateValueBinding()
        {
            var valueBinding = new Binding();
            valueBinding.Mode = BindingMode.TwoWay;
            valueBinding.NotifyOnValidationError = true;
            valueBinding.ValidatesOnExceptions = true;
            valueBinding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;
            valueBinding.Path = new PropertyPath(this.DataMemberBinding.Path.Path);
            return valueBinding;
        }
    }
}
