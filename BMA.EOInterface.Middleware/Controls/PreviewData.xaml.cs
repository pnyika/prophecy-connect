﻿using BMA.MiddlewareApp.Admin;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace BMA.MiddlewareApp.Controls
{
    public partial class PreviewData 
    {

        List<ControlType> fields = new List<ControlType>();
        bool isRowFirst;
        ObservableCollection<ControlType> SourceFieldList2 = new ObservableCollection<ControlType>();
        string filePath; string ext;
        string sheetName; 
        bool isFirstRowAsColumnNames;
        string dataSource = "";
        
        public PreviewData(string FilePath, string Ext, string SheetName, bool isFirstRowAsColumnName, List<ControlType> sourceList, List<ControlType> fieldList, string source)
        {
            InitializeComponent();
            dataSource = source;
            SourceFieldList2 = new ObservableCollection<ControlType>();

            filePath = FilePath;
            ext = Ext;
            sheetName = SheetName;
            isFirstRowAsColumnNames = isFirstRowAsColumnName;
            foreach (var item in sourceList)
            {
                SourceFieldList2.Add(item);
            }
            fields = new List<ControlType>();
            foreach (var item in fieldList)
            {
                fields.Add(item);
            }

            if(dataSource=="Microsoft Excel")
            {
            GetExcelData();
            }
            else if (dataSource == "Microsoft Access")
            {
                GetAccessData();
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();// = false;
        }


        private void GetExcelData()
        {
            gdData.IsBusy = true;
            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.FieldMapped> fields = new ObservableCollection<EOInterface.Middleware.DataTableService.FieldMapped>();
            foreach (var item in SourceFieldList2)
            {
                string AcualName = "";
                var col = ImportWizard.sheetColunList.Where(c => c.Name == item.Value).FirstOrDefault();
                if (col != null)
                {
                    AcualName = col.AcualName;
                }

                fields.Add(new BMA.EOInterface.Middleware.DataTableService.FieldMapped { FieldID = item.index, DataType = item.DataType, DestinationField = item.Value, SourceField = item.Display, ActualColumnName = AcualName });
            }
            var ws = WCF.GetService();
            ws.GetExcelDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetExcelDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted);
            ws.GetExcelDataAsync(filePath, ext, sheetName, isFirstRowAsColumnNames, fields, 0, 0, 0, false);
        }

        //
        void ws_GetColumnDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetExcelDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                //  _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                

              
               
                gdData.Columns.Clear();
                gdData.AutoGenerateColumns = true;
                List<DataObject> dataList = new List<DataObject>();
                foreach (var item in list)
                {
                    DataObject data = item as DataObject;

                    dataList.Add(data);
                }

                if (dataList.Count > 0)
                {
                    var obj = dataList.FirstOrDefault();

                    dataList.Remove(obj);

                    Type t = obj.GetType();
                    PropertyInfo[] props = t.GetProperties();
                    int count = 0;
                    foreach (PropertyInfo prp in props)
                    {
                        count += 1;
                        try
                        {

                            var name = prp.Name;
                            if (name == "State")
                            {




                            }
                            else
                            {
                                ControlType readfield = new ControlType();
                                string value = obj.GetFieldValue(name).ToString();
                                if (isFirstRowAsColumnNames)
                                {
                                    readfield = fields.Where(f => f.Value == value).FirstOrDefault();
                                }
                                else
                                {
                                    readfield = fields.Where(f => f.Value == name).FirstOrDefault();
                                }

                                GridViewDataColumn column = new GridViewDataColumn();
                                //  DateTimePickerColumn.
                                column.DataMemberBinding = new Binding(name);
                                column.Header = readfield.Display;
                                column.UniqueName = name;
                                column.IsFilteringDeferred = true;
                                switch (readfield.DataType)
                                {
                                    case "datetime":
                                        column.DataType = typeof(DateTime);
                                        column.DataFormatString = "{0:dd-MMM-yyyy}";
                                        break;

                                    case "int":
                                        column.DataType = typeof(int);
                                        break;
                                    case "bigint":
                                        column.DataType = typeof(long);
                                        break;
                                    default:
                                        column.DataType = typeof(string);
                                        break;
                                }

                                //  column.IsReadOnly = true;
                                if (prp.PropertyType.ToString() == "System.DateTime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                }

                                //if (field.DataType == "date")
                                //{
                                //    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                //}
                                if (count < 4)
                                {
                                    // column.IsGroupable = false;
                                }

                                /// if (!field.Hidden.Value)
                                this.gdData.Columns.Add(column);
                            }
                        }

                        catch
                        {
                        }
                    }





                    gdData.ItemsSource = dataList;
                    gdData.AutoGenerateColumns = false;
                   
                }

            }

            gdData.IsBusy = false;
        }



        private void GetAccessData()
        {
            gdData.IsBusy = true;
            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.FieldMapped> fields = new ObservableCollection<EOInterface.Middleware.DataTableService.FieldMapped>();
            foreach (var item in SourceFieldList2)
            {
                string AcualName = "";
                var col = ImportWizard.sheetColunList.Where(c => c.Name == item.Value).FirstOrDefault();
                if (col != null)
                {
                    AcualName = col.AcualName;
                }

                fields.Add(new BMA.EOInterface.Middleware.DataTableService.FieldMapped { FieldID = item.index, DataType = item.DataType, DestinationField = item.Value, SourceField = item.Display, ActualColumnName = AcualName });
            }
            var ws = WCF.GetService();
            ws.GetAccessDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetAccessDataCompletedEventArgs>(ws_GetAccessDataCompleted);
            ws.GetAccessDataAsync(filePath, ext, sheetName, ImportWizard.authentication, fields, 0, 0, 0, false);
        }

        //
        void ws_GetAccessDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetAccessDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                //  _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);




                gdData.Columns.Clear();
                gdData.AutoGenerateColumns = true;
                List<DataObject> dataList = new List<DataObject>();
                foreach (var item in list)
                {
                    DataObject data = item as DataObject;

                    dataList.Add(data);
                }

                if (dataList.Count > 0)
                {
                    var obj = dataList.FirstOrDefault();

                    dataList.Remove(obj);

                    Type t = obj.GetType();
                    PropertyInfo[] props = t.GetProperties();
                    int count = 0;
                    foreach (PropertyInfo prp in props)
                    {
                        count += 1;
                        try
                        {

                            var name = prp.Name;
                            if (name == "State")
                            {




                            }
                            else
                            {
                                ControlType readfield = new ControlType();
                                string value = obj.GetFieldValue(name).ToString();
                                string strName = CharacterHandler.ReplaceHexCharacter(name);
                                readfield = fields.Where(f => f.Value == strName).FirstOrDefault();
                                

                                GridViewDataColumn column = new GridViewDataColumn();
                                //  DateTimePickerColumn.
                                column.DataMemberBinding = new Binding(name);
                                column.Header = readfield.Display;
                                column.UniqueName = name;
                                column.IsFilteringDeferred = true;
                                switch (readfield.DataType)
                                {
                                    case "datetime":
                                        column.DataType = typeof(DateTime);
                                        column.DataFormatString = "{0:dd-MMM-yyyy}";
                                        break;

                                    case "int":
                                        column.DataType = typeof(int);
                                        break;
                                    case "bigint":
                                        column.DataType = typeof(long);
                                        break;
                                    default:
                                        column.DataType = typeof(string);
                                        break;
                                }

                                //  column.IsReadOnly = true;
                                if (prp.PropertyType.ToString() == "System.DateTime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                }

                                //if (field.DataType == "date")
                                //{
                                //    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                //}
                                if (count < 4)
                                {
                                    // column.IsGroupable = false;
                                }

                                /// if (!field.Hidden.Value)
                                this.gdData.Columns.Add(column);
                            }
                        }

                        catch
                        {
                        }
                    }





                    gdData.ItemsSource = dataList;
                    gdData.AutoGenerateColumns = false;

                }

            }

            gdData.IsBusy = false;
        }
    }
}

