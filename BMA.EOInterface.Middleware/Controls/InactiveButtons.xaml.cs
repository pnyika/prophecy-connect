﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.Controls
{
    public partial class InactiveButtons : UserControl
    {
       // public event EventHandler SaveClick;
       // public event EventHandler CancelClick;
       // public event EventHandler DeleteClick;
        public event EventHandler ImportClick;
        public event EventHandler FilterClick;
        public event EventHandler ExportClick;
        public event EventHandler SetDefaultClick;

        public InactiveButtons()
        {

            InitializeComponent();
        }



        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //SaveClick(sender, e);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
           // CancelClick(sender, e);

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
          //  DeleteClick(sender, e);

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
           // AddClick(sender, e);
        }

        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {
            FilterClick(sender, e);
        }

        private void btnSetDefault_Click(object sender, RoutedEventArgs e)
        {
            SetDefaultClick(sender, e);
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            ExportClick(sender, e);
        }
        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            ImportClick(sender, e);
        }
  
 
    }
}
