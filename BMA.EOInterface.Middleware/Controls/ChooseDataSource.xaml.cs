﻿using BMA.EOInterface.Middleware;
using BMA.MiddlewareApp.Admin;
using ExcelDataReader.Silverlight;
using ExcelDataReader.Silverlight.Data;
using Lite.ExcelLibrary.SpreadSheet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;


namespace BMA.MiddlewareApp.Controls
{
    public partial class ChooseDataSource : UserControl
    {
        public ChooseDataSource()
        {
            InitializeComponent();
            LoadDropdowns();
        }

        private void LoadDropdowns()
        {
            List<ControlType> data = new List<ControlType>();
            data.Add(new ControlType { Value = "Microsoft Excel", Display = "Microsoft Excel" });
          //  data.Add(new ControlType { Value = "Microsoft Access", Display = "Microsoft Access" });
         //   data.Add(new ControlType { Value = "Flat File", Display = "Flat File" });
           
            ddlDataSouce.ItemsSource = data;
            ddlDataSouce.DisplayMemberPath = "Display";
            ddlDataSouce.SelectedValuePath = "Value";

            List<ControlType> versionList = new List<ControlType>();
            versionList.Add(new ControlType { Value = "xls", Display = "Microsoft Excel 97-2003" });
            versionList.Add(new ControlType { Value = "xlsx", Display = "Microsoft Excel 2007" });

            ddlExcelVersion.ItemsSource = versionList;
            ddlExcelVersion.DisplayMemberPath = "Display";
            ddlExcelVersion.SelectedValuePath = "Value";
           
        }

        private void ddlDataSouce_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            ControlType source = ddlDataSouce.SelectedItem as ControlType;

            if (source == null)
                return;

            if (source.Value == "Microsoft Excel")
            {
                pnlExcel.Visibility = System.Windows.Visibility.Visible;
                pnlAccess.Visibility = System.Windows.Visibility.Collapsed;
            }
            
            if (source.Value ==  "Microsoft Access")
            {
                pnlExcel.Visibility = System.Windows.Visibility.Collapsed;             
                pnlAccess.Visibility= System.Windows.Visibility.Visible;
            }

        }




        string previousFile = "";

        private void RemoveOldFile()
        {
            try
            {
                var ws = WCF.GetService();

                ws.RemoveTempFileAsync(previousFile);
            }
            catch
            {
            }
        }

        FileStream stream;// = new FileStream(null);
        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (previousFile != "")
                {
                    RemoveOldFile();

                }
                string filter = "";
                string sheetname = "";

                filter = "Excel Files (*.xls;*.xlsx;*.xlsm)|*.xls;*.xlsx;*.xlsm|All Files (*.*)|*.*";
                   

                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = filter;
            
               //openFileDialog.RestoreDirectory = true;
              // // if (openFileDialog.ShowDialog() == true)
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        ImportWizard.currFieldName = "";
                        Notify(sender, e);
                    string fileName =    openFileDialog.File.Name;
                    previousFile = fileName;
                    string ext = openFileDialog.File.Extension;
                    string strExt = ext.Replace(".", "");
                    ddlExcelVersion.SelectedValue = strExt;
                   // FileStream fs = openFileDialog.File.OpenRead();
                  //  stream = fs;
                    txtFilePath.Text = fileName;
                    //    Stream stream = this.RadUpload1.SelectedItems.aOpenRead();
                    // Get the stream of the selected file
                    var length = openFileDialog.File.Length;
                    var fileContents = new byte[length];
                    using (var fileStream = openFileDialog.File.OpenRead())
                    {
                        if (length > Int32.MaxValue)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Info" }, Content = "Are you sure you want to load > 2GB into memory.  There may be better options" });
                          //  throw new Exception("Are you sure you want to load > 2GB into memory.  There may be better options");
                        }
                        fileStream.Read(fileContents, 0, (int)length);

                        if(ext ==".xls")
                        {
                        GetSheetNames(fileContents);
                        }
                        else if(ext ==".xlsx")
                        {
                            GetXslxSheetNames(fileContents);
                        }
                        //GetExcelAllSheetNames(fileStream);
                        fileStream.Close();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        ((IDisposable)fileStream).Dispose();
                        openFileDialog = null;
                    }
                    openFileDialog =  new OpenFileDialog();
          
                   
                    }
                   openFileDialog = new OpenFileDialog();
                  //  RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Info" }, Content = sheetname });
            }
            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message.ToString() });
            }
        }

        private void btnAccessUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string filter = "";
                string sheetname = "";

                filter = "Excel Files (*.mdb;*.accdb;)|*.mdb;*.accdb;|All Files (*.*)|*.*";


                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = filter;
                // openFileDialog.RestoreDirectory = true;
                // if (openFileDialog.ShowDialog() == true)
                if (openFileDialog.ShowDialog() ?? false)
                {

                    string fileName = openFileDialog.File.Name;
                    string ext = openFileDialog.File.Extension;
                    string strExt = ext.Replace(".", "");
                    txtAccessPath.Text = fileName;
                 //   GetAccessTableName(fileName, fileName);
                }
            }
            catch
            {
            }
        }

        private void GetExcelAllSheetNames(Stream fileStream)
        {

            string ext = "";
            //.xls)
            List<string> listSheetNames = new List<string>();
            //   listSheetNames.Add("~/TempUploads/" + randomName);
            IExcelDataReader excelReader = null;

            try
            {
                excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
            }
            catch
            {

                // *.xlsx)

                excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
            }
            //...
            //3. DataSet - The result of each spreadsheet will be created in the result.Tables
            ;


            excelReader.WorkBookFactory = new ExcelWorkBookFactory();
            IWorkBook excelWorkBook = excelReader.AsWorkBook();
            List<IWorkSheet> workSheets = (List<IWorkSheet>)excelWorkBook.WorkSheets;

            foreach (var item in workSheets)
            {
                listSheetNames.Add(item.Name);
            }

            //6. Free resources (IExcelDataReader is IDisposable)
            excelReader.Close();

            //if (File.Exists(tempFile))
            //  File.Delete(tempFile);
            //return listSheetNames;

            ImportWizard.sheetList = new ObservableCollection<ControlType>();
            int i = 1;
            foreach (var item in listSheetNames)
            {
                if (item.Contains("~/TempUploads/"))
                {
                    ImportWizard.currFieldName = item;
                }
                else
                {
                    ImportWizard.sheetList.Add(new ControlType { Value = i.ToString(), Display = item, index = i });
                    i += 1;
                }
            }

        }

       

        private void GetSheetNames(byte[] stream)
        {

            var ws = WCF.GetService();
         ws.GetExcelAllSheetNamesCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetExcelAllSheetNamesCompletedEventArgs>(ws_GetColumnDataSetDataCompleted);
            ws.GetExcelAllSheetNamesAsync(stream);
        }

        //
        void ws_GetColumnDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetExcelAllSheetNamesCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
          
            else
            {
                int i = 1;
                ImportWizard.sheetList = new ObservableCollection<ControlType>();
                foreach (var item in e.Result)
                {
                    if(item.Contains("~/TempUploads/"))
                    {
                        ImportWizard.currFieldName = item;
                    }
                    else
                    {
                    ImportWizard.sheetList.Add(new ControlType { Value = i.ToString(), Display = item, index = i });
                    i += 1;
                    }
                }
                NotifyReady(null,null);
               

            }

        }



        private void GetXslxSheetNames(byte[] stream)
        {

            var ws = WCF.GetService();
            ws.GetExcelXslxSheetNamesCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetExcelXslxSheetNamesCompletedEventArgs>(ws_GetXlsxSetDataCompleted);
            ws.GetExcelXslxSheetNamesAsync(stream);
        }

        //
        void ws_GetXlsxSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetExcelXslxSheetNamesCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                int i = 1;
                ImportWizard.sheetList = new ObservableCollection<ControlType>();
                foreach (var item in e.Result)
                {
                    if (item.Contains("~/TempUploads/"))
                    {
                        ImportWizard.currFieldName = item;
                    }
                    else
                    {
                        ImportWizard.sheetList.Add(new ControlType { Value = i.ToString(), Display = item, index = i });
                        i += 1;
                    }
                }

                NotifyReady(null, null);

            }

        }


        private void GetAccessTableName(string path, string dbName)
        {

            var ws = WCF.GetService();
            ws.GetAccessTableNamesCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetAccessTableNamesCompletedEventArgs>(ws_GetAccessTablesCompleted);
            ws.GetAccessTableNamesAsync(path, dbName, ImportWizard.authentication);
        }

        //
        void ws_GetAccessTablesCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetAccessTableNamesCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                int i = 1;
                ImportWizard.sheetList = new ObservableCollection<ControlType>();
                foreach (var item in e.Result)
                {
                    
                   
                        ImportWizard.sheetList.Add(new ControlType { Value = i.ToString(), Display = item, index = i });
                        i += 1;
                    
                }



            }

        }


        private bool isRowHeader = false;
        private void chkFirstRow_Click(object sender, RoutedEventArgs e)
        {
            isRowHeader = chkFirstRow.IsChecked.Value;
          //  RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = chkFirstRow.IsChecked.Value.ToString() });
        }

        private void chkAuthentication_Click(object sender, RoutedEventArgs e)
        {
            bool isUseAuth = chkAuthentication.IsChecked.Value;
            txtUserName.IsEnabled = isUseAuth;
            txtPassword.IsEnabled = isUseAuth;
           // RadUpload1.se.

        }


        public event EventHandler Notify;
        public event EventHandler NotifyReady;
    }

    public class SheetColumn
    {
        public int Index { get; set; }
        public int SheetIndex { get; set; }
       // public int Index { get; set; }
        public string Name { get; set; }
        public string AcualName { get; set; }
    }

    public static class Convertor
    {
         public static  Byte[] ToByteArray(this Stream stream)
        {
            Int32 length = stream.Length > Int32.MaxValue ? Int32.MaxValue : Convert.ToInt32(stream.Length);
            Byte[] buffer = new Byte[length];
            stream.Read(buffer, 0, length);
            return buffer;
        }
    }
}
