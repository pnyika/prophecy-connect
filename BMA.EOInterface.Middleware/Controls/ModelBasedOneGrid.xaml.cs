﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Markup;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.ComponentModel;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Persistence;
using Telerik.Windows.Persistence.Services;
using System.IO;
using System.Text;
using Telerik.Windows.Persistence.Storage;
using System.Windows.Navigation;
using Telerik.Windows;
using SilverlightMessageBox;
using BMA.MiddlewareApp.Web.Services;
using BMA.MiddlewareApp.AppCode;
using System.Reflection;
using BMA.EOInterface.Middleware;
//using SL4PopupMenu;
//using Telerik.Windows.Input;

namespace BMA.MiddlewareApp.Controls
{
    public partial class ModelBasedOneGrid : UserControl
    {
        #region local variable

   
        private EditorContext context = new EditorContext();
        //private List<GridViewRowInfo> modifiedRows = new List<GridViewRowInfo>();
        IEnumerable _lookup;
        ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> _tables;
        private string tableName;
        private int tableID;
        private string databaseName;
        private string connString;
        ObservableCollection<DataObject> newDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList = new ObservableCollection<DataObject>();
        List<DataObject> propertiesList = new List<DataObject>();
        List<string> columnNames = new List<string>();
       
        public ObservableCollection<UserGridConfig> settingObjectList = new ObservableCollection<UserGridConfig>();
        RadGridViewSettings settings = null;
        RadGridViewSettings primarySettings = null;

        private Stream stream;
        private PersistenceManager manager = new PersistenceManager();
        private IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
        private bool useIsolatedStorage;
        private int userID;
        private int displayDefID;
        private string XMLString;
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        List<ViewFieldSummary> fieldList = new List<ViewFieldSummary>();
        List<Table> tableList = new List<Table>();
        List<GroupViewFilter> GroupViewFilterList = new List<GroupViewFilter>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary;
        private int viewID;
        private bool singleTable = true;
        private bool isReadOnly = true;
        public static int DisplayID;
        private MouseButtonEventHandler datagridRightClick;
        private int fieldIndex = 0;
        private int indexMax = 0;
        private string dropDownName;

        private int pageNumber = 1;

        private int pageTotal = 1;
        public static int pageSize;
        string sqlText = "";
        string newSqlText = "";
        private bool canGenerateColumns = true;
        public static long ModelID;
        public static string ModelName;

        private bool useReadSP = false;
        private bool useCreateSP = false;
        private int CreateSPID = 0;
        private int ReadSPID = 0;
        private bool useDeleteSP = false;
        private int DeleteSPID = 0;
        private bool useUpdateSP = false;
        private int UpdateSPID = 0;
        ObservableCollection<ReadField> fieldListSP = new ObservableCollection<ReadField>();
         private List<TempField> tempFieldList = new List<TempField>();

        private List<TempExtendedProperty> TempExtendedPropertyList = new List<TempExtendedProperty>();
        #endregion

        public ModelBasedOneGrid()
        {
            InitializeComponent();
            TemplateManager.LoadTemplate("DataGrid/Content.zip");

            //  ServiceProvider.RegisterPersistenceProvider<ICustomPropertyProvider>(typeof(RadGridView), new GridViewCustomPropertyProvider());

            this.radGridView1.Deleted += new EventHandler<GridViewDeletedEventArgs>(gridView_Deleted);

            // The events below notify when saving and loading have been completed


            userID = MainPage.userID;
            displayDefID = DisplayID;
            mainHight = MainPage.gridHeight;
            settings = new RadGridViewSettings(this.radGridView1);
            //primarySettings = new RadGridViewSettings(this.radGridView1);
            //primarySettings.SaveState();
            loadPageNumber();
            #region

            btnButtons.SaveClick += new EventHandler(btnSave_Click);
            btnButtons.AddClick += new EventHandler(btnAddNew_Click);
            btnButtons.DeleteClick += new EventHandler(btnDelete_Click);
            btnButtons.CancelClick += new EventHandler(btnCancel_Click);
            btnButtons.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            btnButtons.SaveFilterClick += new EventHandler(btnSaveConfig_Click);
            btnButtons.ExportClick += new EventHandler(btnButtons_ExportClick);


            btnInactive.ExportClick += new EventHandler(btnButtons_ExportClick);
            btnInactive.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            btnInactive.FilterClick += new EventHandler(btnSaveConfig_Click);
            btnInactive.Visibility = Visibility.Collapsed;


            dataPager.Back_Click += new EventHandler(dataPager_Back_Click);
            dataPager.Last_Click += new EventHandler(dataPager_Last_Click);
            dataPager.Next_Click += new EventHandler(dataPager_Next_Click);
            dataPager.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged);
            dataPager.First_Click += new EventHandler(dataPager_First_Click);
            Paging();
           
            LoadConfigSetting();
            getTableData();

           /// GenerateDynamicDemo(); ;
            ////RightClickMenu.Save += new EventHandler(RightClickMenu_Save);
            ////RightClickMenu.Cancel += new EventHandler(RightClickMenu_Cancel);
            ////RightClickMenu.Delete += new EventHandler(RightClickMenu_Delete);
            ////RightClickMenu.SaveFilter += new EventHandler(RightClickMenu_SaveFilter);
            ////RightClickMenu.SetDefault += new EventHandler(RightClickMenu_SetDefault);
            ////RightClickMenu.AddNew += new EventHandler(RightClickMenu_AddNew);
            ////RightClickMenu.Export += new EventHandler(RightClickMenu_Export);

            //stack1.MouseRightButtonDown += new MouseButtonEventHandler(stack1_MouseRightButtonDown);
            //LayoutRoot.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
            //radGridView1.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
            // LayoutRoot.LayoutUpdated += new EventHandler(LayoutRoot_LayoutUpdated);

            MainPage.StatusUpdated += new EventHandler(MainPage_StatusUpdated);
            this.radGridView1.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded);
            MainPage.cnUpdate = true;
            //this.radGridView1.GridViewColumn.IsFilteringDeferred = true;
         //   this.radGridView1.MouseRightButtonDown += radGridView1_MouseRightButtonDown;
         //   this.radGridView1.MouseRightButtonUp +=radGridView1_MouseRightButtonUp;
            #endregion
        }

        void radGridView1_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        double mainHight = 400;
        void MainPage_StatusUpdated(object sender, EventArgs e)
        {
            try
            {
                Grid grid = sender as Grid;
                if (grid != null)
                {
                    this.radGridView1.Height = grid.ActualHeight - 60;
                    mainHight = grid.ActualHeight;
                }
            }
            catch
            {
            }
           
        }


        private void updateLayOut()
        {
            try
            {
                this.radGridView1.Height = mainHight - 60;
            }
            catch
            {
            }
        }

       

       

        #region Paging


        private void loadPageNumber()
        {
            List<PageNumber> pageList = new List<PageNumber>();

            pageList.Add(new PageNumber {Size =10, Display= "10" });
            pageList.Add(new PageNumber { Size = 25, Display = "25" });
            pageList.Add(new PageNumber { Size = 50, Display = "50" });
            pageList.Add(new PageNumber { Size = 75, Display = "75" });
            pageList.Add(new PageNumber { Size = 100, Display = "100" });
            pageList.Add(new PageNumber { Size = 150, Display = "150" });
            pageList.Add(new PageNumber { Size = 250, Display = "250" });
            pageList.Add(new PageNumber { Size = 500, Display = "500" });
            pageList.Add(new PageNumber { Size = 750, Display = "750" });
            pageList.Add(new PageNumber { Size = 1000, Display = "1000" });
            pageList.Add(new PageNumber { Size = 100000000, Display = "All" });
            ddlPageSize.DisplayMemberPath = "Display";
            ddlPageSize.SelectedValuePath = "Size";
            ddlPageSize.ItemsSource = pageList;
            ddlPageSize.SelectedValue = pageSize;

         }

        bool isFirst = true;
        private void ddlPageSize_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst)
            {
                PageNumber page = ddlPageSize.SelectedItem as PageNumber;
                pageSize = page.Size;
                isTextBox = false;
                int newPageNumber = 1;
                if (useReadSP)
                {

                    ReCountPage();
                }

                else
                {
                    GetTotalRows(newSqlText);
                }
                ReloadPage(newPageNumber);
            }
            isFirst = false;
        }

        void dataPager_First_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = 1;
            ReloadPage(newPageNumber);
        }

        private void Paging()
        {
            dataPager.txtNumber.Text = pageNumber.ToString();
            dataPager.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox = true;
        void dataPager_Number_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox)
                {
                    int newPageNumber = Convert.ToInt32(dataPager.txtNumber.Text);
                    ReloadPage(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox = true;
        }


        private void ReloadPage(int newPageNumber)
        {
            if ((newDataObjectList.Count > 0) || (editedDataObjectList.Count > 0) || (deletedDataObjectList.Count > 0))
            {
                curPage = newPageNumber; ;

                string confirmText = "This page contains some changes, Do you want to save the changes?";
                RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed));





            }
            else
            {

                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    if (useReadSP)
                    {

                        PullDataSP();
                    }
                    else
                        GetData(newSqlText, pageNumber, pageSize, tableName);
                    //radGridView1.IsBusy = false;
                }
            }




        }
        int curPage = 1;
        private void OnConfirmClosed(object sender, WindowClosedEventArgs e)
        {
            int newPageNumber = curPage;
            if (e.DialogResult == true)
            {
                SaveChanges();

                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    if (useReadSP)
                    {

                        PullDataSP();
                    }
                    else
                    {

                        GetData(newSqlText, pageNumber, pageSize, tableName);
                    }
                    //radGridView1.IsBusy = false;
                }
            }
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
                editedDataObjectList = new ObservableCollection<DataObject>();
                deletedDataObjectList = new ObservableCollection<DataObject>();
                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    if (useReadSP)
                    {

                        PullDataSP();
                    }
                    else
                        GetData(newSqlText, pageNumber, pageSize, tableName);
                    //radGridView1.IsBusy = false;
                }
            }
        }


        void dataPager_Next_Click(object sender, EventArgs e)
        {
            if (pageNumber < pageSize)
            {
                isTextBox = false;
                int newPageNumber = pageNumber + 1;
                ReloadPage(newPageNumber);
            }

        }

    

        void dataPager_Last_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageTotal;
            ReloadPage(newPageNumber);
        }

        void dataPager_Back_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageNumber - 1;
            ReloadPage(newPageNumber);
           
        }

        #endregion



        #region Events

        void LayoutRoot_LayoutUpdated(object sender, EventArgs e)
        {
            radGridView1.Height = this.stack1.RenderSize.Height;
        }




        void RightClickMenu_Export(object sender, EventArgs e)
        {
            ExcelExport();
        }

        void RightClickMenu_AddNew(object sender, EventArgs e)
        {
            if (singleTable)
            {
                this.radGridView1.Items.AddNew();
            }
        }

        void RightClickMenu_SetDefault(object sender, EventArgs e)
        {
            SetDefault();
        }

        void RightClickMenu_SaveFilter(object sender, EventArgs e)
        {
            SaveConfig();
        }

        void RightClickMenu_Delete(object sender, EventArgs e)
        {
            if (singleTable)
            {
                DeleteRows();
            }
        }

        void RightClickMenu_Cancel(object sender, EventArgs e)
        {
            Cancel();
        }



        void RightClickMenu_Save(object sender, EventArgs e)
        {

            SaveChanges();
        }

        public static void color_TestClick()
        {
            MessageBox.Show("Red");
        }

        void stack1_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        void stack1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            RightClickContentMenu contextMenu = new RightClickContentMenu();
            contextMenu.Show(e.GetPosition(LayoutRoot));
        }


        void dataGridRightClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("right");
        }

        protected void btnButtons_ExportClick(object sender, EventArgs e)
        {

            ExcelExport();

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
          var item=  this.radGridView1.Items.AddNew();
          Type t = item.GetType();
          PropertyInfo[] props = t.GetProperties();

          foreach (PropertyInfo prp in props)
          {

              try
              {

                  var name = prp.Name;
                  if (prp.PropertyType.ToString() == "System.DateTime")
                  {

                      
                      var value = DateTime.Now;

                      prp.SetValue(item, value, null);

                  }
              }

              catch { }
          }
          //DataObject newDataObject = item as DataObject;

          //object id = ModelID as object;
          //newDataObject.SetFieldValue("Model_ID", ModelID);


             try
            {
                DataObject newDataObject = item as DataObject;

                object id = ModelID as object;
                newDataObject.SetFieldValue("Model_ID", ModelID);

                foreach (var field in fieldList)
                {
                    if ((field.DefaultValue != null) || (field.DefaultValue != ""))
                    {

                        object obj = field.DefaultValue as object;
                        newDataObject.SetFieldValue(field.FieldName, obj);
                    }
                    
                }
            }
            catch { }

          
        }

        private void btnSave_Click(object sender, EventArgs e)
        { //perform

            SaveChanges();
        }

   
        private void btnCancel_Click(object sender, EventArgs e)
        {
            editedDataObjectList = new ObservableCollection<DataObject>();
            newDataObjectList = new ObservableCollection<DataObject>();
            deletedDataObjectList = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteRows();

        }

      

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            this.busyIndicator.IsBusy = true;
            SaveConfig();
            this.busyIndicator.IsBusy = false;
        }

       
        //DeleteConfig

      
        void ChildWin_Closed(object sender, WindowClosedEventArgs e)
        {
            LoadConfigSetting();
        }

        private void btnSetDefault_Click(object sender, EventArgs e)
        {
            SetDefault();

        }

        bool isClearFilter = false;
        bool isFirstFilter = true;
        private void ddlConfig_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
          
        }



        private void LoadData()
        {
            try
            {
                //if (!isFirstFilter)
                //{
                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;
                else
                {
                    this.radGridView1.IsBusy = true;
                    if (config.ID > 0)
                    {
                        isClearFilter = false;
                        GetDefaultSetting(config.Settings_ID);


                    }
                    else
                    {
                        isClearFilter = true;
                        ClearFilters();

                        settings = new RadGridViewSettings(this.radGridView1);

                        settings.LoadState(primaryXml);
                        newSqlText = sqlText;

                        GetTotalRows(newSqlText);
                        pageNumber = 1;
                        GetData(newSqlText, pageNumber, pageSize, tableName);
                        // isClearFilter = false;
                    }
                }
                // }

                isFirstFilter = false;
            }
            catch
            {
            }
            this.radGridView1.IsBusy = false;
        }
        #endregion



        #region Call Ria Services Method


        private void getTableData()
        {

            App app = (App)Application.Current;
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByDisplayDefIDQuery(DisplayID), CallbackTable, null);
        }

        private void CallbackTable(LoadOperation<Table> loadOp)
        {
            Table table = loadOp.Entities.FirstOrDefault();

            if (table != null)
            {
                tableName = table.TableName;
                tableID = table.ID;
                databaseName = table.DBName;
                getServerConnections(tableID);


            }
        }


        private void getServerConnections(int tableID)
        {

            App app = (App)Application.Current;
            LoadOperation<Server> loadOp = context.Load(context.GetServersByTableIDQuery(tableID), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {
            Server server = loadOp.Entities.FirstOrDefault();

            if (server != null)
            {
                connString = server.ConnectionString;

                //LoadOperation<View> loadOperation = context.Load(context.GetViewsByDisplayDefinitionIDQuery(displayDefID), CallbackView, null);

                GetViewsByDefinitionID();
            }
        }
        

        private void CallbackView(LoadOperation<View> loadOperation)
        {
            View view = loadOperation.Entities.FirstOrDefault();
            if (view != null)
            {
                viewID = view.ID;

                // pageSize = 
                singleTable = view.SingleTable;
                isReadOnly = view.ReadOnly;
                if (isReadOnly)
                {
                    btnInactive.Visibility = System.Windows.Visibility.Visible;
                    btnButtons.Visibility = System.Windows.Visibility.Collapsed; ;
                }
                GetTables();

            }
        }





        private void GetViewsByDefinitionID()
        {
            //LoadOperation<View> loadOperation = context.Load(context.GetViewsByDisplayDefinitionIDQuery(displayDefID), CallbackView, null);
            LoadOperation<GridDefinition> loadOperation = context.Load(context.GetGridDefinitionsByDisplayDefinitionIDQuery(displayDefID), CallbackGridDefinition, null);
        }

        private void CallbackGridDefinition(LoadOperation<GridDefinition> loadOperation)
        {
            GridDefinition grid = loadOperation.Entities.Where(g => g.GridNumber == 1).FirstOrDefault();
            if (grid != null)
            {


                // bottomPane.Title = ModelName + " Facility";
                View view = grid.View;
                viewID = view.ID;
                singleTable = view.SingleTable;
                isReadOnly = view.ReadOnly;
                if (isReadOnly)
                {
                    btnInactive.Visibility = System.Windows.Visibility.Visible;
                    btnButtons.Visibility = System.Windows.Visibility.Collapsed; 
                    radGridView1.IsReadOnly = true;
                }
                //  topPane.Title =  " : " + view.DisplayName;
                if (view.UseStoredProcedure.Value)
                {


                    if (view.ReadSP_ID != null)
                    {
                        useReadSP = true;
                        ReadSPID = view.ReadSP_ID.Value;

                        GetSPReadFields(viewID);


                        ///  
                    }

                    if (view.CreateSP_ID != null)
                    {
                        CreateSPID = view.CreateSP_ID.Value;
                        useCreateSP = true;
                    }

                    if (view.UpdateSP_ID != null)
                    {
                        UpdateSPID = view.UpdateSP_ID.Value;
                        useUpdateSP = true;
                    }


                    if (view.DeleteSP_ID != null)
                    {
                        DeleteSPID = view.DeleteSP_ID.Value;
                        useDeleteSP = true;

                    }

                }
                else
                {
                    GetTables();
                }



                ///LoadOperation<GridRelationship> loadOperation2 = context.Load(context.GetGridRelationshipQuery().Where(g => g.DisplayDefinition_ID == DisplayID), CallbackGridRelationshipSP, null);

            }


        }

        private void GetSPReadFields(int viewID)
        {
            LoadOperation<ReadField> ReadFieldLoadOperation = context.Load(context.GetReadFieldsQuery().Where(s => s.ViewID == viewID), callReadFieldLoadOperation, null);
        }

        void callReadFieldLoadOperation(LoadOperation<ReadField> ReadFieldLoadOperation)
        {
            if (ReadFieldLoadOperation != null)
            {

                GenerateColumnSP(ReadFieldLoadOperation.Entities.ToList());

                fieldListSP = new ObservableCollection<ReadField>(ReadFieldLoadOperation.Entities);
                var field = fieldList.FirstOrDefault();

            }
        }


        void PullDataSP()
        {

            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID), callStoredProcedureNameLoadOperation, null);

        }

        void callStoredProcedureNameLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                connString = storedProc.Server.ConnectionString;
                databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        parameters.Add(item.ParameterName + ";" + " Model_ID = '" + ModelID.ToString() + "'");
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                ExecuteStoreProcedure(storedProc.SP_Function, parameters, pageNumber, pageSize);
            }
        }


        void ExecuteStoreProcedure(string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            this.radGridView1.IsBusy = true;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted);
            ws.ExecuteStoreProcAsync(connString, databaseName, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }

        void ws_ExecuteStoreProcedureCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                 RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
             
            else if (e.ServiceError != null)
             RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });

            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView1.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    radGridView1.ItemsSource = list;
                    foreach (var item in desc)
                    {
                        radGridView1.FilterDescriptors.Add(item);
                    }
                }
            }
            this.radGridView1.IsBusy = false;
        }
        private void GenerateColumnSP(List<ReadField> readFieldList)
        {
            if (canGenerateColumns)
            {
                int i = 1;
                tempFieldList = new List<TempField>();
                this.radGridView1.AutoGenerateColumns = false;
                this.radGridView1.ShowInsertRow = false;

                foreach (ReadField field in readFieldList)
                {
                    tableName = field.TableName;

                    if (field.StoredProcedureNames_ID != null)
                    {


                        if (field.UseValueStoredProcedure)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(field.FieldName);
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = field.FieldName;
                            testName = field.FieldName;


                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";
                            columnComboBox.IsFilteringDeferred = true;
                            columnComboBox.IsFilteringDeferred = true;
                            columnComboBox.IsReadOnly = field.ReadOnly;
                            if (field.Hidden)
                            {
                                columnComboBox.IsVisible = false;
                            }
                            else
                            {
                                this.radGridView1.Columns.Add(columnComboBox);
                            }

                            tempFieldList.Add(new TempField { FieldID = field.ID, ID = field.StoredProcedureNames_ID.Value, Name = field.FieldName, Index = i, StoreProcName = field.SP_Function, OptionCompletion = field.FieldOptionFilter, AutoComplete = field.AutoPopulate, Hidden = field.Hidden, FieldType = field.FieldType });
                            i++;
                        }
                        else
                        {


                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView1.Columns.Add(column);
                            }
                        }
                    }
                    else
                    {


                        GridViewDataColumn column = new GridViewDataColumn();
                        column.DataMemberBinding = new Binding(field.FieldName);
                        column.Header = field.DisplayName;
                        column.UniqueName = field.FieldName;
                        column.IsFilteringDeferred = true;
                        column.IsReadOnly = field.ReadOnly;
                        if (field.FieldType == "datetime")
                        {
                            column.DataFormatString = "{0:dd-MMM-yyyy H:mm:ss tt}";
                        }
                        if (field.FieldType == "date")
                        {
                            column.DataFormatString = "{0:dd-MMM-yyyy}";
                        }

                        if (field.Hidden)
                        {
                            column.IsVisible = false;
                        }

                        else
                        {
                            this.radGridView1.Columns.Add(column);
                        }




                    }
                }
                canGenerateColumns = false;

                tempFieldList = tempFieldList.OrderBy(t => t.Index).ToList();
                indexMax = tempFieldList.Count;

                LoadDropDownSP();
                Attach();


            }

            LoadConfigSetting();
            //   GetTables2();

            ReCountPage();
        }

        void ReCountPage()
        {

            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID), callProcRowTotalLoadOperation, null);

        }

        void callProcRowTotalLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                connString = storedProc.Server.ConnectionString;
                databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {

                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        parameters.Add(item.ParameterName + ";" + "  ");
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                GetTotalRows(connString, databaseName, storedProc.SP_Function, parameters);
            }
        }

        void GetTotalRows(string conn, string db, string procName, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnTotalCompleted);
            ws.TotalProcRowsAsync(conn, procName, db, parameters);

            ///.Progress.Start();
        }
        double totalRows = 0;
        void ws_ReturnTotalCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                double total = (double)(e.Result);
                totalRows = total ;
                lblRows.Text = totalRows.ToString() + " Rows"; 
                if (total > pageSize)
                {
                    double totalDouble = (double)(total / (double)pageSize);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager.lblTotal.Text = tol.ToString();
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    pageTotal = tol;
                }
                else
                {
                    dataPager.lblTotal.Text = "1";
                    dataPager.txtNumber.Text = pageNumber.ToString();

                }
            }
            //this.Progress.Stop();
        }

        private void GetTables()
        {
            //radGridView1.IsBusy = true;
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackTables, null);


        }



        private void CallbackTables(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                tableList = loadOp.Entities.ToList();
                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(viewID), CallbackViewFields, null);
               var table = tableList.FirstOrDefault();
               if (table != null)
               {
                   GetExendedPropertiesData(table.Server.ConnectionString, ExtendedPropertySQL.ExtendedPropertiesSQl(table.DBName, table.TableName), 1, 100, null);
               }
            }

        }



        private void CallbackViewFields(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                viewFieldSummary = new ObservableCollection<ViewFieldSummary>();
                foreach (ViewFieldSummary summary in loadOp.Entities)
                {
                    viewFieldSummary.Add(summary);

                }

                fieldList = loadOp.Entities.ToList();

                GetRelationshipsRefresh();

            }
        
        }


        private void GetGroupViewFilds()
        {
            int groupID = 0;
            if(Globals.CurrentUser.Group_ID !=null)
                groupID = Globals.CurrentUser.Group_ID.Value;
            LoadOperation<GroupViewFilter> loadOp = context.Load(context.GetGroupViewFiltersByGroupViewIDQuery(groupID, viewID), CallbackGroupViewFilters, null);

        }



        private void CallbackGroupViewFilters(LoadOperation<GroupViewFilter> results)
        {

            if (results != null)
            {
                  GroupViewFilterList = results.Entities.ToList();
            }
            string sql = CreateReadOnlyTable();
            sqlText = sql;
            newSqlText = sql;
            //btnSave.IsEnabled = false;

            GenerateColumns();
            //createSqlWithNewFilters();
         //   GetTotalRows(newSqlText);
        //GetData(newSqlText, pageNumber, pageSize, tableName);
        }



        private void GetRelationshipsRefresh()
        {
            EditorContext cont = new EditorContext();
            LoadOperation<ViewRelationship> loadOp = cont.Load(cont.GetViewRelationshipByViewIDQuery(viewID), CallbackRelationships, null);




        }




        private void CallbackRelationships(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {


                relationshipList = new List<ViewRelationship>();
                relationshipList = loadOp.Entities.ToList();

                GetGroupViewFilds();
            }
        }

        private void GetFields()
        {
            LoadOperation<Field> loadOperation = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == tableID), CallbackFields, null);
        }


        private void CallbackFields(LoadOperation<Field> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                this.radGridView1.AutoGenerateColumns = false;
                foreach (Field field in loadOperation.Entities)
                {
                    GridViewDataColumn column = new GridViewDataColumn();
                    column.DataMemberBinding = new Binding(field.FieldName.Replace(" ", "_"));
                    column.Header = field.FriendlyName;
                    column.UniqueName = field.FieldName;
                    if (field.Type == "datetime")
                    {
                        column.DataFormatString = "{0:dd/MM/yyyy}";
                    }


                    this.radGridView1.Columns.Add(column);
                }

            }

        }


        private void LoadDropDowns()
        {
            if (tempFieldList.Count > 0)
            {


                TempField temp = tempFieldList.Where(t => t.Index == fieldIndex + 1).FirstOrDefault();
                dropDownName = temp.Name;
                LoadOperation<DropDownPairResult> loadOp = context.Load(context.GetDropDownPairResultQuery(temp.ID), CallbackDropDown, null);
                fieldIndex = temp.Index;
            }
        }


        string filterOption = "";
        string filterOption2 = "";
        bool autoPopulate = false;
        bool autoPopulate2 = false;
        string filterOption4 = "";
        bool autoPopulate4 = false;
        int currFieldID = 0;
        int currFieldID2 = 0;
        int currFieldID4 = 0;
        bool hidden = false;
        bool hidden2 = false;
        bool hidden4 = false;
        string dataType = "";
        string dataType2 = "";
        string dataType4 = "";

        string filterOption3 = "";
        string filterOption5 = "";
        bool autoPopulate3 = false;
        bool autoPopulate5 = false;
        // string filterOption3 = "";
        // bool autoPopulate5 = false;
        int currFieldID3 = 0;
        int currFieldID5 = 0;

        bool hidden3 = false;
        bool hidden5 = false;

        string dataType3 = "";
        string dataType5 = "";
        private void LoadDropDownSP()
        {
            if (tempFieldList.Count > 0)
            {


                TempField temp = tempFieldList.Where(t => t.Index == fieldIndex + 1).FirstOrDefault();
                dropDownName = temp.Name;
                filterOption = temp.OptionCompletion;
                autoPopulate = temp.AutoComplete;
                currFieldID = temp.FieldID;
                hidden = temp.Hidden;
                dataType = temp.FieldType.ToLower();
                LoadOperation<StoredProcedureName> loadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(x => x.ID == temp.ID), callbackDPLoadOperationSP, null);

                fieldIndex = temp.Index;
            }
        }

        void callbackDPLoadOperationSP(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();

                //databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();
                string subSql = "";


                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                    if (item.ParameterName.Contains("Type"))
                    {
                        parameters.Add(item.ParameterName + ";" + "Facility");
                    }

                    if (item.ParameterName.Contains("Filter"))
                    {
                        parameters.Add(item.ParameterName + ";" + filterOption);
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                }

                GetDropdownDataSP(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, parameters, 1, 1000);

            }
        }


        void GetDropdownDataSP(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            ;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_GetColumnDataCompletedSP);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }



        void ws_GetColumnDataCompletedSP(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {
                    if (!hidden)
                    {

                        List<DropDownIntData> intDataList = new List<DropDownIntData>();
                        List<DropDownLongData> longDataList = new List<DropDownLongData>();
                        List<DropDownStringData> stringDataList = new List<DropDownStringData>();
                        try
                        {
                            if (dataType == "int")
                            {
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    intDataList.Add(new DropDownIntData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = intDataList;

                            }

                            else if (dataType == "bigint")
                            {

                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    longDataList.Add(new DropDownLongData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = longDataList;
                            }
                            else
                            {
                                stringDataList = new List<DropDownStringData>();
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    // long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                                }

                                ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = stringDataList;
                            }
                        }
                        catch
                        {
                            stringDataList = new List<DropDownStringData>();
                            foreach (var dataItem in list.Cast<DataObject>().ToList())
                            {
                                string value = dataItem.GetFieldValue("Value").ToString();

                                // long val = Convert.ToInt64(value);
                                string display = dataItem.GetFieldValue("Display").ToString();

                                stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                            }

                            ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = stringDataList;
                        }
                    }


                    if (autoPopulate)
                    {
                        try
                        {
                            DataObject data = list.Cast<DataObject>().FirstOrDefault();
                            string value = data.GetFieldValue("Value").ToString();
                            var readField = fieldList.Where(f => f.ID == currFieldID).FirstOrDefault();
                            readField.DefaultValue = value;
                        }
                        catch
                        {
                        }
                    }
                    if (fieldIndex < indexMax)
                    {
                        LoadDropDownSP();
                    }


                }

            }

        }



        private void CallbackDropDown(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();
                if (dropDown != null)
                {
                    GetColumnData(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");
                }

            }
        }


        private void LoadConfigSetting()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting, null);
        }

        private void CallbackSetting(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    settingObjectList.Add(sett);
                }
                settingObjectList.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 1, Settings_ID = 0 });

                ddlConfig.ItemsSource = settingObjectList;


                var query = (from s in settingObjectList
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                    GetDefaultSetting(query.Settings_ID);

                //getTableData();

            }


        }


        private void CallbackDefault(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    //primarySettings.ResetState();

                  //  settings = new RadGridViewSettings(this.radGridView1);
                  //  string xml = setting.Settings;
                   // settings.LoadState(xml);




                }
                catch
                {
                }
            }
        }
        #endregion



        #region Private Methods

        private void Cancel()
        {
            editedDataObjectList = new ObservableCollection<DataObject>();
            newDataObjectList = new ObservableCollection<DataObject>();
            deletedDataObjectList = new ObservableCollection<DataObject>();
         //   cMenu.IsOpen = false;
        }

        private void ExcelExport()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView1.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        private void SaveChanges()
        {
            try
            {
                if (newDataObjectList.Count > 0)
                {

                    if (useCreateSP)
                    {
                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == CreateSPID), callCreateSPLoadOperation, null);

                    }
                    else
                    {
                        InsertDataTable();
                    }

                }


                //Update
                if (editedDataObjectList.Count > 0)
                {

                    if (useUpdateSP)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == UpdateSPID), callUpdateSPLoadOperation, null);
                    }
                    else
                    {
                        Update_Click();
                    }
                }

                if (deletedDataObjectList.Count > 0)
                {
                    if (useDeleteSP)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == DeleteSPID), callDeleteSPLoadOperation, null);
                    }
                    else
                    {
                        DeleteDataTable();

                    }
                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }


        void callCreateSPLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" +ModelID.ToString());
                    }
                    else if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                    else if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }

                InsertTable(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, newDataObjectList, parameters);


            }
        }

        void callUpdateSPLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                    if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }


                UpdateTable(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, editedDataObjectList, parameters);


            }
        }
        void callDeleteSPLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                    if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }


                DeleteTable(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, deletedDataObjectList, parameters);


            }
        }
        private void InsertTable(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.InsertDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs>(ws_InsertDataTableCompleted);
            ws.InsertDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }
        void ws_InsertDataTableCompleted(object sender, EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = e.Result.ToString() + " rows have been added" });
               
            }
        }
        private void UpdateTable(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.UpdateDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs>(ws_UpdateDataTableCompleted);
            ws.UpdateDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_UpdateDataTableCompleted(object sender, EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                editedDataObjectList = new ObservableCollection<DataObject>();
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = e.Result.ToString() + " rows have been added" });
            }
        }

        private void DeleteTable(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.DeleteDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs>(ws_DeleteDataTableCompleted);
            ws.DeleteDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_DeleteDataTableCompleted(object sender, EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {

                deletedDataObjectList = new ObservableCollection<DataObject>();
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = e.Result.ToString() + " rows have been deleted" });
            }
        }
        private void DeleteRows()
        {
            try
            {

                if (this.radGridView1.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView1.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView1.Items.Remove(item as DataObject);
                    deletedDataObjectList.Add(item);
                }

                int count = deletedDataObjectList.Count;
            }
            catch
            {
            }
        }


        private void ClearFilters()
        {
        //  primarySettings.LoadOriginalState();
           
            this.radGridView1.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView1.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView1.FilterDescriptors.ResumeNotifications();

            //settings = new RadGridViewSettings(this.radGridView1);
            //  string xml = setting.Settings;
            // / /xmlCache = xml;
          //  primarySettings.LoadState(xmlCache);


        }

        private void SaveConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings = new RadGridViewSettings(this.radGridView1);
                    if (radGridView1.FilterDescriptors.Count > 0)
                    {
                    }


                    string settingsXML = settings.SaveState();
                    config.Setting.Settings = settingsXML;
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML; 
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                         
                        }
                        else
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                          

                            
                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }

        private void SaveConfigAs()
        {
            try
            {

                settings = new RadGridViewSettings(this.radGridView1);

                string settingsXML = settings.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 1, displayDefID);
                config.Closed += ChildWin_Closed;
                config.Left = (Application.Current.Host.Content.ActualWidth - config.ActualWidth) / 2;
                config.Top = (Application.Current.Host.Content.ActualHeight - config.ActualHeight) / 2;
                config.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                config.ShowDialog();
            }
            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }
       

        private void DeleteConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                            ClearFilters();
                            ddlConfig.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private bool isFirstLoad = true;
        string xmlCache = "";
        string primaryXml = "";
        private void GetDefaultSetting(int settingID)
        {
            //if (isFirstLoad)
            //{

            //    primarySettings = new RadGridViewSettings(this.radGridView1);
            //    primarySettings.SaveState();
            //    primaryXml = primarySettings.SaveState();
            //    isFirstLoad = false;
            //}


           
            //if(setting != null)
            if (settingID > 0)
            {
                ddlConfig.SelectedValue = settingID;
                // apply setting to the gridview

                try
                {
               // ClearFilters();
                    //  primarySettings.LoadOriginalState();
                   // primarySettings.ResetState();

                 //   settings = new RadGridViewSettings(this.radGridView1);
                  //  string xml = setting.Settings;
                  // / /xmlCache = xml;
                   /// settings.LoadState(xml);

                 //   createSqlWithNewFilters();



                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }

            
        }

        private string testName = "";


        private void GenerateColumns()
        {
            if(canGenerateColumns)
            {
            int i = 1;
            tempFieldList = new List<TempField>();
            this.radGridView1.AutoGenerateColumns = false;
            this.radGridView1.ShowInsertRow = false;
               

            foreach (ViewFieldSummary field in viewFieldSummary)
            {
                
                if (columnNames.Contains(field.FieldName))
                {
                    GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                    columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                    columnComboBox.Header = field.DisplayName;
                    columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                    testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                    columnComboBox.SelectedValueMemberPath = "Value";
                    columnComboBox.DisplayMemberPath = "Display";
                    columnComboBox.IsFilteringDeferred = true;
                    columnComboBox.IsFilteringDeferred = true;

                    columnComboBox.IsReadOnly = field.ReadOnly.Value;
                    if (field.Hidden.Value)
                    {
                        columnComboBox.IsVisible = false;
                    }
                    else
                    {
                        this.radGridView1.Columns.Add(columnComboBox);
                    }

                    
                }
                else
                {
                    if (field.UseValueField.Value)
                    {
                        GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                        columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                        columnComboBox.Header = field.DisplayName;
                        columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        columnComboBox.SelectedValueMemberPath = "Value";
                        columnComboBox.DisplayMemberPath = "Display";
                        columnComboBox.IsFilteringDeferred = true;
                        columnComboBox.IsReadOnly = field.ReadOnly.Value;
                        if (field.Hidden.Value)
                        {
                            columnComboBox.IsVisible = false;
                        }
                        else
                        {
                            this.radGridView1.Columns.Add(columnComboBox);
                        }


                        tempFieldList.Add(new TempField { ID = field.FieldID, Name = CharacterHandler.ReplaceSpecialCharacter(field.FieldName), Index = i });
                        i++;




                    }
                    else
                    {

                        GridViewDataColumn column = new GridViewDataColumn();
                        column.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                        column.Header = field.DisplayName;
                        column.UniqueName = field.FieldName;
                        column.IsFilteringDeferred = true;
                        if (field.DataType == "datetime")
                        {
                            column.DataFormatString = "{0:dd-MMM-yyyy H:mm:ss tt}";
                        }

                        if (field.DataType == "date")
                        {
                            column.DataFormatString = "{0:dd-MMM-yyyy}";
                        }
                        if (field.IsCalculatedField.Value == true)
                        {


                            column.IsReadOnly = true;

                           // column.Background = Resources["SkyBlue2"] as SolidColorBrush;// new SolidColorBrush(Colors.Blue);
                        }

                        column.IsReadOnly = field.ReadOnly.Value;
                        if (field.Hidden.Value)
                        {
                            column.IsVisible = false;
                        }
                        else
                        {
                            this.radGridView1.Columns.Add(column);
                        }

                        
                        
                        
                    }
                }
            }
            canGenerateColumns = false;

        
            tempFieldList = tempFieldList.OrderBy(t => t.Index).ToList();
            indexMax = tempFieldList.Count;
            LoadExtendedProperties();
            LoadDropDowns();

            primarySettings = new RadGridViewSettings(this.radGridView1);
            string xml = primarySettings.SaveState();
               xmlCache = xml;
                /// settings.LoadState(xmlCache);
             //  GenerateDynamicDemo();
               Attach();
        }
        }




     

        private void LoadExtendedProperties()
        {
            foreach (DataObject dataObject in propertiesList)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView1.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList;
                }
                catch
                {
                }
            }
        }

        private string CreateReadOnlyTable()
        {
            string sql = string.Empty;
            //table list
            //display field list
            //relationshiplist

            string tables = " From ";
            string fields = " Select ";
            string relationships = " Where ";
            string viewFilter = "";
            string modelClause = " Model_ID =" + ModelID.ToString() + " ";
            foreach (Table table in tableList)
            {
                tables += "[" + table.DBName + "].[dbo].[" + table.TableName + "] as " + "[" + table.TableName.ToString() + "]" + " ,";
            }

            foreach (ViewFieldSummary field in fieldList)
            {
                if (field.TableID > 0)
                    if (field.TableID > 0)
                    {
                        fields += field.TableField + " ,";//" as [" + field.DisplayName + "] ,";
                    }
                    else
                    {
                        fields += " (" + field.Expression + " ) as [" + field.DisplayName + "] ,";
                    }
            }

            foreach (ViewRelationship rship in relationshipList)
            {
                relationships += " " + rship.Summary + " and";

            }

          
            foreach (GroupViewFilter item in GroupViewFilterList)
            {
                viewFilter += " " + item.Summary + " and";
            }
            if (viewFilter != "")
            {
                viewFilter = viewFilter.TrimEnd('d');
                viewFilter = viewFilter.TrimEnd('n');
                viewFilter = viewFilter.TrimEnd('a');
            }

            tables = tables.TrimEnd(',');
            fields = fields.TrimEnd(',');
            relationships = relationships.TrimEnd('d');
            relationships = relationships.TrimEnd('n');
            relationships = relationships.TrimEnd('a');


            if (relationships == " Where ")
            {
                if (viewFilter != "")
                {
                    relationships += viewFilter + " and " + modelClause;
                }
                else
                {
                    relationships += modelClause;
                }

            }
            else
            {
                if (viewFilter != "")
                {
                    relationships += " and " + viewFilter + " and " + modelClause;
                }
                else
                {
                    relationships += " and " + modelClause;
                }
            }


            sql = fields + tables + relationships;

            return sql;

        }
        #endregion

        

        #region Call WFC Services Methods

        private void GetData(string sql, int pagenumber, int pagesize, object userState)
        {
            this.radGridView1.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {
                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView1.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    
                    radGridView1.ItemsSource = list;
                    foreach (var item in desc)
                    {
                        radGridView1.FilterDescriptors.Add(item);
                    }

                    if (canGenerateColumns)
                    {
                        GetExendedPropertiesData(connString, ExtendedPropertySQL.ExtendedPropertiesSQl(databaseName, tableName), 1, 500, null);

                        if (isReadOnly)
                        {
                            btnInactive.Visibility = Visibility.Visible;
                            btnButtons.Visibility = Visibility.Collapsed;
                            radGridView1.IsReadOnly = true;
                        }
                        else
                        {
                            btnInactive.Visibility = Visibility.Collapsed;
                            btnButtons.Visibility = Visibility.Visible;
                        }

                    }
                    else
                    {
                        //if (!isClearFilter)
                        //{
                        //    settings = new RadGridViewSettings(this.radGridView1);

                        //    settings.LoadState(xmlCache);
                        //}
                    }


                }
               
            }
            this.radGridView1.IsBusy = false;
        }



        void Update_Click()
        {
            this.busyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.UpdateCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.UpdateCompletedEventArgs>(ws_UpdateCompleted);
            ws.UpdateAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(editedDataObjectList as IEnumerable, _tables), tableName);

            ///.Progress.Start();
        }

        void ws_UpdateCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.UpdateCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                editedDataObjectList = new ObservableCollection<DataObject>();
             
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = e.Result});
            }
            this.busyIndicator.IsBusy = false;
        }



        void InsertDataTable()
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(newDataObjectList as IEnumerable, _tables), tableName);

            ///.Progress.Start();
        }

        void ws_InsertCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
             
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = e.Result });
            }
            //this.Progress.Stop();
        }


        void GetTotalRows(string parameterSql)
        {
            var ws = WCF.GetService();
            ws.TotalRowsCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TotalRowsCompletedEventArgs>(ws_ReturnTotalCompleted);
            ws.TotalRowsAsync(connString, parameterSql, databaseName);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.TotalRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                double total = (double)(e.Result);
                totalRows = total ;
                lblRows.Text = totalRows.ToString() + " Rows"; 
                if (total > pageSize)
                {
                    double totalDouble = (double)(total / (double)pageSize);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager.lblTotal.Text = tol.ToString();
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    pageTotal = tol;
                }
                else
                {
                    dataPager.lblTotal.Text = "1";
                    dataPager.txtNumber.Text = pageNumber.ToString();

                }
            }
            //this.Progress.Stop();
        }
        void radGridView1_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {

            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            int index = this.radGridView1.Items.IndexOf(this.radGridView1.SelectedItem);
            int rowNumber = ((pageNumber - 1) * pageSize) + (index + 1);
            lblRows.Text = rowNumber.ToString() + " Of " + totalRows.ToString() + " Rows";
            //  ReCountPage5();
        }

        private void SetDefault()
        {
            UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 1);
            }
        }


        void ws_updateUserConfigDefaultCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "New default configurating setting have been set" });
                   
                }
                else
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error!, Fail to set new default settings" });

                    
                }
            }
            else
            {

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error" });
                
            }
        }



        private void GetColumnData(string conn, string sql, int pagenumber, int pagesize, object userState)
        {
            this.busyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = list;

                    if (fieldIndex < indexMax)
                    {
                        LoadDropDowns();
                    }


                }
                this.busyIndicator.IsBusy = false;
            }
            this.busyIndicator.IsBusy = false;
        }





        private void GetExendedPropertiesData(string conn, string sql, int pagenumber, int pagesize, object userState)
        {
            this.busyIndicator.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetExendedPropertiesDataCompleted);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetExendedPropertiesDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {
                   

                    columnNames = new List<string>();
                    foreach (DataObject dataObject in list)
                    {

                        propertiesList.Add(dataObject);
                        string column = dataObject.GetFieldValue("ColumnName").ToString();
                        columnNames.Add(column);
                    }


                    

                }

                this.busyIndicator.IsBusy = false;
            }
           // GenerateColumns();
           
          //  LoadConfigSetting();
            this.busyIndicator.IsBusy = false;
        }

        void DeleteDataTable()
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(deletedDataObjectList as IEnumerable, _tables), tableName, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
             
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = e.Result });
            }
            //this.Progress.Stop();
        }
        
        #endregion


        
        #region GridView Events
        private void radGridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e)
        {

        }

        private void radGridView_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList.Contains(dataObject))
                        {
                            if (newDataObjectList.Contains(dataObject))
                            {
                                this.newDataObjectList.Remove(dataObject);
                                this.newDataObjectList.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList.Remove(dataObject);
                            this.editedDataObjectList.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void radGridView_Filtered(object sender, GridViewFilteredEventArgs e)
        {
            PullData();
            
        }

        private void createSqlWithNewFilters()
        {
           // radGridView1.IsBusy = true;
            string newSql = "";
           

            //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)


            if (radGridView1.FilterDescriptors.Count > 0)
            {


                newSql += OperatorHandler.BulidQuery(radGridView1);



            }

            if (newSql.Length > 0)
            {
                newSql = newSql.Remove(newSql.Length - 3, 3);

                if (sqlText.Contains("Where"))
                {
                    newSqlText = sqlText + " and " + newSql;
                }
                else
                {
                    newSqlText = sqlText + " Where " + newSql;
                }
            }
            else
            {
                newSqlText = sqlText;

            }

            //GetTotalRows(newSqlText);
            //pageNumber = 1;
            //GetData(newSqlText, pageNumber, pageSize, tableName);
        }

        void gridView_Deleted(object sender, GridViewDeletedEventArgs e)
        {
            DataObject dataObject = e.Items as DataObject;
            deletedDataObjectList.Add(dataObject);



        }


        private void gridView_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e)
        {
            //RadComboBox countries = e.DetailsElement.FindName("rcbCountries") as RadComboBox;
            //countries.ItemsSource = GetCountries();
            // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }
        #endregion


        #region GridMenu Events & Methods
        public MouseButtonEventHandler gridRightClick { get; set; }

        private void radGridView1_Loaded(object sender, GridViewRowItemEventArgs e)
        {
           
            //e.Handled = true;
        //    GenerateDynamicDemo()
           // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
        }

        private void radGridView1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
           e.Handled = true;
          // tab_MouseRightButtonUp(sender, e);
          //  cMenu.IsOpen = true;
          //  OnMenuOpened(sender, 
           
        }

        //private void GenerateDynamicDemo()
        //{
        //    // Add a DataGrid control with some sample data to the layout root
        //    //var data = new ObservableCollection<string>("Item 1,Item 2,Item 3,Item 4,Item 6,Item 7,Item 8".Split(','));
        //    //DataGrid dataGrid1 = new DataGrid() { Margin = new Thickness(50), ItemsSource = data };
        //    //this.LayoutRoot.Children.Add(dataGrid1);

        //    // Create the submenu
        //    var pmTimeNow = new PopupMenu();
        //    pmTimeNow.AddItem("Time Now", null);
        //    // Create the main menu
        //    var pmMain = new PopupMenu { AccessShortcut = "Ctrl+Alt+M", OpenOnAccessKeyPressed = true };
        //    //pmMain.OpenNextTo(MenuOrientationTypes.MouseBottomRight, FrameworkElement.
        //    // Add the menu items

        //    pmMain.AddItem("Delete row", delegate { deletedDataObjectList.RemoveAt(pmMain.GetClickedElement<DataGridRow>().GetIndex()); });
        //    pmMain.AddSeparator();
        //    pmMain.AddSubMenu(pmTimeNow, "Get Time ", "images/arrow.png", null, null, false, null); // Attach the submenu pmTimeSub
        //    pmMain.AddSeparator();
        //    pmMain.AddItem("Demo2", delegate { App.Current.Host.NavigationState = "/Views/Demo2.xaml"; });
            
        //    // Set dataGrid1 as the trigger element 
        //    pmMain.AddTrigger(TriggerTypes.RightClick, radGridView1);
        //   // RadRowItem
        //    // Showing main menu
        //    pmMain.Showing += (sender, e) =>
        //    {
        //        pmMain.PopupMenuItem(0).Header = "Delete " + radGridView1.SelectedItem;
        //        pmMain.PopupMenuItem(0).IsVisible =
        //        pmMain.PopupMenuItem(1).IsVisible = pmMain.GetClickedElement<DataGridRow>() != null;
        //    };
        //    // Showing submenu
        //    pmTimeNow.Showing += delegate
        //    {
        //        pmTimeNow.PopupMenuItem(0).Header = DateTime.Now.ToLongTimeString();
        //    };
        //   // pmMain.Open(
        //}

     
        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
         //   GridViewHeaderMenu.isHeader = false;
            e.Handled = true;
           // tab_MouseRightButtonUp(sender, e);
          //tab_MouseRightButtonUp(sender, e);
          // cMenu.StaysOpen  = true;
        }
        RadContextMenu cMenu;
        RadTabItem selectedTab;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            GridViewHeaderMenu.isHeader = false;

           var element = sender as UIElement;
            
           
            cMenu = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Changes";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Add";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Selected Rows";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
             exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
             menuItem.Items.Add(exportItem);
             exportItem = new RadMenuItem();
             exportItem.Header = "ExcelML";
             exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
             menuItem.Items.Add(exportItem);
             exportItem = new RadMenuItem();
             exportItem.Header = "Word";
             exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
             menuItem.Items.Add(exportItem);
             exportItem = new RadMenuItem();
             exportItem.Header = "Csv";
             exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
             menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings As";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Set Current Filter Settings as Defualt";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header ="Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            //menuItem = new RadMenuItem();
            //menuItem.Header = " ";
           
            //cMenu.Items.Add(menuItem);
            menuItem = new RadMenuItem();
            menuItem.Header = " ";

            cMenu.Items.Add(menuItem);
            cMenu.PlacementTarget = radGridView1;

            Point p = e.GetPosition(this.radGridView1);


            cMenu.Placement = PlacementMode.MousePoint;
           
           // cMenu.IconColumnWidth = 0;
            cMenu.HorizontalOffset = p.X -10;
            cMenu.VerticalOffset = p.Y+20;


            cMenu.IsOpen = true;

           // cMenu.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
          //  RadContextMenu.SetContextMenu(this.radGridView1, cMenu);
            //cMenu.IsOpen = true;
          //  cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X + 350;
          //  cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }
        void RadGridView1_RowLoaded(object sender, RowLoadedEventArgs e)
        {
           if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {
               // GenerateDynamicDemo();
              //  ((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(OnMenuOpened);
            //  ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                
            }
        }


        void menuItem_Click(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;
           
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
          //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges();
                        break;
                    case "Add New":
                        radGridView1.BeginInsert();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault();
                        break;


                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }
              //  cMenu.IsOpen = false;
            }
        }

        private void RadContextMenu_ItemClick(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges();
                        break;
                    case "Add New":
                        radGridView1.BeginInsert();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                       DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault();
                        break;


                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }
            }
          // cMenu.IsOpen = false;
        }

        private void Export(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView1.Export(stream, exportOptions);
                    }
                }
            }
            catch
            {
               // cMenu.IsOpen = false;
            }
        }


        private void RadContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
              //  menu.IsOpen = false;
            }
        }
        #endregion

        private void btnCascade_Click(object sender, RoutedEventArgs e)
        {
            updateLayOut();
           ClearFilters();
              //primarySettings.LoadOriginalState();
           //primarySettings.ResetState();


         //   settings = new RadGridViewSettings(this.radGridView1);
            UserGridConfig setting = ddlConfig.SelectedItem as UserGridConfig;
            if (setting != null)
            {
                if (setting.Settings_ID > 0)
                {

                    LoadOperation<Setting> loadOperation = context.Load(context.GetSettingsQuery().Where(x => x.ID == setting.Settings_ID), CallbackSettings, null);

                }
                else
                {
                    if (useReadSP)
                    {

                        PullDataSP();
                    }
                    else
                    {
                        PullData();
                    }
                }
            }

            else
            {
                if (useReadSP)
                {

                    PullDataSP();
                }
                else
                {
                    PullData();
                }
            }         
        }

        void PullData()
        {
            createSqlWithNewFilters();
            GetTotalRows(newSqlText);
            pageNumber = 1;
            GetData(newSqlText, pageNumber, pageSize, tableName);
        }

        void CallbackSettings(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache = xml;
                settings.LoadState(xml);
            }

            if (useReadSP)
            {

                PullDataSP();
            }
            else
            {
                PullData();
            }
        }

        #region MyRegion

        private void Attach()
        {
            if (radGridView1 != null)
            {
                
                // create menu
                RadContextMenu contextMenu = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu, StyleManager.GetTheme(radGridView1));

                contextMenu.Opened += OnMenuOpened;
                contextMenu.ItemClick += OnMenuItemClick;

                RadContextMenu.SetContextMenu(radGridView1, contextMenu);
            }
        }
          void OnMenuOpened(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
                RadContextMenu menu = (RadContextMenu)sender;
                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

                if (cell != null)
                {
                    menu.Items.Clear();

                    RadMenuItem item = new RadMenuItem();
                    item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                    menu.Items.Add(item);

                    item = new RadMenuItem();
                    item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                    menu.Items.Add(item);

                    item = new RadMenuItem();
                    item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                    menu.Items.Add(item);

                    item = new RadMenuItem();
                    item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                    menu.Items.Add(item);

                    item = new RadMenuItem();
                    item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                    menu.Items.Add(item);

                    item = new RadMenuItem();
                    item.Header = "Choose Columns:";
                    menu.Items.Add(item);
                    
                    // create menu items
                    foreach (GridViewColumn column in radGridView1.Columns)
                    {
                        RadMenuItem subMenu = new RadMenuItem();
                        subMenu.Header = column.Header;
                        subMenu.IsCheckable = true;
                        subMenu.IsChecked = true;

                        Binding isCheckedBinding = new Binding("IsVisible");
                        isCheckedBinding.Mode = BindingMode.TwoWay;
                        isCheckedBinding.Source = column;

                        // bind IsChecked menu item property to IsVisible column property
                        subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                        item.Items.Add(subMenu);
                    }
                }
                else if (gridCell != null)
                {
                    menu.Items.Clear();
                    //cMenu = new RadContextMenu();
                    RadMenuItem menuItem;

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Save Changes";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Add";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Edit";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Delete Selected Rows";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Export";
                    RadMenuItem exportItem = new RadMenuItem();
                    exportItem = new RadMenuItem();
                    exportItem.Header = "Excel";
                    exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menuItem.Items.Add(exportItem);
                    exportItem = new RadMenuItem();
                    exportItem.Header = "ExcelML";
                    exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menuItem.Items.Add(exportItem);
                    exportItem = new RadMenuItem();
                    exportItem.Header = "Word";
                    exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menuItem.Items.Add(exportItem);
                    exportItem = new RadMenuItem();
                    exportItem.Header = "Csv";
                    exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menuItem.Items.Add(exportItem);
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Save Filter Settings";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Save Filter Settings As";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Delete Filter Settings";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Set Current Filter Settings as Defualt";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    menuItem = new RadMenuItem();
                    menuItem.Header = "Cancel";
                    menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                    menu.Items.Add(menuItem);

                    //menuItem = new RadMenuItem();
                    //menuItem.Header = " ";

                    //cMenu.Items.Add(menuItem);
                    //menuItem = new RadMenuItem();
                    //menuItem.Header = " ";

                    //cMenu.Items.Add(menuItem);
                    //cMenu.PlacementTarget = radGridView1;

                    //Point p = e.GetPosition(this.radGridView1);


                    //cMenu.Placement = PlacementMode.MousePoint;

                    //// cMenu.IconColumnWidth = 0;
                    //cMenu.HorizontalOffset = p.X - 10;
                    //cMenu.VerticalOffset = p.Y + 20;


                    //cMenu.IsOpen = true;

                }

                else
                {
                    menu.IsOpen = false;
                }
            //}
        }

        void OnMenuItemClick(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView1.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView1.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView1.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView1.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView1.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView1.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView1.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView1.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }
        #endregion


    }

    //public class TempField
    //{
    //    public int ID { get; set; }
    //    public string Name { get; set; }
    //    public int Index { get; set; }
    //    public string StoreProcName { get; set; }

    //    public string OptionCompletion { get; set; }

    //}

    //public class TempExtendedProperty
    //{
    //    public string Value { get; set; }
    //    public string Display { get; set; }
      
    //}

    //public class PageNumber
    //{
    //    public int Size { get; set; }
    //    public string Display { get; set; }

    //}
}