﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BMA.MiddlewareApp.Web;
using Telerik.Windows.Controls;
using BMA.MiddlewareApp.Admin;
using System.Collections.ObjectModel;
using System.Collections;
using System.Reflection;
using System.Windows.Data;
using ExcelDataReader.Silverlight;
using System.IO;
using ExcelDataReader.Silverlight.Data;

namespace BMA.MiddlewareApp.Controls
{
    public partial class ImportWizard
    {
        ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> _tables;
        public int currentStep = 1;
        public string currentProvider;
        public static ObservableCollection<ControlType> sheetList;
        public static ObservableCollection<SheetColumn> sheetColunList;
        public static bool IsfirstRowHeader;
        public static int curSheetIndex;
        public static string currFieldName;
        public static string authentication;
        ObservableCollection<ReadField> SourceFieldList = new ObservableCollection<ReadField>();
        ObservableCollection<ControlType> SourceFieldList2 = new ObservableCollection<ControlType>();
        private int ViewID, ServerID, CreateStoreProcID;

        private bool insertOnly;
        public ImportWizard(string DestinationTable, long ModelId, List<ReadField> FieldList, int viewID, int serverID, int createStoreProcID, bool isInsertOnly)
        {
            InitializeComponent();
            //  OKButton.IsEnabled = false;
            btnPreview.Visibility = System.Windows.Visibility.Collapsed;
            IsfirstRowHeader = false;
            SourceFieldList = new ObservableCollection<ReadField>();
            ViewID = viewID;
            ServerID = serverID;
            insertOnly = isInsertOnly;
            CreateStoreProcID = createStoreProcID;
            foreach (var item in FieldList)
            {
                SourceFieldList.Add(item);
            }
            this.Unloaded += ImportWizard_Unloaded;

            ucDataSoure.Notify += ucDataSoure_Notify;
            ucDataSoure.NotifyReady += ucDataSoure_NotifyReady;

        }

        void ucDataSoure_Notify(object sender, EventArgs e)
        {
            OKButton.Content = "Wait...";
            OKButton.IsEnabled = false;
        }

        void ucDataSoure_NotifyReady(object sender, EventArgs e)
        {
            OKButton.Content = "Next";
            OKButton.IsEnabled = true;
        }

        void ImportWizard_Unloaded(object sender, RoutedEventArgs e)
        {
            var ws = WCF.GetService();

            ws.RemoveTempFileAsync(currFieldName);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Steps();

        }


        private void Steps()
        {
            ControlType source = ucDataSoure.ddlDataSouce.SelectedItem as ControlType;
            if (currentStep == 1)
            {

                if (source == null)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "Select Data Source." });


                }
                else
                {
                    currentProvider = source.Value;
                    if (source.Value == "Microsoft Excel")
                    {
                        if (ucDataSoure.txtFilePath.Text == "")
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "File Path is not speified." });
                            return;
                        }
                    }


                    currentStep = 1;
                }
            }
            if (currentStep == 1)
            {
                btnPreview.Visibility = System.Windows.Visibility.Collapsed;
                ucDataSoure.Visibility = System.Windows.Visibility.Collapsed;
                btnBack.IsEnabled = true;
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Visible;
                ucValidateData.Visibility = System.Windows.Visibility.Collapsed;
                ucExcelSelectSheets.ddlDataSouce.ItemsSource = sheetList;
                currentStep = 2;
             if (source.Value == "Microsoft Excel")

                  {
                      ucExcelSelectSheets.lblTitle.Text = "Choose Sheet Name";
                      ucExcelSelectSheets.pnlAccess.Visibility = System.Windows.Visibility.Collapsed;
                      ucExcelSelectSheets.pnlExcess.Visibility = System.Windows.Visibility.Visible;
                currentStep = 2;
                return;
                 }
                

                else  if (source.Value == "Microsoft Access")
                {
                    ucExcelSelectSheets.pnlAccess.Visibility = System.Windows.Visibility.Visible;
                    ucExcelSelectSheets.pnlExcess.Visibility = System.Windows.Visibility.Collapsed;
                    ucExcelSelectSheets.lblTitle.Text = "Enter Table Name";
                    bool useAuth=ucDataSoure.chkAuthentication.IsChecked.Value;
                    authentication = "";
                    if (useAuth)
                    {

                       authentication = "Jet OLEDB:Database Password=" + ucDataSoure.txtPassword.Password + ";";
                    }
                    else
                    {
                       authentication = "Persist Security Info=False;";
                    }
                    string accessSource = ucDataSoure.txtAccessPath.Text;
                    
                //    GetAccessTableName(accessSource, accessSource);
                    return;
                }

            }

            if (currentStep == 2)
            {
                ControlType sheet = ucExcelSelectSheets.ddlDataSouce.SelectedItem as ControlType;
                if (source.Value == "Microsoft Excel")
                {
                    if (sheet == null)
                    {
                        RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Alert" }, Content = "Select Sheet Name!" });
                        return;
                    }
                }
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Collapsed;
                uFieldMapping.Visibility = System.Windows.Visibility.Visible;
                ucValidateData.Visibility = System.Windows.Visibility.Collapsed;
                btnPreview.Visibility = System.Windows.Visibility.Visible;
                SourceFieldList2 = new ObservableCollection<ControlType>();
                foreach (var item in SourceFieldList)
                {
                    SourceFieldList2.Add(new ControlType { Display = item.FieldName, Value = item.FieldName, DataType = item.FieldType, Name = item.FieldName, index = item.ID });
                }
                //ObservableCollection<ReadField> SourceFieldList2 = new ObservableCollection<ReadField>();
                //SourceFieldList2.Add(new ReadField { FieldName="Test", ViewID =1 });

                uFieldMapping.gdField.ItemsSource = SourceFieldList2;
                string filename = "";
               
                if (source.Value == "Microsoft Excel")
                {
                  
                    if (sheet != null)
                    {
                        filename = sheet.Display;

                        bool isRowFirst = ucDataSoure.chkFirstRow.IsChecked.Value;
                        ControlType xcttype = ucDataSoure.ddlExcelVersion.SelectedItem as ControlType;
                        string ext = xcttype.Value;
                        GetSheetNames(currFieldName, ext, filename, isRowFirst);


                        //  ControlType sheet = ucExcelSelectSheets.ddlDataSouce.SelectedItem as ControlType;

                        //  ((GridViewComboBoxColumn)this.uFieldMapping.gdField.Columns["Value"]).ItemsSource = sheetColunList.Where(s => s.SheetIndex == sheet.index);

                        currentStep = 3;
                        return;//0795619
                        //   ReadField fied = new ReadField();
                    }
                   
                    
                }

                else if (source.Value == "Microsoft Access")
                {
                    string path = ucDataSoure.txtAccessPath.Text;
                    string table = ucExcelSelectSheets.txttableName.Text;
                    GetAccessColumnNames(path, path, table);
                    currentStep = 3;
                    return;//0795619
                }
            }

            if (currentStep == 3)
            {
                string filename = "";
                ControlType sheet = ucExcelSelectSheets.ddlDataSouce.SelectedItem as ControlType;
                if (sheet != null)
                {
                    filename = sheet.Display;
                }
                btnPreview.Visibility = System.Windows.Visibility.Collapsed;
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Collapsed;
                uFieldMapping.Visibility = System.Windows.Visibility.Collapsed;
                ucValidateData.Visibility = System.Windows.Visibility.Visible;
                if (source.Value == "Microsoft Excel")
                {
                    bool isRowFirst = ucDataSoure.chkFirstRow.IsChecked.Value;
                    ControlType xcttype = ucDataSoure.ddlExcelVersion.SelectedItem as ControlType;
                    string ext = xcttype.Value;
                    GetExcelData(currFieldName, ext, filename, isRowFirst);
                    
                    currentStep = 4;
                    return;
                }
                else if (source.Value == "Microsoft Access")
                {
                    string tbl = ucDataSoure.txtAccessPath.Text;
                    string table = ucExcelSelectSheets.txttableName.Text;
                    GetAccessData(tbl, tbl, table, false);
                    currentStep = 4;
                    return;
                }
            }







        }


        private void GetSheetNames(string filePath, string ext, string sheetName, bool isFirstRowAsColumnNames)
        {

            var ws = WCF.GetService();
            ws.GetExcelColumnNamesCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetExcelColumnNamesCompletedEventArgs>(ws_GetColumnDataSetDataCompleted);
            ws.GetExcelColumnNamesAsync(filePath, ext, sheetName, isFirstRowAsColumnNames);
        }

        //
        void ws_GetColumnDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetExcelColumnNamesCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                ControlType sheet = ucExcelSelectSheets.ddlDataSouce.SelectedItem as ControlType;
                if (sheet != null)
                {
                    curSheetIndex = sheet.index;
                }
                int i = 1;
                ImportWizard.sheetColunList = new ObservableCollection<SheetColumn>();
                foreach (var item in e.Result)
                {

                    string[] strColumns = item.Split(';');
                    string header = strColumns[0].ToString();
                    string actualColumnName = strColumns[1].ToString();


                    ImportWizard.sheetColunList.Add(new SheetColumn { Index = i, SheetIndex = curSheetIndex, Name = header, AcualName = actualColumnName });
                    i += 1;

                }

                ((GridViewComboBoxColumn)this.uFieldMapping.gdField.Columns["Value"]).ItemsSource = ImportWizard.sheetColunList;//.Where(s => s.SheetIndex == sheet.index);

            }

        }



        private void GetAccessColumnNames(string source, string db, string sheetName)
        {

            var ws = WCF.GetService();
            ws.GetAccessColumnNamesCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetAccessColumnNamesCompletedEventArgs>(ws_GetAccessColumnNamesCompleted);
            ws.GetAccessColumnNamesAsync(source, db, sheetName, authentication);
        }

        //
        void ws_GetAccessColumnNamesCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetAccessColumnNamesCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                ControlType sheet = ucExcelSelectSheets.ddlDataSouce.SelectedItem as ControlType;
                if (sheet != null)
                {
                    curSheetIndex = sheet.index;
                }
                int i = 1;
                ImportWizard.sheetColunList = new ObservableCollection<SheetColumn>();
                foreach (var item in e.Result)
                {

                    string[] strColumns = item.Split(';');
                    string header = strColumns[0].ToString();
                    string actualColumnName = strColumns[1].ToString();


                    ImportWizard.sheetColunList.Add(new SheetColumn { Index = i, SheetIndex = curSheetIndex, Name = header, AcualName = actualColumnName });
                    i += 1;

                }

                ((GridViewComboBoxColumn)this.uFieldMapping.gdField.Columns["Value"]).ItemsSource = ImportWizard.sheetColunList;//.Where(s => s.SheetIndex == sheet.index);

            }

        }

        private void GetExcelData(string filePath, string ext, string sheetName, bool isFirstRowAsColumnNames)
        {
            ucValidateData.gdData.IsBusy = true;
            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.FieldMapped> fields = new ObservableCollection<EOInterface.Middleware.DataTableService.FieldMapped>();
            foreach (var item in SourceFieldList2)
            {
                string AcualName = "";
                var col = ImportWizard.sheetColunList.Where(c => c.Name == item.Value).FirstOrDefault();
                if (col != null)
                {
                    AcualName = col.AcualName;
                }

                fields.Add(new BMA.EOInterface.Middleware.DataTableService.FieldMapped { FieldID = item.index, DataType = item.DataType, DestinationField = item.Value, SourceField = item.Display, ActualColumnName = AcualName });
            }
            var ws = WCF.GetService();
            ws.GetExcelDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetExcelDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted);
            ws.GetExcelDataAsync(filePath, ext, sheetName, isFirstRowAsColumnNames, fields, ViewID, ServerID, 0, false);
        }

        //
        void ws_GetColumnDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetExcelDataCompletedEventArgs e)
        {
            bool import = false;
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                //  _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                List<ControlType> fields = new List<ControlType>();

                bool isRowFirst = ucDataSoure.chkFirstRow.IsChecked.Value;
                foreach (var item in uFieldMapping.gdField.Items)
                {
                    ControlType fld = item as ControlType;
                    fields.Add(fld);
                }
                ucValidateData.gdData.Columns.Clear();

                List<DataObject> dataList = new List<DataObject>();
                foreach (var item in list)
                {
                    DataObject data = item as DataObject;

                    dataList.Add(data);
                }

                if (dataList.Count > 0)
                {
                    import = false;
                    if (isRowFirst)
                    {
                        if (dataList.Count == 1)
                        {
                            import = true;
                        }

                    }

                    var obj = dataList.FirstOrDefault();

                    dataList.Remove(obj);

                    Type t = obj.GetType();
                    PropertyInfo[] props = t.GetProperties();
                    int count = 0;
                    foreach (PropertyInfo prp in props)
                    {
                        count += 1;
                        try
                        {

                            var name = prp.Name;
                            if (name == "State")
                            {




                            }
                            else
                            {
                                ControlType readfield = new ControlType();
                                string value = obj.GetFieldValue(name).ToString();
                                if (isRowFirst)
                                {
                                    readfield = fields.Where(f => f.Value == value).FirstOrDefault();
                                }
                                else
                                {
                                    readfield = fields.Where(f => f.Value == name).FirstOrDefault();
                                }

                                GridViewDataColumn column = new GridViewDataColumn();
                                //  DateTimePickerColumn.
                                column.DataMemberBinding = new Binding(name);
                                column.Header = readfield.Display;
                                column.UniqueName = name;
                                column.IsFilteringDeferred = true;
                                switch (readfield.DataType)
                                {
                                    case "datetime":
                                        column.DataType = typeof(DateTime);
                                        column.DataFormatString = "{0:dd-MMM-yyyy}";
                                        break;

                                    case "int":
                                        column.DataType = typeof(int);
                                        break;
                                    case "bigint":
                                        column.DataType = typeof(long);
                                        break;
                                    default:
                                        column.DataType = typeof(string);
                                        break;
                                }

                                //  column.IsReadOnly = true;
                                if (prp.PropertyType.ToString() == "System.DateTime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                }

                                //if (field.DataType == "date")
                                //{
                                //    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                //}
                                if (count < 4)
                                {
                                    // column.IsGroupable = false;
                                }

                                /// if (!field.Hidden.Value)
                                this.ucValidateData.gdData.Columns.Add(column);
                            }
                        }

                        catch
                        {
                        }
                    }





                    ucValidateData.gdData.ItemsSource = dataList;
                    ucValidateData.gdData.AutoGenerateColumns = false;

                }

            }

            if (import)
            {
                btnImport.IsEnabled = true;
            }
            ucValidateData.gdData.IsBusy = false;
        }


        private void GetAccessData(string source, string db, string tableName, bool isFirstRowAsColumnNames)
        {
            ucValidateData.gdData.IsBusy = true;
            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.FieldMapped> fields = new ObservableCollection<EOInterface.Middleware.DataTableService.FieldMapped>();
            foreach (var item in SourceFieldList2)
            {
                string AcualName = "";
                var col = ImportWizard.sheetColunList.Where(c => c.Name == item.Value).FirstOrDefault();
                if (col != null)
                {
                    AcualName = col.AcualName;
                }

                fields.Add(new BMA.EOInterface.Middleware.DataTableService.FieldMapped { FieldID = item.index, DataType = item.DataType, DestinationField = item.Value, SourceField = item.Display, ActualColumnName = AcualName });
            }
            var ws = WCF.GetService();
            ws.GetAccessDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetAccessDataCompletedEventArgs>(ws_GetAccessDataCompleted);
            ws.GetAccessDataAsync(source, db, tableName, authentication, fields, ViewID, ServerID, 0, false);
        }

        //
        void ws_GetAccessDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetAccessDataCompletedEventArgs e)
        {
            bool import = false;
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                //  _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                List<ControlType> fields = new List<ControlType>();

                bool isRowFirst = ucDataSoure.chkFirstRow.IsChecked.Value;
                foreach (var item in uFieldMapping.gdField.Items)
                {
                    ControlType fld = item as ControlType;
                    fields.Add(fld);
                }
                ucValidateData.gdData.Columns.Clear();

                List<DataObject> dataList = new List<DataObject>();
                foreach (var item in list)
                {
                    DataObject data = item as DataObject;

                    dataList.Add(data);
                }

                if (dataList.Count > 0)
                {
                    import = false;
                    //if (isRowFirst)
                    //{
                    //    if (dataList.Count == 1)
                    //    {
                    //        import = true;
                    //    }

                    //}

                    var obj = dataList.FirstOrDefault();

                    // dataList.Remove(obj);

                    Type t = obj.GetType();
                    PropertyInfo[] props = t.GetProperties();
                    int count = 0;
                    foreach (PropertyInfo prp in props)
                    {
                        count += 1;
                        try
                        {

                            var name = prp.Name;
                            if (name == "State")
                            {




                            }
                            else
                            {
                                ControlType readfield = new ControlType();
                                string value = obj.GetFieldValue(name).ToString();
                                string strName = CharacterHandler.ReplaceHexCharacter(name);
                                readfield = fields.Where(f => f.Value == name).FirstOrDefault();


                                GridViewDataColumn column = new GridViewDataColumn();
                                //  DateTimePickerColumn.
                                column.DataMemberBinding = new Binding(name);
                                column.Header = readfield.Display;
                                column.UniqueName = name;
                                column.IsFilteringDeferred = true;
                                switch (readfield.DataType)
                                {
                                    case "datetime":
                                        column.DataType = typeof(DateTime);
                                        column.DataFormatString = "{0:dd-MMM-yyyy}";
                                        break;

                                    case "int":
                                        column.DataType = typeof(int);
                                        break;
                                    case "bigint":
                                        column.DataType = typeof(long);
                                        break;
                                    default:
                                        column.DataType = typeof(string);
                                        break;
                                }

                                //  column.IsReadOnly = true;
                                if (prp.PropertyType.ToString() == "System.DateTime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                }

                                //if (field.DataType == "date")
                                //{
                                //    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                //}
                                if (count < 4)
                                {
                                    // column.IsGroupable = false;
                                }

                                /// if (!field.Hidden.Value)
                                this.ucValidateData.gdData.Columns.Add(column);
                            }
                        }

                        catch
                        {
                        }
                    }





                    ucValidateData.gdData.ItemsSource = dataList;
                    ucValidateData.gdData.AutoGenerateColumns = false;

                }
                else
                {
                    import = true;
                    ucValidateData.gdData.AutoGenerateColumns = true; ;
                    ucValidateData.gdData.ItemsSource = list;
                    ucValidateData.gdData.AutoGenerateColumns = false;
                }

            }

            if (import)
            {
                btnImport.IsEnabled = true;
            }
            ucValidateData.gdData.IsBusy = false;
        }

        private void ValidateDirtyData(bool isFirstRowAsColumnNames)
        {
            ucValidateData.gdData.IsBusy = true;

            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.FieldMapped> fields = new ObservableCollection<EOInterface.Middleware.DataTableService.FieldMapped>();
            foreach (var item in SourceFieldList2)
            {
                string AcualName = "";
                var col = ImportWizard.sheetColunList.Where(c => c.Name == item.Value).FirstOrDefault();
                if (col != null)
                {
                    AcualName = col.AcualName;
                }

                fields.Add(new BMA.EOInterface.Middleware.DataTableService.FieldMapped { FieldID = item.index, DataType = item.DataType, DestinationField = item.Value, SourceField = item.Display, ActualColumnName = AcualName });
            }
            ObservableCollection<DataObject> dirtyData = new ObservableCollection<DataObject>();

            foreach (var item in ucValidateData.gdData.Items)
            {
                DataObject objItem = item as DataObject;
                dirtyData.Add(objItem);
            }
            var ws = WCF.GetService();
            ws.ValidateDirtyDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ValidateDirtyDataCompletedEventArgs>(ws_ValidateDirtyDataCompleted);
            ws.ValidateDirtyDataAsync(DynamicDataBuilder.GetUpdatedDataSet(dirtyData as IEnumerable, _tables), isFirstRowAsColumnNames, fields, ViewID, ServerID);
        }

        //
        void ws_ValidateDirtyDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.ValidateDirtyDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                //  _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                List<ControlType> fields = new List<ControlType>();

                bool isRowFirst = ucDataSoure.chkFirstRow.IsChecked.Value;
                //foreach (var item in uFieldMapping.gdField.Items)
                //{
                //    ControlType fld = item as ControlType;
                //    fields.Add(fld);
                //}
                //ucValidateData.gdData.Columns.Clear();

                List<DataObject> dataList = new List<DataObject>();
                foreach (var item in list)
                {
                    DataObject data = item as DataObject;

                    dataList.Add(data);
                }






                ucValidateData.gdData.ItemsSource = dataList;
                ucValidateData.gdData.AutoGenerateColumns = false;
                //}

            }
            ucValidateData.gdData.IsBusy = false;
        }




        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            var ws = WCF.GetService();

            ws.RemoveTempFileAsync(currFieldName);
            this.Close();// = false;
        }




        private void txtGroupName_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableOrDisableOKButton(sender, e);

        }
        public void EnableOrDisableOKButton(object sender, RoutedEventArgs e)
        {

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            StepBack();
        }

        private void StepBack()
        {
            if (currentStep == 2)
            {
                ucDataSoure.Visibility = System.Windows.Visibility.Visible;
                btnBack.IsEnabled = false;
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Collapsed;

                uFieldMapping.Visibility = System.Windows.Visibility.Collapsed;
                ucValidateData.Visibility = System.Windows.Visibility.Collapsed;
                currentStep = 1;
            }
            if (currentStep == 3)
            {
                currentStep = 2;
                ucDataSoure.Visibility = System.Windows.Visibility.Collapsed;
                btnBack.IsEnabled = true;
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Visible;
                ucValidateData.Visibility = System.Windows.Visibility.Collapsed;
                uFieldMapping.Visibility = System.Windows.Visibility.Collapsed;
                btnPreview.Visibility = System.Windows.Visibility.Collapsed;

            }
            if (currentStep == 4)
            {
                currentStep = 3;
                ucDataSoure.Visibility = System.Windows.Visibility.Collapsed;
                btnBack.IsEnabled = true;
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Collapsed;
                ucValidateData.Visibility = System.Windows.Visibility.Collapsed;
                uFieldMapping.Visibility = System.Windows.Visibility.Visible;
                btnPreview.Visibility = System.Windows.Visibility.Visible;
            }
        }


        private void InsertExcelData(string filePath, string ext, string sheetName, bool isFirstRowAsColumnNames, bool isInsertOnly)
        {
            ucValidateData.gdData.IsBusy = true;
            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.FieldMapped> fields = new ObservableCollection<EOInterface.Middleware.DataTableService.FieldMapped>();
            foreach (var item in SourceFieldList2)
            {
                string AcualName = "";
                var col = ImportWizard.sheetColunList.Where(c => c.Name == item.Value).FirstOrDefault();
                if (col != null)
                {
                    AcualName = col.AcualName;
                }

                fields.Add(new BMA.EOInterface.Middleware.DataTableService.FieldMapped { FieldID = item.index, DataType = item.DataType, DestinationField = item.Value, SourceField = item.Display, ActualColumnName = AcualName });
            }
            var ws = WCF.GetService();
            ws.InsertExcelDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertExcelDataCompletedEventArgs>(ws_InsertexcelDataCompleted);
            ws.InsertExcelDataAsync(filePath, ext, sheetName, isFirstRowAsColumnNames, fields, ViewID, ServerID, CreateStoreProcID, isInsertOnly);
        }

        //
        void ws_InsertexcelDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.InsertExcelDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });

            else
            {
                currentStep = 4;
                ucDataSoure.Visibility = System.Windows.Visibility.Collapsed;
                btnBack.IsEnabled = false;
                btnImport.IsEnabled = false;
                OKButton.IsEnabled = false;
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Collapsed;
                ucValidateData.Visibility = System.Windows.Visibility.Collapsed;
                uFieldMapping.Visibility = System.Windows.Visibility.Collapsed;

                CancelButton.Content = "Close";
                lblFinish.Visibility = System.Windows.Visibility.Visible;
                lblResults.Visibility = System.Windows.Visibility.Visible;

                lblFinish.Text = "Import Completed ";


                //if (e.Result == -2627)
                //{

                //    lblResults.Text = "Error : Duplicate key fields";

                //}
                //else if (e.Result == -515)
                //{
                //    lblResults.Text = "Error : Cannot insert NULL value" ;

                //}
                //else if (e.Result < 0)
                //{
                //    lblResults.Text = " An Sql error had occured, Error: " + e.Result.ToString() ;

                //}
                //else
                //{
                string alertText = e.Result.ToString();
                lblResults.Text = alertText;

                // }

                // lblResults.Text = e.Result.ToString()+ " rows imported";
            }
        }
        private void GetAccessTableName(string path, string dbName)
        {

            var ws = WCF.GetService();
            ws.GetAccessTableNamesCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetAccessTableNamesCompletedEventArgs>(ws_GetAccessTablesCompleted);
            ws.GetAccessTableNamesAsync(path, dbName, authentication);
        }

        //
        void ws_GetAccessTablesCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetAccessTableNamesCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });

            else
            {
                int i = 1;
                ImportWizard.sheetList = new ObservableCollection<ControlType>();
                foreach (var item in e.Result)
                {


                    ImportWizard.sheetList.Add(new ControlType { Value = i.ToString(), Display = item, index = i });
                    i += 1;

                }



            }

        }


        private void InsertAccessData(string source, string dbName, string tableName, bool isInsertOnly)
        {
            ucValidateData.gdData.IsBusy = true;
            ObservableCollection<BMA.EOInterface.Middleware.DataTableService.FieldMapped> fields = new ObservableCollection<EOInterface.Middleware.DataTableService.FieldMapped>();
            foreach (var item in SourceFieldList2)
            {
                string AcualName = "";
                var col = ImportWizard.sheetColunList.Where(c => c.Name == item.Value).FirstOrDefault();
                if (col != null)
                {
                    AcualName = col.AcualName;
                }

                fields.Add(new BMA.EOInterface.Middleware.DataTableService.FieldMapped { FieldID = item.index, DataType = item.DataType, DestinationField = item.Value, SourceField = item.Display, ActualColumnName = AcualName });
            }
            var ws = WCF.GetService();
            ws.InsertAccessDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertAccessDataCompletedEventArgs>(ws_InsertAccessDataCompleted);
            ws.InsertAccessDataAsync(source, dbName, tableName, authentication,fields, ViewID, ServerID, CreateStoreProcID, isInsertOnly);
        }

        //
        void ws_InsertAccessDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.InsertAccessDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });

            else
            {
                currentStep = 4;
                ucDataSoure.Visibility = System.Windows.Visibility.Collapsed;
                btnBack.IsEnabled = false;
                btnImport.IsEnabled = false;
                OKButton.IsEnabled = false;
                ucExcelSelectSheets.Visibility = System.Windows.Visibility.Collapsed;
                ucValidateData.Visibility = System.Windows.Visibility.Collapsed;
                uFieldMapping.Visibility = System.Windows.Visibility.Collapsed;

                CancelButton.Content = "Close";
                lblFinish.Visibility = System.Windows.Visibility.Visible;
                lblResults.Visibility = System.Windows.Visibility.Visible;

                lblFinish.Text = "Import Completed ";


                //if (e.Result == -2627)
                //{

                //    lblResults.Text = "Error : Duplicate key fields";

                //}
                //else if (e.Result == -515)
                //{
                //    lblResults.Text = "Error : Cannot insert NULL value" ;

                //}
                //else if (e.Result < 0)
                //{
                //    lblResults.Text = " An Sql error had occured, Error: " + e.Result.ToString() ;

                //}
                //else
                //{
                string alertText = e.Result.ToString();
                lblResults.Text = alertText;

                // }

                // lblResults.Text = e.Result.ToString()+ " rows imported";
            }
        }

        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            btnImport.IsEnabled = false;
            btnImport.Content = "Importing...";
            btnBack.IsEnabled = false;
            OKButton.IsEnabled = false;
           
            ControlType source = ucDataSoure.ddlDataSouce.SelectedItem as ControlType;
          
            string filename = "";
            ControlType sheet = ucExcelSelectSheets.ddlDataSouce.SelectedItem as ControlType;
            if (sheet != null)
            {
                filename = sheet.Display;
            }
            if (source.Value == "Microsoft Excel")
            {
                bool isRowFirst = ucDataSoure.chkFirstRow.IsChecked.Value;

                ControlType xstn = ucDataSoure.ddlExcelVersion.SelectedItem as ControlType;
                InsertExcelData(currFieldName, xstn.Value, filename, isRowFirst, insertOnly);
            }
            else if (source.Value == "Microsoft Access")
            {
                string table = ucExcelSelectSheets.txttableName.Text;
                string path = ucDataSoure.txtAccessPath.Text;
                InsertAccessData(path, path, table, insertOnly);
            }
        }

        private void btnPreview_Click(object sender, RoutedEventArgs e)
        {
            ControlType source = ucDataSoure.ddlDataSouce.SelectedItem as ControlType;
            string filename = "";
            ControlType sheet = ucExcelSelectSheets.ddlDataSouce.SelectedItem as ControlType;
            if (sheet != null)
            {
                filename = sheet.Display;
            }

            List<ControlType> fields = new List<ControlType>();
            foreach (var item in uFieldMapping.gdField.Items)
            {
                ControlType fld = item as ControlType;
                fields.Add(fld);
            }
            if (source.Value == "Microsoft Excel")
            {
                bool isRowFirst = ucDataSoure.chkFirstRow.IsChecked.Value;
                ControlType xcttype = ucDataSoure.ddlExcelVersion.SelectedItem as ControlType;
                string ext = xcttype.Value;
                PreviewData view = new PreviewData(currFieldName, ext, filename, isRowFirst, SourceFieldList2.ToList(), fields, source.Value);
                //view.Closed += view_Closed;// +=new RadEventHandler(view_Closed);
                //view.WindowStartupLocation
                // view.Owner = 

                view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
                view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
                view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                view.ShowDialog();
            }

            if (source.Value == "Microsoft Access")
            {

                string path = ucDataSoure.txtAccessPath.Text;
                string tablename = ucExcelSelectSheets.txttableName.Text;
                PreviewData view = new PreviewData(path, path, tablename, false, SourceFieldList2.ToList(), fields, source.Value);
                //view.Closed += view_Closed;// +=new RadEventHandler(view_Closed);
                //view.WindowStartupLocation
                // view.Owner = 

                view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
                view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
                view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                view.ShowDialog();
            }
        }


        public static void SendNotify(bool isReady)
        {
            if (isReady)
            {
                //OKButton.IsEnabled = true;
            }
            else
            {
               // OKButton.IsEnabled = false;
            }

        }
        public delegate void SendNotifyDelegate(bool isReady);
    }
}

