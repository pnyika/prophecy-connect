﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace BMA.MiddlewareApp.Controls
{
    public abstract class Dialog
    {
        private Point location;
        private bool isShowing;
        private Popup popup;
        private Grid grid;
        private Canvas canvas;
        private FrameworkElement content;



        public void Show(Point _location)
        {
            if (isShowing)
                throw new InvalidOperationException();
            isShowing =  true;
            location = _location;
            EnsurePopup();
            popup.IsOpen = true;
        }

        public void Close()
        {
            try
            {

                isShowing = false;
                if (popup != null)
                {
                    popup.IsOpen = false;
                }
                else
                {
                    popup.IsOpen = false;
                }
            }
            catch
            {

            }
        }


        protected abstract FrameworkElement GetContent();

        protected virtual void OnClickOutside() { }

        private void EnsurePopup()
        {
            if (popup != null)
                return;
            popup = new Popup();
            grid = new Grid();

            popup.Child = grid;

            canvas = new Canvas();

            canvas.MouseLeftButtonDown += (sender, args) => { OnClickOutside(); };
            canvas.MouseRightButtonDown += (sender, args) => { args.Handled = true;  OnClickOutside(); };

            canvas.Background = new SolidColorBrush(Colors.Transparent);

            grid.Children.Add(canvas);

            content = GetContent();
            content.HorizontalAlignment = HorizontalAlignment.Left;
            content.VerticalAlignment = VerticalAlignment.Top;
            content.Margin = new Thickness(location.X, location.Y, 0, 0);

            grid.Children.Add(content);

            UpdateSize();


        }

        private void OnPluginSizeChanged(object sender, EventArgs e)
        {
            UpdateSize();
        }

        private void UpdateSize()
        {
            grid.Width = Application.Current.Host.Content.ActualWidth;
            grid.Height = Application.Current.Host.Content.ActualHeight;

            if (canvas != null)
            {
                canvas.Width = grid.Width;
                canvas.Height = grid.Height;
            }
        }

    }
}
