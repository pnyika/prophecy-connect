﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Markup;
using BMA.MiddlewareApp.Web;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.ComponentModel;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Persistence;
using Telerik.Windows.Persistence.Services;
using System.IO;
using System.Text;
using Telerik.Windows.Persistence.Storage;
using System.Windows.Navigation;
using Telerik.Windows;
using System.Xml.Linq;
using SilverlightMessageBox;
using BMA.MiddlewareApp.AppCode;
using System.Reflection;
using BMA.EOInterface.Middleware;
using System.Drawing;
using Telerik.Windows.Controls.Charting;
using AmCharts.Windows.QuickCharts;

  //using System.Windows.Input.ApplicationCommands;
namespace BMA.MiddlewareApp.Controls
{
    public partial class MTPGrids : UserControl
    {
        #region local variable
        EditorContext context = new EditorContext();
        //private List<GridViewRowInfo> modifiedRows = new List<GridViewRowInfo>();
        IEnumerable _lookup;
        ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> _tables;
        private string tableName;
        private int tableID;
        private string tableName2;
        private int tableID2;
        private string tableName3;
        private int tableID3;
        private string tableName4;
        private int tableID4;
        private int serverID;
        private string tableName5;
        private int tableID5;
        private string databaseName;
        private string connString;
        ObservableCollection<DataObject> newDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList = new List<GroupViewFilter>();

        ObservableCollection<DataObject> newDataObjectList2 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList2 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList2 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList2 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList2 = new List<GroupViewFilter>();


        ObservableCollection<DataObject> newDataObjectList4 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList4 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList4 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList4 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList4 = new List<GroupViewFilter>();

        ObservableCollection<DataObject> newDataObjectList3 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList3 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList3 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList3 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList3 = new List<GroupViewFilter>();
        List<GridRelationship> GridRelationshipList = new List<GridRelationship>();
        List<GridRelationship> GridRelationshipList3 = new List<GridRelationship>();

        ObservableCollection<DataObject> newDataObjectList5 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> editedDataObjectList5 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> deletedDataObjectList5 = new ObservableCollection<DataObject>();
        public ObservableCollection<UserGridConfig> settingObjectList5 = new ObservableCollection<UserGridConfig>();
        List<GroupViewFilter> GroupViewFilterList5 = new List<GroupViewFilter>();
        List<GridRelationship> GridRelationshipList5 = new List<GridRelationship>();
        // List<GridRelationship> GridRelationshipList5 = new List<GridRelationship>();

        RadGridViewSettings settings = null;
        RadGridViewSettings primarySettings = null;
        RadGridViewSettings settings2 = null;
        RadGridViewSettings settings4 = null;
        RadGridViewSettings primarySettings2 = null;
        RadGridViewSettings settings3 = null;
        RadGridViewSettings primarySettings3 = null;
        RadGridViewSettings settings5 = null;
        RadGridViewSettings primarySettings5 = null;
        RadGridViewSettings primarySettings4 = null;

        private Stream stream;
        private PersistenceManager manager = new PersistenceManager();
        private IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
        private bool useIsolatedStorage;
        private int userID;
        private int displayDefID;
        private string XMLString;
        List<ViewRelationship> relationshipList = new List<ViewRelationship>();
        ObservableCollection<ReadField> fieldList = new ObservableCollection<ReadField>();
        List<Table> tableList = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary;
        List<ViewRelationship> relationshipList2 = new List<ViewRelationship>();
        ObservableCollection<ReadField> fieldList2 = new ObservableCollection<ReadField>();
        ObservableCollection<ReadField> fieldList4 = new ObservableCollection<ReadField>();
        ObservableCollection<ReadField> fieldList5 = new ObservableCollection<ReadField>();
        List<Table> tableList2 = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary2;

        List<ViewRelationship> relationshipList3 = new List<ViewRelationship>();
        ObservableCollection<ReadField> fieldList3 = new ObservableCollection<ReadField>();
        List<Table> tableList3 = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary3;
        // List<ViewFieldSummary> fieldList5 = new List<ViewFieldSummary>();
        List<Table> tableList5 = new List<Table>();
        ObservableCollection<ViewFieldSummary> viewFieldSummary5;

        private int viewID;
        private int viewID2;
        private int viewID3;
        private int viewID4;
        private int viewID5;
        private bool singleTable = true;
        private bool isReadOnly = true;
        private bool singleTable2 = true;
        private bool readOnly2 = true;
        private bool singleTable3 = true;
        private bool readOnly3 = true;
        public static int DisplayID;
        public static long ModelID;
        public static string ModelName;
        private int fieldIndex = 0;
        private int indexMax = 0;
        private string dropDownName;
        private List<TempField> tempFieldList = new List<TempField>();
        // private string dropDownName4;
        //      private List<TempField> tempFieldList4 = new List<TempField>();
        List<DataObject> propertiesList = new List<DataObject>();
        List<string> columnNames = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList = new List<TempExtendedProperty>();



        private int pageNumber = 1;
        private int pageTotal = 1;
        public static int pageSize;
        string sqlText = "";
        string newSqlText = "";
        private bool canGenerateColumns = true;


        int grid1ID = 0;
        int grid2ID = 0;
        int grid3ID = 0;
        int grid4ID = 0;
        int grid5ID = 0;
        private int pageNumber2 = 1;
        private int pageTotal2 = 1;
        public static int pageSize2;
        string sqlText2 = "";
        string newSqlText2 = "";
        private bool canGenerateColumns2 = true;
        private int fieldIndex2 = 0;
        private int indexMax2 = 0;
        private string dropDownName2;
        private List<TempField> tempFieldList2 = new List<TempField>();
        List<DataObject> propertiesList2 = new List<DataObject>();
        List<string> columnNames2 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList2 = new List<TempExtendedProperty>();


        private int pageNumber4 = 1;
        private int pageTotal4 = 1;
        public static int pageSize4;
        string sqlText4 = "";
        string newSqlText4 = "";
        private bool canGenerateColumns4 = true;
        private int fieldIndex4 = 0;
        private int indexMax4 = 0;
        private string dropDownName4;
        private List<TempField> tempFieldList4 = new List<TempField>();
        List<DataObject> propertiesList4 = new List<DataObject>();
        List<string> columnNames4 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList4 = new List<TempExtendedProperty>();
        private bool useReadSP4 = false;
        private bool useCreateSP4 = false;
        private int CreateSPID4 = 0;
        private int ReadSPID4 = 0;
        private bool useDeleteSP4 = false;
        private int DeleteSPID4 = 0;
        private bool useUpdateSP4 = false;
        private int UpdateSPID4 = 0;




        private int pageNumber5 = 1;
        private int pageTotal5 = 1;
        public static int pageSize5;
        string sqlText5 = "";
        string newSqlText5 = "";
        private bool canGenerateColumns5 = true;
        private int fieldIndex5 = 0;
        private int indexMax5 = 0;
        private string dropDownName5;
        private List<TempField> tempFieldList5 = new List<TempField>();
        List<DataObject> propertiesList5 = new List<DataObject>();
        List<string> columnNames5 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList5 = new List<TempExtendedProperty>();
        private bool useReadSP5 = false;
        private bool useCreateSP5 = false;
        private int CreateSPID5 = 0;
        private int ReadSPID5 = 0;
        private bool useDeleteSP5 = false;
        private int DeleteSPID5 = 0;
        private bool useUpdateSP5 = false;
        private int UpdateSPID5 = 0;

        private int pageNumber3 = 1;
        private int pageTotal3 = 1;
        public static int pageSize3;
        string sqlText3 = "";
        string newSqlText3 = "";
        private bool canGenerateColumns3 = true;
        private int fieldIndex3 = 0;
        private int indexMax3 = 0;
        private string dropDownName3;
        private List<TempField> tempFieldList3 = new List<TempField>();
        List<DataObject> propertiesList3 = new List<DataObject>();
        List<string> columnNames3 = new List<string>();
        private List<TempExtendedProperty> TempExtendedPropertyList3 = new List<TempExtendedProperty>();
        private bool useReadSP3 = false;
        private bool useCreateSP3 = false;
        private int CreateSPID3 = 0;
        private int ReadSPID3 = 0;
        private bool useDeleteSP3 = false;
        private int DeleteSPID3 = 0;
        private bool useUpdateSP3 = false;
        private int UpdateSPID3 = 0;


        private bool useReadSP = false;
        private bool useCreateSP = false;
        private int CreateSPID = 0;
        private int ReadSPID = 0;
        private bool useDeleteSP = false;
        private int DeleteSPID = 0;
        private bool useUpdateSP = false;
        private int UpdateSPID = 0;


        private bool useReadSP2 = false;
        private bool useCreateSP2 = false;
        private int CreateSPID2 = 0;
        private int ReadSPID2 = 0;
        private bool useDeleteSP2 = false;
        private int DeleteSPID2 = 0;
        private bool useUpdateSP2 = false;
        private int UpdateSPID2 = 0;
        //private 

        #endregion
        public MTPGrids()
        {
            InitializeComponent();
            TemplateManager.LoadTemplate("DataGrid/Content.zip");
            LoadLayoutFromString();

            userID = MainPage.userID;
            displayDefID = DisplayID;
            newHeight = MainPage.gridHeight;
            // getTableData();
            settings = new RadGridViewSettings(this.radGridView1);
            primarySettings = new RadGridViewSettings(this.radGridView1);
            primarySettings.SaveState();
            settings2 = new RadGridViewSettings(this.radGridView2);
            primarySettings2 = new RadGridViewSettings(this.radGridView2);
            primarySettings2.SaveState();

            settings3 = new RadGridViewSettings(this.radGridView3);
            primarySettings3 = new RadGridViewSettings(this.radGridView3);
            primarySettings3.SaveState();

            this.radGridView1.Deleted += new EventHandler<GridViewDeletedEventArgs>(gridView_Deleted);

            // The events below notify when saving and loading have been completed
            btnButtons.SaveClick += new EventHandler(btnSave_Click);
            btnButtons.AddClick += new EventHandler(btnAddNew_Click);
            btnButtons.DeleteClick += new EventHandler(btnDelete_Click);
            btnButtons.CancelClick += new EventHandler(btnCancel_Click);
            btnButtons.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            btnButtons.SaveFilterClick += new EventHandler(btnSaveConfig_Click);
            btnButtons.ExportClick += new EventHandler(btnButtons_ExportClick);

            btnButtons.ImportClick += new EventHandler(btnButtons_ImportClick);
            btnButtons2.ImportClick += new EventHandler(btnButtons_ImportClick2);
            btnButtons3.ImportClick += new EventHandler(btnButtons_ImportClick3);
            btnButtons4.ImportClick += new EventHandler(btnButtons_ImportClick4);

            btnInactive.ExportClick += new EventHandler(btnButtons_ExportClick);
            btnInactive.SetDefaultClick += new EventHandler(btnSetDefault_Click);
            btnInactive.FilterClick += new EventHandler(btnSaveConfig_Click);
            btnInactive.Visibility = Visibility.Collapsed;

            btnButtons2.SaveClick += new EventHandler(btnSave_Click2);
            btnButtons2.AddClick += new EventHandler(btnAddNew_Click2);
            btnButtons2.DeleteClick += new EventHandler(btnDelete_Click2);
            btnButtons2.CancelClick += new EventHandler(btnCancel_Click2);
            btnButtons2.SetDefaultClick += new EventHandler(btnSetDefault_Click2);
            btnButtons2.SaveFilterClick += new EventHandler(btnSaveConfig_Click2);
            btnButtons2.ExportClick += new EventHandler(btnButtons_ExportClick2);


            btnInactive2.ExportClick += new EventHandler(btnButtons_ExportClick2);
            btnInactive2.SetDefaultClick += new EventHandler(btnSetDefault_Click2);
            btnInactive2.FilterClick += new EventHandler(btnSaveConfig_Click2);
            btnInactive2.Visibility = Visibility.Collapsed;


            btnButtons4.SaveClick += new EventHandler(btnSave_Click4);
            btnButtons4.AddClick += new EventHandler(btnAddNew_Click4);
            btnButtons4.DeleteClick += new EventHandler(btnDelete_Click4);
            btnButtons4.CancelClick += new EventHandler(btnCancel_Click4);
            btnButtons4.SetDefaultClick += new EventHandler(btnSetDefault_Click4);
            btnButtons4.SaveFilterClick += new EventHandler(btnSaveConfig_Click4);
            btnButtons4.ExportClick += new EventHandler(btnButtons_ExportClick4);


            btnInactive4.ExportClick += new EventHandler(btnButtons_ExportClick4);
            btnInactive4.SetDefaultClick += new EventHandler(btnSetDefault_Click4);
            btnInactive4.FilterClick += new EventHandler(btnSaveConfig_Click4);
            btnInactive4.Visibility = Visibility.Collapsed;


            btnButtons3.SaveClick += new EventHandler(btnSave_Click3);
            btnButtons3.AddClick += new EventHandler(btnAddNew_Click3);
            btnButtons3.DeleteClick += new EventHandler(btnDelete_Click3);
            btnButtons3.CancelClick += new EventHandler(btnCancel_Click3);
            btnButtons3.SetDefaultClick += new EventHandler(btnSetDefault_Click3);
            btnButtons3.SaveFilterClick += new EventHandler(btnSaveConfig_Click3);
            btnButtons3.ExportClick += new EventHandler(btnButtons_ExportClick3);


            btnInactive3.ExportClick += new EventHandler(btnButtons_ExportClick3);
            btnInactive3.SetDefaultClick += new EventHandler(btnSetDefault_Click3);
            btnInactive3.FilterClick += new EventHandler(btnSaveConfig_Click3);
            btnInactive3.Visibility = Visibility.Collapsed;


            RightClickMenu.Save += new EventHandler(RightClickMenu_Save);
            RightClickMenu.Cancel += new EventHandler(RightClickMenu_Cancel);
            RightClickMenu.Delete += new EventHandler(RightClickMenu_Delete);
            RightClickMenu.SaveFilter += new EventHandler(RightClickMenu_SaveFilter);
            RightClickMenu.SetDefault += new EventHandler(RightClickMenu_SetDefault);
            RightClickMenu.AddNew += new EventHandler(RightClickMenu_AddNew);
            RightClickMenu.Export += new EventHandler(RightClickMenu_Export);


          //  btnButtons6.SaveClick += new EventHandler(btnSave_Click4);
           // btnButtons4.AddClick += new EventHandler(btnAddNew_Click4);
          //  btnButtons4.DeleteClick += new EventHandler(btnDelete_Click4);
          //  btnButtons4.CancelClick += new EventHandler(btnCancel_Click4);
            btnFilter6.Click += btnSaveConfig_Click6 ;
            btnSetDefault6.Click +=btnSetDefault_Click6;
           // btnButtons4.ExportClick += new EventHandler(btnButtons_ExportClick4);

            btnFilter5.Click += btnSaveConfig_Click5;
            btnSetDefault5.Click += btnSetDefault_Click5; 
            btnExport5.Click += btnButtons_ExportClick5;

          
            stackPanel1.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonDown);
            //LayoutRoot.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
            radGridView1.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);

            this.radGridView1.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded);
            this.radGridView2.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded2);
            // this.radGridView3.RowLoaded += new EventHandler<RowLoadedEventArgs>(RadGridView1_RowLoaded3);
            ServiceProvider.RegisterPersistenceProvider<ICustomPropertyProvider>(typeof(RadDocking), new DockingCustomPropertyProvider());

            loadPageNumber();

            dataPager.Back_Click += new EventHandler(dataPager_Back_Click);
            dataPager.Last_Click += new EventHandler(dataPager_Last_Click);
            dataPager.Next_Click += new EventHandler(dataPager_Next_Click);
            dataPager.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged);
            dataPager.First_Click += new EventHandler(dataPager_First_Click);
            Paging();

            dataPager2.Back_Click += new EventHandler(dataPager_Back_Click2);
            dataPager2.Last_Click += new EventHandler(dataPager_Last_Click2);
            dataPager2.Next_Click += new EventHandler(dataPager_Next_Click2);
            dataPager2.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged2);
            dataPager2.First_Click += new EventHandler(dataPager_First_Click2);
            Paging2();

            dataPager3.Back_Click += new EventHandler(dataPager_Back_Click3);
            dataPager3.Last_Click += new EventHandler(dataPager_Last_Click3);
            dataPager3.Next_Click += new EventHandler(dataPager_Next_Click3);
            dataPager3.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged3);
            dataPager3.First_Click += new EventHandler(dataPager_First_Click3);
            Paging3();

            dataPager4.Back_Click += new EventHandler(dataPager_Back_Click4);
            dataPager4.Last_Click += new EventHandler(dataPager_Last_Click4);
            dataPager4.Next_Click += new EventHandler(dataPager_Next_Click4);
            dataPager4.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged4);
            dataPager4.First_Click += new EventHandler(dataPager_First_Click4);
            Paging4();

            dataPager5.Back_Click += new EventHandler(dataPager_Back_Click5);
            dataPager5.Last_Click += new EventHandler(dataPager_Last_Click5);
            dataPager5.Next_Click += new EventHandler(dataPager_Next_Click5);
            dataPager5.Number_TextChanged += new EventHandler(dataPager_Number_TextChanged5);
            dataPager5.First_Click += new EventHandler(dataPager_First_Click5);
            Paging5();
            radGridView1.SelectionChanged += radGridView1_SelectionChanged;
            // LoadConfigSetting();
            ///getTableData();
            ///
            GetViewsByDefinitionID();
            MainPage.StatusUpdated += new EventHandler(MainPage_StatusUpdated);
           this.topSpliter.LayoutUpdated += new EventHandler(slipt_LayoutUpdated);

            this.topSpliter.MouseLeftButtonUp += topSpliter_MouseLeftButtonUp;
            MainPage.cnUpdate = true;

            LoadGraphType();
        }

        void topSpliter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            double height = newHeight;//this.myGrid.Height;
            double datagridHeight = (height / 2) - 30;
            double h = topPane.Height - 40;
            topSpliter.Height = datagridHeight + 85;
            var transform = bottomGrid.TransformToVisual(Application.Current.RootVisual);
            var offset = transform.Transform(new Point(0, 0));
            double height2 = newHeight;//this.myGrid.Height;
            double newH = offset.Y;
            double topHeight = offset.Y - 40;

            //  RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = offset.X.ToString() + " " + offset.Y.ToString() });

            this.radGridView1.Height = newH - 125;
            //double ht = topSpliter.ActualHeight;
            //double testh =firstpane.Height;
            this.radGridView2.Height = newH - 140;
            this.radGridView3.Height = newH - 140;
            this.radGridView4.Height = newH - 140;
            this.radGridView5.Height = height2 - newH;
            graphHeight = height - newH - 10;
            gridMTP.Height = height2 - newH - 60;
            gPanel.Height = height2 - newH; ;
            myDispatcherTimer.Stop();
            cnLayoutUpdate = true;
            isUpdating = false;
        }




        private void LoadLayoutFromString()
        {

            XDocument categoriesXML = XDocument.Load("MTPGd.xml");
            string xml = categoriesXML.ToString();
            using (Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                stream.Seek(0, SeekOrigin.Begin);
                this.Docking.LoadLayout(stream);
            }
        }

        private void LoadGraphType()
        {
            List<DropDownStringData> Typelist = new List<DropDownStringData>();
            Typelist.Add(new DropDownStringData { Display = "Line", Value = "Line" });
            Typelist.Add(new DropDownStringData { Display = "Bar", Value = "Bar" });
            Typelist.Add(new DropDownStringData { Display = "Area", Value = "Area" });
            ((GridViewComboBoxColumn)this.gridMTP.Columns["GraphType"]).ItemsSource = Typelist;


        }
        private void loadColors()
        {
            //Colors.


        }
        void slipt_LayoutUpdated(object sender, EventArgs e)
        {
            if (cnLayoutUpdate)
            {
                StartTimerSlipter2();
            }
        }


        System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        bool cnLayoutUpdate = true;
        public void StartTimer()
        {
            isUpdating = true;
            cnLayoutUpdate = false;
           // myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 4, 0); // 15seconds 

            myDispatcherTimer.Tick += new EventHandler(Each_Tick);

            myDispatcherTimer.Start();


        }
        double graphHeight = 250;
        public void Each_Tick(object sender, EventArgs e)
        {
            try
            {
                double height = newHeight;//this.myGrid.Height;
                double datagridHeight = (height / 2) - 30;
                double h = topPane.Height - 40;
                topSpliter.Height = datagridHeight + 85;
                var transform = bottomGrid.TransformToVisual(Application.Current.RootVisual);
                var offset = transform.Transform(new Point(0, 0));
                double height2 = newHeight;//this.myGrid.Height;
                double newH = offset.Y;
                double topHeight = offset.Y - 40;

                //  RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = offset.X.ToString() + " " + offset.Y.ToString() });

                this.radGridView1.Height = newH - 125;
                //double ht = topSpliter.ActualHeight;
                //double testh =firstpane.Height;
                this.radGridView2.Height = newH - 140;
                this.radGridView3.Height = newH - 140;
                this.radGridView4.Height = newH - 140;
                this.radGridView5.Height = height2 - newH;
                graphHeight = height - newH - 10;
                gridMTP.Height = height2 - newH-60;
                gPanel.Height = height2 - newH; ;
                myDispatcherTimer.Stop();
                cnLayoutUpdate = true;
                isUpdating = false;
            }
            catch { }
            isUpdating = false;
            cnLayoutUpdate = true;
        }
        public void Each_Tick2(object sender, EventArgs e)
        {
            try
            {


               var tt = innerGrid1.ActualHeight;
               var tt2 = innerGrid1.Height;

               var transform = bottomGrid.TransformToVisual(Application.Current.RootVisual);
                var offset = transform.Transform(new Point(0, 0));
                   double height = newHeight;//this.myGrid.Height;
                   double newH =   offset.Y;
                   double topHeight = offset.Y-40;             


              //  RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = offset.X.ToString() + " " + offset.Y.ToString() });
               
                this.radGridView1.Height = newH - 125;
                //double ht = topSpliter.ActualHeight;
                //double testh =firstpane.Height;
                this.radGridView2.Height = newH - 140;
                this.radGridView3.Height = newH - 140;
                this.radGridView4.Height = newH - 140;
                this.radGridView5.Height = height - newH ;
                gridMTP.Height = height - newH-60;
                gPanel.Height = height - newH; ;
                graphHeight = height - newH - 10;
                myDispatcherTimer.Stop();
                cnLayoutUpdate = true;
                isUpdating = false;
            }
            catch { }
            cnLayoutUpdate = true;
            isUpdating = false;
        }


        private void layoutUpdate()
        {
            try
            {


                var tt = innerGrid1.ActualHeight;
                var tt2 = innerGrid1.Height;

                var transform = bottomGrid.TransformToVisual(Application.Current.RootVisual);
                var offset = transform.Transform(new Point(0, 0));
                double height = newHeight;//this.myGrid.Height;
                double newH = offset.Y;
                double topHeight = offset.Y - 40;


                //  RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = offset.X.ToString() + " " + offset.Y.ToString() });

                this.radGridView1.Height = newH - 125;
                //double ht = topSpliter.ActualHeight;
                //double testh =firstpane.Height;
                this.radGridView2.Height = newH - 140;
                this.radGridView3.Height = newH - 140;
                this.radGridView4.Height = newH - 140;
                this.radGridView5.Height = height - newH;
                gridMTP.Height = height - newH - 60;
                gPanel.Height = height - newH; ;
                graphHeight = height - newH - 10;
                myDispatcherTimer.Stop();
                cnLayoutUpdate = true;
                isUpdating = false;
            }
            catch { }
        }
        public void StartTimerSlipter()
        {

            cnLayoutUpdate = false;
            myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 4, 0); // 15seconds 

            myDispatcherTimer.Tick += new EventHandler(Each_TickSlipter);

            myDispatcherTimer.Start();


        }
        public void StartTimerSlipter2()
        {

            cnLayoutUpdate = false;
            myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 4, 0); // 15seconds 

            myDispatcherTimer.Tick += new EventHandler(Each_Tick2);

            myDispatcherTimer.Start();


        }

        public void Each_TickSlipter(object sender, EventArgs e)
        {
            try
            {
                double hgt = newHeight;//this.myGrid.Height;
                double height = topSpliter.ActualHeight;
                double datagridHeight = (height) - 30;
                //double h = topPane.Height - 40;
                // topSpliter.Height = datagridHeight + 65;
                this.radGridView1.Height = datagridHeight - 30;
                // double ht = topSpliter.ActualHeight;
                //double testh =firstpane.Height;
                this.radGridView2.Height = datagridHeight - 10;
                this.radGridView3.Height = hgt - datagridHeight - 30;
                myDispatcherTimer.Stop();
                cnLayoutUpdate = true;
            }
            catch { }
        }

        bool isUpdating = false;
        double newHeight;
        void  MainPage_StatusUpdated(object sender, EventArgs e)
        {
            try
            {
                if (!isUpdating)
                {
                    isUpdating = true;
                    Grid grid = sender as Grid;
                    if (grid != null)
                    {
                        this.myGrid.Height = grid.ActualHeight - 25;
                        Docking.Height = grid.ActualHeight - 30;
                        newHeight = grid.ActualHeight - 25;
                        StartTimer();
                    }
                }

            }
            catch
            {
            }
            isUpdating = false;
        }

        #region Grid One
        #region Paging


        private void loadPageNumber()
        {
            List<PageNumber> pageList = new List<PageNumber>();

            pageList.Add(new PageNumber { Size = 10, Display = "10" });
            pageList.Add(new PageNumber { Size = 25, Display = "25" });
            pageList.Add(new PageNumber { Size = 50, Display = "50" });
            pageList.Add(new PageNumber { Size = 75, Display = "75" });
            pageList.Add(new PageNumber { Size = 100, Display = "100" });
            pageList.Add(new PageNumber { Size = 150, Display = "150" });
            pageList.Add(new PageNumber { Size = 250, Display = "250" });
            pageList.Add(new PageNumber { Size = 500, Display = "500" });
            pageList.Add(new PageNumber { Size = 750, Display = "750" });
            pageList.Add(new PageNumber { Size = 1000, Display = "1000" });
            pageList.Add(new PageNumber { Size = 100000000, Display = "All" });
            ddlPageSize.DisplayMemberPath = "Display";
            ddlPageSize.SelectedValuePath = "Size";
            ddlPageSize.ItemsSource = pageList;
            ddlPageSize.SelectedValue = pageSize;

            ddlPageSize2.DisplayMemberPath = "Display";
            ddlPageSize2.SelectedValuePath = "Size";
            ddlPageSize2.ItemsSource = pageList;
            ddlPageSize2.SelectedValue = pageSize2;

            ddlPageSize3.DisplayMemberPath = "Display";
            ddlPageSize3.SelectedValuePath = "Size";
            ddlPageSize3.ItemsSource = pageList;
            ddlPageSize3.SelectedValue = pageSize3;


            ddlPageSize4.DisplayMemberPath = "Display";
            ddlPageSize4.SelectedValuePath = "Size";
            ddlPageSize4.ItemsSource = pageList;
            ddlPageSize4.SelectedValue = pageSize4;

            List<PageNumber> pageList5 = new List<PageNumber>();

            pageList5.Add(new PageNumber { Size = 100000000, Display = "All" });
            ddlPageSize5.DisplayMemberPath = "Display";
            ddlPageSize5.SelectedValuePath = "Size";
            ddlPageSize5.ItemsSource = pageList5;
            ddlPageSize5.SelectedValue = 100000000;

        }

        void dataPager_First_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = 1;
            ReloadPage(newPageNumber);
        }

        private void Paging()
        {
            dataPager.txtNumber.Text = pageNumber.ToString();
            dataPager.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox = true;
        void dataPager_Number_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox)
                {
                    int newPageNumber = Convert.ToInt32(dataPager.txtNumber.Text);
                    ReloadPage(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox = true;
        }
        private void ReloadPage(int newPageNumber)
        {
            if ((newDataObjectList.Count > 0) || (editedDataObjectList.Count > 0) || (deletedDataObjectList.Count > 0))
            {
                curPage = newPageNumber;

                string confirmText = "This page contains some changes, Do you want to save the changes?";
                RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed));

                

               
            }
            else
            {

                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    PullData();
                }
            }




        }
        int curPage = 1;

        private void OnConfirmClosed(object sender, WindowClosedEventArgs e)
        {
            int newPageNumber = curPage;
            if (e.DialogResult == true)
            {
                SaveChanges();

                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();

                    //radGridView1.IsBusy = false;
                }
            }
            else
            {

                newDataObjectList = new ObservableCollection<DataObject>();
                editedDataObjectList = new ObservableCollection<DataObject>();
                deletedDataObjectList = new ObservableCollection<DataObject>();
                if ((newPageNumber > pageTotal) || (newPageNumber < 1))
                {
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    isTextBox = false;
                }
                else
                {
                    pageNumber = newPageNumber;
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    /// GetData(newSqlText, pageNumber, pageSize, tableName);
                    //radGridView1.IsBusy = false;
                    PullData();
                }
            }
        }

        void dataPager_Next_Click(object sender, EventArgs e)
        {
            if (pageNumber < pageSize)
            {
                isTextBox = false;
                int newPageNumber = pageNumber + 1;
                ReloadPage(newPageNumber);
            }

        }

        void dataPager_Last_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageTotal;
            ReloadPage(newPageNumber);
        }

        void dataPager_Back_Click(object sender, EventArgs e)
        {
            isTextBox = false;
            int newPageNumber = pageNumber - 1;
            ReloadPage(newPageNumber);

        }

        #endregion



        #region Events

        void LayoutRoot_LayoutUpdated(object sender, EventArgs e)
        {
            //  radGridView1.MaxHeight = this.stack1.RenderSize.Height;
        }


        private void btnCascade_Click(object sender, RoutedEventArgs e)
        {

            PullData();



        }

        void PullData()
        {

            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID), callStoredProcedureNameLoadOperation, null);

        }


        void ReCountPage()
        {

            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID), callProcRowTotalLoadOperation, null);

        }





        void CallbackSettings(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache = xml;
                settings.LoadState(xml);
            }

            PullData();
        }
        void RightClickMenu_Export(object sender, EventArgs e)
        {
            ExcelExport();
        }

        void RightClickMenu_AddNew(object sender, EventArgs e)
        {
            if (singleTable)
            {
                this.radGridView1.Items.AddNew();
            }
        }

        void RightClickMenu_SetDefault(object sender, EventArgs e)
        {
            SetDefault();
        }

        void RightClickMenu_SaveFilter(object sender, EventArgs e)
        {
            SaveConfig();
        }

        void RightClickMenu_Delete(object sender, EventArgs e)
        {
            if (singleTable)
            {
                DeleteRows();
            }
        }

        void RightClickMenu_Cancel(object sender, EventArgs e)
        {
            Cancel();
        }



        void RightClickMenu_Save(object sender, EventArgs e)
        {

            SaveChanges();
        }

        public static void color_TestClick()
        {
            MessageBox.Show("Red");
        }

        void stack1_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        void stack1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            RightClickContentMenu contextMenu = new RightClickContentMenu();
            //  contextMenu.Show(e.GetPosition(LayoutRoot));
        }


        void dataGridRightClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("right");
        }

        protected void btnButtons_ExportClick(object sender, EventArgs e)
        {

            ExcelExport();

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }


        private void AddNewItem()
        {
            var item = this.radGridView1.Items.AddNew();

            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);

                    }
                }

                catch { }
            }
            try
            {
                DataObject newDataObject = item as DataObject;

                object id = ModelID as object;
                newDataObject.SetFieldValue("Model_ID", ModelID);

                foreach (var field in fieldList)
                {
                    if ((field.DefaultValue != null) || (field.DefaultValue != ""))
                    {

                        object obj = field.DefaultValue as object;
                        newDataObject.SetFieldValue(field.FieldName, obj);
                    }

                }
            }
            catch { }
        }


        private void btnSave_Click(object sender, EventArgs e)
        { //perform

            SaveChanges();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            editedDataObjectList = new ObservableCollection<DataObject>();
            newDataObjectList = new ObservableCollection<DataObject>();
            deletedDataObjectList = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteRows();

        }



        private void btnSaveConfig_Click(object sender, EventArgs e)
        {

            SaveConfig();

        }


        //DeleteConfig


        void ChildWin_Closed(object sender, EventArgs e)
        {
            LoadConfigSetting();
        }

        private void btnSetDefault_Click(object sender, EventArgs e)
        {
            SetDefault();

        }


        bool isClearFilter = false;
        private void ddlConfig_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;
                else
                {
                    if (config.ID > 0)
                    {
                        isClearFilter = false;
                        GetDefaultSetting(config.Setting);


                    }
                    else
                    {
                        isClearFilter = true;
                        ClearFilters();

                        settings = new RadGridViewSettings(this.radGridView1);

                        settings.LoadState(primaryXml);

                    }
                }
            }
            catch
            {
            }

        }

        #endregion



        #region Call Ria Services Method

        private void LoadConfigSetting()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting, null);
        }
        private void getTableData()
        {

            App app = (App)Application.Current;
            LoadOperation<Table> loadOp = context.Load(context.GetTablesByDisplayDefIDQuery(DisplayID), CallbackTable, null);
        }

        private void CallbackTable(LoadOperation<Table> loadOp)
        {
            Table table = loadOp.Entities.FirstOrDefault();

            if (table != null)
            {
                tableName = table.TableName;
                tableID = table.ID;
                databaseName = table.DBName;
                getServerConnections(tableID);


            }
        }


        private void getServerConnections(int tableID)
        {

            App app = (App)Application.Current;
            LoadOperation<Server> loadOp = context.Load(context.GetServersByTableIDQuery(tableID), CallbackServer, null);
        }

        private void CallbackServer(LoadOperation<Server> loadOp)
        {
            Server server = loadOp.Entities.FirstOrDefault();

            if (server != null)
            {
                connString = server.ConnectionString;

                // LoadOperation<View> loadOperation = context.Load(context.GetViewsByDisplayDefinitionIDQuery(displayDefID), CallbackView, null);
                LoadOperation<GridDefinition> loadOperation = context.Load(context.GetGridDefinitionsByDisplayDefinitionIDQuery(displayDefID), CallbackGridDefinition, null);

            }
        }


        private void GetViewsByDefinitionID()
        {
            //LoadOperation<View> loadOperation = context.Load(context.GetViewsByDisplayDefinitionIDQuery(displayDefID), CallbackView, null);
            LoadOperation<GridDefinition> loadOperation = context.Load(context.GetGridDefinitionsByDisplayDefinitionIDQuery(displayDefID), CallbackGridDefinition, null);
        }

        private void CallbackGridDefinition(LoadOperation<GridDefinition> loadOperation)
        {
            GridDefinition grid = loadOperation.Entities.Where(g => g.GridNumber == 1).FirstOrDefault();
            if (grid != null)
            {

                // bottomPane.Title = ModelName + " Facility";
                View view = grid.View;
                viewID = view.ID;
                singleTable = view.SingleTable;
                isReadOnly = view.ReadOnly;
                topPane.Title = ModelName + " : " + view.DisplayName;
                if (view.UseStoredProcedure.Value)
                {

                    if (view.ReadSP_ID != null)
                    {
                        useReadSP = true;
                        ReadSPID = view.ReadSP_ID.Value;

                        GetSPReadFields(viewID);


                        ///  
                    }

                    if (view.CreateSP_ID != null)
                    {
                        CreateSPID = view.CreateSP_ID.Value;
                        useCreateSP = true;
                    }

                    if (view.UpdateSP_ID != null)
                    {
                        UpdateSPID = view.UpdateSP_ID.Value;
                        useUpdateSP = true;
                    }


                    if (view.DeleteSP_ID != null)
                    {
                        DeleteSPID = view.DeleteSP_ID.Value;
                        useDeleteSP = true;

                    }

                }

                GridDefinition grid2 = loadOperation.Entities.Where(g => g.GridNumber == 2).FirstOrDefault();
                //GetTables();

                if (grid2 != null)
                {
                    View view2 = grid2.View;
                    viewID2 = view2.ID;
                    singleTable2 = view2.SingleTable;
                    readOnly2 = view2.ReadOnly;
                    pane2.Title = ModelName + " : " + view2.DisplayName;
                    if (view2.UseStoredProcedure.Value)
                    {


                        if (view2.ReadSP_ID != null)
                        {
                            useReadSP2 = true;
                            ReadSPID2 = view2.ReadSP_ID.Value;

                            GetSPReadFields2(viewID2);
                            ///  
                        }

                        if (view2.CreateSP_ID != null)
                        {
                            CreateSPID2 = view2.CreateSP_ID.Value;
                            useCreateSP2 = true;
                        }

                        if (view2.UpdateSP_ID != null)
                        {
                            UpdateSPID2 = view2.UpdateSP_ID.Value;
                            useUpdateSP2 = true;
                        }

                        if (view2.DeleteSP_ID != null)
                        {
                            DeleteSPID2 = view2.DeleteSP_ID.Value;
                            useDeleteSP2 = true;
                        }

                    }
                }


                GridDefinition grid4 = loadOperation.Entities.Where(g => g.GridNumber == 4).FirstOrDefault();
                //GetTables();

                if (grid4 != null)
                {
                    View view4 = grid4.View;
                    viewID4 = view4.ID;
                    // singleTable4 = view2.SingleTable;
                    // readOnly4 = view2.ReadOnly;


                    paneUserInput.Title = ModelName + " : " + view4.DisplayName;
                    if (view4.SingleTable)
                    {
                        btnInactive4.Visibility = Visibility.Collapsed;
                        btnButtons4.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnInactive4.Visibility = Visibility.Visible;
                        btnButtons4.Visibility = Visibility.Collapsed;
                    }

                    if (view4.UseStoredProcedure.Value)
                    {


                        if (view4.ReadSP_ID != null)
                        {
                            useReadSP4 = true;
                            ReadSPID4 = view4.ReadSP_ID.Value;

                            GetSPReadFields4(viewID4);
                            ///  
                        }

                        if (view4.CreateSP_ID != null)
                        {
                            CreateSPID4 = view4.CreateSP_ID.Value;
                            useCreateSP4 = true;
                        }

                        if (view4.UpdateSP_ID != null)
                        {
                            UpdateSPID4 = view4.UpdateSP_ID.Value;
                            useUpdateSP4 = true;
                        }
                        if (view4.DeleteSP_ID != null)
                        {
                            DeleteSPID4 = view4.DeleteSP_ID.Value;
                            useDeleteSP4 = true;
                        }
                    }
                }
                GridDefinition grid3 = loadOperation.Entities.Where(g => g.GridNumber == 3).FirstOrDefault();
                //GetTables();

                if (grid3 != null)
                {
                    View view3 = grid3.View;
                    viewID3 = view3.ID;
                    // singleTable4 = view2.SingleTable;
                    // readOnly4 = view2.ReadOnly;
                    paneImport.Title = ModelName + " : " + view3.DisplayName;
                    if (view3.SingleTable)
                    {
                        btnInactive3.Visibility = Visibility.Collapsed;
                        btnButtons3.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnInactive3.Visibility = Visibility.Visible;
                        btnButtons3.Visibility = Visibility.Collapsed;
                    }

                    if (view3.UseStoredProcedure.Value)
                    {


                        if (view3.ReadSP_ID != null)
                        {
                            useReadSP3 = true;
                            ReadSPID3 = view3.ReadSP_ID.Value;

                            GetSPReadFields3(viewID3);
                            ///  
                        }

                        if (view3.CreateSP_ID != null)
                        {
                            CreateSPID3 = view3.CreateSP_ID.Value;
                            useCreateSP3 = true;
                        }

                        if (view3.UpdateSP_ID != null)
                        {
                            UpdateSPID3 = view3.UpdateSP_ID.Value;
                            useUpdateSP3 = true;
                        }
                        if (view3.DeleteSP_ID != null)
                        {
                            DeleteSPID3 = view3.DeleteSP_ID.Value;
                            useDeleteSP3 = true;
                        }
                    }
                }

                GridDefinition grid5 = loadOperation.Entities.Where(g => g.GridNumber == 5).FirstOrDefault();
                //GetTables();

                if (grid5 != null)
                {
                    View view5 = grid5.View;
                    viewID5 = view5.ID;
                    // singleTable4 = view2.SingleTable;
                    // readOnly4 = view2.ReadOnly;
                    bottomPane.Title = ModelName + " : " + view5.DisplayName;
                    bottomPane.Header = ModelName + " : " + view5.DisplayName;

                    graphPane.Title = ModelName + " : " + view5.DisplayName + " Graphs";
                    graphPane.Header = ModelName + " : " + view5.DisplayName + " Graphs";

                    //btnInactive5.Visibility = Visibility.Visible;
                    //btnButtons5.Visibility = Visibility.Collapsed;
                    //btnInactive5.btnAdd.Visibility = Visibility.Collapsed;
                    //btnInactive5.btnSave.Visibility = Visibility.Collapsed;
                    //btnInactive5.btnDelete.Visibility = Visibility.Collapsed;
                    //btnInactive5.btnCancel.Visibility = Visibility.Collapsed;




                    //btnButtons6.btnAdd.Visibility = Visibility.Collapsed;
                    //btnButtons6.btnSave.Visibility = Visibility.Collapsed;
                    //btnButtons6.btnDelete.Visibility = Visibility.Collapsed;
                    //btnButtons6.btnCancel.Visibility = Visibility.Collapsed;
                    //btnButtons6.btnExport.Visibility = Visibility.Collapsed;

                    if (view5.UseStoredProcedure.Value)
                    {


                        if (view5.ReadSP_ID != null)
                        {
                            useReadSP5 = true;
                            ReadSPID5 = view5.ReadSP_ID.Value;

                            //  GetSPReadFields3(viewID3);
                            ///  
                        }


                    }
                }



                LoadOperation<GridRelationship> loadOperation2 = context.Load(context.GetGridRelationshipQuery().Where(g => g.DisplayDefinition_ID == DisplayID), CallbackGridRelationshipSP, null);

            }


        }


        private void CallbackView(LoadOperation<View> loadOperation)
        {
            View view = loadOperation.Entities.FirstOrDefault();
            if (view != null)
            {

                // bottomPane.Title = ModelName + " Facility";
                viewID = view.ID;
                singleTable = view.SingleTable;
                isReadOnly = view.ReadOnly;
                topPane.Title = ModelName + " : " + view.DisplayName;
                if (view.UseStoredProcedure.Value)
                {


                    if (view.ReadSP_ID != null)
                    {
                        useReadSP = true;
                        ReadSPID = view.ReadSP_ID.Value;

                        GetSPReadFields(viewID);


                        ///  
                    }

                    if (view.CreateSP_ID != null)
                    {
                        CreateSPID = view.CreateSP_ID.Value;
                        useCreateSP = true;
                    }

                    if (view.UpdateSP_ID != null)
                    {
                        UpdateSPID = view.UpdateSP_ID.Value;
                        useUpdateSP = true;
                    }


                    if (view.DeleteSP_ID != null)
                    {
                        DeleteSPID = view.DeleteSP_ID.Value;
                        useDeleteSP = true;

                    }

                }


                //GetTables();
                View view2 = loadOperation.Entities.Where(v => v.ID != view.ID).FirstOrDefault();
                if (view2 != null)
                {
                    viewID2 = view2.ID;
                    singleTable2 = view2.SingleTable;
                    readOnly2 = view2.ReadOnly;
                }

                pane2.Title = ModelName + " : " + view2.DisplayName;

                if (view2.UseStoredProcedure.Value)
                {


                    if (view2.ReadSP_ID != null)
                    {
                        useReadSP2 = true;
                        ReadSPID2 = view2.ReadSP_ID.Value;

                        GetSPReadFields2(viewID);
                        ///  
                    }

                    if (view2.CreateSP_ID != null)
                    {
                        CreateSPID2 = view2.CreateSP_ID.Value;
                        useCreateSP2 = true;
                    }

                    if (view2.UpdateSP_ID != null)
                    {
                        UpdateSPID2 = view2.UpdateSP_ID.Value;
                        useUpdateSP2 = true;
                    }

                }

                // View view3 = loadOperation.Entities.Where(v => v.ID != view.ID && v.ID != view2.ID).FirstOrDefault();
                // if (view3 != null)
                // {
                //     viewID3 = view3.ID;
                //     singleTable3 = view3.SingleTable;
                //     readOnly3 = view3.ReadOnly;
                // }
                LoadOperation<GridRelationship> loadOperation2 = context.Load(context.GetGridRelationshipQuery().Where(g => g.DisplayDefinition_ID == DisplayID), CallbackGridRelationshipSP, null);

            }


        }

        private void CallbackGridRelationshipSP(LoadOperation<GridRelationship> loadOperation)
        {
            gridRship = loadOperation.Entities.ToList();
        }


        private void GetSPReadFields(int viewID)
        {
            LoadOperation<ReadField> ReadFieldLoadOperation = context.Load(context.GetReadFieldsQuery().Where(s => s.ViewID == viewID), callReadFieldLoadOperation, null);
        }

        void callReadFieldLoadOperation(LoadOperation<ReadField> ReadFieldLoadOperation)
        {
            if (ReadFieldLoadOperation != null)
            {

                GenerateColumns(ReadFieldLoadOperation.Entities.ToList());

                fieldList = new ObservableCollection<ReadField>(ReadFieldLoadOperation.Entities);
                var field = fieldList.FirstOrDefault();

            }
        }


        private void GetSPReadFields2(int viewID)
        {
            LoadOperation<ReadField> ReadFieldLoadOperation = context.Load(context.GetReadFieldsQuery().Where(s => s.ViewID == viewID2), callReadFieldLoadOperation2, null);
        }

        void callReadFieldLoadOperation2(LoadOperation<ReadField> ReadFieldLoadOperation)
        {
            if (ReadFieldLoadOperation != null)
            {

                GenerateColumns2(ReadFieldLoadOperation.Entities.ToList());
                fieldList2 = new ObservableCollection<ReadField>(ReadFieldLoadOperation.Entities);
            }
        }


        private void GetSPReadFields4(int viewID)
        {
            LoadOperation<ReadField> ReadFieldLoadOperation = context.Load(context.GetReadFieldsQuery().Where(s => s.ViewID == viewID4), callReadFieldLoadOperation4, null);
        }

        void callReadFieldLoadOperation4(LoadOperation<ReadField> ReadFieldLoadOperation)
        {
            if (ReadFieldLoadOperation != null)
            {

                GenerateColumns4(ReadFieldLoadOperation.Entities.ToList());
                fieldList4 = new ObservableCollection<ReadField>(ReadFieldLoadOperation.Entities);
            }
        }


        private void GetSPReadFields3(int viewID)
        {
            LoadOperation<ReadField> ReadFieldLoadOperation = context.Load(context.GetReadFieldsQuery().Where(s => s.ViewID == viewID3), callReadFieldLoadOperation3, null);
        }

        void callReadFieldLoadOperation3(LoadOperation<ReadField> ReadFieldLoadOperation)
        {
            if (ReadFieldLoadOperation != null)
            {

                GenerateColumns3(ReadFieldLoadOperation.Entities.ToList());
                fieldList3 = new ObservableCollection<ReadField>(ReadFieldLoadOperation.Entities);
            }
        }


        void callStoredProcedureNameLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                connString = storedProc.Server.ConnectionString;
                serverID = storedProc.Server.ID;
                databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        parameters.Add(item.ParameterName + ";" + " Model_ID = '" + ModelID.ToString() + "'");
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                ExecuteStoreProcedure(storedProc.SP_Function, parameters, pageNumber, pageSize);
            }
        }

        void callProcRowTotalLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                connString = storedProc.Server.ConnectionString;
                databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        parameters.Add(item.ParameterName + ";" + " Model_ID = '" + ModelID.ToString() + "'");
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                GetTotalRows(connString, databaseName, storedProc.SP_Function, parameters);
            }
        }


        void callStoredProcedureNameLoadOperation2(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        string gridrship = "";
                        DataObject dataObject = radGridView1.SelectedItem as DataObject;
                        int max = 0;
                        int curr = 0;
                        foreach (GridRelationship rship in gridRship)
                        {
                            max = gridRship.Count;
                            curr += 1;
                            try
                            {
                                ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                                ReadField field2 = fieldList2.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                                string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                                gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";

                                if (curr < max)
                                {
                                    gridrship += "   and";
                                }
                            }
                            catch { }
                        }
                        gridrship = gridrship.TrimEnd('d');
                        gridrship = gridrship.TrimEnd('n');
                        gridrship = gridrship.TrimEnd('a');
                        //  string parameterValue = "0";
                        string strDefType = " DefinitionType ='" + tableName + "' ";
                        if (gridrship == "")
                        {
                            gridrship = strDefType;
                        }
                        else
                        {
                            gridrship = gridrship + " and " + strDefType;
                        }
                        parameters.Add(item.ParameterName + ";" + gridrship);
                    }


                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID2.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                ExecuteStoreProcedure2(conn, db, storedProc.SP_Function, parameters, pageNumber2, pageSize2);
            }
        }

        void callRowCountLoadOperation2(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();
                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        string gridrship = "";
                        DataObject dataObject = radGridView1.SelectedItem as DataObject;
                        int max = 0;
                        int curr = 0;
                        foreach (GridRelationship rship in gridRship)
                        {
                            max = gridRship.Count;
                            curr += 1;
                            try
                            {
                                ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                                ReadField field2 = fieldList2.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                                string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                                gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";

                                if (curr < max)
                                {
                                    gridrship += "   and";
                                }
                            }
                            catch { }
                        }
                        gridrship = gridrship.TrimEnd('d');
                        gridrship = gridrship.TrimEnd('n');
                        gridrship = gridrship.TrimEnd('a');
                        //  string parameterValue = "0";
                        string strDefType = " DefinitionType ='" + tableName + "' ";
                        if (gridrship == "")
                        {
                            gridrship = strDefType;
                        }
                        else
                        {
                            gridrship = gridrship + " and " + strDefType;
                        }
                        parameters.Add(item.ParameterName + ";" + gridrship);
                    }


                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID2.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }



                GetTotalRows2(connString, db, storedProc.SP_Function, parameters);
            }
        }


        void callStoredProcedureNameLoadOperation4(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        string gridrship = "";
                        DataObject dataObject = radGridView1.SelectedItem as DataObject;
                        int max = 0;
                        int curr = 0;
                        foreach (GridRelationship rship in gridRship)
                        {
                            max = gridRship.Count;
                            curr += 1;
                            try
                            {
                                ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                                ReadField field4 = fieldList4.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                                string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                                gridrship += " " + field4.FieldName + " = '" + parameterValue + "' ";

                                if (curr < max)
                                {
                                    gridrship += "   and";
                                }
                            }
                            catch { }
                        }
                        gridrship = gridrship.TrimEnd('d');
                        gridrship = gridrship.TrimEnd('n');
                        gridrship = gridrship.TrimEnd('a');
                        //  string parameterValue = "0";
                        string strDefType = " DefinitionType ='" + tableName + "' ";
                        if (gridrship == "")
                        {
                            gridrship = strDefType;
                        }
                        else
                        {
                            gridrship = gridrship + " and " + strDefType;
                        }
                        parameters.Add(item.ParameterName + ";" + gridrship);
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID4.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                ExecuteStoreProcedure4(conn, db, storedProc.SP_Function, parameters, pageNumber4, pageSize4);
            }
        }

        void callProcRowCountLoadOperation4(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        string gridrship = "";
                        DataObject dataObject = radGridView1.SelectedItem as DataObject;
                        int max = 0;
                        int curr = 0;
                        foreach (GridRelationship rship in gridRship)
                        {
                            max = gridRship.Count;
                            curr += 1;
                            try
                            {
                                ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                                ReadField field4 = fieldList4.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                                string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                                gridrship += " " + field4.FieldName + " = '" + parameterValue + "' ";

                                if (curr < max)
                                {
                                    gridrship += "   and";
                                }
                            }
                            catch { }
                        }
                        gridrship = gridrship.TrimEnd('d');
                        gridrship = gridrship.TrimEnd('n');
                        gridrship = gridrship.TrimEnd('a');
                        //  string parameterValue = "0";
                        string strDefType = " DefinitionType ='" + tableName + "' ";
                        if (gridrship == "")
                        {
                            gridrship = strDefType;
                        }
                        else
                        {
                            gridrship = gridrship + " and " + strDefType;
                        }
                        parameters.Add(item.ParameterName + ";" + gridrship);
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID4.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                GetTotalRows4(conn, db, storedProc.SP_Function, parameters);
            }
        }

        void callStoredProcedureNameLoadOperation5(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {


                    DataObject dataObject = radGridView1.SelectedItem as DataObject;
                    string parameterValue = dataObject.GetFieldValue("ID").ToString();

                    if (item.ParameterName == "Model_ID")
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }

                    if (item.ParameterName == "Definition_ID")
                    {
                        parameters.Add(item.ParameterName + ";" + parameterValue);
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID5.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                ExecuteStoreProcedure5(conn, db, storedProc.SP_Function, parameters, pageNumber5, pageSize5);
            }
        }



        void callProcRowCountLoadOperation5(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {


                    DataObject dataObject = radGridView1.SelectedItem as DataObject;
                    string parameterValue = dataObject.GetFieldValue("ID").ToString();

                    if (item.ParameterName == "Definition_ID")
                    {
                        parameters.Add(item.ParameterName + ";" + parameterValue);
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID5.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                GetTotalRows5(conn, db, storedProc.SP_Function, parameters);
            }
        }



        void callStoredProcedureNameLoadOperation3(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        string gridrship = "";
                        DataObject dataObject = radGridView1.SelectedItem as DataObject;
                        int max = 0;
                        int curr = 0;
                        foreach (GridRelationship rship in gridRship)
                        {
                            max = gridRship.Count;
                            curr += 1;
                            try
                            {
                                ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                                ReadField field3 = fieldList3.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                                string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                                gridrship += " " + field3.FieldName + " = '" + parameterValue + "' ";

                                if (curr < max)
                                {
                                    gridrship += "   and";
                                }
                            }
                            catch { }
                        }
                        gridrship = gridrship.TrimEnd('d');
                        gridrship = gridrship.TrimEnd('n');
                        gridrship = gridrship.TrimEnd('a');
                        //  string parameterValue = "0";
                        string strDefType = " DefinitionType ='" + tableName + "' ";
                        if (gridrship == "")
                        {
                            gridrship = strDefType;
                        }
                        else
                        {
                            gridrship = gridrship + " and " + strDefType;
                        }
                        parameters.Add(item.ParameterName + ";" + gridrship);
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID3.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }


                ExecuteStoreProcedure3(conn, db, storedProc.SP_Function, parameters, pageNumber3, pageSize3);
            }
        }


        void callProcRowCountLoadOperation3(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                string conn = storedProc.Server.ConnectionString;
                string db = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("FilterCondition"))
                    {
                        string gridrship = "";
                        DataObject dataObject = radGridView1.SelectedItem as DataObject;
                        int max = 0;
                        int curr = 0;
                        foreach (GridRelationship rship in gridRship)
                        {
                            max = gridRship.Count;
                            curr += 1;
                            try
                            {
                                ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                                ReadField field3 = fieldList3.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                                string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                                gridrship += " " + field3.FieldName + " = '" + parameterValue + "' ";

                                if (curr < max)
                                {
                                    gridrship += "   and";
                                }
                            }
                            catch { }
                        }
                        gridrship = gridrship.TrimEnd('d');
                        gridrship = gridrship.TrimEnd('n');
                        gridrship = gridrship.TrimEnd('a');
                        //  string parameterValue = "0";
                        string strDefType = " DefinitionType ='" + tableName + "' ";
                        if (gridrship == "")
                        {
                            gridrship = strDefType;
                        }
                        else
                        {
                            gridrship = gridrship + " and " + strDefType;
                        }
                        parameters.Add(item.ParameterName + ";" + gridrship);
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID3.ToString());
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                    else
                    {
                    }

                }

                GetTotalRows3(conn, db, storedProc.SP_Function, parameters);

            }
        }


        void callCreateSPLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                     if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                     if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }

                InsertTable(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, newDataObjectList, parameters);


            }
        }

        void callCreateSPLoadOperation2(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                     if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID2.ToString());
                    }
                     if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }

                InsertTable2(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, newDataObjectList2, parameters);


            }
        }

        void callCreateSPLoadOperation4(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                     if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID4.ToString());
                    }
                     if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }

                InsertTable4(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, newDataObjectList4, parameters);


            }
        }
        //private void GetMostFrequent(string str1)
        //{
        //    var strList = str1.Split(' ');

        //    var list1 = strList.ToLookup(s => s).Select(s => new { Name = s.Key, Count = s.Count() }).OrderByDescending(x => x.Count);
        //    var newList = list1.Select(x=>x.Name).Take(10);
        //}

        void callCreateSPLoadOperation3(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                     if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID3.ToString());
                    }
                     if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }

                InsertTable3(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, newDataObjectList3, parameters);


            }
        }

        void callUpdateSPLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                    if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }


                UpdateTable(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, editedDataObjectList, parameters);


            }
        }

        void callDeleteSPLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                    if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }


                DeleteTable(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, deletedDataObjectList, parameters);


            }
        }
        void callDeleteSPLoadOperation2(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID2.ToString());
                    }
                    if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }


                DeleteTable2(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, deletedDataObjectList2, parameters);


            }
        }


        void callDeleteSPLoadOperation4(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID4.ToString());
                    }
                    if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }


                DeleteTable4(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, deletedDataObjectList4, parameters);


            }
        }
        void callDeleteSPLoadOperation3(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID3.ToString());
                    }
                    if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }
                }


                DeleteTable3(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, deletedDataObjectList3, parameters);


            }
        }


        void callUpdateSPLoadOperation2(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                     if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID2.ToString());
                    }
                     if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                }


                UpdateTable2(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, editedDataObjectList2, parameters);


            }
        }



        void callUpdateSPLoadOperation4(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                     if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID4.ToString());
                    }
                     if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                }


                UpdateTable4(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, editedDataObjectList4, parameters);


            }
        }


        void callUpdateSPLoadOperation3(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();

                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    else if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID3.ToString());
                    }
                     if (item.DataType.Contains("TableType"))
                    {
                        parameters.Add(item.ParameterName + ";" + item.DataType);
                    }
                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                }


                UpdateTable3(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, editedDataObjectList3, parameters);


            }
        }
        private void GetViewTables()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackViewTables, null);


        }



        private void CallbackViewTables(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                var table = loadOp.Entities.FirstOrDefault();
                if (table != null)
                {
                    GetExendedPropertiesData(table.Server.ConnectionString, ExtendedPropertySQL.ExtendedPropertiesSQl(table.DBName, table.TableName), 1, 500, null);
                }
                //LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(viewID), CallbackViewFields, null);

            }

        }

        private void GetTables()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID), CallbackTables, null);


        }



        private void CallbackTables(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                tableList = loadOp.Entities.ToList();
                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(viewID), CallbackViewFields, null);

            }

        }



        private void CallbackViewFields(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                viewFieldSummary = new ObservableCollection<ViewFieldSummary>();
                foreach (ViewFieldSummary summary in loadOp.Entities)
                {
                    viewFieldSummary.Add(summary);

                }

                //  fieldList = loadOp.Entities.ToList();

                GetRelationshipsRefresh();

            }

        }


        private void GetGroupViewFilds()
        {
            int groupID = 0;
            if (Globals.CurrentUser.Group_ID != null)
                groupID = Globals.CurrentUser.Group_ID.Value;
            LoadOperation<GroupViewFilter> loadOp = context.Load(context.GetGroupViewFiltersByGroupViewIDQuery(groupID, viewID), CallbackGroupViewFilters, null);

        }



        private void CallbackGroupViewFilters(LoadOperation<GroupViewFilter> results)
        {

            if (results != null)
            {
                GroupViewFilterList = results.Entities.ToList();
            }


            string sql = CreateReadOnlyTable();
            sqlText = sql;
            newSqlText = sql;
            //btnSave.IsEnabled = false;
            //  GetTotalRows(newSqlText);
            // GetData(newSqlText, pageNumber, pageSize, tableName);

            ///GenerateColumns();
        }




        private void GetRelationshipsRefresh()
        {
            EditorContext cont = new EditorContext();
            LoadOperation<ViewRelationship> loadOp = cont.Load(cont.GetViewRelationshipByViewIDQuery(viewID), CallbackRelationships, null);




        }




        private void CallbackRelationships(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {


                relationshipList = new List<ViewRelationship>();
                relationshipList = loadOp.Entities.ToList();

                GetGroupViewFilds();
            }
        }

        private void GetFields()
        {
            LoadOperation<Field> loadOperation = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == tableID), CallbackFields, null);
        }


        private void CallbackFields(LoadOperation<Field> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                this.radGridView1.AutoGenerateColumns = false;
                foreach (Field field in loadOperation.Entities)
                {
                    GridViewDataColumn column = new GridViewDataColumn();
                    column.DataMemberBinding = new Binding(field.FieldName.Replace(" ", "_"));
                    column.Header = field.FriendlyName;
                    column.UniqueName = field.FieldName;
                    if (field.Type == "datetime")
                    {
                        column.DataFormatString = "{0:dd/MM/yyyy}";
                    }


                    this.radGridView1.Columns.Add(column);
                }

            }

        }



        private void LoadDropDowns()
        {
            if (tempFieldList.Count > 0)
            {


                TempField temp = tempFieldList.Where(t => t.Index == fieldIndex + 1).FirstOrDefault();
                dropDownName = temp.Name;
                filterOption = temp.OptionCompletion;
                autoPopulate = temp.AutoComplete;
                currFieldID = temp.FieldID;
                hidden = temp.Hidden;
                dataType = temp.FieldType.ToLower();
                LoadOperation<StoredProcedureName> loadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(x => x.ID == temp.ID), callbackDPLoadOperation, null);

                fieldIndex = temp.Index;
            }
        }

        void callbackDPLoadOperation(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();

                //databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();
                string subSql = "";


                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID.ToString());
                    }
                    if (item.ParameterName.Contains("Type"))
                    {
                        parameters.Add(item.ParameterName + ";" + tableName);
                    }

                    if (item.ParameterName.Contains("Filter"))
                    {
                        parameters.Add(item.ParameterName + ";" + filterOption);
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                }

                GetDropdownData(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, parameters, 1, 1000);

            }
        }


        void callbackDPLoadOperation2(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();

                //databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();
                string subSql = "";


                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID2.ToString());
                    }
                    if (item.ParameterName.Contains("Type"))
                    {
                        parameters.Add(item.ParameterName + ";" + tableName);
                    }

                    if (item.ParameterName.Contains("Filter"))
                    {
                        parameters.Add(item.ParameterName + ";" + filterOption2);
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                }

                GetDropdownData2(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, parameters, 1, 1000);

            }
        }


        void callbackDPLoadOperation4(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();

                //databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();
                string subSql = "";


                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID4.ToString());
                    }
                    if (item.ParameterName.Contains("Type"))
                    {
                        parameters.Add(item.ParameterName + ";" + tableName);
                    }

                    if (item.ParameterName.Contains("Filter"))
                    {
                        parameters.Add(item.ParameterName + ";" + filterOption4);
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                    //if (item.ParameterName.Contains("DisplayDefinition"))
                    //{
                    //    parameters.Add(item.ParameterName + ";" + displayDefID);
                    //}

                }

                GetDropdownData4(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, parameters, 1, 1000);

            }
        }


        void callbackDPLoadOperation3(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();

                //databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();
                string subSql = "";


                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID3.ToString());
                    }
                    if (item.ParameterName.Contains("Type"))
                    {
                        parameters.Add(item.ParameterName + ";" + tableName);
                    }

                    if (item.ParameterName.Contains("Filter"))
                    {
                        parameters.Add(item.ParameterName + ";" + filterOption3);
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                    //if (item.ParameterName.Contains("DisplayDefinition"))
                    //{
                    //    parameters.Add(item.ParameterName + ";" + displayDefID);
                    //}

                }

                GetDropdownData3(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, parameters, 1, 1000);

            }
        }

        void callbackDPLoadOperation5(LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation)
        {
            if (StoredProcedureNameLoadOperation != null)
            {
                var storedProc = StoredProcedureNameLoadOperation.Entities.FirstOrDefault();

                //databaseName = storedProc.DBName;
                ObservableCollection<string> parameters = new ObservableCollection<string>();
                var storeParameters = storedProc.StoredProcedureParameters.ToList();
                string subSql = "";


                foreach (var item in storeParameters)
                {
                    if (item.ParameterName.Contains("Model_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + ModelID.ToString());
                    }
                    if (item.ParameterName.Contains("View_ID"))
                    {
                        parameters.Add(item.ParameterName + ";" + viewID5.ToString());
                    }
                    if (item.ParameterName.Contains("Type"))
                    {
                        parameters.Add(item.ParameterName + ";" + tableName);
                    }

                    if (item.ParameterName.Contains("Filter"))
                    {
                        parameters.Add(item.ParameterName + ";" + filterOption4);
                    }

                    if (item.ParameterName.Contains("DisplayDefinition"))
                    {
                        parameters.Add(item.ParameterName + ";" + displayDefID);
                    }

                    //if (item.ParameterName.Contains("DisplayDefinition"))
                    //{
                    //    parameters.Add(item.ParameterName + ";" + displayDefID);
                    //}

                }

                GetDropdownData5(storedProc.Server.ConnectionString, storedProc.DBName, storedProc.SP_Function, parameters, 1, 1000);

            }
        }
        private void CallbackDropDown(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                GetColumnData(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");

            }
        }


        //private void LoadConfigSetting()
        //{
        //    LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting, null);
        //}

        private void CallbackSetting(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 1)
                        settingObjectList.Add(sett);
                }
                settingObjectList.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 1, Settings_ID = 0 });

                ddlConfig.ItemsSource = settingObjectList;


                var query = (from s in settingObjectList
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                {
                    ddlConfig.SelectedValue = query.ID;
                    GetDefaultSetting(query.Setting);
                }


            }


        }


        private void CallbackDefault(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    // primarySettings.ResetState();

                    //settings = new RadGridViewSettings(this.radGridView1);
                    //string xml = setting.Settings;
                    // settings.LoadState(xml);




                }
                catch
                {
                }
            }
        }
        #endregion



        #region Private Methods

        private void Cancel()
        {
            editedDataObjectList = new ObservableCollection<DataObject>();
            newDataObjectList = new ObservableCollection<DataObject>();
            deletedDataObjectList = new ObservableCollection<DataObject>();
            //cMenu.IsOpen = false;
        }

        private void ExcelExport()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView1.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        private void SaveChanges()
        {
            try
            {
                 editedDataObjectList = new ObservableCollection<DataObject>();
                newDataObjectList = new ObservableCollection<DataObject>();
               // deletedDataObjectList3 = new ObservableCollection<DataObject>();
                foreach (var item in dataObjectList)
                {
                    if (item.State == DataObject.DataStates.Modified)
                    {
                        editedDataObjectList.Add(item);
                    }

                    //if (item.State == DataObject.DataStates.Added)
                    //{
                    //    newDataObjectList3.Add(item);
                    //}

                    if (item.State == DataObject.DataStates.Deleted)
                    {
                        deletedDataObjectList.Add(item);
                    }
                }

                foreach (var item in radGridView1.Items)
                {
                    DataObject data = item as DataObject;

                    string strID = data.GetFieldValue("ID").ToString();
                    if (strID == "0")
                    {

                
                        
                        DataObject newDataObject = item as DataObject;
                    try
                    {
                       

                        object id = ModelID as object;
                        newDataObject.SetFieldValue("Model_ID", ModelID);

                        
                      
                    }
                    catch { }
                    foreach (var field in fieldList)
                    {
                        try
                        {
                            if (field.DefaultValue != null)
                            {

                                object obj = field.DefaultValue as object;
                                if (obj.ToString() != "")
                                {

                                    if (field.AutoPopulate)
                                    newDataObject.SetFieldValue(field.FieldName, obj);
                                }
                            }
                        }
                        catch
                        {
                        }

                    }
                    newDataObjectList.Add(newDataObject);
                   
                }
                }

                if (newDataObjectList.Count > 0)
                {
                    if (useCreateSP)
                    {
                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == CreateSPID), callCreateSPLoadOperation, null);

                    }
                    else
                    {
                        InsertDataTable(newDataObjectList, tableName);
                    }

                }


                //Update
                if (editedDataObjectList.Count > 0)
                {

                    if (useUpdateSP)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == UpdateSPID), callUpdateSPLoadOperation, null);
                    }
                    else
                    {
                        Update(editedDataObjectList, tableName);
                    }
                }

                if (deletedDataObjectList.Count > 0)
                {

                    if (useDeleteSP)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == DeleteSPID), callDeleteSPLoadOperation, null);
                    }
                    else
                    {
                        DeleteDataTable(deletedDataObjectList, tableName);

                    }

                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }



        private void DeleteRows()
        {
            try
            {

                if (this.radGridView1.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView1.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView1.Items.Remove(item as DataObject);
                    deletedDataObjectList.Add(item);
                }

                int count = deletedDataObjectList.Count;
            }
            catch
            {
            }
        }


        private void ClearFilters()
        {

            this.radGridView1.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView1.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView1.FilterDescriptors.ResumeNotifications();
            // primarySettings.LoadOriginalState();
        }

        private void SaveConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings = new RadGridViewSettings(this.radGridView1);

                    string settingsXML = settings.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            string alertText = "Error";

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText });
                        }
                        else
                        {

                            string alertText = "Successful";

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                string alertText = ex.Message;

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText });
            }
        }

        private void SaveConfigAs()
        {
            try
            {

                settings = new RadGridViewSettings(this.radGridView1);

                string settingsXML = settings.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 1, displayDefID);
                config.Closed += ChildWin_Closed;
                config.Left = (Application.Current.Host.Content.ActualWidth - config.ActualWidth) / 2;
                config.Top = (Application.Current.Host.Content.ActualHeight - config.ActualHeight) / 2;
                config.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                config.ShowDialog();
            }
            catch (Exception ex)
            {
                string alertText = ex.Message;

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText });
            }
        }


        private void DeleteConfig()
        {
            try
            {


                UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            Message.ErrorMessage(so.Error.Message + " ...");
                        }
                        else
                        {

                            Message.InfoMessage("Successfully Deleted");
                            ClearFilters();
                            ddlConfig.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                string alertText = ex.Message;

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText }); ;
            }
        }


        private bool isFirstLoad = true;
        string xmlCache = "";
        string primaryXml = "";
        private void GetDefaultSetting(Setting setting)
        {
            if (isFirstLoad)
            {

                primarySettings = new RadGridViewSettings(this.radGridView1);
                primarySettings.SaveState();
                primaryXml = primarySettings.SaveState();
                isFirstLoad = false;
            }




            if (setting != null)
            {
                ddlConfig.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {

                    ClearFilters();
                    //  primarySettings.LoadOriginalState();
                    primarySettings.ResetState();

                    settings = new RadGridViewSettings(this.radGridView1);
                    string xml = setting.Settings;
                    xmlCache = xml;
                    settings.LoadState(xml);

                    //createSqlWithNewFilters();




                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }

        private string testName = "";


        private void GenerateColumns(List<ReadField> readFieldList)
        {
            if (canGenerateColumns)
            {
                int i = 1;
                tempFieldList = new List<TempField>();
                this.radGridView1.AutoGenerateColumns = false;
                this.radGridView1.ShowInsertRow = false;

                foreach (ReadField field in readFieldList)
                {
                    tableName = field.TableName;

                    if (field.StoredProcedureNames_ID != null)
                    {


                        if (field.UseValueStoredProcedure)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(field.FieldName);
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = field.FieldName;
                            testName = field.FieldName;


                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";
                            columnComboBox.IsFilteringDeferred = true;
                            columnComboBox.IsFilteringDeferred = true;
                            columnComboBox.IsReadOnly = field.ReadOnly;
                            if (field.Hidden)
                            {
                                columnComboBox.IsVisible = false;
                            }
                            else
                            {
                                this.radGridView1.Columns.Add(columnComboBox);
                            }

                            tempFieldList.Add(new TempField { FieldID = field.ID, ID = field.StoredProcedureNames_ID.Value, Name = field.FieldName, Index = i, StoreProcName = field.SP_Function, OptionCompletion = field.FieldOptionFilter, AutoComplete = field.AutoPopulate, Hidden = field.Hidden, FieldType = field.FieldType });
                            i++;
                        }
                        else
                        {


                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView1.Columns.Add(column);
                            }
                        }
                    }
                    else
                    {


                        GridViewDataColumn column = new GridViewDataColumn();
                        column.DataMemberBinding = new Binding(field.FieldName);
                        column.Header = field.DisplayName;
                        column.UniqueName = field.FieldName;
                        column.IsFilteringDeferred = true;
                        column.IsReadOnly = field.ReadOnly;
                        if (field.Hidden)
                        {
                            column.IsVisible = false;
                        }

                        else
                        {
                            this.radGridView1.Columns.Add(column);
                        }




                    }
                }

                this.radGridView1.CanUserFreezeColumns = true;
                canGenerateColumns = false;

                tempFieldList = tempFieldList.OrderBy(t => t.Index).ToList();
                indexMax = tempFieldList.Count;

                LoadDropDowns();
                Attach();


            }

            LoadConfigSetting();
            //   GetTables2();

            ReCountPage();
        }


        private void GenerateColumns2(List<ReadField> readFieldList)
        {
            if (canGenerateColumns2)
            {
                int i = 1;
                tempFieldList2 = new List<TempField>();
                this.radGridView2.AutoGenerateColumns = false;
                this.radGridView2.ShowInsertRow = false;

                foreach (ReadField field in readFieldList)
                {
                    tableName2 = field.TableName;

                    if (field.StoredProcedureNames_ID != null)
                    {
                        if (field.UseValueStoredProcedure)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(field.FieldName);
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = field.FieldName;
                            testName2 = field.FieldName;
                            columnComboBox.IsReadOnly = field.ReadOnly;

                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";

                            columnComboBox.IsFilteringDeferred = true;
                            //  columnComboBox.IsFilteringDeferred = true;
                            if (field.Hidden)
                            {
                                columnComboBox.IsVisible = false;
                            }
                            else
                            {
                                this.radGridView2.Columns.Add(columnComboBox);
                            }

                            tempFieldList2.Add(new TempField { FieldID = field.ID, ID = field.StoredProcedureNames_ID.Value, Name = field.FieldName, Index = i, StoreProcName = field.SP_Function, OptionCompletion = field.FieldOptionFilter, AutoComplete = field.AutoPopulate, Hidden = field.Hidden, FieldType = field.FieldType });
                            i++;
                        }
                        else
                        {
                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView2.Columns.Add(column);
                            }

                        }
                    }
                   
                    else
                    {
                        if (ModelName.Contains("Hourly"))
                        {

                            if (field.FieldType == "datetime")
                            {
                                DateTimePickerColumn column = new DateTimePickerColumn();
                                column.DataMemberBinding = new Binding(field.FieldName);
                                column.Header = field.DisplayName;
                                column.UniqueName = field.FieldName;
                                column.IsFilteringDeferred = true;
                                column.IsReadOnly = field.ReadOnly;
                               // column.IsFrozen = true;
                                if (field.FieldType == "datetime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy HH:mm tt}";
                                }
                                if (field.Hidden)
                                {
                                    column.IsVisible = false;
                                }

                                else
                                {
                                    this.radGridView2.Columns.Add(column);
                                }

                            }
                            else
                            {
                                GridViewDataColumn column = new GridViewDataColumn();
                                column.DataMemberBinding = new Binding(field.FieldName);
                                column.Header = field.DisplayName;
                                column.UniqueName = field.FieldName;
                                column.IsFilteringDeferred = true;
                                column.IsReadOnly = field.ReadOnly;

                                if (field.FieldType == "datetime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                }
                                if (field.Hidden)
                                {
                                    column.IsVisible = false;
                                }

                                else
                                {
                                    this.radGridView2.Columns.Add(column);
                                }

                            }
                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;

                            if (field.FieldType == "datetime")
                            {
                                column.DataFormatString = "{0:dd-MMM-yyyy}";
                            }
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView2.Columns.Add(column);
                            }

                        }


                    }
                }

                this.radGridView2.CanUserFreezeColumns = true;
                canGenerateColumns2 = false;

                tempFieldList2 = tempFieldList2.OrderBy(t => t.Index).ToList();
                indexMax2 = tempFieldList2.Count;
                //  GetViewTables2();
                ///LoadExtendedProperties();
                LoadDropDowns2();
                Attach2();


            }

            LoadConfigSetting2();
          
        }

        private void GenerateColumns4(List<ReadField> readFieldList)
        {
            if (canGenerateColumns4)
            {
                int i = 1;
                tempFieldList4 = new List<TempField>();
                this.radGridView4.AutoGenerateColumns = false;
                this.radGridView4.ShowInsertRow = false;

                foreach (ReadField field in readFieldList)
                {
                    tableName4 = field.TableName;

                    if (field.StoredProcedureNames_ID != null)
                    {
                        if (field.UseValueStoredProcedure)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(field.FieldName);
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = field.FieldName;
                            testName2 = field.FieldName;
                            columnComboBox.IsReadOnly = field.ReadOnly;

                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";

                            columnComboBox.IsFilteringDeferred = true;
                            //  columnComboBox.IsFilteringDeferred = true;
                            if (field.Hidden)
                            {
                                columnComboBox.IsVisible = false;
                            }
                            else
                            {
                                this.radGridView4.Columns.Add(columnComboBox);
                            }

                            tempFieldList4.Add(new TempField { FieldID = field.ID, ID = field.StoredProcedureNames_ID.Value, Name = field.FieldName, Index = i, StoreProcName = field.SP_Function, OptionCompletion = field.FieldOptionFilter, AutoComplete = field.AutoPopulate, Hidden = field.Hidden, FieldType = field.FieldType });
                            i++;
                        }
                        else
                        {
                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView4.Columns.Add(column);
                            }

                        }
                    }
                    else
                    {


                        if (ModelName.Contains("Hourly"))
                        {

                            if (field.FieldType == "datetime")
                            {
                                DateTimePickerColumn column = new DateTimePickerColumn();
                                column.DataMemberBinding = new Binding(field.FieldName);
                                column.Header = field.DisplayName;
                                column.UniqueName = field.FieldName;
                                column.IsFilteringDeferred = true;
                                column.IsReadOnly = field.ReadOnly;
                                // column.IsFrozen = true;
                                if (field.FieldType == "datetime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy HH:mm tt}";
                                }
                                if (field.Hidden)
                                {
                                    column.IsVisible = false;
                                }

                                else
                                {
                                    this.radGridView4.Columns.Add(column);
                                }

                            }
                            else
                            {
                                GridViewDataColumn column = new GridViewDataColumn();
                                column.DataMemberBinding = new Binding(field.FieldName);
                                column.Header = field.DisplayName;
                                column.UniqueName = field.FieldName;
                                column.IsFilteringDeferred = true;
                                column.IsReadOnly = field.ReadOnly;

                                if (field.FieldType == "datetime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                }
                                if (field.Hidden)
                                {
                                    column.IsVisible = false;
                                }

                                else
                                {
                                    this.radGridView4.Columns.Add(column);
                                }

                            }
                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;

                            if (field.FieldType == "datetime")
                            {
                                column.DataFormatString = "{0:dd-MMM-yyyy}";
                            }
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView4.Columns.Add(column);
                            }

                        }


                    }
                }
                this.radGridView4.CanUserFreezeColumns = true;
                canGenerateColumns4 = false;

                tempFieldList4 = tempFieldList4.OrderBy(t => t.Index).ToList();
                indexMax4 = tempFieldList4.Count;
                //  GetViewTables2();
                ///LoadExtendedProperties();
                LoadDropDowns4();
               
                Attach4();


            }
           // ReCountPage4();
            LoadConfigSetting4();
            LoadConfigSetting5();
            LoadConfigSetting6();
            Attach6();
           
        }
        private void GenerateColumns3(List<ReadField> readFieldList)
        {
            if (canGenerateColumns3)
            {
                int i = 1;
                tempFieldList3 = new List<TempField>();
                this.radGridView3.AutoGenerateColumns = false;
                this.radGridView3.ShowInsertRow = false;

                foreach (ReadField field in readFieldList)
                {
                    tableName3 = field.TableName;

                    if (field.StoredProcedureNames_ID != null)
                    {
                        if (field.UseValueStoredProcedure)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(field.FieldName);
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = field.FieldName;
                            testName2 = field.FieldName;
                            columnComboBox.IsReadOnly = field.ReadOnly;

                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";

                            columnComboBox.IsFilteringDeferred = true;
                            //  columnComboBox.IsFilteringDeferred = true;
                            if (field.Hidden)
                            {
                                columnComboBox.IsVisible = false;
                            }
                            else
                            {
                                this.radGridView3.Columns.Add(columnComboBox);
                            }

                            tempFieldList3.Add(new TempField { FieldID = field.ID, ID = field.StoredProcedureNames_ID.Value, Name = field.FieldName, Index = i, StoreProcName = field.SP_Function, OptionCompletion = field.FieldOptionFilter, AutoComplete = field.AutoPopulate, Hidden = field.Hidden, FieldType = field.FieldType });
                            i++;
                        }
                        else
                        {
                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView3.Columns.Add(column);
                            }

                        }
                    }
                    else
                    {

                if (ModelName.Contains("Hourly"))
                        {

                            if (field.FieldType == "datetime")
                            {
                                DateTimePickerColumn column = new DateTimePickerColumn();
                                column.DataMemberBinding = new Binding(field.FieldName);
                                column.Header = field.DisplayName;
                                column.UniqueName = field.FieldName;
                                column.IsFilteringDeferred = true;
                                column.IsReadOnly = field.ReadOnly;
                               // column.IsFrozen = true;
                                if (field.FieldType == "datetime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy HH:mm tt}";
                                }
                                if (field.Hidden)
                                {
                                    column.IsVisible = false;
                                }

                                else
                                {
                                    this.radGridView3.Columns.Add(column);
                                }

                            }
                            else
                            {
                                GridViewDataColumn column = new GridViewDataColumn();
                                column.DataMemberBinding = new Binding(field.FieldName);
                                column.Header = field.DisplayName;
                                column.UniqueName = field.FieldName;
                                column.IsFilteringDeferred = true;
                                column.IsReadOnly = field.ReadOnly;

                                if (field.FieldType == "datetime")
                                {
                                    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                }
                                if (field.Hidden)
                                {
                                    column.IsVisible = false;
                                }

                                else
                                {
                                    this.radGridView3.Columns.Add(column);
                                }

                            }
                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(field.FieldName);
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;
                            column.IsReadOnly = field.ReadOnly;

                            if (field.FieldType == "datetime")
                            {
                                column.DataFormatString = "{0:dd-MMM-yyyy}";
                            }
                            if (field.Hidden)
                            {
                                column.IsVisible = false;
                            }

                            else
                            {
                                this.radGridView3.Columns.Add(column);
                            }

                        }


                    }



                    }
                

                this.radGridView3.CanUserFreezeColumns = true;
                canGenerateColumns3 = false;

                tempFieldList3 = tempFieldList3.OrderBy(t => t.Index).ToList();
                indexMax3 = tempFieldList3.Count;
                //  GetViewTables2();
                ///LoadExtendedProperties();
                LoadDropDowns3();
                Attach3();


            }

            LoadConfigSetting3();
          
        }



        private void LoadExtendedProperties()
        {
            foreach (DataObject dataObject in propertiesList)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView1.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList;
                }
                catch
                {
                }
            }
        }

        private string CreateReadOnlyTable()
        {
            string sql = string.Empty;
            //table list
            //display field list
            //relationshiplist

            string tables = " From ";
            string fields = " Select ";
            string relationships = " Where ";
            string viewFilter = "";
            foreach (Table table in tableList)
            {
                tables += "[" + table.DBName + "].[dbo].[" + table.TableName + "] as " + "[" + table.TableName.ToString() + "] ,";
            }

            //foreach (ViewFieldSummary field in fieldList)
            //{
            //    if (field.TableID > 0)
            //    {
            //        fields += field.TableField + " ,";//" as [" + field.DisplayName + "] ,";
            //    }
            //    else
            //    {
            //        fields += " (" + field.Expression + " ) as [" + field.DisplayName + "] ,";
            //    }
            //}

            foreach (ViewRelationship rship in relationshipList)
            {
                relationships += " " + rship.Summary + " and";

            }

            foreach (GroupViewFilter item in GroupViewFilterList)
            {
                viewFilter += " " + item.Summary + " and";
            }

            tables = tables.TrimEnd(',');
            fields = fields.TrimEnd(',');
            relationships = relationships.TrimEnd('d');
            relationships = relationships.TrimEnd('n');
            relationships = relationships.TrimEnd('a');
            viewFilter = viewFilter.TrimEnd('d');
            viewFilter = viewFilter.TrimEnd('n');
            viewFilter = viewFilter.TrimEnd('a');

            if (relationships == " Where ")
            {
                if (viewFilter != "")
                {
                    relationships += viewFilter;
                }
                else
                {
                    relationships = string.Empty;
                }

            }
            else
            {
                if (viewFilter != "")
                {
                    relationships += " and " + viewFilter;
                }
            }


            sql = fields + tables + relationships;

            return sql;

        }
        #endregion



        #region Call WFC Services Methods
        //private void GetDropdownData(string conn, string db, string sql, int pagenumber, int pagesize, object userState)
        //{

        //    var ws = WCF.GetService();
        //    ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataCompleted);
        //    ws.GetDataSetDataAsync(conn, db, sql, pagenumber, pagesize, userState);
        //}

        void GetDropdownData(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            ;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_GetColumnDataCompleted);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }



        void ws_GetColumnDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
              

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {
                    if (!hidden)
                    {

                        List<DropDownIntData> intDataList = new List<DropDownIntData>();
                        List<DropDownLongData> longDataList = new List<DropDownLongData>();
                        List<DropDownStringData> stringDataList = new List<DropDownStringData>();
                        try
                        {
                            if (dataType == "int")
                            {
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    intDataList.Add(new DropDownIntData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = intDataList;

                            }

                            else if (dataType == "bigint")
                            {

                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    longDataList.Add(new DropDownLongData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = longDataList;
                            }
                            else
                            {
                                stringDataList = new List<DropDownStringData>();
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    // long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                                }

                                ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = stringDataList;
                            }
                        }
                        catch
                        {
                            stringDataList = new List<DropDownStringData>();
                            foreach (var dataItem in list.Cast<DataObject>().ToList())
                            {
                                string value = dataItem.GetFieldValue("Value").ToString();

                                // long val = Convert.ToInt64(value);
                                string display = dataItem.GetFieldValue("Display").ToString();

                                stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                            }

                            ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = stringDataList;
                        }
                    }


                    if (autoPopulate)
                    {
                        try
                        {
                            DataObject data = list.Cast<DataObject>().FirstOrDefault();
                            string value = data.GetFieldValue("Value").ToString();
                            var readField = fieldList.Where(f => f.ID == currFieldID).FirstOrDefault();
                            readField.DefaultValue = value;
                        }
                        catch
                        {
                        }
                    }
                    if (fieldIndex < indexMax)
                    {
                        LoadDropDowns();
                    }


                }

            }

        }


        void GetDropdownData2(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            ;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_GetColumnDataCompleted2);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }



        void ws_GetColumnDataCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {

                    if (!hidden2)
                    {

                        List<DropDownIntData> intDataList = new List<DropDownIntData>();
                        List<DropDownLongData> longDataList = new List<DropDownLongData>();
                        List<DropDownStringData> stringDataList = new List<DropDownStringData>();
                        try
                        {
                            if (dataType2 == "int")
                            {
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    intDataList.Add(new DropDownIntData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = intDataList;

                            }

                            else if (dataType2 == "bigint")
                            {

                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    longDataList.Add(new DropDownLongData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = longDataList;
                            }
                            else
                            {
                                stringDataList = new List<DropDownStringData>();
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    // long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                                }

                                ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = stringDataList;
                            }
                        }
                        catch
                        {
                            stringDataList = new List<DropDownStringData>();
                            foreach (var dataItem in list.Cast<DataObject>().ToList())
                            {
                                string value = dataItem.GetFieldValue("Value").ToString();

                                // long val = Convert.ToInt64(value);
                                string display = dataItem.GetFieldValue("Display").ToString();

                                stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                            }

                            ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = stringDataList;
                        }
                        // ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = list.Cast<DataObject>().ToList();
                    }
                    if (autoPopulate2)
                    {
                        try
                        {
                            DataObject data = list.Cast<DataObject>().FirstOrDefault();
                            string value = data.GetFieldValue("Value").ToString();
                            var readField = fieldList2.Where(f => f.ID == currFieldID2).FirstOrDefault();
                            readField.DefaultValue = value;
                        }
                        catch
                        {
                        }
                    }
                    if (fieldIndex2 < indexMax2)
                    {
                        LoadDropDowns2();
                    }


                }

            }

        }




        void GetDropdownData4(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            ;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_GetColumnDataCompleted4);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }



        void ws_GetColumnDataCompleted4(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {

                    if (!hidden4)
                    {

                        List<DropDownIntData> intDataList = new List<DropDownIntData>();
                        List<DropDownLongData> longDataList = new List<DropDownLongData>();
                        List<DropDownStringData> stringDataList = new List<DropDownStringData>();
                        try
                        {
                            if (dataType4 == "int")
                            {
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    intDataList.Add(new DropDownIntData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView4.Columns[dropDownName4]).ItemsSource = intDataList;

                            }

                            else if (dataType4 == "bigint")
                            {

                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    longDataList.Add(new DropDownLongData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView4.Columns[dropDownName4]).ItemsSource = longDataList;
                            }
                            else
                            {
                                stringDataList = new List<DropDownStringData>();
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    // long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                                }

                                ((GridViewComboBoxColumn)this.radGridView4.Columns[dropDownName4]).ItemsSource = stringDataList;
                            }
                        }
                        catch
                        {
                            stringDataList = new List<DropDownStringData>();
                            foreach (var dataItem in list.Cast<DataObject>().ToList())
                            {
                                string value = dataItem.GetFieldValue("Value").ToString();

                                // long val = Convert.ToInt64(value);
                                string display = dataItem.GetFieldValue("Display").ToString();

                                stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                            }

                            ((GridViewComboBoxColumn)this.radGridView4.Columns[dropDownName4]).ItemsSource = stringDataList;
                        }
                        // ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = list.Cast<DataObject>().ToList();
                    }
                    if (autoPopulate4)
                    {
                        try
                        {
                            DataObject data = list.Cast<DataObject>().FirstOrDefault();
                            string value = data.GetFieldValue("Value").ToString();
                            var readField = fieldList4.Where(f => f.ID == currFieldID4).FirstOrDefault();
                            readField.DefaultValue = value;
                        }
                        catch
                        {
                        }
                    }
                    if (fieldIndex4 < indexMax4)
                    {
                        LoadDropDowns4();
                    }


                }

            }

        }




        void GetDropdownData3(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {

            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_GetColumnDataCompleted3);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }



        void ws_GetColumnDataCompleted3(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {

                    if (!hidden3)
                    {

                        List<DropDownIntData> intDataList = new List<DropDownIntData>();
                        List<DropDownLongData> longDataList = new List<DropDownLongData>();
                        List<DropDownStringData> stringDataList = new List<DropDownStringData>();
                        try
                        {
                            if (dataType3 == "int")
                            {
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    intDataList.Add(new DropDownIntData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView3.Columns[dropDownName3]).ItemsSource = intDataList;

                            }

                            else if (dataType3 == "bigint")
                            {

                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    longDataList.Add(new DropDownLongData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView3.Columns[dropDownName3]).ItemsSource = longDataList;
                            }
                            else
                            {
                                stringDataList = new List<DropDownStringData>();
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    // long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                                }

                                ((GridViewComboBoxColumn)this.radGridView3.Columns[dropDownName3]).ItemsSource = stringDataList;
                            }
                        }
                        catch
                        {
                            stringDataList = new List<DropDownStringData>();
                            foreach (var dataItem in list.Cast<DataObject>().ToList())
                            {
                                string value = dataItem.GetFieldValue("Value").ToString();

                                // long val = Convert.ToInt64(value);
                                string display = dataItem.GetFieldValue("Display").ToString();

                                stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                            }

                            ((GridViewComboBoxColumn)this.radGridView3.Columns[dropDownName3]).ItemsSource = stringDataList;
                        }
                        // ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = list.Cast<DataObject>().ToList();
                    }
                    if (autoPopulate3)
                    {
                        try
                        {
                            DataObject data = list.Cast<DataObject>().FirstOrDefault();
                            string value = data.GetFieldValue("Value").ToString();
                            var readField = fieldList3.Where(f => f.ID == currFieldID3).FirstOrDefault();
                            readField.DefaultValue = value;
                        }
                        catch
                        {
                        }
                    }
                    if (fieldIndex3 < indexMax3)
                    {
                        LoadDropDowns3();
                    }


                }

            }

        }



        void GetDropdownData5(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            ;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_GetColumnDataCompleted5);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }



        void ws_GetColumnDataCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {

                    if (!hidden5)
                    {

                        List<DropDownIntData> intDataList = new List<DropDownIntData>();
                        List<DropDownLongData> longDataList = new List<DropDownLongData>();
                        List<DropDownStringData> stringDataList = new List<DropDownStringData>();
                        try
                        {
                            if (dataType4 == "int")
                            {
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    int val = Convert.ToInt32(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    intDataList.Add(new DropDownIntData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView5.Columns[dropDownName5]).ItemsSource = intDataList;

                            }

                            else if (dataType4 == "bigint")
                            {

                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    longDataList.Add(new DropDownLongData { Value = val, Display = display });
                                }
                                ((GridViewComboBoxColumn)this.radGridView5.Columns[dropDownName5]).ItemsSource = longDataList;
                            }
                            else
                            {
                                stringDataList = new List<DropDownStringData>();
                                foreach (var dataItem in list.Cast<DataObject>().ToList())
                                {
                                    string value = dataItem.GetFieldValue("Value").ToString();

                                    // long val = Convert.ToInt64(value);
                                    string display = dataItem.GetFieldValue("Display").ToString();

                                    stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                                }

                                ((GridViewComboBoxColumn)this.radGridView5.Columns[dropDownName5]).ItemsSource = stringDataList;
                            }
                        }
                        catch
                        {
                            stringDataList = new List<DropDownStringData>();
                            foreach (var dataItem in list.Cast<DataObject>().ToList())
                            {
                                string value = dataItem.GetFieldValue("Value").ToString();

                                // long val = Convert.ToInt64(value);
                                string display = dataItem.GetFieldValue("Display").ToString();

                                stringDataList.Add(new DropDownStringData { Value = value, Display = display });
                            }

                            ((GridViewComboBoxColumn)this.radGridView5.Columns[dropDownName5]).ItemsSource = stringDataList;
                        }
                        // ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = list.Cast<DataObject>().ToList();
                    }
                    if (autoPopulate5)
                    {
                        try
                        {
                            DataObject data = list.Cast<DataObject>().FirstOrDefault();
                            string value = data.GetFieldValue("Value").ToString();
                            var readField = fieldList5.Where(f => f.ID == currFieldID5).FirstOrDefault();
                            readField.DefaultValue = value;
                        }
                        catch
                        {
                        }
                    }
                    if (fieldIndex5 < indexMax5)
                    {
                        LoadDropDowns5();
                    }


                }

            }

        }

        void ExecuteStoreProcedure(string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            this.radGridView1.IsBusy = true;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted);
            ws.ExecuteStoreProcAsync(connString, databaseName, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }

        void ws_ExecuteStoreProcedureCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });

            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView1.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }
                    radGridView1.ItemsSource = list;
                    dataObjectList = new ObservableCollection<DataObject>();
                    foreach (var item in list)
                    {
                        DataObject dataO = item as DataObject;
                        dataObjectList.Add(dataO);
                    }


                   if(dataObjectList.Count > 0)
                    radGridView1.ItemsSource = dataObjectList;

                    foreach (var item in desc)
                    {
                        radGridView1.FilterDescriptors.Add(item);
                    }
                }
            }
            this.radGridView1.IsBusy = false;
        }


        void ExecuteStoreProcedure2(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            this.radGridView2.IsBusy = true;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted2);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }

        void ws_ExecuteStoreProcedureCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView2.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    radGridView2.ItemsSource = list;
                    dataObjectList2 = new ObservableCollection<DataObject>();
                    foreach (var item in list)
                    {
                        DataObject dataO = item as DataObject;
                        dataObjectList2.Add(dataO);
                    }

                    if(dataObjectList2.Count> 0)
                    radGridView2.ItemsSource = dataObjectList2;


                    foreach (var item in desc)
                    {
                        radGridView2.FilterDescriptors.Add(item);
                    }
                }
            }
            this.radGridView2.IsBusy = false;
        }




        void ExecuteStoreProcedure4(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            this.radGridView4.IsBusy = true;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted4);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }

        void ws_ExecuteStoreProcedureCompleted4(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });

            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView4.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }
                    radGridView4.ItemsSource = list;
                    dataObjectList4 = new ObservableCollection<DataObject>();
                    foreach (var item in list)
                    {
                        DataObject dataO = item as DataObject;
                        dataObjectList4.Add(dataO);
                    }


                    if(dataObjectList4.Count> 0)
                    radGridView4.ItemsSource = dataObjectList4;
                  

                    foreach (var item in desc)
                    {
                        radGridView4.FilterDescriptors.Add(item);
                    }
                }
            }
            this.radGridView4.IsBusy = false;
        }


        void ExecuteStoreProcedure5(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            this.radGridView5.IsBusy = true;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted5);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }
        ObservableCollection<GraphColumn> graphColumnList = new ObservableCollection<GraphColumn>();
        void ws_ExecuteStoreProcedureCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });

            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {
                    //  radGridView5.AutoGenerateColumns = true;
                    radGridView5.Columns.Clear();
                    //List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();
                   graphColumnList = new ObservableCollection<GraphColumn>();


                    List<DataObject> dataList = new List<DataObject>();
                    foreach (var item in list)
                    {
                        DataObject data = item as DataObject;

                        dataList.Add(data);
                    }

                    if (dataList.Count > 0)
                    {
                        var obj = dataList.FirstOrDefault();

                        Type t = obj.GetType();
                        PropertyInfo[] props = t.GetProperties();
                        int count = 0;
                        foreach (PropertyInfo prp in props)
                        {
                            count += 1;
                            try
                            {

                                var name = prp.Name;
                                if (name == "State")
                                {




                                }
                                else
                                {
                                    GridViewDataColumn column = new GridViewDataColumn();
                                    //  DateTimePickerColumn.
                                    column.DataMemberBinding = new Binding(name);
                                    column.Header = name;
                                    column.UniqueName = name;
                                    column.IsFilteringDeferred = true;
                                    column.IsReadOnly = true;
                                    if (prp.PropertyType.ToString() == "System.DateTime")
                                    {
                                        column.DataFormatString = "{0:dd-MMM-yyyy}";
                                    }

                                    //if (field.DataType == "date")
                                    //{
                                    //    column.DataFormatString = "{0:dd-MMM-yyyy}";
                                    //}
                                    if (count < 4)
                                    {
                                        column.IsGroupable = false;
                                    }

                                    /// if (!field.Hidden.Value)
                                    this.radGridView5.Columns.Add(column);
                                }
                            }

                            catch
                            {
                            }
                        }
                        radGridView5.AutoGenerateColumns = false;
                      //  radGridView5.CanUserFreezeColumns = true;
                        radGridView5.FrozenColumnCount = 3;
                        // currentGrid.ItemsSource = dataList;


                        radGridView5.ItemsSource = dataList;
                    }
                    else
                    {
                        radGridView5.ItemsSource = list;
                    }
                   
                    ////foreach (var item in desc)
                    ////{
                    ////    radGridView5.FilterDescriptors.Add(item);
                    ////}

                    int i = 0;
                    int currIndex = 1;
                    bool forFirst = true;
                    string currentgroup = "";
                    bool isFirst = true;
                    AllColorsHelper app = new AllColorsHelper();
                    List<Color> colors = app.PredefinedColors;
                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView5.Columns)
                    {
                        if (column.UniqueName == "State")
                        {
                            column.IsVisible = false;
                        }
                        i += 1;

                        column.TextAlignment = TextAlignment.Center;

                        char[] sep = { '_' };
                        string toList = column.UniqueName;
                        string[] toArray = toList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                        if (i < 4)
                        {
                           // column.IsFrozen = true;
                        }


                        if (i > 3)
                        {
                            if (isFirst)
                            {
                                currentgroup = toArray[0].ToString();
                                string displayHeader = currentgroup;
                                displayHeader = System.Text.RegularExpressions.Regex.Replace(displayHeader, @"\B([A-Z])", " $1");
                                displayHeader = displayHeader.Replace("_", " ");

                                radGridView5.ColumnGroups.Add(new GridViewColumnGroup { Name = currentgroup, Header = displayHeader });
                            }
                            string current = toArray[0].ToString();
                            string currHeader = column.UniqueName;
                            try
                            {
                                currHeader = toArray[1].ToString();
                                currHeader = System.Text.RegularExpressions.Regex.Replace(currHeader, @"\B([A-Z])", " $1");
                            }
                            catch
                            {
                            }
                            if (currentgroup == current)
                            {

                                column.ColumnGroupName = currentgroup;
                                column.Header = currHeader;
                                Color color = colors.Where(c => c.Index == currIndex).FirstOrDefault();
                                column.Background = new SolidColorBrush(BMA.MiddlewareApp.ColorFromString.ToColor(color.ColorName));
                            }
                            else
                            {
                                currIndex += 1;
                                currentgroup = toArray[0].ToString();
                                string displayHeader = currentgroup;
                                displayHeader = System.Text.RegularExpressions.Regex.Replace(displayHeader, @"\B([A-Z])", " $1");
                                displayHeader = displayHeader.Replace("_", " ");
                                radGridView5.ColumnGroups.Add(new GridViewColumnGroup { Name = currentgroup, Header = displayHeader });
                                column.ColumnGroupName = currentgroup;
                                column.Header = currHeader;
                                Color color = colors.Where(c => c.Index == currIndex).FirstOrDefault();
                                column.Background = new SolidColorBrush(BMA.MiddlewareApp.ColorFromString.ToColor(color.ColorName));
                            }

                            graphColumnList.Add(new GraphColumn { index = 1, ColumnName = toList, DisplayName = currHeader, GraphType = "", GroupName = currentgroup, Show = false });

                            currentgroup = toArray[0].ToString();
                            isFirst = false;
                           


                            //if (column.UniqueName.Contains("Result"))
                            //{
                            //    column.Background =new SolidColorBrush(BMA.MiddlewareApp.ColorFromString.ToColor("SteelBlue"));
                            //}
                        }
                        else
                        {
                            if (forFirst)
                            {
                                radGridView5.ColumnGroups.Add(new GridViewColumnGroup { Name = "TimeSeries", Header = "Times Series" });
                            }
                            column.ColumnGroupName = "TimeSeries";
                            forFirst = false;
                        }
                    }

                    gridMTP.ItemsSource = graphColumnList;
                    graphData = new List<DataObject>();
                    //  graphMTP.ItemsSource = null;
                    foreach (var item in list)
                    {
                        DataObject dataitem = item as DataObject;
                        graphData.Add(dataitem);
                    }
                    DrawGraph();
                    ConfigDraw();
                    // GenerateGraph();
                }
            }
            // radGridView5.AutoGenerateColumns = false;
            radGridView5.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            //  radGridView5.HeaderRowStyle = 
            radGridView5.IsReadOnly = true;
           radGridView5.CanUserFreezeColumns = true;
            radGridView5.FrozenColumnCount = 3;
         //   LoadConfigSetting5();
            SetSetting5();
            this.radGridView5.IsBusy = false;

            Attach5();
        }

        private void SetSetting5()
        {
          UserGridConfig gridConfig =  ddlConfig5.SelectedItem as UserGridConfig;
            if (isFirstLoad5)
            {

                primarySettings5 = new RadGridViewSettings(this.radGridView5);
                primarySettings5.SaveState();
                primaryXml5 = primarySettings5.SaveState();
                isFirstLoad5 = false;
            }




            if (gridConfig != null)
            {
                // ddlConfig5.SelectedValue = setting.ID;
                // apply setting to the gridview
                Setting setting = gridConfig.Setting;
                try
                {

                    ClearFilters5();
                    //  primarySettings.LoadOriginalState();
                    primarySettings5.ResetState();

                    settings5 = new RadGridViewSettings(this.radGridView5);
                    string xml = setting.Settings;
                    xmlCache5 = xml;
                    settings5.LoadState(xml);

                    //createSqlWithNewFilters();




                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }

        List<DataObject> graphData = new List<DataObject>();

        //private void GenerateGraph()
        //{
        //    bool isDataValiable = false;
        //    graphMTP.SeriesMappings.Clear();
        //    foreach (var item in gridMTP.Items)
        //    {
        //        GraphColumn column = item as GraphColumn;
        //        if (column != null)
        //        {

        //            if (column.Show)
        //            {

        //                switch (column.GraphType)
        //                {
        //                    case "Line":
        //                        SeriesMapping lineSeriesMapping = new SeriesMapping();
        //                        lineSeriesMapping.LegendLabel = column.GroupName + "" + column.DisplayName;
        //                        lineSeriesMapping.SeriesDefinition = new LineSeriesDefinition() { ShowPointMarks = false, ShowItemLabels = false };
        //                        //lineSeriesMapping.SeriesDefinition.Appearance.Fill = Resources["Yellow1"] as LinearGradientBrush;
        //                        // lineSeriesMapping.SeriesDefinition.Appearance.Stroke = Resources["Yellow2"] as SolidColorBrush;
        //                        lineSeriesMapping.ItemMappings.Add(new ItemMapping("PeriodStartDate", DataPointMember.XCategory));
        //                        lineSeriesMapping.ItemMappings.Add(new ItemMapping(column.ColumnName, DataPointMember.YValue));

        //                        graphMTP.SeriesMappings.Add(lineSeriesMapping);

        //                        isDataValiable = true;

        //                        break;

        //                    case "Area":
        //                        SeriesMapping areaSeriesMapping = new SeriesMapping();
        //                        areaSeriesMapping.LegendLabel = column.GroupName + "" + column.DisplayName;
        //                        areaSeriesMapping.SeriesDefinition = new AreaSeriesDefinition() { ShowPointMarks = false, ShowItemLabels = false };

        //                        // areaSeriesMapping.SeriesDefinition.Appearance.Fill = Resources["SkyBlue1"] as LinearGradientBrush;
        //                        // areaSeriesMapping.SeriesDefinition.Appearance.Stroke = Resources["SkyBlue2"] as SolidColorBrush;
        //                        areaSeriesMapping.ItemMappings.Add(new ItemMapping("PeriodStartDate", DataPointMember.XCategory));
        //                        areaSeriesMapping.ItemMappings.Add(new ItemMapping(column.ColumnName, DataPointMember.YValue));
        //                        graphMTP.SeriesMappings.Add(areaSeriesMapping);
        //                        isDataValiable = true;
        //                        break;
        //                    case "Bar":

        //                        SeriesMapping barSeriesMapping = new SeriesMapping();
        //                        barSeriesMapping.LegendLabel = column.GroupName + "" + column.DisplayName;
        //                        barSeriesMapping.SeriesDefinition = new BarSeriesDefinition() { ShowItemLabels = false };
        //                        //    barSeriesMapping.SeriesDefinition.Appearance.Fill = Resources["Gray1"] as LinearGradientBrush;
        //                        //  barSeriesMapping.SeriesDefinition.Appearance.Stroke = Resources["Gray2"] as SolidColorBrush;
        //                        barSeriesMapping.ItemMappings.Add(new ItemMapping("PeriodStartDate", DataPointMember.XCategory));
        //                        barSeriesMapping.ItemMappings.Add(new ItemMapping(column.ColumnName, DataPointMember.YValue));
        //                        graphMTP.SeriesMappings.Add(barSeriesMapping);
        //                        isDataValiable = true;
        //                        break;
        //                }
        //            }
        //        }

        //    }


        //    graphMTP.DefaultView.ChartArea.AxisX.LabelRotationAngle = -45;
        //    graphMTP.DefaultView.ChartArea.AxisX.DefaultLabelFormat = "dd-MMM";
        //    if (isDataValiable)
        //    {

        //        graphMTP.ItemsSource = graphData;
        //    }

        //}


        private SerialChart createChartBefore()
        {
            bool isDataValiable = false;

            var chart = new SerialChart() { LegendVisibility = System.Windows.Visibility.Visible };
            chart.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            chart.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            chart.CategoryValueMemberPath = "TimePeriod";
            chart.Height = graphHeight;
            //  chart.Background = Resources["Charcoal1"] as GradientBrush;
            // chart.AxisForeground = Resources["Charcoal1"] as SolidColorBrush;

            chart.Name = "graphMTP";
            chart.GridStroke = Resources["Navy2"] as SolidColorBrush;
            chart.AxisForeground = Resources["Navy2"] as SolidColorBrush;
            chart.Foreground = Resources["Navy2"] as SolidColorBrush;
            // chart.PlotAreaBackground="Transparent"

            //var graphMovement = new ColumnGraph();
            //graphMovement.ValueMemberPath = noteType + "Movement";
            //graphMovement.Title =  "Movement";


            foreach (var item in gridMTP.Items)
            {
                GraphColumn column = item as GraphColumn;
                if (column != null)
                {

                    if (column.Show)
                    {

                        switch (column.GraphType)
                        {
                            case "Line":



                                var graphLine = new LineGraph();
                                graphLine.ValueMemberPath = column.ColumnName;

                                graphLine.Title = column.GroupName + " " + column.DisplayName;
                                graphLine.StrokeThickness = 2;
                                chart.Graphs.Add(graphLine);

                                isDataValiable = true;
                                break;

                            case "Area":

                                var graphArea = new AreaGraph();
                                graphArea.Title = column.GroupName + " " + column.DisplayName;
                                graphArea.ValueMemberPath = column.ColumnName;
                                chart.Graphs.Add(graphArea);
                                isDataValiable = true;
                                break;
                            case "Bar":

                                var graphBar = new ColumnGraph();
                                graphBar.ValueMemberPath = column.ColumnName;

                                graphBar.Title = column.GroupName + " " + column.DisplayName;
                                chart.Graphs.Add(graphBar);
                                isDataValiable = true;
                                break;
                        }
                    }
                }

            }


            if (isDataValiable)
            {
                chart.DataSource = graphData;
            }
            else
            {
                chart.DataSource = null;
            }
            return chart;
        }


        void DrawGraph()
        {
            gPanel.Children.Clear();

            TextBlock txtBlock = new TextBlock() { Text = " MTP Results  ", FontSize = 15, FontWeight = FontWeights.Bold };
            gPanel.Children.Add(txtBlock);
            gPanel.Children.Add(createChartBefore());


            // TextBlock space = new TextBlock() { Height = 40 };
            ///  stackPanel1.Children.Add(space);

        }
        void btnApplyClick(object sender, RoutedEventArgs e)
        {
            DrawGraph();
        }

        void ExecuteStoreProcedure3(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        {
            this.radGridView3.IsBusy = true;
            var ws = WCF.GetService();
            ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted3);
            ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

            ///.Progress.Start();
        }

        ObservableCollection<DataObject> dataObjectList3 = new ObservableCollection<DataObject>();
        List<DataObject> dataList3 = new List<DataObject>();
        ObservableCollection<DataObject> dataObjectList = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> dataObjectList2 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> dataObjectList4 = new ObservableCollection<DataObject>();
        ObservableCollection<DataObject> dataObjectList5 = new ObservableCollection<DataObject>();
        void ws_ExecuteStoreProcedureCompleted3(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });

            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView3.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }
                    radGridView3.ItemsSource = list;
                    dataObjectList3 = new ObservableCollection<DataObject>();
                    dataList3 = new List<DataObject>();
                    foreach (var item in list)
                    {
                        DataObject dataO = item as DataObject;
                        dataObjectList3.Add(dataO);
                        dataList3.Add(dataO);
                    }

                    if(dataObjectList3.Count > 0)
                    radGridView3.ItemsSource = dataObjectList3;


                    foreach (var item in desc)
                    {
                        radGridView3.FilterDescriptors.Add(item);
                    }
                }
            }
            this.radGridView3.IsBusy = false;
        }



        //void ExecuteStoreProcedure5(string conn, string db, string procName, ObservableCollection<string> parameters, int pageNumber, int pageSize)
        //{
        //    this.radGridView5.IsBusy = true;
        //    var ws = WCF.GetService();
        //    ws.ExecuteStoreProcCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs>(ws_ExecuteStoreProcedureCompleted5);
        //    ws.ExecuteStoreProcAsync(conn, db, procName, parameters, pageNumber, pageSize);

        //    ///.Progress.Start();
        //}

        //void ws_ExecuteStoreProcedureCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.ExecuteStoreProcCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //        Message.ErrorMessage(e.Error.Message);
        //    else if (e.ServiceError != null)
        //        Message.ErrorMessage(e.ServiceError.Message);

        //    else
        //    {
        //        _tables = e.Result.Tables;
        //        IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
        //        if (e.UserState as string == "Lookup")
        //            _lookup = list;
        //        //radGridView1.ItemsSource = list;
        //        else
        //        {


        //            List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

        //            foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView5.Columns)
        //            {
        //                Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
        //                if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
        //                {
        //                    desc.Add(filterDescriptors);
        //                }
        //            }

        //            radGridView5.ItemsSource = list;
        //            foreach (var item in desc)
        //            {
        //                radGridView5.FilterDescriptors.Add(item);
        //            }
        //        }
        //    }
        //    this.radGridView5.IsBusy = false;
        //}


        private void GetData(string sql, int pagenumber, int pagesize, object userState)
        {
            this.radGridView1.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView1.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    radGridView1.ItemsSource = list;
                    foreach (var item in desc)
                    {
                        radGridView1.FilterDescriptors.Add(item);
                    }
                    if (canGenerateColumns)
                    {
                        GetExendedPropertiesData(connString, ExtendedPropertySQL.ExtendedPropertiesSQl(databaseName, tableName), 1, 500, null);

                        if (!(singleTable))
                        {
                            btnInactive.Visibility = Visibility.Visible;
                            btnButtons.Visibility = Visibility.Collapsed;

                        }
                        else
                        {
                            btnInactive.Visibility = Visibility.Collapsed;
                            btnButtons.Visibility = Visibility.Visible;
                        }

                    }
                    else
                    {
                        //if (!isClearFilter)
                        //{
                        //  settings = new RadGridViewSettings(this.radGridView1);

                        // settings.LoadState(xmlCache);
                        //}
                    }


                }

            }
            this.radGridView1.IsBusy = false;
        }



        void Update(ObservableCollection<DataObject> objectCollection, string strTablename)
        {

            var ws = WCF.GetService();
            ws.UpdateCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.UpdateCompletedEventArgs>(ws_UpdateCompleted);
            ws.UpdateAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), strTablename);

            ///.Progress.Start();
        }

        void ws_UpdateCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.UpdateCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                editedDataObjectList = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }

        }



        void InsertDataTable(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), strTablename);

            ///.Progress.Start();
        }

        void ws_InsertCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }


        void GetTotalRows(string conn, string db, string procName, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnTotalCompleted);
            ws.TotalProcRowsAsync(conn, procName, db, parameters);

            ///.Progress.Start();
        }
        double totalRows = 0;
        void ws_ReturnTotalCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)
               
                 RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message});

            else
            {
                double total = (double)(e.Result);
                totalRows = total +1;
                lblRows.Text = totalRows.ToString() + " Rows"; ;
                if (total > pageSize)
                {
                    double totalDouble = (double)(total / (double)pageSize);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager.lblTotal.Text = tol.ToString();
                    dataPager.txtNumber.Text = pageNumber.ToString();
                    pageTotal = tol;
                }
                else
                {
                    dataPager.lblTotal.Text = "1";
                    dataPager.txtNumber.Text = pageNumber.ToString();

                }
            }
            //this.Progress.Stop();
        }


        private void SetDefault()
        {
            UserGridConfig config = ddlConfig.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 1);
            }
        }


        void ws_updateUserConfigDefaultCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "New default configurating setting have been set" });
                }
                else
                {
                
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error!, Fail to set new default settings" });
                }
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An Error has occurred while processing your request!" });
              
            }
        }

        private void InsertTable(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.InsertDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs>(ws_InsertDataTableCompleted);
            ws.InsertDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_InsertDataTableCompleted(object sender, EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
               

               

                if (e.Result == - 2627)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Duplicate key fields" });
                   
                }
                else if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot insert NULL value" });
                    
                }
                else if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                    
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been added";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                    PullData();
                }
               
            }
        }

        private void InsertTable2(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.InsertDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs>(ws_InsertDataTableCompleted2);
            ws.InsertDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_InsertDataTableCompleted2(object sender, EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {

                if (e.Result == -2627)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Duplicate key fields" });
                    return;
                }
                else if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot insert NULL value" });
                   
                }
                else if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                    
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been added";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                    PullData2();
                }
            }
        }


        private void InsertTable4(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.InsertDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs>(ws_InsertDataTableCompleted4);
            ws.InsertDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_InsertDataTableCompleted4(object sender, EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList4 = new ObservableCollection<DataObject>();

                if (e.Result == -2627)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Duplicate key fields" });
                    
                }
                else if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot insert NULL value" });
                    
                }
                else if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });

                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been added";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                    PullData4();
                }
            }
        }

        private void InsertTable3(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.InsertDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs>(ws_InsertDataTableCompleted3);
            ws.InsertDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_InsertDataTableCompleted3(object sender, EOInterface.Middleware.DataTableService.InsertDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList3 = new ObservableCollection<DataObject>();

                if (e.Result == -2627)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Duplicate key fields" });
                  
                }

                else  if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot insert NULL value" });
                  
                }
                else if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: "+e.Result.ToString() });
                
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been added";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                    PullData3();
                }
            }
        }
        private void UpdateTable(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.UpdateDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs>(ws_UpdateDataTableCompleted);
            ws.UpdateDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_UpdateDataTableCompleted(object sender, EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                editedDataObjectList = new ObservableCollection<DataObject>();


                if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot update NULL value" });
                 //  return;
                }
               
                else     if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                   // return;
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been updated";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });
                  
                }
            }
        }

        private void DeleteTable(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.DeleteDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs>(ws_DeleteDataTableCompleted);
            ws.DeleteDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_DeleteDataTableCompleted(object sender, EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {

                deletedDataObjectList = new ObservableCollection<DataObject>();
                if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                  
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been deleted";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                }

               
            }
        }


        private void DeleteTable2(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.DeleteDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs>(ws_DeleteDataTableCompleted2);
            ws.DeleteDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_DeleteDataTableCompleted2(object sender, EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {

                deletedDataObjectList2 = new ObservableCollection<DataObject>();
                if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                   
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been deleted";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                }
            }
        }


        private void DeleteTable3(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.DeleteDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs>(ws_DeleteDataTableCompleted3);
            ws.DeleteDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_DeleteDataTableCompleted3(object sender, EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {

                deletedDataObjectList3 = new ObservableCollection<DataObject>();
                if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                   
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been deleted";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                }
            }
        }
        private void DeleteTable4(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.DeleteDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs>(ws_DeleteDataTableCompleted);
            ws.DeleteDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_DeleteDataTableCompleted4(object sender, EOInterface.Middleware.DataTableService.DeleteDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {

                deletedDataObjectList4 = new ObservableCollection<DataObject>();
                if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                    
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been deleted";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                }
            }
        }


        private void UpdateTable2(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.UpdateDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs>(ws_UpdateDataTableCompleted2);
            ws.UpdateDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_UpdateDataTableCompleted2(object sender, EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                editedDataObjectList2 = new ObservableCollection<DataObject>();


                if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot update NULL value" });
                    
                }

                else if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                    
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been updated";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                }
            }
        }

        private void UpdateTable4(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.UpdateDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs>(ws_UpdateDataTableCompleted4);
            ws.UpdateDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_UpdateDataTableCompleted4(object sender, EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                editedDataObjectList4 = new ObservableCollection<DataObject>();

                if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot update NULL value" });
                    
                }
                else   if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                   
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been updated";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                }
            }
        }

        private void UpdateTable3(string conn, string db, string procName, ObservableCollection<DataObject> newData, ObservableCollection<string> paramemeters)
        {
            var ws = WCF.GetService();
            ws.UpdateDataTableCompleted += new EventHandler<EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs>(ws_UpdateDataTableCompleted3);
            ws.UpdateDataTableAsync(conn, db, procName, DynamicDataBuilder.GetUpdatedDataSet(newData as IEnumerable, _tables), paramemeters);
        }

        void ws_UpdateDataTableCompleted3(object sender, EOInterface.Middleware.DataTableService.UpdateDataTableCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                editedDataObjectList3 = new ObservableCollection<DataObject>();

                if (e.Result == -515)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error : Cannot update NULL value" });
                    
                }
                else if (e.Result < 0)
                {
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = " An Sql error had occured, Error: " + e.Result.ToString() });
                   
                }
                else
                {
                    string alertText = e.Result.ToString() + " rows have been updated";
                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = alertText });

                }
            }
        }

        private void GetColumnData(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView1.Columns[dropDownName]).ItemsSource = list;

                    if (fieldIndex < indexMax)
                    {
                        LoadDropDowns();
                    }


                }

            }

        }





        private void GetExendedPropertiesData(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            // var ws = WCF.GetService();
            //   ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetExendedPropertiesDataCompleted);
            //  ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetExendedPropertiesDataCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    columnNames = new List<string>();
                    foreach (DataObject dataObject in list)
                    {

                        propertiesList.Add(dataObject);
                        string column = dataObject.GetFieldValue("ColumnName").ToString();
                        columnNames.Add(column);
                    }




                }


            }
            //   GenerateColumns();

            // LoadConfigSetting();

            LoadExtendedProperties();

        }

        void DeleteDataTable(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.Error.Message });
            else if (e.ServiceError != null)
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = e.ServiceError.Message });
            else
            {
                newDataObjectList = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }

        #endregion



        #region GridView Events
        private void radGridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e)
        {

        }

        private void radGridView_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList.Contains(dataObject))
                        {
                            if (newDataObjectList.Contains(dataObject))
                            {
                                this.newDataObjectList.Remove(dataObject);
                                this.newDataObjectList.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList.Remove(dataObject);
                            this.editedDataObjectList.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void radGridView_Filtered(object sender, GridViewFilteredEventArgs e)
        {

            PullData();
        }

        void gridView_Deleted(object sender, GridViewDeletedEventArgs e)
        {
            DataObject dataObject = e.Items as DataObject;
            deletedDataObjectList.Add(dataObject);



        }


        private void gridView_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e)
        {
            //RadComboBox countries = e.DetailsElement.FindName("rcbCountries") as RadComboBox;
            //countries.ItemsSource = GetCountries();
            // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }
        #endregion


        #region GridMenu Events & Methods
        public MouseButtonEventHandler gridRightClick { get; set; }

        private void radGridView1_Loaded(object sender, GridViewRowItemEventArgs e)
        {
            e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }

        private void radGridView1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            stack1_MouseRightButtonUp(sender, e);
        }

        protected void btnButtons_ImportClick(object sender, EventArgs e)
        {

            Import("");

        }

        protected void btnButtons_ImportClick2(object sender, EventArgs e)
        {

            Import2("");

        }


        protected void btnButtons_ImportClick3(object sender, EventArgs e)
        {

            Import3("");

        }


        protected void btnButtons_ImportClick4(object sender, EventArgs e)
        {

            Import4("");

        }
 


        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            // tab_MouseRightButtonUp(sender, e);
            tab_MouseRightButtonUp(sender, e);
            cMenu.StaysOpen = true;
        }
        RadContextMenu cMenu;
        RadTabItem selectedTab;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var element = sender as UIElement;


            cMenu = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Changes";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Add";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Selected Rows";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
            exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "ExcelML";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Word";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Csv";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
            menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings As";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Set Current Filter Settings as Defualt";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.PlacementTarget = radGridView1;

            Point p = e.GetPosition(this.radGridView1);


            cMenu.Placement = PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu.HorizontalOffset = p.X - 10;
            cMenu.VerticalOffset = p.Y + 50;


            cMenu.IsOpen = true;
            // cMenu.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            //  RadContextMenu.SetContextMenu(this.radGridView1, cMenu);
            //cMenu.IsOpen = true;
            //  cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X + 350;
            //  cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }
        void RadGridView1_RowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {

                //  ((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                //  ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);

            }
        }


        void menuItem_Click(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges();
                        break;
                    case "Add":
                        AddNewItem();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault();
                        break;



                    case "Import":
                        Import(header);
                        break;

                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }

            }
        }

        private void RadContextMenu_ItemClick(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges();
                        break;
                    case "Add New":
                        radGridView1.BeginInsert();
                        break;
                    case "Edit":
                        radGridView1.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows();
                        break;

                    case "Save Filter Settings":
                        SaveConfig();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault();
                        break;


                    case "Import":
                        Import(header);
                        break;

                    case "Excel":
                        Export(header);
                        break;

                    case "Word":
                        Export(header);
                        break;

                    case "ExcelML":
                        Export(header);
                        break;
                    case "Csv":
                        Export(header);
                        break;
                    case "Cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }
            }
            // cMenu.IsOpen = false;
        }

        private void Export(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView1.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }


        private void RadContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion
        #region MyRegion

        private void Attach()
        {
            if (radGridView1 != null)
            {

                // create menu
                RadContextMenu contextMenu = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu, StyleManager.GetTheme(radGridView1));

                contextMenu.Opened += OnMenuOpened;
                contextMenu.ItemClick += OnMenuItemClick;

                RadContextMenu.SetContextMenu(radGridView1, contextMenu);
            }
        }
        void OnMenuOpened(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);

                // create menu items
                foreach (GridViewColumn column in radGridView1.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.Header;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Changes";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Add";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Edit";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Selected Rows";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

              //  menuItem = new RadMenuItem();
              //  menuItem.Header = "Copy";
              //  menuItem.Command =System.Windows.Input.ApplicationCommands
              //  menu.Items.Add(menuItem);

              //  menuItem = new RadMenuItem();
              //  menuItem.Header = "Cut";
              ////  menuItem.Command = "ApplicationCommands.Copy";
              //  menu.Items.Add(menuItem);
                menuItem = new RadMenuItem();
                menuItem.Header = "Import";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }

        void OnMenuItemClick(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView1.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView1.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView1.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView1.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView1.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView1.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView1.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView1.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView1.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }


        private void Import(string extension)
        {
            ImportWizard view = new ImportWizard(tableName, ModelID, fieldList.ToList(), viewID, serverID, CreateSPID, false);
            //view.Closed += view_Closed;// +=new RadEventHandler(view_Closed);
            //view.WindowStartupLocation
            // view.Owner = 
            view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
            view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
            view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            view.ShowDialog();

        }
        #endregion

        #endregion



        #region GridTwo



        #region Paging
        void dataPager_First_Click2(object sender, EventArgs e)
        {
            isTextBox2 = false;
            int newPageNumber = 1;
            ReloadPage2(newPageNumber);
        }

        private void Paging2()
        {
            dataPager2.txtNumber.Text = pageNumber.ToString();
            dataPager2.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox2 = true;
        void dataPager_Number_TextChanged2(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox2)
                {
                    int newPageNumber = Convert.ToInt32(dataPager2.txtNumber.Text);
                    ReloadPage2(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox2 = true;
        }
        private void ReloadPage2(int newPageNumber)
        {
            curPage2 = newPageNumber;
            if ((newDataObjectList2.Count > 0) || (editedDataObjectList2.Count > 0) || (deletedDataObjectList2.Count > 0))
            {
                string confirmText = "This page contains some changes, Do you want to save the changes?";
                RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed2));

                

             

            }
            else
            {

                if ((newPageNumber > pageTotal2) || (newPageNumber < 1))
                {
                    dataPager2.txtNumber.Text = pageNumber2.ToString();
                    isTextBox2 = false;
                }
                else
                {
                    pageNumber2 = newPageNumber;
                    dataPager2.txtNumber.Text = pageNumber2.ToString();
                    PullData2();
                }
            }




        }

          int curPage2 = 1;

          private void OnConfirmClosed2(object sender, WindowClosedEventArgs e)
          {
              int newPageNumber = curPage2;
              if (e.DialogResult == true)
              {
                  SaveChanges2();

                  if ((newPageNumber > pageTotal2) || (newPageNumber < 1))
                  {
                      dataPager2.txtNumber.Text = pageNumber2.ToString();
                      isTextBox2 = false;
                  }
                  else
                  {
                      pageNumber2 = newPageNumber;
                      dataPager2.txtNumber.Text = pageNumber.ToString();
                      PullData2();
                      //radGridView1.IsBusy = false;
                  }
              }
              else
              {
                  newDataObjectList2 = new ObservableCollection<DataObject>();
                  editedDataObjectList2 = new ObservableCollection<DataObject>();
                  deletedDataObjectList2 = new ObservableCollection<DataObject>();
                  if ((newPageNumber > pageTotal2) || (newPageNumber < 1))
                  {
                      dataPager2.txtNumber.Text = pageNumber2.ToString();
                      isTextBox2 = false;
                  }
                  else
                  {
                      pageNumber2 = newPageNumber;
                      dataPager2.txtNumber.Text = pageNumber2.ToString();
                      PullData2();
                      //radGridView1.IsBusy = false;

                  }
              }
          }

        void dataPager_Next_Click2(object sender, EventArgs e)
        {
            if (pageNumber2 < pageSize2)
            {
                isTextBox2 = false;
                int newPageNumber = pageNumber2 + 1;
                ReloadPage2(newPageNumber);
            }

        }

        void dataPager_Last_Click2(object sender, EventArgs e)
        {
            isTextBox2 = false;
            int newPageNumber = pageTotal2;
            ReloadPage2(newPageNumber);
        }

        void dataPager_Back_Click2(object sender, EventArgs e)
        {
            isTextBox2 = false;
            int newPageNumber = pageNumber2 - 1;
            ReloadPage2(newPageNumber);

        }

        #endregion



        #region Paging 4
        void dataPager_First_Click4(object sender, EventArgs e)
        {
            isTextBox4 = false;
            int newPageNumber = 1;
            ReloadPage4(newPageNumber);
        }

        private void Paging4()
        {
            dataPager4.txtNumber.Text = pageNumber.ToString();
            dataPager4.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox4 = true;
        void dataPager_Number_TextChanged4(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox4)
                {
                    int newPageNumber = Convert.ToInt32(dataPager4.txtNumber.Text);
                    ReloadPage4(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox4 = true;
        }
        private void ReloadPage4(int newPageNumber)
        {
            curPage4 = newPageNumber;
            if ((newDataObjectList4.Count > 0) || (editedDataObjectList4.Count > 0) || (deletedDataObjectList4.Count > 0))
            {
                string confirmText = "This page contains some changes, Do you want to save the changes?";
                RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed4));

               
            }
            else
            {

                if ((newPageNumber > pageTotal4) || (newPageNumber < 1))
                {
                    dataPager4.txtNumber.Text = pageNumber4.ToString();
                    isTextBox4 = false;
                }
                else
                {
                    pageNumber4 = newPageNumber;
                    dataPager4.txtNumber.Text = pageNumber4.ToString();
                    PullData4();
                }
            }




        }

        int curPage4 = 1;
        private void OnConfirmClosed4(object sender, WindowClosedEventArgs e)
        {
            int newPageNumber = curPage4;
            if (e.DialogResult == true)
            {
                SaveChanges4();

                if ((newPageNumber > pageTotal4) || (newPageNumber < 1))
                {
                    dataPager4.txtNumber.Text = pageNumber4.ToString();
                    isTextBox4 = false;
                }
                else
                {
                    pageNumber4 = newPageNumber;
                    dataPager4.txtNumber.Text = pageNumber.ToString();
                    PullData4();
                    //radGridView1.IsBusy = false;
                }
            }
            else
            {
                newDataObjectList4 = new ObservableCollection<DataObject>();
                editedDataObjectList4 = new ObservableCollection<DataObject>();
                deletedDataObjectList4 = new ObservableCollection<DataObject>();
                if ((newPageNumber > pageTotal4) || (newPageNumber < 1))
                {
                    dataPager4.txtNumber.Text = pageNumber4.ToString();
                    isTextBox4 = false;
                }
                else
                {
                    pageNumber4 = newPageNumber;
                    dataPager4.txtNumber.Text = pageNumber4.ToString();
                    PullData4();
                    //radGridView1.IsBusy = false;

                }
            }
        }

        void dataPager_Next_Click4(object sender, EventArgs e)
        {
            if (pageNumber4 < pageSize4)
            {
                isTextBox4 = false;
                int newPageNumber = pageNumber4 + 1;
                ReloadPage4(newPageNumber);
            }

        }

        void dataPager_Last_Click4(object sender, EventArgs e)
        {
            isTextBox4 = false;
            int newPageNumber = pageTotal4;
            ReloadPage4(newPageNumber);
        }

        void dataPager_Back_Click4(object sender, EventArgs e)
        {
            isTextBox4 = false;
            int newPageNumber = pageNumber4 - 1;
            ReloadPage4(newPageNumber);

        }

        #endregion



        #region Paging 5
        void dataPager_First_Click5(object sender, EventArgs e)
        {
            isTextBox5 = false;
            int newPageNumber = 1;
            ReloadPage5(newPageNumber);
        }

        private void Paging5()
        {
            dataPager5.txtNumber.Text = pageNumber.ToString();
            dataPager5.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox5 = true;
        void dataPager_Number_TextChanged5(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox5)
                {
                    int newPageNumber = Convert.ToInt32(dataPager5.txtNumber.Text);
                    ReloadPage5(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox5 = true;
        }
        private void ReloadPage5(int newPageNumber)
        {
            if ((newDataObjectList5.Count > 0) || (editedDataObjectList5.Count > 0) || (deletedDataObjectList5.Count > 0))
            {
                CustomMessage customMessage = new CustomMessage("This page contains some changes, Do you want to save the changes?", CustomMessage.MessageType.Confirm);

                customMessage.OKButton.Click += (obj, args) =>
                {
                    SaveChanges5();

                    if ((newPageNumber > pageTotal5) || (newPageNumber < 1))
                    {
                        dataPager5.txtNumber.Text = pageNumber5.ToString();
                        isTextBox5 = false;
                    }
                    else
                    {
                        pageNumber5 = newPageNumber;
                        dataPager5.txtNumber.Text = pageNumber.ToString();
                        PullData5();
                        //radGridView1.IsBusy = false;
                    }
                };

                customMessage.CancelButton.Click += (obj, args) =>
                {

                    newDataObjectList5 = new ObservableCollection<DataObject>();
                    editedDataObjectList5 = new ObservableCollection<DataObject>();
                    deletedDataObjectList5 = new ObservableCollection<DataObject>();
                    if ((newPageNumber > pageTotal5) || (newPageNumber < 1))
                    {
                        dataPager5.txtNumber.Text = pageNumber5.ToString();
                        isTextBox5 = false;
                    }
                    else
                    {
                        pageNumber5 = newPageNumber;
                        dataPager5.txtNumber.Text = pageNumber5.ToString();
                        PullData5();
                        //radGridView1.IsBusy = false;

                    }
                };


                customMessage.Show();
            }
            else
            {

                if ((newPageNumber > pageTotal5) || (newPageNumber < 1))
                {
                    dataPager5.txtNumber.Text = pageNumber5.ToString();
                    isTextBox5 = false;
                }
                else
                {
                    pageNumber5 = newPageNumber;
                    dataPager5.txtNumber.Text = pageNumber5.ToString();
                    PullData5();
                }
            }




        }

        void dataPager_Next_Click5(object sender, EventArgs e)
        {
            if (pageNumber5 < pageSize5)
            {
                isTextBox5 = false;
                int newPageNumber = pageNumber5 + 1;
                ReloadPage5(newPageNumber);
            }

        }

        void dataPager_Last_Click5(object sender, EventArgs e)
        {
            isTextBox5 = false;
            int newPageNumber = pageTotal5;
            ReloadPage5(newPageNumber);
        }

        void dataPager_Back_Click5(object sender, EventArgs e)
        {
            isTextBox5 = false;
            int newPageNumber = pageNumber5 - 1;
            ReloadPage5(newPageNumber);

        }

        #endregion


        #region Paging
        void dataPager_First_Click3(object sender, EventArgs e)
        {
            isTextBox3 = false;
            int newPageNumber = 1;
            ReloadPage3(newPageNumber);
        }

        private void Paging3()
        {
            dataPager3.txtNumber.Text = pageNumber.ToString();
            dataPager3.lblTotal.Text = pageTotal.ToString();
        }

        private bool isTextBox3 = true;
        void dataPager_Number_TextChanged3(object sender, EventArgs e)
        {
            try
            {
                if (isTextBox3)
                {
                    int newPageNumber = Convert.ToInt32(dataPager3.txtNumber.Text);
                    ReloadPage3(newPageNumber);
                }
            }
            catch
            {

            }
            isTextBox3 = true;
        }
        private void ReloadPage3(int newPageNumber)
        {
            curPage3 = newPageNumber;
            if ((newDataObjectList3.Count > 0) || (editedDataObjectList3.Count > 0) || (deletedDataObjectList3.Count > 0))
            {
                string confirmText = "This page contains some changes, Do you want to save the changes?";
                RadWindow.Confirm(confirmText, new EventHandler<WindowClosedEventArgs>(OnConfirmClosed3));

               
            }
            else
            {

                if ((newPageNumber > pageTotal3) || (newPageNumber < 1))
                {
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    isTextBox3 = false;
                }
                else
                {
                    pageNumber3 = newPageNumber;
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    PullData3();
                }
            }




        }
        int curPage3 = 1;

        private void OnConfirmClosed3(object sender, WindowClosedEventArgs e)
        {
            int newPageNumber = curPage3;
            if (e.DialogResult == true)
            {
                SaveChanges3();

                if ((newPageNumber > pageTotal3) || (newPageNumber < 1))
                {
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    isTextBox3 = false;
                }
                else
                {
                    pageNumber3 = newPageNumber;
                    dataPager3.txtNumber.Text = pageNumber.ToString();
                    PullData3();
                }
            }
            else
            {

                newDataObjectList3 = new ObservableCollection<DataObject>();
                editedDataObjectList3 = new ObservableCollection<DataObject>();
                deletedDataObjectList3 = new ObservableCollection<DataObject>();
                if ((newPageNumber > pageTotal3) || (newPageNumber < 1))
                {
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    isTextBox3 = false;
                }
                else
                {
                    pageNumber3 = newPageNumber;
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    PullData3();

                }
            }
        }
        void dataPager_Next_Click3(object sender, EventArgs e)
        {
            if (pageNumber3 < pageSize2)
            {
                isTextBox3 = false;
                int newPageNumber = pageNumber3 + 1;
                ReloadPage3(newPageNumber);
            }

        }

        void dataPager_Last_Click3(object sender, EventArgs e)
        {
            isTextBox3 = false;
            int newPageNumber = pageTotal3;
            ReloadPage3(newPageNumber);
        }

        void dataPager_Back_Click3(object sender, EventArgs e)
        {
            isTextBox3 = false;
            int newPageNumber = pageNumber3 - 1;
            ReloadPage3(newPageNumber);

        }

        #endregion

        #region Events
        bool isFirst2 = true;
        private void ddlPageSize_SelectionChanged2(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst2)
            {
                PageNumber page = ddlPageSize2.SelectedItem as PageNumber;
                pageSize2 = page.Size;
                isTextBox2 = false;
                int newPageNumber2 = 1;
                ReCountPage2();
                ReloadPage2(newPageNumber2);
            }
            isFirst2 = false;
        }

        bool isFirst4 = true;
        private void ddlPageSize_SelectionChanged4(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst4)
            {
                PageNumber page = ddlPageSize4.SelectedItem as PageNumber;
                pageSize4 = page.Size;
                // isTextBox4 = false;
                int newPageNumber4 = 1;
                ReCountPage4();//..  
                ReloadPage4(newPageNumber4);

            }
            isFirst4 = false;
        }

        bool isFirst3 = true;
        private void ddlPageSize_SelectionChanged3(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst3)
            {
                PageNumber page = ddlPageSize3.SelectedItem as PageNumber;
                pageSize3 = page.Size;
                // isTextBox4 = false;
                int newPageNumber3 = 1;
                ReCountPage3();//..  
                ReloadPage3(newPageNumber3); ;
            }
            isFirst3 = false;
        }


        bool isFirst5 = true;
        private void ddlPageSize_SelectionChanged5(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst5)
            {
                PageNumber page = ddlPageSize5.SelectedItem as PageNumber;
                pageSize5 = page.Size;
                // isTextBox4 = false;
                int newPageNumber5 = 1;
                //  GetTotalRows4(newSqlText2);
                //..  ReloadPage4(newPageNumber2);
            }
            isFirst5 = false;
        }
        private void btnCascade_Click2(object sender, RoutedEventArgs e)
        {
            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            ReCountPage3();
            ReCountPage2();
            ReCountPage4();
            ReCountPage5();
            PullData2();
            PullData3();
            PullData4();
            PullData5();



        }

        void PullData2()
        {


            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID2), callStoredProcedureNameLoadOperation2, null);



        }

        void ReCountPage2()
        {


            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID2), callRowCountLoadOperation2, null);



        }


        private void btnCascade_Click4(object sender, RoutedEventArgs e)
        {
            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            ReCountPage3();
            ReCountPage2();
            ReCountPage4();
            ReCountPage5();
            PullData4();
            PullData2();
            PullData3();
            
            PullData5();




        }

        private void btnCascade_Click3(object sender, RoutedEventArgs e)
        {


            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            ReCountPage3();
            ReCountPage2();
            ReCountPage4();
            ReCountPage5();
                PullData3();
            PullData2();
           
            PullData4();
            PullData5();

        }
        private void btnCascade_Click5(object sender, RoutedEventArgs e)
        {

            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            ReCountPage3();
            ReCountPage2();
            ReCountPage4();
            ReCountPage5();
            PullData2();
            PullData3();
            PullData4();
          
             PullData5();


        }

        private void btnCascade_Click6(object sender, RoutedEventArgs e)
        {

            ConfigDraw();


        }

        private void ConfigDraw()
        {
            try
            {

                UserGridConfig config = ddlConfig6.SelectedItem as UserGridConfig;
                if (config == null)
                    return;



                if (config.ID > 0)
                {


                    gPanel.Children.Clear();

                    TextBlock txtBlock = new TextBlock() { Text = " MTP Results  ", FontSize = 15, FontWeight = FontWeights.Bold };
                    gPanel.Children.Add(txtBlock);
                    SerialChart seriesChart = (SerialChart)System.Windows.Markup.XamlReader.Load(config.Setting.Settings);

                    gPanel.Children.Add(seriesChart);

                    if (graphData.Count > 0)
                    {
                        seriesChart.DataSource = graphData;
                    }
                    else
                    {
                        seriesChart.DataSource = null;
                    }


                    try
                    {


                        foreach (var item in graphColumnList)
                        {
                            item.Show = false;
                            item.GraphType = "";
                        }
                      //  gridMTP.Columns.Clear();
                        gridMTP.ItemsSource = null;
                        gridMTP.ItemsSource = graphColumnList;
                        foreach (var item in seriesChart.Graphs.OrderBy(g => g.ToString()))
                        {
                            string itemString = item.ToString();
                            string type = itemString;//(item.Template.TargetType).FullName;
                            if (type == "AmCharts.Windows.QuickCharts.LineGraph")
                            {
                                
                                LineGraph lineGraph = item as LineGraph;
                                var datacolumn = graphColumnList.Where(c => c.ColumnName == lineGraph.ValueMemberPath).FirstOrDefault();
                                datacolumn.Show = true;
                                datacolumn.GraphType = "Line";
                                
                            }

                            if (type == "AmCharts.Windows.QuickCharts.AreaGraph")
                            {
                               
                                AreaGraph areaGraph = item as AreaGraph;
                                var datacolumn = graphColumnList.Where(c => c.ColumnName == areaGraph.ValueMemberPath).FirstOrDefault();
                                datacolumn.Show = true;
                                datacolumn.GraphType = "Area";
                             
                            }

                            if (type == "AmCharts.Windows.QuickCharts.ColumnGraph")
                            {
                               
                                ColumnGraph columnGraph = item as ColumnGraph;
                                var datacolumn = graphColumnList.Where(c => c.ColumnName == columnGraph.ValueMemberPath).FirstOrDefault();
                                datacolumn.Show = true;
                                datacolumn.GraphType = "Bar";
                               
                            }
                        }

                    }
                    catch
                    {
                    }

                }
                else
                {


                }
            }

            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }

          
        }
        void radGridView1_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {

            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            int index = this.radGridView1.Items.IndexOf(this.radGridView1.SelectedItem);
            int rowNumber = ((pageNumber - 1) * pageSize) + (index + 1);
            lblRows.Text = rowNumber.ToString() + " Of " +totalRows.ToString() + " Rows";
            //  ReCountPage5();
        }
        double totalRows2 = 0;
        void radGridView2_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {

            DataObject dataObject = radGridView2.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            int index = this.radGridView2.Items.IndexOf(this.radGridView2.SelectedItem);
            int rowNumber = ((pageNumber2 - 1) * pageSize2) + (index + 1);
            lblRows2.Text = rowNumber.ToString() + " Of " + totalRows2.ToString() + " Rows";
            //  ReCountPage5();
        }

        double totalRows3 = 0;
        void radGridView3_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {

            DataObject dataObject = radGridView3.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            int index = this.radGridView3.Items.IndexOf(this.radGridView3.SelectedItem);
            int rowNumber = ((pageNumber3 - 1) * pageSize3) + (index + 1);
            lblRows3.Text = rowNumber.ToString() + " Of " + totalRows3.ToString() + " Rows";
            //  ReCountPage5();
        }


        double totalRows4 = 0;
        void radGridView4_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {

            DataObject dataObject = radGridView4.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            int index = this.radGridView4.Items.IndexOf(this.radGridView4.SelectedItem);
            int rowNumber = ((pageNumber4 - 1) * pageSize4) + (index + 1);
            lblRows4.Text = rowNumber.ToString() + " Of " + totalRows4.ToString() + " Rows";
            //  ReCountPage5();
        }

        double totalRows5 = 0;
        void radGridView5_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {

            DataObject dataObject = radGridView5.SelectedItem as DataObject;
            if (dataObject == null)
                return;
            int index = this.radGridView5.Items.IndexOf(this.radGridView5.SelectedItem);
            int rowNumber = ((pageNumber5 - 1) * pageSize5) + (index + 1);
            lblRows5.Text = rowNumber.ToString() + " Of " + totalRows5.ToString() + " Rows";
            //  ReCountPage5();
        }

        void PullData4()
        {


            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID4), callStoredProcedureNameLoadOperation4, null);

        }
        void ReCountPage4()
        {
            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID4), callProcRowCountLoadOperation4, null);
        }

        void PullData3()
        {


            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID3), callStoredProcedureNameLoadOperation3, null);

        }
        void ReCountPage3()
        {
            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID3), callProcRowCountLoadOperation3, null);
        }

        void PullData5()
        {
            layoutUpdate();


            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID5), callStoredProcedureNameLoadOperation5, null);

        }

        void ReCountPage5()
        {

            LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == ReadSPID5), callProcRowCountLoadOperation5, null);

        }

        void CallbackSettings2(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache2 = xml;
                settings.LoadState(xml);
            }

            PullData2();
        }




        void CallbackSettings4(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache4 = xml;
                settings.LoadState(xml);
            }

            PullData4();
        }



        void CallbackSettings3(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache3 = xml;
                settings.LoadState(xml);
            }

            PullData3();
        }

        void CallbackSettings5(LoadOperation<Setting> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                Setting setting = loadOperation.Entities.FirstOrDefault();
                string xml = setting.Settings;
                xmlCache5 = xml;
                settings.LoadState(xml);
            }

            PullData5();
        }


        bool isFirst = true;
        private void ddlPageSize_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!isFirst)
            {
                PageNumber page = ddlPageSize.SelectedItem as PageNumber;
                pageSize = page.Size;
                isTextBox = false;
                int newPageNumber = 1;
                ReCountPage();
                ReloadPage(newPageNumber);
            }
            isFirst = false;
        }

        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {


            Filter();
        }

        private void Filter()
        {
            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;


            // Get tables for grid two

            GetTables2();

        }

        void LayoutRoot_LayoutUpdated2(object sender, EventArgs e)
        {
            //  radGridView1.MaxHeight = this.stack1.RenderSize.Height;
        }




        void RightClickMenu_Export2(object sender, EventArgs e)
        {
            ExcelExport2();
        }

        void RightClickMenu_AddNew2(object sender, EventArgs e)
        {
            if (singleTable2)
            {
                this.radGridView2.Items.AddNew();
            }
        }

        void RightClickMenu_SetDefault2(object sender, EventArgs e)
        {
            SetDefault2();
        }

        void RightClickMenu_SaveFilter2(object sender, EventArgs e)
        {
            SaveConfig2();
        }

        void RightClickMenu_Delete2(object sender, EventArgs e)
        {
            if (singleTable2)
            {
                DeleteRows2();
            }
        }

        void RightClickMenu_Cancel2(object sender, EventArgs e)
        {
            Cancel2();
        }



        void RightClickMenu_Save2(object sender, EventArgs e)
        {

            SaveChanges2();
        }

        public static void color_TestClick2()
        {
            MessageBox.Show("Red");
        }

        void stack1_MouseRightButtonDown2(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        void stack1_MouseRightButtonUp2(object sender, MouseButtonEventArgs e)
        {
            RightClickContentMenu contextMenu = new RightClickContentMenu();
            //  contextMenu.Show(e.GetPosition(LayoutRoot));
        }


        void dataGridRightClick2(object sender, MouseEventArgs e)
        {
            MessageBox.Show("right");
        }

        protected void btnButtons_ExportClick2(object sender, EventArgs e)
        {

            ExcelExport2();

        }


        protected void btnButtons_ExportClick4(object sender, EventArgs e)
        {

            ExcelExport4();

        }



        protected void btnButtons_ExportClick3(object sender, EventArgs e)
        {

            ExcelExport3();

        }



        protected void btnButtons_ExportClick5(object sender, EventArgs e)
        {

            ExcelExport5();

        }


        private void btnAddNew_Click2(object sender, EventArgs e)
        {
            AddNewItem2();
        }

        private void AddNewItem2()
        {
            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;


            var item = this.radGridView2.Items.AddNew();

            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);



                    }
                }

                catch { }
            }



            DataObject newDataObject = item as DataObject;

            foreach (GridRelationship rship in gridRship)
            {
                //  max = gridRship.Count;
                // curr += 1;
                try
                {

                    ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                    ReadField field2 = fieldList2.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                    //   gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";
                    newDataObject.SetFieldValue(field2.FieldName, parameterValue);

                }
                catch { }
            }

            foreach (var field in fieldList2)
            {
                try
                {
                    if ((field.DefaultValue != null) || (field.DefaultValue != ""))
                    {

                        object obj = field.DefaultValue as object;
                        newDataObject.SetFieldValue(field.FieldName, obj);
                    }
                }
                catch
                {
                }

            }
        }
        private void btnAddNew_Click4(object sender, EventArgs e)
        {
            AddNewItem4();
        }

        private void AddNewItem4()
        {
            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;


            var item = this.radGridView4.Items.AddNew();

            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);



                    }
                }

                catch { }
            }



            DataObject newDataObject = item as DataObject;
            try
            {


                object id = ModelID as object;
                newDataObject.SetFieldValue("Model_ID", ModelID);


            }
            catch { }

            foreach (GridRelationship rship in gridRship)
            {
                //  max = gridRship.Count;
                // curr += 1;
                try
                {

                    ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                    ReadField field4 = fieldList4.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                    //   gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";
                    newDataObject.SetFieldValue(field4.FieldName, parameterValue);

                }
                catch { }
            }

            foreach (var field in fieldList4)
            {
                try
                {
                    if ((field.DefaultValue != null) || (field.DefaultValue != ""))
                    {

                        object obj = field.DefaultValue as object;
                        newDataObject.SetFieldValue(field.FieldName, obj);
                    }
                }
                catch
                {
                }

            }
        }

        private void btnAddNew_Click3(object sender, EventArgs e)
        {
            AddNewItem3();
        }

        private void AddNewItem3()
        {
            System.Globalization.CultureInfo cultureInfo =
    new System.Globalization.CultureInfo("en-ZA");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");

            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;


            var item = this.radGridView3.Items.AddNew();

            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);



                    }
                }

                catch { }
            }



            DataObject newDataObject = item as DataObject;
            try
            {


                object id = ModelID as object;
                newDataObject.SetFieldValue("Model_ID", ModelID);


            }
            catch { }

            foreach (GridRelationship rship in gridRship)
            {
                //  max = gridRship.Count;
                // curr += 1;
                try
                {

                    ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                    ReadField field3 = fieldList3.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                    //   gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";
                    newDataObject.SetFieldValue(field3.FieldName, parameterValue);

                }
                catch { }
            }

            foreach (var field in fieldList3)
            {
                try
                {
                    if ((field.DefaultValue != null) || (field.DefaultValue != ""))
                    {

                        object obj = field.DefaultValue as object;
                        newDataObject.SetFieldValue(field.FieldName, obj);
                    }
                }
                catch
                {
                }

            }
        }
        private void btnAddNew_Click5(object sender, EventArgs e)
        {
            DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;


            var item = this.radGridView5.Items.AddNew();

            Type t = item.GetType();
            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prp in props)
            {

                try
                {

                    var name = prp.Name;
                    if (prp.PropertyType.ToString() == "System.DateTime")
                    {


                        var value = DateTime.Now;

                        prp.SetValue(item, value, null);



                    }
                }

                catch { }
            }



            DataObject newDataObject = item as DataObject;
            try
            {


                object id = ModelID as object;
                newDataObject.SetFieldValue("Model_ID", ModelID);


            }
            catch { }

            foreach (GridRelationship rship in gridRship)
            {
                //  max = gridRship.Count;
                // curr += 1;
                try
                {

                    ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                    ReadField field5 = fieldList5.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                    //   gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";
                    newDataObject.SetFieldValue(field5.FieldName, parameterValue);

                }
                catch { }
            }

            foreach (var field in fieldList4)
            {
                try
                {
                    if ((field.DefaultValue != null) || (field.DefaultValue != ""))
                    {

                        object obj = field.DefaultValue as object;
                        newDataObject.SetFieldValue(field.FieldName, obj);
                    }
                }
                catch
                {
                }

            }
        }
        private void btnSave_Click2(object sender, EventArgs e)
        { //perform

            SaveChanges2();
        }


        private void btnCancel_Click2(object sender, EventArgs e)
        {
            editedDataObjectList2 = new ObservableCollection<DataObject>();
            newDataObjectList2 = new ObservableCollection<DataObject>();
            deletedDataObjectList2 = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click2(object sender, EventArgs e)
        {
            DeleteRows2();

        }



        private void btnSaveConfig_Click2(object sender, EventArgs e)
        {

            SaveConfig2();

        }


        //DeleteConfig


        void ChildWin_Closed2(object sender, EventArgs e)
        {
            LoadConfigSetting2();
        }

        private void btnSetDefault_Click2(object sender, EventArgs e)
        {
            SetDefault2();
        }


        //private void btnSetDefault_Click4(object sender, EventArgs e)
        //{
        //    SetDefault4();

        //}

        bool isClearFilter2 = false;


        private void btnSave_Click4(object sender, EventArgs e)
        { //perform

            SaveChanges4();
        }


        private void btnCancel_Click4(object sender, EventArgs e)
        {
            editedDataObjectList4 = new ObservableCollection<DataObject>();
            newDataObjectList4 = new ObservableCollection<DataObject>();
            deletedDataObjectList4 = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click4(object sender, EventArgs e)
        {
            DeleteRows4();

        }



        private void btnSaveConfig_Click4(object sender, EventArgs e)
        {

            SaveConfig4();

        }


        private void btnSaveConfig_Click5(object sender, EventArgs e)
        {

            SaveConfig5();

        }

        //DeleteConfig


        void ChildWin_Closed5(object sender, EventArgs e)
        {
            LoadConfigSetting5();
        }

        void ChildWin_Closed6(object sender, EventArgs e)
        {
            LoadConfigSetting6();
        }

        void ChildWin_Closed4(object sender, EventArgs e)
        {
            LoadConfigSetting4();
        }
        private void btnSetDefault_Click4(object sender, EventArgs e)
        {
            SetDefault4();

        }

        private void btnSetDefault_Click3(object sender, EventArgs e)
        {
            SetDefault3();

        }

        private void btnSetDefault_Click5(object sender, EventArgs e)
        {
            SetDefault5();

        }

        private void btnSetDefault_Click6(object sender, EventArgs e)
        {
            SetDefault6();

        }


        private void btnSaveConfig_Click6(object sender, EventArgs e)
        {

            SaveConfig6();

        }


        bool isClearFilter4 = false;


        private void btnSave_Click3(object sender, EventArgs e)
        { //perform

            SaveChanges3();
        }


        private void btnCancel_Click3(object sender, EventArgs e)
        {
            editedDataObjectList3 = new ObservableCollection<DataObject>();
            newDataObjectList3 = new ObservableCollection<DataObject>();
            deletedDataObjectList3 = new ObservableCollection<DataObject>();

        }

        private void btnDelete_Click3(object sender, EventArgs e)
        {
            DeleteRows3();

        }



        private void btnSaveConfig_Click3(object sender, EventArgs e)
        {

            SaveConfig3();

        }


        //DeleteConfig


        void ChildWin_Closed3(object sender, EventArgs e)
        {
            LoadConfigSetting3();
        }

        //private void btnSetDefault_Click4(object sender, EventArgs e)
        //{
        //    SetDefault3();

        //}


        bool isClearFilter3 = false;

        bool isClearFilter5 = false;
        private void ddlConfig_SelectionChanged2(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
                if (config == null)
                    return;
                else
                {
                    if (config.ID > 0)
                    {
                        isClearFilter2 = false;
                        GetDefaultSetting2(config.Setting);


                    }
                    else
                    {
                        isClearFilter2 = true;
                        ClearFilters2();

                        settings2 = new RadGridViewSettings(this.radGridView2);

                        settings2.LoadState(primaryXml2);

                    }
                }
            }
            catch
            {
            }

        }
        private void ddlConfig_SelectionChanged4(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {

            try
            {
                UserGridConfig config = ddlConfig4.SelectedItem as UserGridConfig;
                if (config == null)
                    return;
                else
                {
                    if (config.ID > 0)
                    {
                        isClearFilter4 = false;
                        GetDefaultSetting4(config.Setting);


                    }
                    else
                    {
                        isClearFilter4 = true;
                        ClearFilters4();

                        settings4 = new RadGridViewSettings(this.radGridView4);

                        settings4.LoadState(primaryXml4);

                    }
                }
            }
            catch
            {
            }
        }

        private void ddlConfig_SelectionChanged3(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
                if (config == null)
                    return;
                else
                {
                    if (config.ID > 0)
                    {
                        isClearFilter3 = false;
                        GetDefaultSetting3(config.Setting);


                    }
                    else
                    {
                        isClearFilter3 = true;
                        ClearFilters3();

                        settings3 = new RadGridViewSettings(this.radGridView3);

                        settings3.LoadState(primaryXml3);

                    }
                }
            }
            catch
            {
            }

        }

        private void ddlConfig_SelectionChanged5(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {

            try
            {
                UserGridConfig config = ddlConfig5.SelectedItem as UserGridConfig;
                if (config == null)
                    return;
                else
                {
                    if (config.ID > 0)
                    {
                        isClearFilter5 = false;
                        GetDefaultSetting5(config.Setting);


                    }
                    else
                    {
                        isClearFilter5 = true;
                        ClearFilters5();

                        settings5 = new RadGridViewSettings(this.radGridView5);

                        settings5.LoadState(primaryXml);

                    }
                }
            }
            catch
            {
            }
        }

        private void ddlConfig_SelectionChanged6(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {

            try
            {
                //UserGridConfig config = ddlConfig5.SelectedItem as UserGridConfig;
                //if (config == null)
                //    return;
                //else
                //{
                //    if (config.ID > 0)
                //    {
                //        isClearFilter5 = false;
                //        GetDefaultSetting5(config.Setting);


                //    }
                //    else
                //    {
                //        isClearFilter5 = true;
                //        ClearFilters5();

                //        settings5 = new RadGridViewSettings(this.radGridView5);

                //        settings5.LoadState(primaryXml);

                //    }
                //}
            }
            catch
            {
            }
        }
        #endregion



        #region Call Ria Services Method




        private void GetTables2()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID2), CallbackTables2, null);


        }


        private void GetTables3()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID2), CallbackTables2, null);


        }


        private void CallbackTables2(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                tableList2 = loadOp.Entities.ToList();
                if (tableList2.Count > 0)
                {
                    tableName2 = tableList2.FirstOrDefault().TableName;
                }
                LoadOperation<ViewFieldSummary> loadOpF = context.Load(context.GetViewFieldByViewIDQuery(viewID2), CallbackViewFields2, null);

            }

        }

        private void GetViewTables2()
        {

            LoadOperation<Table> loadOp = context.Load(context.GetTablesByViewIDQuery(viewID2), CallbackViewTables2, null);


        }



        private void CallbackViewTables2(LoadOperation<Table> loadOp)
        {


            if (loadOp != null)
            {

                var table = loadOp.Entities.FirstOrDefault();
                if (table != null)
                {


                    GetExendedPropertiesData2(connString, ExtendedPropertySQL.ExtendedPropertiesSQl(table.DBName, table.TableName), 1, 500, null);


                }


            }

        }



        private void CallbackViewFields2(LoadOperation<ViewFieldSummary> loadOp)
        {


            if (loadOp != null)
            {
                viewFieldSummary2 = new ObservableCollection<ViewFieldSummary>();
                foreach (ViewFieldSummary summary in loadOp.Entities)
                {
                    viewFieldSummary2.Add(summary);

                }

                //    fieldList2 = loadOp.Entities.ToList();

                GetRelationshipsRefresh2();

            }

        }


        private void GetGroupViewFilds2()
        {
            int groupID = 0;
            if (Globals.CurrentUser.Group_ID != null)
                groupID = Globals.CurrentUser.Group_ID.Value;
            LoadOperation<GroupViewFilter> loadOp = context.Load(context.GetGroupViewFiltersByGroupViewIDQuery(groupID, viewID2), CallbackGroupViewFilters2, null);

        }



        private void CallbackGroupViewFilters2(LoadOperation<GroupViewFilter> results)
        {

            if (results != null)
            {
                GroupViewFilterList2 = results.Entities.ToList();
            }

            LoadOperation<GridRelationship> loadOperation = context.Load(context.GetGridRelationshipQuery().Where(g => g.DisplayDefinition_ID == DisplayID), CallbackGridRelationships, null);

        }
        private List<GridRelationship> gridRship = new List<GridRelationship>();
        private void CallbackGridRelationships(LoadOperation<GridRelationship> loadOperation)
        {
            gridRship = new List<GridRelationship>(loadOperation.Entities.ToList());

            // string sql = CreateReadOnlyTable2(loadOperation.Entities.ToList());


            //sqlText2 = sql;
            //newSqlText2 = sql;
            //GenerateColumns2();
            //btnSave.IsEnabled = false;
            // GetTotalRows2(newSqlText2);
            // GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);



        }

        private void GetRelationshipsRefresh2()
        {
            EditorContext cont = new EditorContext();
            LoadOperation<ViewRelationship> loadOp = cont.Load(cont.GetViewRelationshipByViewIDQuery(viewID2), CallbackRelationships2, null);




        }




        private void CallbackRelationships2(LoadOperation<ViewRelationship> loadOp)
        {


            if (loadOp != null)
            {


                relationshipList2 = new List<ViewRelationship>();
                relationshipList2 = loadOp.Entities.ToList();

                GetGroupViewFilds2();
            }
        }

        private void GetFields2()
        {
            LoadOperation<Field> loadOperation = context.Load(context.GetFieldsQuery().Where(x => x.Table_ID == tableID2), CallbackFields2, null);
        }


        private void CallbackFields2(LoadOperation<Field> loadOperation)
        {
            if (loadOperation.Entities != null)
            {
                this.radGridView2.AutoGenerateColumns = false;
                foreach (Field field in loadOperation.Entities)
                {
                    GridViewDataColumn column = new GridViewDataColumn();
                    column.DataMemberBinding = new Binding(field.FieldName.Replace(" ", "_"));
                    column.Header = field.FriendlyName;
                    column.UniqueName = field.FieldName;
                    if (field.Type == "datetime")
                    {
                        column.DataFormatString = "{0:dd/MM/yyyy}";
                    }


                    this.radGridView2.Columns.Add(column);
                }

            }

        }

        string filterOption = "";
        string filterOption2 = "";
        bool autoPopulate = false;
        bool autoPopulate2 = false;
        string filterOption4 = "";
        bool autoPopulate4 = false;
        int currFieldID = 0;
        int currFieldID2 = 0;
        int currFieldID4 = 0;
        bool hidden = false;
        bool hidden2 = false;
        bool hidden4 = false;
        string dataType = "";
        string dataType2 = "";
        string dataType4 = "";

        string filterOption3 = "";
        string filterOption5 = "";
        bool autoPopulate3 = false;
        bool autoPopulate5 = false;
        // string filterOption3 = "";
        // bool autoPopulate5 = false;
        int currFieldID3 = 0;
        int currFieldID5 = 0;

        bool hidden3 = false;
        bool hidden5 = false;

        string dataType3 = "";
        string dataType5 = "";

        //   string dropDownName4 = "";
        private void LoadDropDowns2()
        {
            if (tempFieldList2.Count > 0)
            {

                TempField temp = tempFieldList2.Where(t => t.Index == fieldIndex2 + 1).FirstOrDefault();
                dropDownName2 = temp.Name;
                filterOption2 = temp.OptionCompletion;
                autoPopulate2 = temp.AutoComplete;
                currFieldID2 = temp.FieldID;
                hidden2 = temp.Hidden;
                dataType2 = temp.FieldType.ToLower();
                LoadOperation<StoredProcedureName> loadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(x => x.ID == temp.ID), callbackDPLoadOperation2, null);

                fieldIndex2 = temp.Index;



            }
        }

        private void CallbackDropDown2(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                GetColumnData2(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");

            }
        }

        private void LoadDropDowns3()
        {
            if (tempFieldList3.Count > 0)
            {

                TempField temp = tempFieldList3.Where(t => t.Index == fieldIndex3 + 1).FirstOrDefault();
                dropDownName3 = temp.Name;
                filterOption3 = temp.OptionCompletion;
                autoPopulate3 = temp.AutoComplete;
                currFieldID3 = temp.FieldID;
                hidden3 = temp.Hidden;
                dataType3 = temp.FieldType.ToLower();
                LoadOperation<StoredProcedureName> loadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(x => x.ID == temp.ID), callbackDPLoadOperation3, null);

                fieldIndex3 = temp.Index;



            }
        }

        private void CallbackDropDown3(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                GetColumnData3(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");

            }
        }

        private void LoadDropDowns5()
        {
            if (tempFieldList5.Count > 0)
            {

                TempField temp = tempFieldList5.Where(t => t.Index == fieldIndex2 + 1).FirstOrDefault();
                dropDownName5 = temp.Name;
                filterOption5 = temp.OptionCompletion;
                autoPopulate5 = temp.AutoComplete;
                currFieldID5 = temp.FieldID;
                hidden5 = temp.Hidden;
                dataType5 = temp.FieldType.ToLower();
                LoadOperation<StoredProcedureName> loadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(x => x.ID == temp.ID), callbackDPLoadOperation5, null);

                fieldIndex5 = temp.Index;



            }
        }

        private void CallbackDropDown5(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                GetColumnData5(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");

            }
        }


        private void LoadDropDowns4()
        {
            if (tempFieldList4.Count > 0)
            {

                TempField temp = tempFieldList4.Where(t => t.Index == fieldIndex4 + 1).FirstOrDefault();
                dropDownName4 = temp.Name;
                filterOption4 = temp.OptionCompletion;
                autoPopulate4 = temp.AutoComplete;
                currFieldID4 = temp.FieldID;
                hidden4 = temp.Hidden;
                dataType4 = temp.FieldType.ToLower();
                fieldIndex4 = temp.Index;
                LoadOperation<StoredProcedureName> loadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(x => x.ID == temp.ID), callbackDPLoadOperation4, null);





            }
        }

        private void CallbackDropDown4(LoadOperation<DropDownPairResult> loadOp)
        {


            if (loadOp != null)
            {
                DropDownPairResult dropDown = loadOp.Entities.FirstOrDefault();

                GetColumnData2(dropDown.ConnectionString, dropDown.SqlQueryString, 1, 500, "");

            }
        }

        private void LoadConfigSetting2()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting2, null);
        }

        private void CallbackSetting2(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList2 = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 2)
                        settingObjectList2.Add(sett);
                }
                settingObjectList2.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 2, Settings_ID = 0 });

                ddlConfig2.ItemsSource = settingObjectList2;


                var query = (from s in settingObjectList2
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                {
                    ddlConfig2.SelectedValue = query.ID;
                    GetDefaultSetting2(query.Setting);
                }



            }


        }


        private void LoadConfigSetting3()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting3, null);
        }

        private void CallbackSetting3(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList3 = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 3)
                        settingObjectList3.Add(sett);
                }
                settingObjectList3.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 3, Settings_ID = 0 });

                ddlConfig3.ItemsSource = settingObjectList3;


                var query = (from s in settingObjectList3
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                {
                    ddlConfig3.SelectedValue = query.ID;
                    GetDefaultSetting3(query.Setting);
                }



            }


        }

        private void LoadConfigSetting5()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting5, null);
        }

        private void CallbackSetting5(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList5 = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 5)
                        settingObjectList5.Add(sett);
                }
                settingObjectList5.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 5, Settings_ID = 0 });

                ddlConfig5.ItemsSource = settingObjectList5;


                var query = (from s in settingObjectList5
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();

                if (query != null)
                {
                    ddlConfig5.SelectedValue = query.ID;
                    GetDefaultSetting5(query.Setting);
                }



            }


        }

        private void LoadConfigSetting6()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting6, null);
        }
        ObservableCollection<UserGridConfig> settingObjectList6 = new ObservableCollection<UserGridConfig>();
        private void CallbackSetting6(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList6 = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 6)
                        settingObjectList6.Add(sett);
                }
                settingObjectList6.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Settings", DefaultConfiguration = 0, GridNumber = 6, Settings_ID = 0 });

                ddlConfig6.ItemsSource = settingObjectList6;


                var query = (from s in settingObjectList6
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();

                if (query != null)
                {
                    ddlConfig6.SelectedValue = query.ID;
                    GetDefaultSetting6(query.Setting);
                }



            }


        }
        private void LoadConfigSetting4()
        {
            LoadOperation<UserGridConfig> loadOp = context.Load(context.GetUserGridConfigsQuery().Where(x => x.UserInformation_ID == userID && x.DisplayDefinition_ID == displayDefID), CallbackSetting4, null);
        }

        private void CallbackSetting4(LoadOperation<UserGridConfig> loadOp)
        {


            if (loadOp != null)
            {

                settingObjectList4 = new ObservableCollection<UserGridConfig>();

                foreach (UserGridConfig sett in loadOp.Entities)
                {
                    if (sett.GridNumber == 4)
                        settingObjectList4.Add(sett);
                }
                settingObjectList4.Add(new UserGridConfig { ID = 0, ConfigurationName = "Clear Filters", DefaultConfiguration = 0, GridNumber = 4, Settings_ID = 0 });

                ddlConfig4.ItemsSource = settingObjectList4;


                var query = (from s in settingObjectList4
                             where s.DefaultConfiguration == 1
                             select s).FirstOrDefault();
                if (query != null)
                {
                    ddlConfig4.SelectedValue = query.ID;
                    GetDefaultSetting4(query.Setting);
                }



            }


        }


        private void CallbackDefault2(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig2.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    primarySettings2.ResetState();

                    settings2 = new RadGridViewSettings(this.radGridView2);
                    string xml = setting.Settings;
                    settings2.LoadState(xml);




                }
                catch
                {
                }
            }
        }
        private void CallbackDefault3(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig3.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    primarySettings3.ResetState();

                    settings3 = new RadGridViewSettings(this.radGridView3);
                    string xml = setting.Settings;
                    settings3.LoadState(xml);




                }
                catch
                {
                }
            }
        }


        private void CallbackDefault5(LoadOperation<Setting> loadOp)
        {


            if (loadOp != null)
            {
                Setting setting = loadOp.Entities.FirstOrDefault();



                ddlConfig5.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {
                    //  primarySettings.LoadOriginalState();
                    primarySettings5.ResetState();

                    settings5 = new RadGridViewSettings(this.radGridView5);
                    string xml = setting.Settings;
                    settings5.LoadState(xml);




                }
                catch
                {
                }
            }
        }
        #endregion



        #region Private Methods

        private void Cancel2()
        {
            editedDataObjectList2 = new ObservableCollection<DataObject>();
            newDataObjectList2 = new ObservableCollection<DataObject>();
            deletedDataObjectList2 = new ObservableCollection<DataObject>();
            // cMenu2.IsOpen = false;
        }

        private void ExcelExport2()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView2.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }
        private void Cancel3()
        {
            editedDataObjectList3 = new ObservableCollection<DataObject>();
            newDataObjectList3 = new ObservableCollection<DataObject>();
            deletedDataObjectList3 = new ObservableCollection<DataObject>();
            // cMenu2.IsOpen = false;
        }
        private void Cancel4()
        {
            editedDataObjectList4 = new ObservableCollection<DataObject>();
            newDataObjectList4 = new ObservableCollection<DataObject>();
            deletedDataObjectList4 = new ObservableCollection<DataObject>();
            // cMenu2.IsOpen = false;
        }
        private void ExcelExport3()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView3.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        private void Cancel5()
        {
            editedDataObjectList5 = new ObservableCollection<DataObject>();
            newDataObjectList5 = new ObservableCollection<DataObject>();
            deletedDataObjectList5 = new ObservableCollection<DataObject>();
            // cMenu2.IsOpen = false;
        }

        private void ExcelExport5()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView5.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }

        private void ExcelExport4()
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView4.Export(stream,
                new GridViewExportOptions()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                });
                }
            }
        }
        private void SaveChanges2()
        {
            try
            {



              DataObject dataObject = radGridView1.SelectedItem as DataObject;
            if (dataObject == null)
                return;
                
                editedDataObjectList2 = new ObservableCollection<DataObject>();
                newDataObjectList2 = new ObservableCollection<DataObject>();
               // deletedDataObjectList3 = new ObservableCollection<DataObject>();
                foreach (var item in dataObjectList2)
                {
                    if (item.State == DataObject.DataStates.Modified)
                    {
                        editedDataObjectList2.Add(item);
                    }

                    //if (item.State == DataObject.DataStates.Added)
                    //{
                    //    newDataObjectList3.Add(item);
                    //}

                    if (item.State == DataObject.DataStates.Deleted)
                    {
                        deletedDataObjectList2.Add(item);
                    }
                }

                foreach (var item in radGridView2.Items)
                {

                    DataObject data = item as DataObject;

                    string strID = data.GetFieldValue("ID").ToString();
                    if (strID == "0")
                    {

                   

                    DataObject newDataObject = item as DataObject;

                    foreach (GridRelationship rship in gridRship)
                    {
                        //  max = gridRship.Count;
                        // curr += 1;
                        try
                        {

                            ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                            ReadField field2 = fieldList2.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                            string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                            //   gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";
                            if(field2 != null)
                            newDataObject.SetFieldValue(field2.FieldName, parameterValue);

                        }
                        catch { }
                    }

                    foreach (var field in fieldList2)
                    {
                        try
                        {
                            if (field.DefaultValue != null)
                            {

                                object obj = field.DefaultValue as object;
                                if (obj.ToString() != "")
                                {

                                    if (field.AutoPopulate)
                                    newDataObject.SetFieldValue(field.FieldName, obj);
                                }
                            }
                        }
                        catch
                        {
                        }

                    }
                    newDataObjectList2.Add(newDataObject);
                }
                }

                if (newDataObjectList2.Count > 0)
                {
                    if (useCreateSP2)
                    {
                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == CreateSPID2), callCreateSPLoadOperation2, null);

                    }
                    else
                    {
                        InsertDataTable2(newDataObjectList2, tableName2);
                    }

                }
            

                //Update
                if (editedDataObjectList2.Count > 0)
                {

                    foreach (var item in editedDataObjectList2)
                    {
                        if (item.State == DataObject.DataStates.Modified)
                        {
                        }
                    }

                    if (useUpdateSP2)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == UpdateSPID2), callUpdateSPLoadOperation2, null);
                    }
                    else
                    {
                        Update(editedDataObjectList2, tableName2);
                    }
                }

                if (deletedDataObjectList2.Count > 0)
                {

                    if (useDeleteSP2)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == DeleteSPID2), callDeleteSPLoadOperation2, null);
                    }
                    else
                    {
                        DeleteDataTable2(deletedDataObjectList2, tableName2);

                    }

                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }


        private void SaveChanges3()
        {
            try
            {
                editedDataObjectList3 = new ObservableCollection<DataObject>();
                newDataObjectList3 = new ObservableCollection<DataObject>();
               // deletedDataObjectList3 = new ObservableCollection<DataObject>();
                List<DataObject> cuurList = new List<DataObject>();
               foreach (var item in dataList3)
                {
                   DataObject data = item as DataObject;
                   if (!dataObjectList3.Contains(data))
                       deletedDataObjectList3.Add(data);
                 }
                foreach (var item in dataObjectList3)
                {
                    if (item.State == DataObject.DataStates.Modified)
                    {
                        editedDataObjectList3.Add(item);
                    }

                    //if (item.State == DataObject.DataStates.Added)
                    //{
                    //    newDataObjectList3.Add(item);
                    //}

                    if (item.State == DataObject.DataStates.Deleted)
                    {
                        deletedDataObjectList3.Add(item);
                    }

                //  var id =  item.GetFieldValue("ID");


                }

               

                foreach (var item in radGridView3.Items)
                {
                    try
                    {
                        DataObject data = item as DataObject;

                        string strID = data.GetFieldValue("ID").ToString();
                        if (strID == "0")
                        {

                            DataObject dataObject = radGridView1.SelectedItem as DataObject;
                            if (dataObject == null)
                                return;




                            DataObject newDataObject = item as DataObject;
                            try
                            {


                                object id = ModelID as object;
                                newDataObject.SetFieldValue("Model_ID", ModelID);


                            }
                            catch { }

                            foreach (GridRelationship rship in gridRship)
                            {
                                //  max = gridRship.Count;
                                // curr += 1;
                                try
                                {

                                    ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                                    ReadField field3 = fieldList3.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                                    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                                    //   gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";
                                    if(field3 != null)
                                    newDataObject.SetFieldValue(field3.FieldName, parameterValue);

                                }
                                catch { }
                            }

                            foreach (var field in fieldList3)
                            {
                                try
                                {
                                    if (field.DefaultValue != null)
                                    {

                                        object obj = field.DefaultValue as object;
                                        if (obj.ToString() != "")
                                        {
                                          
                                            if(field.AutoPopulate)
                                            newDataObject.SetFieldValue(field.FieldName, obj);
                                        }
                                    }
                                }
                                catch
                                {
                                }

                            }

                           

                            newDataObjectList3.Add(newDataObject);
                        }
                    }
                    catch
                    {
                    }
                }

                if (newDataObjectList3.Count > 0)
                {
                    if (useCreateSP3)
                    {
                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == CreateSPID3), callCreateSPLoadOperation3, null);

                    }
                    else
                    {
                        InsertDataTable(newDataObjectList3, tableName3);
                    }

                }


                //Update
                if (editedDataObjectList3.Count > 0)
                {

                    if (useUpdateSP3)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == UpdateSPID3), callUpdateSPLoadOperation3, null);
                    }
                    else
                    {
                        Update(editedDataObjectList3, tableName3);
                    }
                }

                if (deletedDataObjectList3.Count > 0)
                {

                    if (useDeleteSP3)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == DeleteSPID3), callDeleteSPLoadOperation3, null);
                    }
                    else
                    {
                        DeleteDataTable(deletedDataObjectList3, tableName3);

                    }

                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }

        private void SaveChanges5()
        {
            try
            {
                if (newDataObjectList5.Count > 0)
                {
                    if (useCreateSP5)
                    {
                        //   LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == CreateSPID2), callCreateSPLoadOperation5, null);

                    }
                    else
                    {
                        InsertDataTable2(newDataObjectList5, tableName5);
                    }

                }


                //Update
                if (editedDataObjectList5.Count > 0)
                {

                    if (useUpdateSP5)
                    {

                        //   LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == UpdateSPID5), callUpdateSPLoadOperation5, null);
                    }
                    else
                    {
                        Update(editedDataObjectList5, tableName5);
                    }
                }

                if (deletedDataObjectList5.Count > 0)
                {

                    if (useDeleteSP5)
                    {

                        ///    LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == DeleteSPID5), callDeleteSPLoadOperation5, null);
                    }
                    else
                    {
                        DeleteDataTable(deletedDataObjectList5, tableName5);

                    }

                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }
        private void SaveChanges4()
        {
            try
            {
                DataObject dataObject = radGridView1.SelectedItem as DataObject;
                if (dataObject == null)
                    return;

                editedDataObjectList4 = new ObservableCollection<DataObject>();
                newDataObjectList4 = new ObservableCollection<DataObject>();
               // deletedDataObjectList3 = new ObservableCollection<DataObject>();
                foreach (var item in dataObjectList4)
                {
                    if (item.State == DataObject.DataStates.Modified)
                    {
                        editedDataObjectList4.Add(item);
                    }

                    //if (item.State == DataObject.DataStates.Added)
                    //{
                    //    newDataObjectList3.Add(item);
                    //}

                    if (item.State == DataObject.DataStates.Deleted)
                    {
                        deletedDataObjectList4.Add(item);
                    }
                }

                foreach (var item in radGridView4.Items)
                {

                    DataObject data = item as DataObject;

                    string strID = data.GetFieldValue("ID").ToString();
                    if (strID == "0")
                    {
                   


                    DataObject newDataObject = item as DataObject;
                    try
                    {


                        object id = ModelID as object;
                        newDataObject.SetFieldValue("Model_ID", ModelID);


                    }
                    catch { }

                    foreach (GridRelationship rship in gridRship)
                    {
                        //  max = gridRship.Count;
                        // curr += 1;
                        try
                        {

                            ReadField field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                            ReadField field4 = fieldList4.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                            string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                            //   gridrship += " " + field2.FieldName + " = '" + parameterValue + "' ";
                            if(field4 != null)
                            newDataObject.SetFieldValue(field4.FieldName, parameterValue);

                        }
                        catch { }
                    }
                    foreach (var field in fieldList4)
                    {
                        try
                        {
                            if (field.DefaultValue != null)
                            {

                                object obj = field.DefaultValue as object;
                                if (obj.ToString() != "")
                                {
                                    if (field.AutoPopulate)
                                    newDataObject.SetFieldValue(field.FieldName, obj);
                                }
                            }
                        }
                        catch
                        {
                        }

                    }

                  
                    newDataObjectList4.Add(newDataObject);
                }

                }

                if (newDataObjectList4.Count > 0)
                {
                    if (useCreateSP4)
                    {
                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == CreateSPID4), callCreateSPLoadOperation4, null);

                    }
                    else
                    {
                        InsertDataTable4(newDataObjectList4, tableName4);
                    }

                }


                //Update
                if (editedDataObjectList4.Count > 0)
                {

                    if (useUpdateSP4)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == UpdateSPID4), callUpdateSPLoadOperation4, null);
                    }
                    else
                    {
                        Update(editedDataObjectList4, tableName4);
                    }
                }

                if (deletedDataObjectList4.Count > 0)
                {

                    if (useDeleteSP4)
                    {

                        LoadOperation<StoredProcedureName> StoredProcedureNameLoadOperation = context.Load(context.GetStoredProcedureNamesQuery().Where(s => s.ID == DeleteSPID4), callDeleteSPLoadOperation4, null);
                    }
                    else
                    {
                        DeleteDataTable4(deletedDataObjectList4, tableName4);

                    }

                }
                //this.radGridView1.DeferRefresh();
            }
            catch
            {
            }
        }



        private void DeleteRows2()
        {
            try
            {

                if (this.radGridView2.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView2.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView2.Items.Remove(item as DataObject);
                    deletedDataObjectList2.Add(item);
                }

                int count = deletedDataObjectList2.Count;
            }
            catch
            {
            }
        }
        private void DeleteRows3()
        {
            try
            {

                if (this.radGridView3.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView3.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView3.Items.Remove(item as DataObject);
                    deletedDataObjectList3.Add(item);
                }

                int count = deletedDataObjectList3.Count;
            }
            catch
            {
            }
        }

        private void DeleteRows5()
        {
            try
            {

                if (this.radGridView5.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView5.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView5.Items.Remove(item as DataObject);
                    deletedDataObjectList5.Add(item);
                }

                int count = deletedDataObjectList5.Count;
            }
            catch
            {
            }
        }
        private void DeleteRows4()
        {
            try
            {

                if (this.radGridView4.SelectedItems.Count == 0)
                {
                    return;
                }
                ObservableCollection<DataObject> itemsToRemove = new ObservableCollection<DataObject>();


                //Remove the items from the RadGridView
                foreach (var item in this.radGridView4.SelectedItems)
                {
                    itemsToRemove.Add(item as DataObject);
                }
                foreach (var item in itemsToRemove)
                {
                    this.radGridView4.Items.Remove(item as DataObject);
                    deletedDataObjectList4.Add(item);
                }

                int count = deletedDataObjectList4.Count;
            }
            catch
            {
            }
        }

        private void ClearFilters2()
        {

            this.radGridView2.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView2.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView2.FilterDescriptors.ResumeNotifications();
            // primarySettings2.LoadOriginalState();
        }
        private void ClearFilters3()
        {

            this.radGridView3.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView3.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView3.FilterDescriptors.ResumeNotifications();
            // primarySettings2.LoadOriginalState();
        }

        //private void ClearFilters5()
        //{

        //    this.radGridView5.FilterDescriptors.SuspendNotifications();
        //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView5.Columns)
        //    {
        //        column.IsVisible = true;
        //        column.ClearFilters();
        //    }
        //    this.radGridView5.FilterDescriptors.ResumeNotifications();
        //    // primarySettings2.LoadOriginalState();
        //}
        private void ClearFilters4()
        {

            this.radGridView4.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView4.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView4.FilterDescriptors.ResumeNotifications();
            // primarySettings2.LoadOriginalState();
        }

        private void ClearFilters5()
        {

            this.radGridView5.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView5.Columns)
            {
                column.IsVisible = true;
                column.ClearFilters();
            }
            this.radGridView5.FilterDescriptors.ResumeNotifications();
            // primarySettings2.LoadOriginalState();
        }


        private void SaveConfig2()
        {
            try
            {


                UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings2 = new RadGridViewSettings(this.radGridView2);

                    string settingsXML = settings2.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {

                            string alertText = "Error";

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText });
                          //  Message.ErrorMessage(so.Error.Message + " Error adding setting...");
                        }
                        else
                        {
                            string alertText = "Successfully Saved";

                         //   Message.InfoMessage("Successfully Saved");
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successsfull" }, Content = alertText });


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                string alertText = "Error";

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }

        private void SaveConfigAs2()
        {
            try
            {

                settings2 = new RadGridViewSettings(this.radGridView2);

                string settingsXML = settings2.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 2, displayDefID);
               // config.Closed += new EventHandler(ChildWin_Closed2);
                config.Closed += ChildWin_Closed2;
                config.Left = (Application.Current.Host.Content.ActualWidth - config.ActualWidth) / 2;
                config.Top = (Application.Current.Host.Content.ActualHeight - config.ActualHeight) / 2;
                config.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                config.ShowDialog();
            }
            catch (Exception ex)
            {
                string alertText = "Error";

                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private void DeleteConfig2()
        {
            try
            {


                UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                          //  Message.ErrorMessage(so.Error.Message + " ...");

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {

                            string alertText = "Successful";

                            //   Message.InfoMessage("Successfully Saved");
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successsfull" }, Content = alertText });
                            ClearFilters2();
                            ddlConfig2.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }
        private void SaveConfig3()
        {
            try
            {


                UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings3 = new RadGridViewSettings(this.radGridView3);

                    string settingsXML = settings3.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            string alertText = "Error";

                            //   Message.InfoMessage("Successfully Saved");
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = alertText });
                        }
                        else
                        {

                            string alertText = "Successful";

                            //   Message.InfoMessage("Successfully Saved");
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successsfull" }, Content = alertText });



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }

        private void SaveConfigAs3()
        {
            try
            {

                settings3 = new RadGridViewSettings(this.radGridView3);

                string settingsXML = settings3.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 3, displayDefID);
                config.Closed += ChildWin_Closed;
                config.Left = (Application.Current.Host.Content.ActualWidth - config.ActualWidth) / 2;
                config.Top = (Application.Current.Host.Content.ActualHeight - config.ActualHeight) / 2;
                config.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                config.ShowDialog();
            }
            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private void DeleteConfig3()
        {
            try
            {


                UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content =so.Error.Message });
                        }
                        else
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                            ClearFilters3();
                            ddlConfig3.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private void SaveConfig5()
        {
            try
            {


                UserGridConfig config = ddlConfig5.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings5 = new RadGridViewSettings(this.radGridView5);

                    string settingsXML = settings5.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }

        private void SaveConfigAs5()
        {
            try
            {

                settings5 = new RadGridViewSettings(this.radGridView5);

                string settingsXML = settings5.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 5, displayDefID);
              //  config.Closed += new EventHandler(ChildWin_Closed5);
                config.Closed += ChildWin_Closed5;
                config.Left = (Application.Current.Host.Content.ActualWidth - config.ActualWidth) / 2;
                config.Top = (Application.Current.Host.Content.ActualHeight - config.ActualHeight) / 2;
                config.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                config.ShowDialog();
            }
            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private void DeleteConfig5()
        {
            try
            {


                UserGridConfig config = ddlConfig5.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                            ClearFilters5();
                            ddlConfig5.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }

        private void SaveConfig4()
        {
            try
            {


                UserGridConfig config = ddlConfig4.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                    settings4 = new RadGridViewSettings(this.radGridView4);

                    string settingsXML = settings4.SaveState();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }

        private void SaveConfigAs4()
        {
            try
            {

                settings4 = new RadGridViewSettings(this.radGridView4);

                string settingsXML = settings4.SaveState();



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 4, displayDefID);
                //config.Closed += new EventHandler(ChildWin_Closed4);
                config.Closed += ChildWin_Closed4;
                config.Left = (Application.Current.Host.Content.ActualWidth - config.ActualWidth) / 2;
                config.Top = (Application.Current.Host.Content.ActualHeight - config.ActualHeight) / 2;
                config.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                config.ShowDialog();
            }
            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private void DeleteConfig4()
        {
            try
            {


                UserGridConfig config = ddlConfig4.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                            ClearFilters4();
                            ddlConfig4.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }




        private void SaveConfig6()
        {
            try
            {


                UserGridConfig config = ddlConfig6.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {

                  
                    string settingsXML = GetXmlString();
                    Setting setting = config.Setting;
                    setting.Settings = settingsXML;


                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {
                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });



                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }

        private string GetXmlString()
        {
            string xmlString = "";

            SerialChart serialChart = FindControl<SerialChart>((UIElement)gPanel, typeof(SerialChart), "graphMTP");
            serialChart.Name = "graphMTP";
            Control newControl = serialChart as Control;
            string  objectType = "SerialChart";
            SerialChart newserialChart = new SerialChart();
            xmlString = ChartControlManager.CreateControlXmlString(newControl, newserialChart, objectType);
            return xmlString;
        }


        private void SaveConfigAs6()
        {
            try
            {

              
                string settingsXML = GetXmlString();
              



                Views.SaveConfig config = new Views.SaveConfig(settingsXML, 6, displayDefID);
                //config.Closed += new EventHandler(ChildWin_Closed4);
                config.Closed += ChildWin_Closed6;
                config.Left = (Application.Current.Host.Content.ActualWidth - config.ActualWidth) / 2;
                config.Top = (Application.Current.Host.Content.ActualHeight - config.ActualHeight) / 2;
                config.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                config.ShowDialog();
            }
            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private void DeleteConfig6()
        {
            try
            {


                UserGridConfig config = ddlConfig6.SelectedItem as UserGridConfig;
                if (config == null)
                    return;

                if (config.Settings_ID > 0)
                {


                    context.UserGridConfigs.Remove(config);
                    context.SubmitChanges(so =>
                    {
                        if (so.HasError)
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = so.Error.Message });
                        }
                        else
                        {

                            RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "Successful" });
                           // ClearFilters4();
                            ddlConfig6.SelectedValue = 0;


                        }

                    }, null);
                }
            }


            catch (Exception ex)
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = ex.Message });
            }
        }


        private bool isFirstLoad2 = true;
        string xmlCache2 = "";
        string primaryXml2 = "";
        private bool isFirstLoad4 = true;
        string xmlCache4 = "";
        string primaryXml4 = "";


        private bool isFirstLoad3 = true;
        string xmlCache3 = "";
        string primaryXml3 = "";
        private bool isFirstLoad5 = true;
        string xmlCache5 = "";
        string primaryXml5 = "";


        private void GetDefaultSetting2(Setting setting)
        {
            if (isFirstLoad2)
            {

                primarySettings2 = new RadGridViewSettings(this.radGridView2);
                primarySettings2.SaveState();
                primaryXml2 = primarySettings2.SaveState();
                isFirstLoad2 = false;
            }




            if (setting != null)
            {
                //  ddlConfig2.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {

                    ClearFilters2();
                    //  primarySettings.LoadOriginalState();
                    primarySettings2.ResetState();

                    settings2 = new RadGridViewSettings(this.radGridView2);
                    string xml = setting.Settings;
                    xmlCache2 = xml;
                    settings2.LoadState(xml);

                    //createSqlWithNewFilters();




                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }


        private void GetDefaultSetting3(Setting setting)
        {
            if (isFirstLoad3)
            {

                primarySettings3 = new RadGridViewSettings(this.radGridView3);
                primarySettings3.SaveState();
                primaryXml3 = primarySettings3.SaveState();
                isFirstLoad3 = false;
            }




            if (setting != null)
            {
                //   ddlConfig3.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {

                    ClearFilters3();
                    //  primarySettings.LoadOriginalState();
                    primarySettings3.ResetState();

                    settings3 = new RadGridViewSettings(this.radGridView3);
                    string xml = setting.Settings;
                    xmlCache3 = xml;
                    settings3.LoadState(xml);

                    //createSqlWithNewFilters();




                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }

        private void GetDefaultSetting4(Setting setting)
        {
            if (isFirstLoad4)
            {

                primarySettings4 = new RadGridViewSettings(this.radGridView4);
                primarySettings4.SaveState();
                primaryXml4 = primarySettings4.SaveState();
                isFirstLoad4 = false;
            }




            if (setting != null)
            {
                // ddlConfig4.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {

                    ClearFilters4();
                    //  primarySettings.LoadOriginalState();
                    primarySettings4.ResetState();

                    settings4 = new RadGridViewSettings(this.radGridView4);
                    string xml = setting.Settings;
                    xmlCache4 = xml;
                    settings4.LoadState(xml);

                    //createSqlWithNewFilters();




                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }

        private void GetDefaultSetting5(Setting setting)
        {
            if (isFirstLoad5)
            {

                primarySettings5 = new RadGridViewSettings(this.radGridView5);
                primarySettings5.SaveState();
                primaryXml5 = primarySettings5.SaveState();
                isFirstLoad5 = false;
            }




            if (setting != null)
            {
                // ddlConfig5.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {

                    ClearFilters5();
                    //  primarySettings.LoadOriginalState();
                    primarySettings5.ResetState();

                    settings5 = new RadGridViewSettings(this.radGridView5);
                    string xml = setting.Settings;
                    xmlCache5 = xml;
                    settings5.LoadState(xml);

                    //createSqlWithNewFilters();




                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }
        bool isFirstLoad6 = false;

        private void GetDefaultSetting6(Setting setting)
        {
            if (isFirstLoad6)
            {

                
            }




            if (setting != null)
            {
                //ddlConfig6.SelectedValue = setting.ID;
                // apply setting to the gridview

                try
                {

                   




                }
                catch
                {
                }
                //   LoadOperation<Setting> loadOp = context.Load(context.GetSettingsQuery().Where(x => x.ID == setttingID), CallbackDefault, null);
            }
        }
        private string testName2 = "";


        private void GenerateColumns2()
        {
            if (canGenerateColumns2)
            {
                int i = 1;
                tempFieldList2 = new List<TempField>();
                this.radGridView2.AutoGenerateColumns = false;
                this.radGridView2.ShowInsertRow = false;


                foreach (ViewFieldSummary field in viewFieldSummary2)
                {

                    if (columnNames2.Contains(field.FieldName))
                    {
                        GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                        columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                        columnComboBox.Header = field.DisplayName;
                        columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                        columnComboBox.SelectedValueMemberPath = "Value";
                        columnComboBox.DisplayMemberPath = "Display";
                        columnComboBox.IsFilteringDeferred = true;
                        columnComboBox.IsFilteringDeferred = true;
                        this.radGridView2.Columns.Add(columnComboBox);
                    }
                    else
                    {
                        if (field.UseValueField != null)
                        {
                            GridViewComboBoxColumn columnComboBox = new GridViewComboBoxColumn();

                            columnComboBox.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            columnComboBox.Header = field.DisplayName;
                            columnComboBox.UniqueName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            testName = CharacterHandler.ReplaceSpecialCharacter(field.FieldName);
                            columnComboBox.SelectedValueMemberPath = "Value";
                            columnComboBox.DisplayMemberPath = "Display";
                            columnComboBox.IsFilteringDeferred = true;
                            this.radGridView2.Columns.Add(columnComboBox);

                            tempFieldList2.Add(new TempField { ID = field.FieldID, Name = CharacterHandler.ReplaceSpecialCharacter(field.FieldName), Index = i });
                            i++;




                        }
                        else
                        {

                            GridViewDataColumn column = new GridViewDataColumn();
                            column.DataMemberBinding = new Binding(CharacterHandler.ReplaceSpecialCharacter(field.FieldName));
                            column.Header = field.DisplayName;
                            column.UniqueName = field.FieldName;
                            column.IsFilteringDeferred = true;

                            if (field.IsCalculatedField.Value == true)
                            {


                                column.IsReadOnly = true;

                                column.Background = Resources["SkyBlue2"] as SolidColorBrush;// new SolidColorBrush(Colors.Blue);
                            }

                            this.radGridView2.Columns.Add(column);



                        }
                    }
                }
                canGenerateColumns2 = false;

                tempFieldList2 = tempFieldList2.OrderBy(t => t.Index).ToList();
                indexMax2 = tempFieldList2.Count;
                LoadExtendedProperties2();
                LoadDropDowns2();

                Attach2();

            }

            LoadConfigSetting3();
            GetTables3();
        }


        private void LoadExtendedProperties2()
        {
            foreach (DataObject dataObject in propertiesList2)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList2 = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList2.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView2.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList2;
                }
                catch
                {
                }
            }
        }
        private void LoadExtendedProperties3()
        {
            foreach (DataObject dataObject in propertiesList3)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList3 = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList3.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView3.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList3;
                }
                catch
                {
                }
            }
        }
        private void LoadExtendedProperties5()
        {
            foreach (DataObject dataObject in propertiesList5)
            {
                string source = dataObject.GetFieldValue("ExtendedPropertyValue").ToString();
                string column = dataObject.GetFieldValue("ColumnName").ToString();
                string gridColumnName = CharacterHandler.ReplaceSpecialCharacter(column);
                string[] stringSeparators = new string[] { ";" };
                string[] result = source.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                TempExtendedPropertyList5 = new List<TempExtendedProperty>();
                foreach (string s in result)
                {
                    TempExtendedPropertyList5.Add(new TempExtendedProperty { Value = s, Display = s });
                }
                try
                {
                    ((GridViewComboBoxColumn)this.radGridView5.Columns[gridColumnName]).ItemsSource = TempExtendedPropertyList5;
                }
                catch
                {
                }
            }
        }

        private string CreateReadOnlyTable2(List<GridRelationship> gridRships)
        {
            //   DataObject dataObject = this.radGridView1.SelectedItem as DataObject;
            GridRelationshipList = gridRships;
            string sql = string.Empty;
            //table list
            //display field list
            //relationshiplist

            string tables = " From ";
            string fields = " Select ";
            string relationships = " Where ";
            string viewFilter = "";
            //   string gridrship = "";
            foreach (Table table in tableList2)
            {
                tables += "[" + table.DBName + "].[dbo].[" + table.TableName + "] as " + "[" + table.TableName.ToString() + "] ,";
            }

            //foreach (ViewFieldSummary field in fieldList2)
            //{
            //    if (field.TableID > 0)
            //    {
            //        fields += field.TableField + " ,";//" as [" + field.DisplayName + "] ,";
            //    }
            //    else
            //    {
            //        fields += " (" + field.Expression + " ) as [" + field.DisplayName + "] ,";
            //    }
            //}

            foreach (ViewRelationship rship in relationshipList2)
            {
                relationships += " " + rship.Summary + " and";

            }


            //foreach (GridRelationship rship in gridRships)
            //{
            //    try
            //    {
            //        ViewFieldSummary field = fieldList.Where(f => f.FieldID == rship.ViewField1).FirstOrDefault();
            //        ViewFieldSummary field2 = fieldList2.Where(f => f.FieldID == rship.ViewField2).FirstOrDefault();
            //        string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
            //        gridrship += " " + field2.TableField + " = '" + parameterValue + "' and";
            //    }
            //    catch { }
            //}

            //if (gridrship != string.Empty)
            //{
            //    gridrship = gridrship.TrimEnd('d');
            //    gridrship = gridrship.TrimEnd('n');
            //    gridrship = gridrship.TrimEnd('a');
            //}

            foreach (GroupViewFilter item in GroupViewFilterList2)
            {
                viewFilter += " " + item.Summary + " and";
            }

            tables = tables.TrimEnd(',');
            fields = fields.TrimEnd(',');
            relationships = relationships.TrimEnd('d');
            relationships = relationships.TrimEnd('n');
            relationships = relationships.TrimEnd('a');
            viewFilter = viewFilter.TrimEnd('d');
            viewFilter = viewFilter.TrimEnd('n');
            viewFilter = viewFilter.TrimEnd('a');

            if (relationships == " Where ")
            {

                if (viewFilter != "")
                {
                    relationships += viewFilter;
                }
                else
                {

                    relationships = "";
                }

            }
            else
            {


                if (viewFilter != "")
                {
                    relationships += " and " + viewFilter;
                }
            }





            sql = fields + tables + relationships;

            return sql;

        }
        #endregion



        #region Call WFC Services Methods

        private void GetData2(string sql, int pagenumber, int pagesize, object userState)
        {
            this.radGridView2.IsBusy = true;
            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetDataSetDataCompleted2);
            ws.GetDataSetDataAsync(connString, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetDataSetDataCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    List<IColumnFilterDescriptor> desc = new List<IColumnFilterDescriptor>();

                    foreach (Telerik.Windows.Controls.GridViewColumn column in radGridView2.Columns)
                    {
                        Telerik.Windows.Controls.GridView.IColumnFilterDescriptor filterDescriptors = column.ColumnFilterDescriptor;
                        if (filterDescriptors.FieldFilter.Filter1.Value.ToString() != "")
                        {
                            desc.Add(filterDescriptors);
                        }
                    }

                    radGridView2.ItemsSource = list;
                    foreach (var item in desc)
                    {
                        radGridView2.FilterDescriptors.Add(item);
                    }
                    if (canGenerateColumns2)
                    {
                        GetExendedPropertiesData2(connString, ExtendedPropertySQL.ExtendedPropertiesSQl(databaseName, tableName2), 1, 500, null);

                        if (!(singleTable2))
                        {
                            btnInactive2.Visibility = Visibility.Visible;
                            btnButtons2.Visibility = Visibility.Collapsed;

                        }
                        else
                        {
                            btnInactive2.Visibility = Visibility.Collapsed;
                            btnButtons2.Visibility = Visibility.Visible;
                        }

                    }

                    else
                    {
                        //if (!isClearFilter2)
                        //{
                        //    settings2 = new RadGridViewSettings(this.radGridView2);

                        //    settings2.LoadState(xmlCache2);
                        //}
                    }


                }

            }
            this.radGridView2.IsBusy = false;
        }



        void Update2(ObservableCollection<DataObject> objectCollection, string strTablename)
        {

            var ws = WCF.GetService();
            ws.UpdateCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.UpdateCompletedEventArgs>(ws_UpdateCompleted2);
            ws.UpdateAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName2);

            ///.Progress.Start();
        }

        void ws_UpdateCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.UpdateCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                editedDataObjectList2 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }

        }



        void InsertDataTable2(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted2);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName2);

            ///.Progress.Start();
        }

        void ws_InsertCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList2 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }

        void InsertDataTable4(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted4);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName4);

            ///.Progress.Start();
        }

        void ws_InsertCompleted4(object sender, BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList4 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }

        void InsertDataTable3(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted3);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName4);

            ///.Progress.Start();
        }

        void ws_InsertCompleted3(object sender, BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList3 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }


        void InsertDataTable5(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.InsertCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs>(ws_InsertCompleted5);
            ws.InsertAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName5);

            ///.Progress.Start();
        }

        void ws_InsertCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.InsertCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList5 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }
        void GetTotalRows2(string conn, string db, string procName, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnTotalCompleted2);
            ws.TotalProcRowsAsync(conn, procName, db, parameters);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);
                totalRows2 = total + 1;
                lblRows2.Text = totalRows2.ToString() + " Rows"; ;
                if (total > pageSize2)
                {
                    double totalDouble = (double)(total / (double)pageSize2);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager2.lblTotal.Text = tol.ToString();
                    dataPager2.txtNumber.Text = pageNumber2.ToString();
                    pageTotal2 = tol;
                }
                else
                {
                    dataPager2.lblTotal.Text = "1";
                    dataPager2.txtNumber.Text = pageNumber2.ToString();

                }
            }
            //this.Progress.Stop();
        }

        void GetTotalRows3(string conn, string db, string procName, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnTotalCompleted3);
            ws.TotalProcRowsAsync(conn, procName, db, parameters);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted3(object sender, BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);
                totalRows3 = total + 1;
                lblRows3.Text = totalRows3.ToString() + " Rows"; ;
                if (total > pageSize3)
                {
                    double totalDouble = (double)(total / (double)pageSize3);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager3.lblTotal.Text = tol.ToString();
                    dataPager3.txtNumber.Text = pageNumber3.ToString();
                    pageTotal3 = tol;
                }
                else
                {
                    dataPager3.lblTotal.Text = "1";
                    dataPager3.txtNumber.Text = pageNumber3.ToString();

                }
            }
            //this.Progress.Stop();
        }




        void GetTotalRows4(string conn, string db, string procName, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnTotalCompleted4);
            ws.TotalProcRowsAsync(conn, procName, db, parameters);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted4(object sender, BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);
                totalRows4 = total + 1;
                lblRows4.Text = totalRows4.ToString() + " Rows"; ;
                if (total > pageSize4)
                {
                    double totalDouble = (double)(total / (double)pageSize4);
                    int tol = (int)(totalDouble);
                    if (totalDouble > tol)
                    {
                        tol += 1;
                    }

                    dataPager4.lblTotal.Text = tol.ToString();
                    dataPager4.txtNumber.Text = pageNumber4.ToString();
                    pageTotal4 = tol;
                }
                else
                {
                    dataPager4.lblTotal.Text = "1";
                    dataPager4.txtNumber.Text = pageNumber4.ToString();

                }
            }
            //this.Progress.Stop();
        }


        void GetTotalRows5(string conn, string db, string procName, ObservableCollection<string> parameters)
        {
            var ws = WCF.GetService();
            ws.TotalProcRowsCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs>(ws_ReturnTotalCompleted5);
            ws.TotalProcRowsAsync(conn, procName, db, parameters);

            ///.Progress.Start();
        }

        void ws_ReturnTotalCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.TotalProcRowsCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);

            else
            {
                double total = (double)(e.Result);
                totalRows5 = total + 1;
                lblRows5.Text = totalRows5.ToString() + " Rows"; ;
                //if (total > pageSize5)
                //{
                //    double totalDouble = (double)(total / (double)pageSize5);
                //    int tol = (int)(totalDouble);
                //    if (totalDouble > tol)
                //    {
                //        tol += 1;
                //    }

                //    dataPager5.lblTotal.Text = tol.ToString();
                //    dataPager5.txtNumber.Text = pageNumber5.ToString();
                //    pageTotal5 = tol;
                //}
                //else
                //{
                    dataPager5.lblTotal.Text = "1";
                    dataPager5.txtNumber.Text = pageNumber5.ToString();

               // }
            }
            //this.Progress.Stop();
        }
        private void SetDefault2()
        {
            UserGridConfig config = ddlConfig2.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted2);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 2);
            }
        }


        void ws_updateUserConfigDefaultCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "New default configurating setting have been set" });
                }
                else
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error!, Fail to set new default settings" });
                }
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An Error has occurred while processing your request!" });
            }
              
        }


        private void SetDefault4()
        {
            UserGridConfig config = ddlConfig4.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted4);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 4);
            }
        }


        void ws_updateUserConfigDefaultCompleted4(object sender, BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "New default configurating setting have been set" });
                }
                else
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error!, Fail to set new default settings" });
                }
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An Error has occurred while processing your request!" });
            }
              
        }


        private void SetDefault3()
        {
            UserGridConfig config = ddlConfig3.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted3);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 3);
            }
        }


        void ws_updateUserConfigDefaultCompleted3(object sender, BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "New default configurating setting have been set" });
                }
                else
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error!, Fail to set new default settings" });
                }
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An Error has occurred while processing your request!" });
            }
              
        }


        private void SetDefault5()
        {
            UserGridConfig config = ddlConfig5.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted5);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 5);
            }
        }


        void ws_updateUserConfigDefaultCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "New default configurating setting have been set" });
                }
                else
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error!, Fail to set new default settings" });
                }
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An Error has occurred while processing your request!" });
            }
              
        }


        private void SetDefault6()
        {
            UserGridConfig config = ddlConfig6.SelectedItem as UserGridConfig;
            if (config == null)
                return;

            if (config.Settings_ID > 0)
            {
                var ws = WCF.GetService();
                ws.updateUserConfigDefaultCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs>(ws_updateUserConfigDefaultCompleted6);
                ws.updateUserConfigDefaultAsync(config.UserInformation_ID.Value, config.DisplayDefinition_ID, config.ID, 6);
            }
        }


        void ws_updateUserConfigDefaultCompleted6(object sender, BMA.EOInterface.Middleware.DataTableService.updateUserConfigDefaultCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Successful" }, Content = "New default configurating setting have been set" });
                }
                else
                {

                    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "Error!, Fail to set new default settings" });
                }
            }
            else
            {
                RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Error" }, Content = "An Error has occurred while processing your request!" });
            }

        }
        private void GetColumnData2(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted2);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView2.Columns[dropDownName2]).ItemsSource = list;

                    if (fieldIndex < indexMax)
                    {
                        LoadDropDowns2();
                    }


                }

            }

        }


        private void GetColumnData3(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted3);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted3(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView3.Columns[dropDownName3]).ItemsSource = list;

                    if (fieldIndex3 < indexMax3)
                    {
                        LoadDropDowns3();
                    }


                }

            }

        }

        private void GetColumnData5(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            var ws = WCF.GetService();
            ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetColumnDataSetDataCompleted5);
            ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetColumnDataSetDataCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    ((GridViewComboBoxColumn)this.radGridView5.Columns[dropDownName5]).ItemsSource = list;

                    if (fieldIndex5 < indexMax5)
                    {
                        LoadDropDowns5();
                    }


                }

            }

        }




        private void GetExendedPropertiesData2(string conn, string sql, int pagenumber, int pagesize, object userState)
        {

            //var ws = WCF.GetService();
            // ws.GetDataSetDataCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs>(ws_GetExendedPropertiesDataCompleted2);
            // ws.GetDataSetDataAsync(conn, databaseName, sql, pagenumber, pagesize, userState);
        }


        void ws_GetExendedPropertiesDataCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.GetDataSetDataCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                _tables = e.Result.Tables;
                IEnumerable list = DynamicDataBuilder.GetDataList(e.Result);
                if (e.UserState as string == "Lookup")
                    _lookup = list;
                //radGridView1.ItemsSource = list;
                else
                {


                    columnNames2 = new List<string>();
                    foreach (DataObject dataObject in list)
                    {

                        propertiesList2.Add(dataObject);
                        string column = dataObject.GetFieldValue("ColumnName").ToString();
                        columnNames2.Add(column);
                    }




                }


            }
            LoadExtendedProperties2();
            //  GenerateColumns2();

            // LoadConfigSetting3();

        }

        void DeleteDataTable2(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted2);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName2, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted2(object sender, BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList2 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }
        void DeleteDataTable4(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted4);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName4, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted4(object sender, BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList4 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }

        void DeleteDataTable3(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted3);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName4, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted3(object sender, BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList3 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }
        void DeleteDataTable5(ObservableCollection<DataObject> objectCollection, string strTablename)
        {
            var ws = WCF.GetService();
            ws.DeleteCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs>(ws_DeleteCompleted5);
            ws.DeleteAsync(connString, databaseName, DynamicDataBuilder.GetUpdatedDataSet(objectCollection as IEnumerable, _tables), tableName5, databaseName);

            ///.Progress.Start();
        }

        void ws_DeleteCompleted5(object sender, BMA.EOInterface.Middleware.DataTableService.DeleteCompletedEventArgs e)
        {
            if (e.Error != null)
                Message.ErrorMessage(e.Error.Message);
            else if (e.ServiceError != null)
                Message.ErrorMessage(e.ServiceError.Message);
            else
            {
                newDataObjectList5 = new ObservableCollection<DataObject>();
                Message.InfoMessage(e.Result);
            }
            //this.Progress.Stop();
        }
        #endregion



        #region GridView Events
        private void radGridView_AddingNewDataItem2(object sender, GridViewAddingNewEventArgs e)
        {

        }
        private void radGridView_AddingNewDataItem4(object sender, GridViewAddingNewEventArgs e)
        {

        }
        private void radGridView_AddingNewDataItem3(object sender, GridViewAddingNewEventArgs e)
        {

        }

        private void radGridView_AddingNewDataItem5(object sender, GridViewAddingNewEventArgs e)
        {

        }


        private void radGridView_RowEditEnded2(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList2.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList2.Contains(dataObject))
                        {
                            if (newDataObjectList2.Contains(dataObject))
                            {
                                this.newDataObjectList2.Remove(dataObject);
                                this.newDataObjectList2.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList2.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList2.Remove(dataObject);
                            this.editedDataObjectList2.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }



        private void radGridView_RowEditEnded4(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList4.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList4.Contains(dataObject))
                        {
                            if (newDataObjectList4.Contains(dataObject))
                            {
                                this.newDataObjectList4.Remove(dataObject);
                                this.newDataObjectList4.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList4.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList4.Remove(dataObject);
                            this.editedDataObjectList4.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }

        private void radGridView_RowEditEnded3(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList3.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList3.Contains(dataObject))
                        {
                            if (newDataObjectList3.Contains(dataObject))
                            {
                                this.newDataObjectList3.Remove(dataObject);
                                this.newDataObjectList3.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList3.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList3.Remove(dataObject);
                            this.editedDataObjectList3.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }


        private void radGridView_RowEditEnded5(object sender, GridViewRowEditEndedEventArgs e)
        {
            try
            {
                DataObject dataObject = e.EditedItem as DataObject;
                DataObject newDataObject = e.NewData as DataObject;
                if ((dataObject != null) || (newDataObject != null))
                {
                    if (e.EditOperationType == GridViewEditOperationType.Insert)
                    {
                        //Add the new entry to the data base.
                        newDataObjectList5.Add(newDataObject);
                    }
                    if (e.EditOperationType == GridViewEditOperationType.Edit)
                    {
                        if (!this.editedDataObjectList5.Contains(dataObject))
                        {
                            if (newDataObjectList5.Contains(dataObject))
                            {
                                this.newDataObjectList5.Remove(dataObject);
                                this.newDataObjectList5.Add(dataObject);
                            }
                            else
                            {
                                this.editedDataObjectList5.Add(dataObject);
                            }
                        }
                        else
                        {
                            this.editedDataObjectList5.Remove(dataObject);
                            this.editedDataObjectList5.Add(dataObject);
                        }
                    }
                }

            }
            catch
            {

            }
        }

        private void radGridView_Filtered2(object sender, GridViewFilteredEventArgs e)
        {


            PullData2();
        }

        private void radGridView_Filtered4(object sender, GridViewFilteredEventArgs e)
        {


            PullData4();
        }
        private void radGridView_Filtered3(object sender, GridViewFilteredEventArgs e)
        {


            PullData3();
        }

        private void radGridView_Filtered5(object sender, GridViewFilteredEventArgs e)
        {


            PullData5();
        }
        private void createSqlWithNewFilters2()
        {
            DataObject dataObject = this.radGridView1.SelectedItem as DataObject;

            string newSql = "";
            string gridrship = "";

            //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)

            foreach (GridRelationship rship in GridRelationshipList)
            {
                try
                {
                    //ViewFieldSummary field = fieldList.Where(f => f.ID == rship.ViewField1).FirstOrDefault();
                    //  ViewFieldSummary field2 = fieldList2.Where(f => f.ID == rship.ViewField2).FirstOrDefault();
                    //    string parameterValue = dataObject.GetFieldValue(field.FieldName).ToString();
                    //  gridrship += " " + field2.TableField + " = '" + parameterValue + "' and";
                }
                catch { }
            }
            if (gridrship != string.Empty)
            {
                gridrship = gridrship.TrimEnd('d');
                gridrship = gridrship.TrimEnd('n');
                gridrship = gridrship.TrimEnd('a');
            }



            if (sqlText2.Contains("Where"))
            {
                newSqlText2 = sqlText2 + " and " + gridrship;
            }
            else
            {
                newSqlText2 = sqlText2 + " Where " + gridrship;
            }




            if (radGridView2.FilterDescriptors.Count > 0)
            {


                newSql += OperatorHandler.BulidQuery(radGridView2);



            }

            if (newSql.Length > 0)
            {
                newSql = newSql.Remove(newSql.Length - 3, 3);

                if (newSqlText2.Contains("Where"))
                {
                    newSqlText2 = newSqlText2 + " and " + newSql;
                }
                else
                {
                    newSqlText2 = newSqlText2 + " Where " + newSql;
                }
            }
            //  else
            // {
            // newSqlText2 = sqlText2;
            // }
            // GetTotalRows2(newSqlText2);
            pageNumber2 = 1;
            // GetData2(newSqlText2, pageNumber2, pageSize2, tableName2);
        }

        private void createSqlWithNewFilters()
        {
            string newSql = "";


            //    foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)


            if (radGridView1.FilterDescriptors.Count > 0)
            {


                newSql += OperatorHandler.BulidQuery(radGridView1);



            }

            if (newSql.Length > 0)
            {
                newSql = newSql.Remove(newSql.Length - 3, 3);

                if (sqlText.Contains("Where"))
                {
                    newSqlText = sqlText + " and " + newSql;
                }
                else
                {
                    newSqlText = sqlText + " Where " + newSql;
                }
            }
            else
            {
                newSqlText = sqlText;

            }

            //  GetTotalRows(newSqlText);
            //  pageNumber = 1;
            // GetData(newSqlText, pageNumber, pageSize, tableName);
        }

        void gridView_Deleted2(object sender, GridViewDeletedEventArgs e)
        {
            DataObject dataObject = e.Items as DataObject;
            deletedDataObjectList2.Add(dataObject);



        }


        private void gridView_LoadingRowDetails2(object sender, GridViewRowDetailsEventArgs e)
        {
            //RadComboBox countries = e.DetailsElement.FindName("rcbCountries") as RadComboBox;
            //countries.ItemsSource = GetCountries();
            // e.Row.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
        }
        #endregion


        #region GridMenu Events & Methods
        public MouseButtonEventHandler gridRightClick2 { get; set; }








        void tab_MouseRightButtonDown2(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            // tab_MouseRightButtonUp(sender, e);
            tab_MouseRightButtonUp2(sender, e);
            cMenu2.StaysOpen = true;
        }
        RadContextMenu cMenu2;
        RadTabItem selectedTab2;
        void tab_MouseRightButtonUp2(object sender, MouseButtonEventArgs e)
        {
            var element = sender as UIElement;


            cMenu2 = new RadContextMenu();
            RadMenuItem menuItem;

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Changes";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Add";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Edit";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Selected Rows";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Export";
            RadMenuItem exportItem = new RadMenuItem();
            exportItem = new RadMenuItem();
            exportItem.Header = "Excel";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "ExcelML";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Word";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            exportItem = new RadMenuItem();
            exportItem.Header = "Csv";
            exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            menuItem.Items.Add(exportItem);
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Save Filter Settings As";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Delete Filter Settings";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Set Current Filter Settings as Defualt";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);

            menuItem = new RadMenuItem();
            menuItem.Header = "Cancel";
            menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
            cMenu2.Items.Add(menuItem);
            cMenu2.PlacementTarget = Docking;

            Point p = e.GetPosition(this.Docking);


            cMenu2.Placement = PlacementMode.MousePoint;

            // cMenu.IconColumnWidth = 0;
            cMenu2.HorizontalOffset = p.X - 10;
            cMenu2.VerticalOffset = p.Y + 50;


            cMenu2.IsOpen = true;



        }
        void RadGridView1_RowLoaded2(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewRow && !(e.Row is GridViewNewRow))
            {

                //  ((GridViewRow)e.Row).MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp2);
                // ((GridViewRow)e.Row).MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown2);

            }
        }


        void menuItem_Click2(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges2();
                        break;
                    case "Add":
                        AddNewItem2();
                        break;
                    case "Edit":
                        radGridView2.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows2();
                        break;

                    case "Save Filter Settings":
                        SaveConfig2();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs2();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig2();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault2();
                        break;


                    case "Import":
                        Import2(header);
                        break;

                    case "Excel":
                        Export2(header);
                        break;

                    case "Word":
                        Export2(header);
                        break;

                    case "ExcelML":
                        Export2(header);
                        break;
                    case "Csv":
                        Export2(header);
                        break;
                    case "Cancel":
                        Cancel2();
                        break;
                    default:
                        break;
                }

            }
        }

        private void RadContextMenu_ItemClick2(object sender, RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges2();
                        break;
                    case "Add New":
                        radGridView2.BeginInsert();
                        break;
                    case "Edit":
                        radGridView2.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows2();
                        break;

                    case "Save Filter Settings":
                        SaveConfig2();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs2();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig2();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault2();
                        break;


                    case "Excel":
                        Export2(header);
                        break;

                    case "Word":
                        Export2(header);
                        break;

                    case "ExcelML":
                        Export2(header);
                        break;
                    case "Csv":
                        Export2(header);
                        break;
                    case "Cancel":
                        Cancel2();
                        break;
                    default:
                        break;
                }
            }
            // cMenu.IsOpen = false;
        }

        private void Export2(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView2.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }


        private void RadContextMenu_Opened2(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion


        #region MyRegion

        private void Attach2()
        {
            if (radGridView2 != null)
            {

                // create menu
                RadContextMenu contextMenu2 = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu2, StyleManager.GetTheme(radGridView2));

                contextMenu2.Opened += OnMenuOpened2;
                contextMenu2.ItemClick += OnMenuItemClick2;

                RadContextMenu.SetContextMenu(radGridView2, contextMenu2);
            }
        }
        void OnMenuOpened2(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);

                // create menu items
                foreach (GridViewColumn column in radGridView2.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.Header;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Changes";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Add";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Edit";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Selected Rows";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Import";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click2);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }

        void OnMenuItemClick2(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView2.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView2.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView2.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView2.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView2.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView2.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView2.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView2.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView2.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView2.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView2.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }

        private void Import2(string extension)
        {
            ImportWizard view = new ImportWizard(tableName2, ModelID, fieldList2.ToList(), viewID2, serverID, CreateSPID2, true);
            //view.Closed += view_Closed;// +=new RadEventHandler(view_Closed);
            //view.WindowStartupLocation
            // view.Owner = 
            view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
            view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
            view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            view.ShowDialog();

        }
        #endregion


        #endregion

        #region MyRegion
        void menuItem_Click3(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges3();
                        break;
                    case "Add":
                        AddNewItem3();
                        break;
                    case "Edit":
                        radGridView3.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows3();
                        break;

                    case "Save Filter Settings":
                        SaveConfig3();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs3();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig3();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault3();
                        break;
                    case "Excel":
                        Export3(header);
                        break;

                   
                    case "Import":
                        Import3(header);
                        break;

                    case "Excel File":
                        Import3(header);
                        break;

                    case "Word":
                        Export3(header);
                        break;

                    case "ExcelML":
                        Export3(header);
                        break;
                    case "Csv":
                        Export3(header);
                        break;
                    case "Cancel":
                        Cancel3();
                        break;
                    default:
                        break;
                }

            }
        }
        private void Attach3()
        {
            if (radGridView3 != null)
            {

                // create menu
                RadContextMenu contextMenu3 = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu3, StyleManager.GetTheme(radGridView3));

                contextMenu3.Opened += OnMenuOpened3;
                contextMenu3.ItemClick += OnMenuItemClick3;

                RadContextMenu.SetContextMenu(radGridView3, contextMenu3);
            }
        }
        void OnMenuOpened3(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);

                // create menu items
                foreach (GridViewColumn column in radGridView3.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.Header;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Changes";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Add";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Edit";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Selected Rows";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);
                menuItem = new RadMenuItem();
                menuItem.Header = "Import";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);
                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }

        void OnMenuItemClick3(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView3.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView3.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView3.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView3.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView3.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView3.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView3.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView3.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView3.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView3.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView3.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }

        private void Export3(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView3.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }

        private void Import3(string extension)
        {


            ImportWizard view = new ImportWizard(tableName3, ModelID, fieldList3.ToList(), viewID3,serverID, CreateSPID3,true);
            //view.Closed += view_Closed;// +=new RadEventHandler(view_Closed);
            //view.WindowStartupLocation
            // view.Owner = 
            view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
            view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
            view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            view.ShowDialog();
            
        }



        private void InsertImportData3()
        {

        }

        

        private void RadContextMenu_Opened3(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion


        #region MyRegion 4
        void menuItem_Click4(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges4();
                        break;
                    case "Add":
                        AddNewItem4();
                        break;
                    case "Edit":
                        radGridView4.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows4();
                        break;

                    case "Save Filter Settings":
                        SaveConfig4();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs4();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig4();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault4();
                        break;


                    case "Excel":
                        Export4(header);
                        break;


                    case "Import":
                        Import4(header);
                        break;

                    case "Word":
                        Export4(header);
                        break;

                    case "ExcelML":
                        Export4(header);
                        break;
                    case "Csv":
                        Export4(header);
                        break;
                    case "Cancel":
                        Cancel4();
                        break;
                    default:
                        break;
                }

            }
        }
        private void Attach4()
        {
            if (radGridView4 != null)
            {

                // create menu
                RadContextMenu contextMenu4 = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu4, StyleManager.GetTheme(radGridView4));

                contextMenu4.Opened += OnMenuOpened4;
                contextMenu4.ItemClick += OnMenuItemClick4;

                RadContextMenu.SetContextMenu(radGridView4, contextMenu4);
            }
        }
        void OnMenuOpened4(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);

                // create menu items
                foreach (GridViewColumn column in radGridView4.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.Header;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Changes";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Add";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Edit";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Selected Rows";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Import";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click3);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click4);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }

        void OnMenuItemClick4(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView4.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView4.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView4.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView4.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView3.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView4.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView4.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView4.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView4.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView4.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView4.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }

        private void Export4(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView4.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }



        private void Import4(string extension)
        {


            ImportWizard view = new ImportWizard(tableName4, ModelID, fieldList4.ToList(), viewID4, serverID, CreateSPID4,true);
            //view.Closed += view_Closed;// +=new RadEventHandler(view_Closed);
            //view.WindowStartupLocation
            // view.Owner = 
            view.Left = (Application.Current.Host.Content.ActualWidth - view.ActualWidth) / 2;
            view.Top = (Application.Current.Host.Content.ActualHeight - view.ActualHeight) / 2;
            view.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            view.ShowDialog();

        }



        private void RadContextMenu_Opened4(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }
        #endregion



        #region MyRegion 5
        void menuItem_Click5(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    case "Save Changes":
                        SaveChanges5();
                        break;
                    case "Add New":
                        radGridView5.BeginInsert();
                        break;
                    case "Edit":
                        radGridView5.BeginEdit();
                        break;
                    case "Delete Selected Rows":
                        DeleteRows5();
                        break;

                    case "Save Filter Settings":
                        SaveConfig5();
                        break;

                    case "Save Filter Settings As":
                        SaveConfigAs5();
                        break;

                    case "Delete Filter Settings":
                        DeleteConfig5();
                        break;


                    case "Set Current Filter Settings as Defualt":
                        SetDefault5();
                        break;


                    case "Excel":
                        Export5(header);
                        break;

                    case "Word":
                        Export5(header);
                        break;

                    case "ExcelML":
                        Export5(header);
                        break;
                    case "Csv":
                        Export5(header);
                        break;
                    case "Cancel":
                        Cancel5();
                        break;
                    default:
                        break;
                }

            }
        }
        private void Attach5()
        {
            if (radGridView5 != null)
            {

                // create menu
               
                RadContextMenu contextMenu5 = new RadContextMenu();
                contextMenu5.MaxHeight = 500;
                // set menu Theme
                StyleManager.SetTheme(contextMenu5, StyleManager.GetTheme(radGridView5));

                contextMenu5.Opened += OnMenuOpened5;
                contextMenu5.ItemClick += OnMenuItemClick5;

                RadContextMenu.SetContextMenu(radGridView5, contextMenu5);
            }
        }
        void OnMenuOpened5(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
            GridViewCell gridCell = menu.GetClickedElement<GridViewCell>();

            if (cell != null)
            {
                menu.Items.Clear();

                RadMenuItem item = new RadMenuItem();
                item.Header = String.Format(@"Sort Ascending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Sort Descending by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Clear Sorting by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Group by ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = String.Format(@"Ungroup ""{0}""", cell.Column.Header);
                menu.Items.Add(item);

                item = new RadMenuItem();
                item.Header = "Choose Columns:";
                menu.Items.Add(item);
                menu.MaxHeight = 500;
                item.MaxHeight = 500;
                // create menu items
                foreach (GridViewColumn column in radGridView5.Columns)
                {
                    RadMenuItem subMenu = new RadMenuItem();
                    subMenu.Header = column.UniqueName;
                    subMenu.IsCheckable = true;
                    subMenu.IsChecked = true;

                    Binding isCheckedBinding = new Binding("IsVisible");
                    isCheckedBinding.Mode = BindingMode.TwoWay;
                    isCheckedBinding.Source = column;

                    // bind IsChecked menu item property to IsVisible column property
                    subMenu.SetBinding(RadMenuItem.IsCheckedProperty, isCheckedBinding);

                    item.Items.Add(subMenu);
                }
            }
            else if (gridCell != null)
            {
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

                //menuItem = new RadMenuItem();
                //menuItem.Header = "Save Changes";
                //menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                //menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = "Add";
                //menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                //menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = "Edit";
                //menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                //menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = "Delete Selected Rows";
                //menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                //menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Export";
                RadMenuItem exportItem = new RadMenuItem();
                exportItem = new RadMenuItem();
                exportItem.Header = "Excel";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "ExcelML";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Word";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menuItem.Items.Add(exportItem);
                exportItem = new RadMenuItem();
                exportItem.Header = "Csv";
                exportItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menuItem.Items.Add(exportItem);
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Filter Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete Filter Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Filter Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click5);
                menu.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Header = " ";

                //cMenu.Items.Add(menuItem);
                //cMenu.PlacementTarget = radGridView1;

                //Point p = e.GetPosition(this.radGridView1);


                //cMenu.Placement = PlacementMode.MousePoint;

                //// cMenu.IconColumnWidth = 0;
                //cMenu.HorizontalOffset = p.X - 10;
                //cMenu.VerticalOffset = p.Y + 20;


                //cMenu.IsOpen = true;

            }

            else
            {
                menu.IsOpen = false;
            }
            //}
        }



        void OnMenuItemClick5(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

                GridViewHeaderCell cell = menu.GetClickedElement<GridViewHeaderCell>();
                RadMenuItem clickedItem = ((RadRoutedEventArgs)e).OriginalSource as RadMenuItem;
                GridViewColumn column = cell.Column;

                if (clickedItem.Parent is RadMenuItem)
                    return;

                string header = Convert.ToString(clickedItem.Header);

                using (radGridView5.DeferRefresh())
                {
                    ColumnSortDescriptor sd = (from d in radGridView5.SortDescriptors.OfType<ColumnSortDescriptor>()
                                               where object.Equals(d.Column, column)
                                               select d).FirstOrDefault();

                    if (header.Contains("Sort Ascending"))
                    {
                        if (sd != null)
                        {
                            radGridView5.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Ascending;

                        radGridView5.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Sort Descending"))
                    {
                        if (sd != null)
                        {
                            radGridView5.SortDescriptors.Remove(sd);
                        }

                        ColumnSortDescriptor newDescriptor = new ColumnSortDescriptor();
                        newDescriptor.Column = column;
                        newDescriptor.SortDirection = ListSortDirection.Descending;

                        radGridView5.SortDescriptors.Add(newDescriptor);
                    }
                    else if (header.Contains("Clear Sorting"))
                    {
                        if (sd != null)
                        {
                            radGridView5.SortDescriptors.Remove(sd);
                        }
                    }
                    else if (header.Contains("Group by"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView5.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();

                        if (gd == null)
                        {
                            ColumnGroupDescriptor newDescriptor = new ColumnGroupDescriptor();
                            newDescriptor.Column = column;
                            newDescriptor.SortDirection = ListSortDirection.Ascending;
                            radGridView5.GroupDescriptors.Add(newDescriptor);
                        }
                    }
                    else if (header.Contains("Ungroup"))
                    {
                        ColumnGroupDescriptor gd = (from d in radGridView5.GroupDescriptors.OfType<ColumnGroupDescriptor>()
                                                    where object.Equals(d.Column, column)
                                                    select d).FirstOrDefault();
                        if (gd != null)
                        {
                            radGridView5.GroupDescriptors.Remove(gd);
                        }
                    }
                }
            }
            catch
            {
                // proceed its not a header cell
            }
        }

        private void Export5(string selectedItem)
        {
            try
            {
                string extension = "";
                ExportFormat format = ExportFormat.Html;



                switch (selectedItem)
                {
                    case "Excel": extension = "xls";
                        format = ExportFormat.Html;
                        break;
                    case "ExcelML": extension = "xml";
                        format = ExportFormat.ExcelML;
                        break;
                    case "Word": extension = "doc";
                        format = ExportFormat.Html;
                        break;
                    case "Csv": extension = "csv";
                        format = ExportFormat.Csv;
                        break;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = extension;
                dialog.Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, selectedItem);
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() == true)
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        GridViewExportOptions exportOptions = new GridViewExportOptions();
                        exportOptions.Format = format;
                        exportOptions.ShowColumnFooters = true;
                        exportOptions.ShowColumnHeaders = true;
                        exportOptions.ShowGroupFooters = true;

                        radGridView5.Export(stream, exportOptions);
                    }
                }
            }
            catch { }
        }


        private void RadContextMenu_Opened5(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }





        private void Attach6()
        {
            
            if (radGridView5 != null)
            {

                // create menu
                RadContextMenu contextMenu6 = new RadContextMenu();
                // set menu Theme
                StyleManager.SetTheme(contextMenu6, StyleManager.GetTheme(radGridView5));

                contextMenu6.Opened += OnMenuOpened6;
                contextMenu6.ItemClick += OnMenuItemClick6;

                RadContextMenu.SetContextMenu(gPanel, contextMenu6);
            }
        }
        void OnMenuOpened6(object sender, RoutedEventArgs e)
        {
            //if (isHeader)
            //{
            RadContextMenu menu = (RadContextMenu)sender;
          

           
      
                menu.Items.Clear();
                //cMenu = new RadContextMenu();
                RadMenuItem menuItem;

             

               

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click6);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Save Settings As";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click6);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Delete  Settings";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click6);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Set Current Settings as Defualt";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click6);
                menu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Header = "Cancel";
                menuItem.Click += new RadRoutedEventHandler(menuItem_Click6);
                menu.Items.Add(menuItem);

                
            
        }

        void menuItem_Click6(object sender, RadRoutedEventArgs e)
        {
            RadMenuItem menu = sender as RadMenuItem;

            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            //  GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {

                    

                    case "Save Settings":
                        SaveConfig6();
                        break;

                    case "Save Settings As":
                        SaveConfigAs6();
                        break;

                    case "Delete Settings":
                        DeleteConfig6();
                        break;


                    case "Set Current Settings as Defualt":
                        SetDefault6();
                        break;


                  
                    default:
                        break;
                }

            }
        }

        void OnMenuItemClick6(object sender, RoutedEventArgs e)
        {
            try
            {
                RadContextMenu menu = (RadContextMenu)sender;

            }
            catch { }
        }

        public T FindControl<T>(UIElement parent, Type targetType, string ControlName) where T : FrameworkElement
        {

            if (parent == null) return null;

            if (parent.GetType() == targetType && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }
            T result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                if (FindControl<T>(child, targetType, ControlName) != null)
                {
                    result = FindControl<T>(child, targetType, ControlName);
                    break;
                }
            }
            return result;
        }
        #endregion
    }


    public class GraphColumn
    {
        public int index { get; set; }
        public bool Show { get; set; }
        public string ColumnName { get; set; }
        public string GroupName { get; set; }
        public string DisplayName { get; set; }
        public string GraphType { get; set; }
    }

    public class DropDownIntData
    {
        public int Value { get; set; }
        public string Display { get; set; }
    }
    public class DropDownLongData
    {
        public long Value { get; set; }
        public string Display { get; set; }
    }

    public class DropDownStringData
    {
        public string Value { get; set; }
        public string Display { get; set; }
    }

    public class ColorContainer
    {
        public Color Colour { get; set; }
        public int Index { get; set; }
    }

    public class Color
    {
        public string ColorName { get; set; }
        public uint ColorCode { get; set; }
        public int Index { get; set; }
    }

    public class AllColorsHelper
    {
        public AllColorsHelper()
        {
            PredefinedColors = new List<Color> 
      {
         new Color{ColorName = "Aliceblue", ColorCode = 0xFFF0F8FF, Index=1},
         new Color{ColorName = "AntiqueWhite", ColorCode = 0xFFFAEBD7, Index=2},
         new Color{ColorName = "Aqua", ColorCode = 0xFF00FFFF, Index=3},
         new Color{ColorName = "Aquamarine", ColorCode = 0xFF7FFFD4, Index=4},
         new Color{ColorName = "Azure", ColorCode = 0xFFF0FFFF, Index=5},
         new Color{ColorName = "Beige", ColorCode = 0xFFF5F5DC, Index=6},
         new Color{ColorName = "Bisque", ColorCode = 0xFFFFE4C4, Index=7},
         new Color{ColorName = "Black", ColorCode = 0xFF000000, Index=8},
         new Color{ColorName = "BlanchedAlmond", ColorCode = 0xFFFFEBCD, Index=9},
         new Color{ColorName = "Blue", ColorCode = 0xFF0000FF, Index=10},
         new Color{ColorName = "BlueViolet", ColorCode = 0xFF8A2BE2, Index=11},
         new Color{ColorName = "Brown", ColorCode = 0xFFA52A2A, Index=12},
         new Color{ColorName = "BurlyWood", ColorCode = 0xFFDEB887, Index=13},
         new Color{ColorName = "CadetBlue", ColorCode = 0xFF5F9EA0, Index=14},
         new Color{ColorName = "Chartreuse", ColorCode = 0xFF7FFF00, Index=15},
         new Color{ColorName = "Chocolate", ColorCode = 0xFFD2691E, Index=16},
         new Color{ColorName = "Coral", ColorCode = 0xFFFF7F50, Index=17},
         new Color{ColorName = "CornflowerBlue", ColorCode = 0xFF6495ED, Index=18},
         new Color{ColorName = "Cornsilk", ColorCode = 0xFFFFF8DC, Index=19},
         new Color{ColorName = "Crimson", ColorCode = 0xFFDC143C, Index=20},
         new Color{ColorName = "Cyan", ColorCode = 0xFF00FFFF, Index=21},
         new Color{ColorName = "DarkBlue", ColorCode = 0xFF00008B, Index=22},
         new Color{ColorName = "DarkCyan", ColorCode = 0xFF008B8B, Index=23},
         new Color{ColorName = "DarkGoldenrod", ColorCode = 0xFFB8860B, Index=24},
         new Color{ColorName = "DarkGray", ColorCode = 0xFFA9A9A9, Index=25},
         new Color{ColorName = "DarkGreen", ColorCode = 0xFF006400, Index=26},
         new Color{ColorName = "DarkKhaki", ColorCode = 0xFFBDB76B, Index=27},
         new Color{ColorName = "DarkMagenta", ColorCode = 0xFF8B008B, Index=28},
         new Color{ColorName = "DarkOliveGreen", ColorCode = 0xFF556B2F, Index=29},
         new Color{ColorName = "DarkOrange", ColorCode = 0xFFFF8C00, Index=30},
         new Color{ColorName = "DarkOrchid", ColorCode = 0xFF9932CC},
         new Color{ColorName = "DarkRed", ColorCode = 0xFF8B0000},
         new Color{ColorName = "DarkSalmon", ColorCode = 0xFFE9967A},
         new Color{ColorName = "DarkSeaGreen", ColorCode = 0xFF8FBC8F},
         new Color{ColorName = "DarkSlateBlue", ColorCode = 0xFF483D8B},
         new Color{ColorName = "DarkSlateGray", ColorCode = 0xFF2F4F4F},
         new Color{ColorName = "DarkTurquoise", ColorCode = 0xFF00CED1},
         new Color{ColorName = "DarkViolet", ColorCode = 0xFF9400D3},
         new Color{ColorName = "DeepPink", ColorCode = 0xFFFF1493},
         new Color{ColorName = "DeepSkyBlue", ColorCode = 0xFF00BFFF},
         new Color{ColorName = "DimGray", ColorCode = 0xFF696969},
         new Color{ColorName = "DodgerBlue", ColorCode = 0xFF1E90FF},
         new Color{ColorName = "Firebrick", ColorCode = 0xFFB22222},
         new Color{ColorName = "FloralWhite", ColorCode = 0xFFFFFAF0},
         new Color{ColorName = "ForestGreen", ColorCode = 0xFF228B22},
         new Color{ColorName = "Fuchsia", ColorCode = 0xFFFF00FF},
         new Color{ColorName = "Gainsboro", ColorCode = 0xFFDCDCDC},
         new Color{ColorName = "GhostWhite", ColorCode = 0xFFF8F8FF},
         new Color{ColorName = "Gold", ColorCode = 0xFFFFD700},
         new Color{ColorName = "Goldenrod", ColorCode = 0xFFDAA520},
         new Color{ColorName = "Gray", ColorCode = 0xFF808080},
         new Color{ColorName = "Green", ColorCode = 0xFF008000},
         new Color{ColorName = "GreenYellow", ColorCode = 0xFFADFF2F},
         new Color{ColorName = "Honeydew", ColorCode = 0xFFF0FFF0},
         new Color{ColorName = "HotPink", ColorCode = 0xFFFF69B4},
         new Color{ColorName = "IndianRed", ColorCode = 0xFFCD5C5C},
         new Color{ColorName = "Indigo", ColorCode = 0xFF4B0082},
         new Color{ColorName = "Ivory", ColorCode = 0xFFFFFFF0},
         new Color{ColorName = "Khaki", ColorCode = 0xFFF0E68C},
         new Color{ColorName = "Lavender", ColorCode = 0xFFE6E6FA},
         new Color{ColorName = "LavenderBlush", ColorCode = 0xFFFFF0F5},
         new Color{ColorName = "LawnGreen", ColorCode = 0xFF7CFC00},
         new Color{ColorName = "LemonChiffon", ColorCode = 0xFFFFFACD},
         new Color{ColorName = "LightBlue", ColorCode = 0xFFADD8E6},
         new Color{ColorName = "LightCoral", ColorCode = 0xFFF08080},
         new Color{ColorName = "LightCyan", ColorCode = 0xFFE0FFFF},
         new Color{ColorName = "LightGoldenrodYellow", ColorCode = 0xFFFAFAD2},
         new Color{ColorName = "LightGray", ColorCode = 0xFFD3D3D3},
         new Color{ColorName = "LightGreen", ColorCode = 0xFF90EE90},
         new Color{ColorName = "LightPink", ColorCode = 0xFFFFB6C1},
         new Color{ColorName = "LightSalmon", ColorCode = 0xFFFFA07A},
         new Color{ColorName = "LightSeaGreen", ColorCode = 0xFF20B2AA},
         new Color{ColorName = "LightSkyBlue", ColorCode = 0xFF87CEFA},
         new Color{ColorName = "LightSlateGray", ColorCode = 0xFF778899},
         new Color{ColorName = "LightSteelBlue", ColorCode = 0xFFB0C4DE},
         new Color{ColorName = "LightYellow", ColorCode = 0xFFFFFFE0},
         new Color{ColorName = "Lime", ColorCode = 0xFF00FF00},
         new Color{ColorName = "LimeGreen", ColorCode = 0xFF32CD32},
         new Color{ColorName = "Linen", ColorCode = 0xFFFAF0E6},
         new Color{ColorName = "Magenta", ColorCode = 0xFFFF00FF},
         new Color{ColorName = "Maroon", ColorCode = 0xFF800000},
         new Color{ColorName = "MediumAquamarine", ColorCode = 0xFF66CDAA},
         new Color{ColorName = "MediumBlue", ColorCode = 0xFF0000CD},
         new Color{ColorName = "MediumOrchid", ColorCode = 0xFFBA55D3},
         new Color{ColorName = "MediumPurple", ColorCode = 0xFF9370DB},
         new Color{ColorName = "MediumSeaGreen", ColorCode = 0xFF3CB371},
         new Color{ColorName = "MediumSlateBlue", ColorCode = 0xFF7B68EE},
         new Color{ColorName = "MediumSpringGreen", ColorCode = 0xFF00FA9A},
         new Color{ColorName = "MediumTurquoise", ColorCode = 0xFF48D1CC},
         new Color{ColorName = "MediumVioletRed", ColorCode = 0xFFC71585},
         new Color{ColorName = "MidnightBlue", ColorCode = 0xFF191970},
         new Color{ColorName = "MintCream", ColorCode = 0xFFF5FFFA},
         new Color{ColorName = "MistyRose", ColorCode = 0xFFFFE4E1},
         new Color{ColorName = "Moccasin", ColorCode = 0xFFFFE4B5},
         new Color{ColorName = "NavajoWhite", ColorCode = 0xFFFFDEAD},
         new Color{ColorName = "Navy", ColorCode = 0xFF000080},
         new Color{ColorName = "OldLace", ColorCode = 0xFFFDF5E6},
         new Color{ColorName = "Olive", ColorCode = 0xFF808000},
         new Color{ColorName = "OliveDrab", ColorCode = 0xFF6B8E23},
         new Color{ColorName = "Orange", ColorCode = 0xFFFFA500},
         new Color{ColorName = "OrangeRed", ColorCode = 0xFFFF4500},
         new Color{ColorName = "Orchid", ColorCode = 0xFFDA70D6},
         new Color{ColorName = "PaleGoldenrod", ColorCode = 0xFFEEE8AA},
         new Color{ColorName = "PaleGreen", ColorCode = 0xFF98FB98},
         new Color{ColorName = "PaleTurquoise", ColorCode = 0xFFAFEEEE},
         new Color{ColorName = "PaleVioletRed", ColorCode = 0xFFDB7093},
         new Color{ColorName = "PapayaWhip", ColorCode = 0xFFFFEFD5},
         new Color{ColorName = "PeachPuff", ColorCode = 0xFFFFDAB9},
         new Color{ColorName = "Peru", ColorCode = 0xFFCD853F},
         new Color{ColorName = "Pink", ColorCode = 0xFFFFC0CB},
         new Color{ColorName = "Plum", ColorCode = 0xFFDDA0DD},
         new Color{ColorName = "PowderBlue", ColorCode = 0xFFB0E0E6},
         new Color{ColorName = "Purple", ColorCode = 0xFF800080},
         new Color{ColorName = "Red", ColorCode = 0xFFFF0000},
         new Color{ColorName = "RosyBrown", ColorCode = 0xFFBC8F8F},
         new Color{ColorName = "RoyalBlue", ColorCode = 0xFF4169E1},
         new Color{ColorName = "SaddleBrown", ColorCode = 0xFF8B4513},
         new Color{ColorName = "Salmon", ColorCode = 0xFFFA8072},
         new Color{ColorName = "SandyBrown", ColorCode = 0xFFF4A460},
         new Color{ColorName = "SeaGreen", ColorCode = 0xFF2E8B57},
         new Color{ColorName = "SeaShell", ColorCode = 0xFFFFF5EE},
         new Color{ColorName = "Sienna", ColorCode = 0xFFA0522D},
         new Color{ColorName = "Silver", ColorCode = 0xFFC0C0C0},
         new Color{ColorName = "SkyBlue", ColorCode = 0xFF87CEEB},
         new Color{ColorName = "SlateBlue", ColorCode = 0xFF6A5ACD},
         new Color{ColorName = "SlateGray", ColorCode = 0xFF708090},
         new Color{ColorName = "Snow", ColorCode = 0xFFFFFAFA},
         new Color{ColorName = "SpringGreen", ColorCode = 0xFF00FF7F},
         new Color{ColorName = "SteelBlue", ColorCode = 0xFF4682B4},
         new Color{ColorName = "Tan", ColorCode = 0xFFD2B48C},
         new Color{ColorName = "Teal", ColorCode = 0xFF008080},
         new Color{ColorName = "Thistle", ColorCode = 0xFFD8BFD8},
         new Color{ColorName = "Tomato", ColorCode = 0xFFFF6347},
         new Color{ColorName = "Transparent", ColorCode = 0x00FFFFFF},
         new Color{ColorName = "Turquoise", ColorCode = 0xFF40E0D0},
         new Color{ColorName = "Violet", ColorCode = 0xFFEE82EE},
         new Color{ColorName = "Wheat", ColorCode = 0xFFF5DEB3},
         new Color{ColorName = "White", ColorCode = 0xFFFFFFFF},
         new Color{ColorName = "WhiteSmoke", ColorCode = 0xFFF5F5F5},
         new Color{ColorName = "Yellow", ColorCode = 0xFFFFFF00},
         new Color{ColorName = "YellowGreen", ColorCode = 0xFF9ACD32}
       };
        }
        public List<Color> PredefinedColors { get; set; }
    }


}
