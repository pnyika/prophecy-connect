﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp.Controls
{
    public partial class RightClickMenu : UserControl 
    {
        
        public static event EventHandler Save;
        public static event EventHandler AddNew;
        public static event EventHandler Delete;
        public static event EventHandler Cancel;
        public static event EventHandler SaveFilter;
        public static event EventHandler SetDefault;
        public static event EventHandler Export;



        public RightClickMenu()
        {
            InitializeComponent();
        }

        private void Save_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
         Save(sender, e);
         
        }

        private void AddNew_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            AddNew(sender, e);
        }

        private void lblDelete_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Delete(sender, e);
        }

        private void lblSaveFilter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SaveFilter(sender, e);
        }

        private void lblsetDefault_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SetDefault(sender, e);
        }

        private void lblCancel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Cancel(sender, e);
        }

        private void lblExport_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Export(sender, e);
        }

        private void lblSave_MouseMove(object sender, MouseEventArgs e)
        {
            lblSave.Background = new SolidColorBrush(Colors.Gray);
        }

        private void lblCancel_MouseMove(object sender, MouseEventArgs e)
        {
            lblCancel.Background = new SolidColorBrush(Colors.Gray);
        }

        private void lblCancel_MouseLeave(object sender, MouseEventArgs e)
        {
            lblCancel.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void lblsetDefault_MouseLeave(object sender, MouseEventArgs e)
        {
            lblsetDefault.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void lblsetDefault_MouseLeave_1(object sender, MouseEventArgs e)
        {
            lblsetDefault.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void lblsetDefault_MouseMove(object sender, MouseEventArgs e)
        {
            lblsetDefault.Background = new SolidColorBrush(Colors.Gray);
        }

        private void lblSaveFilter_MouseMove(object sender, MouseEventArgs e)
        {
            lblSaveFilter.Background = new SolidColorBrush(Colors.Gray);
        }

        private void lblSaveFilter_MouseLeave(object sender, MouseEventArgs e)
        {
            lblSaveFilter.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void lblExport_MouseMove(object sender, MouseEventArgs e)
        {
            lblExport.Background = new SolidColorBrush(Colors.Gray);
        }

        private void lblExport_MouseLeave(object sender, MouseEventArgs e)
        {
            lblExport.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void lblDelete_MouseMove(object sender, MouseEventArgs e)
        {
            lblDelete.Background = new SolidColorBrush(Colors.Gray);
        }

        private void lblDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            lblDelete.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void lblAddNew_MouseMove(object sender, MouseEventArgs e)
        {
            lblAddNew.Background = new SolidColorBrush(Colors.Gray);
        }

        private void lblAddNew_MouseLeave(object sender, MouseEventArgs e)
        {
            lblAddNew.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void lblSave_MouseLeave(object sender, MouseEventArgs e)
        {
            lblSave.Background = new SolidColorBrush(Colors.LightGray);
        }
       
    }

    public class CloseMenu : Dialog
    {
        public CloseMenu()
        {
            Close();
        }

         protected override void OnClickOutside()
        {
            Close();
        }


         protected override FrameworkElement GetContent()
         {
             return null;
         }
    }
}
