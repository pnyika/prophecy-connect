﻿#pragma checksum "C:\Users\Prince\Documents\Visual Studio 2010\Projects\BMA.EOMiddleware\BMA.EOInterface.Middleware\Controls\ExcelSelectBook.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1BAF52288B4B5D3FA571AB4B3D0BD874"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace BMA.MiddlewareApp.Controls {
    
    
    public partial class ExcelSelectBook : System.Windows.Controls.UserControl {
        
        internal Telerik.Windows.Controls.RadBusyIndicator busy1;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock lblTitle;
        
        internal System.Windows.Controls.StackPanel pnlExcess;
        
        internal Telerik.Windows.Controls.RadComboBox ddlDataSouce;
        
        internal System.Windows.Controls.StackPanel pnlAccess;
        
        internal System.Windows.Controls.TextBox txttableName;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/BMA.EOInterface.Middleware;component/Controls/ExcelSelectBook.xaml", System.UriKind.Relative));
            this.busy1 = ((Telerik.Windows.Controls.RadBusyIndicator)(this.FindName("busy1")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.lblTitle = ((System.Windows.Controls.TextBlock)(this.FindName("lblTitle")));
            this.pnlExcess = ((System.Windows.Controls.StackPanel)(this.FindName("pnlExcess")));
            this.ddlDataSouce = ((Telerik.Windows.Controls.RadComboBox)(this.FindName("ddlDataSouce")));
            this.pnlAccess = ((System.Windows.Controls.StackPanel)(this.FindName("pnlAccess")));
            this.txttableName = ((System.Windows.Controls.TextBox)(this.FindName("txttableName")));
        }
    }
}

