﻿#pragma checksum "C:\Users\Prince\Documents\Visual Studio 2010\Projects\BMA.EOMiddleware\BMA.EOInterface.Middleware\Admin\UserControls\DBTableControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "0C5C66E5C4F95CB6E5AEBCAA6B42BDC3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace BMA.MiddlewareApp.Admin.UserControls {
    
    
    public partial class DBTableControl : System.Windows.Controls.UserControl {
        
        internal Telerik.Windows.Controls.RadBusyIndicator radBusyIndicator;
        
        internal System.Windows.Controls.StackPanel stackPanel3;
        
        internal System.Windows.Controls.TextBlock textBlock2;
        
        internal System.Windows.Controls.ListBox lstTable;
        
        internal Telerik.Windows.Controls.RadButton btnAddTable;
        
        internal Telerik.Windows.Controls.RadButton btnCheckFields;
        
        internal System.Windows.Controls.StackPanel pnlField;
        
        internal Telerik.Windows.Controls.RadGridView radGridView1;
        
        internal Telerik.Windows.Controls.RadButton btnSave;
        
        internal Telerik.Windows.Controls.RadButton btnCancel;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/BMA.EOInterface.Middleware;component/Admin/UserControls/DBTableControl.xaml", System.UriKind.Relative));
            this.radBusyIndicator = ((Telerik.Windows.Controls.RadBusyIndicator)(this.FindName("radBusyIndicator")));
            this.stackPanel3 = ((System.Windows.Controls.StackPanel)(this.FindName("stackPanel3")));
            this.textBlock2 = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock2")));
            this.lstTable = ((System.Windows.Controls.ListBox)(this.FindName("lstTable")));
            this.btnAddTable = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnAddTable")));
            this.btnCheckFields = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnCheckFields")));
            this.pnlField = ((System.Windows.Controls.StackPanel)(this.FindName("pnlField")));
            this.radGridView1 = ((Telerik.Windows.Controls.RadGridView)(this.FindName("radGridView1")));
            this.btnSave = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnSave")));
            this.btnCancel = ((Telerik.Windows.Controls.RadButton)(this.FindName("btnCancel")));
        }
    }
}

