﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BMA.MiddlewareApp
{
    public class WCF
    {
        public static BMA.EOInterface.Middleware.DataTableService.GetDataClient GetService()
        {
            System.ServiceModel.BasicHttpBinding binding = new System.ServiceModel.BasicHttpBinding();
            binding.MaxReceivedMessageSize = 2147483647; // int's max size
            binding.MaxBufferSize = 2147483647; // int's max size

            TimeSpan duration = new TimeSpan(0, 0, 20, 0);
            binding.ReceiveTimeout = duration;
            binding.SendTimeout = duration;
            binding.OpenTimeout = duration;
            System.ServiceModel.EndpointAddress address = new System.ServiceModel.EndpointAddress(new Uri(Application.Current.Host.Source, "../GetData.svc"));
            try
            {
                return new BMA.EOInterface.Middleware.DataTableService.GetDataClient(binding, address);
            }
            catch (Exception e)
            {

            }
            return null;
        }
    }
}

