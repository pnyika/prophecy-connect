﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Reflection.Emit;
using System.Reflection;
using System.Collections;
using System.Linq;

namespace BMA.MiddlewareApp
{
    public class DynamicDataBuilder
    {

        private static System.Type BuildDataObjectType(ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataColumnInfo> Columns, string DataObjectName)
        {
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName("AprimoDynamicData"), AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("DataModule");

            TypeBuilder tb = moduleBuilder.DefineType(DataObjectName,
                                                    TypeAttributes.Public |
                                                    TypeAttributes.Class,
                                                    typeof(DataObject));

            ConstructorBuilder constructor = tb.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);
            foreach (var col in Columns)
            {
                string propertyName = CharacterHandler.ReplaceSpecialCharacter(col.ColumnName);
                Type dataType = System.Type.GetType(col.DataTypeName, false, true);
                if (dataType != null)
                {
                    FieldBuilder fb = tb.DefineField("_" + propertyName, dataType, FieldAttributes.Private);
                    PropertyBuilder pb = tb.DefineProperty(propertyName, PropertyAttributes.HasDefault, dataType, null);
                    MethodBuilder getMethod = tb.DefineMethod("get_" + propertyName,
                                                                MethodAttributes.Public |
                                                                MethodAttributes.HideBySig |
                                                                MethodAttributes.SpecialName,
                                                                dataType,
                                                                Type.EmptyTypes);

                    ILGenerator ilgen = getMethod.GetILGenerator();
                    //Emit Get property, return _prop
                    ilgen.Emit(OpCodes.Ldarg_0);
                    ilgen.Emit(OpCodes.Ldfld, fb);
                    ilgen.Emit(OpCodes.Ret);
                    pb.SetGetMethod(getMethod);
                    MethodBuilder setMethod = tb.DefineMethod("set_" + propertyName,
                    MethodAttributes.Public |
                    MethodAttributes.HideBySig |
                    MethodAttributes.SpecialName,
                    null,
                    new Type[] { dataType });
                    ilgen = setMethod.GetILGenerator();
                    LocalBuilder localBuilder = ilgen.DeclareLocal(typeof(String[]));
                    //Emit set property, _Prop = value;
                    ilgen.Emit(OpCodes.Ldarg_0);
                    ilgen.Emit(OpCodes.Ldarg_1);
                    ilgen.Emit(OpCodes.Stfld, fb);

                    //Notify Change:
                    Type[] wlParams = new Type[] { typeof(string[]) };
                    MethodInfo notifyMI = typeof(DataObject).GetMethod("NotifyChange",
                    BindingFlags.NonPublic |
                    BindingFlags.Instance,
                    null,
                    CallingConventions.HasThis,
                    wlParams,
                    null);

                    //NotifyChange Property change
                    ilgen.Emit(OpCodes.Ldc_I4_1);
                    ilgen.Emit(OpCodes.Newarr, typeof(String));
                    ilgen.Emit(OpCodes.Stloc_0);
                    ilgen.Emit(OpCodes.Ldloc_0);
                    ilgen.Emit(OpCodes.Ldc_I4_0);
                    ilgen.Emit(OpCodes.Ldstr, propertyName);
                    ilgen.Emit(OpCodes.Stelem_Ref);
                    ilgen.Emit(OpCodes.Ldarg_0);
                    ilgen.Emit(OpCodes.Ldloc_0);
                    ilgen.EmitCall(OpCodes.Call, notifyMI, null); // call nodifyChange function

                    ilgen.Emit(OpCodes.Ret);
                    pb.SetSetMethod(setMethod);
                }
            }
            System.Type rowType = tb.CreateType();
            //assemblyBuilder.Save("DynamicData.dll");
            return rowType;
        }

        public static IEnumerable GetDataList(BMA.EOInterface.Middleware.DataTableService.DataSetData data)
        {
            System.Globalization.CultureInfo cultureInfo =
      new System.Globalization.CultureInfo("en-ZA");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");
            System.Globalization.DateTimeFormatInfo dateTimeInfo =
   new System.Globalization.DateTimeFormatInfo();
            dateTimeInfo.LongDatePattern = "dd-MMM-yyyy";
            dateTimeInfo.ShortDatePattern = "dd-MMM-yyyy";
            cultureInfo.DateTimeFormat = dateTimeInfo;

            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = dateTimeInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat = dateTimeInfo;

            ObservableCollection<DataObject> myObjects = new ObservableCollection<DataObject>();
            if (data.Tables.Count() == 0)
                return null;

            BMA.EOInterface.Middleware.DataTableService.DataTableInfo tableInfo = data.Tables[0];

            System.Type dataType = BuildDataObjectType(tableInfo.Columns, "MyDataObject");

            //ObservableCollection<DataObject> l = new ObservableCollection<DataObject>();

            var listType = typeof(ObservableCollection<>).MakeGenericType(new[] { dataType });
            var list = Activator.CreateInstance(listType);

            XDocument xd = XDocument.Parse(data.DataXML);
            var table = from row in xd.Descendants(tableInfo.TableName)
                        select row.Elements().ToDictionary(r => r.Name, r => r.Value);

            foreach (var r in table)
            {
                var rowData = Activator.CreateInstance(dataType) as DataObject;
                if (rowData != null)
                {
                    foreach (BMA.EOInterface.Middleware.DataTableService.DataColumnInfo col in tableInfo.Columns)
                    {
                        if (r.ContainsKey(CharacterHandler.ReplaceSpecialCharacter(col.ColumnName)) && col.DataTypeName != typeof(System.Byte[]).FullName)
                        {
                            var t = r.Where(x => x.Key == CharacterHandler.ReplaceSpecialCharacter(col.ColumnName)).FirstOrDefault();
                            if ((t.Value != ""))
                            {
                                if(col.DataTypeName == typeof(System.Guid).FullName)
                                {
                                         object Data = null;

                                        string datavalue = r[CharacterHandler.ReplaceSpecialCharacter(col.ColumnName)];
                                        Data = new Guid(datavalue);
                                        rowData.SetFieldValue(col.ColumnName, Data, true);
                                }
                                else if(col.DataTypeName == typeof(System.TimeSpan).FullName)
                                {
                                    try
                                    {
                                        object Data = null;

                                        string datavalue = r[CharacterHandler.ReplaceSpecialCharacter(col.ColumnName)];
                                        TimeSpan myTimeSpan = TimeSpan.Parse(datavalue);
                                        Data = myTimeSpan;
                                        rowData.SetFieldValue(col.ColumnName, Data, true);
                                    }
                                    catch { }

                                }
                                else if (col.DataTypeName == typeof(System.DateTime).FullName)
                                {
                                    try
                                    {
                                        object Data = null;

                                        string datavalue = r[CharacterHandler.ReplaceSpecialCharacter(col.ColumnName)];
                                        DateTime myTimeSpan = DateTime.Parse(datavalue);
                                        Data = myTimeSpan;
                                        rowData.SetFieldValue(col.ColumnName, Data, true);
                                    }
                                    catch { }

                                }

                                else
                                {

                                rowData.SetFieldValue(col.ColumnName, r[CharacterHandler.ReplaceSpecialCharacter(col.ColumnName)], true);
                                }
                            }
                            //else
                              //  rowData.SetFieldValue(col.ColumnName, r[col.ColumnName]);
                            
                        }
                    }
                }
                listType.GetMethod("Add").Invoke(list, new[] { rowData });
                myObjects.Add(rowData);
            }
            ObservableCollection<DataObject> l = list as ObservableCollection<DataObject>;
            return list as IEnumerable;
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public static IEnumerable GetDesDataList(BMA.EOInterface.Middleware.DataTableService.DataSetData data)
        {
            System.Globalization.CultureInfo cultureInfo =
      new System.Globalization.CultureInfo("en-ZA");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");
            System.Globalization.DateTimeFormatInfo dateTimeInfo =
 new System.Globalization.DateTimeFormatInfo();
            dateTimeInfo.LongDatePattern = "dd-MMM-yyyy";
            dateTimeInfo.ShortDatePattern = "dd-MMM-yyyy";
            cultureInfo.DateTimeFormat = dateTimeInfo;

            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = dateTimeInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat = dateTimeInfo;

            if (data.Tables.Count() == 0)
                return null;

            BMA.EOInterface.Middleware.DataTableService.DataTableInfo tableInfo = data.Tables[0];

            System.Type dataType = BuildDataObjectType(tableInfo.Columns, "MyDataObject");

            //ObservableCollection<DataObject> l = new ObservableCollection<DataObject>();

            var listType = typeof(ObservableCollection<>).MakeGenericType(new[] { dataType });
            var list = Activator.CreateInstance(listType);

            XDocument xd = XDocument.Parse(data.DataXML);
            var table = from row in xd.Descendants(tableInfo.TableName)
                        select row.Elements().ToDictionary(r => r.Name, r => r.Value);

            foreach (var r in table)
            {
                var rowData = Activator.CreateInstance(dataType) as DataObject;
                if (rowData != null)
                {
                    foreach (BMA.EOInterface.Middleware.DataTableService.DataColumnInfo col in tableInfo.Columns)
                    {
                        if (r.ContainsKey(col.ColumnName) && col.DataTypeName != typeof(System.Byte[]).FullName && col.DataTypeName != typeof(System.Guid).FullName)
                            rowData.SetFieldValue(col.ColumnName, r[col.ColumnName], true);
                    }
                }
                listType.GetMethod("Add").Invoke(list, new[] { rowData });
            }
            ObservableCollection<DataObject> l = list as ObservableCollection<DataObject>;
            return list as IEnumerable;
        }

        public static BMA.EOInterface.Middleware.DataTableService.DataSetData GetUpdatedDataSet(IEnumerable list, ObservableCollection<BMA.EOInterface.Middleware.DataTableService.DataTableInfo> tables)
        {
            BMA.EOInterface.Middleware.DataTableService.DataSetData data = new BMA.EOInterface.Middleware.DataTableService.DataSetData();
            data.Tables = tables;
            //data.Tables = new ObservableCollection<DataSetInDataGrid.Silverlight.MyDataService.DataTableInfo>();
            //foreach (MyDataService.DataTableInfo t in tables)
            //{
            //    MyDataService.DataTableInfo table = new MyDataService.DataTableInfo { TableName = t.TableName };
            //    table.Columns = new ObservableCollection<DataSetInDataGrid.Silverlight.MyDataService.DataColumnInfo>();
            //    foreach (MyDataService.DataColumnInfo c in t.Columns)
            //    {
            //        table.Columns.Add(new MyDataService.DataColumnInfo{ColumnName= c.ColumnName, DataTypeName
            //    }
            //}

            XElement root = new XElement("DataSet");
            foreach (DataObject d in list)
            {
                //if (d.State != DataObject.DataStates.Unchanged)
                //{
                    XElement row = new XElement("Data", new XAttribute("RowState", d.State.ToString()));
                    PropertyInfo[] pis = d.GetType().GetProperties();
                    foreach (PropertyInfo pi in pis)
                    {
                        object val = pi.GetValue(d, null);
                        if (val != null)
                            row.Add(new XElement(pi.Name, val.ToString()));
                        else
                            row.Add(new XElement(pi.Name, ""));
                    }
                    root.Add(row);
                //}
            }
            XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), root);
            data.DataXML = xdoc.ToString();

            return data;
        }
    }
}