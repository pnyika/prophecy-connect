﻿namespace BMA.MiddlewareApp
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Navigation;
    //using BMA.MiddlewareApp.LoginUI;
    using BMA.MiddlewareApp.Web;
    using System.Linq;
    using System.Collections.Generic;
    using System;
    using Telerik.Windows.Controls;
    using System.Xml.Linq;
    using System.ServiceModel.DomainServices.Client;
    using Telerik.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using BMA.MiddlewareApp.Controls;
    using System.Collections.ObjectModel;
using Telerik.Windows.Controls.GridView;
    using System.ServiceModel.DomainServices.Client.ApplicationServices;
    using System.Windows.Browser;
    using BMA.EOInterface.Middleware;
   
    /// <summary>
    /// <see cref="UserControl"/> class providing the main UI for the application.
    /// </summary>
    public partial class MainPage : UserControl
    {
        /// <summary>
        /// Creates a new <see cref="MainPage"/> instance.
        /// </summary>
        /// 
        public static event EventHandler StatusUpdated;
        public static bool cnUpdate = false;
        EditorContext context = new EditorContext();
        UserInformation activeUser = new UserInformation();
        MenuStructure thisMenuStruct = new MenuStructure();
        public List<BMA.MiddlewareApp.Web.MenuItem> menuList { get; set; }
        public List<BMA.MiddlewareApp.Web.MenuItem> SubMenu { get; set; }
        public static int DisplayID;
        private int pageSize;
        public static int userID;
        public static List<GroupModel> ModelList;
        public static double gridHeight;
        public static long groupID;
        public static long ModelID;
        public static string ModelName;
        public static DateTime ReferenceDate;
        int menuID;
       // public static int newHeight = outlookBar.Height;
        string title;
        ObservableCollection<TabItemModel> tabItemsModel = new ObservableCollection<TabItemModel>();
        System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer barDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer timeOutDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public MainPage()
        {
           
          
            InitializeComponent();

            // btnButtons.SaveClick += new EventHandler(btnSave_Click);
         ctlLogin.LoginComplete += (se, ev) => { LoginClick(); };
            if (App.Current.IsRunningOutOfBrowser)
            {
              //  App.Current.MainWindow.WindowState = WindowState.Maximized;
                //Dispatcher.BeginInvoke(() =>
                //{

                //    App.Current.MainWindow.TopMost = false;
                //    App.Current.Host.Content.IsFullScreen = true;
                //});
            }
            radExpander.Visibility = Visibility.Collapsed;

            radExpanderModel.Visibility = Visibility.Collapsed;
            LayoutRoot.MouseRightButtonUp += new MouseButtonEventHandler(stack1_MouseRightButtonUp);
           // new BureauBlueTheme()},
            
            //if (WebContext.Current.Authentication.User.Identity.Name != "")
            //{
            //    LoginClick();
            //}
            // this.DataContext = new Test.MainViewModel();
            int count = 0;
            Dispatcher.BeginInvoke(() =>
            {

                if (Application.Current.IsRunningOutOfBrowser)
                {
                    Application.Current.MainWindow.WindowState = WindowState.Maximized;
                    ctlLogin.txtUsername.Focus();
                }
                else
                {
                    System.Windows.Browser.HtmlPage.Plugin.Focus();
                    ctlLogin.txtUsername.Focus();
                    
                }
                //AuthenticationService _authService = RiaContext.Current.Authentication;

                //var loadUserOp = _authService.LoadUser();
                //loadUserOp.Completed += new EventHandler(loadUserOp_Completed);
                if (WebContext.Current.Authentication.User.Identity.IsAuthenticated)
                {
                    LoginClick();
                }
                else
                {
                 //   GetCurrentUser();
                }
                
               
            });

           outlookBar.LayoutUpdated +=outlookBar_LayoutUpdated ;
          
      // Loaded += new RoutedEventHandler(MainPage_Loaded);
        }


        public void TimeOutStartTimer()
        {

           
            timeOutDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            timeOutDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 4, 0); // 15seconds 

            timeOutDispatcherTimer.Tick += new EventHandler(TimeOutEach_Tick);

            timeOutDispatcherTimer.Start();


        }

        public void TimeOutEach_Tick(object sender, EventArgs e)
        {
            try
            {
                      App app = (App)Application.Current;
                      if (app.LastActivity < DateTime.Now.AddMinutes(-20))
                      {
                          timeOutDispatcherTimer.Stop();
                          WebContext.Current.Authentication.Logout(LogoutCompleteCallback, null);
                      }

               
            }
            catch
            { 
            }

        }

        bool outlookBarUpdate = true;
        void outlookBar_LayoutUpdated(object sender, EventArgs e)
        {
            try
            {
                if (outlookBarUpdate)
                {

                    BarStartTimer();
                }
               
            }
            catch { }
        }

        public void BarStartTimer()
        {

            outlookBarUpdate = false;
            barDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            barDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 4, 0); // 15seconds 

            barDispatcherTimer.Tick += new EventHandler(BarEach_Tick);

            barDispatcherTimer.Start();


        }

        public void BarEach_Tick(object sender, EventArgs e)
        {
            try
            {

                double height = outlookBar.ActualHeight;
                // double height2 =  LayoutRoot.Height;
                // double height3 = LayoutRoot.ActualHeight;
                // outlookBar.Height = height;
                innerPanel.Height = height - 100;

                barDispatcherTimer.Stop();

                outlookBarUpdate = true;
            }
            catch
            { barDispatcherTimer.Stop(); }

        }


        void LayoutRoot_LayoutUpdated(object sender, EventArgs e)
        {
            try
            {
                if (cnUpdate)
                {

                    StartTimer();
                }
                else
                {
                    new NotImplementedException();
                }
            }
            catch { }
        }

        public void StartTimer()
        {

            cnUpdate = false;
            myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 4, 0); // 15seconds 

            myDispatcherTimer.Tick += new EventHandler(Each_Tick);

            myDispatcherTimer.Start();


        }

        public void Each_Tick(object sender, EventArgs e)
        {

            StatusUpdated(InnerPage, e);
         //  double height =  outlookBar.Height;
           //innerPanel.Height = height - 60;

            myDispatcherTimer.Stop();

            cnUpdate = true;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
          //  string Width = HtmlPage.Window.Eval("screen.width").ToString();
          //  string Height = HtmlPage.Window.Eval("screen.height").ToString();
         //   MessageBox.Show(string.Format("Current resolution : {0} X {1}", Width, Height));

           // LayoutRoot.Height = Convert.ToDouble(Height)-150;
         //   LayoutRoot.Width = Convert.ToDouble(Width)-120;

           
        }


          private void GetCurrentUser()
        {
           
            var ws = WCF.GetService();
            ws.GetCurrentUserCompleted += new EventHandler<BMA.EOInterface.Middleware.DataTableService.GetCurrentUserCompletedEventArgs>(ws_GetCurrentUserCompleted);
            ws.GetCurrentUserAsync();
        }


          void ws_GetCurrentUserCompleted(object sender, BMA.EOInterface.Middleware.DataTableService.GetCurrentUserCompletedEventArgs e)
          {
              if (e.Error != null)
              {

              }


              else
              {
                  autoLogin(e.Result);
              }
          }

        public void autoLogin(string currentUser)
        {
            
            ctlLogin.btnLogin.Content = "Loging....";
            ctlLogin.btnLogin.IsEnabled = false;
            ctlLogin.busyIndicator.IsBusy = true;

            // LoginComplete(this, null);
            LoginParameters para = new LoginParameters();

            WebContext.Current.Authentication.Login(new LoginParameters(currentUser,
            "activedirectoryuse"), LoginCompleteCallback, null);
        }


        private void LoginCompleteCallback(LoginOperation result)
        {
            try
            {
                if (!result.HasError)
                {
                    if (result.LoginSuccess)
                    {

                        // Let the parent control know that the login was successful

                     
                        // Clearing the error text here prevents the login failed message from
                        // being displayed again if the user did fail a login attempt and then 
                        // eventually succeeded.  

                        //Without doing this after the user logs out the previous error
                        // message would still be visible on the home page
                        if (ctlLogin.chkPersistance.IsChecked.Value == true)
                        {
                            var wcf = WCF.GetService();
                            wcf.IssueAuthenticationTokenAsync();
                            //context.IssueAuthenticationToken();
                        }
                        ctlLogin.busyIndicator.IsBusy = false;
                        ctlLogin.loginError.Text = "";
                        LoginClick();
                    }
                    else
                    {

                        ctlLogin.btnLogin.Content = "Login";
                        ctlLogin.btnLogin.IsEnabled = true;
                        ctlLogin.loginError.Text = "Incorrect username or password!";
                        ctlLogin.busyIndicator.IsBusy = false;
                        //busyIndicator.IsBusy = false;
                        //busyIndicator.Content = "";


                    }
                }
                else
                {
                    string error = result.Error.Message;
                    ctlLogin.btnLogin.Content = "Login";
                    ctlLogin.busyIndicator.IsBusy = false;
                    ctlLogin.btnLogin.IsEnabled = true;
                    ctlLogin.loginError.Text = "Incorrect username or password!";
                    //busyIndicator.IsBusy = false;
                    //busyIndicator.Content = "";
                    result.MarkErrorAsHandled();
                }
            }
            catch
            {
                ctlLogin.busyIndicator.IsBusy = false;
            }
            ctlLogin.busyIndicator.IsBusy = false;
        }

        //void ctlLogin_LoginClick(object sender, EventArgs e)
        //{
        //    LoginClick();
        //}
        private void LoginClick()
        {
            try{
                TimeOutStartTimer();
                gridHeight = LayoutRoot.ActualHeight - 25;
                if (WebContext.Current.Authentication.User.IsInRole("Developer"))
                {
                    this.Content = new Admin.AdminMain();
                }
                else
                {
                    radExpander.Visibility = Visibility.Visible;
                    lblRole.Text = "Application User";
                    lblUsername.Text = WebContext.Current.Authentication.User.Identity.Name;
                    lblInfo.Text = "";
                    radExpanderModel.Visibility = Visibility.Visible;

                    string username = WebContext.Current.Authentication.User.Identity.Name;
                    this.tvHierarchyView.AddHandler(Telerik.Windows.Controls.RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.radTreeView_MouseLeftButtonDown), true);
                    ctlLogin.Visibility = Visibility.Collapsed;


                    LoadOperation<UserInformation> loadOperation = context.Load(context.GetUserInformationWithMenuStructureQuery().Where(u => u.UserName == username), CallbackUser, null);

                }
            }
                catch{}

           // string Width = HtmlPage.Window.Eval("screen.width").ToString();
           // string Height = HtmlPage.Window.Eval("screen.height").ToString();
           // MessageBox.Show(string.Format("Current resolution : {0} X {1}", Width, Height));
            
           //// LayoutRoot.Height = Convert.ToDouble(Height)-150;
           // LayoutRoot.Width = Convert.ToDouble(Width)-120;
           
        }


        private void loadGroupModels()
        {

            LoadOperation<GroupModel> loadOp = context.Load(context.GetGroupModelsQuery(), CallbackGroupModels, null);




        }


        private void CallbackGroupModels(LoadOperation<GroupModel> result)
        {


            if (result != null)
            {
                ObservableCollection<GroupModel> currentGroupModel = new ObservableCollection<GroupModel>(result.Entities.Where(g => g.Group_ID == groupID));

                lstModelList.ItemsSource = currentGroupModel;
                ModelList = new List<GroupModel>(currentGroupModel);

            }
          
        }
        //private void CreateTabItems()
        //{
        //    // Create items:

        //        tabItemsModel.Add(new TabItemModel()
        //        {
        //            Title ="Start Page",
        //            Content = new Controls.TestPanels()
        //        });

        //    // Attach the items:
        //    tabControl.ItemsSource = tabItemsModel;
        //}


        public void OnCloseClicked(object sender, RoutedEventArgs e)
        {
            var tabItem = sender as RadTabItem;
            // Remove the item from the collection the control is bound to
            tabItemsModel.Remove(tabItem.DataContext as TabItemModel);
        }


        void stack1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            //RightClickContentMenu contextMenu = new RightClickContentMenu();
            //contextMenu.Show(e.GetPosition(LayoutRoot));
        }
        private void radTreeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Telerik.Windows.Controls.RadTreeView treeView = sender as Telerik.Windows.Controls.RadTreeView;


            Category selectedItem = treeView.SelectedItem as Category;
            if (selectedItem == null)
                return;
             menuID = Convert.ToInt32(selectedItem.ID);

            //GetTable(menuID);
            // this.Content = new Page2();
            if (selectedItem.DefID != null)
            {

                DisplayID = selectedItem.DefID.Value;
                if (selectedItem.PageSize != null)
                    pageSize = selectedItem.PageSize.Value;
                else
                    pageSize = 25;
                LoadOperation<FormType> ldop = context.Load(context.GetFormTypeByMenuIDQuery(menuID), CallbackType, null);

                title = selectedItem.Name;
                //  LoadOperation<DisplayDefinition> ldop = context.Load(context.GetDisplayDefinitionsQuery(selectedItem.DefID.Value), CallbackDefDisplay, null);

            }
            else
            {
                if (selectedItem.Name == "Logout")
                {
                    cnUpdate = false;
                    WebContext.Current.Authentication.Logout(LogoutCompleteCallback, null);
                 
                }

                else if(selectedItem.Name == "Design")
                {
                    cnUpdate = false;
                    myDispatcherTimer.Stop();
                    Content = new Admin.AdminMain();
                }
            }

        }


        private void LogoutCompleteCallback(LogoutOperation result)
        {
           //
            if (Application.Current.IsRunningOutOfBrowser)
            {
                this.Content = new MainPage();
            }
            else
            {

                HtmlPage.Window.Navigate(new Uri(Application.Current.Host.Source, "../Default.aspx"));
            }

            //this.tvHierarchyView = new RadTreeView();
            //tvHierarchyView.Visibility = Visibility.Collapsed;
            //ctlLogin.Visibility = Visibility.Visible;
        }


        private void CallbackUser(LoadOperation<UserInformation> loadedUser)
        {
            List<UserInformation> orig = loadedUser.Entities.ToList();


            if (orig != null)
            {
                var activeUser = orig.FirstOrDefault();
                Globals.CurrentUser = activeUser;
               
                userID = activeUser.ID;
                groupID = 0;
                if (activeUser.Group_ID != null)
                    groupID = activeUser.Group_ID.Value;

                if (WebContext.Current.Authentication.User.IsInRole("Super User"))
                {
                    LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => (m.MenuStructure_ID == activeUser.MenuStructure_ID || m.DisplayName == "Logout" || m.DisplayName == "Design") && (m.User_ID == userID || m.User_ID == 0)), Callback, null);
                }
                else
                {
                    LoadOperation ldop = context.Load<BMA.MiddlewareApp.Web.MenuItem>(context.GetMenuItemsQuery().Where(m => (m.MenuStructure_ID == activeUser.MenuStructure_ID || m.DisplayName == "Logout") && (m.User_ID == userID || m.User_ID == 0)), Callback, null);

                    }

                loadGroupModels();
                 }

        }


        private void Callback(LoadOperation<BMA.MiddlewareApp.Web.MenuItem> loadMenus)
        {
            if (loadMenus != null)
            {
                List<BMA.MiddlewareApp.Web.MenuItem> orig = loadMenus.Entities.ToList().OrderBy(x => x.SequenceNumber).ToList();

                this.tvHierarchyView.ItemsSource = orig;
                LoadData(orig);
                stackPanel221.LayoutUpdated += new EventHandler(LayoutRoot_LayoutUpdated);
            }
        }

        /// <summary>
        /// After the Frame navigates, ensure the <see cref="HyperlinkButton"/> representing the current page is selected
        /// </summary>
        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            //foreach (UIElement child in LinksStackPanel.Children)
            //{
            //    HyperlinkButton hb = child as HyperlinkButton;
            //    if (hb != null && hb.NavigateUri != null)
            //    {
            //        if (hb.NavigateUri.ToString().Equals(e.Uri.ToString()))
            //        {
            //            VisualStateManager.GoToState(hb, "ActiveLink", true);
            //        }
            //        else
            //        {
            //            VisualStateManager.GoToState(hb, "InactiveLink", true);
            //        }
            //    }
            //}
        }
        private void DoubleFrame_Navigated(object sender, NavigationEventArgs e)
        {

            // Frame frame =this.parent as Frame
            //this.DoubleFrame = Frame as pare
        }
        /// <summary>
        /// If an error occurs during navigation, show an error window
        /// </summary>
        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            //ContentFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }
        private void DoubleFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            //DoubleFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }

        private void TrippleFrame_Navigated(object sender, NavigationEventArgs e)
        {

        }
        private void TrippleFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            //  DoubleFrame.Navigate(new Uri("/Admin/ErrorPage.xaml", UriKind.Relative));
        }


        private void tvHierarchyView_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {


        }



        private void LoadData(List<BMA.MiddlewareApp.Web.MenuItem> elements)
        {
            List<Category> categories = new List<Category>();
            categories = this.GetCategories(elements);
            this.tvHierarchyView.ItemsSource = categories;
        }




        private List<Category> GetCategories(List<BMA.MiddlewareApp.Web.MenuItem> element)
        {
            int id = 0;
            return (from category in element.Where(p => p.ParentMenu_ID == 0)
                    
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        DefID = category.DisplayDefinition_ID,
                        ImageUrl = category.ImageUrl,
                      //  if(category.DisplayDefinition != null)
                       // PageSize =category.DisplayDefinition.PageSize.Value,


                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                     
                    }).ToList();
        }


        private List<Category> GetChild(List<BMA.MiddlewareApp.Web.MenuItem> element, int menuLevel)
        {
            foreach (var item in element)
            {
                if (item.DisplayDefinition_ID == null)
                {
                    item.DisplayDefinition = new DisplayDefinition { ID = 0, PageSize = 0 };
                }
            }

            return (from category in element.Where(p => p.ParentMenu_ID == menuLevel)
                    select new Category()
                    {
                        Name = category.DisplayName,
                        ID = category.ID,
                        DefID = category.DisplayDefinition_ID,
                        PageSize = 25,//category.DisplayDefinition.PageSize.Value,
                        ImageUrl = category.ImageUrl,
                        SubCategories = this.GetChild(element.Where(c => c.ParentMenu_ID > 0).ToList(), category.ID)
                    }).ToList();
        }


        private void tvHierarchyView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            // this.Content = new Page2();
            Category selectedItem = tvHierarchyView.SelectedItem as Category;

            try
            {

                string selected = selectedItem.ID.ToString();
                // MessageBox.Show(selected);
                int menuID = Convert.ToInt32(selected);

                //GetTable(menuID);
                // this.Content = new Page2();

                if (selectedItem.DefID != null)
                {
                    if (selectedItem.Name == "Logout")
                    {
                        MessageBox.Show("Logged out !");
                    }
                    else
                    {
                        LoadOperation<FormType> ldop = context.Load(context.GetFormTypeByMenuIDQuery(menuID), CallbackType, null);
                        DisplayID = selectedItem.DefID.Value;
                      

                        //  LoadOperation<DisplayDefinition> ldop = context.Load(context.GetDisplayDefinitionsQuery(selectedItem.DefID.Value), CallbackDefDisplay, null);
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //private void CallbackDefDisplay(LoadOperation<DisplayDefinition> lo)
        //{
        //    if (lo.Entities.Count<FormType>() == 0)
        //        return;
        //}


        private void CallbackType(LoadOperation<FormType> lo)
        {
            if (lo.Entities.Count<FormType>() == 0)
                return;
            FormType orig = lo.Entities.FirstOrDefault();

            if (orig.Description == "Single")
            {


                try
                {
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        string docPaneName = title.Replace(" ", "");

                        Controls.OneGrid.DisplayID = DisplayID;

                        Controls.OneGrid.pageSize = pageSize;
;
                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.OneGrid();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);

                    }





                }
                catch
                {

                }
            }
            if (orig.Description == "Double")
            {

                try
                {
                    Controls.TwoGrids.DisplayID = DisplayID;
                    Controls.TwoGrids.pageSize = pageSize;
                    Controls.TwoGrids.pageSize2 = pageSize;
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.TwoGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.TwoGrids();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "Tripple")
            {


                Controls.ThreeGrids.DisplayID = DisplayID;
                Controls.ThreeGrids.pageSize = pageSize;
                Controls.ThreeGrids.pageSize2 = pageSize;
                Controls.ThreeGrids.pageSize3 = pageSize;
                string docPaneName = title.Replace(" ", "");





                RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                if (pane != null)
                    MainTab.SelectedItem = pane;
                else
                {
                    //  string docPaneName = title.Replace(" ", "");

                    Controls.ThreeGrids.DisplayID = DisplayID;


                    RadTabItem tab = new RadTabItem();
                    tab.Content = new Controls.ThreeGrids();
                    // tab.Padding = 
                    tab.DropDownContent = title;
                    tab.Header = title;
                    tab.Name = title.Replace(" ", "") + menuID;
                    tab.IsSelected = true;
                    tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                    tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                    this.MainTab.Items.Add(tab);

                }

            }


            if (orig.Description =="Design")
            {

                try
                {
                    Controls.NewForm.DisplayID = DisplayID;
                    
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.NewForm.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.NewForm();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "EO")
            {

                try
                {
                    Controls.FixedThreeGrids.DisplayID = DisplayID;
                    Controls.FixedThreeGrids.pageSize = pageSize;
                    Controls.FixedThreeGrids.pageSize2 = pageSize;
                    Controls.FixedThreeGrids.pageSize3 = pageSize;
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.FixedThreeGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.FixedThreeGrids();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "Profile")
            {

                try
                {
                    Controls.ProfileGrid.DisplayID = DisplayID;
                    Controls.ProfileGrid.pageSize = pageSize;
                    Controls.ProfileGrid.pageSize2 = pageSize;
                    Controls.ProfileGrid.pageSize3 = pageSize;

                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID);

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.ProfileGrid.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.ProfileGrid();
                        // tab.Padding = 
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID;
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "MTP")
            {

                try
                {
                    Controls.MTPGrids.DisplayID = DisplayID;
                    Controls.MTPGrids.pageSize = pageSize;
                    Controls.MTPGrids.pageSize2 = pageSize;
                    Controls.MTPGrids.pageSize3 = pageSize;
                    Controls.MTPGrids.pageSize4 = pageSize;
                   // Controls.MTPGrids.pageSize5 = pageSize;
                    Controls.MTPGrids.ModelID = ModelID;
                    Controls.MTPGrids.ModelName = ModelName;
               //     Controls.DefinitionGrid.Definition = 
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID + ModelID.ToString());

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.MTPGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.MTPGrids();
                        // tab.Padding = 
                       
                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID + ModelID.ToString();
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        RadToolTip tooltip = new RadToolTip();
                        tooltip.Content = ModelName;
                        ToolTipService.SetToolTip(tab, tooltip);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }

            if (orig.Description == "Model Based Single")
            {

                try
                {
                    Controls.ModelBasedOneGrid.DisplayID = DisplayID;
                    Controls.ModelBasedOneGrid.pageSize = pageSize;
                    Controls.ModelBasedOneGrid.ModelID = ModelID;
                    Controls.ModelBasedOneGrid.ModelName = ModelName;
                    //     Controls.DefinitionGrid.Definition = 
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID + ModelID.ToString());

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.ModelBasedOneGrid.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.ModelBasedOneGrid();
                        // tab.Padding = 

                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID + ModelID.ToString();
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        RadToolTip tooltip = new RadToolTip();
                        tooltip.Content = ModelName;
                        ToolTipService.SetToolTip(tab, tooltip);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }


            if (orig.Description == "Model Based Double")
            {

                try
                {
                    Controls.ModelBasedTwoGrids.DisplayID = DisplayID;
                    Controls.ModelBasedTwoGrids.pageSize = pageSize;
                    Controls.ModelBasedTwoGrids.pageSize2 = pageSize;
                   // Controls.ModelBasedTwoGrids.pageSize3 = pageSize;
                    Controls.ModelBasedTwoGrids.ModelID = ModelID;
                    Controls.ModelBasedTwoGrids.ModelName = ModelName;
                    //     Controls.DefinitionGrid.Definition = 
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID + ModelID.ToString());

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.ModelBasedTwoGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.ModelBasedTwoGrids();
                        // tab.Padding = 

                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID + ModelID.ToString();
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        RadToolTip tooltip = new RadToolTip();
                        tooltip.Content = ModelName;
                        ToolTipService.SetToolTip(tab, tooltip);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }
            if (orig.Description == "Model Based Tripple")
            {

                try
                {
                    Controls.ModelBasedThreeGrids.DisplayID = DisplayID;
                    Controls.ModelBasedThreeGrids.pageSize = pageSize;
                    Controls.ModelBasedThreeGrids.pageSize2 = pageSize;
                    Controls.ModelBasedThreeGrids.pageSize3 = pageSize;
                    Controls.ModelBasedThreeGrids.ModelID = ModelID;
                    Controls.ModelBasedThreeGrids.ModelName = ModelName;
                    //     Controls.DefinitionGrid.Definition = 
                    string docPaneName = title.Replace(" ", "");
                    RadTabItem pane = FindControl<RadTabItem>((UIElement)MainTab, typeof(RadTabItem), title.Replace(" ", "") + menuID + ModelID.ToString());

                    if (pane != null)
                        MainTab.SelectedItem = pane;
                    else
                    {
                        //  string docPaneName = title.Replace(" ", "");

                        Controls.ModelBasedThreeGrids.DisplayID = DisplayID;


                        RadTabItem tab = new RadTabItem();
                        tab.Content = new Controls.ModelBasedThreeGrids();
                        // tab.Padding = 

                        tab.DropDownContent = title;
                        tab.Header = title;
                        tab.Name = title.Replace(" ", "") + menuID + ModelID.ToString();
                        tab.IsSelected = true;
                        tab.MouseRightButtonUp += new MouseButtonEventHandler(tab_MouseRightButtonUp);
                        tab.MouseRightButtonDown += new MouseButtonEventHandler(tab_MouseRightButtonDown);
                        RadToolTip tooltip = new RadToolTip();
                        tooltip.Content = ModelName;
                        ToolTipService.SetToolTip(tab, tooltip);
                        this.MainTab.Items.Add(tab);
                    }
                }
                catch
                {
                }


            }
        }

        void tab_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        ContextMenu cMenu;
        RadTabItem selectedTab;
        void tab_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            selectedTab = sender as RadTabItem;

            cMenu = new ContextMenu();
            System.Windows.Controls.MenuItem menuItem;

            menuItem = new System.Windows.Controls.MenuItem();
            menuItem.Header = "Close";
            menuItem.Click += new RoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);

            menuItem = new System.Windows.Controls.MenuItem();
            menuItem.Header = "Close All But This";
            menuItem.Click += new RoutedEventHandler(menuItem_Click);
            cMenu.Items.Add(menuItem);
            cMenu.IsOpen = true;
            cMenu.HorizontalOffset = e.GetPosition(LayoutRoot).X;
            cMenu.VerticalOffset = e.GetPosition(LayoutRoot).Y;


        }

        void menuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.MenuItem menu = sender as System.Windows.Controls.MenuItem;

            switch (menu.Header.ToString())
            {
                case "Close":
                    CloseTab();
                    break;

                case "Close All But This":
                    CloseAllTab();
                    break;
                default:
                    break;
            }
            cMenu.IsOpen = false;
        }


        private void CloseTab()
        {
            try
            {
                this.MainTab.Items.Remove(selectedTab);
            }
            catch
            {
            }
        }


        private void CloseAllTab()
        {
            try
            {
                List<RadTabItem> tabs = new List<RadTabItem>();

                foreach (RadTabItem item in MainTab.Items)
                {
                    if (item != selectedTab)
                       tabs.Add(item);
                }
                foreach (RadTabItem item in tabs)
                {
                    
                        this.MainTab.Items.Remove(item);
                }

            }
            catch
            {
            }
        }



        public T FindControl<T>(UIElement parent, Type targetType, string ControlName) where T : FrameworkElement
        {

            if (parent == null) return null;

            if (parent.GetType() == targetType && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }
            T result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                if (FindControl<T>(child, targetType, ControlName) != null)
                {
                    result = FindControl<T>(child, targetType, ControlName);
                    break;
                }
            }
            return result;
        }

        private void lstModelList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            GroupModel model = lstModelList.SelectedItem as GroupModel;
            if (model != null)
            {
                ModelID = model.Model_ID;
               // lblModelName.Text =  model.ModelName;
                expanderModelCaption.Text = model.ModelName;
                ModelName = model.ModelName;
                
            }
        }
    }

    public class TabItemModel
    {
        public string Title
        {
            get;
            set;
        }
        public UserControl Content
        {
            get;
            set;
        }
    }


    public class Category
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public int ID { get; set; }
        public int? DefID { get; set; }
        public int? PageSize { get; set; }

        public List<Category> SubCategories { get; set; }
    }
    public class TempDetails
    {
        public int TableID{ get;set;}
        public string TableName { get; set; }
        public bool SingleTable { get; set; }
        public string GridNumber { get; set; }
        public string Setting { get; set; }
      
    }
}

