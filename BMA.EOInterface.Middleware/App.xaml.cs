﻿namespace BMA.EOInterface.Middleware
{
    using System;
    using System.Runtime.Serialization;
    using System.ServiceModel.DomainServices.Client.ApplicationServices;
    using System.Windows;
    using System.Windows.Controls;

using System.Collections.Generic;
    using BMA.MiddlewareApp.Web;
    using System.Collections.ObjectModel;
    using Telerik.Windows.Controls;
    using BMA.MiddlewareApp;
    using System.Globalization;
    using System.Windows.Input;
    using BMA.MiddlewareApp.Admin;
  

    /// <summary>
    /// Main <see cref="Application"/> class.
    /// </summary>
    public partial class App : Application
    {

        //public List<TempDetails> tableList { get; set; }

       public ObservableCollection<Server> serverObjectList{ get; set; }
       public ObservableCollection<ControlType> sheetList { get; set; }
       public ObservableCollection<Table> tablebjectList { get; set; }
       public ObservableCollection<StoredProcedureName> storedProcedureObjectList { get; set; }
        public List<Server> serverList { get; set; }
        public List<Table> tableList { get; set; }
        public List<StoredProcedureName> storedProcedureList { get; set; }
        public List<Field> fieldList { get; set; }
        public int serverID { get; set; }
        public int tableID { get; set; }
        public int form1ID { get; set; }
        public DateTime LastActivity { get; set; }

        /// <summary>
        /// Creates a new <see cref="App"/> instance.
        /// </summary>
        public App()
        {
            InitializeComponent();
            serverID = 1000000;

            tableID = 1000000;
            // Create a WebContext and add it to the ApplicationLifetimeObjects collection.
            // This will then be available as WebContext.Current.
            WebContext webContext = new WebContext();
            webContext.Authentication = new FormsAuthentication();
            //webContext.Authentication = new WindowsAuthentication();
            this.ApplicationLifetimeObjects.Add(webContext);
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            System.Globalization.CultureInfo cultureInfo =
         new System.Globalization.CultureInfo("en-ZA");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");

            System.Globalization.DateTimeFormatInfo dateTimeInfo =
     new System.Globalization.DateTimeFormatInfo();
            dateTimeInfo.LongDatePattern = "dd-MMM-yyyy";
            dateTimeInfo.ShortDatePattern = "dd-MMM-yyyy";
            cultureInfo.DateTimeFormat = dateTimeInfo;
           // cultureInfo.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
           // cultureInfo.DateTimeFormat.LongTimePattern = "";

          
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");


            NumberFormatInfo oCurrentNFI = new NumberFormatInfo();

            oCurrentNFI.CurrencyGroupSeparator = " ";
            oCurrentNFI.CurrencyDecimalSeparator = ".";
            oCurrentNFI.CurrencyDecimalDigits = 2;
            oCurrentNFI.CurrencyNegativePattern = 1;
            oCurrentNFI.CurrencySymbol = "R";
            oCurrentNFI.NumberDecimalSeparator = ".";

           // cultureInfo.d
            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = dateTimeInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat = dateTimeInfo;
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat = oCurrentNFI;
            System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat = oCurrentNFI;
            // This will enable you to bind controls in XAML to WebContext.Current properties.
            this.Resources.Add("WebContext", WebContext.Current);

            // With the WebContext in a different library the AuthenticationContext cannot be found automatically
           ((WebAuthenticationService)WebContext.Current.Authentication).DomainContext = new AuthenticationContext();
            // This will automatically authenticate a user when using Windows authentication or when the user chose "Keep me signed in" on a previous login attempt.
          WebContext.Current.Authentication.LoadUser(this.Application_UserLoaded, null);
            Application.Current.Host.Settings.EnableAutoZoom = false;
            // Show some UI to the user while LoadUser is in progress
            this.InitializeRootVisual();
            if (Current.IsRunningOutOfBrowser)
            {
                Current.CheckAndDownloadUpdateCompleted += (sender2, e2) =>
                {
                    if (e2.Error == null && e2.UpdateAvailable)
                        MessageBox.Show("New version! Please restart!");
                };
                Current.CheckAndDownloadUpdateAsync();
            }
            Application.Current.RootVisual.MouseMove += new MouseEventHandler(RootVisual_MouseMove);
            Application.Current.RootVisual.KeyDown += new KeyEventHandler(RootVisual_KeyDown);
        }


        private void RootVisual_KeyDown(object sender, KeyEventArgs e)
        {
            LastActivity = DateTime.Now;
        }

        private void RootVisual_MouseMove(object sender, MouseEventArgs e)
        {
            LastActivity = DateTime.Now;
        }
        /// <summary>
        /// Invoked when the <see cref="LoadUserOperation"/> completes.
        /// Use this event handler to switch from the "loading UI" you created in <see cref="InitializeRootVisual"/> to the "application UI".
        /// </summary>
        private void Application_UserLoaded(LoadUserOperation operation)
        {
            
        }

        /// <summary>
        /// Initializes the <see cref="Application.RootVisual"/> property.
        /// The initial UI will be displayed before the LoadUser operation has completed.
        /// The LoadUser operation will cause user to be logged in automatically if using Windows authentication or if the user had selected the "Keep me signed in" option on a previous login.
        /// </summary>
        protected virtual void InitializeRootVisual()
        {
            BMA.MiddlewareApp.Controls.BusyIndicator busyIndicator = new BMA.MiddlewareApp.Controls.BusyIndicator();
            busyIndicator.Content = new RefreshPage();
            StyleManager.ApplicationTheme = new Windows8Theme();
           
          
            busyIndicator.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            busyIndicator.VerticalContentAlignment = VerticalAlignment.Stretch;

            this.RootVisual = busyIndicator;
        }


        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // If the app is running outside of the debugger then report the exception using a ChildWindow control.
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                // NOTE: This will allow the application to continue running after an exception has been thrown but not handled. 
                // For production applications this error handling should be replaced with something that will report the error to the website and stop the application.
                e.Handled = true;
                var ws = WCF.GetService();
                ws.sendMailAsync("pnyika@businessmodelling.com;pnthree@gmail.com", "pnyika@businessmodelling.com", "Middleware Application error", e.ExceptionObject.Message);

                e.Handled = true;

               string error = "An unexpected problem has occured in the system.  The appropriate personal has been notified.  If this is an urgent matter please contact the IT help desk." + e.ExceptionObject.Message;

           //    RadWindow.Alert(new DialogParameters() { Header = new ContentPresenter() { Content = "Application Error" }, Content = error });

                //ErrorWindow.CreateNew("An unexpected problem has occured in the system.  The appropriate personal has been notified.  If this is an urgent matter please contact the IT help desk." + e.ExceptionObject.Message);
            }
        }


    }
}