﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;



namespace FlipControl
{
    [ContentProperty("Page1")]
    public class FlipControl : Control
    {
        public FlipControl()
        {
            this.DefaultStyleKey = typeof(FlipControl);
        }




        public VerticalAlignment Side2VerticalAlignment
        {
            get { return (VerticalAlignment)GetValue(Side2VerticalAlignmentProperty); }
            set { SetValue(Side2VerticalAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Side2VerticalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Side2VerticalAlignmentProperty =
            DependencyProperty.Register("Side2VerticalAlignment", typeof(VerticalAlignment), typeof(FlipControl), new PropertyMetadata(System.Windows.VerticalAlignment.Stretch, Side2VerticalAlignmentChanged));

        private static void Side2VerticalAlignmentChanged(
                    object sender,
                    DependencyPropertyChangedEventArgs e)
        {

            var owner = (FlipControl)sender;
            owner.OnSide2VerticalAlignmentChanged(e);


        }

        protected void OnSide2VerticalAlignmentChanged(DependencyPropertyChangedEventArgs e)
        {
            //TODO: Write your implementation here

        }




        public VerticalAlignment Side1VerticalAlignment
        {
            get { return (VerticalAlignment)GetValue(Side1VerticalAlignmentProperty); }
            set { SetValue(Side1VerticalAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Side1VerticalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Side1VerticalAlignmentProperty =
            DependencyProperty.Register("Side1VerticalAlignment", typeof(VerticalAlignment), typeof(FlipControl), new PropertyMetadata(System.Windows.VerticalAlignment.Stretch, Side1VerticalAlignmentChanged));

        private static void Side1VerticalAlignmentChanged(
                    object sender,
                    DependencyPropertyChangedEventArgs e)
        {

            var owner = (FlipControl)sender;
            owner.OnSide1VerticalAlignmentChanged(e);


        }

        protected void OnSide1VerticalAlignmentChanged(DependencyPropertyChangedEventArgs e)
        {
            //TODO: Write your implementation here

        }




        public HorizontalAlignment Side2HorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(Side2HorizontalAlignmentProperty); }
            set { SetValue(Side2HorizontalAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Side2HorizontalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Side2HorizontalAlignmentProperty =
            DependencyProperty.Register("Side2HorizontalAlignment", typeof(HorizontalAlignment), typeof(FlipControl), new PropertyMetadata(System.Windows.HorizontalAlignment.Stretch, Side2HorizontalAlignmentChanged));

        private static void Side2HorizontalAlignmentChanged(
                    object sender,
                    DependencyPropertyChangedEventArgs e)
        {

            var owner = (FlipControl)sender;
            owner.OnSide2HorizontalAlignmentChanged(e);


        }

        protected void OnSide2HorizontalAlignmentChanged(DependencyPropertyChangedEventArgs e)
        {
            //TODO: Write your implementation here

        }



        public HorizontalAlignment Side1HorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(Side1HorizontalAlignmentProperty); }
            set { SetValue(Side1HorizontalAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Side1HorizontalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Side1HorizontalAlignmentProperty =
            DependencyProperty.Register("Side1HorizontalAlignment", typeof(HorizontalAlignment), typeof(FlipControl), new PropertyMetadata(System.Windows.HorizontalAlignment.Stretch, Side1HorizontalAlignmentChanged));

        private static void Side1HorizontalAlignmentChanged(
                    object sender,
                    DependencyPropertyChangedEventArgs e)
        {

            var owner = (FlipControl)sender;
            owner.OnSide1HorizontalAlignmentChanged(e);


        }

        protected void OnSide1HorizontalAlignmentChanged(DependencyPropertyChangedEventArgs e)
        {
            //TODO: Write your implementation here

        }








        public override void OnApplyTemplate()
        {
            _isLoading = true;
            try
            {

                _rectangle = GetTemplateChild("rectangle") as Border ?? new Border();
                _side1 = GetTemplateChild("Side1") as ContentPresenter;
                _side2 = GetTemplateChild("Side2") as ContentPresenter;
                _root = GetTemplateChild("RootElement") as Panel;
                _image1 = GetTemplateChild("Side1Image") as Image;
                _image2 = GetTemplateChild("Side2Image") as Image;

                _vsGroup = GetTemplateChild("VisualStateGroup") as VisualStateGroup;
                _vsGroup.CurrentStateChanging += VsGroupCurrentStateChanging;
                _vsGroup.CurrentStateChanged += VsGroupCurrentStateChanged;
                _side1.Content = Page1;
                _side2.Content = Page2;

                if (this.Effect != null)
                {
                    _rectangle.Effect = this.Effect;
                    this.Effect = null;
                }

                _rectangle.Background = FlipBackground;
                _rectangle.BorderBrush = BorderBrush;
                _rectangle.BorderThickness = BorderThickness;


                _side2.Opacity = 0;
                _side2.IsHitTestVisible = false;
                Page2Visible = false;
                VisualStateManager.GoToState(this, "Side1Visible", false);


                _isLoaded = true;

                base.OnApplyTemplate();
            }
            finally
            {
                _isLoading = false;
            }
        }

        private Size _side1Size;
        private Size _side2Size;

        public static bool ScanForFocus(FrameworkElement pnl)
        {
            if (pnl is Panel)
                foreach (var ct in ((Panel)pnl).Children.OfType<Control>())
                {
                    if (ct.Visibility == Visibility.Visible)
                    {
                        if (ct.Focus()) return true;
                        if (ct is UserControl)
                        {
                            var c = ct as UserControl;
                            if (ScanForFocus(c.FindName("LayoutRoot") as FrameworkElement)) return true;
                        }
                    }
                }
            else if (pnl is Control)
            {
                var c = pnl as Control;
                if (c.Visibility == Visibility.Visible)
                    if (c.IsTabStop)
                    {
                        c.Focus();
                        return true;
                    }
            }

            return false;
        }

        void VsGroupCurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {
            _image1.Visibility = Visibility.Collapsed;
            _image2.Visibility = Visibility.Collapsed;
            _side2.Visibility = Visibility.Visible;
            _side1.Visibility = Visibility.Visible;

            if (Page2Visible)
            {
                ScanForFocus(_side2.Content as FrameworkElement);
            }
            else
            {
                ScanForFocus(_side1.Content as FrameworkElement);

            }

        }

        void VsGroupCurrentStateChanging(object sender, VisualStateChangedEventArgs e)
        {



        }

        private void CreateImages()
        {
            if (_side1 == null) return;
            _side1.Visibility = Visibility.Visible;
            _side2.Visibility = Visibility.Visible;

            _side1.Opacity = 1;
            _side2.Opacity = 1;
            var transform1 = _side1.RenderTransform;
            var transform2 = _side2.RenderTransform;
            _side1.RenderTransform = null;
            _side2.RenderTransform = null;
            //  this.Measure();
            var bm1 = new WriteableBitmap(_side1, null);
            var bm2 = new WriteableBitmap(_side2, null);

            _side1.RenderTransform = transform1;
            _side2.RenderTransform = transform2;

            _image1.Source = bm1;
            _image2.Source = bm2;

            _image1.Height = bm1.PixelHeight;
            _image1.Width = bm1.PixelWidth;
            _image2.Height = bm2.PixelHeight;
            _image2.Width = bm2.PixelWidth;
            _side1.Visibility = System.Windows.Visibility.Collapsed;
            _side2.Visibility = System.Windows.Visibility.Collapsed;
            _image1.Visibility = Visibility.Visible;
            _image2.Visibility = Visibility.Visible;
            if (Page2Visible)
            {
                _image2.Opacity = 0;
                _image1.Opacity = 1;
            }
            else
            {
                _image1.Opacity = 0;
                _image2.Opacity = 1;
            }
        }

        private VisualStateGroup _vsGroup;
        private Border _rectangle;
        private ContentPresenter _side1;
        private ContentPresenter _side2;
        internal Panel _root;

        private bool _isLoaded;
        public Brush FlipBackground
        {

            get { return (Brush)GetValue(FlipBackgroundProperty); }
            set { SetValue(FlipBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FlipBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FlipBackgroundProperty =
            DependencyProperty.Register("FlipBackground", typeof(Brush), typeof(FlipControl), new PropertyMetadata(null, FlipBackgroundChanged));

        private static void FlipBackgroundChanged(
            object sender,
            DependencyPropertyChangedEventArgs e)
        {

            var owner = (FlipControl)sender;



        }



        public class PageChangedEventArgs : EventArgs
        {
            public int NewPage { get; set; }
        }

        public delegate void PageChangedEventHandler(object sender, PageChangedEventArgs e);

        public event PageChangedEventHandler PageChanged;

        public void InvokePageChanged(PageChangedEventArgs e)
        {
            PageChangedEventHandler handler = PageChanged;
            if (handler != null) handler(this, e);
        }


        public bool Page2Visible
        {
            get { return (bool)GetValue(Page2VisibleProperty); }
            set { SetValue(Page2VisibleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Page2Visible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Page2VisibleProperty =
            DependencyProperty.Register("Page2Visible", typeof(bool), typeof(FlipControl), new PropertyMetadata(false, Page2VisibleChanged));

        private static void Page2VisibleChanged(
            object sender,
            DependencyPropertyChangedEventArgs e)
        {
            if (_isLoading) return;
            var owner = (FlipControl)sender;
            owner.CreateImages();
            if (owner._side1 == null) return;
            if (owner.Page2Visible)
            {
                owner.Dispatcher.BeginInvoke(() => VisualStateManager.GoToState(owner, "Side2Visible", owner._isLoaded));
                owner.InvokePageChanged(new PageChangedEventArgs { NewPage = 2 });
            }
            else
            {
                owner.Dispatcher.BeginInvoke(() => VisualStateManager.GoToState(owner, "Side1Visible", owner._isLoaded));
                owner.InvokePageChanged(new PageChangedEventArgs { NewPage = 1 });
            }

        }




        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(FlipControl), new PropertyMetadata(new CornerRadius(0), CornerRadiusChanged));

        private static void CornerRadiusChanged(
            object sender,
            DependencyPropertyChangedEventArgs e)
        {

            var owner = (FlipControl)sender;
            owner.OnCornerRadiusChanged(e);


        }

        protected void OnCornerRadiusChanged(DependencyPropertyChangedEventArgs e)
        {

        }




        public FrameworkElement Page1
        {
            get { return (FrameworkElement)GetValue(Page1Property); }
            set { SetValue(Page1Property, value); }
        }

        // Using a DependencyProperty as the backing store for Page1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Page1Property =
            DependencyProperty.Register("Page1", typeof(FrameworkElement), typeof(FlipControl), new PropertyMetadata(new StackPanel()));


        public FrameworkElement Page2
        {
            get { return (FrameworkElement)GetValue(Page2Property); }
            set { SetValue(Page2Property, value); }
        }

        // Using a DependencyProperty as the backing store for Page2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Page2Property =
            DependencyProperty.Register("Page2", typeof(FrameworkElement), typeof(FlipControl), new PropertyMetadata(new StackPanel()));

        private Image _image2;
        private Image _image1;
        private static bool _isLoading;
    }
}


